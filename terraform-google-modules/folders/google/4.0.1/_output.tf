
output "folder" {
  description = "Folder resource (for single use)."
  value       = module.folders.folder
}

output "folders" {
  description = "Folder resources as list."
  value       = module.folders.folders
}

output "folders_map" {
  description = "Folder resources by name."
  value       = module.folders.folders_map
}

output "id" {
  description = "Folder id (for single use)."
  value       = module.folders.id
}

output "ids" {
  description = "Folder ids."
  value       = module.folders.ids
}

output "ids_list" {
  description = "List of folder ids."
  value       = module.folders.ids_list
}

output "name" {
  description = "Folder name (for single use)."
  value       = module.folders.name
}

output "names" {
  description = "Folder names."
  value       = module.folders.names
}

output "names_list" {
  description = "List of folder names."
  value       = module.folders.names_list
}

output "per_folder_admins" {
  description = "IAM-style members per folder who will get extended permissions."
  value       = module.folders.per_folder_admins
}


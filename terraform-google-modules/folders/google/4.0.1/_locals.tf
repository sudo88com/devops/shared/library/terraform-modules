
locals {
  all_folder_admins = var.all_folder_admins
  folder_admin_roles = var.folder_admin_roles
  names = var.names
  parent = var.parent
  per_folder_admins = var.per_folder_admins
  prefix = var.prefix
  set_roles = var.set_roles
}


module "kms" {
  source = "terraform-aws-modules/kms/aws"
  version = "2.3.0"
  decrypters = var.decrypters
  encrypters = var.encrypters
  key_algorithm = var.key_algorithm
  key_destroy_scheduled_duration = var.key_destroy_scheduled_duration
  key_protection_level = var.key_protection_level
  key_rotation_period = var.key_rotation_period
  keyring = var.keyring
  keys = var.keys
  labels = var.labels
  location = var.location
  owners = var.owners
  prevent_destroy = var.prevent_destroy
  project_id = var.project_id
  purpose = var.purpose
  set_decrypters_for = var.set_decrypters_for
  set_encrypters_for = var.set_encrypters_for
  set_owners_for = var.set_owners_for
}

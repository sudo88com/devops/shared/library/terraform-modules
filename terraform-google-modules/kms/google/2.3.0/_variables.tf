
variable "decrypters" {
  type        = list(string)
  description = "List of comma-separated owners for each key declared in set_decrypters_for."
  default     = []
}

variable "encrypters" {
  type        = list(string)
  description = "List of comma-separated owners for each key declared in set_encrypters_for."
  default     = []
}

variable "key_algorithm" {
  type        = string
  description = "The algorithm to use when creating a version based on this template. See the https://cloud.google.com/kms/docs/reference/rest/v1/CryptoKeyVersionAlgorithm for possible inputs."
  default     = "GOOGLE_SYMMETRIC_ENCRYPTION"
}

variable "key_destroy_scheduled_duration" {
  type        = string
  description = "Set the period of time that versions of keys spend in the DESTROY_SCHEDULED state before transitioning to DESTROYED."
  default     = null
}

variable "key_protection_level" {
  type        = string
  description = "The protection level to use when creating a version based on this template. Default value: \"SOFTWARE\" Possible values: [\"SOFTWARE\", \"HSM\"]"
  default     = "SOFTWARE"
}

variable "key_rotation_period" {
  type        = string
  description = "Generate a new key every time this period passes."
  default     = "7776000s"
}

variable "keyring" {
  type        = string
  description = "Keyring name."
  default     = ""
}

variable "keys" {
  type        = list(string)
  description = "Key names."
  default     = []
}

variable "labels" {
  type        = map(string)
  description = "Labels, provided as a map"
  default     = {}
}

variable "location" {
  type        = string
  description = "Location for the keyring."
  default     = ""
}

variable "owners" {
  type        = list(string)
  description = "List of comma-separated owners for each key declared in set_owners_for."
  default     = []
}

variable "prevent_destroy" {
  type        = bool
  description = "Set the prevent_destroy lifecycle attribute on keys."
  default     = true
}

variable "project_id" {
  type        = string
  description = "Project id where the keyring will be created."
  default     = ""
}

variable "purpose" {
  type        = string
  description = "The immutable purpose of the CryptoKey. Possible values are ENCRYPT_DECRYPT, ASYMMETRIC_SIGN, and ASYMMETRIC_DECRYPT."
  default     = "ENCRYPT_DECRYPT"
}

variable "set_decrypters_for" {
  type        = list(string)
  description = "Name of keys for which decrypters will be set."
  default     = []
}

variable "set_encrypters_for" {
  type        = list(string)
  description = "Name of keys for which encrypters will be set."
  default     = []
}

variable "set_owners_for" {
  type        = list(string)
  description = "Name of keys for which owners will be set."
  default     = []
}



variable "ensure_undelete" {
  type        = bool
  description = "Run gcloud command before creating cloud endpoint to force undelete of service endpoint. If endpoint has recently been deleted, it cannot be re-created without first undeleting it."
  default     = true
}

variable "external_ip" {
  type        = string
  description = "External IP the endpoint will point to."
  default     = ""
}

variable "name" {
  type        = string
  description = "Name of the cloud endpoints service. This will create a DNS record in the form of: NAME.endpoints.PROJECT.cloud.goog"
  default     = ""
}

variable "project" {
  type        = string
  description = "Project to create the Cloud Endpoint service in. If not given, the default provider is used."
  default     = ""
}



output "config_id" {
  description = "The rollout config ID for the endpoint service."
  value       = module.endpoints-dns.config_id
}

output "endpoint" {
  description = "The name of the DNS record conventional to the Cloud Endpoints format of: NAME.endpoints.PROJECT.cloud.goog. Not dependent on google_endpoints_service resource."
  value       = module.endpoints-dns.endpoint
}

output "endpoint_computed" {
  description = "The address of the cloud endpoint. This is computed from the google_endpoints_service resource and can be used to create dependencies between resources."
  value       = module.endpoints-dns.endpoint_computed
}

output "external_ip" {
  description = "The value of the external IP the endpoint points to."
  value       = module.endpoints-dns.external_ip
}

output "name" {
  description = "Name of the cloud endpoints service."
  value       = module.endpoints-dns.name
}

output "project" {
  description = "The project where the cloud endpoint was created."
  value       = module.endpoints-dns.project
}



locals {
  ensure_undelete = var.ensure_undelete
  external_ip = var.external_ip
  name = var.name
  project = var.project
}


module "endpoints-dns" {
  source = "terraform-aws-modules/endpoints-dns/aws"
  version = "3.0.1"
  ensure_undelete = var.ensure_undelete
  external_ip = var.external_ip
  name = var.name
  project = var.project
}

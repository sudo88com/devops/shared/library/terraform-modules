
output "name" {
  description = "Name of the Cloud NAT"
  value       = module.cloud-nat.name
}

output "nat_ip_allocate_option" {
  description = "NAT IP allocation mode"
  value       = module.cloud-nat.nat_ip_allocate_option
}

output "region" {
  description = "Cloud NAT region"
  value       = module.cloud-nat.region
}

output "router_name" {
  description = "Cloud NAT router name"
  value       = module.cloud-nat.router_name
}


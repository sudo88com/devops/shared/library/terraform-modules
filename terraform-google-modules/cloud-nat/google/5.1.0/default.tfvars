
create_router = false

enable_dynamic_port_allocation = false

enable_endpoint_independent_mapping = false

icmp_idle_timeout_sec = "30"

log_config_enable = false

log_config_filter = "ALL"

max_ports_per_vm = null

min_ports_per_vm = "64"

name = ""

nat_ips = []

network = ""

project_id = 

region = 

router = 

router_asn = "64514"

router_keepalive_interval = "20"

source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

subnetworks = []

tcp_established_idle_timeout_sec = "1200"

tcp_time_wait_timeout_sec = "120"

tcp_transitory_idle_timeout_sec = "30"

udp_idle_timeout_sec = "30"


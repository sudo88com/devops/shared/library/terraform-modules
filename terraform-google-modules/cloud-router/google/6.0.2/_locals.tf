
locals {
  bgp = var.bgp
  description = var.description
  name = var.name
  nats = var.nats
  network = var.network
  project = var.project
  region = var.region
}


variable "bgp" {
  type        = object({
    asn               = string
    advertise_mode    = optional(string, "CUSTOM")
    advertised_groups = optional(list(string))
    advertised_ip_ranges = optional(list(object({
      range       = string
      description = optional(string)
    })), [])
    keepalive_interval = optional(number)
  })
  description = "BGP information specific to this router."
  default     = null
}

variable "description" {
  type        = string
  description = "An optional description of this resource"
  default     = null
}

variable "name" {
  type        = string
  description = "Name of the router"
  default     = ""
}

variable "nats" {
  type        = list(object({
    name                                = string
    nat_ip_allocate_option              = optional(string)
    source_subnetwork_ip_ranges_to_nat  = optional(string)
    nat_ips                             = optional(list(string), [])
    min_ports_per_vm                    = optional(number)
    max_ports_per_vm                    = optional(number)
    udp_idle_timeout_sec                = optional(number)
    icmp_idle_timeout_sec               = optional(number)
    tcp_established_idle_timeout_sec    = optional(number)
    tcp_transitory_idle_timeout_sec     = optional(number)
    tcp_time_wait_timeout_sec           = optional(number)
    enable_endpoint_independent_mapping = optional(bool)
    enable_dynamic_port_allocation      = optional(bool)

    log_config = optional(object({
      enable = optional(bool, true)
      filter = optional(string, "ALL")
    }), {})

    subnetworks = optional(list(object({
      name                     = string
      source_ip_ranges_to_nat  = list(string)
      secondary_ip_range_names = optional(list(string))
    })), [])

  }))
  description = "NATs to deploy on this router."
  default     = []
}

variable "network" {
  type        = string
  description = "A reference to the network to which this router belongs"
  default     = ""
}

variable "project" {
  type        = string
  description = "The project ID to deploy to"
  default     = ""
}

variable "region" {
  type        = string
  description = "Region where the router resides"
  default     = ""
}



output "nat" {
  description = "Created NATs"
  value       = module.cloud-router.nat
}

output "router" {
  description = "Created Router"
  value       = module.cloud-router.router
}


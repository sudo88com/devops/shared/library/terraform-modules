
variable "datafusion_version" {
  type        = string
  description = "Data Fusion Version to deploy."
  default     = null
}

variable "dataproc_subnet" {
  type        = string
  description = "Name for subnet to create and configure for Dataproc clusters controlled by private Data Fusion instance."
  default     = "dataproc-subnet"
}

variable "description" {
  type        = string
  description = "An optional description of the instance."
  default     = null
}

variable "labels" {
  type        = map(string)
  description = "The resource labels for instance to use to annotate any related underlying resources, such as Compute Engine VMs."
  default     = {}
}

variable "name" {
  type        = string
  description = "Name of the instance."
  default     = ""
}

variable "network" {
  type        = string
  description = "Name for VPC to create or reuse to be configured for use with private Data Fusion instance."
  default     = ""
}

variable "options" {
  type        = map(string)
  description = "Map of additional options used to configure the behavior of Data Fusion instance."
  default     = {}
}

variable "project" {
  type        = string
  description = "The project ID to deploy to."
  default     = ""
}

variable "region" {
  type        = string
  description = "The region of the instance."
  default     = ""
}

variable "type" {
  type        = string
  description = "Represents the type of the instance (BASIC or ENTERPRISE)"
  default     = "ENTERPRISE"
}


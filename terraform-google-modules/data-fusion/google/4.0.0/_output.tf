
output "instance" {
  description = "The created CDF instance"
  value       = module.data-fusion.instance
}

output "tenant_project" {
  description = "The Google managed tenant project ID in which the instance will run its jobs"
  value       = module.data-fusion.tenant_project
}


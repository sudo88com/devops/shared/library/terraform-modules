
output "id" {
  description = "The unique Id of the newly created Dataflow job"
  value       = module.dataflow.id
}

output "name" {
  description = "The name of the dataflow job"
  value       = module.dataflow.name
}

output "state" {
  description = "The state of the newly created Dataflow job"
  value       = module.dataflow.state
}

output "temp_gcs_location" {
  description = "The GCS path for the Dataflow job's temporary data."
  value       = module.dataflow.temp_gcs_location
}

output "template_gcs_path" {
  description = "The GCS path to the Dataflow job template."
  value       = module.dataflow.template_gcs_path
}



locals {
  additional_experiments = var.additional_experiments
  ip_configuration = var.ip_configuration
  kms_key_name = var.kms_key_name
  labels = var.labels
  machine_type = var.machine_type
  max_workers = var.max_workers
  name = var.name
  network_self_link = var.network_self_link
  on_delete = var.on_delete
  parameters = var.parameters
  project_id = var.project_id
  region = var.region
  service_account_email = var.service_account_email
  subnetwork_self_link = var.subnetwork_self_link
  temp_gcs_location = var.temp_gcs_location
  template_gcs_path = var.template_gcs_path
  zone = var.zone
}


output "jenkins_instance_initial_password" {
  description = "The initial password assigned to the Jenkins instance's `user` username"
  value       = module.jenkins.jenkins_instance_initial_password
}

output "jenkins_instance_name" {
  description = "The name of the running Jenkins instance"
  value       = module.jenkins.jenkins_instance_name
}

output "jenkins_instance_public_ip" {
  description = "The public IP of the Jenkins instance"
  value       = module.jenkins.jenkins_instance_public_ip
}

output "jenkins_instance_service_account_email" {
  description = "The email address of the created service account"
  value       = module.jenkins.jenkins_instance_service_account_email
}

output "jenkins_instance_zone" {
  description = "The zone in which Jenkins is running"
  value       = module.jenkins.jenkins_instance_zone
}



module "utils" {
  source = "terraform-aws-modules/utils/aws"
  version = "0.7.1"
  additional_regions = var.additional_regions
  region = var.region
}

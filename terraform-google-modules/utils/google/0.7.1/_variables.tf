
variable "additional_regions" {
  type        = list(string)
  description = "A user-supplied list of regions to extend the lookup map."
  default     = []
}

variable "region" {
  type        = string
  description = "The GCP region to retrieve a short name for (ex. `us-central1)."
  default     = null
}



output "hostname" {
  description = "Host name of the bastion"
  value       = module.bastion-host.hostname
}

output "instance_template" {
  description = "Self link of the bastion instance template for use with a MIG"
  value       = module.bastion-host.instance_template
}

output "ip_address" {
  description = "Internal IP address of the bastion host"
  value       = module.bastion-host.ip_address
}

output "self_link" {
  description = "Self link of the bastion host"
  value       = module.bastion-host.self_link
}

output "service_account" {
  description = "The email for the service account created for the bastion host"
  value       = module.bastion-host.service_account
}


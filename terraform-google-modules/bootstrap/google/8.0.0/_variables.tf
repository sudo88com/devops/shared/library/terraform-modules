
variable "activate_apis" {
  type        = list(string)
  description = "List of APIs to enable in the seed project."
  default     = [
  "serviceusage.googleapis.com",
  "servicenetworking.googleapis.com",
  "compute.googleapis.com",
  "logging.googleapis.com",
  "bigquery.googleapis.com",
  "cloudresourcemanager.googleapis.com",
  "cloudbilling.googleapis.com",
  "iam.googleapis.com",
  "admin.googleapis.com",
  "appengine.googleapis.com",
  "storage-api.googleapis.com",
  "monitoring.googleapis.com"
]
}

variable "billing_account" {
  type        = string
  description = "The ID of the billing account to associate projects with."
  default     = ""
}

variable "create_terraform_sa" {
  type        = bool
  description = "If the Terraform service account should be created."
  default     = true
}

variable "default_region" {
  type        = string
  description = "Default region to create resources where applicable."
  default     = "us-central1"
}

variable "encrypt_gcs_bucket_tfstate" {
  type        = bool
  description = "Encrypt bucket used for storing terraform state files in seed project."
  default     = false
}

variable "folder_id" {
  type        = string
  description = "The ID of a folder to host this project"
  default     = ""
}

variable "force_destroy" {
  type        = bool
  description = "If supplied, the state bucket will be deleted even while containing objects."
  default     = false
}

variable "grant_billing_user" {
  type        = bool
  description = "Grant roles/billing.user role to CFT service account"
  default     = true
}

variable "group_billing_admins" {
  type        = string
  description = "Google Group for GCP Billing Administrators"
  default     = ""
}

variable "group_org_admins" {
  type        = string
  description = "Google Group for GCP Organization Administrators"
  default     = ""
}

variable "key_protection_level" {
  type        = string
  description = "The protection level to use when creating a version based on this template. Default value: \"SOFTWARE\" Possible values: [\"SOFTWARE\", \"HSM\"]"
  default     = "SOFTWARE"
}

variable "key_rotation_period" {
  type        = string
  description = "The rotation period of the key."
  default     = null
}

variable "kms_prevent_destroy" {
  type        = bool
  description = "Set the prevent_destroy lifecycle attribute on keys."
  default     = true
}

variable "org_admins_org_iam_permissions" {
  type        = list(string)
  description = "List of permissions granted to the group supplied in group_org_admins variable across the GCP organization."
  default     = [
  "roles/billing.user",
  "roles/resourcemanager.organizationAdmin"
]
}

variable "org_id" {
  type        = string
  description = "GCP Organization ID"
  default     = ""
}

variable "org_project_creators" {
  type        = list(string)
  description = "Additional list of members to have project creator role accross the organization. Prefix of group: user: or serviceAccount: is required."
  default     = []
}

variable "parent_folder" {
  type        = string
  description = "GCP parent folder ID in the form folders/{id}"
  default     = ""
}

variable "project_id" {
  type        = string
  description = "Custom project ID to use for project created. If not supplied, the default id is {project_prefix}-seed-{random suffix}."
  default     = ""
}

variable "project_labels" {
  type        = map(string)
  description = "Labels to apply to the project."
  default     = {}
}

variable "project_prefix" {
  type        = string
  description = "Name prefix to use for projects created."
  default     = "cft"
}

variable "random_suffix" {
  type        = bool
  description = "Appends a 4 character random suffix to project ID and GCS bucket name."
  default     = true
}

variable "sa_enable_impersonation" {
  type        = bool
  description = "Allow org_admins group to impersonate service account & enable APIs required."
  default     = false
}

variable "sa_org_iam_permissions" {
  type        = list(string)
  description = "List of permissions granted to Terraform service account across the GCP organization."
  default     = [
  "roles/billing.user",
  "roles/compute.networkAdmin",
  "roles/compute.xpnAdmin",
  "roles/iam.securityAdmin",
  "roles/iam.serviceAccountAdmin",
  "roles/logging.configWriter",
  "roles/orgpolicy.policyAdmin",
  "roles/resourcemanager.folderAdmin",
  "roles/resourcemanager.organizationViewer"
]
}

variable "state_bucket_name" {
  type        = string
  description = "Custom state bucket name. If not supplied, the default name is {project_prefix}-tfstate-{random suffix}."
  default     = ""
}

variable "storage_bucket_labels" {
  type        = map(string)
  description = "Labels to apply to the storage bucket."
  default     = {}
}

variable "tf_service_account_id" {
  type        = string
  description = "ID of service account for terraform in seed project"
  default     = "org-terraform"
}

variable "tf_service_account_name" {
  type        = string
  description = "Display name of service account for terraform in seed project"
  default     = "CFT Organization Terraform Account"
}


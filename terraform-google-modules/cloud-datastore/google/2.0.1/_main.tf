
module "cloud-datastore" {
  source = "terraform-aws-modules/cloud-datastore/aws"
  version = "2.0.1"
  indexes = var.indexes
  project = var.project
}

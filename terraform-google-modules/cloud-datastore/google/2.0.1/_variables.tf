
variable "indexes" {
  type        = string
  description = "The contents of a index.yaml file, to apply indexes from"
  default     = ""
}

variable "project" {
  type        = string
  description = "The project id"
  default     = ""
}



output "ca_certificate" {
  description = "Cluster ca certificate (base64 encoded)"
  value       = module.kubernetes-engine.ca_certificate
}

output "cluster_id" {
  description = "Cluster ID"
  value       = module.kubernetes-engine.cluster_id
}

output "dns_cache_enabled" {
  description = "Whether DNS Cache enabled"
  value       = module.kubernetes-engine.dns_cache_enabled
}

output "endpoint" {
  description = "Cluster endpoint"
  value       = module.kubernetes-engine.endpoint
}

output "fleet_membership" {
  description = "Fleet membership (if registered)"
  value       = module.kubernetes-engine.fleet_membership
}

output "gateway_api_channel" {
  description = "The gateway api channel of this cluster."
  value       = module.kubernetes-engine.gateway_api_channel
}

output "horizontal_pod_autoscaling_enabled" {
  description = "Whether horizontal pod autoscaling enabled"
  value       = module.kubernetes-engine.horizontal_pod_autoscaling_enabled
}

output "http_load_balancing_enabled" {
  description = "Whether http load balancing enabled"
  value       = module.kubernetes-engine.http_load_balancing_enabled
}

output "identity_namespace" {
  description = "Workload Identity pool"
  value       = module.kubernetes-engine.identity_namespace
}

output "instance_group_urls" {
  description = "List of GKE generated instance groups"
  value       = module.kubernetes-engine.instance_group_urls
}

output "location" {
  description = "Cluster location (region if regional cluster, zone if zonal cluster)"
  value       = module.kubernetes-engine.location
}

output "logging_service" {
  description = "Logging service used"
  value       = module.kubernetes-engine.logging_service
}

output "master_authorized_networks_config" {
  description = "Networks from which access to master is permitted"
  value       = module.kubernetes-engine.master_authorized_networks_config
}

output "master_version" {
  description = "Current master kubernetes version"
  value       = module.kubernetes-engine.master_version
}

output "mesh_certificates_config" {
  description = "Mesh certificates configuration"
  value       = module.kubernetes-engine.mesh_certificates_config
}

output "min_master_version" {
  description = "Minimum master kubernetes version"
  value       = module.kubernetes-engine.min_master_version
}

output "monitoring_service" {
  description = "Monitoring service used"
  value       = module.kubernetes-engine.monitoring_service
}

output "name" {
  description = "Cluster name"
  value       = module.kubernetes-engine.name
}

output "network_policy_enabled" {
  description = "Whether network policy enabled"
  value       = module.kubernetes-engine.network_policy_enabled
}

output "node_pools_names" {
  description = "List of node pools names"
  value       = module.kubernetes-engine.node_pools_names
}

output "node_pools_versions" {
  description = "Node pool versions by node pool name"
  value       = module.kubernetes-engine.node_pools_versions
}

output "region" {
  description = "Cluster region"
  value       = module.kubernetes-engine.region
}

output "release_channel" {
  description = "The release channel of this cluster"
  value       = module.kubernetes-engine.release_channel
}

output "service_account" {
  description = "The service account to default running nodes as if not overridden in `node_pools`."
  value       = module.kubernetes-engine.service_account
}

output "tpu_ipv4_cidr_block" {
  description = "The IP range in CIDR notation used for the TPUs"
  value       = module.kubernetes-engine.tpu_ipv4_cidr_block
}

output "type" {
  description = "Cluster type (regional / zonal)"
  value       = module.kubernetes-engine.type
}

output "vertical_pod_autoscaling_enabled" {
  description = "Whether vertical pod autoscaling enabled"
  value       = module.kubernetes-engine.vertical_pod_autoscaling_enabled
}

output "zones" {
  description = "List of zones in which the cluster resides"
  value       = module.kubernetes-engine.zones
}


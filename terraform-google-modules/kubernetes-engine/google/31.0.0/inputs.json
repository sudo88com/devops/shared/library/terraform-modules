[
  {
    "name": "filestore_csi_driver",
    "type": "bool",
    "description": "The status of the Filestore CSI driver addon, which allows the usage of filestore instance as volumes",
    "default": "false",
    "required": false
  },
  {
    "name": "node_metadata",
    "type": "string",
    "description": "Specifies how node metadata is exposed to the workload running on the node",
    "default": "\"GKE_METADATA\"",
    "required": false
  },
  {
    "name": "http_load_balancing",
    "type": "bool",
    "description": "Enable httpload balancer addon",
    "default": "true",
    "required": false
  },
  {
    "name": "stack_type",
    "type": "string",
    "description": "The stack type to use for this cluster. Either `IPV4` or `IPV4_IPV6`. Defaults to `IPV4`.",
    "default": "\"IPV4\"",
    "required": false
  },
  {
    "name": "config_connector",
    "type": "bool",
    "description": "Whether ConfigConnector is enabled for this cluster.",
    "default": "false",
    "required": false
  },
  {
    "name": "monitoring_service",
    "type": "string",
    "description": "The monitoring service that the cluster should write metrics to. Automatically send metrics from pods in the cluster to the Google Cloud Monitoring API. VM metrics will be collected by Google Compute Engine regardless of this setting Available options include monitoring.googleapis.com, monitoring.googleapis.com/kubernetes (beta) and none",
    "default": "\"monitoring.googleapis.com/kubernetes\"",
    "required": false
  },
  {
    "name": "dns_cache",
    "type": "bool",
    "description": "The status of the NodeLocal DNSCache addon.",
    "default": "false",
    "required": false
  },
  {
    "name": "release_channel",
    "type": "string",
    "description": "The release channel of this cluster. Accepted values are `UNSPECIFIED`, `RAPID`, `REGULAR` and `STABLE`. Defaults to `REGULAR`.",
    "default": "\"REGULAR\"",
    "required": false
  },
  {
    "name": "project_id",
    "type": "string",
    "description": "The project ID to host the cluster in (required)",
    "default": "",
    "required": true
  },
  {
    "name": "stub_domains",
    "type": "map(list(string))",
    "description": "Map of stub domains and their resolvers to forward DNS queries for a certain domain to an external DNS server",
    "default": "{}",
    "required": false
  },
  {
    "name": "database_encryption",
    "type": "list(object({ state = string, key_name = string }))",
    "description": "Application-layer Secrets Encryption settings. The object format is {state = string, key_name = string}. Valid values of state are: \"ENCRYPTED\"; \"DECRYPTED\". key_name is the name of a CloudKMS key.",
    "default": "[\n  {\n    \"key_name\": \"\",\n    \"state\": \"DECRYPTED\"\n  }\n]",
    "required": false
  },
  {
    "name": "notification_config_topic",
    "type": "string",
    "description": "The desired Pub/Sub topic to which notifications will be sent by GKE. Format is projects/{project}/topics/{topic}.",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "master_authorized_networks",
    "type": "list(object({ cidr_block = string, display_name = string }))",
    "description": "List of master authorized networks. If none are provided, disallow external access (except the cluster node IPs, which GKE automatically whitelists).",
    "default": "[]",
    "required": false
  },
  {
    "name": "horizontal_pod_autoscaling",
    "type": "bool",
    "description": "Enable horizontal pod autoscaling addon",
    "default": "true",
    "required": false
  },
  {
    "name": "shadow_firewall_rules_log_config",
    "type": "object({\n    metadata = string\n  })",
    "description": "The log_config for shadow firewall rules. You can set this variable to `null` to disable logging.",
    "default": "{\n  \"metadata\": \"INCLUDE_ALL_METADATA\"\n}",
    "required": false
  },
  {
    "name": "deletion_protection",
    "type": "bool",
    "description": "Whether or not to allow Terraform to destroy the cluster.",
    "default": "true",
    "required": false
  },
  {
    "name": "cluster_dns_scope",
    "type": "string",
    "description": "The scope of access to cluster DNS records. DNS_SCOPE_UNSPECIFIED (default) or CLUSTER_SCOPE or VPC_SCOPE. ",
    "default": "\"DNS_SCOPE_UNSPECIFIED\"",
    "required": false
  },
  {
    "name": "node_pools_metadata",
    "type": "map(map(string))",
    "description": "Map of maps containing node metadata by node-pool name",
    "default": "{\n  \"all\": {},\n  \"default-node-pool\": {}\n}",
    "required": false
  },
  {
    "name": "enable_confidential_nodes",
    "type": "bool",
    "description": "An optional flag to enable confidential node config.",
    "default": "false",
    "required": false
  },
  {
    "name": "gce_pd_csi_driver",
    "type": "bool",
    "description": "Whether this cluster should enable the Google Compute Engine Persistent Disk Container Storage Interface (CSI) Driver.",
    "default": "true",
    "required": false
  },
  {
    "name": "stateful_ha",
    "type": "bool",
    "description": "Whether the Stateful HA Addon is enabled for this cluster.",
    "default": "false",
    "required": false
  },
  {
    "name": "regional",
    "type": "bool",
    "description": "Whether is a regional cluster (zonal cluster if set false. WARNING: changing this after cluster creation is destructive!)",
    "default": "true",
    "required": false
  },
  {
    "name": "registry_project_ids",
    "type": "list(string)",
    "description": "Projects holding Google Container Registries. If empty, we use the cluster project. If a service account is created and the `grant_registry_access` variable is set to `true`, the `storage.objectViewer` and `artifactregsitry.reader` roles are assigned on these projects.",
    "default": "[]",
    "required": false
  },
  {
    "name": "enable_mesh_certificates",
    "type": "bool",
    "description": "Controls the issuance of workload mTLS certificates. When enabled the GKE Workload Identity Certificates controller and node agent will be deployed in the cluster. Requires Workload Identity.",
    "default": "false",
    "required": false
  },
  {
    "name": "timeouts",
    "type": "map(string)",
    "description": "Timeout for cluster operations.",
    "default": "{}",
    "required": false
  },
  {
    "name": "default_max_pods_per_node",
    "type": "number",
    "description": "The maximum number of pods to schedule per node",
    "default": "110",
    "required": false
  },
  {
    "name": "enable_l4_ilb_subsetting",
    "type": "bool",
    "description": "Enable L4 ILB Subsetting on the cluster",
    "default": "false",
    "required": false
  },
  {
    "name": "gateway_api_channel",
    "type": "string",
    "description": "The gateway api channel of this cluster. Accepted values are `CHANNEL_STANDARD` and `CHANNEL_DISABLED`.",
    "default": "null",
    "required": false
  },
  {
    "name": "gcs_fuse_csi_driver",
    "type": "bool",
    "description": "Whether GCE FUSE CSI driver is enabled for this cluster.",
    "default": "false",
    "required": false
  },
  {
    "name": "name",
    "type": "string",
    "description": "The name of the cluster (required)",
    "default": "",
    "required": true
  },
  {
    "name": "network_project_id",
    "type": "string",
    "description": "The project ID of the shared VPC's host (for shared vpc support)",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "remove_default_node_pool",
    "type": "bool",
    "description": "Remove default node pool while setting up the cluster",
    "default": "false",
    "required": false
  },
  {
    "name": "enable_kubernetes_alpha",
    "type": "bool",
    "description": "Whether to enable Kubernetes Alpha features for this cluster. Note that when this option is enabled, the cluster cannot be upgraded and will be automatically deleted after 30 days.",
    "default": "false",
    "required": false
  },
  {
    "name": "monitoring_observability_metrics_relay_mode",
    "type": "string",
    "description": "Mode used to make advanced datapath metrics relay available.",
    "default": "null",
    "required": false
  },
  {
    "name": "monitoring_enabled_components",
    "type": "list(string)",
    "description": "List of services to monitor: SYSTEM_COMPONENTS, WORKLOADS. Empty list is default GKE configuration.",
    "default": "[]",
    "required": false
  },
  {
    "name": "kubernetes_version",
    "type": "string",
    "description": "The Kubernetes version of the masters. If set to 'latest' it will pull latest available version in the selected region.",
    "default": "\"latest\"",
    "required": false
  },
  {
    "name": "enable_cost_allocation",
    "type": "bool",
    "description": "Enables Cost Allocation Feature and the cluster name and namespace of your GKE workloads appear in the labels field of the billing export to BigQuery",
    "default": "false",
    "required": false
  },
  {
    "name": "add_master_webhook_firewall_rules",
    "type": "bool",
    "description": "Create master_webhook firewall rules for ports defined in `firewall_inbound_ports`",
    "default": "false",
    "required": false
  },
  {
    "name": "initial_node_count",
    "type": "number",
    "description": "The number of nodes to create in this cluster's default node pool.",
    "default": "0",
    "required": false
  },
  {
    "name": "monitoring_enable_observability_metrics",
    "type": "bool",
    "description": "Whether or not the advanced datapath metrics are enabled.",
    "default": "false",
    "required": false
  },
  {
    "name": "ip_range_pods",
    "type": "string",
    "description": "The _name_ of the secondary subnet ip range to use for pods",
    "default": "",
    "required": true
  },
  {
    "name": "resource_usage_export_dataset_id",
    "type": "string",
    "description": "The ID of a BigQuery Dataset for using BigQuery as the destination of resource usage export.",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "node_pools_taints",
    "type": "map(list(object({ key = string, value = string, effect = string })))",
    "description": "Map of lists containing node taints by node-pool name",
    "default": "{\n  \"all\": [],\n  \"default-node-pool\": []\n}",
    "required": false
  },
  {
    "name": "service_account_name",
    "type": "string",
    "description": "The name of the service account that will be created if create_service_account is true. If you wish to use an existing service account, use service_account variable.",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "issue_client_certificate",
    "type": "bool",
    "description": "Issues a client certificate to authenticate to the cluster endpoint. To maximize the security of your cluster, leave this option disabled. Client certificates don't automatically rotate and aren't easily revocable. WARNING: changing this after cluster creation is destructive!",
    "default": "false",
    "required": false
  },
  {
    "name": "add_shadow_firewall_rules",
    "type": "bool",
    "description": "Create GKE shadow firewall (the same as default firewall rules with firewall logs enabled).",
    "default": "false",
    "required": false
  },
  {
    "name": "security_posture_mode",
    "type": "string",
    "description": "Security posture mode.  Accepted values are `DISABLED` and `BASIC`. Defaults to `DISABLED`.",
    "default": "\"DISABLED\"",
    "required": false
  },
  {
    "name": "maintenance_start_time",
    "type": "string",
    "description": "Time window specified for daily or recurring maintenance operations in RFC3339 format",
    "default": "\"05:00\"",
    "required": false
  },
  {
    "name": "non_masquerade_cidrs",
    "type": "list(string)",
    "description": "List of strings in CIDR notation that specify the IP address ranges that do not use IP masquerading.",
    "default": "[\n  \"10.0.0.0/8\",\n  \"172.16.0.0/12\",\n  \"192.168.0.0/16\"\n]",
    "required": false
  },
  {
    "name": "identity_namespace",
    "type": "string",
    "description": "The workload pool to attach all Kubernetes service accounts to. (Default value of `enabled` automatically sets project-based pool `[project_id].svc.id.goog`)",
    "default": "\"enabled\"",
    "required": false
  },
  {
    "name": "ip_masq_resync_interval",
    "type": "string",
    "description": "The interval at which the agent attempts to sync its ConfigMap file from the disk.",
    "default": "\"60s\"",
    "required": false
  },
  {
    "name": "authenticator_security_group",
    "type": "string",
    "description": "The name of the RBAC security group for use with Google security groups in Kubernetes RBAC. Group name must be in format gke-security-groups@yourdomain.com",
    "default": "null",
    "required": false
  },
  {
    "name": "logging_enabled_components",
    "type": "list(string)",
    "description": "List of services to monitor: SYSTEM_COMPONENTS, WORKLOADS. Empty list is default GKE configuration.",
    "default": "[]",
    "required": false
  },
  {
    "name": "service_account",
    "type": "string",
    "description": "The service account to run nodes as if not overridden in `node_pools`. The create_service_account variable default value (true) will cause a cluster-specific service account to be created. This service account should already exists and it will be used by the node pools. If you wish to only override the service account name, you can use service_account_name variable.",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "add_cluster_firewall_rules",
    "type": "bool",
    "description": "Create additional firewall rules",
    "default": "false",
    "required": false
  },
  {
    "name": "region",
    "type": "string",
    "description": "The region to host the cluster in (optional if zonal cluster / required if regional)",
    "default": "null",
    "required": false
  },
  {
    "name": "node_pools",
    "type": "list(map(any))",
    "description": "List of maps containing node pools",
    "default": "[\n  {\n    \"name\": \"default-node-pool\"\n  }\n]",
    "required": false
  },
  {
    "name": "node_pools_resource_labels",
    "type": "map(map(string))",
    "description": "Map of maps containing resource labels by node-pool name",
    "default": "{\n  \"all\": {},\n  \"default-node-pool\": {}\n}",
    "required": false
  },
  {
    "name": "enable_binary_authorization",
    "type": "bool",
    "description": "Enable BinAuthZ Admission controller",
    "default": "false",
    "required": false
  },
  {
    "name": "enable_shielded_nodes",
    "type": "bool",
    "description": "Enable Shielded Nodes features on all nodes in this cluster",
    "default": "true",
    "required": false
  },
  {
    "name": "description",
    "type": "string",
    "description": "The description of the cluster",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "service_external_ips",
    "type": "bool",
    "description": "Whether external ips specified by a service will be allowed in this cluster",
    "default": "false",
    "required": false
  },
  {
    "name": "network_policy_provider",
    "type": "string",
    "description": "The network policy provider.",
    "default": "\"CALICO\"",
    "required": false
  },
  {
    "name": "shadow_firewall_rules_priority",
    "type": "number",
    "description": "The firewall priority of GKE shadow firewall rules. The priority should be less than default firewall, which is 1000.",
    "default": "999",
    "required": false
  },
  {
    "name": "enable_vertical_pod_autoscaling",
    "type": "bool",
    "description": "Vertical Pod Autoscaling automatically adjusts the resources of pods controlled by it",
    "default": "false",
    "required": false
  },
  {
    "name": "node_pools_linux_node_configs_sysctls",
    "type": "map(map(string))",
    "description": "Map of maps containing linux node config sysctls by node-pool name",
    "default": "{\n  \"all\": {},\n  \"default-node-pool\": {}\n}",
    "required": false
  },
  {
    "name": "network_policy",
    "type": "bool",
    "description": "Enable network policy addon",
    "default": "false",
    "required": false
  },
  {
    "name": "cluster_dns_provider",
    "type": "string",
    "description": "Which in-cluster DNS provider should be used. PROVIDER_UNSPECIFIED (default) or PLATFORM_DEFAULT or CLOUD_DNS.",
    "default": "\"PROVIDER_UNSPECIFIED\"",
    "required": false
  },
  {
    "name": "cluster_ipv4_cidr",
    "type": "string",
    "description": "The IP address range of the kubernetes pods in this cluster. Default is an automatically assigned CIDR.",
    "default": "null",
    "required": false
  },
  {
    "name": "maintenance_recurrence",
    "type": "string",
    "description": "Frequency of the recurring maintenance window in RFC5545 format.",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "additional_ip_range_pods",
    "type": "list(string)",
    "description": "List of _names_ of the additional secondary subnet ip ranges to use for pods",
    "default": "[]",
    "required": false
  },
  {
    "name": "maintenance_exclusions",
    "type": "list(object({ name = string, start_time = string, end_time = string, exclusion_scope = string }))",
    "description": "List of maintenance exclusions. A cluster can have up to three",
    "default": "[]",
    "required": false
  },
  {
    "name": "cluster_dns_domain",
    "type": "string",
    "description": "The suffix used for all cluster service records.",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "ip_range_services",
    "type": "string",
    "description": "The _name_ of the secondary subnet range to use for services",
    "default": "",
    "required": true
  },
  {
    "name": "upstream_nameservers",
    "type": "list(string)",
    "description": "If specified, the values replace the nameservers taken by default from the node’s /etc/resolv.conf",
    "default": "[]",
    "required": false
  },
  {
    "name": "grant_registry_access",
    "type": "bool",
    "description": "Grants created cluster-specific service account storage.objectViewer and artifactregistry.reader roles.",
    "default": "false",
    "required": false
  },
  {
    "name": "disable_legacy_metadata_endpoints",
    "type": "bool",
    "description": "Disable the /0.1/ and /v1beta1/ metadata server endpoints on the node. Changing this value will cause all node pools to be recreated.",
    "default": "true",
    "required": false
  },
  {
    "name": "network",
    "type": "string",
    "description": "The VPC network to host the cluster in (required)",
    "default": "",
    "required": true
  },
  {
    "name": "node_pools_labels",
    "type": "map(map(string))",
    "description": "Map of maps containing node labels by node-pool name",
    "default": "{\n  \"all\": {},\n  \"default-node-pool\": {}\n}",
    "required": false
  },
  {
    "name": "enable_resource_consumption_export",
    "type": "bool",
    "description": "Whether to enable resource consumption metering on this cluster. When enabled, a table will be created in the resource export BigQuery dataset to store resource consumption data. The resulting table can be joined with the resource usage table or with BigQuery billing export.",
    "default": "true",
    "required": false
  },
  {
    "name": "maintenance_end_time",
    "type": "string",
    "description": "Time window specified for recurring maintenance operations in RFC3339 format",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "configure_ip_masq",
    "type": "bool",
    "description": "Enables the installation of ip masquerading, which is usually no longer required when using aliasied IP addresses. IP masquerading uses a kubectl call, so when you have a private cluster, you will need access to the API server.",
    "default": "false",
    "required": false
  },
  {
    "name": "security_posture_vulnerability_mode",
    "type": "string",
    "description": "Security posture vulnerability mode.  Accepted values are `VULNERABILITY_DISABLED`, `VULNERABILITY_BASIC`, and `VULNERABILITY_ENTERPRISE`. Defaults to `VULNERABILITY_DISABLED`.",
    "default": "\"VULNERABILITY_DISABLED\"",
    "required": false
  },
  {
    "name": "gke_backup_agent_config",
    "type": "bool",
    "description": "Whether Backup for GKE agent is enabled for this cluster.",
    "default": "false",
    "required": false
  },
  {
    "name": "subnetwork",
    "type": "string",
    "description": "The subnetwork to host the cluster in (required)",
    "default": "",
    "required": true
  },
  {
    "name": "cluster_autoscaling",
    "type": "object({\n    enabled                 = bool\n    autoscaling_profile     = string\n    min_cpu_cores           = number\n    max_cpu_cores           = number\n    min_memory_gb           = number\n    max_memory_gb           = number\n    gpu_resources           = list(object({ resource_type = string, minimum = number, maximum = number }))\n    auto_repair             = bool\n    auto_upgrade            = bool\n    disk_size               = optional(number)\n    disk_type               = optional(string)\n    image_type              = optional(string)\n    strategy                = optional(string)\n    max_surge               = optional(number)\n    max_unavailable         = optional(number)\n    node_pool_soak_duration = optional(string)\n    batch_soak_duration     = optional(string)\n    batch_percentage        = optional(number)\n    batch_node_count        = optional(number)\n  })",
    "description": "Cluster autoscaling configuration. See [more details](https://cloud.google.com/kubernetes-engine/docs/reference/rest/v1beta1/projects.locations.clusters#clusterautoscaling)",
    "default": "{\n  \"auto_repair\": true,\n  \"auto_upgrade\": true,\n  \"autoscaling_profile\": \"BALANCED\",\n  \"disk_size\": 100,\n  \"disk_type\": \"pd-standard\",\n  \"enabled\": false,\n  \"gpu_resources\": [],\n  \"image_type\": \"COS_CONTAINERD\",\n  \"max_cpu_cores\": 0,\n  \"max_memory_gb\": 0,\n  \"min_cpu_cores\": 0,\n  \"min_memory_gb\": 0\n}",
    "required": false
  },
  {
    "name": "node_pools_tags",
    "type": "map(list(string))",
    "description": "Map of lists containing node network tags by node-pool name",
    "default": "{\n  \"all\": [],\n  \"default-node-pool\": []\n}",
    "required": false
  },
  {
    "name": "logging_service",
    "type": "string",
    "description": "The logging service that the cluster should write logs to. Available options include logging.googleapis.com, logging.googleapis.com/kubernetes (beta), and none",
    "default": "\"logging.googleapis.com/kubernetes\"",
    "required": false
  },
  {
    "name": "network_tags",
    "type": "list(string)",
    "description": "(Optional) - List of network tags applied to auto-provisioned node pools.",
    "default": "[]",
    "required": false
  },
  {
    "name": "create_service_account",
    "type": "bool",
    "description": "Defines if service account specified to run nodes should be created.",
    "default": "true",
    "required": false
  },
  {
    "name": "enable_tpu",
    "type": "bool",
    "description": "Enable Cloud TPU resources in the cluster. WARNING: changing this after cluster creation is destructive!",
    "default": "false",
    "required": false
  },
  {
    "name": "monitoring_enable_managed_prometheus",
    "type": "bool",
    "description": "Configuration for Managed Service for Prometheus. Whether or not the managed collection is enabled.",
    "default": "false",
    "required": false
  },
  {
    "name": "disable_default_snat",
    "type": "bool",
    "description": "Whether to disable the default SNAT to support the private use of public IP addresses",
    "default": "false",
    "required": false
  },
  {
    "name": "zones",
    "type": "list(string)",
    "description": "The zones to host the cluster in (optional if regional cluster / required if zonal)",
    "default": "[]",
    "required": false
  },
  {
    "name": "node_pools_oauth_scopes",
    "type": "map(list(string))",
    "description": "Map of lists containing node oauth scopes by node-pool name",
    "default": "{\n  \"all\": [\n    \"https://www.googleapis.com/auth/cloud-platform\"\n  ],\n  \"default-node-pool\": []\n}",
    "required": false
  },
  {
    "name": "ip_masq_link_local",
    "type": "bool",
    "description": "Whether to masquerade traffic to the link-local prefix (169.254.0.0/16).",
    "default": "false",
    "required": false
  },
  {
    "name": "cluster_resource_labels",
    "type": "map(string)",
    "description": "The GCE resource labels (a map of key/value pairs) to be applied to the cluster",
    "default": "{}",
    "required": false
  },
  {
    "name": "datapath_provider",
    "type": "string",
    "description": "The desired datapath provider for this cluster. By default, `DATAPATH_PROVIDER_UNSPECIFIED` enables the IPTables-based kube-proxy implementation. `ADVANCED_DATAPATH` enables Dataplane-V2 feature.",
    "default": "\"DATAPATH_PROVIDER_UNSPECIFIED\"",
    "required": false
  },
  {
    "name": "fleet_project",
    "type": "string",
    "description": "(Optional) Register the cluster with the fleet in this project.",
    "default": "null",
    "required": false
  },
  {
    "name": "enable_network_egress_export",
    "type": "bool",
    "description": "Whether to enable network egress metering for this cluster. If enabled, a daemonset will be created in the cluster to meter network egress traffic.",
    "default": "false",
    "required": false
  },
  {
    "name": "firewall_priority",
    "type": "number",
    "description": "Priority rule for firewall rules",
    "default": "1000",
    "required": false
  },
  {
    "name": "windows_node_pools",
    "type": "list(map(string))",
    "description": "List of maps containing Windows node pools",
    "default": "[]",
    "required": false
  },
  {
    "name": "firewall_inbound_ports",
    "type": "list(string)",
    "description": "List of TCP ports for admission/webhook controllers. Either flag `add_master_webhook_firewall_rules` or `add_cluster_firewall_rules` (also adds egress rules) must be set to `true` for inbound-ports firewall rules to be applied.",
    "default": "[\n  \"8443\",\n  \"9443\",\n  \"15017\"\n]",
    "required": false
  }
]

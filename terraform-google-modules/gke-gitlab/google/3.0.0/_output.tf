
output "cluster_ca_certificate" {
  description = "CA Certificate for the GKE cluster that GitLab is deployed in."
  value       = module.gke-gitlab.cluster_ca_certificate
}

output "cluster_location" {
  description = "Location of the GKE cluster that GitLab is deployed in."
  value       = module.gke-gitlab.cluster_location
}

output "cluster_name" {
  description = "Name of the GKE cluster that GitLab is deployed in."
  value       = module.gke-gitlab.cluster_name
}

output "gitlab_address" {
  description = "IP address where you can connect to your GitLab instance"
  value       = module.gke-gitlab.gitlab_address
}

output "gitlab_url" {
  description = "URL where you can access your GitLab instance"
  value       = module.gke-gitlab.gitlab_url
}

output "host" {
  description = "Host for the GKE cluster that GitLab is deployed in."
  value       = module.gke-gitlab.host
}

output "root_password_instructions" {
  description = "Instructions for getting the root user's password for initial setup"
  value       = module.gke-gitlab.root_password_instructions
}

output "token" {
  description = "Token for the GKE cluster that GitLab is deployed in."
  value       = module.gke-gitlab.token
}


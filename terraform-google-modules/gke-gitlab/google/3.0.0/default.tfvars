
allow_force_destroy = false

certmanager_email = 

domain = ""

gitlab_address_name = ""

gitlab_db_name = "gitlab-db"

gitlab_db_password = ""

gitlab_db_random_prefix = false

gitlab_deletion_protection = true

gitlab_nodes_subnet_cidr = "10.0.0.0/16"

gitlab_pods_subnet_cidr = "10.3.0.0/16"

gitlab_runner_install = true

gitlab_services_subnet_cidr = "10.2.0.0/16"

gke_machine_type = "n1-standard-4"

gke_version = "1.21"

helm_chart_version = "4.2.4"

project_id = 

region = "us-central1"


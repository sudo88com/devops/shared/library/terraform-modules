
variable "consent_stores" {
  type        = any
  description = "Datastore that contain all information related to the configuration and operation of the Consent Management API (https://cloud.google.com/healthcare/docs/how-tos/consent-managing)."
  default     = []
}

variable "dicom_stores" {
  type        = any
  description = "Datastore that conforms to the DICOM (https://www.dicomstandard.org/about/) standard for Healthcare information exchange."
  default     = []
}

variable "fhir_stores" {
  type        = any
  description = "Datastore that conforms to the FHIR standard for Healthcare information exchange."
  default     = []
}

variable "hl7_v2_stores" {
  type        = any
  description = "Datastore that conforms to the HL7 V2 (https://www.hl7.org/hl7V2/STU3/) standard for Healthcare information exchange."
  default     = []
}

variable "iam_members" {
  type        = list(object({
    role   = string
    member = string
  }))
  description = "Updates the IAM policy to grant a role to a new member. Other members for the role for the dataset are preserved."
  default     = []
}

variable "location" {
  type        = string
  description = "The location for the Dataset."
  default     = ""
}

variable "name" {
  type        = string
  description = "The resource name for the Dataset."
  default     = ""
}

variable "project" {
  type        = string
  description = "The ID of the project in which the resource belongs."
  default     = ""
}

variable "time_zone" {
  type        = string
  description = "The default timezone used by this dataset."
  default     = null
}


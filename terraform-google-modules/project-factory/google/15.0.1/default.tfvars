
activate_api_identities = []

activate_apis = [
  "compute.googleapis.com"
]

auto_create_network = false

billing_account = 

bucket_force_destroy = false

bucket_labels = {}

bucket_location = "US"

bucket_name = ""

bucket_pap = "inherited"

bucket_project = ""

bucket_ula = true

bucket_versioning = false

budget_alert_pubsub_topic = null

budget_alert_spend_basis = "CURRENT_SPEND"

budget_alert_spent_percents = [
  0.5,
  0.7,
  1
]

budget_amount = null

budget_calendar_period = null

budget_custom_period_end_date = null

budget_custom_period_start_date = null

budget_display_name = null

budget_labels = {}

budget_monitoring_notification_channels = []

consumer_quotas = []

create_project_sa = true

default_network_tier = ""

default_service_account = "disable"

disable_dependent_services = true

disable_services_on_destroy = true

domain = ""

enable_shared_vpc_host_project = false

essential_contacts = {}

folder_id = ""

grant_network_role = true

grant_services_security_admin_role = false

group_name = ""

group_role = "roles/editor"

labels = {}

language_tag = "en-US"

lien = false

name = 

org_id = null

project_id = ""

project_sa_name = "project-service-account"

random_project_id = false

random_project_id_length = null

sa_role = ""

shared_vpc_subnets = []

svpc_host_project_id = ""

tag_binding_values = []

usage_bucket_name = ""

usage_bucket_prefix = ""

vpc_service_control_attach_dry_run = false

vpc_service_control_attach_enabled = false

vpc_service_control_perimeter_name = null

vpc_service_control_sleep_duration = "5s"


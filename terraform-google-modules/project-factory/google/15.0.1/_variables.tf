
variable "activate_api_identities" {
  type        = list(object({
    api   = string
    roles = list(string)
  }))
  description = "    The list of service identities (Google Managed service account for the API) to force-create for the project (e.g. in order to grant additional roles).\n    APIs in this list will automatically be appended to `activate_apis`.\n    Not including the API in this list will follow the default behaviour for identity creation (which is usually when the first resource using the API is created).\n    Any roles (e.g. service agent role) must be explicitly listed. See https://cloud.google.com/iam/docs/understanding-roles#service-agent-roles-roles for a list of related roles.\n"
  default     = []
}

variable "activate_apis" {
  type        = list(string)
  description = "The list of apis to activate within the project"
  default     = [
  "compute.googleapis.com"
]
}

variable "auto_create_network" {
  type        = bool
  description = "Create the default network"
  default     = false
}

variable "billing_account" {
  type        = string
  description = "The ID of the billing account to associate this project with"
  default     = ""
}

variable "bucket_force_destroy" {
  type        = bool
  description = "Force the deletion of all objects within the GCS bucket when deleting the bucket (optional)"
  default     = false
}

variable "bucket_labels" {
  type        = map(string)
  description = " A map of key/value label pairs to assign to the bucket (optional)"
  default     = {}
}

variable "bucket_location" {
  type        = string
  description = "The location for a GCS bucket to create (optional)"
  default     = "US"
}

variable "bucket_name" {
  type        = string
  description = "A name for a GCS bucket to create (in the bucket_project project), useful for Terraform state (optional)"
  default     = ""
}

variable "bucket_pap" {
  type        = string
  description = "Enable Public Access Prevention. Possible values are \"enforced\" or \"inherited\"."
  default     = "inherited"
}

variable "bucket_project" {
  type        = string
  description = "A project to create a GCS bucket (bucket_name) in, useful for Terraform state (optional)"
  default     = ""
}

variable "bucket_ula" {
  type        = bool
  description = "Enable Uniform Bucket Level Access"
  default     = true
}

variable "bucket_versioning" {
  type        = bool
  description = "Enable versioning for a GCS bucket to create (optional)"
  default     = false
}

variable "budget_alert_pubsub_topic" {
  type        = string
  description = "The name of the Cloud Pub/Sub topic where budget related messages will be published, in the form of `projects/{project_id}/topics/{topic_id}`"
  default     = null
}

variable "budget_alert_spend_basis" {
  type        = string
  description = "The type of basis used to determine if spend has passed the threshold"
  default     = "CURRENT_SPEND"
}

variable "budget_alert_spent_percents" {
  type        = list(number)
  description = "A list of percentages of the budget to alert on when threshold is exceeded"
  default     = [
  0.5,
  0.7,
  1
]
}

variable "budget_amount" {
  type        = number
  description = "The amount to use for a budget alert"
  default     = null
}

variable "budget_calendar_period" {
  type        = string
  description = "Specifies the calendar period for the budget. Possible values are MONTH, QUARTER, YEAR, CALENDAR_PERIOD_UNSPECIFIED, CUSTOM. custom_period_start_date and custom_period_end_date must be set if CUSTOM"
  default     = null
}

variable "budget_custom_period_end_date" {
  type        = string
  description = "Specifies the end date (DD-MM-YYYY) for the calendar_period CUSTOM"
  default     = null
}

variable "budget_custom_period_start_date" {
  type        = string
  description = "Specifies the start date (DD-MM-YYYY) for the calendar_period CUSTOM"
  default     = null
}

variable "budget_display_name" {
  type        = string
  description = "The display name of the budget. If not set defaults to `Budget For <projects[0]|All Projects>` "
  default     = null
}

variable "budget_labels" {
  type        = map(string)
  description = "A single label and value pair specifying that usage from only this set of labeled resources should be included in the budget."
  default     = {}
}

variable "budget_monitoring_notification_channels" {
  type        = list(string)
  description = "A list of monitoring notification channels in the form `[projects/{project_id}/notificationChannels/{channel_id}]`. A maximum of 5 channels are allowed."
  default     = []
}

variable "consumer_quotas" {
  type        = list(object({
    service    = string,
    metric     = string,
    dimensions = map(string),
    limit      = string,
    value      = string,
  }))
  description = "The quotas configuration you want to override for the project."
  default     = []
}

variable "create_project_sa" {
  type        = bool
  description = "Whether the default service account for the project shall be created"
  default     = true
}

variable "default_network_tier" {
  type        = string
  description = "Default Network Service Tier for resources created in this project. If unset, the value will not be modified. See https://cloud.google.com/network-tiers/docs/using-network-service-tiers and https://cloud.google.com/network-tiers."
  default     = ""
}

variable "default_service_account" {
  type        = string
  description = "Project default service account setting: can be one of `delete`, `deprivilege`, `disable`, or `keep`."
  default     = "disable"
}

variable "disable_dependent_services" {
  type        = bool
  description = "Whether services that are enabled and which depend on this service should also be disabled when this service is destroyed."
  default     = true
}

variable "disable_services_on_destroy" {
  type        = bool
  description = "Whether project services will be disabled when the resources are destroyed"
  default     = true
}

variable "domain" {
  type        = string
  description = "The domain name (optional)."
  default     = ""
}

variable "enable_shared_vpc_host_project" {
  type        = bool
  description = "If this project is a shared VPC host project. If true, you must *not* set svpc_host_project_id variable. Default is false."
  default     = false
}

variable "essential_contacts" {
  type        = map(list(string))
  description = "A mapping of users or groups to be assigned as Essential Contacts to the project, specifying a notification category"
  default     = {}
}

variable "folder_id" {
  type        = string
  description = "The ID of a folder to host this project"
  default     = ""
}

variable "grant_network_role" {
  type        = bool
  description = "Whether or not to grant networkUser role on the host project/subnets"
  default     = true
}

variable "grant_services_security_admin_role" {
  type        = bool
  description = "Whether or not to grant Kubernetes Engine Service Agent the Security Admin role on the host project so it can manage firewall rules"
  default     = false
}

variable "group_name" {
  type        = string
  description = "A group to control the project by being assigned group_role (defaults to project editor)"
  default     = ""
}

variable "group_role" {
  type        = string
  description = "The role to give the controlling group (group_name) over the project (defaults to project editor)"
  default     = "roles/editor"
}

variable "labels" {
  type        = map(string)
  description = "Map of labels for project"
  default     = {}
}

variable "language_tag" {
  type        = string
  description = "Language code to be used for essential contacts notifications"
  default     = "en-US"
}

variable "lien" {
  type        = bool
  description = "Add a lien on the project to prevent accidental deletion"
  default     = false
}

variable "name" {
  type        = string
  description = "The name for the project"
  default     = ""
}

variable "org_id" {
  type        = string
  description = "The organization ID."
  default     = null
}

variable "project_id" {
  type        = string
  description = "The ID to give the project. If not provided, the `name` will be used."
  default     = ""
}

variable "project_sa_name" {
  type        = string
  description = "Default service account name for the project."
  default     = "project-service-account"
}

variable "random_project_id" {
  type        = bool
  description = "Adds a suffix of 4 random characters to the `project_id`."
  default     = false
}

variable "random_project_id_length" {
  type        = number
  description = "Sets the length of `random_project_id` to the provided length, and uses a `random_string` for a larger collusion domain.  Recommended for use with CI."
  default     = null
}

variable "sa_role" {
  type        = string
  description = "A role to give the default Service Account for the project (defaults to none)"
  default     = ""
}

variable "shared_vpc_subnets" {
  type        = list(string)
  description = "List of subnets fully qualified subnet IDs (ie. projects/$project_id/regions/$region/subnetworks/$subnet_id)"
  default     = []
}

variable "svpc_host_project_id" {
  type        = string
  description = "The ID of the host project which hosts the shared VPC"
  default     = ""
}

variable "tag_binding_values" {
  type        = list(string)
  description = "Tag values to bind the project to."
  default     = []
}

variable "usage_bucket_name" {
  type        = string
  description = "Name of a GCS bucket to store GCE usage reports in (optional)"
  default     = ""
}

variable "usage_bucket_prefix" {
  type        = string
  description = "Prefix in the GCS bucket to store GCE usage reports in (optional)"
  default     = ""
}

variable "vpc_service_control_attach_dry_run" {
  type        = bool
  description = "Whether the project will be attached to a VPC Service Control Perimeter in Dry Run Mode. vpc_service_control_attach_enabled should be false for this to be true"
  default     = false
}

variable "vpc_service_control_attach_enabled" {
  type        = bool
  description = "Whether the project will be attached to a VPC Service Control Perimeter in ENFORCED MODE. vpc_service_control_attach_dry_run should be false for this to be true"
  default     = false
}

variable "vpc_service_control_perimeter_name" {
  type        = string
  description = "The name of a VPC Service Control Perimeter to add the created project to"
  default     = null
}

variable "vpc_service_control_sleep_duration" {
  type        = string
  description = "The duration to sleep in seconds before adding the project to a shared VPC after the project is added to the VPC Service Control Perimeter. VPC-SC is eventually consistent."
  default     = "5s"
}


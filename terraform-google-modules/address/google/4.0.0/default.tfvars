
address_type = "INTERNAL"

addresses = [
  ""
]

dns_domain = ""

dns_managed_zone = ""

dns_project = ""

dns_record_type = "A"

dns_reverse_zone = ""

dns_short_names = []

dns_ttl = 300

enable_cloud_dns = false

enable_reverse_dns = false

global = false

ip_version = "IPV4"

labels = {}

names = []

network_tier = "PREMIUM"

prefix_length = 16

project_id = 

purpose = "GCE_ENDPOINT"

region = 

subnetwork = ""


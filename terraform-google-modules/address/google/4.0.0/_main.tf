
module "address" {
  source = "terraform-aws-modules/address/aws"
  version = "4.0.0"
  address_type = var.address_type
  addresses = var.addresses
  dns_domain = var.dns_domain
  dns_managed_zone = var.dns_managed_zone
  dns_project = var.dns_project
  dns_record_type = var.dns_record_type
  dns_reverse_zone = var.dns_reverse_zone
  dns_short_names = var.dns_short_names
  dns_ttl = var.dns_ttl
  enable_cloud_dns = var.enable_cloud_dns
  enable_reverse_dns = var.enable_reverse_dns
  global = var.global
  ip_version = var.ip_version
  labels = var.labels
  names = var.names
  network_tier = var.network_tier
  prefix_length = var.prefix_length
  project_id = var.project_id
  purpose = var.purpose
  region = var.region
  subnetwork = var.subnetwork
}

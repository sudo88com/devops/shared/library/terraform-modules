
variable "address_type" {
  type        = string
  description = "The type of address to reserve, either \"INTERNAL\" or \"EXTERNAL\". If unspecified, defaults to \"INTERNAL\"."
  default     = "INTERNAL"
}

variable "addresses" {
  type        = list(string)
  description = "A list of IP addresses to create.  GCP will reserve unreserved addresses if given the value \"\".  If multiple names are given the default value is sufficient to have multiple addresses automatically picked for each name."
  default     = [
  ""
]
}

variable "dns_domain" {
  type        = string
  description = "The domain to append to DNS short names when registering in Cloud DNS."
  default     = ""
}

variable "dns_managed_zone" {
  type        = string
  description = "The name of the managed zone to create records within.  This managed zone must exist in the host project."
  default     = ""
}

variable "dns_project" {
  type        = string
  description = "The project where DNS A records will be configured."
  default     = ""
}

variable "dns_record_type" {
  type        = string
  description = "The type of records to create in the managed zone.  (e.g. \"A\")"
  default     = "A"
}

variable "dns_reverse_zone" {
  type        = string
  description = "The name of the managed zone to create PTR records within.  This managed zone must exist in the host project."
  default     = ""
}

variable "dns_short_names" {
  type        = list(string)
  description = "A list of DNS short names to register within Cloud DNS.  Names corresponding to addresses must align by their list index position in the two input variables, `names` and `dns_short_names`.  If an empty list, no domain names are registered.  Multiple names may be registered to the same address by passing a single element list to names and multiple elements to dns_short_names.  (e.g. [\"gusw1-dev-fooapp-fe-0001-a-001\"])"
  default     = []
}

variable "dns_ttl" {
  type        = number
  description = "The DNS TTL in seconds for records created in Cloud DNS.  The default value should be used unless the application demands special handling."
  default     = 300
}

variable "enable_cloud_dns" {
  type        = bool
  description = "If a value is set, register records in Cloud DNS."
  default     = false
}

variable "enable_reverse_dns" {
  type        = bool
  description = "If a value is set, register reverse DNS PTR records in Cloud DNS in the managed zone specified by dns_reverse_zone"
  default     = false
}

variable "global" {
  type        = bool
  description = "The scope in which the address should live. If set to true, the IP address will be globally scoped. Defaults to false, i.e. regionally scoped. When set to true, do not provide a subnetwork."
  default     = false
}

variable "ip_version" {
  type        = string
  description = "The IP Version that will be used by this address."
  default     = "IPV4"
}

variable "labels" {
  type        = map(string)
  description = "Labels to apply to this address."
  default     = {}
}

variable "names" {
  type        = list(string)
  description = "A list of IP address resource names to create.  This is the GCP resource name and not the associated hostname of the IP address.  Existing resource names may be found with `gcloud compute addresses list` (e.g. [\"gusw1-dev-fooapp-fe-0001-a-001-ip\"])"
  default     = []
}

variable "network_tier" {
  type        = string
  description = "The networking tier used for configuring this address."
  default     = "PREMIUM"
}

variable "prefix_length" {
  type        = number
  description = "The prefix length of the IP range."
  default     = 16
}

variable "project_id" {
  type        = string
  description = "The project ID to create the address in"
  default     = ""
}

variable "purpose" {
  type        = string
  description = "The purpose of the resource(GCE_ENDPOINT, SHARED_LOADBALANCER_VIP, VPC_PEERING)."
  default     = "GCE_ENDPOINT"
}

variable "region" {
  type        = string
  description = "The region to create the address in"
  default     = ""
}

variable "subnetwork" {
  type        = string
  description = "The subnet containing the address.  For EXTERNAL addresses use the empty string, \"\".  (e.g. \"projects/<project-name>/regions/<region-name>/subnetworks/<subnetwork-name>\")"
  default     = ""
}


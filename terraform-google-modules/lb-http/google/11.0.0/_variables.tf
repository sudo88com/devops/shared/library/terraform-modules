
variable "address" {
  type        = string
  description = "Existing IPv4 address to use (the actual IP address value)"
  default     = null
}

variable "backends" {
  type        = map(object({
    port                    = optional(number)
    project                 = optional(string)
    protocol                = optional(string)
    port_name               = optional(string)
    description             = optional(string)
    enable_cdn              = optional(bool)
    compression_mode        = optional(string)
    security_policy         = optional(string, null)
    edge_security_policy    = optional(string, null)
    custom_request_headers  = optional(list(string))
    custom_response_headers = optional(list(string))

    timeout_sec                     = optional(number)
    connection_draining_timeout_sec = optional(number)
    session_affinity                = optional(string)
    affinity_cookie_ttl_sec         = optional(number)
    locality_lb_policy              = optional(string)

    health_check = optional(object({
      host                = optional(string)
      request_path        = optional(string)
      request             = optional(string)
      response            = optional(string)
      port                = optional(number)
      port_name           = optional(string)
      proxy_header        = optional(string)
      port_specification  = optional(string)
      protocol            = optional(string)
      check_interval_sec  = optional(number)
      timeout_sec         = optional(number)
      healthy_threshold   = optional(number)
      unhealthy_threshold = optional(number)
      logging             = optional(bool)
    }))

    log_config = object({
      enable      = optional(bool)
      sample_rate = optional(number)
    })

    groups = list(object({
      group = string

      balancing_mode               = optional(string)
      capacity_scaler              = optional(number)
      description                  = optional(string)
      max_connections              = optional(number)
      max_connections_per_instance = optional(number)
      max_connections_per_endpoint = optional(number)
      max_rate                     = optional(number)
      max_rate_per_instance        = optional(number)
      max_rate_per_endpoint        = optional(number)
      max_utilization              = optional(number)
    }))
    iap_config = object({
      enable               = bool
      oauth2_client_id     = optional(string)
      oauth2_client_secret = optional(string)
    })
    cdn_policy = optional(object({
      cache_mode                   = optional(string)
      signed_url_cache_max_age_sec = optional(string)
      default_ttl                  = optional(number)
      max_ttl                      = optional(number)
      client_ttl                   = optional(number)
      negative_caching             = optional(bool)
      negative_caching_policy = optional(object({
        code = optional(number)
        ttl  = optional(number)
      }))
      serve_while_stale = optional(number)
      cache_key_policy = optional(object({
        include_host           = optional(bool)
        include_protocol       = optional(bool)
        include_query_string   = optional(bool)
        query_string_blacklist = optional(list(string))
        query_string_whitelist = optional(list(string))
        include_http_headers   = optional(list(string))
        include_named_cookies  = optional(list(string))
      }))
      bypass_cache_on_request_headers = optional(list(string))
    }))
    outlier_detection = optional(object({
      base_ejection_time = optional(object({
        seconds = number
        nanos   = optional(number)
      }))
      consecutive_errors                    = optional(number)
      consecutive_gateway_failure           = optional(number)
      enforcing_consecutive_errors          = optional(number)
      enforcing_consecutive_gateway_failure = optional(number)
      enforcing_success_rate                = optional(number)
      interval = optional(object({
        seconds = number
        nanos   = optional(number)
      }))
      max_ejection_percent        = optional(number)
      success_rate_minimum_hosts  = optional(number)
      success_rate_request_volume = optional(number)
      success_rate_stdev_factor   = optional(number)
    }))
  }))
  description = "Map backend indices to list of backend maps."
  default     = ""
}

variable "certificate" {
  type        = string
  description = "Content of the SSL certificate. Requires `ssl` to be set to `true` and `create_ssl_certificate` set to `true`"
  default     = null
}

variable "certificate_map" {
  type        = string
  description = "Certificate Map ID in format projects/{project}/locations/global/certificateMaps/{name}. Identifies a certificate map associated with the given target proxy.  Requires `ssl` to be set to `true`"
  default     = null
}

variable "create_address" {
  type        = bool
  description = "Create a new global IPv4 address"
  default     = true
}

variable "create_ipv6_address" {
  type        = bool
  description = "Allocate a new IPv6 address. Conflicts with \"ipv6_address\" - if both specified, \"create_ipv6_address\" takes precedence."
  default     = false
}

variable "create_ssl_certificate" {
  type        = bool
  description = "If `true`, Create certificate using `private_key/certificate`"
  default     = false
}

variable "create_url_map" {
  type        = bool
  description = "Set to `false` if url_map variable is provided."
  default     = true
}

variable "edge_security_policy" {
  type        = string
  description = "The resource URL for the edge security policy to associate with the backend service"
  default     = null
}

variable "enable_ipv6" {
  type        = bool
  description = "Enable IPv6 address on the CDN load-balancer"
  default     = false
}

variable "firewall_networks" {
  type        = list(string)
  description = "Names of the networks to create firewall rules in"
  default     = [
  "default"
]
}

variable "firewall_projects" {
  type        = list(string)
  description = "Names of the projects to create firewall rules in"
  default     = [
  "default"
]
}

variable "http_forward" {
  type        = bool
  description = "Set to `false` to disable HTTP port 80 forward"
  default     = true
}

variable "http_port" {
  type        = number
  description = "The port for the HTTP load balancer"
  default     = 80
}

variable "https_port" {
  type        = number
  description = "The port for the HTTPS load balancer"
  default     = 443
}

variable "https_redirect" {
  type        = bool
  description = "Set to `true` to enable https redirect on the lb."
  default     = false
}

variable "ipv6_address" {
  type        = string
  description = "An existing IPv6 address to use (the actual IP address value)"
  default     = null
}

variable "labels" {
  type        = map(string)
  description = "The labels to attach to resources created by this module"
  default     = {}
}

variable "load_balancing_scheme" {
  type        = string
  description = "Load balancing scheme type (EXTERNAL for classic external load balancer, EXTERNAL_MANAGED for Envoy-based load balancer, and INTERNAL_SELF_MANAGED for traffic director)"
  default     = "EXTERNAL"
}

variable "managed_ssl_certificate_domains" {
  type        = list(string)
  description = "Create Google-managed SSL certificates for specified domains. Requires `ssl` to be set to `true`"
  default     = []
}

variable "name" {
  type        = string
  description = "Name for the forwarding rule and prefix for supporting resources"
  default     = ""
}

variable "network" {
  type        = string
  description = "Network for INTERNAL_SELF_MANAGED load balancing scheme"
  default     = "default"
}

variable "private_key" {
  type        = string
  description = "Content of the private SSL key. Requires `ssl` to be set to `true` and `create_ssl_certificate` set to `true`"
  default     = null
}

variable "project" {
  type        = string
  description = "The project to deploy to, if not set the default provider project is used."
  default     = ""
}

variable "quic" {
  type        = bool
  description = "Specifies the QUIC override policy for this resource. Set true to enable HTTP/3 and Google QUIC support, false to disable both. Defaults to null which enables support for HTTP/3 only."
  default     = null
}

variable "random_certificate_suffix" {
  type        = bool
  description = "Bool to enable/disable random certificate name generation. Set and keep this to true if you need to change the SSL cert."
  default     = false
}

variable "security_policy" {
  type        = string
  description = "The resource URL for the security policy to associate with the backend service"
  default     = null
}

variable "server_tls_policy" {
  type        = string
  description = "The resource URL for the server TLS policy to associate with the https proxy service"
  default     = null
}

variable "ssl" {
  type        = bool
  description = "Set to `true` to enable SSL support. If `true` then at least one of these are required: 1) `ssl_certificates` OR 2) `create_ssl_certificate` set to `true` and `private_key/certificate` OR  3) `managed_ssl_certificate_domains`, OR 4) `certificate_map`"
  default     = false
}

variable "ssl_certificates" {
  type        = list(string)
  description = "SSL cert self_link list. Requires `ssl` to be set to `true`"
  default     = []
}

variable "ssl_policy" {
  type        = string
  description = "Selfink to SSL Policy"
  default     = null
}

variable "target_service_accounts" {
  type        = list(string)
  description = "List of target service accounts for health check firewall rule. Exactly one of target_tags or target_service_accounts should be specified."
  default     = []
}

variable "target_tags" {
  type        = list(string)
  description = "List of target tags for health check firewall rule. Exactly one of target_tags or target_service_accounts should be specified."
  default     = []
}

variable "url_map" {
  type        = string
  description = "The url_map resource to use. Default is to send all traffic to first backend."
  default     = null
}


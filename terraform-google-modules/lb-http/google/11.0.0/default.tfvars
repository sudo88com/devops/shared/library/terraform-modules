
address = null

backends = 

certificate = null

certificate_map = null

create_address = true

create_ipv6_address = false

create_ssl_certificate = false

create_url_map = true

edge_security_policy = null

enable_ipv6 = false

firewall_networks = [
  "default"
]

firewall_projects = [
  "default"
]

http_forward = true

http_port = 80

https_port = 443

https_redirect = false

ipv6_address = null

labels = {}

load_balancing_scheme = "EXTERNAL"

managed_ssl_certificate_domains = []

name = 

network = "default"

private_key = null

project = 

quic = null

random_certificate_suffix = false

security_policy = null

server_tls_policy = null

ssl = false

ssl_certificates = []

ssl_policy = null

target_service_accounts = []

target_tags = []

url_map = null


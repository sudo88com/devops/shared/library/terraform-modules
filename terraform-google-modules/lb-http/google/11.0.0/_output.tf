
output "backend_services" {
  description = "The backend service resources."
  value       = module.lb-http.backend_services
}

output "external_ip" {
  description = "The external IPv4 assigned to the global fowarding rule."
  value       = module.lb-http.external_ip
}

output "external_ipv6_address" {
  description = "The external IPv6 assigned to the global fowarding rule."
  value       = module.lb-http.external_ipv6_address
}

output "http_proxy" {
  description = "The HTTP proxy used by this module."
  value       = module.lb-http.http_proxy
}

output "https_proxy" {
  description = "The HTTPS proxy used by this module."
  value       = module.lb-http.https_proxy
}

output "ipv6_enabled" {
  description = "Whether IPv6 configuration is enabled on this load-balancer"
  value       = module.lb-http.ipv6_enabled
}

output "ssl_certificate_created" {
  description = "The SSL certificate create from key/pem"
  value       = module.lb-http.ssl_certificate_created
}

output "url_map" {
  description = "The default URL map used by this module."
  value       = module.lb-http.url_map
}


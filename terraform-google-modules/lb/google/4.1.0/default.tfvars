
allowed_ips = [
  "0.0.0.0/0"
]

disable_health_check = false

firewall_project = ""

health_check = {
  "check_interval_sec": null,
  "healthy_threshold": null,
  "host": null,
  "port": null,
  "request_path": null,
  "timeout_sec": null,
  "unhealthy_threshold": null
}

ip_address = null

ip_protocol = "TCP"

labels = {}

name = 

network = "default"

project = ""

region = 

service_port = 

session_affinity = "NONE"

target_service_accounts = null

target_tags = null


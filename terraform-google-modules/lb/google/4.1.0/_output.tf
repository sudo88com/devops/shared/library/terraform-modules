
output "external_ip" {
  description = "The external ip address of the forwarding rule."
  value       = module.lb.external_ip
}

output "target_pool" {
  description = "The `self_link` to the target pool resource created."
  value       = module.lb.target_pool
}


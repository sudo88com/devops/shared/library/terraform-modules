
variable "allowed_ips" {
  type        = list(string)
  description = "The IP address ranges which can access the load balancer."
  default     = [
  "0.0.0.0/0"
]
}

variable "disable_health_check" {
  type        = bool
  description = "Disables the health check on the target pool."
  default     = false
}

variable "firewall_project" {
  type        = string
  description = "Name of the project to create the firewall rule in. Useful for shared VPC. Default is var.project."
  default     = ""
}

variable "health_check" {
  type        = object({
    check_interval_sec  = number
    healthy_threshold   = number
    timeout_sec         = number
    unhealthy_threshold = number
    port                = number
    request_path        = string
    host                = string
  })
  description = "Health check to determine whether instances are responsive and able to do work"
  default     = {
  "check_interval_sec": null,
  "healthy_threshold": null,
  "host": null,
  "port": null,
  "request_path": null,
  "timeout_sec": null,
  "unhealthy_threshold": null
}
}

variable "ip_address" {
  type        = string
  description = "IP address of the external load balancer, if empty one will be assigned."
  default     = null
}

variable "ip_protocol" {
  type        = string
  description = "The IP protocol for the frontend forwarding rule and firewall rule. TCP, UDP, ESP, AH, SCTP or ICMP."
  default     = "TCP"
}

variable "labels" {
  type        = map(string)
  description = "The labels to attach to resources created by this module."
  default     = {}
}

variable "name" {
  type        = string
  description = "Name for the forwarding rule and prefix for supporting resources."
  default     = ""
}

variable "network" {
  type        = string
  description = "Name of the network to create resources in."
  default     = "default"
}

variable "project" {
  type        = string
  description = "The project to deploy to, if not set the default provider project is used."
  default     = ""
}

variable "region" {
  type        = string
  description = "Region used for GCP resources."
  default     = ""
}

variable "service_port" {
  type        = number
  description = "TCP port your service is listening on."
  default     = ""
}

variable "session_affinity" {
  type        = string
  description = "How to distribute load. Options are `NONE`, `CLIENT_IP` and `CLIENT_IP_PROTO`"
  default     = "NONE"
}

variable "target_service_accounts" {
  type        = list(string)
  description = "List of target service accounts to allow traffic using firewall rule."
  default     = null
}

variable "target_tags" {
  type        = list(string)
  description = "List of target tags to allow traffic using firewall rule."
  default     = null
}



output "container" {
  description = "The container definition provided"
  value       = module.container-vm.container
}

output "container_vm" {
  description = "The complete container VM image object to use for the GCE instance"
  value       = module.container-vm.container_vm
}

output "metadata_key" {
  description = "The key to assign `metadata_value` to, so container information is attached to the instance"
  value       = module.container-vm.metadata_key
}

output "metadata_value" {
  description = "The generated container configuration"
  value       = module.container-vm.metadata_value
}

output "restart_policy" {
  description = "The restart policy provided"
  value       = module.container-vm.restart_policy
}

output "source_image" {
  description = "The self_link to the COS image to use for the GCE instance. Equivalent to container_vm.self_link"
  value       = module.container-vm.source_image
}

output "vm_container_label" {
  description = "The COS version to deploy to the instance. To be used as the value for the `vm_container_label_key` label key. Equivalent to container_vm.name"
  value       = module.container-vm.vm_container_label
}

output "vm_container_label_key" {
  description = "The label key for the COS version deployed to the instance"
  value       = module.container-vm.vm_container_label_key
}

output "volumes" {
  description = "The volume definition provided"
  value       = module.container-vm.volumes
}


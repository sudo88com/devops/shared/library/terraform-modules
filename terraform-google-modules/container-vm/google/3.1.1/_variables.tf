
variable "container" {
  type        = any
  description = "A description of the container to deploy"
  default     = {
  "command": "ls",
  "image": "gcr.io/google-containers/busybox"
}
}

variable "cos_image_family" {
  type        = string
  description = "The COS image family to use (eg: stable, beta, or dev)"
  default     = "stable"
}

variable "cos_image_name" {
  type        = string
  description = "Name of a specific COS image to use instead of the latest cos family image"
  default     = null
}

variable "cos_project" {
  type        = string
  description = "COS project where the image is located"
  default     = "cos-cloud"
}

variable "restart_policy" {
  type        = string
  description = "The restart policy for a Docker container. Defaults to `OnFailure`"
  default     = "OnFailure"
}

variable "volumes" {
  type        = any
  description = "A set of Docker Volumes to configure"
  default     = []
}


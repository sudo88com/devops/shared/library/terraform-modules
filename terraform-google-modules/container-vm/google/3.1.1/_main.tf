
module "container-vm" {
  source = "terraform-aws-modules/container-vm/aws"
  version = "3.1.1"
  container = var.container
  cos_image_family = var.cos_image_family
  cos_image_name = var.cos_image_name
  cos_project = var.cos_project
  restart_policy = var.restart_policy
  volumes = var.volumes
}

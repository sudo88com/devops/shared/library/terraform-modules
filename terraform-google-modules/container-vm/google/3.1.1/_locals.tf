
locals {
  container = var.container
  cos_image_family = var.cos_image_family
  cos_image_name = var.cos_image_name
  cos_project = var.cos_project
  restart_policy = var.restart_policy
  volumes = var.volumes
}

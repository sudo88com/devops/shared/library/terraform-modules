
container = {
  "command": "ls",
  "image": "gcr.io/google-containers/busybox"
}

cos_image_family = "stable"

cos_image_name = null

cos_project = "cos-cloud"

restart_policy = "OnFailure"

volumes = []


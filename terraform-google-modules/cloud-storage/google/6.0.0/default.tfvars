
admins = []

autoclass = {}

bucket_admins = {}

bucket_creators = {}

bucket_hmac_key_admins = {}

bucket_lifecycle_rules = {}

bucket_policy_only = {}

bucket_storage_admins = {}

bucket_viewers = {}

cors = []

creators = []

custom_placement_config = {}

default_event_based_hold = {}

encryption_key_names = {}

folders = {}

force_destroy = {}

hmac_key_admins = []

hmac_service_accounts = {}

labels = {}

lifecycle_rules = []

location = "EU"

logging = {}

names = 

prefix = ""

project_id = 

public_access_prevention = "inherited"

randomize_suffix = false

retention_policy = {}

set_admin_roles = false

set_creator_roles = false

set_hmac_access = false

set_hmac_key_admin_roles = false

set_storage_admin_roles = false

set_viewer_roles = false

soft_delete_policy = {}

storage_admins = []

storage_class = "STANDARD"

versioning = {}

viewers = []

website = {}


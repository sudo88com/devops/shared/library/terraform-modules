
variable "admins" {
  type        = list(string)
  description = "IAM-style members who will be granted roles/storage.objectAdmin on all buckets."
  default     = []
}

variable "autoclass" {
  type        = map(bool)
  description = "Optional map of lowercase unprefixed bucket name => boolean, defaults to false."
  default     = {}
}

variable "bucket_admins" {
  type        = map(string)
  description = "Map of lowercase unprefixed name => comma-delimited IAM-style per-bucket admins."
  default     = {}
}

variable "bucket_creators" {
  type        = map(string)
  description = "Map of lowercase unprefixed name => comma-delimited IAM-style per-bucket creators."
  default     = {}
}

variable "bucket_hmac_key_admins" {
  type        = map(string)
  description = "Map of lowercase unprefixed name => comma-delimited IAM-style per-bucket HMAC Key admins."
  default     = {}
}

variable "bucket_lifecycle_rules" {
  type        = map(set(object({
    # Object with keys:
    # - type - The type of the action of this Lifecycle Rule. Supported values: Delete and SetStorageClass.
    # - storage_class - (Required if action type is SetStorageClass) The target Storage Class of objects affected by this Lifecycle Rule.
    action = map(string)

    # Object with keys:
    # - age - (Optional) Minimum age of an object in days to satisfy this condition.
    # - created_before - (Optional) Creation date of an object in RFC 3339 (e.g. 2017-06-13) to satisfy this condition.
    # - with_state - (Optional) Match to live and/or archived objects. Supported values include: "LIVE", "ARCHIVED", "ANY".
    # - matches_storage_class - (Optional) Comma delimited string for storage class of objects to satisfy this condition. Supported values include: MULTI_REGIONAL, REGIONAL, NEARLINE, COLDLINE, STANDARD, DURABLE_REDUCED_AVAILABILITY.
    # - num_newer_versions - (Optional) Relevant only for versioned objects. The number of newer versions of an object to satisfy this condition.
    # - custom_time_before - (Optional) A date in the RFC 3339 format YYYY-MM-DD. This condition is satisfied when the customTime metadata for the object is set to an earlier date than the date used in this lifecycle condition.
    # - days_since_custom_time - (Optional) The number of days from the Custom-Time metadata attribute after which this condition becomes true.
    # - days_since_noncurrent_time - (Optional) Relevant only for versioned objects. Number of days elapsed since the noncurrent timestamp of an object.
    # - noncurrent_time_before - (Optional) Relevant only for versioned objects. The date in RFC 3339 (e.g. 2017-06-13) when the object became nonconcurrent.
    condition = map(string)
  })))
  description = "Additional lifecycle_rules for specific buckets. Map of lowercase unprefixed name => list of lifecycle rules to configure."
  default     = {}
}

variable "bucket_policy_only" {
  type        = map(bool)
  description = "Disable ad-hoc ACLs on specified buckets. Defaults to true. Map of lowercase unprefixed name => boolean"
  default     = {}
}

variable "bucket_storage_admins" {
  type        = map(string)
  description = "Map of lowercase unprefixed name => comma-delimited IAM-style per-bucket storage admins."
  default     = {}
}

variable "bucket_viewers" {
  type        = map(string)
  description = "Map of lowercase unprefixed name => comma-delimited IAM-style per-bucket viewers."
  default     = {}
}

variable "cors" {
  type        = set(any)
  description = "Set of maps of mixed type attributes for CORS values. See appropriate attribute types here: https://www.terraform.io/docs/providers/google/r/storage_bucket.html#cors"
  default     = []
}

variable "creators" {
  type        = list(string)
  description = "IAM-style members who will be granted roles/storage.objectCreators on all buckets."
  default     = []
}

variable "custom_placement_config" {
  type        = any
  description = "Map of lowercase unprefixed name => custom placement config object. Format is the same as described in provider documentation https://www.terraform.io/docs/providers/google/r/storage_bucket#custom_placement_config"
  default     = {}
}

variable "default_event_based_hold" {
  type        = map(bool)
  description = "Enable event based hold to new objects added to specific bucket. Defaults to false. Map of lowercase unprefixed name => boolean"
  default     = {}
}

variable "encryption_key_names" {
  type        = map(string)
  description = "Optional map of lowercase unprefixed name => string, empty strings are ignored."
  default     = {}
}

variable "folders" {
  type        = map(list(string))
  description = "Map of lowercase unprefixed name => list of top level folder objects."
  default     = {}
}

variable "force_destroy" {
  type        = map(bool)
  description = "Optional map of lowercase unprefixed name => boolean, defaults to false."
  default     = {}
}

variable "hmac_key_admins" {
  type        = list(string)
  description = "IAM-style members who will be granted roles/storage.hmacKeyAdmin on all buckets."
  default     = []
}

variable "hmac_service_accounts" {
  type        = map(string)
  description = "List of HMAC service accounts to grant access to GCS."
  default     = {}
}

variable "labels" {
  type        = map(string)
  description = "Labels to be attached to the buckets"
  default     = {}
}

variable "lifecycle_rules" {
  type        = set(object({
    # Object with keys:
    # - type - The type of the action of this Lifecycle Rule. Supported values: Delete and SetStorageClass.
    # - storage_class - (Required if action type is SetStorageClass) The target Storage Class of objects affected by this Lifecycle Rule.
    action = map(string)

    # Object with keys:
    # - age - (Optional) Minimum age of an object in days to satisfy this condition.
    # - created_before - (Optional) Creation date of an object in RFC 3339 (e.g. 2017-06-13) to satisfy this condition.
    # - with_state - (Optional) Match to live and/or archived objects. Supported values include: "LIVE", "ARCHIVED", "ANY".
    # - matches_storage_class - (Optional) Comma delimited string for storage class of objects to satisfy this condition. Supported values include: MULTI_REGIONAL, REGIONAL, NEARLINE, COLDLINE, STANDARD, DURABLE_REDUCED_AVAILABILITY.
    # - matches_prefix - (Optional) One or more matching name prefixes to satisfy this condition.
    # - matches_suffix - (Optional) One or more matching name suffixes to satisfy this condition.
    # - num_newer_versions - (Optional) Relevant only for versioned objects. The number of newer versions of an object to satisfy this condition.
    # - custom_time_before - (Optional) A date in the RFC 3339 format YYYY-MM-DD. This condition is satisfied when the customTime metadata for the object is set to an earlier date than the date used in this lifecycle condition.
    # - days_since_custom_time - (Optional) The number of days from the Custom-Time metadata attribute after which this condition becomes true.
    # - days_since_noncurrent_time - (Optional) Relevant only for versioned objects. Number of days elapsed since the noncurrent timestamp of an object.
    # - noncurrent_time_before - (Optional) Relevant only for versioned objects. The date in RFC 3339 (e.g. 2017-06-13) when the object became nonconcurrent.
    condition = map(string)
  }))
  description = "List of lifecycle rules to configure. Format is the same as described in provider documentation https://www.terraform.io/docs/providers/google/r/storage_bucket.html#lifecycle_rule except condition.matches_storage_class should be a comma delimited string."
  default     = []
}

variable "location" {
  type        = string
  description = "Bucket location."
  default     = "EU"
}

variable "logging" {
  type        = any
  description = "Map of lowercase unprefixed name => bucket logging config object. Format is the same as described in provider documentation https://www.terraform.io/docs/providers/google/r/storage_bucket.html#logging"
  default     = {}
}

variable "names" {
  type        = list(string)
  description = "Bucket name suffixes."
  default     = ""
}

variable "prefix" {
  type        = string
  description = "Prefix used to generate the bucket name."
  default     = ""
}

variable "project_id" {
  type        = string
  description = "Bucket project id."
  default     = ""
}

variable "public_access_prevention" {
  type        = string
  description = "Prevents public access to a bucket. Acceptable values are inherited or enforced. If inherited, the bucket uses public access prevention, only if the bucket is subject to the public access prevention organization policy constraint."
  default     = "inherited"
}

variable "randomize_suffix" {
  type        = bool
  description = "Adds an identical, but randomized 4-character suffix to all bucket names"
  default     = false
}

variable "retention_policy" {
  type        = any
  description = "Map of retention policy values. Format is the same as described in provider documentation https://www.terraform.io/docs/providers/google/r/storage_bucket#retention_policy"
  default     = {}
}

variable "set_admin_roles" {
  type        = bool
  description = "Grant roles/storage.objectAdmin role to admins and bucket_admins."
  default     = false
}

variable "set_creator_roles" {
  type        = bool
  description = "Grant roles/storage.objectCreator role to creators and bucket_creators."
  default     = false
}

variable "set_hmac_access" {
  type        = bool
  description = "Set S3 compatible access to GCS."
  default     = false
}

variable "set_hmac_key_admin_roles" {
  type        = bool
  description = "Grant roles/storage.hmacKeyAdmin role to hmac_key_admins and bucket_hmac_key_admins."
  default     = false
}

variable "set_storage_admin_roles" {
  type        = bool
  description = "Grant roles/storage.admin role to storage_admins and bucket_storage_admins."
  default     = false
}

variable "set_viewer_roles" {
  type        = bool
  description = "Grant roles/storage.objectViewer role to viewers and bucket_viewers."
  default     = false
}

variable "soft_delete_policy" {
  type        = map(any)
  description = "Soft delete policies to apply. Map of lowercase unprefixed name => soft delete policy. Format is the same as described in provider documentation https://www.terraform.io/docs/providers/google/r/storage_bucket.html#nested_soft_delete_policy"
  default     = {}
}

variable "storage_admins" {
  type        = list(string)
  description = "IAM-style members who will be granted roles/storage.admin on all buckets."
  default     = []
}

variable "storage_class" {
  type        = string
  description = "Bucket storage class."
  default     = "STANDARD"
}

variable "versioning" {
  type        = map(bool)
  description = "Optional map of lowercase unprefixed name => boolean, defaults to false."
  default     = {}
}

variable "viewers" {
  type        = list(string)
  description = "IAM-style members who will be granted roles/storage.objectViewer on all buckets."
  default     = []
}

variable "website" {
  type        = map(any)
  description = "Map of website values. Supported attributes: main_page_suffix, not_found_page"
  default     = {}
}



output "bucket" {
  description = "Bucket resource (for single use)."
  value       = module.cloud-storage.bucket
}

output "buckets" {
  description = "Bucket resources as list."
  value       = module.cloud-storage.buckets
}

output "buckets_map" {
  description = "Bucket resources by name."
  value       = module.cloud-storage.buckets_map
}

output "hmac_keys" {
  description = "List of HMAC keys."
  value       = module.cloud-storage.hmac_keys
}

output "name" {
  description = "Bucket name (for single use)."
  value       = module.cloud-storage.name
}

output "names" {
  description = "Bucket names."
  value       = module.cloud-storage.names
}

output "names_list" {
  description = "List of bucket names."
  value       = module.cloud-storage.names_list
}

output "url" {
  description = "Bucket URL (for single use)."
  value       = module.cloud-storage.url
}

output "urls" {
  description = "Bucket URLs."
  value       = module.cloud-storage.urls
}

output "urls_list" {
  description = "List of bucket URLs."
  value       = module.cloud-storage.urls_list
}


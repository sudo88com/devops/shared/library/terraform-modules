
output "id" {
  description = "ID of the group. For Google-managed entities, the ID is the email address the group"
  value       = module.group.id
}

output "name" {
  description = "Name of the group with the domain removed. For Google-managed entities, the ID is the email address the group"
  value       = module.group.name
}

output "resource_name" {
  description = "Resource name of the group in the format: groups/{group_id}, where group_id is the unique ID assigned to the group."
  value       = module.group.resource_name
}


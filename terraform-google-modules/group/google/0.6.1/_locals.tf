
locals {
  customer_id = var.customer_id
  description = var.description
  display_name = var.display_name
  domain = var.domain
  id = var.id
  initial_group_config = var.initial_group_config
  managers = var.managers
  members = var.members
  owners = var.owners
  types = var.types
}

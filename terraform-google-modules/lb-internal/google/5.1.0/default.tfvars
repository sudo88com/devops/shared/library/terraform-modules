
all_ports = null

backends = 

connection_draining_timeout_sec = null

create_backend_firewall = true

create_health_check_firewall = true

firewall_enable_logging = false

global_access = false

health_check = 

ip_address = null

ip_protocol = "TCP"

labels = {}

name = 

network = "default"

network_project = ""

ports = 

project = ""

region = "us-central1"

service_label = null

session_affinity = "NONE"

source_ip_ranges = null

source_service_accounts = null

source_tags = 

subnetwork = "default"

target_service_accounts = null

target_tags = 



variable "all_ports" {
  type        = bool
  description = "Boolean for all_ports setting on forwarding rule."
  default     = null
}

variable "backends" {
  type        = list(any)
  description = "List of backends, should be a map of key-value pairs for each backend, must have the 'group' key."
  default     = ""
}

variable "connection_draining_timeout_sec" {
  type        = number
  description = "Time for which instance will be drained"
  default     = null
}

variable "create_backend_firewall" {
  type        = bool
  description = "Controls if firewall rules for the backends will be created or not. Health-check firewall rules are controlled separately."
  default     = true
}

variable "create_health_check_firewall" {
  type        = bool
  description = "Controls if firewall rules for the health check will be created or not. If this rule is not present backend healthcheck will fail."
  default     = true
}

variable "firewall_enable_logging" {
  type        = bool
  description = "Controls if firewall rules that are created are to have logging configured. This will be ignored for firewall rules that are not created."
  default     = false
}

variable "global_access" {
  type        = bool
  description = "Allow all regions on the same VPC network access."
  default     = false
}

variable "health_check" {
  type        = object({
    type                = string
    check_interval_sec  = number
    healthy_threshold   = number
    timeout_sec         = number
    unhealthy_threshold = number
    response            = string
    proxy_header        = string
    port                = number
    port_name           = string
    request             = string
    request_path        = string
    host                = string
    enable_log          = bool
  })
  description = "Health check to determine whether instances are responsive and able to do work"
  default     = ""
}

variable "ip_address" {
  type        = string
  description = "IP address of the internal load balancer, if empty one will be assigned. Default is empty."
  default     = null
}

variable "ip_protocol" {
  type        = string
  description = "The IP protocol for the backend and frontend forwarding rule. TCP or UDP."
  default     = "TCP"
}

variable "labels" {
  type        = map(string)
  description = "The labels to attach to resources created by this module."
  default     = {}
}

variable "name" {
  type        = string
  description = "Name for the forwarding rule and prefix for supporting resources."
  default     = ""
}

variable "network" {
  type        = string
  description = "Name of the network to create resources in."
  default     = "default"
}

variable "network_project" {
  type        = string
  description = "Name of the project for the network. Useful for shared VPC. Default is var.project."
  default     = ""
}

variable "ports" {
  type        = list(string)
  description = "List of ports range to forward to backend services. Max is 5."
  default     = ""
}

variable "project" {
  type        = string
  description = "The project to deploy to, if not set the default provider project is used."
  default     = ""
}

variable "region" {
  type        = string
  description = "Region for cloud resources."
  default     = "us-central1"
}

variable "service_label" {
  type        = string
  description = "Service label is used to create internal DNS name"
  default     = null
}

variable "session_affinity" {
  type        = string
  description = "The session affinity for the backends example: NONE, CLIENT_IP. Default is `NONE`."
  default     = "NONE"
}

variable "source_ip_ranges" {
  type        = list(string)
  description = "List of source ip ranges for traffic between the internal load balancer."
  default     = null
}

variable "source_service_accounts" {
  type        = list(string)
  description = "List of source service accounts for traffic between the internal load balancer."
  default     = null
}

variable "source_tags" {
  type        = list(string)
  description = "List of source tags for traffic between the internal load balancer."
  default     = ""
}

variable "subnetwork" {
  type        = string
  description = "Name of the subnetwork to create resources in."
  default     = "default"
}

variable "target_service_accounts" {
  type        = list(string)
  description = "List of target service accounts for traffic between the internal load balancer."
  default     = null
}

variable "target_tags" {
  type        = list(string)
  description = "List of target tags for traffic between the internal load balancer."
  default     = ""
}


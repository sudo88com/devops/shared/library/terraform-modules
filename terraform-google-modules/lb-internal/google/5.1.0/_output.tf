
output "forwarding_rule" {
  description = "The forwarding rule self_link."
  value       = module.lb-internal.forwarding_rule
}

output "forwarding_rule_id" {
  description = "The forwarding rule id."
  value       = module.lb-internal.forwarding_rule_id
}

output "ip_address" {
  description = "The internal IP assigned to the regional forwarding rule."
  value       = module.lb-internal.ip_address
}


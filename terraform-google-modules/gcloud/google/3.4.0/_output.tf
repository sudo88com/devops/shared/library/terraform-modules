
output "bin_dir" {
  description = "The full bin path of the modules executables"
  value       = module.gcloud.bin_dir
}

output "create_cmd_bin" {
  description = "The full bin path & command used on create"
  value       = module.gcloud.create_cmd_bin
}

output "destroy_cmd_bin" {
  description = "The full bin path & command used on destroy"
  value       = module.gcloud.destroy_cmd_bin
}

output "downloaded" {
  description = "Whether gcloud was downloaded or not"
  value       = module.gcloud.downloaded
}

output "wait" {
  description = "An output to use when you want to depend on cmd finishing"
  value       = module.gcloud.wait
}


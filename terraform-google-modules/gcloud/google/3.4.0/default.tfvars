
activate_service_account = true

additional_components = []

create_cmd_body = "info"

create_cmd_entrypoint = "gcloud"

create_cmd_triggers = {}

destroy_cmd_body = "info"

destroy_cmd_entrypoint = "gcloud"

enabled = true

gcloud_download_url = ""

gcloud_sdk_version = "434.0.0"

jq_download_url = ""

jq_version = "1.6"

module_depends_on = []

platform = "linux"

service_account_key_file = ""

skip_download = true

upgrade = true

use_tf_google_credentials_env_var = false



module "vpc-service-controls" {
  source = "terraform-aws-modules/vpc-service-controls/aws"
  version = "6.0.0"
  parent_id = var.parent_id
  policy_name = var.policy_name
}

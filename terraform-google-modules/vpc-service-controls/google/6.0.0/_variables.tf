
variable "parent_id" {
  type        = string
  description = "The parent of this AccessPolicy in the Cloud Resource Hierarchy. As of now, only organization are accepted as parent."
  default     = ""
}

variable "policy_name" {
  type        = string
  description = "The policy's name."
  default     = ""
}


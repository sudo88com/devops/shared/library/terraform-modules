
output "policy_id" {
  description = "Resource name of the AccessPolicy."
  value       = module.vpc-service-controls.policy_id
}

output "policy_name" {
  description = "The policy's name."
  value       = module.vpc-service-controls.policy_name
}



variable "default_key_specs_key" {
  type        = any
  description = "Object containing default key signing specifications : algorithm, key_length, key_type, kind. Please see https://www.terraform.io/docs/providers/google/r/dns_managed_zone#dnssec_config for futhers details"
  default     = {}
}

variable "default_key_specs_zone" {
  type        = any
  description = "Object containing default zone signing specifications : algorithm, key_length, key_type, kind. Please see https://www.terraform.io/docs/providers/google/r/dns_managed_zone#dnssec_config for futhers details"
  default     = {}
}

variable "description" {
  type        = string
  description = "zone description (shown in console)"
  default     = "Managed by Terraform"
}

variable "dnssec_config" {
  type        = any
  description = "Object containing : kind, non_existence, state. Please see https://www.terraform.io/docs/providers/google/r/dns_managed_zone#dnssec_config for futhers details"
  default     = {}
}

variable "domain" {
  type        = string
  description = "Zone domain, must end with a period."
  default     = ""
}

variable "enable_logging" {
  type        = bool
  description = "Enable query logging for this ManagedZone"
  default     = false
}

variable "force_destroy" {
  type        = bool
  description = "Set this true to delete all records in the zone."
  default     = false
}

variable "labels" {
  type        = map(any)
  description = "A set of key/value label pairs to assign to this ManagedZone"
  default     = {}
}

variable "name" {
  type        = string
  description = "Zone name, must be unique within the project."
  default     = ""
}

variable "private_visibility_config_networks" {
  type        = list(string)
  description = "List of VPC self links that can see this zone."
  default     = []
}

variable "project_id" {
  type        = string
  description = "Project id for the zone."
  default     = ""
}

variable "recordsets" {
  type        = list(object({
    name    = string
    type    = string
    ttl     = number
    records = optional(list(string), null)

    routing_policy = optional(object({
      wrr = optional(list(object({
        weight  = number
        records = list(string)
      })), [])
      geo = optional(list(object({
        location = string
        records  = list(string)
      })), [])
    }))
  }))
  description = "List of DNS record objects to manage, in the standard terraform dns structure."
  default     = []
}

variable "service_namespace_url" {
  type        = string
  description = "The fully qualified or partial URL of the service directory namespace that should be associated with the zone. This should be formatted like https://servicedirectory.googleapis.com/v1/projects/{project}/locations/{location}/namespaces/{namespace_id} or simply projects/{project}/locations/{location}/namespaces/{namespace_id}."
  default     = ""
}

variable "target_name_server_addresses" {
  type        = list(map(any))
  description = "List of target name servers for forwarding zone."
  default     = []
}

variable "target_network" {
  type        = string
  description = "Peering network."
  default     = ""
}

variable "type" {
  type        = string
  description = "Type of zone to create, valid values are 'public', 'private', 'forwarding', 'peering', 'reverse_lookup' and 'service_directory'."
  default     = "private"
}


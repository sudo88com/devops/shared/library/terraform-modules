
output "network" {
  description = "The created network"
  value       = module.network.network
}

output "network_id" {
  description = "The ID of the VPC being created"
  value       = module.network.network_id
}

output "network_name" {
  description = "The name of the VPC being created"
  value       = module.network.network_name
}

output "network_self_link" {
  description = "The URI of the VPC being created"
  value       = module.network.network_self_link
}

output "project_id" {
  description = "VPC project id"
  value       = module.network.project_id
}

output "route_names" {
  description = "The route names associated with this VPC"
  value       = module.network.route_names
}

output "subnets" {
  description = "A map with keys of form subnet_region/subnet_name and values being the outputs of the google_compute_subnetwork resources used to create corresponding subnets."
  value       = module.network.subnets
}

output "subnets_flow_logs" {
  description = "Whether the subnets will have VPC flow logs enabled"
  value       = module.network.subnets_flow_logs
}

output "subnets_ids" {
  description = "The IDs of the subnets being created"
  value       = module.network.subnets_ids
}

output "subnets_ips" {
  description = "The IPs and CIDRs of the subnets being created"
  value       = module.network.subnets_ips
}

output "subnets_names" {
  description = "The names of the subnets being created"
  value       = module.network.subnets_names
}

output "subnets_private_access" {
  description = "Whether the subnets will have access to Google API's without a public IP"
  value       = module.network.subnets_private_access
}

output "subnets_regions" {
  description = "The region where the subnets will be created"
  value       = module.network.subnets_regions
}

output "subnets_secondary_ranges" {
  description = "The secondary ranges associated with these subnets"
  value       = module.network.subnets_secondary_ranges
}

output "subnets_self_links" {
  description = "The self-links of subnets being created"
  value       = module.network.subnets_self_links
}


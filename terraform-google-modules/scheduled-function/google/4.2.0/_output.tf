
output "name" {
  description = "The name of the job created"
  value       = module.scheduled-function.name
}

output "pubsub_topic_name" {
  description = "PubSub topic name"
  value       = module.scheduled-function.pubsub_topic_name
}

output "scheduler_job" {
  description = "The Cloud Scheduler job instance"
  value       = module.scheduled-function.scheduler_job
}


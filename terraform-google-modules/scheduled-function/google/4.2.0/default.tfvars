
bucket_force_destroy = true

bucket_name = ""

create_bucket = true

function_available_memory_mb = 256

function_description = "Processes log export events provided through a Pub/Sub topic subscription."

function_docker_registry = null

function_docker_repository = null

function_entry_point = 

function_environment_variables = {}

function_event_trigger_failure_policy_retry = false

function_kms_key_name = null

function_labels = {}

function_max_instances = null

function_name = 

function_runtime = "nodejs10"

function_secret_environment_variables = []

function_service_account_email = ""

function_source_archive_bucket_labels = {}

function_source_dependent_files = []

function_source_directory = 

function_timeout_s = 60

grant_token_creator = false

ingress_settings = null

job_description = ""

job_name = null

job_schedule = "*/2 * * * *"

message_data = "dGVzdA=="

project_id = 

region = 

scheduler_job = null

time_zone = "Etc/UTC"

topic_kms_key_name = null

topic_labels = {}

topic_name = "test-topic"

vpc_connector = null

vpc_connector_egress_settings = null


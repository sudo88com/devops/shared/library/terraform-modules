
variable "bucket_force_destroy" {
  type        = bool
  description = "When deleting the GCS bucket containing the cloud function, delete all objects in the bucket first."
  default     = true
}

variable "bucket_name" {
  type        = string
  description = "The name to apply to the bucket. Will default to a string of <project-id>-scheduled-function-XXXX> with XXXX being random characters."
  default     = ""
}

variable "create_bucket" {
  type        = bool
  description = "Create bucket (default). Set to `false` to use existing one"
  default     = true
}

variable "function_available_memory_mb" {
  type        = number
  description = "The amount of memory in megabytes allotted for the function to use."
  default     = 256
}

variable "function_description" {
  type        = string
  description = "The description of the function."
  default     = "Processes log export events provided through a Pub/Sub topic subscription."
}

variable "function_docker_registry" {
  type        = string
  description = "Docker Registry to use for storing the function's Docker images. Allowed values are CONTAINER_REGISTRY (default) and ARTIFACT_REGISTRY."
  default     = null
}

variable "function_docker_repository" {
  type        = string
  description = "User managed repository created in Artifact Registry optionally with a customer managed encryption key. If specified, deployments will use Artifact Registry."
  default     = null
}

variable "function_entry_point" {
  type        = string
  description = "The name of a method in the function source which will be invoked when the function is executed."
  default     = ""
}

variable "function_environment_variables" {
  type        = map(string)
  description = "A set of key/value environment variable pairs to assign to the function."
  default     = {}
}

variable "function_event_trigger_failure_policy_retry" {
  type        = bool
  description = "A toggle to determine if the function should be retried on failure."
  default     = false
}

variable "function_kms_key_name" {
  type        = string
  description = "Resource name of a KMS crypto key (managed by the user) used to encrypt/decrypt function resources."
  default     = null
}

variable "function_labels" {
  type        = map(string)
  description = "A set of key/value label pairs to assign to the function."
  default     = {}
}

variable "function_max_instances" {
  type        = number
  description = "The maximum number of parallel executions of the function."
  default     = null
}

variable "function_name" {
  type        = string
  description = "The name to apply to the function"
  default     = ""
}

variable "function_runtime" {
  type        = string
  description = "The runtime in which the function will be executed."
  default     = "nodejs10"
}

variable "function_secret_environment_variables" {
  type        = list(map(string))
  description = "A list of maps which contains key, project_id, secret_name (not the full secret id) and version to assign to the function as a set of secret environment variables."
  default     = []
}

variable "function_service_account_email" {
  type        = string
  description = "The service account to run the function as."
  default     = ""
}

variable "function_source_archive_bucket_labels" {
  type        = map(string)
  description = "A set of key/value label pairs to assign to the function source archive bucket."
  default     = {}
}

variable "function_source_dependent_files" {
  type        = list(object({
    filename = string
    id       = string
  }))
  description = "A list of any terraform created `local_file`s that the module will wait for before creating the archive."
  default     = []
}

variable "function_source_directory" {
  type        = string
  description = "The contents of this directory will be archived and used as the function source."
  default     = ""
}

variable "function_timeout_s" {
  type        = number
  description = "The amount of time in seconds allotted for the execution of the function."
  default     = 60
}

variable "grant_token_creator" {
  type        = bool
  description = "Specify true if you want to add token creator role to the default Pub/Sub SA"
  default     = false
}

variable "ingress_settings" {
  type        = string
  description = "The ingress settings for the function. Allowed values are ALLOW_ALL, ALLOW_INTERNAL_AND_GCLB and ALLOW_INTERNAL_ONLY. Changes to this field will recreate the cloud function."
  default     = null
}

variable "job_description" {
  type        = string
  description = "Addition text to describe the job"
  default     = ""
}

variable "job_name" {
  type        = string
  description = "The name of the scheduled job to run"
  default     = null
}

variable "job_schedule" {
  type        = string
  description = "The job frequency, in cron syntax"
  default     = "*/2 * * * *"
}

variable "message_data" {
  type        = string
  description = "The data to send in the topic message."
  default     = "dGVzdA=="
}

variable "project_id" {
  type        = string
  description = "The ID of the project where the resources will be created"
  default     = ""
}

variable "region" {
  type        = string
  description = "The region in which resources will be applied."
  default     = ""
}

variable "scheduler_job" {
  type        = object({ name = string })
  description = "An existing Cloud Scheduler job instance"
  default     = null
}

variable "time_zone" {
  type        = string
  description = "The timezone to use in scheduler"
  default     = "Etc/UTC"
}

variable "topic_kms_key_name" {
  type        = string
  description = "The resource name of the Cloud KMS CryptoKey to be used to protect access to messages published on this topic."
  default     = null
}

variable "topic_labels" {
  type        = map(string)
  description = "A set of key/value label pairs to assign to the pubsub topic."
  default     = {}
}

variable "topic_name" {
  type        = string
  description = "Name of pubsub topic connecting the scheduled job and the function"
  default     = "test-topic"
}

variable "vpc_connector" {
  type        = string
  description = "The VPC Network Connector that this cloud function can connect to. It should be set up as fully-qualified URI. The format of this field is projects//locations//connectors/*."
  default     = null
}

variable "vpc_connector_egress_settings" {
  type        = string
  description = "The egress settings for the connector, controlling what traffic is diverted through it. Allowed values are ALL_TRAFFIC and PRIVATE_RANGES_ONLY. If unset, this field preserves the previously set value."
  default     = null
}


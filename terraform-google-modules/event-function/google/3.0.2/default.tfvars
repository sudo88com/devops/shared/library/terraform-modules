
available_memory_mb = 256

bucket_force_destroy = false

bucket_labels = {}

bucket_name = ""

build_environment_variables = {}

create_bucket = true

description = "Processes events."

docker_registry = null

docker_repository = null

entry_point = 

environment_variables = {}

event_trigger = {}

event_trigger_failure_policy_retry = false

files_to_exclude_in_source_dir = []

ingress_settings = "ALLOW_ALL"

kms_key_name = null

labels = {}

log_bucket = null

log_object_prefix = null

max_instances = 0

name = 

project_id = 

region = 

runtime = 

secret_environment_variables = []

service_account_email = ""

source_dependent_files = []

source_directory = 

timeout_s = 60

trigger_http = null

vpc_connector = null

vpc_connector_egress_settings = null


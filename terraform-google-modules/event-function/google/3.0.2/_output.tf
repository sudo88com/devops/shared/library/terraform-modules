
output "https_trigger_url" {
  description = "URL which triggers function execution."
  value       = module.event-function.https_trigger_url
}

output "name" {
  description = "The name of the function."
  value       = module.event-function.name
}


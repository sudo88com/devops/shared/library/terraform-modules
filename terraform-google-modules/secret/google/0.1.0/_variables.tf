
variable "application_name" {
  type        = 
  description = "The application to fetch secrets for"
  default     = ""
}

variable "credentials_file_path" {
  type        = 
  description = "The path to the GCP credentials"
  default     = ""
}

variable "env" {
  type        = 
  description = "The environment to fetch secrets for"
  default     = ""
}

variable "secret" {
  type        = 
  description = "The name of the secret to fetch"
  default     = ""
}

variable "shared" {
  type        = 
  description = "Will we fetch the secret from the shared bucket instead of an application-specific bucket?"
  default     = "false"
}


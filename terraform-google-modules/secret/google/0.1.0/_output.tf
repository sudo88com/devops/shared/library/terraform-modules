
output "contents" {
  description = "The actual value of the requested secret"
  value       = module.secret.contents
}


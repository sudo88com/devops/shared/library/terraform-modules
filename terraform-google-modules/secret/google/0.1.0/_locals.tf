
locals {
  application_name = var.application_name
  credentials_file_path = var.credentials_file_path
  env = var.env
  secret = var.secret
  shared = var.shared
}


output "id" {
  description = "The ID of the Pub/Sub topic"
  value       = module.pubsub.id
}

output "subscription_names" {
  description = "The name list of Pub/Sub subscriptions"
  value       = module.pubsub.subscription_names
}

output "subscription_paths" {
  description = "The path list of Pub/Sub subscriptions"
  value       = module.pubsub.subscription_paths
}

output "topic" {
  description = "The name of the Pub/Sub topic"
  value       = module.pubsub.topic
}

output "topic_labels" {
  description = "Labels assigned to the Pub/Sub topic"
  value       = module.pubsub.topic_labels
}

output "uri" {
  description = "The URI of the Pub/Sub topic"
  value       = module.pubsub.uri
}


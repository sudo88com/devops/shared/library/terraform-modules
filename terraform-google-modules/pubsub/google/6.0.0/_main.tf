
module "pubsub" {
  source = "terraform-aws-modules/pubsub/aws"
  version = "6.0.0"
  bigquery_subscriptions = var.bigquery_subscriptions
  cloud_storage_subscriptions = var.cloud_storage_subscriptions
  create_subscriptions = var.create_subscriptions
  create_topic = var.create_topic
  grant_token_creator = var.grant_token_creator
  message_storage_policy = var.message_storage_policy
  project_id = var.project_id
  pull_subscriptions = var.pull_subscriptions
  push_subscriptions = var.push_subscriptions
  schema = var.schema
  subscription_labels = var.subscription_labels
  topic = var.topic
  topic_kms_key_name = var.topic_kms_key_name
  topic_labels = var.topic_labels
  topic_message_retention_duration = var.topic_message_retention_duration
}

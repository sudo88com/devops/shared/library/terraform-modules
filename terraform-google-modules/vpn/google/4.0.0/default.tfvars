
advertised_route_priority = 100

bgp_cr_session_range = [
  "169.254.1.1/30",
  "169.254.1.5/30"
]

bgp_remote_session_range = [
  "169.254.1.2",
  "169.254.1.6"
]

cr_enabled = false

cr_name = ""

gateway_name = "test-vpn"

ike_version = 2

local_traffic_selector = [
  "0.0.0.0/0"
]

network = 

peer_asn = [
  "65101"
]

peer_ips = 

project_id = 

region = 

remote_subnet = []

remote_traffic_selector = [
  "0.0.0.0/0"
]

route_priority = 1000

route_tags = []

shared_secret = ""

tunnel_count = 1

tunnel_name_prefix = ""

vpn_gw_ip = ""



output "forseti-cai-storage-bucket" {
  description = "Forseti CAI storage bucket"
  value       = module.forseti.forseti-cai-storage-bucket
}

output "forseti-client-service-account" {
  description = "Forseti Client service account"
  value       = module.forseti.forseti-client-service-account
}

output "forseti-client-storage-bucket" {
  description = "Forseti Client storage bucket"
  value       = module.forseti.forseti-client-storage-bucket
}

output "forseti-client-vm-ip" {
  description = "Forseti Client VM private IP address"
  value       = module.forseti.forseti-client-vm-ip
}

output "forseti-client-vm-name" {
  description = "Forseti Client VM name"
  value       = module.forseti.forseti-client-vm-name
}

output "forseti-cloudsql-connection-name" {
  description = "Forseti CloudSQL Connection String"
  value       = module.forseti.forseti-cloudsql-connection-name
}

output "forseti-cloudsql-instance-ip" {
  description = "The IP of the master CloudSQL instance"
  value       = module.forseti.forseti-cloudsql-instance-ip
}

output "forseti-cloudsql-password" {
  description = "CloudSQL password"
  value       = module.forseti.forseti-cloudsql-password
}

output "forseti-cloudsql-user" {
  description = "CloudSQL user"
  value       = module.forseti.forseti-cloudsql-user
}

output "forseti-server-git-public-key-openssh" {
  description = "The public OpenSSH key generated to allow the Forseti Server to clone the policy library repository."
  value       = module.forseti.forseti-server-git-public-key-openssh
}

output "forseti-server-service-account" {
  description = "Forseti Server service account"
  value       = module.forseti.forseti-server-service-account
}

output "forseti-server-storage-bucket" {
  description = "Forseti Server storage bucket"
  value       = module.forseti.forseti-server-storage-bucket
}

output "forseti-server-vm-internal-dns" {
  description = "Forseti Server internal DNS"
  value       = module.forseti.forseti-server-vm-internal-dns
}

output "forseti-server-vm-ip" {
  description = "Forseti Server VM private IP address"
  value       = module.forseti.forseti-server-vm-ip
}

output "forseti-server-vm-name" {
  description = "Forseti Server VM name"
  value       = module.forseti.forseti-server-vm-name
}

output "suffix" {
  description = "The random suffix appended to Forseti resources"
  value       = module.forseti.suffix
}


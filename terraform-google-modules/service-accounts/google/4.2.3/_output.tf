
output "email" {
  description = "Service account email (for single use)."
  value       = module.service-accounts.email
}

output "emails" {
  description = "Service account emails by name."
  value       = module.service-accounts.emails
}

output "emails_list" {
  description = "Service account emails as list."
  value       = module.service-accounts.emails_list
}

output "iam_email" {
  description = "IAM-format service account email (for single use)."
  value       = module.service-accounts.iam_email
}

output "iam_emails" {
  description = "IAM-format service account emails by name."
  value       = module.service-accounts.iam_emails
}

output "iam_emails_list" {
  description = "IAM-format service account emails as list."
  value       = module.service-accounts.iam_emails_list
}

output "key" {
  description = "Service account key (for single use)."
  value       = module.service-accounts.key
}

output "keys" {
  description = "Map of service account keys."
  value       = module.service-accounts.keys
}

output "service_account" {
  description = "Service account resource (for single use)."
  value       = module.service-accounts.service_account
}

output "service_accounts" {
  description = "Service account resources as list."
  value       = module.service-accounts.service_accounts
}

output "service_accounts_map" {
  description = "Service account resources by name."
  value       = module.service-accounts.service_accounts_map
}


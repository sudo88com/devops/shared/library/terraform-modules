
admin_user = 

api = "reports_v1"

applications = [
  "admin",
  "login",
  "drive",
  "mobile",
  "token"
]

export_filter = ""

frequency = "*/10 * * * *"

gsuite_exporter_version = "0.0.4"

machine_image = "debian-cloud/debian-11"

machine_name = "gsuite-exporter"

machine_network = "default"

machine_project = ""

machine_type = "f1-micro"

machine_zone = "us-central1-a"

project_id = 

service_account = 


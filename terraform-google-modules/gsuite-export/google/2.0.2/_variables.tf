
variable "admin_user" {
  type        = string
  description = "The GSuite Admin user to impersonate"
  default     = ""
}

variable "api" {
  type        = string
  description = "The Admin SDK API to sync data from"
  default     = "reports_v1"
}

variable "applications" {
  type        = list(string)
  description = "The Admin SDK applications to sync data from"
  default     = [
  "admin",
  "login",
  "drive",
  "mobile",
  "token"
]
}

variable "export_filter" {
  type        = string
  description = "The export filter to use in a log export (if any)"
  default     = ""
}

variable "frequency" {
  type        = string
  description = "The crontab entry that controls the sync frequency"
  default     = "*/10 * * * *"
}

variable "gsuite_exporter_version" {
  type        = string
  description = "Version of the gsuite-exporter PyPi package"
  default     = "0.0.4"
}

variable "machine_image" {
  type        = string
  description = "The instance image"
  default     = "debian-cloud/debian-11"
}

variable "machine_name" {
  type        = string
  description = "The instance name"
  default     = "gsuite-exporter"
}

variable "machine_network" {
  type        = string
  description = "The instance network"
  default     = "default"
}

variable "machine_project" {
  type        = string
  description = "The instance project id. Defaults to `project_id`"
  default     = ""
}

variable "machine_type" {
  type        = string
  description = "The instance type"
  default     = "f1-micro"
}

variable "machine_zone" {
  type        = string
  description = "The instance zone"
  default     = "us-central1-a"
}

variable "project_id" {
  type        = string
  description = "The project to export GSuite data to."
  default     = ""
}

variable "service_account" {
  type        = string
  description = "The service account for exporting GSuite data. Needs domain-wide delegation and correct access scopes."
  default     = ""
}



output "filter" {
  description = "Log export filter for logs exported by GSuite-exporter"
  value       = module.gsuite-export.filter
}

output "instance_name" {
  description = "GSuite Exporter instance name"
  value       = module.gsuite-export.instance_name
}

output "instance_project" {
  description = "GSuite Exporter instance project"
  value       = module.gsuite-export.instance_project
}

output "instance_zone" {
  description = "GSuite Exporter instance zone"
  value       = module.gsuite-export.instance_zone
}


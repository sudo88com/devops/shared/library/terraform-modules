
module "composer" {
  source = "terraform-aws-modules/composer/aws"
  version = "5.1.0"
  composer_env_name = var.composer_env_name
  enable_private_endpoint = var.enable_private_endpoint
  network = var.network
  project_id = var.project_id
  region = var.region
  subnetwork = var.subnetwork
  zone = var.zone
}

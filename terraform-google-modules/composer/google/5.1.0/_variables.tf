
variable "composer_env_name" {
  type        = string
  description = "Name of Cloud Composer Environment"
  default     = ""
}

variable "enable_private_endpoint" {
  type        = bool
  description = "Configure public access to the cluster endpoint."
  default     = false
}

variable "network" {
  type        = string
  description = "Network where Cloud Composer is created."
  default     = ""
}

variable "project_id" {
  type        = string
  description = "Project ID where Cloud Composer Environment is created."
  default     = ""
}

variable "region" {
  type        = string
  description = "Region where the Cloud Composer Environment is created."
  default     = ""
}

variable "subnetwork" {
  type        = string
  description = "Subetwork where Cloud Composer is created."
  default     = ""
}

variable "zone" {
  type        = string
  description = "Zone where the Cloud Composer Environment is created."
  default     = ""
}



variable "access" {
  type        = any
  description = "An array of objects that define dataset access for one or more entities."
  default     = [
  {
    "role": "roles/bigquery.dataOwner",
    "special_group": "projectOwners"
  }
]
}

variable "dataset_id" {
  type        = string
  description = "Unique ID for the dataset being provisioned."
  default     = ""
}

variable "dataset_labels" {
  type        = map(string)
  description = "Key value pairs in a map for dataset labels"
  default     = {}
}

variable "dataset_name" {
  type        = string
  description = "Friendly name for the dataset being provisioned."
  default     = null
}

variable "default_table_expiration_ms" {
  type        = number
  description = "TTL of tables using the dataset in MS"
  default     = null
}

variable "delete_contents_on_destroy" {
  type        = bool
  description = "(Optional) If set to true, delete all the tables in the dataset when destroying the resource; otherwise, destroying the resource will fail if tables are present."
  default     = null
}

variable "deletion_protection" {
  type        = bool
  description = "Whether or not to allow Terraform to destroy the instance. Unless this field is set to false in Terraform state, a terraform destroy or terraform apply that would delete the instance will fail"
  default     = false
}

variable "description" {
  type        = string
  description = "Dataset description."
  default     = null
}

variable "encryption_key" {
  type        = string
  description = "Default encryption key to apply to the dataset. Defaults to null (Google-managed)."
  default     = null
}

variable "external_tables" {
  type        = list(object({
    table_id              = string,
    description           = optional(string),
    autodetect            = bool,
    compression           = string,
    ignore_unknown_values = bool,
    max_bad_records       = number,
    schema                = string,
    source_format         = string,
    source_uris           = list(string),
    csv_options = object({
      quote                 = string,
      allow_jagged_rows     = bool,
      allow_quoted_newlines = bool,
      encoding              = string,
      field_delimiter       = string,
      skip_leading_rows     = number,
    }),
    google_sheets_options = object({
      range             = string,
      skip_leading_rows = number,
    }),
    hive_partitioning_options = object({
      mode              = string,
      source_uri_prefix = string,
    }),
    expiration_time = string,
    labels          = map(string),
  }))
  description = "A list of objects which include table_id, expiration_time, external_data_configuration, and labels."
  default     = []
}

variable "location" {
  type        = string
  description = "The regional location for the dataset only US and EU are allowed in module"
  default     = "US"
}

variable "materialized_views" {
  type        = list(object({
    view_id             = string,
    description         = optional(string),
    query               = string,
    enable_refresh      = bool,
    refresh_interval_ms = string,
    clustering          = list(string),
    time_partitioning = object({
      expiration_ms            = string,
      field                    = string,
      type                     = string,
      require_partition_filter = bool,
    }),
    range_partitioning = object({
      field = string,
      range = object({
        start    = string,
        end      = string,
        interval = string,
      }),
    }),
    expiration_time = string,
    labels          = map(string),
  }))
  description = "A list of objects which includes view_id, view_query, clustering, time_partitioning, range_partitioning, expiration_time and labels"
  default     = []
}

variable "max_time_travel_hours" {
  type        = number
  description = "Defines the time travel window in hours"
  default     = null
}

variable "project_id" {
  type        = string
  description = "Project where the dataset and table are created"
  default     = ""
}

variable "routines" {
  type        = list(object({
    routine_id      = string,
    routine_type    = string,
    language        = string,
    definition_body = string,
    return_type     = string,
    description     = string,
    arguments = list(object({
      name          = string,
      data_type     = string,
      argument_kind = string,
      mode          = string,
    })),
  }))
  description = "A list of objects which include routine_id, routine_type, routine_language, definition_body, return_type, routine_description and arguments."
  default     = []
}

variable "tables" {
  type        = list(object({
    table_id    = string,
    description = optional(string),
    table_name  = optional(string),
    schema      = string,
    clustering  = list(string),
    time_partitioning = object({
      expiration_ms            = string,
      field                    = string,
      type                     = string,
      require_partition_filter = bool,
    }),
    range_partitioning = object({
      field = string,
      range = object({
        start    = string,
        end      = string,
        interval = string,
      }),
    }),
    expiration_time = string,
    labels          = map(string),
  }))
  description = "A list of objects which include table_id, table_name, schema, clustering, time_partitioning, range_partitioning, expiration_time and labels."
  default     = []
}

variable "views" {
  type        = list(object({
    view_id        = string,
    description    = optional(string),
    query          = string,
    use_legacy_sql = bool,
    labels         = map(string),
  }))
  description = "A list of objects which include view_id and view query"
  default     = []
}


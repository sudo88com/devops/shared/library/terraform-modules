
access = [
  {
    "role": "roles/bigquery.dataOwner",
    "special_group": "projectOwners"
  }
]

dataset_id = 

dataset_labels = {}

dataset_name = null

default_table_expiration_ms = null

delete_contents_on_destroy = null

deletion_protection = false

description = null

encryption_key = null

external_tables = []

location = "US"

materialized_views = []

max_time_travel_hours = null

project_id = 

routines = []

tables = []

views = []


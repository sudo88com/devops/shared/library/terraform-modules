
module "log-export" {
  source = "terraform-aws-modules/log-export/aws"
  version = "8.1.0"
  bigquery_options = var.bigquery_options
  description = var.description
  destination_uri = var.destination_uri
  disabled = var.disabled
  exclusions = var.exclusions
  filter = var.filter
  include_children = var.include_children
  log_sink_name = var.log_sink_name
  parent_resource_id = var.parent_resource_id
  parent_resource_type = var.parent_resource_type
  unique_writer_identity = var.unique_writer_identity
}


output "filter" {
  description = "The filter to be applied when exporting logs."
  value       = module.log-export.filter
}

output "log_sink_resource_id" {
  description = "The resource ID of the log sink that was created."
  value       = module.log-export.log_sink_resource_id
}

output "log_sink_resource_name" {
  description = "The resource name of the log sink that was created."
  value       = module.log-export.log_sink_resource_name
}

output "parent_resource_id" {
  description = "The ID of the GCP resource in which you create the log sink."
  value       = module.log-export.parent_resource_id
}

output "writer_identity" {
  description = "The service account that logging uses to write log entries to the destination."
  value       = module.log-export.writer_identity
}



variable "bigquery_options" {
  type        = object({
    use_partitioned_tables = bool
  })
  description = "(Optional) Options that affect sinks exporting data to BigQuery. use_partitioned_tables - (Required) Whether to use BigQuery's partition tables."
  default     = null
}

variable "description" {
  type        = string
  description = "A description of this sink. The maximum length of the description is 8000 characters."
  default     = null
}

variable "destination_uri" {
  type        = string
  description = "The self_link URI of the destination resource (This is available as an output coming from one of the destination submodules)"
  default     = ""
}

variable "disabled" {
  type        = bool
  description = "(Optional) If set to true, then the sink is disabled and it does not export any log entries."
  default     = false
}

variable "exclusions" {
  type        = list(object({
    name        = string,
    description = string,
    filter      = string,
    disabled    = bool
  }))
  description = "(Optional) A list of sink exclusion filters."
  default     = []
}

variable "filter" {
  type        = string
  description = "The filter to apply when exporting logs. Only log entries that match the filter are exported. Default is '' which exports all logs."
  default     = ""
}

variable "include_children" {
  type        = bool
  description = "Only valid if 'organization' or 'folder' is chosen as var.parent_resource.type. Determines whether or not to include children organizations/folders in the sink export. If true, logs associated with child projects are also exported; otherwise only logs relating to the provided organization/folder are included."
  default     = false
}

variable "log_sink_name" {
  type        = string
  description = "The name of the log sink to be created."
  default     = ""
}

variable "parent_resource_id" {
  type        = string
  description = "The ID of the GCP resource in which you create the log sink. If var.parent_resource_type is set to 'project', then this is the Project ID (and etc)."
  default     = ""
}

variable "parent_resource_type" {
  type        = string
  description = "The GCP resource in which you create the log sink. The value must not be computed, and must be one of the following: 'project', 'folder', 'billing_account', or 'organization'."
  default     = "project"
}

variable "unique_writer_identity" {
  type        = bool
  description = "Whether or not to create a unique identity associated with this sink. If false (the default), then the writer_identity used is serviceAccount:cloud-logs@system.gserviceaccount.com. If true, then a unique service account is created and used for the logging sink."
  default     = false
}


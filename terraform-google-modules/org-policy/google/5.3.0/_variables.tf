
variable "allow" {
  type        = list(string)
  description = "(Only for list constraints) List of values which should be allowed"
  default     = [
  ""
]
}

variable "allow_list_length" {
  type        = number
  description = "The number of elements in the allow list"
  default     = 0
}

variable "constraint" {
  type        = string
  description = "The constraint to be applied"
  default     = ""
}

variable "deny" {
  type        = list(string)
  description = "(Only for list constraints) List of values which should be denied"
  default     = [
  ""
]
}

variable "deny_list_length" {
  type        = number
  description = "The number of elements in the deny list"
  default     = 0
}

variable "enforce" {
  type        = bool
  description = "If boolean constraint, whether the policy is enforced at the root; if list constraint, whether to deny all (true) or allow all"
  default     = null
}

variable "exclude_folders" {
  type        = set(string)
  description = "Set of folders to exclude from the policy"
  default     = []
}

variable "exclude_projects" {
  type        = set(string)
  description = "Set of projects to exclude from the policy"
  default     = []
}

variable "folder_id" {
  type        = string
  description = "The folder id for putting the policy"
  default     = null
}

variable "organization_id" {
  type        = string
  description = "The organization id for putting the policy"
  default     = null
}

variable "policy_for" {
  type        = string
  description = "Resource hierarchy node to apply the policy to: can be one of `organization`, `folder`, or `project`."
  default     = ""
}

variable "policy_type" {
  type        = string
  description = "The constraint type to work with (either 'boolean' or 'list')"
  default     = "list"
}

variable "project_id" {
  type        = string
  description = "The project id for putting the policy"
  default     = null
}


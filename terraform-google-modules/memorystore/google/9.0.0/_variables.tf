
variable "alternative_location_id" {
  type        = string
  description = "The alternative zone where the instance will be provisioned."
  default     = null
}

variable "auth_enabled" {
  type        = bool
  description = "Indicates whether OSS Redis AUTH is enabled for the instance. If set to true AUTH is enabled on the instance."
  default     = false
}

variable "authorized_network" {
  type        = string
  description = "The full name of the Google Compute Engine network to which the instance is connected. If left unspecified, the default network will be used."
  default     = null
}

variable "connect_mode" {
  type        = string
  description = "The connection mode of the Redis instance. Can be either DIRECT_PEERING or PRIVATE_SERVICE_ACCESS. The default connect mode if not provided is DIRECT_PEERING."
  default     = null
}

variable "customer_managed_key" {
  type        = string
  description = "Default encryption key to apply to the Redis instance. Defaults to null (Google-managed)."
  default     = null
}

variable "display_name" {
  type        = string
  description = "An arbitrary and optional user-provided name for the instance."
  default     = null
}

variable "enable_apis" {
  type        = bool
  description = "Flag for enabling redis.googleapis.com in your project"
  default     = true
}

variable "labels" {
  type        = map(string)
  description = "The resource labels to represent user provided metadata."
  default     = null
}

variable "location_id" {
  type        = string
  description = "The zone where the instance will be provisioned. If not provided, the service will choose a zone for the instance. For STANDARD_HA tier, instances will be created across two zones for protection against zonal failures. If [alternativeLocationId] is also provided, it must be different from [locationId]."
  default     = null
}

variable "maintenance_policy" {
  type        = object({
    day = string
    start_time = object({
      hours   = number
      minutes = number
      seconds = number
      nanos   = number
    })
  })
  description = "The maintenance policy for an instance."
  default     = null
}

variable "memory_size_gb" {
  type        = number
  description = "Redis memory size in GiB. Defaulted to 1 GiB"
  default     = 1
}

variable "name" {
  type        = string
  description = "The ID of the instance or a fully qualified identifier for the instance."
  default     = ""
}

variable "persistence_config" {
  type        = object({
    persistence_mode    = string
    rdb_snapshot_period = string
  })
  description = "The Redis persistence configuration parameters. https://cloud.google.com/memorystore/docs/redis/reference/rest/v1/projects.locations.instances#persistenceconfig"
  default     = null
}

variable "project" {
  type        = string
  description = "The ID of the project in which the resource belongs to."
  default     = ""
}

variable "read_replicas_mode" {
  type        = string
  description = "Read replicas mode. https://cloud.google.com/memorystore/docs/redis/reference/rest/v1/projects.locations.instances#readreplicasmode "
  default     = "READ_REPLICAS_DISABLED"
}

variable "redis_configs" {
  type        = map(any)
  description = "The Redis configuration parameters. See [more details](https://cloud.google.com/memorystore/docs/redis/reference/rest/v1/projects.locations.instances#Instance.FIELDS.redis_configs)"
  default     = {}
}

variable "redis_version" {
  type        = string
  description = "The version of Redis software."
  default     = null
}

variable "region" {
  type        = string
  description = "The GCP region to use."
  default     = null
}

variable "replica_count" {
  type        = number
  description = "The number of replicas. can"
  default     = null
}

variable "reserved_ip_range" {
  type        = string
  description = "The CIDR range of internal addresses that are reserved for this instance."
  default     = null
}

variable "secondary_ip_range" {
  type        = string
  description = "Optional. Additional IP range for node placement. Required when enabling read replicas on an existing instance."
  default     = null
}

variable "tier" {
  type        = string
  description = "The service tier of the instance. https://cloud.google.com/memorystore/docs/redis/reference/rest/v1/projects.locations.instances#Tier"
  default     = "STANDARD_HA"
}

variable "transit_encryption_mode" {
  type        = string
  description = "The TLS mode of the Redis instance, If not provided, TLS is enabled for the instance."
  default     = "SERVER_AUTHENTICATION"
}


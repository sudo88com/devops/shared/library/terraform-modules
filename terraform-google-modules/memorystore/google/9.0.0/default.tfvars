
alternative_location_id = null

auth_enabled = false

authorized_network = null

connect_mode = null

customer_managed_key = null

display_name = null

enable_apis = true

labels = null

location_id = null

maintenance_policy = null

memory_size_gb = 1

name = 

persistence_config = null

project = 

read_replicas_mode = "READ_REPLICAS_DISABLED"

redis_configs = {}

redis_version = null

region = null

replica_count = null

reserved_ip_range = null

secondary_ip_range = null

tier = "STANDARD_HA"

transit_encryption_mode = "SERVER_AUTHENTICATION"



output "apiserver_private_host" {
  description = "Private OKE cluster endpoint address"
  value       = module.oke.apiserver_private_host
}

output "availability_domains" {
  description = "Availability domains for tenancy & region"
  value       = module.oke.availability_domains
}

output "bastion_id" {
  description = "ID of bastion instance"
  value       = module.oke.bastion_id
}

output "bastion_nsg_id" {
  description = "Network Security Group for bastion host(s)."
  value       = module.oke.bastion_nsg_id
}

output "bastion_public_ip" {
  description = "Public IP address of bastion host"
  value       = module.oke.bastion_public_ip
}

output "bastion_subnet_cidr" {
  description = ""
  value       = module.oke.bastion_subnet_cidr
}

output "bastion_subnet_id" {
  description = ""
  value       = module.oke.bastion_subnet_id
}

output "cluster_ca_cert" {
  description = "OKE cluster CA certificate"
  value       = module.oke.cluster_ca_cert
}

output "cluster_endpoints" {
  description = "Endpoints for the OKE cluster"
  value       = module.oke.cluster_endpoints
}

output "cluster_id" {
  description = "ID of the OKE cluster"
  value       = module.oke.cluster_id
}

output "cluster_kubeconfig" {
  description = "OKE kubeconfig"
  value       = module.oke.cluster_kubeconfig
}

output "control_plane_nsg_id" {
  description = "Network Security Group for Kubernetes control plane(s)."
  value       = module.oke.control_plane_nsg_id
}

output "control_plane_subnet_cidr" {
  description = ""
  value       = module.oke.control_plane_subnet_cidr
}

output "control_plane_subnet_id" {
  description = ""
  value       = module.oke.control_plane_subnet_id
}

output "dynamic_group_ids" {
  description = "Cluster IAM dynamic group IDs"
  value       = module.oke.dynamic_group_ids
}

output "fss_nsg_id" {
  description = "Network Security Group for File Storage Service resources."
  value       = module.oke.fss_nsg_id
}

output "fss_subnet_cidr" {
  description = ""
  value       = module.oke.fss_subnet_cidr
}

output "fss_subnet_id" {
  description = ""
  value       = module.oke.fss_subnet_id
}

output "ig_route_table_id" {
  description = "Internet gateway route table ID"
  value       = module.oke.ig_route_table_id
}

output "int_lb_nsg_id" {
  description = "Network Security Group for internal load balancers."
  value       = module.oke.int_lb_nsg_id
}

output "int_lb_subnet_cidr" {
  description = ""
  value       = module.oke.int_lb_subnet_cidr
}

output "int_lb_subnet_id" {
  description = ""
  value       = module.oke.int_lb_subnet_id
}

output "lpg_all_attributes" {
  description = "all attributes of created lpg"
  value       = module.oke.lpg_all_attributes
}

output "nat_route_table_id" {
  description = "NAT gateway route table ID"
  value       = module.oke.nat_route_table_id
}

output "network_security_rules" {
  description = ""
  value       = module.oke.network_security_rules
}

output "operator_id" {
  description = "ID of operator instance"
  value       = module.oke.operator_id
}

output "operator_nsg_id" {
  description = "Network Security Group for operator host(s)."
  value       = module.oke.operator_nsg_id
}

output "operator_private_ip" {
  description = "Private IP address of operator host"
  value       = module.oke.operator_private_ip
}

output "operator_subnet_cidr" {
  description = ""
  value       = module.oke.operator_subnet_cidr
}

output "operator_subnet_id" {
  description = ""
  value       = module.oke.operator_subnet_id
}

output "pod_nsg_id" {
  description = "Network Security Group for pods."
  value       = module.oke.pod_nsg_id
}

output "pod_subnet_cidr" {
  description = ""
  value       = module.oke.pod_subnet_cidr
}

output "pod_subnet_id" {
  description = ""
  value       = module.oke.pod_subnet_id
}

output "policy_statements" {
  description = "Cluster IAM policy statements"
  value       = module.oke.policy_statements
}

output "pub_lb_nsg_id" {
  description = "Network Security Group for public load balancers."
  value       = module.oke.pub_lb_nsg_id
}

output "pub_lb_subnet_cidr" {
  description = ""
  value       = module.oke.pub_lb_subnet_cidr
}

output "pub_lb_subnet_id" {
  description = ""
  value       = module.oke.pub_lb_subnet_id
}

output "ssh_to_bastion" {
  description = "SSH command for bastion host"
  value       = module.oke.ssh_to_bastion
}

output "ssh_to_operator" {
  description = "SSH command for operator host"
  value       = module.oke.ssh_to_operator
}

output "state_id" {
  description = ""
  value       = module.oke.state_id
}

output "vcn_id" {
  description = "VCN ID"
  value       = module.oke.vcn_id
}

output "worker_instances" {
  description = "Created worker pools (mode == 'instance')"
  value       = module.oke.worker_instances
}

output "worker_nsg_id" {
  description = "Network Security Group for worker nodes."
  value       = module.oke.worker_nsg_id
}

output "worker_pool_ids" {
  description = "Enabled worker pool IDs"
  value       = module.oke.worker_pool_ids
}

output "worker_pool_ips" {
  description = "Created worker instance private IPs by pool for available modes ('node-pool', 'instance')."
  value       = module.oke.worker_pool_ips
}

output "worker_pools" {
  description = "Created worker pools (mode != 'instance')"
  value       = module.oke.worker_pools
}

output "worker_subnet_cidr" {
  description = ""
  value       = module.oke.worker_subnet_cidr
}

output "worker_subnet_id" {
  description = ""
  value       = module.oke.worker_subnet_id
}



variable "agent_config" {
  type        = object({
    are_all_plugins_disabled = bool,
    is_management_disabled   = bool,
    is_monitoring_disabled   = bool,
    plugins_config           = map(string),
  })
  description = "Default agent_config for self-managed worker pools created with mode: 'instance', 'instance-pool', or 'cluster-network'. See <a href=https://docs.oracle.com/en-us/iaas/api/#/en/iaas/20160918/datatypes/InstanceAgentConfig for more information."
  default     = null
}

variable "allow_bastion_cluster_access" {
  type        = bool
  description = "Whether to allow access to the Kubernetes cluster endpoint from the bastion host."
  default     = false
}

variable "allow_node_port_access" {
  type        = bool
  description = "Whether to allow access from worker NodePort range to load balancers."
  default     = false
}

variable "allow_pod_internet_access" {
  type        = bool
  description = "Allow pods to egress to internet. Ignored when cni_type != 'npn'."
  default     = true
}

variable "allow_rules_internal_lb" {
  type        = any
  description = "A map of additional rules to allow incoming traffic for internal load balancers."
  default     = {}
}

variable "allow_rules_public_lb" {
  type        = any
  description = "A map of additional rules to allow incoming traffic for public load balancers."
  default     = {}
}

variable "allow_worker_internet_access" {
  type        = bool
  description = "Allow worker nodes to egress to internet. Required if container images are in a registry other than OCIR."
  default     = true
}

variable "allow_worker_ssh_access" {
  type        = bool
  description = "Whether to allow SSH access to worker nodes."
  default     = false
}

variable "api_fingerprint" {
  type        = string
  description = "Fingerprint of the API private key to use with OCI API."
  default     = null
}

variable "api_private_key" {
  type        = string
  description = "The contents of the private key file to use with OCI API, optionally base64-encoded. This takes precedence over private_key_path if both are specified in the provider."
  default     = null
}

variable "api_private_key_password" {
  type        = string
  description = "The corresponding private key password to use with the api private key if it is encrypted."
  default     = null
}

variable "api_private_key_path" {
  type        = string
  description = "The path to the OCI API private key."
  default     = null
}

variable "assign_dns" {
  type        = bool
  description = "Whether to assign DNS records to created instances or disable DNS resolution of hostnames in the VCN."
  default     = true
}

variable "assign_public_ip_to_control_plane" {
  type        = bool
  description = "Whether to assign a public IP address to the API endpoint for public access. Requires the control plane subnet to be public to assign a public IP address."
  default     = false
}

variable "await_node_readiness" {
  type        = string
  description = "Optionally block completion of Terraform apply until one/all worker nodes become ready."
  default     = "none"
}

variable "bastion_allowed_cidrs" {
  type        = list(string)
  description = "A list of CIDR blocks to allow SSH access to the bastion host. NOTE: Default is empty i.e. no access permitted. Allow access from anywhere with '0.0.0.0/0'."
  default     = []
}

variable "bastion_availability_domain" {
  type        = string
  description = "The availability domain for bastion placement. Defaults to first available."
  default     = null
}

variable "bastion_defined_tags" {
  type        = map(string)
  description = "Defined tags applied to created resources."
  default     = {}
}

variable "bastion_freeform_tags" {
  type        = map(string)
  description = "Freeform tags applied to created resources."
  default     = {}
}

variable "bastion_image_id" {
  type        = string
  description = "Image ID for created bastion instance."
  default     = null
}

variable "bastion_image_os" {
  type        = string
  description = "Bastion image operating system name when bastion_image_type = 'platform'."
  default     = "Oracle Autonomous Linux"
}

variable "bastion_image_os_version" {
  type        = string
  description = "Bastion image operating system version when bastion_image_type = 'platform'."
  default     = "8"
}

variable "bastion_image_type" {
  type        = string
  description = "Whether to use a platform or custom image for the created bastion instance. When custom is set, the bastion_image_id must be specified."
  default     = "platform"
}

variable "bastion_is_public" {
  type        = bool
  description = "Whether to create allocate a public IP and subnet for the created bastion host."
  default     = true
}

variable "bastion_nsg_ids" {
  type        = list(string)
  description = "An additional list of network security group (NSG) IDs for bastion security."
  default     = []
}

variable "bastion_public_ip" {
  type        = string
  description = "The IP address of an existing bastion host, if create_bastion = false."
  default     = null
}

variable "bastion_shape" {
  type        = map(any)
  description = "The shape of bastion instance."
  default     = {
  "boot_volume_size": 50,
  "memory": 4,
  "ocpus": 1,
  "shape": "VM.Standard.E4.Flex"
}
}

variable "bastion_upgrade" {
  type        = bool
  description = "Whether to upgrade bastion packages after provisioning."
  default     = false
}

variable "bastion_user" {
  type        = string
  description = "User for SSH access through bastion host."
  default     = "opc"
}

variable "cilium_helm_values" {
  type        = map(string)
  description = "Map of individual Helm chart values. See https://registry.terraform.io/providers/hashicorp/helm/latest/docs/data-sources/template."
  default     = {}
}

variable "cilium_helm_values_files" {
  type        = list(string)
  description = "Paths to a local YAML files with Helm chart values (as with `helm install -f` which supports multiple). Generate with defaults using `helm show values [CHART] [flags]`."
  default     = []
}

variable "cilium_helm_version" {
  type        = string
  description = "Version of the Helm chart to install. List available releases using `helm search repo [keyword] --versions`."
  default     = "1.14.4"
}

variable "cilium_install" {
  type        = bool
  description = "Whether to deploy the Cilium Helm chart. May only be enabled when cni_type = 'flannel'. See https://docs.cilium.io. NOTE: Provided only as a convenience and not supported by or sourced from Oracle - use at your own risk."
  default     = false
}

variable "cilium_namespace" {
  type        = string
  description = "Kubernetes namespace for deployed resources."
  default     = "kube-system"
}

variable "cilium_reapply" {
  type        = bool
  description = "Whether to force reapply of the chart when no changes are detected, e.g. with state modified externally."
  default     = false
}

variable "cluster_autoscaler_helm_values" {
  type        = map(string)
  description = "Map of individual Helm chart values. See <a href=https://registry.terraform.io/providers/hashicorp/helm/latest/docs/data-sources/template>data.helm_template</a>."
  default     = {}
}

variable "cluster_autoscaler_helm_values_files" {
  type        = list(string)
  description = "Paths to a local YAML files with Helm chart values (as with `helm install -f` which supports multiple). Generate with defaults using `helm show values [CHART] [flags]`."
  default     = []
}

variable "cluster_autoscaler_helm_version" {
  type        = string
  description = "Version of the Helm chart to install. List available releases using `helm search repo [keyword] --versions`."
  default     = "9.24.0"
}

variable "cluster_autoscaler_install" {
  type        = bool
  description = "Whether to deploy the Kubernetes Cluster Autoscaler Helm chart. See <a href=https://github.com/kubernetes/autoscaler>kubernetes/autoscaler</a>. NOTE: Provided only as a convenience and not supported by or sourced from Oracle - use at your own risk."
  default     = false
}

variable "cluster_autoscaler_namespace" {
  type        = string
  description = "Kubernetes namespace for deployed resources."
  default     = "kube-system"
}

variable "cluster_ca_cert" {
  type        = string
  description = "Base64+PEM-encoded cluster CA certificate for unmanaged instance pools. Determined automatically when 'create_cluster' = true or 'cluster_id' is provided."
  default     = null
}

variable "cluster_defined_tags" {
  type        = map(string)
  description = "Defined tags applied to created resources."
  default     = {}
}

variable "cluster_dns" {
  type        = string
  description = "Cluster DNS resolver IP address. Determined automatically when not set (recommended)."
  default     = null
}

variable "cluster_freeform_tags" {
  type        = map(string)
  description = "Freeform tags applied to created resources."
  default     = {}
}

variable "cluster_id" {
  type        = string
  description = "An existing OKE cluster OCID when `create_cluster = false`."
  default     = null
}

variable "cluster_kms_key_id" {
  type        = string
  description = "The id of the OCI KMS key to be used as the master encryption key for Kubernetes secrets encryption."
  default     = ""
}

variable "cluster_name" {
  type        = string
  description = "The name of oke cluster."
  default     = "oke"
}

variable "cluster_type" {
  type        = string
  description = "The cluster type. See <a href=https://docs.oracle.com/en-us/iaas/Content/ContEng/Tasks/contengworkingwithenhancedclusters.htm>Working with Enhanced Clusters and Basic Clusters</a> for more information."
  default     = "basic"
}

variable "cni_type" {
  type        = string
  description = "The CNI for the cluster: 'flannel' or 'npn'. See <a href=https://docs.oracle.com/en-us/iaas/Content/ContEng/Concepts/contengpodnetworking.htm>Pod Networking</a>."
  default     = "flannel"
}

variable "compartment_id" {
  type        = string
  description = "The compartment id where resources will be created."
  default     = null
}

variable "compartment_ocid" {
  type        = string
  description = "A compartment OCID automatically populated by Resource Manager."
  default     = null
}

variable "config_file_profile" {
  type        = string
  description = "The profile within the OCI config file to use."
  default     = "DEFAULT"
}

variable "control_plane_allowed_cidrs" {
  type        = list(string)
  description = "The list of CIDR blocks from which the control plane can be accessed."
  default     = []
}

variable "control_plane_is_public" {
  type        = bool
  description = "Whether the Kubernetes control plane endpoint should be allocated a public IP address to enable access over public internet."
  default     = false
}

variable "control_plane_nsg_ids" {
  type        = set(string)
  description = "An additional list of network security groups (NSG) ids for the cluster endpoint."
  default     = []
}

variable "create_bastion" {
  type        = bool
  description = "Whether to create a bastion host."
  default     = true
}

variable "create_cluster" {
  type        = bool
  description = "Whether to create the OKE cluster and dependent resources."
  default     = true
}

variable "create_drg" {
  type        = bool
  description = "Whether to create a Dynamic Routing Gateway and attach it to the VCN."
  default     = false
}

variable "create_iam_autoscaler_policy" {
  type        = string
  description = "Whether to create an IAM dynamic group and policy rules for Cluster Autoscaler management. Depends on configuration of associated component when set to 'auto'. Ignored when 'create_iam_resources' is false."
  default     = "auto"
}

variable "create_iam_defined_tags" {
  type        = bool
  description = "Whether to create defined tags used for IAM policy and tracking. Ignored when 'create_iam_resources' is false."
  default     = false
}

variable "create_iam_kms_policy" {
  type        = string
  description = "Whether to create an IAM dynamic group and policy rules for cluster autoscaler. Depends on configuration of associated components when set to 'auto'. Ignored when 'create_iam_resources' is false."
  default     = "auto"
}

variable "create_iam_operator_policy" {
  type        = string
  description = "Whether to create an IAM dynamic group and policy rules for operator access to the OKE control plane. Depends on configuration of associated components when set to 'auto'. Ignored when 'create_iam_resources' is false."
  default     = "auto"
}

variable "create_iam_resources" {
  type        = bool
  description = "Whether to create IAM dynamic groups, policies, and tags. Resources for components may be controlled individually with 'create_iam_*' variables when enabled. Ignored when 'create_iam_resources' is false."
  default     = false
}

variable "create_iam_tag_namespace" {
  type        = bool
  description = "Whether to create a namespace for defined tags used for IAM policy and tracking. Ignored when 'create_iam_resources' is false."
  default     = false
}

variable "create_iam_worker_policy" {
  type        = string
  description = "Whether to create an IAM dynamic group and policy rules for self-managed worker nodes. Depends on configuration of associated components when set to 'auto'. Ignored when 'create_iam_resources' is false."
  default     = "auto"
}

variable "create_operator" {
  type        = bool
  description = "Whether to create an operator server in a private subnet."
  default     = true
}

variable "create_service_account" {
  type        = bool
  description = "Wether to create a service account or not."
  default     = false
}

variable "create_vcn" {
  type        = bool
  description = "Whether to create a Virtual Cloud Network."
  default     = true
}

variable "current_user_ocid" {
  type        = string
  description = "A user OCID automatically populated by Resource Manager."
  default     = null
}

variable "dcgm_exporter_helm_values" {
  type        = map(string)
  description = "Map of individual Helm chart values. See <a href=https://registry.terraform.io/providers/hashicorp/helm/latest/docs/data-sources/template>data.helm_template</a>."
  default     = {}
}

variable "dcgm_exporter_helm_values_files" {
  type        = list(string)
  description = "Paths to a local YAML files with Helm chart values (as with `helm install -f` which supports multiple). Generate with defaults using `helm show values [CHART] [flags]`."
  default     = []
}

variable "dcgm_exporter_helm_version" {
  type        = string
  description = "Version of the Helm chart to install. List available releases using `helm search repo [keyword] --versions`."
  default     = "3.1.5"
}

variable "dcgm_exporter_install" {
  type        = bool
  description = "Whether to deploy the DCGM exporter Helm chart. See <a href=https://docs.nvidia.com/datacenter/cloud-native/gpu-telemetry/dcgm-exporter.html>DCGM-Exporter</a>. NOTE: Provided only as a convenience and not supported by or sourced from Oracle - use at your own risk."
  default     = false
}

variable "dcgm_exporter_namespace" {
  type        = string
  description = "Kubernetes namespace for deployed resources."
  default     = "metrics"
}

variable "dcgm_exporter_reapply" {
  type        = bool
  description = "Whether to force reapply of the Helm chart when no changes are detected, e.g. with state modified externally."
  default     = false
}

variable "defined_tags" {
  type        = any
  description = "Defined tags to be applied to created resources. Must already exist in the tenancy."
  default     = {
  "bastion": {},
  "cluster": {},
  "iam": {},
  "network": {},
  "operator": {},
  "persistent_volume": {},
  "service_lb": {},
  "workers": {}
}
}

variable "drg_attachments" {
  type        = any
  description = "DRG attachment configurations."
  default     = {}
}

variable "drg_compartment_id" {
  type        = string
  description = "Compartment for the DRG resource. Can be used to override network_compartment_id."
  default     = null
}

variable "drg_display_name" {
  type        = string
  description = "(Updatable) Name of the created Dynamic Routing Gateway. Does not have to be unique. Defaults to 'oke' suffixed with the generated Terraform 'state_id' value."
  default     = null
}

variable "drg_id" {
  type        = string
  description = "ID of an external created Dynamic Routing Gateway to be attached to the VCN."
  default     = null
}

variable "enable_waf" {
  type        = bool
  description = "Whether to enable WAF monitoring of load balancers."
  default     = false
}

variable "freeform_tags" {
  type        = any
  description = "Freeform tags to be applied to created resources."
  default     = {
  "bastion": {},
  "cluster": {},
  "iam": {},
  "network": {},
  "operator": {},
  "persistent_volume": {},
  "service_lb": {},
  "workers": {}
}
}

variable "gatekeeper_helm_values" {
  type        = map(string)
  description = "Map of individual Helm chart values. See <a href=https://registry.terraform.io/providers/hashicorp/helm/latest/docs/data-sources/template>data.helm_template</a>."
  default     = {}
}

variable "gatekeeper_helm_values_files" {
  type        = list(string)
  description = "Paths to a local YAML files with Helm chart values (as with `helm install -f` which supports multiple). Generate with defaults using `helm show values [CHART] [flags]`."
  default     = []
}

variable "gatekeeper_helm_version" {
  type        = string
  description = "Version of the Helm chart to install. List available releases using `helm search repo [keyword] --versions`."
  default     = "3.11.0"
}

variable "gatekeeper_install" {
  type        = bool
  description = "Whether to deploy the Gatekeeper Helm chart. See https://github.com/open-policy-agent/gatekeeper. NOTE: Provided only as a convenience and not supported by or sourced from Oracle - use at your own risk."
  default     = false
}

variable "gatekeeper_namespace" {
  type        = string
  description = "Kubernetes namespace for deployed resources."
  default     = "kube-system"
}

variable "home_region" {
  type        = string
  description = "The tenancy's home region. Required to perform identity operations."
  default     = null
}

variable "iam_defined_tags" {
  type        = map(string)
  description = "Defined tags applied to created resources."
  default     = {}
}

variable "iam_freeform_tags" {
  type        = map(string)
  description = "Freeform tags applied to created resources."
  default     = {}
}

variable "ig_route_table_id" {
  type        = string
  description = "Optional ID of existing internet gateway in VCN."
  default     = null
}

variable "image_signing_keys" {
  type        = set(string)
  description = "A list of KMS key ids used by the worker nodes to verify signed images. The keys must use RSA algorithm."
  default     = []
}

variable "internet_gateway_route_rules" {
  type        = list(map(string))
  description = "(Updatable) List of routing rules to add to Internet Gateway Route Table."
  default     = null
}

variable "kubeproxy_mode" {
  type        = string
  description = "The mode in which to run kube-proxy when unspecified on a pool."
  default     = "iptables"
}

variable "kubernetes_version" {
  type        = string
  description = "The version of kubernetes to use when provisioning OKE or to upgrade an existing OKE cluster to."
  default     = "v1.26.2"
}

variable "load_balancers" {
  type        = string
  description = "The type of subnets to create for load balancers."
  default     = "both"
}

variable "local_peering_gateways" {
  type        = map(any)
  description = "Map of Local Peering Gateways to attach to the VCN."
  default     = null
}

variable "lockdown_default_seclist" {
  type        = bool
  description = "Whether to remove all default security rules from the VCN Default Security List."
  default     = true
}

variable "max_pods_per_node" {
  type        = number
  description = "The default maximum number of pods to deploy per node when unspecified on a pool. Absolute maximum is 110. Ignored when when cni_type != 'npn'."
  default     = 31
}

variable "metrics_server_helm_values" {
  type        = map(string)
  description = "Map of individual Helm chart values. See <a href=https://registry.terraform.io/providers/hashicorp/helm/latest/docs/data-sources/template>data.helm_template</a>."
  default     = {}
}

variable "metrics_server_helm_values_files" {
  type        = list(string)
  description = "Paths to a local YAML files with Helm chart values (as with `helm install -f` which supports multiple). Generate with defaults using `helm show values [CHART] [flags]`."
  default     = []
}

variable "metrics_server_helm_version" {
  type        = string
  description = "Version of the Helm chart to install. List available releases using `helm search repo [keyword] --versions`."
  default     = "3.8.3"
}

variable "metrics_server_install" {
  type        = bool
  description = "Whether to deploy the Kubernetes Metrics Server Helm chart. See <a href=https://github.com/kubernetes-sigs/metrics-server>kubernetes-sigs/metrics-server</a>. NOTE: Provided only as a convenience and not supported by or sourced from Oracle - use at your own risk."
  default     = false
}

variable "metrics_server_namespace" {
  type        = string
  description = "Kubernetes namespace for deployed resources."
  default     = "metrics"
}

variable "mpi_operator_deployment_url" {
  type        = string
  description = "The URL path to the manifest. Leave unset for tags of <a href=https://github.com/kubeflow/mpi-operator>kubeflow/mpi-operator</a> using mpi_operator_version."
  default     = null
}

variable "mpi_operator_install" {
  type        = bool
  description = "Whether to deploy the MPI Operator. See <a href=https://github.com/kubeflow/mpi-operator>kubeflow/mpi-operator</a>. NOTE: Provided only as a convenience and not supported by or sourced from Oracle - use at your own risk."
  default     = false
}

variable "mpi_operator_namespace" {
  type        = string
  description = "Kubernetes namespace for deployed resources."
  default     = "default"
}

variable "mpi_operator_version" {
  type        = string
  description = "Version to install. Ignored when an explicit value for mpi_operator_deployment_url is provided."
  default     = "0.4.0"
}

variable "multus_daemonset_url" {
  type        = string
  description = "The URL path to the Multus manifest. Leave unset for tags of <a href=https://github.com/k8snetworkplumbingwg/multus-cni>k8snetworkplumbingwg/multus-cni</a> using multus_version."
  default     = null
}

variable "multus_install" {
  type        = bool
  description = "Whether to deploy Multus. See <a href=https://github.com/k8snetworkplumbingwg/multus-cni>k8snetworkplumbingwg/multus-cni</a>. NOTE: Provided only as a convenience and not supported by or sourced from Oracle - use at your own risk."
  default     = false
}

variable "multus_namespace" {
  type        = string
  description = "Kubernetes namespace for deployed resources."
  default     = "network"
}

variable "multus_version" {
  type        = string
  description = "Version of Multus to install. Ignored when an explicit value for multus_daemonset_url is provided."
  default     = "3.9.3"
}

variable "nat_gateway_public_ip_id" {
  type        = string
  description = "OCID of reserved IP address for NAT gateway. The reserved public IP address needs to be manually created."
  default     = null
}

variable "nat_gateway_route_rules" {
  type        = list(map(string))
  description = "(Updatable) List of routing rules to add to NAT Gateway Route Table."
  default     = null
}

variable "nat_route_table_id" {
  type        = string
  description = "Optional ID of existing NAT gateway in VCN."
  default     = null
}

variable "network_compartment_id" {
  type        = string
  description = "The compartment id where network resources will be created."
  default     = null
}

variable "network_defined_tags" {
  type        = map(string)
  description = "Defined tags applied to created resources."
  default     = {}
}

variable "network_freeform_tags" {
  type        = map(string)
  description = "Freeform tags applied to created resources."
  default     = {}
}

variable "nsgs" {
  type        = map(object({
    create = optional(string)
    id     = optional(string)
  }))
  description = "Configuration for standard network security groups (NSGs).  The 'create' parameter of each entry defaults to 'auto', creating NSGs when other enabled components are expected to utilize them, and may be configured with 'never' or 'always' to force disabled/enabled."
  default     = {
  "bastion": {},
  "cp": {},
  "int_lb": {},
  "operator": {},
  "pods": {},
  "pub_lb": {},
  "workers": {}
}
}

variable "ocir_email_address" {
  type        = string
  description = "The email address used for the Oracle Container Image Registry (OCIR)."
  default     = null
}

variable "ocir_secret_id" {
  type        = string
  description = "The OCI Vault secret ID for the OCIR authentication token."
  default     = null
}

variable "ocir_secret_name" {
  type        = string
  description = "The name of the Kubernetes secret to be created with the OCIR authentication token."
  default     = "ocirsecret"
}

variable "ocir_secret_namespace" {
  type        = string
  description = "The Kubernetes namespace in which to create the OCIR secret."
  default     = "default"
}

variable "ocir_username" {
  type        = string
  description = "A username with access to the OCI Vault secret for OCIR access. Required when 'ocir_secret_id' is provided."
  default     = null
}

variable "operator_availability_domain" {
  type        = string
  description = "The availability domain for FSS placement. Defaults to first available."
  default     = null
}

variable "operator_cloud_init" {
  type        = list(map(string))
  description = "List of maps containing cloud init MIME part configuration for operator host. See https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/cloudinit_config.html#part for expected schema of each element."
  default     = []
}

variable "operator_defined_tags" {
  type        = map(string)
  description = "Defined tags applied to created resources."
  default     = {}
}

variable "operator_freeform_tags" {
  type        = map(string)
  description = "Freeform tags applied to created resources."
  default     = {}
}

variable "operator_image_id" {
  type        = string
  description = "Image ID for created operator instance."
  default     = null
}

variable "operator_image_os" {
  type        = string
  description = "Operator image operating system name when operator_image_type = 'platform'."
  default     = "Oracle Linux"
}

variable "operator_image_os_version" {
  type        = string
  description = "Operator image operating system version when operator_image_type = 'platform'."
  default     = "8"
}

variable "operator_image_type" {
  type        = string
  description = "Whether to use a platform or custom image for the created operator instance. When custom is set, the operator_image_id must be specified."
  default     = "platform"
}

variable "operator_install_helm" {
  type        = bool
  description = "Whether to install Helm on the created operator host."
  default     = true
}

variable "operator_install_istioctl" {
  type        = bool
  description = "Whether to install istioctl on the created operator host."
  default     = false
}

variable "operator_install_k9s" {
  type        = bool
  description = "Whether to install k9s on the created operator host. NOTE: Provided only as a convenience and not supported by or sourced from Oracle - use at your own risk."
  default     = false
}

variable "operator_install_kubectl_from_repo" {
  type        = bool
  description = "Whether to install kubectl on the created operator host from olcne repo."
  default     = true
}

variable "operator_install_kubectx" {
  type        = bool
  description = "Whether to install kubectx/kubens on the created operator host. NOTE: Provided only as a convenience and not supported by or sourced from Oracle - use at your own risk."
  default     = true
}

variable "operator_nsg_ids" {
  type        = list(string)
  description = "An optional and updatable list of network security groups that the operator will be part of."
  default     = []
}

variable "operator_private_ip" {
  type        = string
  description = "The IP address of an existing operator host. Ignored when create_operator = true."
  default     = null
}

variable "operator_pv_transit_encryption" {
  type        = bool
  description = "Whether to enable in-transit encryption for the data volume's paravirtualized attachment."
  default     = false
}

variable "operator_shape" {
  type        = map(any)
  description = "Shape of the created operator instance."
  default     = {
  "boot_volume_size": 50,
  "memory": 4,
  "ocpus": 1,
  "shape": "VM.Standard.E4.Flex"
}
}

variable "operator_upgrade" {
  type        = bool
  description = "Whether to upgrade operator packages after provisioning."
  default     = false
}

variable "operator_user" {
  type        = string
  description = "User for SSH access to operator host."
  default     = "opc"
}

variable "operator_volume_kms_key_id" {
  type        = string
  description = "The OCID of the OCI KMS key to assign as the master encryption key for the boot volume."
  default     = null
}

variable "output_detail" {
  type        = bool
  description = "Whether to include detailed output in state."
  default     = false
}

variable "persistent_volume_defined_tags" {
  type        = map(string)
  description = "Defined tags applied to created resources."
  default     = {}
}

variable "persistent_volume_freeform_tags" {
  type        = map(string)
  description = "Freeform tags applied to created resources."
  default     = {}
}

variable "platform_config" {
  type        = object({
    type                                           = optional(string),
    are_virtual_instructions_enabled               = optional(bool),
    is_access_control_service_enabled              = optional(bool),
    is_input_output_memory_management_unit_enabled = optional(bool),
    is_measured_boot_enabled                       = optional(bool),
    is_memory_encryption_enabled                   = optional(bool),
    is_secure_boot_enabled                         = optional(bool),
    is_symmetric_multi_threading_enabled           = optional(bool),
    is_trusted_platform_module_enabled             = optional(bool),
    numa_nodes_per_socket                          = optional(number),
    percentage_of_cores_enabled                    = optional(bool),
  })
  description = "Default platform_config for self-managed worker pools created with mode: 'instance', 'instance-pool', or 'cluster-network'. See <a href=https://docs.oracle.com/en-us/iaas/api/#/en/iaas/20160918/datatypes/PlatformConfig>PlatformConfig</a> for more information."
  default     = null
}

variable "pod_nsg_ids" {
  type        = list(string)
  description = "An additional list of network security group (NSG) IDs for pod security. Combined with 'pod_nsg_ids' specified on each pool."
  default     = []
}

variable "pods_cidr" {
  type        = string
  description = "The CIDR range used for IP addresses by the pods. A /16 CIDR is generally sufficient. This CIDR should not overlap with any subnet range in the VCN (it can also be outside the VCN CIDR range). Ignored when cni_type = 'npn'."
  default     = "10.244.0.0/16"
}

variable "preferred_load_balancer" {
  type        = string
  description = "The preferred load balancer subnets that OKE will automatically choose when creating a load balancer. Valid values are 'public' or 'internal'. If 'public' is chosen, the value for load_balancers must be either 'public' or 'both'. If 'private' is chosen, the value for load_balancers must be either 'internal' or 'both'. NOTE: Service annotations for internal load balancers must still be specified regardless of this setting. See <a href=https://github.com/oracle/oci-cloud-controller-manager/blob/master/docs/load-balancer-annotations.md>Load Balancer Annotations</a> for more information."
  default     = "public"
}

variable "prometheus_helm_values" {
  type        = map(string)
  description = "Map of individual Helm chart values. See <a href=https://registry.terraform.io/providers/hashicorp/helm/latest/docs/data-sources/template>data.helm_template</a>."
  default     = {}
}

variable "prometheus_helm_values_files" {
  type        = list(string)
  description = "Paths to a local YAML files with Helm chart values (as with `helm install -f` which supports multiple). Generate with defaults using `helm show values [CHART] [flags]`."
  default     = []
}

variable "prometheus_helm_version" {
  type        = string
  description = "Version of the Helm chart to install. List available releases using `helm search repo [keyword] --versions`."
  default     = "45.2.0"
}

variable "prometheus_install" {
  type        = bool
  description = "Whether to deploy the Prometheus Helm chart. See https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack. NOTE: Provided only as a convenience and not supported by or sourced from Oracle - use at your own risk."
  default     = false
}

variable "prometheus_namespace" {
  type        = string
  description = "Kubernetes namespace for deployed resources."
  default     = "metrics"
}

variable "prometheus_reapply" {
  type        = bool
  description = "Whether to force reapply of the Prometheus Helm chart when no changes are detected, e.g. with state modified externally."
  default     = false
}

variable "rdma_cni_plugin_daemonset_url" {
  type        = string
  description = "The URL path to the manifest. Leave unset for tags of <a href=https://github.com/openshift/sriov-cni</a> using rdma_cni_plugin_version."
  default     = null
}

variable "rdma_cni_plugin_install" {
  type        = bool
  description = "Whether to deploy the SR-IOV CNI Plugin. See <a href=https://github.com/openshift/sriov-cni</a>. NOTE: Provided only as a convenience and not supported by or sourced from Oracle - use at your own risk."
  default     = false
}

variable "rdma_cni_plugin_namespace" {
  type        = string
  description = "Kubernetes namespace for deployed resources."
  default     = "network"
}

variable "rdma_cni_plugin_version" {
  type        = string
  description = "Version to install. Ignored when an explicit value for rdma_cni_plugin_daemonset_url is provided."
  default     = "master"
}

variable "region" {
  type        = string
  description = "The OCI region where OKE resources will be created."
  default     = "us-ashburn-1"
}

variable "remote_peering_connections" {
  type        = map(any)
  description = "Map of parameters to add and optionally to peer to remote peering connections. Key-only items represent local acceptors and no peering attempted; items containing key and values represent local requestor and must have the OCID and region of the remote acceptor to peer to"
  default     = {}
}

variable "service_accounts" {
  type        = map(any)
  description = "Map of service accounts and associated parameters."
  default     = {
  "kubeconfigsa": {
    "sa_cluster_role": "cluster-admin",
    "sa_cluster_role_binding": "kubeconfigsa-crb",
    "sa_name": "kubeconfigsa",
    "sa_namespace": "kube-system"
  }
}
}

variable "service_lb_defined_tags" {
  type        = map(string)
  description = "Defined tags applied to created resources."
  default     = {}
}

variable "service_lb_freeform_tags" {
  type        = map(string)
  description = "Freeform tags applied to created resources."
  default     = {}
}

variable "services_cidr" {
  type        = string
  description = "The CIDR range used within the cluster by Kubernetes services (ClusterIPs). This CIDR should not overlap with the VCN CIDR range."
  default     = "10.96.0.0/16"
}

variable "sriov_cni_plugin_daemonset_url" {
  type        = string
  description = "The URL path to the manifest. Leave unset for tags of <a href=https://github.com/openshift/sriov-cni</a> using sriov_cni_plugin_version."
  default     = null
}

variable "sriov_cni_plugin_install" {
  type        = bool
  description = "Whether to deploy the SR-IOV CNI Plugin. See <a href=https://github.com/openshift/sriov-cni</a>. NOTE: Provided only as a convenience and not supported by or sourced from Oracle - use at your own risk."
  default     = false
}

variable "sriov_cni_plugin_namespace" {
  type        = string
  description = "Kubernetes namespace for deployed resources."
  default     = "network"
}

variable "sriov_cni_plugin_version" {
  type        = string
  description = "Version to install. Ignored when an explicit value for sriov_cni_plugin_daemonset_url is provided."
  default     = "master"
}

variable "sriov_device_plugin_daemonset_url" {
  type        = string
  description = "The URL path to the manifest. Leave unset for tags of <a href=https://github.com/k8snetworkplumbingwg/sriov-network-device-plugin>k8snetworkplumbingwg/sriov-network-device-plugin</a> using sriov_device_plugin_version."
  default     = null
}

variable "sriov_device_plugin_install" {
  type        = bool
  description = "Whether to deploy the SR-IOV Network Device Plugin. See <a href=https://github.com/k8snetworkplumbingwg/sriov-network-device-plugin>k8snetworkplumbingwg/sriov-network-device-plugin</a>. NOTE: Provided only as a convenience and not supported by or sourced from Oracle - use at your own risk."
  default     = false
}

variable "sriov_device_plugin_namespace" {
  type        = string
  description = "Kubernetes namespace for deployed resources."
  default     = "network"
}

variable "sriov_device_plugin_version" {
  type        = string
  description = "Version to install. Ignored when an explicit value for sriov_device_plugin_daemonset_url is provided."
  default     = "master"
}

variable "ssh_private_key" {
  type        = string
  description = "The contents of the SSH private key file, optionally base64-encoded. May be provided via SSH agent when unset."
  default     = null
}

variable "ssh_private_key_path" {
  type        = string
  description = "A path on the local filesystem to the SSH private key. May be provided via SSH agent when unset."
  default     = null
}

variable "ssh_public_key" {
  type        = string
  description = "The contents of the SSH public key file, optionally base64-encoded. Used to allow login for workers/bastion/operator with corresponding private key."
  default     = null
}

variable "ssh_public_key_path" {
  type        = string
  description = "A path on the local filesystem to the SSH public key. Used to allow login for workers/bastion/operator with corresponding private key."
  default     = null
}

variable "state_id" {
  type        = string
  description = "Optional Terraform state_id from an existing deployment of the module to re-use with created resources."
  default     = null
}

variable "subnets" {
  type        = map(object({
    create    = optional(string)
    id        = optional(string)
    newbits   = optional(string)
    netnum    = optional(string)
    cidr      = optional(string)
    dns_label = optional(string)
  }))
  description = "Configuration for standard subnets. The 'create' parameter of each entry defaults to 'auto', creating subnets when other enabled components are expected to utilize them, and may be configured with 'never' or 'always' to force disabled/enabled."
  default     = {
  "bastion": {
    "newbits": 13
  },
  "cp": {
    "newbits": 13
  },
  "int_lb": {
    "newbits": 11
  },
  "operator": {
    "newbits": 13
  },
  "pods": {
    "newbits": 2
  },
  "pub_lb": {
    "newbits": 11
  },
  "workers": {
    "newbits": 4
  }
}
}

variable "tag_namespace" {
  type        = string
  description = "The tag namespace for standard OKE defined tags."
  default     = "oke"
}

variable "tenancy_id" {
  type        = string
  description = "The tenancy id of the OCI Cloud Account in which to create the resources."
  default     = null
}

variable "tenancy_ocid" {
  type        = string
  description = "A tenancy OCID automatically populated by Resource Manager."
  default     = null
}

variable "timezone" {
  type        = string
  description = "The preferred timezone for workers, operator, and bastion instances."
  default     = "Etc/UTC"
}

variable "use_defined_tags" {
  type        = bool
  description = "Whether to apply defined tags to created resources for IAM policy and tracking."
  default     = false
}

variable "use_signed_images" {
  type        = bool
  description = "Whether to enforce the use of signed images. If set to true, at least 1 RSA key must be provided through image_signing_keys."
  default     = false
}

variable "user_id" {
  type        = string
  description = "The id of the user that terraform will use to create the resources."
  default     = null
}

variable "vcn_cidrs" {
  type        = list(string)
  description = "The list of IPv4 CIDR blocks the VCN will use."
  default     = [
  "10.0.0.0/16"
]
}

variable "vcn_create_internet_gateway" {
  type        = string
  description = "Whether to create an internet gateway with the VCN. Defaults to automatic creation when public network resources are expected to utilize it."
  default     = "auto"
}

variable "vcn_create_nat_gateway" {
  type        = string
  description = "Whether to create a NAT gateway with the VCN. Defaults to automatic creation when private network resources are expected to utilize it."
  default     = "auto"
}

variable "vcn_create_service_gateway" {
  type        = string
  description = "Whether to create a service gateway with the VCN. Defaults to always created."
  default     = "always"
}

variable "vcn_dns_label" {
  type        = string
  description = "A DNS label for the VCN, used in conjunction with the VNIC's hostname and subnet's DNS label to form a fully qualified domain name (FQDN) for each VNIC within this subnet. Defaults to the generated Terraform 'state_id' value."
  default     = null
}

variable "vcn_id" {
  type        = string
  description = "Optional ID of existing VCN. Takes priority over vcn_name filter. Ignored when `create_vcn = true`."
  default     = null
}

variable "vcn_name" {
  type        = string
  description = "Display name for the created VCN. Defaults to 'oke' suffixed with the generated Terraform 'state_id' value."
  default     = null
}

variable "whereabouts_daemonset_url" {
  type        = string
  description = "The URL path to the manifest. Leave unset for tags of <a href=https://github.com/k8snetworkplumbingwg/whereabouts>k8snetworkplumbingwg/whereabouts</a> using whereabouts_version."
  default     = null
}

variable "whereabouts_install" {
  type        = bool
  description = "Whether to deploy the MPI Operator. See <a href=https://github.com/k8snetworkplumbingwg/whereabouts>k8snetworkplumbingwg/whereabouts</a>. NOTE: Provided only as a convenience and not supported by or sourced from Oracle - use at your own risk."
  default     = false
}

variable "whereabouts_namespace" {
  type        = string
  description = "Kubernetes namespace for deployed resources."
  default     = "default"
}

variable "whereabouts_version" {
  type        = string
  description = "Version to install. Ignored when an explicit value for whereabouts_daemonset_url is provided."
  default     = "master"
}

variable "worker_block_volume_type" {
  type        = string
  description = "Default block volume attachment type for Instance Configurations when unspecified on a pool."
  default     = "paravirtualized"
}

variable "worker_capacity_reservation_id" {
  type        = string
  description = "The ID of the Compute capacity reservation the worker node will be launched under. See <a href=https://docs.oracle.com/en-us/iaas/Content/Compute/Tasks/reserve-capacity.htm>Capacity Reservations</a> for more information."
  default     = null
}

variable "worker_cloud_init" {
  type        = list(map(string))
  description = "List of maps containing cloud init MIME part configuration for worker nodes. Merged with pool-specific definitions. See https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/cloudinit_config.html#part for expected schema of each element."
  default     = []
}

variable "worker_compartment_id" {
  type        = string
  description = "The compartment id where worker group resources will be created."
  default     = null
}

variable "worker_disable_default_cloud_init" {
  type        = bool
  description = "Whether to disable the default OKE cloud init and only use the cloud init explicitly passed to the worker pool in 'worker_cloud_init'."
  default     = false
}

variable "worker_drain_delete_local_data" {
  type        = bool
  description = "Whether to accept removal of data stored locally on draining worker pools. See <a href=https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#drain>kubectl drain</a> for more information."
  default     = true
}

variable "worker_drain_ignore_daemonsets" {
  type        = bool
  description = "Whether to ignore DaemonSet-managed Pods when draining worker pools. See <a href=https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#drain>kubectl drain</a> for more information."
  default     = true
}

variable "worker_drain_timeout_seconds" {
  type        = number
  description = "The length of time to wait before giving up on draining nodes in a pool. See <a href=https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#drain>kubectl drain</a> for more information."
  default     = 900
}

variable "worker_image_id" {
  type        = string
  description = "Default image for worker pools  when unspecified on a pool."
  default     = null
}

variable "worker_image_os" {
  type        = string
  description = "Default worker image operating system name when worker_image_type = 'oke' or 'platform' and unspecified on a pool."
  default     = "Oracle Linux"
}

variable "worker_image_os_version" {
  type        = string
  description = "Default worker image operating system version when worker_image_type = 'oke' or 'platform' and unspecified on a pool."
  default     = "8"
}

variable "worker_image_type" {
  type        = string
  description = "Whether to use a platform, OKE, or custom image for worker nodes by default when unspecified on a pool. When custom is set, the worker_image_id must be specified."
  default     = "oke"
}

variable "worker_is_public" {
  type        = bool
  description = "Whether to provision workers with public IPs allocated by default when unspecified on a pool."
  default     = false
}

variable "worker_node_labels" {
  type        = map(string)
  description = "Default worker node labels. Merged with labels defined on each pool."
  default     = {}
}

variable "worker_node_metadata" {
  type        = map(string)
  description = "Map of additional worker node instance metadata. Merged with metadata defined on each pool."
  default     = {}
}

variable "worker_nsg_ids" {
  type        = list(string)
  description = "An additional list of network security group (NSG) IDs for node security. Combined with 'nsg_ids' specified on each pool."
  default     = []
}

variable "worker_pool_mode" {
  type        = string
  description = "Default management mode for workers when unspecified on a pool."
  default     = "node-pool"
}

variable "worker_pool_size" {
  type        = number
  description = "Default size for worker pools when unspecified on a pool."
  default     = 0
}

variable "worker_pools" {
  type        = any
  description = "Tuple of OKE worker pools where each key maps to the OCID of an OCI resource, and value contains its definition."
  default     = {}
}

variable "worker_preemptible_config" {
  type        = map(any)
  description = "Default preemptible Compute configuration when unspecified on a pool. See <a href=https://docs.oracle.com/en-us/iaas/Content/ContEng/Tasks/contengusingpreemptiblecapacity.htm>Preemptible Worker Nodes</a> for more information."
  default     = {
  "enable": false,
  "is_preserve_boot_volume": false
}
}

variable "worker_pv_transit_encryption" {
  type        = bool
  description = "Whether to enable in-transit encryption for the data volume's paravirtualized attachment by default when unspecified on a pool."
  default     = false
}

variable "worker_shape" {
  type        = map(any)
  description = "Default shape of the created worker instance when unspecified on a pool."
  default     = {
  "boot_volume_size": 50,
  "boot_volume_vpus_per_gb": 10,
  "memory": 16,
  "ocpus": 2,
  "shape": "VM.Standard.E4.Flex"
}
}

variable "worker_volume_kms_key_id" {
  type        = string
  description = "The ID of the OCI KMS key to be used as the master encryption key for Boot Volume and Block Volume encryption by default when unspecified on a pool."
  default     = null
}

variable "workers_defined_tags" {
  type        = map(string)
  description = "Defined tags applied to created resources."
  default     = {}
}

variable "workers_freeform_tags" {
  type        = map(string)
  description = "Freeform tags applied to created resources."
  default     = {}
}


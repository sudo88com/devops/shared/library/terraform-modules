
variable "assign_dns" {
  type        = bool
  description = "Whether to assign DNS records to created instances"
  default     = true
}

variable "availability_domain" {
  type        = number
  description = "the AD to place the bastion host"
  default     = 1
}

variable "bastion_access" {
  type        = list
  description = "A list of CIDR blocks to which ssh access to the bastion must be restricted to. *anywhere* is equivalent to 0.0.0.0/0 and allows ssh access from anywhere."
  default     = [
  "anywhere"
]
}

variable "bastion_image_id" {
  type        = string
  description = "Provide a custom image id for the bastion host or leave as Autonomous."
  default     = "Autonomous"
}

variable "bastion_notification_endpoint" {
  type        = string
  description = "The subscription notification endpoint. Email address to be notified."
  default     = null
}

variable "bastion_notification_protocol" {
  type        = string
  description = "The notification protocol used."
  default     = "EMAIL"
}

variable "bastion_notification_topic" {
  type        = string
  description = "The name of the notification topic"
  default     = "bastion"
}

variable "bastion_os_version" {
  type        = string
  description = "In case Autonomous Linux is used, allow specification of Autonomous version"
  default     = "7.9"
}

variable "bastion_shape" {
  type        = map(any)
  description = "The shape of bastion instance."
  default     = {
  "boot_volume_size": 50,
  "memory": 4,
  "ocpus": 1,
  "shape": "VM.Standard.E4.Flex"
}
}

variable "bastion_state" {
  type        = string
  description = "The target state for the instance. Could be set to RUNNING or STOPPED. (Updatable)"
  default     = "RUNNING"
}

variable "bastion_timezone" {
  type        = string
  description = "The preferred timezone for the bastion host."
  default     = "Australia/Sydney"
}

variable "bastion_type" {
  type        = string
  description = "Whether to make the bastion host public or private."
  default     = "public"
}

variable "compartment_id" {
  type        = string
  description = "compartment id where to create all resources"
  default     = ""
}

variable "enable_bastion_notification" {
  type        = bool
  description = "Whether to enable ONS notification for the bastion host."
  default     = false
}

variable "freeform_tags" {
  type        = map(any)
  description = "Freeform tags for bastion"
  default     = {
  "access": "public",
  "environment": "dev",
  "role": "bastion"
}
}

variable "ig_route_id" {
  type        = string
  description = "the route id to the internet gateway"
  default     = ""
}

variable "label_prefix" {
  type        = string
  description = "a string that will be prepended to all resources"
  default     = "none"
}

variable "netnum" {
  type        = number
  description = "0-based index of the bastion subnet when the VCN's CIDR is masked with the corresponding newbit value."
  default     = 0
}

variable "newbits" {
  type        = number
  description = "The difference between the VCN's netmask and the desired bastion subnet mask"
  default     = 14
}

variable "ssh_public_key" {
  type        = string
  description = "the content of the ssh public key used to access the bastion. set this or the ssh_public_key_path"
  default     = ""
}

variable "ssh_public_key_path" {
  type        = string
  description = "path to the ssh public key used to access the bastion. set this or the ssh_public_key"
  default     = ""
}

variable "tenancy_id" {
  type        = string
  description = "tenancy id where to create the sources"
  default     = ""
}

variable "upgrade_bastion" {
  type        = bool
  description = "Whether to upgrade the bastion host packages after provisioning. It's useful to set this to false during development/testing so the bastion is provisioned faster."
  default     = false
}

variable "vcn_id" {
  type        = string
  description = "The id of the VCN to use when creating the bastion resources."
  default     = ""
}



ad_number = null

assign_public_ip = false

attachment_type = "paravirtualized"

baseline_ocpu_utilization = "BASELINE_1_1"

block_storage_sizes_in_gbs = []

boot_volume_backup_policy = "disabled"

boot_volume_size_in_gbs = null

cloud_agent_plugins = {
  "autonomous_linux": "ENABLED",
  "bastion": "ENABLED",
  "block_volume_mgmt": "DISABLED",
  "custom_logs": "ENABLED",
  "java_management_service": "DISABLED",
  "management": "DISABLED",
  "monitoring": "ENABLED",
  "osms": "ENABLED",
  "run_command": "ENABLED",
  "vulnerability_scanning": "ENABLED"
}

compartment_ocid = 

defined_tags = null

extended_metadata = {}

freeform_tags = null

hostname_label = ""

instance_count = 1

instance_display_name = ""

instance_flex_memory_in_gbs = null

instance_flex_ocpus = null

instance_state = "RUNNING"

instance_timeout = "25m"

ipxe_script = null

preserve_boot_volume = false

primary_vnic_nsg_ids = null

private_ips = []

public_ip = "NONE"

public_ip_display_name = null

resource_platform = "linux"

shape = "VM.Standard2.1"

skip_source_dest_check = false

source_ocid = 

source_type = "image"

ssh_authorized_keys = null

ssh_public_keys = null

subnet_ocids = 

use_chap = false

user_data = null

vnic_name = ""


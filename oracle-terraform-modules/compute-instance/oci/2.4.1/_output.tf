
output "instance_all_attributes" {
  description = "all attributes of created instance"
  value       = module.compute-instance.instance_all_attributes
}

output "instance_id" {
  description = "ocid of created instances. "
  value       = module.compute-instance.instance_id
}

output "instance_password" {
  description = "Passwords to login to Windows instance. "
  value       = module.compute-instance.instance_password
}

output "instance_username" {
  description = "Usernames to login to Windows instance. "
  value       = module.compute-instance.instance_username
}

output "instances_summary" {
  description = "Private and Public IPs for each instance."
  value       = module.compute-instance.instances_summary
}

output "private_ip" {
  description = "Private IPs of created instances. "
  value       = module.compute-instance.private_ip
}

output "private_ips_all_attributes" {
  description = "all attributes of created private ips"
  value       = module.compute-instance.private_ips_all_attributes
}

output "public_ip" {
  description = "Public IPs of created instances. "
  value       = module.compute-instance.public_ip
}

output "public_ip_all_attributes" {
  description = "all attributes of created public ip"
  value       = module.compute-instance.public_ip_all_attributes
}

output "vnic_attachment_all_attributes" {
  description = "all attributes of created vnic attachments"
  value       = module.compute-instance.vnic_attachment_all_attributes
}

output "volume_all_attributes" {
  description = "all attributes of created volumes"
  value       = module.compute-instance.volume_all_attributes
}

output "volume_attachment_all_attributes" {
  description = "all attributes of created volumes attachments"
  value       = module.compute-instance.volume_attachment_all_attributes
}


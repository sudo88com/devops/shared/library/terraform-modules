
attached_drg_id = null

compartment_id = 

create_internet_gateway = false

create_nat_gateway = false

create_service_gateway = false

defined_tags = null

enable_ipv6 = false

freeform_tags = {
  "module": "oracle-terraform-modules/vcn/oci",
  "terraformed": "Please do not edit manually"
}

internet_gateway_display_name = "internet-gateway"

internet_gateway_route_rules = null

label_prefix = "none"

local_peering_gateways = null

lockdown_default_seclist = true

nat_gateway_display_name = "nat-gateway"

nat_gateway_public_ip_id = "none"

nat_gateway_route_rules = null

region = null

service_gateway_display_name = "service-gateway"

subnets = {}

vcn_cidrs = [
  "10.0.0.0/16"
]

vcn_dns_label = "vcnmodule"

vcn_name = "vcn"



output "default_security_list_id" {
  description = ""
  value       = module.vcn.default_security_list_id
}

output "ig_route_all_attributes" {
  description = "all attributes of created ig route table"
  value       = module.vcn.ig_route_all_attributes
}

output "ig_route_id" {
  description = "id of internet gateway route table"
  value       = module.vcn.ig_route_id
}

output "internet_gateway_all_attributes" {
  description = "all attributes of created internet gateway"
  value       = module.vcn.internet_gateway_all_attributes
}

output "internet_gateway_id" {
  description = "id of internet gateway if it is created"
  value       = module.vcn.internet_gateway_id
}

output "lpg_all_attributes" {
  description = "all attributes of created lpg"
  value       = module.vcn.lpg_all_attributes
}

output "nat_gateway_all_attributes" {
  description = "all attributes of created nat gateway"
  value       = module.vcn.nat_gateway_all_attributes
}

output "nat_gateway_id" {
  description = "id of nat gateway if it is created"
  value       = module.vcn.nat_gateway_id
}

output "nat_route_all_attributes" {
  description = "all attributes of created nat gateway route table"
  value       = module.vcn.nat_route_all_attributes
}

output "nat_route_id" {
  description = "id of VCN NAT gateway route table"
  value       = module.vcn.nat_route_id
}

output "service_gateway_all_attributes" {
  description = "all attributes of created service gateway"
  value       = module.vcn.service_gateway_all_attributes
}

output "service_gateway_id" {
  description = "id of service gateway if it is created"
  value       = module.vcn.service_gateway_id
}

output "sgw_route_id" {
  description = "id of VCN Service gateway route table"
  value       = module.vcn.sgw_route_id
}

output "subnet_all_attributes" {
  description = ""
  value       = module.vcn.subnet_all_attributes
}

output "subnet_id" {
  description = ""
  value       = module.vcn.subnet_id
}

output "vcn_all_attributes" {
  description = "all attributes of created vcn"
  value       = module.vcn.vcn_all_attributes
}

output "vcn_id" {
  description = "id of vcn that is created"
  value       = module.vcn.vcn_id
}



assign_dns = true

availability_domain = 1

boot_volume_encryption_key = ""

compartment_id = 

enable_operator_instance_principal = false

enable_operator_notification = false

enable_pv_encryption_in_transit = false

freeform_tags = {
  "access": "restricted",
  "environment": "dev",
  "role": "operator"
}

label_prefix = "none"

nat_route_id = 

netnum = 33

newbits = 13

nsg_ids = []

operator_image_id = "Oracle"

operator_notification_endpoint = null

operator_notification_protocol = "EMAIL"

operator_notification_topic = "operator"

operator_os_version = "8"

operator_shape = {
  "boot_volume_size": 50,
  "memory": 4,
  "ocpus": 1,
  "shape": "VM.Standard.E4.Flex"
}

operator_state = "RUNNING"

operator_timezone = "Australia/Sydney"

ssh_public_key = ""

ssh_public_key_path = ""

tenancy_id = ""

upgrade_operator = false

vcn_id = 



variable "assign_dns" {
  type        = bool
  description = "Whether to assign DNS records for operator subnet"
  default     = true
}

variable "availability_domain" {
  type        = number
  description = "the AD to place the operator host"
  default     = 1
}

variable "boot_volume_encryption_key" {
  type        = string
  description = "The OCID of the OCI KMS key to assign as the master encryption key for the boot volume."
  default     = ""
}

variable "compartment_id" {
  type        = string
  description = "compartment id where to create all resources"
  default     = ""
}

variable "enable_operator_instance_principal" {
  type        = bool
  description = "whether to enable instance_principal on the operator"
  default     = false
}

variable "enable_operator_notification" {
  type        = bool
  description = "Whether to enable ONS notification for the operator host."
  default     = false
}

variable "enable_pv_encryption_in_transit" {
  type        = bool
  description = "Whether to enable in-transit encryption for the data volume's paravirtualized attachment. The default value is false"
  default     = false
}

variable "freeform_tags" {
  type        = map(any)
  description = "Freeform tags for operator"
  default     = {
  "access": "restricted",
  "environment": "dev",
  "role": "operator"
}
}

variable "label_prefix" {
  type        = string
  description = "a string that will be prepended to all resources"
  default     = "none"
}

variable "nat_route_id" {
  type        = string
  description = "the id of the route table to the nat gateway."
  default     = ""
}

variable "netnum" {
  type        = number
  description = "0-based index of the operator subnet when the VCN's CIDR is masked with the corresponding newbit value."
  default     = 33
}

variable "newbits" {
  type        = number
  description = "The difference between the VCN's netmask and the desired operator subnet mask"
  default     = 13
}

variable "nsg_ids" {
  type        = list(string)
  description = "Optional list of network security groups that the operator will be part of"
  default     = []
}

variable "operator_image_id" {
  type        = string
  description = "Provide a custom image id for the operator host or leave as Oracle."
  default     = "Oracle"
}

variable "operator_notification_endpoint" {
  type        = string
  description = "The subscription notification endpoint. Email address to be notified."
  default     = null
}

variable "operator_notification_protocol" {
  type        = string
  description = "The notification protocol used."
  default     = "EMAIL"
}

variable "operator_notification_topic" {
  type        = string
  description = "The name of the notification topic"
  default     = "operator"
}

variable "operator_os_version" {
  type        = string
  description = "The version of the Oracle Linux to use."
  default     = "8"
}

variable "operator_shape" {
  type        = map(any)
  description = "The shape of the operator instance."
  default     = {
  "boot_volume_size": 50,
  "memory": 4,
  "ocpus": 1,
  "shape": "VM.Standard.E4.Flex"
}
}

variable "operator_state" {
  type        = string
  description = "The target state for the instance. Could be set to RUNNING or STOPPED. (Updatable)"
  default     = "RUNNING"
}

variable "operator_timezone" {
  type        = string
  description = "The preferred timezone for the operator host."
  default     = "Australia/Sydney"
}

variable "ssh_public_key" {
  type        = string
  description = "the content of the ssh public key used to access the operator. set this or the ssh_public_key_path"
  default     = ""
}

variable "ssh_public_key_path" {
  type        = string
  description = "path to the ssh public key used to access the operator. set this or the ssh_public_key"
  default     = ""
}

variable "tenancy_id" {
  type        = string
  description = "tenancy id where to create the sources"
  default     = ""
}

variable "upgrade_operator" {
  type        = bool
  description = "Whether to upgrade the operator host packages after provisioning. It's useful to set this to false during development/testing so the operator is provisioned faster."
  default     = false
}

variable "vcn_id" {
  type        = string
  description = "The id of the VCN to use when creating the operator resources."
  default     = ""
}


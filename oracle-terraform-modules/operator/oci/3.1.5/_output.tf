
output "operator_instance_principal_group_name" {
  description = ""
  value       = module.operator.operator_instance_principal_group_name
}

output "operator_nsg_id" {
  description = ""
  value       = module.operator.operator_nsg_id
}

output "operator_private_ip" {
  description = ""
  value       = module.operator.operator_private_ip
}

output "operator_subnet_id" {
  description = ""
  value       = module.operator.operator_subnet_id
}



variable "compartment_ocid" {
  type        = 
  description = "Compartment's OCID where VCN will be created. "
  default     = ""
}

variable "subnet_dns_label" {
  type        = 
  description = "A DNS label prefix for the subnet, used in conjunction with the VNIC's hostname and VCN's DNS label to form a fully qualified domain name (FQDN) for each VNIC within this subnet. "
  default     = "subnet"
}

variable "vcn_cidr" {
  type        = 
  description = "A VCN covers a single, contiguous IPv4 CIDR block of your choice. "
  default     = "10.0.0.0/16"
}

variable "vcn_display_name" {
  type        = 
  description = "Name of Virtual Cloud Network. "
  default     = ""
}

variable "vcn_dns_label" {
  type        = 
  description = "A DNS label for the VCN, used in conjunction with the VNIC's hostname and subnet's DNS label to form a fully qualified domain name (FQDN) for each VNIC within this subnet. "
  default     = "vcn"
}


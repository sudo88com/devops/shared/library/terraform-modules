
output "default_dhcp_options_id" {
  description = "ocid of default DHCP options. "
  value       = module.default-vcn.default_dhcp_options_id
}

output "default_route_table_id" {
  description = "ocid of default route table. "
  value       = module.default-vcn.default_route_table_id
}

output "default_security_list_id" {
  description = "ocid of default security list. "
  value       = module.default-vcn.default_security_list_id
}

output "internet_gateway_id" {
  description = "ocid of internet gateway. "
  value       = module.default-vcn.internet_gateway_id
}

output "subnet_ids" {
  description = "ocid of subnet ids. "
  value       = module.default-vcn.subnet_ids
}

output "vcn_id" {
  description = "ocid of created VCN. "
  value       = module.default-vcn.vcn_id
}


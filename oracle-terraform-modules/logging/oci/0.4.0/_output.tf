
output "apigw_accesslogid" {
  description = "APIGateway access logs id"
  value       = module.logging.apigw_accesslogid
}

output "apigw_execlogid" {
  description = "APIGateway execution logs id"
  value       = module.logging.apigw_execlogid
}

output "apigw_loggroupid" {
  description = "APIGateway loggroup id"
  value       = module.logging.apigw_loggroupid
}

output "apm_droppeddatalogid" {
  description = "APM Dropped data logs id"
  value       = module.logging.apm_droppeddatalogid
}

output "apm_loggroupid" {
  description = "APM loggroup id"
  value       = module.logging.apm_loggroupid
}

output "devops_loggroupid" {
  description = "Devops loggroup id"
  value       = module.logging.devops_loggroupid
}

output "devops_logid" {
  description = "Devops logs id"
  value       = module.logging.devops_logid
}

output "email_acceptlogid" {
  description = "Email logs id"
  value       = module.logging.email_acceptlogid
}

output "email_loggroupid" {
  description = "Email loggroup id"
  value       = module.logging.email_loggroupid
}

output "email_relaylogid" {
  description = "Email logs id"
  value       = module.logging.email_relaylogid
}

output "event_loggroupid" {
  description = "Event loggroup id"
  value       = module.logging.event_loggroupid
}

output "event_logid" {
  description = "Event logs id"
  value       = module.logging.event_logid
}

output "func_loggroupid" {
  description = "Function loggroup id"
  value       = module.logging.func_loggroupid
}

output "func_logid" {
  description = "Function logs id"
  value       = module.logging.func_logid
}

output "int_loggroupid" {
  description = "Integration loggroup id"
  value       = module.logging.int_loggroupid
}

output "int_logid" {
  description = "Integration logs id"
  value       = module.logging.int_logid
}

output "lb_accesslogid" {
  description = "Loadbalancer access logs id"
  value       = module.logging.lb_accesslogid
}

output "lb_errorlogid" {
  description = "Loadbalancer error logs id"
  value       = module.logging.lb_errorlogid
}

output "lb_loggroupid" {
  description = "Loadbalancer loggroup id"
  value       = module.logging.lb_loggroupid
}

output "linux_loggroupid" {
  description = "Custom Linux loggroup id"
  value       = module.logging.linux_loggroupid
}

output "linux_logid" {
  description = "Custom Linux logs id"
  value       = module.logging.linux_logid
}

output "media_executionlogid" {
  description = "MediaFlow execution logs id"
  value       = module.logging.media_executionlogid
}

output "media_loggroupid" {
  description = "MediaFlow loggroup id"
  value       = module.logging.media_loggroupid
}

output "os_loggroupid" {
  description = "ObjectStorage loggroup id"
  value       = module.logging.os_loggroupid
}

output "os_readlogid" {
  description = "ObjectStorage read logs id"
  value       = module.logging.os_readlogid
}

output "os_writelogid" {
  description = "ObjectStorage write logs id"
  value       = module.logging.os_writelogid
}

output "vcn_loggroupid" {
  description = "VCN subnet flowlogs loggroup id"
  value       = module.logging.vcn_loggroupid
}

output "vcn_logid" {
  description = "VCN subnet flowlogs log id"
  value       = module.logging.vcn_logid
}

output "vpn_loggroupid" {
  description = "VPN IPSEC loggroup id"
  value       = module.logging.vpn_loggroupid
}

output "vpn_logid" {
  description = "VPN  IPSEC read logs id"
  value       = module.logging.vpn_logid
}

output "waf_loggroupid" {
  description = "WAF loggroup id"
  value       = module.logging.waf_loggroupid
}

output "waf_logid" {
  description = "WAF logs id"
  value       = module.logging.waf_logid
}

output "windows_loggroupid" {
  description = "Custom Windows loggroup id"
  value       = module.logging.windows_loggroupid
}

output "windows_logid" {
  description = "Custom Windows logs id"
  value       = module.logging.windows_logid
}



module "logging" {
  source = "terraform-aws-modules/logging/aws"
  version = "0.4.0"
  compartment_id = var.compartment_id
  label_prefix = var.label_prefix
  linux_logdef = var.linux_logdef
  log_retention_duration = var.log_retention_duration
  loggroup_tags = var.loggroup_tags
  service_logdef = var.service_logdef
  tenancy_id = var.tenancy_id
  vcn_id = var.vcn_id
  windows_logdef = var.windows_logdef
}

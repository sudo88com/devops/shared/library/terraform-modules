
variable "compartment_id" {
  type        = string
  description = "compartment id where to create the resources"
  default     = ""
}

variable "label_prefix" {
  type        = string
  description = "A string that will be prepended to log resources."
  default     = "none"
}

variable "linux_logdef" {
  type        = map(any)
  description = "Custom Linux Operating System Log Definition"
  default     = {}
}

variable "log_retention_duration" {
  type        = number
  description = "Log retention duration"
  default     = 30
}

variable "loggroup_tags" {
  type        = map(any)
  description = "Freeform Tags"
  default     = {
  "Environment": "Dev"
}
}

variable "service_logdef" {
  type        = map(any)
  description = "OCI Service log definition.Please refer doc for example definition"
  default     = ""
}

variable "tenancy_id" {
  type        = string
  description = "Tenancy OCID"
  default     = ""
}

variable "vcn_id" {
  type        = string
  description = "VCN OCID"
  default     = "none"
}

variable "windows_logdef" {
  type        = map(any)
  description = "Custom Windows Operating System Log Definition"
  default     = {}
}


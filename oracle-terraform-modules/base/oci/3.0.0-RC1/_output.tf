
output "ad_names" {
  description = "list of ADs in the selected region"
  value       = module.base.ad_names
}

output "bastion_public_ip" {
  description = "public ip address of operator host"
  value       = module.base.bastion_public_ip
}

output "group_name" {
  description = "name of dynamic group for operator host instance_principal"
  value       = module.base.group_name
}

output "home_region" {
  description = "tenancy home region"
  value       = module.base.home_region
}

output "ig_route_id" {
  description = "Internet Gateway id"
  value       = module.base.ig_route_id
}

output "nat_gateway_id" {
  description = "NAT gateway id of VCN"
  value       = module.base.nat_gateway_id
}

output "nat_route_id" {
  description = "NAT Routing table id"
  value       = module.base.nat_route_id
}

output "operator_private_ip" {
  description = "private ip address of operator host"
  value       = module.base.operator_private_ip
}

output "ssh_to_bastion" {
  description = "convenient output to ssh to the bastion host"
  value       = module.base.ssh_to_bastion
}

output "ssh_to_operator" {
  description = "convenient output to ssh to the operator host"
  value       = module.base.ssh_to_operator
}

output "vcn_id" {
  description = "VCN id"
  value       = module.base.vcn_id
}


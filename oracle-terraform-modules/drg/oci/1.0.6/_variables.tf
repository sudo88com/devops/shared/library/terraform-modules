
variable "compartment_id" {
  type        = string
  description = "compartment id where to create all resources"
  default     = ""
}

variable "defined_tags" {
  type        = map(any)
  description = "predefined and scoped to a namespace to tag the created resources using OCI Defined tags."
  default     = null
}

variable "drg_compartment_id" {
  type        = string
  description = "compartment id where the DRG is located"
  default     = null
}

variable "drg_display_name" {
  type        = string
  description = "(Updatable) Name of Dynamic Routing Gateway. Does not have to be unique."
  default     = "drg"
}

variable "drg_id" {
  type        = string
  description = "ID of an external created DRG"
  default     = null
}

variable "drg_vcn_attachments" {
  type        = map(any)
  description = "The OCID of the network resource attached to the DRG"
  default     = null
}

variable "freeform_tags" {
  type        = map(any)
  description = "simple key-value pairs to tag the created resources using freeform OCI Free-form tags."
  default     = {
  "module": "oracle-terraform-modules/vcn/oci//modules/drg",
  "terraformed": "Please do not edit manually"
}
}

variable "label_prefix" {
  type        = string
  description = "a string that will be prepended to all resources"
  default     = "none"
}

variable "remote_peering_connections" {
  type        = map(any)
  description = "List of parameters to add and optionally peer to remote peering connections"
  default     = null
}



output "drg_all_attributes" {
  description = "all attributes of created drg"
  value       = module.drg.drg_all_attributes
}

output "drg_attachment_all_attributes" {
  description = "all attributes related to drg attachment"
  value       = module.drg.drg_attachment_all_attributes
}

output "drg_display_name" {
  description = "display name of drg if it is created"
  value       = module.drg.drg_display_name
}

output "drg_id" {
  description = "id of drg if it is created"
  value       = module.drg.drg_id
}

output "drg_summary" {
  description = "drg information summary"
  value       = module.drg.drg_summary
}

output "rpc_all_attributes" {
  description = "all attributes of created RPCs"
  value       = module.drg.rpc_all_attributes
}

output "rpc_ids" {
  description = "IDs of created RPCs"
  value       = module.drg.rpc_ids
}



output "ssh_to_bastion" {
  description = "ssh to bastion"
  value       = module.olcne.ssh_to_bastion
}

output "ssh_to_master" {
  description = "ssh to primary master node"
  value       = module.olcne.ssh_to_master
}

output "ssh_to_operator" {
  description = "ssh to operator"
  value       = module.olcne.ssh_to_operator
}


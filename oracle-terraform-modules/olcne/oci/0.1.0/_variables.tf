
variable "admin_enabled" {
  type        = bool
  description = "whether to create an admin server in a private subnet"
  default     = false
}

variable "admin_image_id" {
  type        = string
  description = "image id to use for admin server. set either an image id or to Oracle. if value is set to Oracle, the default Oracle Linux platform image will be used."
  default     = "Oracle"
}

variable "admin_instance_principal" {
  type        = bool
  description = "enable the admin server host to call OCI API services without requiring api key"
  default     = true
}

variable "admin_notification_enabled" {
  type        = bool
  description = "whether to enable notification on the admin host"
  default     = false
}

variable "admin_notification_endpoint" {
  type        = string
  description = "the subscription notification endpoint for the admin. email address to be notified."
  default     = ""
}

variable "admin_notification_protocol" {
  type        = string
  description = "the notification protocol used."
  default     = "EMAIL"
}

variable "admin_notification_topic" {
  type        = string
  description = "the name of the notification topic."
  default     = "admin"
}

variable "admin_package_upgrade" {
  type        = bool
  description = "whether to upgrade the bastion host packages after provisioning. it’s useful to set this to false during development so the bastion is provisioned faster."
  default     = true
}

variable "admin_shape" {
  type        = string
  description = "shape of admin server instance"
  default     = "VM.Standard.E2.1"
}

variable "admin_timezone" {
  type        = string
  description = "the preferred timezone for the admin host."
  default     = "Australia/Sydney"
}

variable "allow_master_ssh_access" {
  type        = bool
  description = "whether to allow ssh access to master nodes"
  default     = false
}

variable "allow_worker_ssh_access" {
  type        = bool
  description = "whether to allow ssh access to worker nodes"
  default     = false
}

variable "api_fingerprint" {
  type        = string
  description = "fingerprint of oci api private key"
  default     = ""
}

variable "api_private_key_path" {
  type        = string
  description = "path to oci api private key"
  default     = ""
}

variable "availability_domains" {
  type        = map
  description = "Availability Domains where to provision specific resources"
  default     = {
  "admin": 1,
  "bastion": 1,
  "operator": 1
}
}

variable "bastion_access" {
  type        = string
  description = "cidr from where the bastion can be sshed into. default is ANYWHERE and equivalent to 0.0.0.0/0"
  default     = "ANYWHERE"
}

variable "bastion_enabled" {
  type        = bool
  description = "whether to create a bastion host"
  default     = true
}

variable "bastion_image_id" {
  type        = string
  description = "image id to use for bastion."
  default     = "Autonomous"
}

variable "bastion_notification_enabled" {
  type        = bool
  description = "whether to enable notification on the bastion host"
  default     = false
}

variable "bastion_notification_endpoint" {
  type        = string
  description = "the subscription notification endpoint for the bastion. email address to be notified."
  default     = ""
}

variable "bastion_notification_protocol" {
  type        = string
  description = "The notification protocol used."
  default     = "EMAIL"
}

variable "bastion_notification_topic" {
  type        = string
  description = "the name of the notification topic."
  default     = "bastion"
}

variable "bastion_package_upgrade" {
  type        = bool
  description = "whether to upgrade the bastion host packages after provisioning. it’s useful to set this to false during development so the bastion is provisioned faster."
  default     = true
}

variable "bastion_shape" {
  type        = string
  description = "shape of bastion instance"
  default     = "VM.Standard.E2.1"
}

variable "bastion_timezone" {
  type        = string
  description = "the preferred timezone for the bastion host."
  default     = "Australia/Sydney"
}

variable "city" {
  type        = string
  description = ""
  default     = ""
}

variable "cluster_name" {
  type        = string
  description = "name of the cluster"
  default     = "olcne"
}

variable "common_name" {
  type        = string
  description = ""
  default     = ""
}

variable "compartment_id" {
  type        = string
  description = "compartment id"
  default     = ""
}

variable "country" {
  type        = string
  description = "2 letter country code"
  default     = ""
}

variable "create_kata_runtime" {
  type        = bool
  description = "whether to create kata runtime class"
  default     = false
}

variable "disable_auto_retries" {
  type        = bool
  description = ""
  default     = false
}

variable "environment_name" {
  type        = string
  description = "name of the environment"
  default     = "dev"
}

variable "helm_version" {
  type        = string
  description = "version of helm client to install on operator"
  default     = "3.1.1"
}

variable "int_lb_shape" {
  type        = string
  description = "the preferred shape of the internal load balancer"
  default     = "100Mbps"
}

variable "kata_runtime_class_name" {
  type        = string
  description = "the name of the kata runtime class"
  default     = "kata"
}

variable "label_prefix" {
  type        = string
  description = "a string that will be prependend to all resources"
  default     = "dev"
}

variable "master_image_id" {
  type        = string
  description = "image id to use for master nodes."
  default     = "Oracle Linux"
}

variable "master_package_upgrade" {
  type        = bool
  description = "whether to upgrade the master host packages after provisioning. it’s useful to set this to false during development so the master nodes are provisioned faster."
  default     = true
}

variable "master_shape" {
  type        = string
  description = "shape of master instance"
  default     = "VM.Standard.E2.2"
}

variable "master_size" {
  type        = number
  description = "number of master nodes to provision"
  default     = 1
}

variable "master_timezone" {
  type        = string
  description = "the preferred timezone for the master nodes."
  default     = "Australia/Sydney"
}

variable "nat_gateway_enabled" {
  type        = bool
  description = "whether to create a nat gateway"
  default     = true
}

variable "netnum" {
  type        = map
  description = "zero-based index of the subnet when the network is masked with the newbit."
  default     = {
  "admin": 33,
  "bastion": 32,
  "int_lb": 16,
  "master": 48,
  "operator": 18,
  "pub_lb": 17,
  "worker": 1
}
}

variable "newbits" {
  type        = map
  description = "new mask for the subnet within the virtual network. use as newbits parameter for cidrsubnet function"
  default     = {
  "admin": 13,
  "bastion": 13,
  "lb": 11,
  "master": 12,
  "operator": 13,
  "worker": 2
}
}

variable "operator_image_id" {
  type        = string
  description = "image id to use for operator node."
  default     = "Oracle Linux"
}

variable "operator_package_upgrade" {
  type        = bool
  description = "whether to upgrade the operator host packages after provisioning. it’s useful to set this to false during development so the operator nodes are provisioned faster."
  default     = true
}

variable "operator_shape" {
  type        = string
  description = "shape of operator instance"
  default     = "VM.Standard.E2.2"
}

variable "operator_timezone" {
  type        = string
  description = "the preferred timezone for the operator nodes."
  default     = "Australia/Sydney"
}

variable "org" {
  type        = string
  description = ""
  default     = ""
}

variable "org_unit" {
  type        = string
  description = ""
  default     = ""
}

variable "public_lb_shape" {
  type        = string
  description = "the preferred shape of the public load balancer"
  default     = "100Mbps"
}

variable "region" {
  type        = string
  description = "region"
  default     = ""
}

variable "secret_id" {
  type        = string
  description = "id of secret where the private ssh key is stored in encrypted format"
  default     = ""
}

variable "service_gateway_enabled" {
  type        = bool
  description = "whether to create a service gateway"
  default     = true
}

variable "ssh_private_key_path" {
  type        = string
  description = "path to ssh private key"
  default     = ""
}

variable "ssh_public_key_path" {
  type        = string
  description = "path to ssh public key"
  default     = ""
}

variable "state" {
  type        = string
  description = ""
  default     = ""
}

variable "tenancy_id" {
  type        = string
  description = "tenancy id"
  default     = ""
}

variable "user_id" {
  type        = string
  description = "user id"
  default     = ""
}

variable "vcn_cidr" {
  type        = string
  description = "cidr block of VCN"
  default     = "10.0.0.0/16"
}

variable "vcn_dns_label" {
  type        = string
  description = "internal vcn dns domain that will be prepended to oraclevcn.com"
  default     = "olcne"
}

variable "vcn_name" {
  type        = string
  description = "name of vcn"
  default     = "olcne"
}

variable "worker_image_id" {
  type        = string
  description = "image id to use for worker nodes."
  default     = "Oracle Linux"
}

variable "worker_package_upgrade" {
  type        = bool
  description = "whether to upgrade the worker host packages after provisioning. it’s useful to set this to false during development so the worker nodes are provisioned faster."
  default     = true
}

variable "worker_shape" {
  type        = string
  description = "shape of worker instance"
  default     = "VM.Standard.E2.2"
}

variable "worker_size" {
  type        = number
  description = "number of worker nodes to provision"
  default     = 3
}

variable "worker_timezone" {
  type        = string
  description = "the preferred timezone for the worker nodes."
  default     = "Australia/Sydney"
}



locals {
  location = var.location
  other_subscription_ids = var.other_subscription_ids
  this_subscription_id = var.this_subscription_id
}

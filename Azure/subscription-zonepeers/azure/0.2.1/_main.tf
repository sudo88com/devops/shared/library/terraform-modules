
module "subscription-zonepeers" {
  source = "terraform-aws-modules/subscription-zonepeers/aws"
  version = "0.2.1"
  location = var.location
  other_subscription_ids = var.other_subscription_ids
  this_subscription_id = var.this_subscription_id
}

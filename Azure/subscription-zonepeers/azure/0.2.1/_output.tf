
output "response" {
  description = "A map of this subscription's availability zones, to a map of subscription IDs to zone numbers.\n\nE.g.\n\nIf you have a resource deployed in `var.other_subscription_id` in zone `1` in `var.location`, then you can get the equivalent zone in this subscription with:\n\n```hcl\nmodule \"zone_peers_westus2\" {\n  source                 = \"../../\"\n  this_subscription_id   = var.this_subscription_id\n  location               = \"westus2\"\n  other_subscription_ids = [var.other_subscription_id]\n}\n\nlocals {\n  this_sub_equiv_az = module.zone_peers_westus2.response[var.other_subscription_id].zone[\"1\"]\n}\n```\n"
  value       = module.subscription-zonepeers.response
}



edge_zone = null

enable_telemetry = true

express_route_circuits = {}

express_route_remote_vnet_traffic_enabled = false

express_route_virtual_wan_traffic_enabled = false

ip_configurations = {}

local_network_gateways = {}

location = 

name = 

route_table_bgp_route_propagation_enabled = true

route_table_creation_enabled = false

route_table_name = null

route_table_tags = {}

sku = "ErGw1AZ"

subnet_address_prefix = ""

subnet_creation_enabled = true

tags = null

type = "ExpressRoute"

virtual_network_id = 

vpn_active_active_enabled = false

vpn_bgp_enabled = false

vpn_bgp_route_translation_for_nat_enabled = false

vpn_bgp_settings = null

vpn_custom_route = null

vpn_default_local_network_gateway_id = null

vpn_dns_forwarding_enabled = null

vpn_generation = null

vpn_ip_sec_replay_protection_enabled = true

vpn_point_to_site = null

vpn_policy_groups = {}

vpn_private_ip_address_enabled = null

vpn_type = "RouteBased"



locals {
  edge_zone = var.edge_zone
  enable_telemetry = var.enable_telemetry
  express_route_circuits = var.express_route_circuits
  express_route_remote_vnet_traffic_enabled = var.express_route_remote_vnet_traffic_enabled
  express_route_virtual_wan_traffic_enabled = var.express_route_virtual_wan_traffic_enabled
  ip_configurations = var.ip_configurations
  local_network_gateways = var.local_network_gateways
  location = var.location
  name = var.name
  route_table_bgp_route_propagation_enabled = var.route_table_bgp_route_propagation_enabled
  route_table_creation_enabled = var.route_table_creation_enabled
  route_table_name = var.route_table_name
  route_table_tags = var.route_table_tags
  sku = var.sku
  subnet_address_prefix = var.subnet_address_prefix
  subnet_creation_enabled = var.subnet_creation_enabled
  tags = var.tags
  type = var.type
  virtual_network_id = var.virtual_network_id
  vpn_active_active_enabled = var.vpn_active_active_enabled
  vpn_bgp_enabled = var.vpn_bgp_enabled
  vpn_bgp_route_translation_for_nat_enabled = var.vpn_bgp_route_translation_for_nat_enabled
  vpn_bgp_settings = var.vpn_bgp_settings
  vpn_custom_route = var.vpn_custom_route
  vpn_default_local_network_gateway_id = var.vpn_default_local_network_gateway_id
  vpn_dns_forwarding_enabled = var.vpn_dns_forwarding_enabled
  vpn_generation = var.vpn_generation
  vpn_ip_sec_replay_protection_enabled = var.vpn_ip_sec_replay_protection_enabled
  vpn_point_to_site = var.vpn_point_to_site
  vpn_policy_groups = var.vpn_policy_groups
  vpn_private_ip_address_enabled = var.vpn_private_ip_address_enabled
  vpn_type = var.vpn_type
}

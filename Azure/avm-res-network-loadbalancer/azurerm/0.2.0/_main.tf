
module "avm-res-network-loadbalancer" {
  source = "terraform-aws-modules/avm-res-network-loadbalancer/aws"
  version = "0.2.0"
  backend_address_pool_addresses = var.backend_address_pool_addresses
  backend_address_pool_configuration = var.backend_address_pool_configuration
  backend_address_pool_network_interfaces = var.backend_address_pool_network_interfaces
  backend_address_pools = var.backend_address_pools
  diagnostic_settings = var.diagnostic_settings
  edge_zone = var.edge_zone
  enable_telemetry = var.enable_telemetry
  frontend_ip_configurations = var.frontend_ip_configurations
  frontend_subnet_resource_id = var.frontend_subnet_resource_id
  lb_nat_pools = var.lb_nat_pools
  lb_nat_rules = var.lb_nat_rules
  lb_outbound_rules = var.lb_outbound_rules
  lb_probes = var.lb_probes
  lb_rules = var.lb_rules
  location = var.location
  lock = var.lock
  name = var.name
  public_ip_address_configuration = var.public_ip_address_configuration
  resource_group_name = var.resource_group_name
  role_assignments = var.role_assignments
  sku = var.sku
  sku_tier = var.sku_tier
  tags = var.tags
}

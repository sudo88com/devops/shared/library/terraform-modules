
variable "managed_disk_names" {
  type        = 
  description = "Names of the disks to snapshot provided in list format"
  default     = ""
}

variable "resource_group_name" {
  type        = 
  description = "Name of Resource group where the managed data disks resides"
  default     = ""
}

variable "tags" {
  type        = 
  description = "The tags to associate with your network and subnets."
  default     = {
  "tag1": "",
  "tag2": ""
}
}

variable "version" {
  type        = 
  description = "Snapshot version"
  default     = "1"
}


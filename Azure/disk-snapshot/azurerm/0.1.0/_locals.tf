
locals {
  managed_disk_names = var.managed_disk_names
  resource_group_name = var.resource_group_name
  tags = var.tags
  version = var.version
}

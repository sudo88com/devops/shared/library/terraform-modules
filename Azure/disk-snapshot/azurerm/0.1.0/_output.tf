
output "snapshot_ids" {
  description = "The id of Snapshot resources created"
  value       = module.disk-snapshot.snapshot_ids
}


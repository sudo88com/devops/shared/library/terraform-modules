
module "disk-snapshot" {
  source = "terraform-aws-modules/disk-snapshot/aws"
  version = "0.1.0"
  managed_disk_names = var.managed_disk_names
  resource_group_name = var.resource_group_name
  tags = var.tags
  version = var.version
}

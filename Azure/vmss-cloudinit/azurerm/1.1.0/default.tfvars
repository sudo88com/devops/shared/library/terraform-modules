
admin_password = ""

admin_username = "azureuser"

cloudconfig_file = 

computer_name_prefix = "vmss"

data_disk_size = "10"

load_balancer_backend_address_pool_ids = 

location = ""

managed_disk_type = "Standard_LRS"

nb_instance = "1"

network_profile = "terraformnetworkprofile"

resource_group_name = "vmssrg"

ssh_key = "~/.ssh/id_rsa.pub"

tags = {
  "source": "terraform"
}

vm_os_id = ""

vm_os_offer = ""

vm_os_publisher = ""

vm_os_simple = ""

vm_os_sku = ""

vm_os_version = "latest"

vm_size = "Standard_A0"

vmscaleset_name = "vmscaleset"

vnet_subnet_id = 


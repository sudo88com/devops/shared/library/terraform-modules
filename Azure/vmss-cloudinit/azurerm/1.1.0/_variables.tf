
variable "admin_password" {
  type        = 
  description = "The admin password to be used on the VMSS that will be deployed. The password must meet the complexity requirements of Azure"
  default     = ""
}

variable "admin_username" {
  type        = 
  description = "The admin username of the VMSS that will be deployed"
  default     = "azureuser"
}

variable "cloudconfig_file" {
  type        = 
  description = "The location of the cloud init configuration file."
  default     = ""
}

variable "computer_name_prefix" {
  type        = 
  description = "The prefix that will be used for the hostname of the instances part of the VM scale set"
  default     = "vmss"
}

variable "data_disk_size" {
  type        = 
  description = "Specify the size in GB of the data disk"
  default     = "10"
}

variable "load_balancer_backend_address_pool_ids" {
  type        = 
  description = "The id of the backend address pools of the loadbalancer to which the VM scale set is attached"
  default     = ""
}

variable "location" {
  type        = 
  description = "The location where the resources will be created"
  default     = ""
}

variable "managed_disk_type" {
  type        = 
  description = "Type of managed disk for the VMs that will be part of this compute group. Allowable values are 'Standard_LRS' or 'Premium_LRS'."
  default     = "Standard_LRS"
}

variable "nb_instance" {
  type        = 
  description = "Specify the number of vm instances"
  default     = "1"
}

variable "network_profile" {
  type        = 
  description = "The name of the network profile that will be used in the VM scale set"
  default     = "terraformnetworkprofile"
}

variable "resource_group_name" {
  type        = 
  description = "The name of the resource group in which the resources will be created"
  default     = "vmssrg"
}

variable "ssh_key" {
  type        = 
  description = "Path to the public key to be used for ssh access to the VM"
  default     = "~/.ssh/id_rsa.pub"
}

variable "tags" {
  type        = 
  description = "A map of the tags to use on the resources that are deployed with this module."
  default     = {
  "source": "terraform"
}
}

variable "vm_os_id" {
  type        = 
  description = "The ID of the image that you want to deploy if you are using a custom image."
  default     = ""
}

variable "vm_os_offer" {
  type        = 
  description = "The name of the offer of the image that you want to deploy"
  default     = ""
}

variable "vm_os_publisher" {
  type        = 
  description = "The name of the publisher of the image that you want to deploy"
  default     = ""
}

variable "vm_os_simple" {
  type        = 
  description = "Specify Ubuntu or Windows to get the latest version of each os"
  default     = ""
}

variable "vm_os_sku" {
  type        = 
  description = "The sku of the image that you want to deploy"
  default     = ""
}

variable "vm_os_version" {
  type        = 
  description = "The version of the image that you want to deploy."
  default     = "latest"
}

variable "vm_size" {
  type        = 
  description = "Size of the Virtual Machine based on Azure sizing"
  default     = "Standard_A0"
}

variable "vmscaleset_name" {
  type        = 
  description = "The name of the VM scale set that will be created in Azure"
  default     = "vmscaleset"
}

variable "vnet_subnet_id" {
  type        = 
  description = "The subnet id of the virtual network on which the vm scale set will be connected"
  default     = ""
}



diagnostic_settings = {}

enable_telemetry = true

firewall_policy_auto_learn_private_ranges_enabled = null

firewall_policy_base_policy_id = null

firewall_policy_dns = null

firewall_policy_explicit_proxy = null

firewall_policy_identity = null

firewall_policy_insights = null

firewall_policy_intrusion_detection = null

firewall_policy_private_ip_ranges = null

firewall_policy_sku = null

firewall_policy_sql_redirect_allowed = null

firewall_policy_threat_intelligence_allowlist = null

firewall_policy_threat_intelligence_mode = null

firewall_policy_timeouts = null

firewall_policy_tls_certificate = null

location = 

lock = null

name = 

resource_group_name = 

role_assignments = {}

tags = null


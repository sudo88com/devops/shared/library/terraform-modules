
variable "disk_size_gb" {
  type        = 
  description = "For a new empty disk, size of the disk in gb. For a disk copy or a platform image, if provided must be >= the size of the source. Providing a 0 means disk size remains the same as the source."
  default     = 0
}

variable "image_reference_id" {
  type        = 
  description = "ID of a platform image to copy, resulting in a new managed disk. source_uri, source_resource_id, and image_reference_id are mutually exclusive. Leave them all blank to create a new empty managed disk."
  default     = ""
}

variable "import_or_copy_os_type" {
  type        = 
  description = "If new managed disk is imported or copied, the os type contained in the source object. May be 'Linux' or 'Windows'"
  default     = ""
}

variable "keyURL" {
  type        = 
  description = "URL of the key to the secret in the key vault that encrypts the disk"
  default     = ""
}

variable "keyVaultID" {
  type        = 
  description = "ID of the key vault providing secret & key for disk encryption"
  default     = ""
}

variable "location" {
  type        = 
  description = "The location/region where the managed disk will be created. The full list of Azure regions can be found at https://azure.microsoft.com/regions."
  default     = ""
}

variable "managed_disk_name" {
  type        = 
  description = "Name of the new managed disk."
  default     = "myManagedDisk01"
}

variable "resource_group_name" {
  type        = 
  description = "Name of resource group that the managed disk will be created in."
  default     = "myapp-rg"
}

variable "secretURL" {
  type        = 
  description = "URL of the secret in the key vault that encrypts the disk"
  default     = ""
}

variable "source_resource_id" {
  type        = 
  description = "ID of an existing managed disk to copy, resulting in a new managed disk. source_uri, source_resource_id, and image_reference_id are mutually exclusive. Leave them all blank to create a new empty managed disk."
  default     = ""
}

variable "source_uri" {
  type        = 
  description = "URI of a VHD to be imported, resulting in a new managed disk. source_uri, source_resource_id, and image_reference_id are mutually exclusive. Leave them all blank to create a new empty managed disk."
  default     = ""
}

variable "storage_account_type" {
  type        = 
  description = "Type of storage to use for new managed disk. May be 'Standard_LRS' or 'Premium_LRS'."
  default     = "Premium_LRS"
}

variable "tags" {
  type        = 
  description = "The tags to associate with your managed disk."
  default     = {
  "tag1": "",
  "tag2": ""
}
}


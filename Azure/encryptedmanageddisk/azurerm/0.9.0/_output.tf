
output "copy_disk" {
  description = "Says that a source_resource_id was provided."
  value       = module.encryptedmanageddisk.copy_disk
}

output "copy_image" {
  description = "Says that a image_resource_id was provided."
  value       = module.encryptedmanageddisk.copy_image
}

output "create_empty" {
  description = "Says that a new empty disk will be created."
  value       = module.encryptedmanageddisk.create_empty
}

output "create_option" {
  description = "Tells the create option for the managed disk resource."
  value       = module.encryptedmanageddisk.create_option
}

output "disk_size_gb" {
  description = "Tells the disk_size_gb provided by input."
  value       = module.encryptedmanageddisk.disk_size_gb
}

output "import_vhd" {
  description = "Says that a source_uri was provided."
  value       = module.encryptedmanageddisk.import_vhd
}

output "managed_disk_id" {
  description = "The id of the newly created managed disk"
  value       = module.encryptedmanageddisk.managed_disk_id
}


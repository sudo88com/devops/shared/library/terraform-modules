
output "virtual_machine_dns_name" {
  description = "The DNS name of the Jenkins virtual machine."
  value       = module.module-test-jenkins.virtual_machine_dns_name
}

output "virtual_machine_public_ip" {
  description = "The public IP address of the Jenkins virtual machine."
  value       = module.module-test-jenkins.virtual_machine_public_ip
}


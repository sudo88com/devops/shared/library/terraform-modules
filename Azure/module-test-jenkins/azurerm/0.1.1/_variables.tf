
variable "admin_username" {
  type        = 
  description = "The username of the administrator of the Jenkins virtual machine."
  default     = ""
}

variable "location" {
  type        = 
  description = "The data center location where all resources will be put into."
  default     = ""
}

variable "network_interface_name" {
  type        = 
  description = "The name of the primary network interface which will be used by the Jenkins virtual machine."
  default     = "jenkins-nic"
}

variable "network_security_group_name" {
  type        = 
  description = "The name of the network security group (firewall rules) for Jenkins virtual machine."
  default     = "ssh-allow"
}

variable "public_domain_name" {
  type        = 
  description = "The domain name of the Jenkins virtual machine (without azure subdomain)."
  default     = ""
}

variable "public_ip_name" {
  type        = 
  description = "The name of the public IP address for Jenkins virtual machine."
  default     = "jenkins-pip"
}

variable "resource_group_name" {
  type        = 
  description = "The name of the resource group which the Jenkins test farm will be created in."
  default     = ""
}

variable "ssh_private_key_data" {
  type        = 
  description = "The SSH private key for remote connection. It is used to configure the environment after the virtual machine is created."
  default     = ""
}

variable "ssh_public_key_data" {
  type        = 
  description = "The SSH public key for remote connection of the Jenkins virtual machine."
  default     = ""
}

variable "subnet_name" {
  type        = 
  description = "The name of the subnet where the Jenkins virtual machine will be put into."
  default     = "jenkins-subnet"
}

variable "virtual_machine_computer_name" {
  type        = 
  description = "The computer name of the Jenkins virtual machine."
  default     = "tfmod-jenkins"
}

variable "virtual_machine_name" {
  type        = 
  description = "The name of the Jenkins virtual machine which contains the Jenkins server."
  default     = "jenkins-vm"
}

variable "virtual_machine_osdisk_name" {
  type        = 
  description = "The managed OS disk name of the Jenkins virtual machine."
  default     = "jenkins-osdisk"
}

variable "virtual_machine_osdisk_type" {
  type        = 
  description = "The managed OS disk type of the Jenkins virtual machine."
  default     = "Standard_LRS"
}

variable "virtual_machine_size" {
  type        = 
  description = "The size of the Jenkins virtual machine."
  default     = "Standard_A2_v2"
}

variable "virtual_network_name" {
  type        = 
  description = "The name of the virtual network connecting all resources."
  default     = "module-test-farm-vnet"
}


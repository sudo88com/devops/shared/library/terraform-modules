
locals {
  admin_username = var.admin_username
  location = var.location
  network_interface_name = var.network_interface_name
  network_security_group_name = var.network_security_group_name
  public_domain_name = var.public_domain_name
  public_ip_name = var.public_ip_name
  resource_group_name = var.resource_group_name
  ssh_private_key_data = var.ssh_private_key_data
  ssh_public_key_data = var.ssh_public_key_data
  subnet_name = var.subnet_name
  virtual_machine_computer_name = var.virtual_machine_computer_name
  virtual_machine_name = var.virtual_machine_name
  virtual_machine_osdisk_name = var.virtual_machine_osdisk_name
  virtual_machine_osdisk_type = var.virtual_machine_osdisk_type
  virtual_machine_size = var.virtual_machine_size
  virtual_network_name = var.virtual_network_name
}


locals {
  authorization_rules = var.authorization_rules
  capacity = var.capacity
  customer_managed_key = var.customer_managed_key
  diagnostic_settings = var.diagnostic_settings
  enable_telemetry = var.enable_telemetry
  infrastructure_encryption_enabled = var.infrastructure_encryption_enabled
  local_auth_enabled = var.local_auth_enabled
  location = var.location
  lock = var.lock
  managed_identities = var.managed_identities
  minimum_tls_version = var.minimum_tls_version
  name = var.name
  network_rule_config = var.network_rule_config
  premium_messaging_partitions = var.premium_messaging_partitions
  private_endpoints = var.private_endpoints
  private_endpoints_manage_dns_zone_group = var.private_endpoints_manage_dns_zone_group
  public_network_access_enabled = var.public_network_access_enabled
  queues = var.queues
  resource_group_name = var.resource_group_name
  role_assignments = var.role_assignments
  sku = var.sku
  tags = var.tags
  topics = var.topics
  zone_redundant = var.zone_redundant
}

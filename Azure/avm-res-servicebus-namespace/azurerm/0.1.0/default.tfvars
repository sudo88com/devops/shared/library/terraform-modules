
authorization_rules = {}

capacity = null

customer_managed_key = null

diagnostic_settings = {}

enable_telemetry = true

infrastructure_encryption_enabled = true

local_auth_enabled = true

location = 

lock = null

managed_identities = {}

minimum_tls_version = "1.2"

name = 

network_rule_config = {}

premium_messaging_partitions = null

private_endpoints = {}

private_endpoints_manage_dns_zone_group = true

public_network_access_enabled = true

queues = {}

resource_group_name = 

role_assignments = {}

sku = "Standard"

tags = null

topics = {}

zone_redundant = null



output "name" {
  description = "The name of cognitive account created."
  value       = module.avm-res-cognitiveservices-account.name
}

output "private_endpoints" {
  description = "  A map of the private endpoints created.\n"
  value       = module.avm-res-cognitiveservices-account.private_endpoints
}

output "resource" {
  description = "The cognitive account resource created."
  value       = module.avm-res-cognitiveservices-account.resource
}

output "resource_cognitive_deployment" {
  description = "The map of cognitive deployments created."
  value       = module.avm-res-cognitiveservices-account.resource_cognitive_deployment
}

output "resource_id" {
  description = "The resource ID of cognitive account created."
  value       = module.avm-res-cognitiveservices-account.resource_id
}

output "system_assigned_mi_principal_id" {
  description = "The principal ID of system assigned managed identity on the cognitive account created, when `var.managed_identities` is `null` or `var.managed_identities.system_assigned` is `false` this output is `null`."
  value       = module.avm-res-cognitiveservices-account.system_assigned_mi_principal_id
}



locals {
  cognitive_deployments = var.cognitive_deployments
  custom_question_answering_search_service_id = var.custom_question_answering_search_service_id
  custom_question_answering_search_service_key = var.custom_question_answering_search_service_key
  custom_subdomain_name = var.custom_subdomain_name
  customer_managed_key = var.customer_managed_key
  diagnostic_settings = var.diagnostic_settings
  dynamic_throttling_enabled = var.dynamic_throttling_enabled
  enable_telemetry = var.enable_telemetry
  fqdns = var.fqdns
  kind = var.kind
  local_auth_enabled = var.local_auth_enabled
  location = var.location
  lock = var.lock
  managed_identities = var.managed_identities
  metrics_advisor_aad_client_id = var.metrics_advisor_aad_client_id
  metrics_advisor_aad_tenant_id = var.metrics_advisor_aad_tenant_id
  metrics_advisor_super_user_name = var.metrics_advisor_super_user_name
  metrics_advisor_website_name = var.metrics_advisor_website_name
  name = var.name
  network_acls = var.network_acls
  outbound_network_access_restricted = var.outbound_network_access_restricted
  private_endpoints = var.private_endpoints
  public_network_access_enabled = var.public_network_access_enabled
  qna_runtime_endpoint = var.qna_runtime_endpoint
  resource_group_name = var.resource_group_name
  role_assignments = var.role_assignments
  sku_name = var.sku_name
  storage = var.storage
  tags = var.tags
  timeouts = var.timeouts
}


access_tier = "Hot"

account_kind = "StorageV2"

account_replication_type = "ZRS"

account_tier = "Standard"

allow_nested_items_to_be_public = false

allowed_copy_scope = null

azure_files_authentication = null

blob_properties = null

containers = {}

cross_tenant_replication_enabled = false

custom_domain = null

customer_managed_key = null

default_to_oauth_authentication = null

diagnostic_settings_blob = {}

diagnostic_settings_file = {}

diagnostic_settings_queue = {}

diagnostic_settings_storage_account = {}

diagnostic_settings_table = {}

edge_zone = null

enable_https_traffic_only = true

enable_telemetry = true

immutability_policy = null

infrastructure_encryption_enabled = false

is_hns_enabled = null

large_file_share_enabled = null

local_user = {}

location = 

lock = null

managed_identities = {}

min_tls_version = "TLS1_2"

name = 

network_rules = {}

nfsv3_enabled = false

private_endpoints = {}

public_network_access_enabled = false

queue_encryption_key_type = null

queue_properties = null

queues = {}

resource_group_name = 

role_assignments = {}

routing = null

sas_policy = null

sftp_enabled = false

share_properties = null

shared_access_key_enabled = false

shares = {}

static_website = null

table_encryption_key_type = null

tables = {}

tags = null

timeouts = null

use_nested_nacl = false

wait_for_rbac_before_container_operations = {}

wait_for_rbac_before_queue_operations = {}

wait_for_rbac_before_share_operations = {}

wait_for_rbac_before_table_operations = {}



module "search-service" {
  source = "terraform-aws-modules/search-service/aws"
  version = "0.9.0"
  hosting_mode = var.hosting_mode
  location = var.location
  partition_count = var.partition_count
  replica_count = var.replica_count
  resource_group = var.resource_group
  search_name = var.search_name
  sku = var.sku
  tags = var.tags
}

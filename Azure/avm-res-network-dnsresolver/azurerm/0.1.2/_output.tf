
output "inbound_endpoint_ips" {
  description = "The IP addresses of the inbound endpoints."
  value       = module.avm-res-network-dnsresolver.inbound_endpoint_ips
}

output "name" {
  description = "The name of the DNS resolver."
  value       = module.avm-res-network-dnsresolver.name
}

output "resource" {
  description = "This is the full output for the resource."
  value       = module.avm-res-network-dnsresolver.resource
}

output "resource_id" {
  description = "The ID of the DNS resolver."
  value       = module.avm-res-network-dnsresolver.resource_id
}


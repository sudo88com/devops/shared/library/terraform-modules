
output "CICDAgentSubnetId" {
  description = ""
  value       = module.appservice-lz-accelerator.CICDAgentSubnetId
}

output "CICDAgentSubnetName" {
  description = ""
  value       = module.appservice-lz-accelerator.CICDAgentSubnetName
}

output "appServicePlanId" {
  description = ""
  value       = module.appservice-lz-accelerator.appServicePlanId
}

output "appServicePlanName" {
  description = ""
  value       = module.appservice-lz-accelerator.appServicePlanName
}

output "app_service_default_hostname" {
  description = "The Default Hostname associated with the App Service - such as mysite.azurewebsites.net"
  value       = module.appservice-lz-accelerator.app_service_default_hostname
}

output "app_service_name" {
  description = "Name of the app service"
  value       = module.appservice-lz-accelerator.app_service_name
}

output "aseId" {
  description = ""
  value       = module.appservice-lz-accelerator.aseId
}

output "aseName" {
  description = ""
  value       = module.appservice-lz-accelerator.aseName
}

output "aseSubnetId" {
  description = ""
  value       = module.appservice-lz-accelerator.aseSubnetId
}

output "aseSubnetName" {
  description = ""
  value       = module.appservice-lz-accelerator.aseSubnetName
}

output "bastionSubnetId" {
  description = ""
  value       = module.appservice-lz-accelerator.bastionSubnetId
}

output "bastionSubnetName" {
  description = ""
  value       = module.appservice-lz-accelerator.bastionSubnetName
}

output "hubVNetId" {
  description = ""
  value       = module.appservice-lz-accelerator.hubVNetId
}

output "hubVNetName" {
  description = ""
  value       = module.appservice-lz-accelerator.hubVNetName
}

output "jumpBoxSubnetId" {
  description = ""
  value       = module.appservice-lz-accelerator.jumpBoxSubnetId
}

output "jumpBoxSubnetName" {
  description = ""
  value       = module.appservice-lz-accelerator.jumpBoxSubnetName
}

output "spokeVNetId" {
  description = ""
  value       = module.appservice-lz-accelerator.spokeVNetId
}

output "spokeVNetName" {
  description = ""
  value       = module.appservice-lz-accelerator.spokeVNetName
}


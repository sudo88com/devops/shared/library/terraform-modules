
variable "CICDAgentNameAddressPrefix" {
  type        = string
  description = "CIDR prefix to use for Spoke VNet"
  default     = "10.0.2.0/24"
}

variable "aseAddressPrefix" {
  type        = string
  description = "CIDR prefix to use for ASE"
  default     = "10.1.1.0/24"
}

variable "bastionAddressPrefix" {
  type        = string
  description = "CIDR prefix to use for Hub VNet"
  default     = "10.0.1.0/24"
}

variable "environment" {
  type        = string
  description = "The environment for which the deployment is being executed"
  default     = "dev"
}

variable "hubVNetNameAddressPrefix" {
  type        = string
  description = "CIDR prefix to use for Hub VNet"
  default     = "10.0.0.0/16"
}

variable "jumpBoxAddressPrefix" {
  type        = string
  description = "CIDR prefix to use for Jumpbox VNet"
  default     = "10.0.3.0/24"
}

variable "location" {
  type        = string
  description = "The Azure location where all resources should be created"
  default     = "westus2"
}

variable "numberOfWorkers" {
  type        = number
  description = "numberOfWorkers for ASE"
  default     = 3
}

variable "spokeVNetNameAddressPrefix" {
  type        = string
  description = "CIDR prefix to use for Spoke VNet"
  default     = "10.1.0.0/16"
}

variable "workerPool" {
  type        = number
  description = "workerPool for ASE"
  default     = 1
}

variable "workloadName" {
  type        = string
  description = "A short name for the workload being deployed"
  default     = "ase"
}


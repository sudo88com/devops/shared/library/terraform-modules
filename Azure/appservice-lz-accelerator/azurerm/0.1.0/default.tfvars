
CICDAgentNameAddressPrefix = "10.0.2.0/24"

aseAddressPrefix = "10.1.1.0/24"

bastionAddressPrefix = "10.0.1.0/24"

environment = "dev"

hubVNetNameAddressPrefix = "10.0.0.0/16"

jumpBoxAddressPrefix = "10.0.3.0/24"

location = "westus2"

numberOfWorkers = 3

spokeVNetNameAddressPrefix = "10.1.0.0/16"

workerPool = 1

workloadName = "ase"



module "appservice-lz-accelerator" {
  source = "terraform-aws-modules/appservice-lz-accelerator/aws"
  version = "0.1.0"
  CICDAgentNameAddressPrefix = var.CICDAgentNameAddressPrefix
  aseAddressPrefix = var.aseAddressPrefix
  bastionAddressPrefix = var.bastionAddressPrefix
  environment = var.environment
  hubVNetNameAddressPrefix = var.hubVNetNameAddressPrefix
  jumpBoxAddressPrefix = var.jumpBoxAddressPrefix
  location = var.location
  numberOfWorkers = var.numberOfWorkers
  spokeVNetNameAddressPrefix = var.spokeVNetNameAddressPrefix
  workerPool = var.workerPool
  workloadName = var.workloadName
}

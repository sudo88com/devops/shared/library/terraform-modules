
admin_enabled = false

anonymous_pull_enabled = false

data_endpoint_enabled = false

diagnostic_settings = {}

enable_telemetry = true

export_policy_enabled = true

georeplications = []

location = null

lock = {}

managed_identities = {}

name = 

network_rule_bypass_option = "None"

network_rule_set = null

private_endpoints = {}

public_network_access_enabled = true

quarantine_policy_enabled = false

resource_group_name = 

retention_policy = {}

role_assignments = {}

sku = "Premium"

tags = {}

zone_redundancy_enabled = true


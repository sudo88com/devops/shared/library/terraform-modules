
variable "application_type" {
  type        = string
  description = "(Required) The type of the application. Possible values are 'web', 'ios', 'java', 'phone', 'MobileCenter', 'other', 'store'."
  default     = "web"
}

variable "daily_data_cap_in_gb" {
  type        = number
  description = "(Optional) The daily data cap in GB. 0 means unlimited."
  default     = 100
}

variable "daily_data_cap_notifications_disabled" {
  type        = bool
  description = "(Optional) Disables the daily data cap notifications."
  default     = false
}

variable "disable_ip_masking" {
  type        = bool
  description = "(Optional) Disables IP masking. Defaults to false. For more information see <https://aka.ms/avm/ipmasking>."
  default     = false
}

variable "enable_telemetry" {
  type        = bool
  description = "This variable controls whether or not telemetry is enabled for the module.\nFor more information see <https://aka.ms/avm/telemetryinfo>.\nIf it is set to false, then no telemetry will be collected.\n"
  default     = true
}

variable "location" {
  type        = string
  description = "Azure region where the resource should be deployed."
  default     = ""
}

variable "lock" {
  type        = object({
    kind = string
    name = optional(string, null)
  })
  description = "  Controls the Resource Lock configuration for this resource. The following properties can be specified:\n  \n  - `kind` - (Required) The type of lock. Possible values are `\\"CanNotDelete\\"` and `\\"ReadOnly\\"`.\n  - `name` - (Optional) The name of the lock. If not specified, a name will be generated based on the `kind` value. Changing this forces the creation of a new resource.\n"
  default     = null
}

variable "managed_identities" {
  type        = object({
    system_assigned            = optional(bool, false)
    user_assigned_resource_ids = optional(set(string), [])
  })
  description = "  Controls the Managed Identity configuration on this resource. The following properties can be specified:\n  \n  - `system_assigned` - (Optional) Specifies if the System Assigned Managed Identity should be enabled.\n  - `user_assigned_resource_ids` - (Optional) Specifies a list of User Assigned Managed Identity resource IDs to be assigned to this resource.\n"
  default     = {}
}

variable "name" {
  type        = string
  description = "The name of the this resource."
  default     = ""
}

variable "resource_group_name" {
  type        = string
  description = "The resource group where the resources will be deployed."
  default     = ""
}

variable "retention_in_days" {
  type        = number
  description = "(Optional) The retention period in days. 0 means unlimited."
  default     = 90
}

variable "sampling_percentage" {
  type        = number
  description = "(Optional) The sampling percentage. 100 means all."
  default     = 100
}

variable "tags" {
  type        = map(string)
  description = "(Optional) Tags of the resource."
  default     = null
}

variable "workspace_id" {
  type        = string
  description = "(Required) The ID of the Log Analytics workspace to send data to. AzureRm supports classic; however, Azure has deprecated it, thus it's required"
  default     = ""
}


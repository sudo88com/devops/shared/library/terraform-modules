
output "name" {
  description = "Name of the Application Insights"
  value       = module.avm-res-insights-component.name
}

output "resource" {
  description = "This is the full output for the resource."
  value       = module.avm-res-insights-component.resource
}

output "resource_id" {
  description = "The ID of the Application Insights"
  value       = module.avm-res-insights-component.resource_id
}


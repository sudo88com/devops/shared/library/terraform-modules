
variable "aks_agent_count" {
  type        = 
  description = "The number of agent nodes for the cluster."
  default     = 3
}

variable "aks_agent_os_disk_size" {
  type        = 
  description = "Disk size (in GB) to provision for each of the agent pool nodes. This value ranges from 0 to 1023. Specifying 0 will apply the default disk size for that agentVMSize."
  default     = 40
}

variable "aks_agent_vm_size" {
  type        = 
  description = "The size of the Virtual Machine."
  default     = "Standard_D3_v2"
}

variable "aks_dns_prefix" {
  type        = 
  description = "Optional DNS prefix to use with hosted Kubernetes API server FQDN."
  default     = "aks"
}

variable "aks_dns_service_ip" {
  type        = 
  description = "Containers DNS server IP address."
  default     = "10.0.0.10"
}

variable "aks_docker_bridge_cidr" {
  type        = 
  description = "A CIDR notation IP for Docker bridge."
  default     = "172.17.0.1/16"
}

variable "aks_enable_rbac" {
  type        = 
  description = "Enable RBAC on the AKS cluster. Defaults to false."
  default     = "false"
}

variable "aks_name" {
  type        = 
  description = "Name of the AKS cluster."
  default     = "aks-cluster1"
}

variable "aks_service_cidr" {
  type        = 
  description = "A CIDR notation IP range from which to assign service cluster IPs."
  default     = "10.0.0.0/16"
}

variable "aks_service_principal_app_id" {
  type        = 
  description = "Application ID/Client ID  of the service principal. Used by AKS to manage AKS related resources on Azure like vms, subnets."
  default     = ""
}

variable "aks_service_principal_client_secret" {
  type        = 
  description = "Secret of the service principal. Used by AKS to manage Azure."
  default     = ""
}

variable "aks_service_principal_object_id" {
  type        = 
  description = "Object ID of the service principal."
  default     = ""
}

variable "aks_subnet_address_prefix" {
  type        = 
  description = "Containers DNS server IP address."
  default     = "15.0.0.0/16"
}

variable "aks_subnet_name" {
  type        = 
  description = "AKS Subnet Name."
  default     = "kubesubnet"
}

variable "app_gateway_name" {
  type        = 
  description = "Name of the Application Gateway."
  default     = "ApplicationGateway1"
}

variable "app_gateway_sku" {
  type        = 
  description = "Name of the Application Gateway SKU."
  default     = "Standard_v2"
}

variable "app_gateway_subnet_address_prefix" {
  type        = 
  description = "Containers DNS server IP address."
  default     = "15.1.0.0/16"
}

variable "app_gateway_tier" {
  type        = 
  description = "Tier of the Application Gateway SKU."
  default     = "Standard_v2"
}

variable "kubernetes_version" {
  type        = 
  description = "The version of Kubernetes."
  default     = "1.11.5"
}

variable "location" {
  type        = 
  description = "Location of the cluster."
  default     = "Central US"
}

variable "public_ssh_key_path" {
  type        = 
  description = "Public key path for SSH."
  default     = "~/.ssh/id_rsa.pub"
}

variable "resource_group_name" {
  type        = 
  description = "Name of the resource group."
  default     = "aksrg1"
}

variable "tags" {
  type        = 
  description = ""
  default     = {
  "source": "terraform"
}
}

variable "virtual_network_address_prefix" {
  type        = 
  description = "Containers DNS server IP address."
  default     = "15.0.0.0/8"
}

variable "virtual_network_name" {
  type        = 
  description = "Virtual network name"
  default     = "aksVirtualNetwork"
}

variable "vm_user_name" {
  type        = 
  description = "User name for the VM"
  default     = "vmuser1"
}


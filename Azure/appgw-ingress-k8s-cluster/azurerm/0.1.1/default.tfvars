
aks_agent_count = 3

aks_agent_os_disk_size = 40

aks_agent_vm_size = "Standard_D3_v2"

aks_dns_prefix = "aks"

aks_dns_service_ip = "10.0.0.10"

aks_docker_bridge_cidr = "172.17.0.1/16"

aks_enable_rbac = "false"

aks_name = "aks-cluster1"

aks_service_cidr = "10.0.0.0/16"

aks_service_principal_app_id = ""

aks_service_principal_client_secret = ""

aks_service_principal_object_id = ""

aks_subnet_address_prefix = "15.0.0.0/16"

aks_subnet_name = "kubesubnet"

app_gateway_name = "ApplicationGateway1"

app_gateway_sku = "Standard_v2"

app_gateway_subnet_address_prefix = "15.1.0.0/16"

app_gateway_tier = "Standard_v2"

kubernetes_version = "1.11.5"

location = "Central US"

public_ssh_key_path = "~/.ssh/id_rsa.pub"

resource_group_name = "aksrg1"

tags = {
  "source": "terraform"
}

virtual_network_address_prefix = "15.0.0.0/8"

virtual_network_name = "aksVirtualNetwork"

vm_user_name = "vmuser1"



address_space = [
  "10.0.0.0/16"
]

bgp_community = null

ddos_protection_plan = null

dns_servers = []

nsg_ids = {}

resource_group_name = 

route_tables_ids = {}

subnet_delegation = {}

subnet_enforce_private_link_endpoint_network_policies = {}

subnet_enforce_private_link_service_network_policies = {}

subnet_names = [
  "subnet1",
  "subnet2",
  "subnet3"
]

subnet_prefixes = [
  "10.0.1.0/24",
  "10.0.2.0/24",
  "10.0.3.0/24"
]

subnet_service_endpoints = {}

tags = {
  "ENV": "test"
}

tracing_tags_enabled = false

tracing_tags_prefix = "avm_"

use_for_each = 

vnet_location = 

vnet_name = "acctvnet"


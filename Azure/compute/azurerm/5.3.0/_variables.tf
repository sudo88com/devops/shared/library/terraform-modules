
variable "admin_password" {
  type        = string
  description = "The admin password to be used on the VMSS that will be deployed. The password must meet the complexity requirements of Azure."
  default     = ""
}

variable "admin_username" {
  type        = string
  description = "The admin username of the VM that will be deployed."
  default     = "azureuser"
}

variable "allocation_method" {
  type        = string
  description = "Defines how an IP address is assigned. Options are Static or Dynamic."
  default     = "Dynamic"
}

variable "as_platform_fault_domain_count" {
  type        = number
  description = "(Optional) Specifies the number of fault domains that are used. Defaults to `2`. Changing this forces a new resource to be created."
  default     = 2
}

variable "as_platform_update_domain_count" {
  type        = number
  description = "(Optional) Specifies the number of update domains that are used. Defaults to `2`. Changing this forces a new resource to be created."
  default     = 2
}

variable "availability_set_enabled" {
  type        = bool
  description = "(Optional) Enable or Disable availability set.  Default is `true` (enabled)."
  default     = true
}

variable "boot_diagnostics" {
  type        = bool
  description = "(Optional) Enable or Disable boot diagnostics."
  default     = false
}

variable "boot_diagnostics_sa_type" {
  type        = string
  description = "(Optional) Storage account type for boot diagnostics."
  default     = "Standard_LRS"
}

variable "custom_data" {
  type        = string
  description = "The custom data to supply to the machine. This can be used as a cloud-init for Linux systems."
  default     = ""
}

variable "data_disk_size_gb" {
  type        = number
  description = "Storage data disk size size."
  default     = 30
}

variable "data_sa_type" {
  type        = string
  description = "Data Disk Storage Account type."
  default     = "Standard_LRS"
}

variable "delete_data_disks_on_termination" {
  type        = bool
  description = "Delete data disks when machine is terminated."
  default     = false
}

variable "delete_os_disk_on_termination" {
  type        = bool
  description = "Delete OS disk when machine is terminated."
  default     = false
}

variable "enable_accelerated_networking" {
  type        = bool
  description = "(Optional) Enable accelerated networking on Network interface."
  default     = false
}

variable "enable_ip_forwarding" {
  type        = bool
  description = "(Optional) Should IP Forwarding be enabled? Defaults to `false`."
  default     = false
}

variable "enable_ssh_key" {
  type        = bool
  description = "(Optional) Enable ssh key authentication in Linux virtual Machine."
  default     = true
}

variable "external_boot_diagnostics_storage" {
  type        = object({
    uri = string
  })
  description = "(Optional) The Storage Account's Blob Endpoint which should hold the virtual machine's diagnostic files. Set this argument would disable the creation of `azurerm_storage_account` resource."
  default     = null
}

variable "extra_disks" {
  type        = list(object({
    name = string
    size = number
  }))
  description = "(Optional) List of extra data disks attached to each virtual machine."
  default     = []
}

variable "extra_ssh_keys" {
  type        = list(string)
  description = "Same as ssh_key, but allows for setting multiple public keys. Set your first key in ssh_key, and the extras here."
  default     = []
}

variable "identity_ids" {
  type        = list(string)
  description = "Specifies a list of user managed identity ids to be assigned to the VM."
  default     = []
}

variable "identity_type" {
  type        = string
  description = "The Managed Service Identity Type of this Virtual Machine."
  default     = ""
}

variable "is_marketplace_image" {
  type        = bool
  description = "Boolean flag to notify when the image comes from the marketplace."
  default     = false
}

variable "is_windows_image" {
  type        = bool
  description = "Boolean flag to notify when the custom image is windows based."
  default     = false
}

variable "license_type" {
  type        = string
  description = "Specifies the BYOL Type for this Virtual Machine. This is only applicable to Windows Virtual Machines. Possible values are Windows_Client and Windows_Server"
  default     = null
}

variable "location" {
  type        = string
  description = "(Optional) The location in which the resources will be created."
  default     = null
}

variable "managed_data_disk_encryption_set_id" {
  type        = string
  description = "(Optional) The disk encryption set ID for the managed data disk attached using the azurerm_virtual_machine_data_disk_attachment resource."
  default     = null
}

variable "name_template_availability_set" {
  type        = string
  description = "The name template for the availability set. The following replacements are automatically made: `${vm_hostname}` => `var.vm_hostname`. All other text can be set as desired."
  default     = "${vm_hostname}-avset"
}

variable "name_template_data_disk" {
  type        = string
  description = "The name template for the data disks. The following replacements are automatically made: `${vm_hostname}` => `var.vm_hostname`, `${host_number}` => 'host index', `${data_disk_number}` => 'data disk index'. All other text can be set as desired."
  default     = "${vm_hostname}-datadisk-${host_number}-${data_disk_number}"
}

variable "name_template_extra_disk" {
  type        = string
  description = "The name template for the extra disks. The following replacements are automatically made: `${vm_hostname}` => `var.vm_hostname`, `${host_number}` => 'host index', `${extra_disk_name}` => 'name of extra disk'. All other text can be set as desired."
  default     = "${vm_hostname}-extradisk-${host_number}-${extra_disk_name}"
}

variable "name_template_network_interface" {
  type        = string
  description = "The name template for the network interface. The following replacements are automatically made: `${vm_hostname}` => `var.vm_hostname`, `${host_number}` => 'host index'. All other text can be set as desired."
  default     = "${vm_hostname}-nic-${host_number}"
}

variable "name_template_network_security_group" {
  type        = string
  description = "The name template for the network security group. The following replacements are automatically made: `${vm_hostname}` => `var.vm_hostname`. All other text can be set as desired."
  default     = "${vm_hostname}-nsg"
}

variable "name_template_public_ip" {
  type        = string
  description = "The name template for the public ip. The following replacements are automatically made: `${vm_hostname}` => `var.vm_hostname`, `${ip_number}` => 'public ip index'. All other text can be set as desired."
  default     = "${vm_hostname}-pip-${ip_number}"
}

variable "name_template_vm_linux" {
  type        = string
  description = "The name template for the Linux virtual machine. The following replacements are automatically made: `${vm_hostname}` => `var.vm_hostname`, `${host_number}` => 'host index'. All other text can be set as desired."
  default     = "${vm_hostname}-vmLinux-${host_number}"
}

variable "name_template_vm_linux_os_disk" {
  type        = string
  description = "The name template for the Linux VM OS disk. The following replacements are automatically made: `${vm_hostname}` => `var.vm_hostname`, `${host_number}` => 'host index'. All other text can be set as desired."
  default     = "osdisk-${vm_hostname}-${host_number}"
}

variable "name_template_vm_windows" {
  type        = string
  description = "The name template for the Windows virtual machine. The following replacements are automatically made: `${vm_hostname}` => `var.vm_hostname`, `${host_number}` => 'host index'. All other text can be set as desired."
  default     = "${vm_hostname}-vmWindows-${host_number}"
}

variable "name_template_vm_windows_os_disk" {
  type        = string
  description = "The name template for the Windows VM OS disk. The following replacements are automatically made: `${vm_hostname}` => `var.vm_hostname`, `${host_number}` => 'host index'. All other text can be set as desired."
  default     = "${vm_hostname}-osdisk-${host_number}"
}

variable "nb_data_disk" {
  type        = number
  description = "(Optional) Number of the data disks attached to each virtual machine."
  default     = 0
}

variable "nb_instances" {
  type        = number
  description = "Specify the number of vm instances."
  default     = 1
}

variable "nb_public_ip" {
  type        = number
  description = "Number of public IPs to assign corresponding to one IP per vm. Set to 0 to not assign any public IP addresses."
  default     = 1
}

variable "nested_data_disks" {
  type        = bool
  description = "(Optional) When `true`, use nested data disks directly attached to the VM.  When `false`, use azurerm_virtual_machine_data_disk_attachment resource to attach the data disks after the VM is created.  Default is `true`."
  default     = true
}

variable "network_security_group" {
  type        = object({
    id = string
  })
  description = "The network security group we'd like to bind with virtual machine. Set this variable will disable the creation of `azurerm_network_security_group` and `azurerm_network_security_rule` resources."
  default     = null
}

variable "os_profile_secrets" {
  type        = list(map(string))
  description = "Specifies a list of certificates to be installed on the VM, each list item is a map with the keys source_vault_id, certificate_url and certificate_store."
  default     = []
}

variable "public_ip_dns" {
  type        = list(string)
  description = "Optional globally unique per datacenter region domain name label to apply to each public ip address. e.g. thisvar.varlocation.cloudapp.azure.com where you specify only thisvar here. This is an array of names which will pair up sequentially to the number of public ips defined in var.nb_public_ip. One name or empty string is required for every public ip. If no public ip is desired, then set this to an array with a single empty string."
  default     = [
  null
]
}

variable "public_ip_sku" {
  type        = string
  description = "Defines the SKU of the Public IP. Accepted values are Basic and Standard. Defaults to Basic."
  default     = "Basic"
}

variable "remote_port" {
  type        = string
  description = "Remote tcp port to be used for access to the vms created via the nsg applied to the nics."
  default     = ""
}

variable "resource_group_name" {
  type        = string
  description = "The name of the resource group in which the resources will be created."
  default     = ""
}

variable "source_address_prefixes" {
  type        = list(string)
  description = "(Optional) List of source address prefixes allowed to access var.remote_port."
  default     = [
  "0.0.0.0/0"
]
}

variable "ssh_key" {
  type        = string
  description = "Path to the public key to be used for ssh access to the VM. Only used with non-Windows vms and can be left as-is even if using Windows vms. If specifying a path to a certification on a Windows machine to provision a linux vm use the / in the path versus backslash.e.g. c : /home/id_rsa.pub."
  default     = "~/.ssh/id_rsa.pub"
}

variable "ssh_key_values" {
  type        = list(string)
  description = "List of Public SSH Keys values to be used for ssh access to the VMs."
  default     = []
}

variable "storage_account_type" {
  type        = string
  description = "Defines the type of storage account to be created. Valid options are Standard_LRS, Standard_ZRS, Standard_GRS, Standard_RAGRS, Premium_LRS."
  default     = "Premium_LRS"
}

variable "storage_os_disk_size_gb" {
  type        = number
  description = "(Optional) Specifies the size of the data disk in gigabytes."
  default     = null
}

variable "tags" {
  type        = map(string)
  description = "A map of the tags to use on the resources that are deployed with this module."
  default     = {
  "source": "terraform"
}
}

variable "tracing_tags_enabled" {
  type        = bool
  description = "Whether enable tracing tags that generated by BridgeCrew Yor."
  default     = false
}

variable "tracing_tags_prefix" {
  type        = string
  description = "Default prefix for generated tracing tags"
  default     = "avm_"
}

variable "vm_extension" {
  type        = object({
    name                        = string
    publisher                   = string
    type                        = string
    type_handler_version        = string
    auto_upgrade_minor_version  = optional(bool)
    automatic_upgrade_enabled   = optional(bool)
    failure_suppression_enabled = optional(bool, false)
    settings                    = optional(string)
    protected_settings          = optional(string)
    protected_settings_from_key_vault = optional(object({
      secret_url      = string
      source_vault_id = string
    }))
  })
  description = "(Deprecated) This variable has been superseded by the `vm_extensions`. Argument to create `azurerm_virtual_machine_extension` resource, the argument descriptions could be found at [the document](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_machine_extension)."
  default     = null
}

variable "vm_extensions" {
  type        = set(object({
    name                        = string
    publisher                   = string
    type                        = string
    type_handler_version        = string
    auto_upgrade_minor_version  = optional(bool)
    automatic_upgrade_enabled   = optional(bool)
    failure_suppression_enabled = optional(bool, false)
    settings                    = optional(string)
    protected_settings          = optional(string)
    protected_settings_from_key_vault = optional(object({
      secret_url      = string
      source_vault_id = string
    }))
  }))
  description = "Argument to create `azurerm_virtual_machine_extension` resource, the argument descriptions could be found at [the document](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_machine_extension)."
  default     = []
}

variable "vm_hostname" {
  type        = string
  description = "local name of the Virtual Machine."
  default     = "myvm"
}

variable "vm_os_id" {
  type        = string
  description = "The resource ID of the image that you want to deploy if you are using a custom image.Note, need to provide is_windows_image = true for windows custom images."
  default     = ""
}

variable "vm_os_offer" {
  type        = string
  description = "The name of the offer of the image that you want to deploy. This is ignored when vm_os_id or vm_os_simple are provided."
  default     = ""
}

variable "vm_os_publisher" {
  type        = string
  description = "The name of the publisher of the image that you want to deploy. This is ignored when vm_os_id or vm_os_simple are provided."
  default     = ""
}

variable "vm_os_simple" {
  type        = string
  description = "Specify UbuntuServer, WindowsServer, RHEL, openSUSE-Leap, CentOS, Debian, CoreOS and SLES to get the latest image version of the specified os.  Do not provide this value if a custom value is used for vm_os_publisher, vm_os_offer, and vm_os_sku."
  default     = ""
}

variable "vm_os_sku" {
  type        = string
  description = "The sku of the image that you want to deploy. This is ignored when vm_os_id or vm_os_simple are provided."
  default     = ""
}

variable "vm_os_version" {
  type        = string
  description = "The version of the image that you want to deploy. This is ignored when vm_os_id or vm_os_simple are provided."
  default     = "latest"
}

variable "vm_size" {
  type        = string
  description = "Specifies the size of the virtual machine."
  default     = "Standard_D2s_v3"
}

variable "vnet_subnet_id" {
  type        = string
  description = "The subnet id of the virtual network where the virtual machines will reside."
  default     = ""
}

variable "zone" {
  type        = string
  description = "(Optional) The Availability Zone which the Virtual Machine should be allocated in, only one zone would be accepted. If set then this module won't create `azurerm_availability_set` resource. Changing this forces a new resource to be created."
  default     = null
}


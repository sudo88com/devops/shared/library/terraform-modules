
admin_password = ""

admin_username = "azureuser"

allocation_method = "Dynamic"

as_platform_fault_domain_count = 2

as_platform_update_domain_count = 2

availability_set_enabled = true

boot_diagnostics = false

boot_diagnostics_sa_type = "Standard_LRS"

custom_data = ""

data_disk_size_gb = 30

data_sa_type = "Standard_LRS"

delete_data_disks_on_termination = false

delete_os_disk_on_termination = false

enable_accelerated_networking = false

enable_ip_forwarding = false

enable_ssh_key = true

external_boot_diagnostics_storage = null

extra_disks = []

extra_ssh_keys = []

identity_ids = []

identity_type = ""

is_marketplace_image = false

is_windows_image = false

license_type = null

location = null

managed_data_disk_encryption_set_id = null

name_template_availability_set = "${vm_hostname}-avset"

name_template_data_disk = "${vm_hostname}-datadisk-${host_number}-${data_disk_number}"

name_template_extra_disk = "${vm_hostname}-extradisk-${host_number}-${extra_disk_name}"

name_template_network_interface = "${vm_hostname}-nic-${host_number}"

name_template_network_security_group = "${vm_hostname}-nsg"

name_template_public_ip = "${vm_hostname}-pip-${ip_number}"

name_template_vm_linux = "${vm_hostname}-vmLinux-${host_number}"

name_template_vm_linux_os_disk = "osdisk-${vm_hostname}-${host_number}"

name_template_vm_windows = "${vm_hostname}-vmWindows-${host_number}"

name_template_vm_windows_os_disk = "${vm_hostname}-osdisk-${host_number}"

nb_data_disk = 0

nb_instances = 1

nb_public_ip = 1

nested_data_disks = true

network_security_group = null

os_profile_secrets = []

public_ip_dns = [
  null
]

public_ip_sku = "Basic"

remote_port = ""

resource_group_name = 

source_address_prefixes = [
  "0.0.0.0/0"
]

ssh_key = "~/.ssh/id_rsa.pub"

ssh_key_values = []

storage_account_type = "Premium_LRS"

storage_os_disk_size_gb = null

tags = {
  "source": "terraform"
}

tracing_tags_enabled = false

tracing_tags_prefix = "avm_"

vm_extension = null

vm_extensions = []

vm_hostname = "myvm"

vm_os_id = ""

vm_os_offer = ""

vm_os_publisher = ""

vm_os_simple = ""

vm_os_sku = ""

vm_os_version = "latest"

vm_size = "Standard_D2s_v3"

vnet_subnet_id = 

zone = null


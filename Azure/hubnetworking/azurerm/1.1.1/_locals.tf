
locals {
  hub_virtual_networks = var.hub_virtual_networks
  tracing_tags_enabled = var.tracing_tags_enabled
  tracing_tags_prefix = var.tracing_tags_prefix
}


module "hubnetworking" {
  source = "terraform-aws-modules/hubnetworking/aws"
  version = "1.1.1"
  hub_virtual_networks = var.hub_virtual_networks
  tracing_tags_enabled = var.tracing_tags_enabled
  tracing_tags_prefix = var.tracing_tags_prefix
}

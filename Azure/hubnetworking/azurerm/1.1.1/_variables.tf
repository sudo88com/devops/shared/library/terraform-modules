
variable "hub_virtual_networks" {
  type        = map(object({
    name                            = string
    address_space                   = list(string)
    location                        = string
    resource_group_name             = string
    route_table_name                = optional(string)
    bgp_community                   = optional(string)
    ddos_protection_plan_id         = optional(string)
    dns_servers                     = optional(list(string))
    flow_timeout_in_minutes         = optional(number, 4)
    mesh_peering_enabled            = optional(bool, true)
    resource_group_creation_enabled = optional(bool, true)
    resource_group_lock_enabled     = optional(bool, true)
    resource_group_lock_name        = optional(string)
    resource_group_tags             = optional(map(string))
    routing_address_space           = optional(list(string), [])
    hub_router_ip_address           = optional(string)
    tags                            = optional(map(string), {})

    route_table_entries = optional(set(object({
      name           = string
      address_prefix = string
      next_hop_type  = string

      has_bgp_override    = optional(bool, false)
      next_hop_ip_address = optional(string)
    })), [])

    subnets = optional(map(object(
      {
        address_prefixes = list(string)
        nat_gateway = optional(object({
          id = string
        }))
        network_security_group = optional(object({
          id = string
        }))
        private_endpoint_network_policies_enabled     = optional(bool, true)
        private_link_service_network_policies_enabled = optional(bool, true)
        assign_generated_route_table                  = optional(bool, true)
        external_route_table_id                       = optional(string)
        service_endpoints                             = optional(set(string))
        service_endpoint_policy_ids                   = optional(set(string))
        delegations = optional(list(
          object(
            {
              name = string
              service_delegation = object({
                name    = string
                actions = optional(list(string))
              })
            }
          )
        ))
      }
    )), {})

    firewall = optional(object({
      sku_name                         = string
      sku_tier                         = string
      subnet_address_prefix            = string
      dns_servers                      = optional(list(string))
      firewall_policy_id               = optional(string)
      management_subnet_address_prefix = optional(string, null)
      name                             = optional(string)
      private_ip_ranges                = optional(list(string))
      subnet_route_table_id            = optional(string)
      tags                             = optional(map(string))
      threat_intel_mode                = optional(string, "Alert")
      zones                            = optional(list(string))
      default_ip_configuration = optional(object({
        name = optional(string)
        public_ip_config = optional(object({
          ip_version = optional(string)
          name       = optional(string)
          sku_tier   = optional(string, "Regional")
          zones      = optional(set(string))
        }))
      }))
      management_ip_configuration = optional(object({
        name = optional(string)
        public_ip_config = optional(object({
          ip_version = optional(string)
          name       = optional(string)
          sku_tier   = optional(string, "Regional")
          zones      = optional(set(string))
        }))
      }))
    }))
  }))
  description = "A map of the hub virtual networks to create. The map key is an arbitrary value to avoid Terraform's restriction that map keys must be known at plan time.\n\n### Mandatory fields\n\n- `name` - The name of the Virtual Network.\n- `address_space` - A list of IPv4 address spaces that are used by this virtual network in CIDR format, e.g. `[\"192.168.0.0/24\"]`.\n- `location` - The Azure location where the virtual network should be created.\n- `resource_group_name` - The name of the resource group in which the virtual network should be created.\n\n### Optional fields\n\n- `bgp_community` - The BGP community associated with the virtual network.\n- `ddos_protection_plan_id` - The ID of the DDoS protection plan associated with the virtual network.\n- `dns_servers` - A list of DNS servers IP addresses for the virtual network.\n- `flow_timeout_in_minutes` - The flow timeout in minutes for the virtual network. Default `4`.\n- `mesh_peering_enabled` - Should the virtual network be peered to other hub networks with this flag enabled? Default `true`.\n- `resource_group_creation_enabled` - Should the resource group for this virtual network be created by this module? Default `true`.\n- `resource_group_lock_enabled` - Should the resource group for this virtual network be locked? Default `true`.\n- `resource_group_lock_name` - The name of the resource group lock.\n- `resource_group_tags` - A map of tags to apply to the resource group.\n- `routing_address_space` - A list of IPv4 address spaces in CIDR format that are used for routing to this hub, e.g. `[\"192.168.0.0\",\"172.16.0.0/12\"]`.\n- `hub_router_ip_address` - If not using Azure Firewall, this is the IP address of the hub router. This is used to create route table entries for other hub networks.\n- `tags` - A map of tags to apply to the virtual network.\n\n#### Route table entries\n\n- `route_table_entries` - (Optional) A set of additional route table entries to add to the route table for this hub network. Default empty `[]`. The value is an object with the following fields:\n  - `name` - The name of the route table entry.\n  - `address_prefix` - The address prefix to match for this route table entry.\n  - `next_hop_type` - The type of the next hop. Possible values include `Internet`, `VirtualAppliance`, `VirtualNetworkGateway`, `VnetLocal`, `None`.\n  - `has_bgp_override` - Should the BGP override be enabled for this route table entry? Default `false`.\n  - `next_hop_ip_address` - The IP address of the next hop. Required if `next_hop_type` is `VirtualAppliance`.\n\n#### Subnets\n\n- `subnets` - (Optional) A map of subnets to create in the virtual network. The value is an object with the following fields:\n  - `address_prefixes` - The IPv4 address prefixes to use for the subnet in CIDR format.\n  - `nat_gateway` - (Optional) An object with the following fields:\n    - `id` - The ID of the NAT Gateway which should be associated with the Subnet. Changing this forces a new resource to be created.\n  - `network_security_group` - (Optional) An object with the following fields:\n    - `id` - The ID of the Network Security Group which should be associated with the Subnet. Changing this forces a new association to be created.\n  - `private_endpoint_network_policies_enabled` - (Optional) Enable or Disable network policies for the private endpoint on the subnet. Setting this to true will Enable the policy and setting this to false will Disable the policy. Defaults to true.\n  - `private_link_service_network_policies_enabled` - (Optional) Enable or Disable network policies for the private link service on the subnet. Setting this to true will Enable the policy and setting this to false will Disable the policy. Defaults to true.\n  - `assign_generated_route_table` - (Optional) Should the Route Table generated by this module be associated with this Subnet? Default `true`. Cannot be used with `external_route_table_id`.\n  - `external_route_table_id` - (Optional) The ID of the Route Table which should be associated with the Subnet. Changing this forces a new association to be created. Cannot be used with `assign_generated_route_table`.\n  - `service_endpoints` - (Optional) The list of Service endpoints to associate with the subnet.\n  - `service_endpoint_policy_ids` - (Optional) The list of Service Endpoint Policy IDs to associate with the subnet.\n  - `service_endpoint_policy_assignment_enabled` - (Optional) Should the Service Endpoint Policy be assigned to the subnet? Default `true`.\n  - `delegation` - (Optional) An object with the following fields:\n    - `name` - The name of the delegation.\n    - `service_delegation` - An object with the following fields:\n      - `name` - The name of the service delegation.\n      - `actions` - A list of actions that should be delegated, the list is specific to the service being delegated.\n\n#### Azure Firewall\n\n- `firewall` - (Optional) An object with the following fields:\n  - `sku_name` - The name of the SKU to use for the Azure Firewall. Possible values include `AZFW_Hub`, `AZFW_VNet`.\n  - `sku_tier` - The tier of the SKU to use for the Azure Firewall. Possible values include `Basic`, ``Standard`, `Premium`.\n  - `subnet_address_prefix` - The IPv4 address prefix to use for the Azure Firewall subnet in CIDR format. Needs to be a part of the virtual network's address space.\n  - `dns_servers` - (Optional) A list of DNS server IP addresses for the Azure Firewall.\n  - `firewall_policy_id` - (Optional) The resource id of the Azure Firewall Policy to associate with the Azure Firewall.\n  - `management_subnet_address_prefix` - (Optional) The IPv4 address prefix to use for the Azure Firewall management subnet in CIDR format. Needs to be a part of the virtual network's address space.\n  - `name` - (Optional) The name of the firewall resource. If not specified will use `afw-{vnetname}`.\n  - `private_ip_ranges` - (Optional) A list of private IP ranges to use for the Azure Firewall, to which the firewall will not NAT traffic. If not specified will use RFC1918.\n  - `subnet_route_table_id` = (Optional) The resource id of the Route Table which should be associated with the Azure Firewall subnet. If not specified the module will assign the generated route table.\n  - `tags` - (Optional) A map of tags to apply to the Azure Firewall. If not specified\n  - `threat_intel_mode` - (Optional) The threat intelligence mode for the Azure Firewall. Possible values include `Alert`, `Deny`, `Off`.\n  - `zones` - (Optional) A list of availability zones to use for the Azure Firewall. If not specified will be `null`.\n  - `default_ip_configuration` - (Optional) An object with the following fields. If not specified the defaults below will be used:\n    - `name` - (Optional) The name of the default IP configuration. If not specified will use `default`.\n    - `public_ip_config` - (Optional) An object with the following fields:\n      - `name` - (Optional) The name of the public IP configuration. If not specified will use `pip-afw-{vnetname}`.\n      - `zones` - (Optional) A list of availability zones to use for the public IP configuration. If not specified will be `null`.\n      - `ip_version` - (Optional) The IP version to use for the public IP configuration. Possible values include `IPv4`, `IPv6`. If not specified will be `IPv4`.\n      - `sku_tier` - (Optional) The SKU tier to use for the public IP configuration. Possible values include `Regional`, `Global`. If not specified will be `Regional`.\n  - `management_ip_configuration` - (Optional) An object with the following fields. If not specified the defaults below will be used:\n    - `name` - (Optional) The name of the management IP configuration. If not specified will use `defaultMgmt`.\n    - `public_ip_config` - (Optional) An object with the following fields:\n      - `name` - (Optional) The name of the public IP configuration. If not specified will use `pip-afw-mgmt-<Map Key>`.\n      - `zones` - (Optional) A list of availability zones to use for the public IP configuration. If not specified will be `null`.\n      - `ip_version` - (Optional) The IP version to use for the public IP configuration. Possible values include `IPv4`, `IPv6`. If not specified will be `IPv4`.\n      - `sku_tier` - (Optional) The SKU tier to use for the public IP configuration. Possible values include `Regional`, `Global`. If not specified will be `Regional`.\n"
  default     = {}
}

variable "tracing_tags_enabled" {
  type        = bool
  description = "Whether enable tracing tags that generated by BridgeCrew Yor."
  default     = false
}

variable "tracing_tags_prefix" {
  type        = string
  description = "Default prefix for generated tracing tags"
  default     = "avm_"
}


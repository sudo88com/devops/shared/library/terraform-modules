
output "firewalls" {
  description = "A curated output of the firewalls created by this module."
  value       = module.hubnetworking.firewalls
}

output "hub_route_tables" {
  description = "A curated output of the route tables created by this module."
  value       = module.hubnetworking.hub_route_tables
}

output "resource_groups" {
  description = "A curated output of the resource groups created by this module."
  value       = module.hubnetworking.resource_groups
}

output "virtual_networks" {
  description = "A curated output of the virtual networks created by this module."
  value       = module.hubnetworking.virtual_networks
}


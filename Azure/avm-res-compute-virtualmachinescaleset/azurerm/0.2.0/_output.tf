
output "resource" {
  description = "All attributes of the Virtual Machine Scale Set resource."
  value       = module.avm-res-compute-virtualmachinescaleset.resource
}

output "resource_id" {
  description = "The ID of the Virtual Machine Scale Set."
  value       = module.avm-res-compute-virtualmachinescaleset.resource_id
}

output "resource_name" {
  description = "The name of the Virtual Machine Scale Set."
  value       = module.avm-res-compute-virtualmachinescaleset.resource_name
}


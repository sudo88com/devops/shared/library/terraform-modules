
additional_capabilities = null

admin_password = 

admin_ssh_keys = 

automatic_instance_repair = {
  "enabled": true
}

boot_diagnostics = null

capacity_reservation_group_id = null

data_disk = null

enable_telemetry = true

encryption_at_host_enabled = null

eviction_policy = null

extension = 

extension_operations_enabled = null

extension_protected_setting = 

extensions_time_budget = null

identity = null

instances = null

license_type = null

location = 

lock = null

max_bid_price = -1

name = 

network_interface = null

os_disk = {
  "caching": "ReadWrite",
  "storage_account_type": "Premium_LRS"
}

os_profile = null

plan = null

platform_fault_domain_count = 

priority = "Regular"

priority_mix = null

proximity_placement_group_id = null

resource_group_name = 

role_assignments = {}

single_placement_group = null

sku_name = null

source_image_id = null

source_image_reference = null

tags = null

termination_notification = null

timeouts = null

user_data_base64 = 

zone_balance = null

zones = [
  "1",
  "2",
  "3"
]


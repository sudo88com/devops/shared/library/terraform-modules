
default_subplan = null

default_tier = "Standard"

location = "West Europe"

mdc_databases_plans = [
  "OpenSourceRelationalDatabases",
  "SqlServers",
  "SqlServerVirtualMachines",
  "CosmosDbs"
]

mdc_plans_list = [
  "AppServices",
  "Arm",
  "CloudPosture",
  "Containers",
  "KeyVaults",
  "OpenSourceRelationalDatabases",
  "SqlServers",
  "SqlServerVirtualMachines",
  "CosmosDbs",
  "StorageAccounts",
  "VirtualMachines",
  "Api"
]

subplans = {}

tracing_tags_enabled = false

tracing_tags_prefix = "avm_"


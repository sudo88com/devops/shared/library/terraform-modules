
locals {
  default_subplan = var.default_subplan
  default_tier = var.default_tier
  location = var.location
  mdc_databases_plans = var.mdc_databases_plans
  mdc_plans_list = var.mdc_plans_list
  subplans = var.subplans
  tracing_tags_enabled = var.tracing_tags_enabled
  tracing_tags_prefix = var.tracing_tags_prefix
}


output "plans_details" {
  description = "All plans details"
  value       = module.mdc-defender-plans-azure.plans_details
}

output "subscription_pricing_id" {
  description = "The subscription pricing ID"
  value       = module.mdc-defender-plans-azure.subscription_pricing_id
}


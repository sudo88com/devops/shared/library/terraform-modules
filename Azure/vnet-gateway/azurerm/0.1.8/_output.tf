
output "local_network_gateways" {
  description = "A curated output of the Local Network Gateways created by this module."
  value       = module.vnet-gateway.local_network_gateways
}

output "public_ip_addresses" {
  description = "A curated output of the Public IP Addresses created by this module."
  value       = module.vnet-gateway.public_ip_addresses
}

output "route_table" {
  description = "A curated output of the Route Table created by this module."
  value       = module.vnet-gateway.route_table
}

output "subnet" {
  description = "A curated output of the GatewaySubnet created by this module."
  value       = module.vnet-gateway.subnet
}

output "virtual_network_gateway" {
  description = "A curated output of the Virtual Network Gateway created by this module."
  value       = module.vnet-gateway.virtual_network_gateway
}

output "virtual_network_gateway_connections" {
  description = "A curated output of the Virtual Network Gateway Connections created by this module."
  value       = module.vnet-gateway.virtual_network_gateway_connections
}


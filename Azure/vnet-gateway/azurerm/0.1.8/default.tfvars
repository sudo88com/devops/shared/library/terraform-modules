
default_tags = {}

edge_zone = null

express_route_circuits = {}

ip_configurations = {}

local_network_gateways = {}

location = 

name = 

route_table_bgp_route_propagation_enabled = true

route_table_creation_enabled = false

route_table_name = null

route_table_tags = {}

sku = 

subnet_address_prefix = ""

subnet_id = ""

tags = {}

tracing_tags_enabled = false

tracing_tags_prefix = "avm_"

type = 

virtual_network_name = 

virtual_network_resource_group_name = 

vpn_active_active_enabled = false

vpn_bgp_enabled = false

vpn_bgp_settings = null

vpn_generation = null

vpn_point_to_site = null

vpn_private_ip_address_enabled = null

vpn_type = "RouteBased"


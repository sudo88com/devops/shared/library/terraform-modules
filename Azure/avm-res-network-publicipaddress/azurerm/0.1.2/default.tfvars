
allocation_method = "Static"

ddos_protection_mode = "VirtualNetworkInherited"

ddos_protection_plan_id = null

diagnostic_settings = {}

domain_name_label = null

edge_zone = null

enable_telemetry = true

idle_timeout_in_minutes = 4

ip_tags = {}

ip_version = "IPv4"

location = 

lock = {}

name = 

public_ip_prefix_id = null

resource_group_name = 

reverse_fqdn = null

role_assignments = {}

sku = "Standard"

sku_tier = "Regional"

tags = null

zones = [
  1,
  2,
  3
]



output "credentials" {
  description = "This value returns the vcenter and nsxt cloudadmin credential values."
  value       = module.avm-res-avs-privatecloud.credentials
}

output "identity" {
  description = "This output returns the managed identity values if the managed identity has been enabled on the module."
  value       = module.avm-res-avs-privatecloud.identity
}

output "resource" {
  description = "This output returns the full private cloud resource object properties."
  value       = module.avm-res-avs-privatecloud.resource
}

output "resource_id" {
  description = "The azure resource if of the private cloud."
  value       = module.avm-res-avs-privatecloud.resource_id
}

output "system_assigned_mi_principal_id" {
  description = "The principal id of the system managed identity assigned to the virtual machine"
  value       = module.avm-res-avs-privatecloud.system_assigned_mi_principal_id
}


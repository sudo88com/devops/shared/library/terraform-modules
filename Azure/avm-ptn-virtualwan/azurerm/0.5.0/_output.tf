
output "er_gw_id" {
  description = "ExpressRoute Gateway ID"
  value       = module.avm-ptn-virtualwan.er_gw_id
}

output "fw" {
  description = "Firewall Name"
  value       = module.avm-ptn-virtualwan.fw
}

output "p2s_vpn_gw_id" {
  description = "P2S VPN Gateway ID"
  value       = module.avm-ptn-virtualwan.p2s_vpn_gw_id
}

output "resource" {
  description = "The full resource outputs."
  value       = module.avm-ptn-virtualwan.resource
}

output "resource_group_name" {
  description = "Resource Group Name"
  value       = module.avm-ptn-virtualwan.resource_group_name
}

output "resource_id" {
  description = "Virtual WAN ID"
  value       = module.avm-ptn-virtualwan.resource_id
}

output "s2s_vpn_gw" {
  description = "S2S VPN Gateway Objects"
  value       = module.avm-ptn-virtualwan.s2s_vpn_gw
}

output "s2s_vpn_gw_id" {
  description = "S2S VPN Gateway ID"
  value       = module.avm-ptn-virtualwan.s2s_vpn_gw_id
}

output "virtual_hub_id" {
  description = "Virtual Hub ID"
  value       = module.avm-ptn-virtualwan.virtual_hub_id
}

output "virtual_wan_id" {
  description = "Virtual WAN ID"
  value       = module.avm-ptn-virtualwan.virtual_wan_id
}


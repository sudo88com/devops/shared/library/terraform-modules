
module "routetable" {
  source = "terraform-aws-modules/routetable/aws"
  version = "1.0.0"
  disable_bgp_route_propagation = var.disable_bgp_route_propagation
  location = var.location
  resource_group_name = var.resource_group_name
  route_names = var.route_names
  route_nexthop_types = var.route_nexthop_types
  route_prefixes = var.route_prefixes
  route_table_name = var.route_table_name
  tags = var.tags
}

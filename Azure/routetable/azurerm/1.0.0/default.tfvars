
disable_bgp_route_propagation = "true"

location = "South Central US"

resource_group_name = "myapp-rg"

route_names = [
  "subnet1"
]

route_nexthop_types = [
  "VnetLocal"
]

route_prefixes = [
  "10.0.1.0/24"
]

route_table_name = "routetable"

tags = {
  "tag1": "",
  "tag2": ""
}


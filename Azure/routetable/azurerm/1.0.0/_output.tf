
output "routetable_id" {
  description = "The id of the newly created Route Table"
  value       = module.routetable.routetable_id
}



locals {
  copy_paste_enabled = var.copy_paste_enabled
  diagnostic_settings = var.diagnostic_settings
  enable_telemetry = var.enable_telemetry
  file_copy_enabled = var.file_copy_enabled
  ip_configuration = var.ip_configuration
  ip_connect_enabled = var.ip_connect_enabled
  kerberos_enabled = var.kerberos_enabled
  location = var.location
  lock = var.lock
  name = var.name
  resource_group_name = var.resource_group_name
  role_assignments = var.role_assignments
  scale_units = var.scale_units
  shareable_link_enabled = var.shareable_link_enabled
  sku = var.sku
  tags = var.tags
  tunneling_enabled = var.tunneling_enabled
}

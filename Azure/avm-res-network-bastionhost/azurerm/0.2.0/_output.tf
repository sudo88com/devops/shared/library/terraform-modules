
output "dns_name" {
  description = "The FQDN of the Azure Bastion resource"
  value       = module.avm-res-network-bastionhost.dns_name
}

output "name" {
  description = "The name of the Azure Bastion resource"
  value       = module.avm-res-network-bastionhost.name
}

output "resource" {
  description = "The Azure Bastion resource"
  value       = module.avm-res-network-bastionhost.resource
}

output "resource_id" {
  description = "The ID of the Azure Bastion resource"
  value       = module.avm-res-network-bastionhost.resource_id
}


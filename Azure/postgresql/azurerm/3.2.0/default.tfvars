
administrator_login = 

administrator_password = 

auto_grow_enabled = true

backup_retention_days = 7

create_mode = "Default"

creation_source_server_id = null

db_charset = "UTF8"

db_collation = "English_United States.1252"

db_names = []

firewall_rule_prefix = "firewall-"

firewall_rules = []

geo_redundant_backup_enabled = true

infrastructure_encryption_enabled = true

location = 

postgresql_configurations = {}

public_network_access_enabled = false

resource_group_name = 

server_name = 

server_version = "9.5"

sku_name = "GP_Gen5_4"

ssl_enforcement_enabled = true

ssl_minimal_tls_version_enforced = "TLS1_2"

storage_mb = 102400

tags = {}

threat_detection_policy = null

tracing_tags_enabled = false

tracing_tags_prefix = "avm_"

vnet_rule_name_prefix = "postgresql-vnet-rule-"

vnet_rules = []


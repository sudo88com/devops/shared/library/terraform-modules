
locals {
  administrator_login = var.administrator_login
  administrator_password = var.administrator_password
  auto_grow_enabled = var.auto_grow_enabled
  backup_retention_days = var.backup_retention_days
  create_mode = var.create_mode
  creation_source_server_id = var.creation_source_server_id
  db_charset = var.db_charset
  db_collation = var.db_collation
  db_names = var.db_names
  firewall_rule_prefix = var.firewall_rule_prefix
  firewall_rules = var.firewall_rules
  geo_redundant_backup_enabled = var.geo_redundant_backup_enabled
  infrastructure_encryption_enabled = var.infrastructure_encryption_enabled
  location = var.location
  postgresql_configurations = var.postgresql_configurations
  public_network_access_enabled = var.public_network_access_enabled
  resource_group_name = var.resource_group_name
  server_name = var.server_name
  server_version = var.server_version
  sku_name = var.sku_name
  ssl_enforcement_enabled = var.ssl_enforcement_enabled
  ssl_minimal_tls_version_enforced = var.ssl_minimal_tls_version_enforced
  storage_mb = var.storage_mb
  tags = var.tags
  threat_detection_policy = var.threat_detection_policy
  tracing_tags_enabled = var.tracing_tags_enabled
  tracing_tags_prefix = var.tracing_tags_prefix
  vnet_rule_name_prefix = var.vnet_rule_name_prefix
  vnet_rules = var.vnet_rules
}

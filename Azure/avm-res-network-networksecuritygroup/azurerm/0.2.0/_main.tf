
module "avm-res-network-networksecuritygroup" {
  source = "terraform-aws-modules/avm-res-network-networksecuritygroup/aws"
  version = "0.2.0"
  diagnostic_settings = var.diagnostic_settings
  enable_telemetry = var.enable_telemetry
  location = var.location
  lock = var.lock
  name = var.name
  resource_group_name = var.resource_group_name
  role_assignments = var.role_assignments
  security_rules = var.security_rules
  tags = var.tags
  timeouts = var.timeouts
}


module "avm-res-managedidentity-userassignedidentity" {
  source = "terraform-aws-modules/avm-res-managedidentity-userassignedidentity/aws"
  version = "0.3.0"
  enable_telemetry = var.enable_telemetry
  location = var.location
  lock = var.lock
  name = var.name
  resource_group_name = var.resource_group_name
  tags = var.tags
}

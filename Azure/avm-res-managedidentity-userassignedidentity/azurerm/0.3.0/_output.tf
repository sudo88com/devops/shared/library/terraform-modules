
output "principal_id" {
  description = "This is the principal id for the user assigned identity."
  value       = module.avm-res-managedidentity-userassignedidentity.principal_id
}

output "resource" {
  description = "The object of type User Assigned Identity that was created."
  value       = module.avm-res-managedidentity-userassignedidentity.resource
}

output "resource_id" {
  description = "This is the full output for the resource."
  value       = module.avm-res-managedidentity-userassignedidentity.resource_id
}

output "resource_name" {
  description = "The name of the User Assigned Identity that was created."
  value       = module.avm-res-managedidentity-userassignedidentity.resource_name
}



variable "enable_telemetry" {
  type        = bool
  description = "This variable controls whether or not telemetry is enabled for the module.\nFor more information see <https://aka.ms/avm/telemetryinfo>.\nIf it is set to false, then no telemetry will be collected.\n"
  default     = true
}

variable "location" {
  type        = string
  description = "Azure region where the resource should be deployed.  If null, the location will be inferred from the resource group location."
  default     = ""
}

variable "lock" {
  type        = object({
    kind = string
    name = optional(string, null)
  })
  description = "  Controls the Resource Lock configuration for this resource. The following properties can be specified:\n  \n  - `kind` - (Required) The type of lock. Possible values are `\\"CanNotDelete\\"` and `\\"ReadOnly\\"`.\n  - `name` - (Optional) The name of the lock. If not specified, a name will be generated based on the `kind` value. Changing this forces the creation of a new resource.\n"
  default     = null
}

variable "name" {
  type        = string
  description = "The name of the this resource."
  default     = ""
}

variable "resource_group_name" {
  type        = string
  description = "The resource group where the resources will be deployed."
  default     = ""
}

variable "tags" {
  type        = map(string)
  description = "(Optional) Tags of the resource."
  default     = null
}



allocation_method = "Static"

disable_outbound_snat = false

edge_zone = null

frontend_ip_zones = null

frontend_name = "myPublicIP"

frontend_private_ip_address = ""

frontend_private_ip_address_allocation = "Dynamic"

frontend_private_ip_address_version = null

frontend_subnet_id = ""

frontend_subnet_name = ""

frontend_vnet_name = ""

lb_floating_ip_enabled = false

lb_port = {}

lb_probe = {}

lb_probe_interval = 5

lb_probe_unhealthy_threshold = 2

lb_sku = "Basic"

lb_sku_tier = "Regional"

location = ""

name = ""

pip_ddos_protection_mode = "VirtualNetworkInherited"

pip_ddos_protection_plan_id = null

pip_domain_name_label = null

pip_idle_timeout_in_minutes = 4

pip_ip_tags = null

pip_ip_version = "IPv4"

pip_name = ""

pip_public_ip_prefix_id = null

pip_reverse_fqdn = null

pip_sku = "Basic"

pip_sku_tier = "Regional"

pip_zones = null

prefix = "azure_lb"

remote_port = {}

resource_group_name = 

tags = {
  "source": "terraform"
}

tracing_tags_enabled = false

tracing_tags_prefix = "avm_"

type = "public"


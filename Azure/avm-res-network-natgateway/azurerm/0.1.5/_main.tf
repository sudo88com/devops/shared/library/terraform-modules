
module "avm-res-network-natgateway" {
  source = "terraform-aws-modules/avm-res-network-natgateway/aws"
  version = "0.1.5"
  enable_telemetry = var.enable_telemetry
  idle_timeout_in_minutes = var.idle_timeout_in_minutes
  location = var.location
  lock = var.lock
  name = var.name
  public_ip_prefix_length = var.public_ip_prefix_length
  resource_group_name = var.resource_group_name
  role_assignments = var.role_assignments
  sku_name = var.sku_name
  tags = var.tags
  timeouts = var.timeouts
  zones = var.zones
}

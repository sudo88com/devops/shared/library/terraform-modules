
output "resource" {
  description = "The NAT Gateway resource."
  value       = module.avm-res-network-natgateway.resource
}


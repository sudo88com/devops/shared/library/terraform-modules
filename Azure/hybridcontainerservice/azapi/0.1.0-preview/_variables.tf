
variable "admin_group_object_IDs" {
  type        = list(string)
  description = "(Optional) If you want to Use Azure AD for authentication and Kubernetes native RBAC for authorization, specify AAD group here. Assign Azure Active Directory groups that will have admin access within the cluster. Make sure you are part of the assigned groups to ensure cluster access after deployment regardless of if you are an Owner or a Contributor. "
  default     = []
}

variable "cluster_name" {
  type        = string
  description = "(Required) The name of cluster"
  default     = ""
}

variable "controlplane_VM_size" {
  type        = string
  description = "(Optional) VM sku of the control plane"
  default     = "Standard_A4_v2"
}

variable "controlplane_count" {
  type        = number
  description = "(Optional) VM count of the control plane"
  default     = 1
}

variable "customLocation_id" {
  type        = string
  description = "(Required) The name of the customer location that the resources for consul will run in"
  default     = ""
}

variable "default_agent_VM_size" {
  type        = string
  description = "(Optional) VM sku of the default node pool"
  default     = "Standard_A4_v2"
}

variable "default_agent_count" {
  type        = number
  description = "(Optional) VM count of the default node pool"
  default     = 1
}

variable "environment" {
  type        = string
  description = "(Optional) The environment of the site, possiblily value like: test/prod"
  default     = ""
}

variable "kubernetes_version" {
  type        = string
  description = "(Optional) kubernates version of this hybrid aks"
  default     = "v1.22.11"
}

variable "loadbalancer_VM_size" {
  type        = string
  description = "(Optional) VM sku of the load balancer"
  default     = "Standard_K8S3_v1"
}

variable "loadbalancer_count" {
  type        = number
  description = "(Optional) VM count of the load balancer"
  default     = 1
}

variable "loadbalancer_sku" {
  type        = string
  description = "(Optional) value"
  default     = "unstacked-haproxy"
}

variable "network_policy" {
  type        = string
  description = "(Optional) network cni of this hybrid aks"
  default     = "calico"
}

variable "pod_cidr" {
  type        = string
  description = "(Optional) CIDR of pods in this hybrid aks"
  default     = "10.245.0.0/16"
}

variable "public_key" {
  type        = string
  description = "(Required) Base64 encoded public certificate used by the agent to do the initial handshake to the backend services in Azure."
  default     = ""
}

variable "resource_group_name" {
  type        = string
  description = "(Required) The name of the resource group that the resources for consul will run in"
  default     = ""
}

variable "site_name" {
  type        = string
  description = "(Optional) The name of the site"
  default     = ""
}

variable "vnet_name" {
  type        = string
  description = "(Required) The name of the vnet that the resources for consul will run in"
  default     = ""
}


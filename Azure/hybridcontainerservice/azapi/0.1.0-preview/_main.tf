
module "hybridcontainerservice" {
  source = "terraform-aws-modules/hybridcontainerservice/aws"
  version = "0.1.0-preview"
  admin_group_object_IDs = var.admin_group_object_IDs
  cluster_name = var.cluster_name
  controlplane_VM_size = var.controlplane_VM_size
  controlplane_count = var.controlplane_count
  customLocation_id = var.customLocation_id
  default_agent_VM_size = var.default_agent_VM_size
  default_agent_count = var.default_agent_count
  environment = var.environment
  kubernetes_version = var.kubernetes_version
  loadbalancer_VM_size = var.loadbalancer_VM_size
  loadbalancer_count = var.loadbalancer_count
  loadbalancer_sku = var.loadbalancer_sku
  network_policy = var.network_policy
  pod_cidr = var.pod_cidr
  public_key = var.public_key
  resource_group_name = var.resource_group_name
  site_name = var.site_name
  vnet_name = var.vnet_name
}


output "cluster_id" {
  description = "the id of created hybrid aks"
  value       = module.hybridcontainerservice.cluster_id
}

output "resource_group_id" {
  description = "the id of the resource group"
  value       = module.hybridcontainerservice.resource_group_id
}


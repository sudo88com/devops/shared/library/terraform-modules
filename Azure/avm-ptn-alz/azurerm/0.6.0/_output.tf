
output "management_group_resource_id" {
  description = "The resource id of the created management group."
  value       = module.avm-ptn-alz.management_group_resource_id
}


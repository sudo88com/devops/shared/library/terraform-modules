
output "private_endpoints" {
  description = "A map of private endpoints. The map key is the supplied input to var.private_endpoints. The map value is the entire azurerm_private_endpoint resource."
  value       = module.avm-res-keyvault-vault.private_endpoints
}

output "resource" {
  description = "The Key Vault resource."
  value       = module.avm-res-keyvault-vault.resource
}

output "resource_keys" {
  description = "A map of key objects. The map key is the supplied input to var.keys. The map value is the entire azurerm_key_vault_key resource."
  value       = module.avm-res-keyvault-vault.resource_keys
}

output "resource_secrets" {
  description = "A map of secret objects. The map key is the supplied input to var.secrets. The map value is the entire azurerm_key_vault_secret resource."
  value       = module.avm-res-keyvault-vault.resource_secrets
}


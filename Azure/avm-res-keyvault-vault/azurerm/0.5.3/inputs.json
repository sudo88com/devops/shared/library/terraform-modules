[
  {
    "name": "secrets_value",
    "type": "map(string)",
    "description": "A map of secret keys to values.\nThe map key is the supplied input to `var.secrets`.\nThe map value is the secret value.\n\nThis is a separate variable to `var.secrets` because it is sensitive and therefore cannot be used in a `for_each` loop.\n",
    "default": "null",
    "required": false
  },
  {
    "name": "sku_name",
    "type": "string",
    "description": "The SKU name of the Key Vault. Default is `premium`. `Possible values are `standard` and `premium`.",
    "default": "\"premium\"",
    "required": false
  },
  {
    "name": "wait_for_rbac_before_key_operations",
    "type": "object({\n    create  = optional(string, \"30s\")\n    destroy = optional(string, \"0s\")\n  })",
    "description": "This variable controls the amount of time to wait before performing key operations.\nIt only applies when `var.role_assignments` and `var.keys` are both set.\nThis is useful when you are creating role assignments on the key vault and immediately creating keys in it.\nThe default is 30 seconds for create and 0 seconds for destroy.\n",
    "default": "{}",
    "required": false
  },
  {
    "name": "location",
    "type": "string",
    "description": "The Azure location where the resources will be deployed.",
    "default": "",
    "required": true
  },
  {
    "name": "diagnostic_settings",
    "type": "map(object({\n    name                                     = optional(string, null)\n    log_categories                           = optional(set(string), [])\n    log_groups                               = optional(set(string), [\"allLogs\"])\n    metric_categories                        = optional(set(string), [\"AllMetrics\"])\n    log_analytics_destination_type           = optional(string, \"Dedicated\")\n    workspace_resource_id                    = optional(string, null)\n    storage_account_resource_id              = optional(string, null)\n    event_hub_authorization_rule_resource_id = optional(string, null)\n    event_hub_name                           = optional(string, null)\n    marketplace_partner_resource_id          = optional(string, null)\n  }))",
    "description": "A map of diagnostic settings to create on the Key Vault. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.\n\n- `name` - (Optional) The name of the diagnostic setting. One will be generated if not set, however this will not be unique if you want to create multiple diagnostic setting resources.\n- `log_categories` - (Optional) A set of log categories to send to the log analytics workspace. Defaults to `[]`.\n- `log_groups` - (Optional) A set of log groups to send to the log analytics workspace. Defaults to `[\"allLogs\"]`.\n- `metric_categories` - (Optional) A set of metric categories to send to the log analytics workspace. Defaults to `[\"AllMetrics\"]`.\n- `log_analytics_destination_type` - (Optional) The destination type for the diagnostic setting. Possible values are `Dedicated` and `AzureDiagnostics`. Defaults to `Dedicated`.\n- `workspace_resource_id` - (Optional) The resource ID of the log analytics workspace to send logs and metrics to.\n- `storage_account_resource_id` - (Optional) The resource ID of the storage account to send logs and metrics to.\n- `event_hub_authorization_rule_resource_id` - (Optional) The resource ID of the event hub authorization rule to send logs and metrics to.\n- `event_hub_name` - (Optional) The name of the event hub. If none is specified, the default event hub will be selected.\n- `marketplace_partner_resource_id` - (Optional) The full ARM resource ID of the Marketplace resource to which you would like to send Diagnostic LogsLogs.\n",
    "default": "{}",
    "required": false
  },
  {
    "name": "enable_telemetry",
    "type": "bool",
    "description": "This variable controls whether or not telemetry is enabled for the module.\nFor more information see https://aka.ms/avm/telemetryinfo.\nIf it is set to false, then no telemetry will be collected.\n",
    "default": "true",
    "required": false
  },
  {
    "name": "private_endpoints",
    "type": "map(object({\n    name = optional(string, null)\n    role_assignments = optional(map(object({\n      role_definition_id_or_name             = string\n      principal_id                           = string\n      description                            = optional(string, null)\n      skip_service_principal_aad_check       = optional(bool, false)\n      condition                              = optional(string, null)\n      condition_version                      = optional(string, null)\n      delegated_managed_identity_resource_id = optional(string, null)\n    })), {})\n    lock = optional(object({\n      name = optional(string, null)\n      kind = optional(string, \"None\")\n    }), {})\n    tags                                    = optional(map(any), null)\n    subnet_resource_id                      = string\n    private_dns_zone_group_name             = optional(string, \"default\")\n    private_dns_zone_resource_ids           = optional(set(string), [])\n    application_security_group_associations = optional(map(string), {})\n    private_service_connection_name         = optional(string, null)\n    network_interface_name                  = optional(string, null)\n    location                                = optional(string, null)\n    resource_group_name                     = optional(string, null)\n    ip_configurations = optional(map(object({\n      name               = string\n      private_ip_address = string\n    })), {})\n  }))",
    "description": "A map of private endpoints to create on the Key Vault. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.\n\n- `name` - (Optional) The name of the private endpoint. One will be generated if not set.\n- `role_assignments` - (Optional) A map of role assignments to create on the private endpoint. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time. See `var.role_assignments` for more information.\n- `lock` - (Optional) The lock level to apply to the private endpoint. Default is `None`. Possible values are `None`, `CanNotDelete`, and `ReadOnly`.\n- `tags` - (Optional) A mapping of tags to assign to the private endpoint.\n- `subnet_resource_id` - The resource ID of the subnet to deploy the private endpoint in.\n- `private_dns_zone_group_name` - (Optional) The name of the private DNS zone group. One will be generated if not set.\n- `private_dns_zone_resource_ids` - (Optional) A set of resource IDs of private DNS zones to associate with the private endpoint. If not set, no zone groups will be created and the private endpoint will not be associated with any private DNS zones. DNS records must be managed external to this module.\n- `application_security_group_resource_ids` - (Optional) A map of resource IDs of application security groups to associate with the private endpoint. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.\n- `private_service_connection_name` - (Optional) The name of the private service connection. One will be generated if not set.\n- `network_interface_name` - (Optional) The name of the network interface. One will be generated if not set.\n- `location` - (Optional) The Azure location where the resources will be deployed. Defaults to the location of the resource group.\n- `resource_group_name` - (Optional) The resource group where the resources will be deployed. Defaults to the resource group of the Key Vault.\n- `ip_configurations` - (Optional) A map of IP configurations to create on the private endpoint. If not specified the platform will create one. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.\n  - `name` - The name of the IP configuration.\n  - `private_ip_address` - The private IP address of the IP configuration.\n",
    "default": "{}",
    "required": false
  },
  {
    "name": "resource_group_name",
    "type": "string",
    "description": "The resource group where the resources will be deployed.",
    "default": "",
    "required": true
  },
  {
    "name": "enabled_for_template_deployment",
    "type": "bool",
    "description": "Specifies whether Azure Resource Manager is permitted to retrieve secrets from the vault.",
    "default": "false",
    "required": false
  },
  {
    "name": "role_assignments",
    "type": "map(object({\n    role_definition_id_or_name             = string\n    principal_id                           = string\n    description                            = optional(string, null)\n    skip_service_principal_aad_check       = optional(bool, false)\n    condition                              = optional(string, null)\n    condition_version                      = optional(string, null)\n    delegated_managed_identity_resource_id = optional(string, null)\n  }))",
    "description": "A map of role assignments to create on the Key Vault. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.\n\n- `role_definition_id_or_name` - The ID or name of the role definition to assign to the principal.\n- `principal_id` - The ID of the principal to assign the role to.\n- `description` - The description of the role assignment.\n- `skip_service_principal_aad_check` - If set to true, skips the Azure Active Directory check for the service principal in the tenant. Defaults to false.\n- `condition` - The condition which will be used to scope the role assignment.\n- `condition_version` - The version of the condition syntax. If you are using a condition, valid values are '2.0'.\n\n> Note: only set `skip_service_principal_aad_check` to true if you are assigning a role to a service principal.\n",
    "default": "{}",
    "required": false
  },
  {
    "name": "keys",
    "type": "map(object({\n    name     = string\n    key_type = string\n    key_opts = optional(list(string), [\"sign\", \"verify\"])\n\n    key_size        = optional(number, null)\n    curve           = optional(string, null)\n    not_before_date = optional(string, null)\n    expiration_date = optional(string, null)\n    tags            = optional(map(any), null)\n\n    role_assignments = optional(map(object({\n      role_definition_id_or_name             = string\n      principal_id                           = string\n      description                            = optional(string, null)\n      skip_service_principal_aad_check       = optional(bool, false)\n      condition                              = optional(string, null)\n      condition_version                      = optional(string, null)\n      delegated_managed_identity_resource_id = optional(string, null)\n    })), {})\n\n    rotation_policy = optional(object({\n      automatic = optional(object({\n        time_after_creation = optional(string, null)\n        time_before_expiry  = optional(string, null)\n      }), null)\n      expire_after         = optional(string, null)\n      notify_before_expiry = optional(string, null)\n    }), null)\n  }))",
    "description": "A map of keys to create on the Key Vault. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.\n\n- `name` - The name of the key.\n- `key_type` - The type of the key. Possible values are `EC` and `RSA`.\n- `key_opts` - A list of key options. Possible values are `decrypt`, `encrypt`, `sign`, `unwrapKey`, `verify`, and `wrapKey`.\n- `key_size` - The size of the key. Required for `RSA` keys.\n- `curve` - The curve of the key. Required for `EC` keys.  Possible values are `P-256`, `P-256K`, `P-384`, and `P-521`. The API will default to `P-256` if nothing is specified.\n- `not_before_date` - The not before date of the key.\n- `expiration_date` - The expiration date of the key.\n- `tags` - A mapping of tags to assign to the key.\n- `rotation_policy` - The rotation policy of the key.\n  - `automatic` - The automatic rotation policy of the key.\n    - `time_after_creation` - The time after creation of the key before it is automatically rotated.\n    - `time_before_expiry` - The time before expiry of the key before it is automatically rotated.\n  - `expire_after` - The time after which the key expires.\n  - `notify_before_expiry` - The time before expiry of the key when notification emails will be sent.\n\nSupply role assignments in the same way as for `var.role_assignments`.\n",
    "default": "{}",
    "required": false
  },
  {
    "name": "lock",
    "type": "object({\n    name = optional(string, null)\n    kind = optional(string, \"None\")\n  })",
    "description": "The lock level to apply to the Key Vault. Default is `None`. Possible values are `None`, `CanNotDelete`, and `ReadOnly`.",
    "default": "{}",
    "required": false
  },
  {
    "name": "network_acls",
    "type": "object({\n    bypass                     = optional(string, \"None\")\n    default_action             = optional(string, \"Deny\")\n    ip_rules                   = optional(list(string), [])\n    virtual_network_subnet_ids = optional(list(string), [])\n  })",
    "description": "The network ACL configuration for the Key Vault.\nIf not specified then the Key Vault will be created with a firewall that blocks access.\nSpecify `null` to create the Key Vault with no firewall.\n\n- `bypass` - (Optional) Should Azure Services bypass the ACL. Possible values are `AzureServices` and `None`. Defaults to `None`.\n- `default_action` - (Optional) The default action when no rule matches. Possible values are `Allow` and `Deny`. Defaults to `Deny`.\n- `ip_rules` - (Optional) A list of IP rules in CIDR format. Defaults to `[]`.\n- `virtual_network_subnet_ids` - (Optional) When using with Service Endpoints, a list of subnet IDs to associate with the Key Vault. Defaults to `[]`.\n",
    "default": "{}",
    "required": false
  },
  {
    "name": "purge_protection_enabled",
    "type": "bool",
    "description": "Specifies whether protection against purge is enabled for this Key Vault. Note once enabled this cannot be disabled.",
    "default": "true",
    "required": false
  },
  {
    "name": "tenant_id",
    "type": "string",
    "description": "The Azure tenant ID used for authenticating requests to Key Vault. You can use the `azurerm_client_config` data source to retrieve it.",
    "default": "",
    "required": true
  },
  {
    "name": "contacts",
    "type": "map(object({\n    email = string\n    name  = optional(string, null)\n    phone = optional(string, null)\n  }))",
    "description": "A map of contacts for the Key Vault. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.",
    "default": "{}",
    "required": false
  },
  {
    "name": "enabled_for_deployment",
    "type": "bool",
    "description": "Specifies whether Azure Virtual Machines are permitted to retrieve certificates stored as secrets from the vault.",
    "default": "false",
    "required": false
  },
  {
    "name": "enabled_for_disk_encryption",
    "type": "bool",
    "description": "Specifies whether Azure Disk Encryption is permitted to retrieve secrets from the vault and unwrap keys.",
    "default": "false",
    "required": false
  },
  {
    "name": "secrets",
    "type": "map(object({\n    name            = string\n    content_type    = optional(string, null)\n    tags            = optional(map(any), null)\n    not_before_date = optional(string, null)\n    expiration_date = optional(string, null)\n\n    role_assignments = optional(map(object({\n      role_definition_id_or_name             = string\n      principal_id                           = string\n      description                            = optional(string, null)\n      skip_service_principal_aad_check       = optional(bool, false)\n      condition                              = optional(string, null)\n      condition_version                      = optional(string, null)\n      delegated_managed_identity_resource_id = optional(string, null)\n    })), {})\n  }))",
    "description": "A map of secrets to create on the Key Vault. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.\n\n- `name` - The name of the secret.\n- `content_type` - The content type of the secret.\n- `tags` - A mapping of tags to assign to the secret.\n- `not_before_date` - The not before date of the secret.\n- `expiration_date` - The expiration date of the secret.\n\nSupply role assignments in the same way as for `var.role_assignments`.\n\n> Note: the `value` of the secret is supplied via the `var.secrets_value` variable. Make sure to use the same map key.\n",
    "default": "{}",
    "required": false
  },
  {
    "name": "soft_delete_retention_days",
    "type": "number",
    "description": "The number of days that items should be retained for once soft-deleted. This value can be between 7 and 90 (the default) days.\n",
    "default": "null",
    "required": false
  },
  {
    "name": "wait_for_rbac_before_secret_operations",
    "type": "object({\n    create  = optional(string, \"30s\")\n    destroy = optional(string, \"0s\")\n  })",
    "description": "This variable controls the amount of time to wait before performing secret operations.\nIt only applies when `var.role_assignments` and `var.secrets` are both set.\nThis is useful when you are creating role assignments on the key vault and immediately creating secrets in it.\nThe default is 30 seconds for create and 0 seconds for destroy.\n",
    "default": "{}",
    "required": false
  },
  {
    "name": "name",
    "type": "string",
    "description": "The name of the Key Vault.",
    "default": "",
    "required": true
  },
  {
    "name": "public_network_access_enabled",
    "type": "bool",
    "description": "Specifies whether public access is permitted.",
    "default": "true",
    "required": false
  },
  {
    "name": "tags",
    "type": "map(any)",
    "description": "Map of tags to assign to the Key Vault resource.",
    "default": "null",
    "required": false
  }
]

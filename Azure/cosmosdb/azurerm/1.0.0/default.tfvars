
additional_capabilities = []

analytical_storage = {
  "enabled": false,
  "schema_type": null
}

application_name = "dev"

auto_failover = false

azure_dc_access = [
  "0.0.0.0"
]

azure_portal_access = [
  "104.42.195.92,40.76.54.131,52.176.6.30,52.169.50.45,52.187.184.26"
]

backup_enabled = true

backup_interval = 60

backup_retention = 8

backup_storage_redundancy = "Geo"

backup_type = "periodic"

capabilities = {
  "cassandra": "EnableCassandra",
  "gremlin": "EnableGremlin",
  "mongo": "EnableMongo",
  "sql": "SQL",
  "table": "EnableTable"
}

cassandra_keyspaces = {}

cassandra_tables = {}

consistency_level = "Eventual"

cosmos_account_name = ""

cosmos_api = 

enable_systemassigned_identity = false

environment = "dev"

event_hub = {}

firewall_ip = []

free_tier = false

geo_locations = [
  {
    "failover_priority": 0,
    "geo_location": "eastus",
    "zone_redundant": false
  }
]

gremlin_dbs = {}

gremlin_graphs = {}

ip_firewall_enabled = true

key_vault_key_name = ""

key_vault_name = ""

key_vault_rg_name = ""

location = ""

log_analytics = {}

logs_config = {
  "cassandra": "CassandraRequests",
  "gremlin": "GremlinRequests",
  "mongo": "MongoRequests",
  "sql": "SQL",
  "table": "TableApiRequests"
}

max_interval_in_seconds = 300

max_staleness_prefix = 100000

mongo_db_collections = {}

mongo_dbs = {}

multi_region_write = false

pe_subresource = {
  "cassandra": "Cassandra",
  "gremlin": "Gremlin",
  "mongo": "MongoDB",
  "sql": "SQL",
  "table": "Table"
}

private_dns_zone_name = {
  "cassandra": "privatelink.cassandra.cosmos.azure.com",
  "gremlin": "privatelink.gremlin.cosmos.azure.com",
  "mongo": "privatelink.mongo.cosmos.azure.com",
  "sql": "privatelink.documents.azure.com",
  "table": "privatelink.table.cosmos.azure.com"
}

private_endpoint = {}

public_network_access_enabled = false

resource_group_name = 

sql_db_containers = {}

sql_dbs = {}

storage_account = {}

tables = {}


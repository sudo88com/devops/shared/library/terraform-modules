[
  {
    "name": "storage_account",
    "type": "map(object({\n    storage_account_name         = string\n    storage_account_rg_name      = string\n    enable_logs_retention_policy = bool\n    logs_retention_days          = number\n  }))",
    "description": "Storage account parameters for one or more storage account to send daignostic logs to storage account.",
    "default": "{}",
    "required": false
  },
  {
    "name": "azure_dc_access",
    "type": "list(string)",
    "description": "List of ip address to enable the Accept connections from within public Azure datacenters behavior.",
    "default": "[\n  \"0.0.0.0\"\n]",
    "required": false
  },
  {
    "name": "gremlin_graphs",
    "type": "map(object({\n    graph_name            = string\n    db_name               = string\n    partition_key_path    = string\n    partition_key_version = number\n    default_ttl_seconds   = string\n    graph_throughput      = number\n    graph_max_throughput  = number\n    index_policy_settings = object({\n      indexing_automatic = bool\n      indexing_mode      = string\n      included_paths     = list(string)\n      excluded_paths     = list(string)\n      composite_indexes = map(object({\n        indexes = set(object({\n          index_path  = string\n          index_order = string\n        }))\n      }))\n      spatial_indexes = map(object({\n        spatial_index_path = string\n      }))\n    })\n    conflict_resolution_policy = object({\n      mode      = string\n      path      = string\n      procedure = string\n    })\n    unique_key = list(string)\n  }))",
    "description": "List of Cosmos DB Gremlin Graph to create. Some parameters are inherited from cosmos account.",
    "default": "{}",
    "required": false
  },
  {
    "name": "private_endpoint",
    "type": "map(object({\n    name                            = string\n    vnet_rg_name                    = string\n    vnet_name                       = string\n    subnet_name                     = string\n    dns_zone_rg_name                = string\n    enable_private_dns_entry        = bool\n    dns_zone_group_name             = string\n    private_service_connection_name = string\n    is_manual_connection            = bool\n  }))",
    "description": "Parameters for private endpoint creation",
    "default": "{}",
    "required": false
  },
  {
    "name": "backup_storage_redundancy",
    "type": "string",
    "description": "he storage redundancy which is used to indicate type of backup residency. This is configurable only when type is Periodic. Possible values are Geo, Local and Zone",
    "default": "\"Geo\"",
    "required": false
  },
  {
    "name": "key_vault_name",
    "type": "string",
    "description": "Name of the existing key vault. It is needed for encryption using customer managed key.",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "firewall_ip",
    "type": "list(string)",
    "description": "List of ip address to allow access from the internet or on-premisis network.",
    "default": "[]",
    "required": false
  },
  {
    "name": "consistency_level",
    "type": "string",
    "description": "The Consistency Level to use for this CosmosDB Account - can be either BoundedStaleness, Eventual, Session, Strong or ConsistentPrefix",
    "default": "\"Eventual\"",
    "required": false
  },
  {
    "name": "auto_failover",
    "type": "bool",
    "description": "Enable automatic fail over for this Cosmos DB account - can be either true or false",
    "default": "false",
    "required": false
  },
  {
    "name": "free_tier",
    "type": "bool",
    "description": "Enable Free Tier pricing option for this Cosmos DB account - can be either true or false",
    "default": "false",
    "required": false
  },
  {
    "name": "multi_region_write",
    "type": "bool",
    "description": "Enable multiple write locations for this Cosmos DB account",
    "default": "false",
    "required": false
  },
  {
    "name": "cassandra_tables",
    "type": "map(object({\n    table_name             = string\n    keyspace_name          = string\n    default_ttl_seconds    = string\n    analytical_storage_ttl = number\n    table_throughout       = number\n    table_max_throughput   = number\n    cassandra_schema_settings = object({\n      column = map(object({\n        column_key_name = string\n        column_key_type = string\n      }))\n      partition_key = map(object({\n        partition_key_name = string\n      }))\n      cluster_key = map(object({\n        cluster_key_name     = string\n        cluster_key_order_by = string\n      }))\n    })\n  }))",
    "description": "List of Cosmos DB Cassandra tables to create. Some parameters are inherited from cosmos account.",
    "default": "{}",
    "required": false
  },
  {
    "name": "public_network_access_enabled",
    "type": "bool",
    "description": "Enable public network access to cosmos db",
    "default": "false",
    "required": false
  },
  {
    "name": "ip_firewall_enabled",
    "type": "bool",
    "description": "Enable ip firewwall to allow connection to this cosmosdb from client's machine and from azure portal.",
    "default": "true",
    "required": false
  },
  {
    "name": "environment",
    "type": "string",
    "description": "Name of the environment. Example dev, test, qa, cert, prod etc....",
    "default": "\"dev\"",
    "required": false
  },
  {
    "name": "key_vault_rg_name",
    "type": "string",
    "description": "Name of the resource group in which key vault exists.",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "key_vault_key_name",
    "type": "string",
    "description": "Name of the existing key in key vault. It is needed for encryption using customer managed key.",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "mongo_dbs",
    "type": "map(object({\n    db_name           = string\n    db_throughput     = number\n    db_max_throughput = number\n  }))",
    "description": "Map of Cosmos DB Mongo DBs to create. Some parameters are inherited from cosmos account.",
    "default": "{}",
    "required": false
  },
  {
    "name": "mongo_db_collections",
    "type": "map(object({\n    collection_name           = string\n    db_name                   = string\n    default_ttl_seconds       = string\n    shard_key                 = string\n    collection_throughout     = number\n    collection_max_throughput = number\n    analytical_storage_ttl    = number\n    indexes = map(object({\n      mongo_index_keys   = list(string)\n      mongo_index_unique = bool\n    }))\n  }))",
    "description": "List of Cosmos DB Mongo collections to create. Some parameters are inherited from cosmos account.",
    "default": "{}",
    "required": false
  },
  {
    "name": "pe_subresource",
    "type": "map(any)",
    "description": "Map of subresources to choose appropriate Private Endpoint sub resource for DB API",
    "default": "{\n  \"cassandra\": \"Cassandra\",\n  \"gremlin\": \"Gremlin\",\n  \"mongo\": \"MongoDB\",\n  \"sql\": \"SQL\",\n  \"table\": \"Table\"\n}",
    "required": false
  },
  {
    "name": "sql_db_containers",
    "type": "map(object({\n    container_name           = string\n    db_name                  = string\n    partition_key_path       = string\n    partition_key_version    = number\n    container_throughout     = number\n    container_max_throughput = number\n    default_ttl              = number\n    analytical_storage_ttl   = number\n    indexing_policy_settings = object({\n      sql_indexing_mode = string\n      sql_included_path = string\n      sql_excluded_path = string\n      composite_indexes = map(object({\n        indexes = set(object({\n          path  = string\n          order = string\n        }))\n      }))\n      spatial_indexes = map(object({\n        path = string\n      }))\n    })\n    sql_unique_key = list(string)\n    conflict_resolution_policy = object({\n      mode      = string\n      path      = string\n      procedure = string\n    })\n  }))",
    "description": "List of Cosmos DB SQL Containers to create. Some parameters are inherited from cosmos account.",
    "default": "{}",
    "required": false
  },
  {
    "name": "cosmos_account_name",
    "type": "string",
    "description": "Name of the Cosmos DB account.",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "cosmos_api",
    "type": "string",
    "description": "",
    "default": "",
    "required": true
  },
  {
    "name": "application_name",
    "type": "string",
    "description": "Name of the application.",
    "default": "\"dev\"",
    "required": false
  },
  {
    "name": "backup_interval",
    "type": "string",
    "description": "The interval in minutes between two backups. This is configurable only when type is Periodic. Possible values are between 60 and 1440.",
    "default": "60",
    "required": false
  },
  {
    "name": "cassandra_keyspaces",
    "type": "map(object({\n    keyspace_name           = string\n    keyspace_throughput     = number\n    keyspace_max_throughput = number\n  }))",
    "description": "List of Cosmos DB Cassandra keyspaces to create. Some parameters are inherited from cosmos account.",
    "default": "{}",
    "required": false
  },
  {
    "name": "azure_portal_access",
    "type": "list(string)",
    "description": "List of ip address to enable the Allow access from the Azure portal behavior.",
    "default": "[\n  \"104.42.195.92,40.76.54.131,52.176.6.30,52.169.50.45,52.187.184.26\"\n]",
    "required": false
  },
  {
    "name": "gremlin_dbs",
    "type": "map(object({\n    db_name           = string\n    db_throughput     = number\n    db_max_throughput = number\n  }))",
    "description": "Map of Cosmos DB Gremlin DBs to create. Some parameters are inherited from cosmos account.",
    "default": "{}",
    "required": false
  },
  {
    "name": "private_dns_zone_name",
    "type": "map(any)",
    "description": "Map of the private DNS zone to choose approrite Private DNS Zone for DB API.",
    "default": "{\n  \"cassandra\": \"privatelink.cassandra.cosmos.azure.com\",\n  \"gremlin\": \"privatelink.gremlin.cosmos.azure.com\",\n  \"mongo\": \"privatelink.mongo.cosmos.azure.com\",\n  \"sql\": \"privatelink.documents.azure.com\",\n  \"table\": \"privatelink.table.cosmos.azure.com\"\n}",
    "required": false
  },
  {
    "name": "sql_dbs",
    "type": "map(object({\n    db_name           = string\n    db_throughput     = number\n    db_max_throughput = number\n  }))",
    "description": "Map of Cosmos DB SQL DBs to create. Some parameters are inherited from cosmos account.",
    "default": "{}",
    "required": false
  },
  {
    "name": "analytical_storage",
    "type": "object({\n    enabled     = bool\n    schema_type = string\n  })",
    "description": "Analytical storage enablement.",
    "default": "{\n  \"enabled\": false,\n  \"schema_type\": null\n}",
    "required": false
  },
  {
    "name": "capabilities",
    "type": "map(any)",
    "description": "Map of non-sql DB API to enable support for API other than SQL",
    "default": "{\n  \"cassandra\": \"EnableCassandra\",\n  \"gremlin\": \"EnableGremlin\",\n  \"mongo\": \"EnableMongo\",\n  \"sql\": \"SQL\",\n  \"table\": \"EnableTable\"\n}",
    "required": false
  },
  {
    "name": "log_analytics",
    "type": "map(object({\n    la_workspace_name    = string\n    la_workspace_rg_name = string\n  }))",
    "description": "Log Analytics parameters for one or more log analytics workspace to send daignostic logs to log analytics workspace.",
    "default": "{}",
    "required": false
  },
  {
    "name": "event_hub",
    "type": "map(object({\n    event_hub_name           = string\n    event_hub_namespace_name = string\n    event_hub_rg_name        = string\n    event_hub_auth_rule_name = string\n  }))",
    "description": "Event Hub parameters for one or more event hub to send daignostic logs to event hub.",
    "default": "{}",
    "required": false
  },
  {
    "name": "logs_config",
    "type": "map(any)",
    "description": "Map of non-sql DB API logs configuration to enable logging for respective API",
    "default": "{\n  \"cassandra\": \"CassandraRequests\",\n  \"gremlin\": \"GremlinRequests\",\n  \"mongo\": \"MongoRequests\",\n  \"sql\": \"SQL\",\n  \"table\": \"TableApiRequests\"\n}",
    "required": false
  },
  {
    "name": "resource_group_name",
    "type": "string",
    "description": "Name of the azure resource group.",
    "default": "",
    "required": true
  },
  {
    "name": "location",
    "type": "string",
    "description": "Cosmos DB deployment region. Can be different vs. RG location",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "backup_enabled",
    "type": "bool",
    "description": "Enable backup for this Cosmos DB account",
    "default": "true",
    "required": false
  },
  {
    "name": "max_staleness_prefix",
    "type": "string",
    "description": "When used with the Bounded Staleness consistency level, this value represents the number of stale requests tolerated. Accepted range for this value is 10 – 2147483647",
    "default": "100000",
    "required": false
  },
  {
    "name": "backup_retention",
    "type": "string",
    "description": "The time in hours that each backup is retained. This is configurable only when type is Periodic. Possible values are between 8 and 720.",
    "default": "8",
    "required": false
  },
  {
    "name": "geo_locations",
    "type": "any",
    "description": "List of map of geo locations and other properties to create primary and secodanry databasees.",
    "default": "[\n  {\n    \"failover_priority\": 0,\n    \"geo_location\": \"eastus\",\n    \"zone_redundant\": false\n  }\n]",
    "required": false
  },
  {
    "name": "additional_capabilities",
    "type": "list(string)",
    "description": "List of additional capabilities for Cosmos DB API. - possible options are DisableRateLimitingResponses, EnableAggregationPipeline, EnableServerless, mongoEnableDocLevelTTL, MongoDBv3.4, AllowSelfServeUpgradeToMongo36",
    "default": "[]",
    "required": false
  },
  {
    "name": "tables",
    "type": "map(object({\n    table_name           = string\n    table_throughput     = number\n    table_max_throughput = number\n  }))",
    "description": "Map of Cosmos DB Tables to create. Some parameters are inherited from cosmos account.",
    "default": "{}",
    "required": false
  },
  {
    "name": "max_interval_in_seconds",
    "type": "string",
    "description": "When used with the Bounded Staleness consistency level, this value represents the time amount of staleness (in seconds) tolerated. Accepted range for this value is 5 - 86400 (1 day)",
    "default": "300",
    "required": false
  },
  {
    "name": "backup_type",
    "type": "string",
    "description": "Type of backup - can be either Periodic or Continuous",
    "default": "\"periodic\"",
    "required": false
  },
  {
    "name": "enable_systemassigned_identity",
    "type": "bool",
    "description": "Enable System Assigned Identity",
    "default": "false",
    "required": false
  }
]

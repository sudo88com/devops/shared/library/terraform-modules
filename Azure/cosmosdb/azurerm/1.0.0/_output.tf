
output "cassandra_keyspace_id" {
  description = "Cassandra API Keyspace IDs"
  value       = module.cosmosdb.cassandra_keyspace_id
}

output "cassandra_table_id" {
  description = "Cassandra API Table IDs"
  value       = module.cosmosdb.cassandra_table_id
}

output "cosmosdb_connection_strings" {
  description = "Cosmos DB Connection Strings"
  value       = module.cosmosdb.cosmosdb_connection_strings
}

output "cosmosdb_endpoint" {
  description = "Cosmos DB Endpoint"
  value       = module.cosmosdb.cosmosdb_endpoint
}

output "cosmosdb_id" {
  description = "Cosmos DB Account ID"
  value       = module.cosmosdb.cosmosdb_id
}

output "cosmosdb_primary_key" {
  description = "Cosmos DB Primary Keys"
  value       = module.cosmosdb.cosmosdb_primary_key
}

output "cosmosdb_primary_readonly_key" {
  description = "Cosmos DB Primary Read Only Keys"
  value       = module.cosmosdb.cosmosdb_primary_readonly_key
}

output "cosmosdb_read_endpoint" {
  description = "Cosmos DB Read Endpoint"
  value       = module.cosmosdb.cosmosdb_read_endpoint
}

output "cosmosdb_secondary_key" {
  description = "Cosmos DB Secondary Keys"
  value       = module.cosmosdb.cosmosdb_secondary_key
}

output "cosmosdb_secondary_readonly_key" {
  description = "Cosmos DB Secondary Read Only Keys"
  value       = module.cosmosdb.cosmosdb_secondary_readonly_key
}

output "cosmosdb_systemassigned_identity" {
  description = "Cosmos DB System Assigned Identity (Tenant ID and Principal ID)"
  value       = module.cosmosdb.cosmosdb_systemassigned_identity
}

output "cosmosdb_write_endpoint" {
  description = "Cosmos DB Write Endpoint"
  value       = module.cosmosdb.cosmosdb_write_endpoint
}

output "gremlin_db_id" {
  description = "Gremlin API DB IDs"
  value       = module.cosmosdb.gremlin_db_id
}

output "gremlin_graph_id" {
  description = "Gremlin API Graph IDs"
  value       = module.cosmosdb.gremlin_graph_id
}

output "mongo_db_collection_id" {
  description = "Mongo API Collection IDs"
  value       = module.cosmosdb.mongo_db_collection_id
}

output "mongo_db_id" {
  description = "Mongo API DB IDs"
  value       = module.cosmosdb.mongo_db_id
}

output "sql_containers_id" {
  description = "SQL API Container IDs"
  value       = module.cosmosdb.sql_containers_id
}

output "sql_db_id" {
  description = "SQL API DB IDs"
  value       = module.cosmosdb.sql_db_id
}

output "table_id" {
  description = "Table API Table IDs"
  value       = module.cosmosdb.table_id
}


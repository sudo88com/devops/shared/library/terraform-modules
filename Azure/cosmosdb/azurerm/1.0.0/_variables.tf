
variable "additional_capabilities" {
  type        = list(string)
  description = "List of additional capabilities for Cosmos DB API. - possible options are DisableRateLimitingResponses, EnableAggregationPipeline, EnableServerless, mongoEnableDocLevelTTL, MongoDBv3.4, AllowSelfServeUpgradeToMongo36"
  default     = []
}

variable "analytical_storage" {
  type        = object({
    enabled     = bool
    schema_type = string
  })
  description = "Analytical storage enablement."
  default     = {
  "enabled": false,
  "schema_type": null
}
}

variable "application_name" {
  type        = string
  description = "Name of the application."
  default     = "dev"
}

variable "auto_failover" {
  type        = bool
  description = "Enable automatic fail over for this Cosmos DB account - can be either true or false"
  default     = false
}

variable "azure_dc_access" {
  type        = list(string)
  description = "List of ip address to enable the Accept connections from within public Azure datacenters behavior."
  default     = [
  "0.0.0.0"
]
}

variable "azure_portal_access" {
  type        = list(string)
  description = "List of ip address to enable the Allow access from the Azure portal behavior."
  default     = [
  "104.42.195.92,40.76.54.131,52.176.6.30,52.169.50.45,52.187.184.26"
]
}

variable "backup_enabled" {
  type        = bool
  description = "Enable backup for this Cosmos DB account"
  default     = true
}

variable "backup_interval" {
  type        = string
  description = "The interval in minutes between two backups. This is configurable only when type is Periodic. Possible values are between 60 and 1440."
  default     = 60
}

variable "backup_retention" {
  type        = string
  description = "The time in hours that each backup is retained. This is configurable only when type is Periodic. Possible values are between 8 and 720."
  default     = 8
}

variable "backup_storage_redundancy" {
  type        = string
  description = "he storage redundancy which is used to indicate type of backup residency. This is configurable only when type is Periodic. Possible values are Geo, Local and Zone"
  default     = "Geo"
}

variable "backup_type" {
  type        = string
  description = "Type of backup - can be either Periodic or Continuous"
  default     = "periodic"
}

variable "capabilities" {
  type        = map(any)
  description = "Map of non-sql DB API to enable support for API other than SQL"
  default     = {
  "cassandra": "EnableCassandra",
  "gremlin": "EnableGremlin",
  "mongo": "EnableMongo",
  "sql": "SQL",
  "table": "EnableTable"
}
}

variable "cassandra_keyspaces" {
  type        = map(object({
    keyspace_name           = string
    keyspace_throughput     = number
    keyspace_max_throughput = number
  }))
  description = "List of Cosmos DB Cassandra keyspaces to create. Some parameters are inherited from cosmos account."
  default     = {}
}

variable "cassandra_tables" {
  type        = map(object({
    table_name             = string
    keyspace_name          = string
    default_ttl_seconds    = string
    analytical_storage_ttl = number
    table_throughout       = number
    table_max_throughput   = number
    cassandra_schema_settings = object({
      column = map(object({
        column_key_name = string
        column_key_type = string
      }))
      partition_key = map(object({
        partition_key_name = string
      }))
      cluster_key = map(object({
        cluster_key_name     = string
        cluster_key_order_by = string
      }))
    })
  }))
  description = "List of Cosmos DB Cassandra tables to create. Some parameters are inherited from cosmos account."
  default     = {}
}

variable "consistency_level" {
  type        = string
  description = "The Consistency Level to use for this CosmosDB Account - can be either BoundedStaleness, Eventual, Session, Strong or ConsistentPrefix"
  default     = "Eventual"
}

variable "cosmos_account_name" {
  type        = string
  description = "Name of the Cosmos DB account."
  default     = ""
}

variable "cosmos_api" {
  type        = string
  description = ""
  default     = ""
}

variable "enable_systemassigned_identity" {
  type        = bool
  description = "Enable System Assigned Identity"
  default     = false
}

variable "environment" {
  type        = string
  description = "Name of the environment. Example dev, test, qa, cert, prod etc...."
  default     = "dev"
}

variable "event_hub" {
  type        = map(object({
    event_hub_name           = string
    event_hub_namespace_name = string
    event_hub_rg_name        = string
    event_hub_auth_rule_name = string
  }))
  description = "Event Hub parameters for one or more event hub to send daignostic logs to event hub."
  default     = {}
}

variable "firewall_ip" {
  type        = list(string)
  description = "List of ip address to allow access from the internet or on-premisis network."
  default     = []
}

variable "free_tier" {
  type        = bool
  description = "Enable Free Tier pricing option for this Cosmos DB account - can be either true or false"
  default     = false
}

variable "geo_locations" {
  type        = any
  description = "List of map of geo locations and other properties to create primary and secodanry databasees."
  default     = [
  {
    "failover_priority": 0,
    "geo_location": "eastus",
    "zone_redundant": false
  }
]
}

variable "gremlin_dbs" {
  type        = map(object({
    db_name           = string
    db_throughput     = number
    db_max_throughput = number
  }))
  description = "Map of Cosmos DB Gremlin DBs to create. Some parameters are inherited from cosmos account."
  default     = {}
}

variable "gremlin_graphs" {
  type        = map(object({
    graph_name            = string
    db_name               = string
    partition_key_path    = string
    partition_key_version = number
    default_ttl_seconds   = string
    graph_throughput      = number
    graph_max_throughput  = number
    index_policy_settings = object({
      indexing_automatic = bool
      indexing_mode      = string
      included_paths     = list(string)
      excluded_paths     = list(string)
      composite_indexes = map(object({
        indexes = set(object({
          index_path  = string
          index_order = string
        }))
      }))
      spatial_indexes = map(object({
        spatial_index_path = string
      }))
    })
    conflict_resolution_policy = object({
      mode      = string
      path      = string
      procedure = string
    })
    unique_key = list(string)
  }))
  description = "List of Cosmos DB Gremlin Graph to create. Some parameters are inherited from cosmos account."
  default     = {}
}

variable "ip_firewall_enabled" {
  type        = bool
  description = "Enable ip firewwall to allow connection to this cosmosdb from client's machine and from azure portal."
  default     = true
}

variable "key_vault_key_name" {
  type        = string
  description = "Name of the existing key in key vault. It is needed for encryption using customer managed key."
  default     = ""
}

variable "key_vault_name" {
  type        = string
  description = "Name of the existing key vault. It is needed for encryption using customer managed key."
  default     = ""
}

variable "key_vault_rg_name" {
  type        = string
  description = "Name of the resource group in which key vault exists."
  default     = ""
}

variable "location" {
  type        = string
  description = "Cosmos DB deployment region. Can be different vs. RG location"
  default     = ""
}

variable "log_analytics" {
  type        = map(object({
    la_workspace_name    = string
    la_workspace_rg_name = string
  }))
  description = "Log Analytics parameters for one or more log analytics workspace to send daignostic logs to log analytics workspace."
  default     = {}
}

variable "logs_config" {
  type        = map(any)
  description = "Map of non-sql DB API logs configuration to enable logging for respective API"
  default     = {
  "cassandra": "CassandraRequests",
  "gremlin": "GremlinRequests",
  "mongo": "MongoRequests",
  "sql": "SQL",
  "table": "TableApiRequests"
}
}

variable "max_interval_in_seconds" {
  type        = string
  description = "When used with the Bounded Staleness consistency level, this value represents the time amount of staleness (in seconds) tolerated. Accepted range for this value is 5 - 86400 (1 day)"
  default     = 300
}

variable "max_staleness_prefix" {
  type        = string
  description = "When used with the Bounded Staleness consistency level, this value represents the number of stale requests tolerated. Accepted range for this value is 10 – 2147483647"
  default     = 100000
}

variable "mongo_db_collections" {
  type        = map(object({
    collection_name           = string
    db_name                   = string
    default_ttl_seconds       = string
    shard_key                 = string
    collection_throughout     = number
    collection_max_throughput = number
    analytical_storage_ttl    = number
    indexes = map(object({
      mongo_index_keys   = list(string)
      mongo_index_unique = bool
    }))
  }))
  description = "List of Cosmos DB Mongo collections to create. Some parameters are inherited from cosmos account."
  default     = {}
}

variable "mongo_dbs" {
  type        = map(object({
    db_name           = string
    db_throughput     = number
    db_max_throughput = number
  }))
  description = "Map of Cosmos DB Mongo DBs to create. Some parameters are inherited from cosmos account."
  default     = {}
}

variable "multi_region_write" {
  type        = bool
  description = "Enable multiple write locations for this Cosmos DB account"
  default     = false
}

variable "pe_subresource" {
  type        = map(any)
  description = "Map of subresources to choose appropriate Private Endpoint sub resource for DB API"
  default     = {
  "cassandra": "Cassandra",
  "gremlin": "Gremlin",
  "mongo": "MongoDB",
  "sql": "SQL",
  "table": "Table"
}
}

variable "private_dns_zone_name" {
  type        = map(any)
  description = "Map of the private DNS zone to choose approrite Private DNS Zone for DB API."
  default     = {
  "cassandra": "privatelink.cassandra.cosmos.azure.com",
  "gremlin": "privatelink.gremlin.cosmos.azure.com",
  "mongo": "privatelink.mongo.cosmos.azure.com",
  "sql": "privatelink.documents.azure.com",
  "table": "privatelink.table.cosmos.azure.com"
}
}

variable "private_endpoint" {
  type        = map(object({
    name                            = string
    vnet_rg_name                    = string
    vnet_name                       = string
    subnet_name                     = string
    dns_zone_rg_name                = string
    enable_private_dns_entry        = bool
    dns_zone_group_name             = string
    private_service_connection_name = string
    is_manual_connection            = bool
  }))
  description = "Parameters for private endpoint creation"
  default     = {}
}

variable "public_network_access_enabled" {
  type        = bool
  description = "Enable public network access to cosmos db"
  default     = false
}

variable "resource_group_name" {
  type        = string
  description = "Name of the azure resource group."
  default     = ""
}

variable "sql_db_containers" {
  type        = map(object({
    container_name           = string
    db_name                  = string
    partition_key_path       = string
    partition_key_version    = number
    container_throughout     = number
    container_max_throughput = number
    default_ttl              = number
    analytical_storage_ttl   = number
    indexing_policy_settings = object({
      sql_indexing_mode = string
      sql_included_path = string
      sql_excluded_path = string
      composite_indexes = map(object({
        indexes = set(object({
          path  = string
          order = string
        }))
      }))
      spatial_indexes = map(object({
        path = string
      }))
    })
    sql_unique_key = list(string)
    conflict_resolution_policy = object({
      mode      = string
      path      = string
      procedure = string
    })
  }))
  description = "List of Cosmos DB SQL Containers to create. Some parameters are inherited from cosmos account."
  default     = {}
}

variable "sql_dbs" {
  type        = map(object({
    db_name           = string
    db_throughput     = number
    db_max_throughput = number
  }))
  description = "Map of Cosmos DB SQL DBs to create. Some parameters are inherited from cosmos account."
  default     = {}
}

variable "storage_account" {
  type        = map(object({
    storage_account_name         = string
    storage_account_rg_name      = string
    enable_logs_retention_policy = bool
    logs_retention_days          = number
  }))
  description = "Storage account parameters for one or more storage account to send daignostic logs to storage account."
  default     = {}
}

variable "tables" {
  type        = map(object({
    table_name           = string
    table_throughput     = number
    table_max_throughput = number
  }))
  description = "Map of Cosmos DB Tables to create. Some parameters are inherited from cosmos account."
  default     = {}
}


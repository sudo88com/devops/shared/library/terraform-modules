
module "manageddisk" {
  source = "terraform-aws-modules/manageddisk/aws"
  version = "1.0.0"
  disk_size_gb = var.disk_size_gb
  image_reference_id = var.image_reference_id
  import_or_copy_os_type = var.import_or_copy_os_type
  location = var.location
  managed_disk_name = var.managed_disk_name
  resource_group_name = var.resource_group_name
  source_resource_id = var.source_resource_id
  source_uri = var.source_uri
  storage_account_type = var.storage_account_type
  tags = var.tags
}

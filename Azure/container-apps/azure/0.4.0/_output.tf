
output "container_app_environment_id" {
  description = "The ID of the Container App Environment within which this Container App should exist."
  value       = module.container-apps.container_app_environment_id
}

output "container_app_fqdn" {
  description = "The FQDN of the Container App's ingress."
  value       = module.container-apps.container_app_fqdn
}

output "container_app_identities" {
  description = "The identities of the Container App, key is Container App's name."
  value       = module.container-apps.container_app_identities
}

output "container_app_ips" {
  description = "The IPs of the Latest Revision of the Container App."
  value       = module.container-apps.container_app_ips
}


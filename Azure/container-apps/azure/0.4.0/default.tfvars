
container_app_environment = null

container_app_environment_infrastructure_subnet_id = null

container_app_environment_internal_load_balancer_enabled = null

container_app_environment_name = 

container_app_environment_tags = {}

container_app_secrets = {}

container_apps = 

dapr_component = {}

dapr_component_secrets = {}

env_storage = {}

environment_storage_access_key = null

location = 

log_analytics_workspace = null

log_analytics_workspace_allow_resource_only_permissions = true

log_analytics_workspace_cmk_for_query_forced = false

log_analytics_workspace_daily_quota_gb = -1

log_analytics_workspace_internet_ingestion_enabled = true

log_analytics_workspace_internet_query_enabled = true

log_analytics_workspace_local_authentication_disabled = false

log_analytics_workspace_name = null

log_analytics_workspace_reservation_capacity_in_gb_per_day = null

log_analytics_workspace_retention_in_days = null

log_analytics_workspace_sku = "PerGB2018"

log_analytics_workspace_tags = null

resource_group_name = 

tracing_tags_enabled = false

tracing_tags_prefix = "avm_"


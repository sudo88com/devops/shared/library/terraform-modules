
diagnostic_settings = {}

enable_telemetry = true

lock = null

private_endpoints = {}

resource_group_name = 

role_assignments = {}

tags = null

tracing_tags_enabled = false

tracing_tags_prefix = "avm_"

virtual_desktop_host_pool_custom_rdp_properties = null

virtual_desktop_host_pool_description = null

virtual_desktop_host_pool_friendly_name = null

virtual_desktop_host_pool_load_balancer_type = 

virtual_desktop_host_pool_location = 

virtual_desktop_host_pool_maximum_sessions_allowed = null

virtual_desktop_host_pool_name = 

virtual_desktop_host_pool_personal_desktop_assignment_type = null

virtual_desktop_host_pool_preferred_app_group_type = null

virtual_desktop_host_pool_resource_group_name = 

virtual_desktop_host_pool_scheduled_agent_updates = null

virtual_desktop_host_pool_start_vm_on_connect = null

virtual_desktop_host_pool_tags = null

virtual_desktop_host_pool_timeouts = null

virtual_desktop_host_pool_type = 

virtual_desktop_host_pool_validate_environment = null


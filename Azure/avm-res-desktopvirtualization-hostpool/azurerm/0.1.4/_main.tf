
module "avm-res-desktopvirtualization-hostpool" {
  source = "terraform-aws-modules/avm-res-desktopvirtualization-hostpool/aws"
  version = "0.1.4"
  diagnostic_settings = var.diagnostic_settings
  enable_telemetry = var.enable_telemetry
  lock = var.lock
  private_endpoints = var.private_endpoints
  resource_group_name = var.resource_group_name
  role_assignments = var.role_assignments
  tags = var.tags
  tracing_tags_enabled = var.tracing_tags_enabled
  tracing_tags_prefix = var.tracing_tags_prefix
  virtual_desktop_host_pool_custom_rdp_properties = var.virtual_desktop_host_pool_custom_rdp_properties
  virtual_desktop_host_pool_description = var.virtual_desktop_host_pool_description
  virtual_desktop_host_pool_friendly_name = var.virtual_desktop_host_pool_friendly_name
  virtual_desktop_host_pool_load_balancer_type = var.virtual_desktop_host_pool_load_balancer_type
  virtual_desktop_host_pool_location = var.virtual_desktop_host_pool_location
  virtual_desktop_host_pool_maximum_sessions_allowed = var.virtual_desktop_host_pool_maximum_sessions_allowed
  virtual_desktop_host_pool_name = var.virtual_desktop_host_pool_name
  virtual_desktop_host_pool_personal_desktop_assignment_type = var.virtual_desktop_host_pool_personal_desktop_assignment_type
  virtual_desktop_host_pool_preferred_app_group_type = var.virtual_desktop_host_pool_preferred_app_group_type
  virtual_desktop_host_pool_resource_group_name = var.virtual_desktop_host_pool_resource_group_name
  virtual_desktop_host_pool_scheduled_agent_updates = var.virtual_desktop_host_pool_scheduled_agent_updates
  virtual_desktop_host_pool_start_vm_on_connect = var.virtual_desktop_host_pool_start_vm_on_connect
  virtual_desktop_host_pool_tags = var.virtual_desktop_host_pool_tags
  virtual_desktop_host_pool_timeouts = var.virtual_desktop_host_pool_timeouts
  virtual_desktop_host_pool_type = var.virtual_desktop_host_pool_type
  virtual_desktop_host_pool_validate_environment = var.virtual_desktop_host_pool_validate_environment
}

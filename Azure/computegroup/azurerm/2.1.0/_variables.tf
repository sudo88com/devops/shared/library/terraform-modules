
variable "admin_password" {
  type        = string
  description = "The admin password to be used on the VMSS that will be deployed. The password must meet the complexity requirements of Azure"
  default     = ""
}

variable "admin_username" {
  type        = string
  description = "The admin username of the VMSS that will be deployed"
  default     = "azureuser"
}

variable "cmd_extension" {
  type        = string
  description = "Command to be excuted by the custom script extension"
  default     = "echo hello"
}

variable "computer_name_prefix" {
  type        = string
  description = "The prefix that will be used for the hostname of the instances part of the VM scale set"
  default     = "vmss"
}

variable "load_balancer_backend_address_pool_ids" {
  type        = string
  description = "The id of the backend address pools of the loadbalancer to which the VM scale set is attached"
  default     = ""
}

variable "location" {
  type        = string
  description = "The location where the resources will be created"
  default     = ""
}

variable "managed_disk_type" {
  type        = string
  description = "Type of managed disk for the VMs that will be part of this compute group. Allowable values are 'Standard_LRS' or 'Premium_LRS'."
  default     = "Standard_LRS"
}

variable "nb_instance" {
  type        = string
  description = "Specify the number of vm instances"
  default     = "1"
}

variable "network_profile" {
  type        = string
  description = "The name of the network profile that will be used in the VM scale set"
  default     = "terraformnetworkprofile"
}

variable "resource_group_name" {
  type        = string
  description = "The name of the resource group in which the resources will be created"
  default     = "vmssrg"
}

variable "ssh_key" {
  type        = string
  description = "Path to the public key to be used for ssh access to the VM"
  default     = "~/.ssh/id_rsa.pub"
}

variable "tags" {
  type        = map
  description = "A map of the tags to use on the resources that are deployed with this module."
  default     = {
  "source": "terraform"
}
}

variable "vm_os_id" {
  type        = string
  description = "The ID of the image that you want to deploy if you are using a custom image."
  default     = ""
}

variable "vm_os_offer" {
  type        = string
  description = "The name of the offer of the image that you want to deploy"
  default     = ""
}

variable "vm_os_publisher" {
  type        = string
  description = "The name of the publisher of the image that you want to deploy"
  default     = ""
}

variable "vm_os_simple" {
  type        = string
  description = "Specify Ubuntu or Windows to get the latest version of each os"
  default     = ""
}

variable "vm_os_sku" {
  type        = string
  description = "The sku of the image that you want to deploy"
  default     = ""
}

variable "vm_os_version" {
  type        = string
  description = "The version of the image that you want to deploy."
  default     = "latest"
}

variable "vm_size" {
  type        = string
  description = "Size of the Virtual Machine based on Azure sizing"
  default     = "Standard_A0"
}

variable "vmscaleset_name" {
  type        = string
  description = "The name of the VM scale set that will be created in Azure"
  default     = "vmscaleset"
}

variable "vnet_subnet_id" {
  type        = string
  description = "The subnet id of the virtual network on which the vm scale set will be connected"
  default     = ""
}



module "computegroup" {
  source = "terraform-aws-modules/computegroup/aws"
  version = "2.1.0"
  admin_password = var.admin_password
  admin_username = var.admin_username
  cmd_extension = var.cmd_extension
  computer_name_prefix = var.computer_name_prefix
  load_balancer_backend_address_pool_ids = var.load_balancer_backend_address_pool_ids
  location = var.location
  managed_disk_type = var.managed_disk_type
  nb_instance = var.nb_instance
  network_profile = var.network_profile
  resource_group_name = var.resource_group_name
  ssh_key = var.ssh_key
  tags = var.tags
  vm_os_id = var.vm_os_id
  vm_os_offer = var.vm_os_offer
  vm_os_publisher = var.vm_os_publisher
  vm_os_simple = var.vm_os_simple
  vm_os_sku = var.vm_os_sku
  vm_os_version = var.vm_os_version
  vm_size = var.vm_size
  vmscaleset_name = var.vmscaleset_name
  vnet_subnet_id = var.vnet_subnet_id
}


additional_unattend_contents = []

admin_credential_key_vault_resource_id = null

admin_generated_ssh_key_vault_secret_name = null

admin_password = null

admin_password_key_vault_secret_name = null

admin_ssh_keys = []

admin_username = "azureuser"

allow_extension_operations = true

availability_set_resource_id = null

azure_backup_configurations = {}

boot_diagnostics = false

boot_diagnostics_storage_account_uri = null

bypass_platform_safety_checks_on_user_schedule_enabled = false

capacity_reservation_group_resource_id = null

computer_name = null

custom_data = null

data_disk_managed_disks = {}

dedicated_host_group_resource_id = null

dedicated_host_resource_id = null

diagnostic_settings = {}

disable_password_authentication = true

disk_controller_type = null

edge_zone = null

enable_automatic_updates = true

enable_telemetry = true

encryption_at_host_enabled = null

eviction_policy = null

extensions = {}

extensions_time_budget = "PT1H30M"

gallery_applications = {}

generate_admin_password_or_ssh_key = true

generated_secrets_key_vault_secret_config = {}

hotpatching_enabled = false

license_type = null

location = 

lock = null

maintenance_configuration_resource_ids = {}

managed_identities = {}

max_bid_price = -1

name = 

network_interfaces = {
  "ipconfig_1": {
    "accelerated_networking_enabled": true,
    "dns_servers": null,
    "internal_dns_name_label": null,
    "ip_configurations": {
      "ip_config_1": {
        "gateway_load_balancer_frontend_ip_configuration_resource_id": null,
        "is_primary_ipconfiguration": true,
        "name": "ipv4-ipconfig",
        "private_ip_address": null,
        "private_ip_address_allocation": "Dynamic",
        "private_ip_address_version": "IPv4",
        "private_ip_subnet_resource_id": null,
        "public_ip_address_resource_id": null
      }
    },
    "ip_forwarding_enabled": false,
    "name": "default-ipv4-ipconfig",
    "tags": null
  }
}

os_disk = {
  "caching": "ReadWrite",
  "storage_account_type": "StandardSSD_LRS"
}

patch_assessment_mode = "ImageDefault"

patch_mode = null

plan = null

platform_fault_domain = null

priority = "Regular"

provision_vm_agent = true

proximity_placement_group_resource_id = null

public_ip_configuration_details = {
  "allocation_method": "Static",
  "ddos_protection_mode": "VirtualNetworkInherited",
  "idle_timeout_in_minutes": 30,
  "ip_version": "IPv4",
  "sku": "Standard",
  "sku_tier": "Regional"
}

reboot_setting = null

resource_group_name = 

role_assignments = {}

role_assignments_system_managed_identity = {}

secrets = []

secure_boot_enabled = null

shutdown_schedules = {}

source_image_reference = null

source_image_resource_id = null

tags = null

termination_notification = null

timezone = null

user_data = null

virtual_machine_scale_set_resource_id = null

virtualmachine_os_type = "Windows"

virtualmachine_sku_size = 

vm_additional_capabilities = null

vtpm_enabled = null

winrm_listeners = []

zone = 


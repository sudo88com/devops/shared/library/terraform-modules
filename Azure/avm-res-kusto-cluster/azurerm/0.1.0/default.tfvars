
allowed_fqdns = null

allowed_ip_ranges = null

auto_stop_enabled = true

customer_managed_key = {}

databases = {}

diagnostic_settings = {}

disk_encryption_enabled = true

double_encryption_enabled = null

enable_telemetry = true

kusto_cluster_principal_assignments = {}

kusto_database_principal_assignment = {}

language_extensions = null

location = null

lock = {}

managed_identities = null

name = 

optimized_auto_scale = null

outbound_network_access_restricted = false

private_endpoints = {}

public_ip_type = "IPv4"

public_network_access_enabled = false

purge_enabled = null

resource_group_name = 

role_assignments = {}

sku = 

streaming_ingestion_enabled = null

tags = null

trusted_external_tenants = []

virtual_network_configuration = null

zones = null


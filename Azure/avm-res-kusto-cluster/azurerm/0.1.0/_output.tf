
output "data_ingestion_uri" {
  description = "The Kusto Cluster URI to be used for data ingestion."
  value       = module.avm-res-kusto-cluster.data_ingestion_uri
}

output "id" {
  description = "The Kusto Cluster ID."
  value       = module.avm-res-kusto-cluster.id
}

output "identity" {
  description = "An identity block exports the following:\n\nprincipal_id - The Principal ID associated with this System Assigned Managed Service Identity.\n\ntenant_id - The Tenant ID associated with this System Assigned Managed Service Identity.\n"
  value       = module.avm-res-kusto-cluster.identity
}

output "private_endpoints" {
  description = "A map of private endpoints. The map key is the supplied input to var.private_endpoints. The map value is the entire azurerm_private_endpoint resource."
  value       = module.avm-res-kusto-cluster.private_endpoints
}

output "resource" {
  description = "This is the full output for the resource."
  value       = module.avm-res-kusto-cluster.resource
}

output "uri" {
  description = "The FQDN of the Azure Kusto Cluster."
  value       = module.avm-res-kusto-cluster.uri
}


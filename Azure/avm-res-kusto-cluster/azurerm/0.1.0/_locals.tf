
locals {
  allowed_fqdns = var.allowed_fqdns
  allowed_ip_ranges = var.allowed_ip_ranges
  auto_stop_enabled = var.auto_stop_enabled
  customer_managed_key = var.customer_managed_key
  databases = var.databases
  diagnostic_settings = var.diagnostic_settings
  disk_encryption_enabled = var.disk_encryption_enabled
  double_encryption_enabled = var.double_encryption_enabled
  enable_telemetry = var.enable_telemetry
  kusto_cluster_principal_assignments = var.kusto_cluster_principal_assignments
  kusto_database_principal_assignment = var.kusto_database_principal_assignment
  language_extensions = var.language_extensions
  location = var.location
  lock = var.lock
  managed_identities = var.managed_identities
  name = var.name
  optimized_auto_scale = var.optimized_auto_scale
  outbound_network_access_restricted = var.outbound_network_access_restricted
  private_endpoints = var.private_endpoints
  public_ip_type = var.public_ip_type
  public_network_access_enabled = var.public_network_access_enabled
  purge_enabled = var.purge_enabled
  resource_group_name = var.resource_group_name
  role_assignments = var.role_assignments
  sku = var.sku
  streaming_ingestion_enabled = var.streaming_ingestion_enabled
  tags = var.tags
  trusted_external_tenants = var.trusted_external_tenants
  virtual_network_configuration = var.virtual_network_configuration
  zones = var.zones
}

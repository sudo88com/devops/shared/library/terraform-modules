
automation_account_encryption = null

automation_account_identity = null

automation_account_local_authentication_enabled = true

automation_account_location = null

automation_account_name = 

automation_account_public_network_access_enabled = true

automation_account_sku_name = "Basic"

linked_automation_account_creation_enabled = true

location = 

log_analytics_solution_plans = [
  {
    "product": "OMSGallery/AgentHealthAssessment",
    "publisher": "Microsoft"
  },
  {
    "product": "OMSGallery/AntiMalware",
    "publisher": "Microsoft"
  },
  {
    "product": "OMSGallery/ChangeTracking",
    "publisher": "Microsoft"
  },
  {
    "product": "OMSGallery/ContainerInsights",
    "publisher": "Microsoft"
  },
  {
    "product": "OMSGallery/Security",
    "publisher": "Microsoft"
  },
  {
    "product": "OMSGallery/SecurityInsights",
    "publisher": "Microsoft"
  },
  {
    "product": "OMSGallery/ServiceMap",
    "publisher": "Microsoft"
  },
  {
    "product": "OMSGallery/SQLAdvancedThreatProtection",
    "publisher": "Microsoft"
  },
  {
    "product": "OMSGallery/SQLAssessment",
    "publisher": "Microsoft"
  },
  {
    "product": "OMSGallery/SQLVulnerabilityAssessment",
    "publisher": "Microsoft"
  },
  {
    "product": "OMSGallery/Updates",
    "publisher": "Microsoft"
  },
  {
    "product": "OMSGallery/VMInsights",
    "publisher": "Microsoft"
  }
]

log_analytics_workspace_allow_resource_only_permissions = false

log_analytics_workspace_cmk_for_query_forced = null

log_analytics_workspace_daily_quota_gb = null

log_analytics_workspace_internet_ingestion_enabled = true

log_analytics_workspace_internet_query_enabled = true

log_analytics_workspace_name = 

log_analytics_workspace_reservation_capacity_in_gb_per_day = null

log_analytics_workspace_retention_in_days = 30

log_analytics_workspace_sku = "PerGB2018"

resource_group_creation_enabled = true

resource_group_name = 

tags = {}

tracing_tags_enabled = false

tracing_tags_prefix = "avm_"



output "automation_account" {
  description = "A curated output of the Azure Automation Account."
  value       = module.alz-management.automation_account
}

output "log_analytics_workspace" {
  description = "A curated output of the Log Analytics Workspace."
  value       = module.alz-management.log_analytics_workspace
}

output "resource_group" {
  description = "A curated output of the Azure Resource Group."
  value       = module.alz-management.resource_group
}


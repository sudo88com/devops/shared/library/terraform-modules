
output "resource" {
  description = "\"This is the full output for the resource. This is the default output for the module following AVM standards. Review the examples below for the correct output to use in your module.\"\nExamples:\n- module.firewall.resource.id\n- module.firewall.resource.name\n- module.firewall.resource.ip_configuration\n- module.firewall.resource.virtual_hub\n"
  value       = module.avm-res-network-azurefirewall.resource
}


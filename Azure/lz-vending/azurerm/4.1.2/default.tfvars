
budget_enabled = false

budgets = {}

disable_telemetry = false

location = 

network_watcher_resource_group_enabled = false

resource_group_creation_enabled = false

resource_groups = {}

role_assignment_enabled = false

role_assignments = {}

subscription_alias_enabled = false

subscription_alias_name = ""

subscription_billing_scope = ""

subscription_display_name = ""

subscription_id = ""

subscription_management_group_association_enabled = false

subscription_management_group_id = ""

subscription_register_resource_providers_and_features = {
  "Microsoft.AVS": [],
  "Microsoft.ApiManagement": [],
  "Microsoft.AppPlatform": [],
  "Microsoft.Authorization": [],
  "Microsoft.Automation": [],
  "Microsoft.Blueprint": [],
  "Microsoft.BotService": [],
  "Microsoft.Cache": [],
  "Microsoft.Cdn": [],
  "Microsoft.CognitiveServices": [],
  "Microsoft.Compute": [],
  "Microsoft.ContainerInstance": [],
  "Microsoft.ContainerRegistry": [],
  "Microsoft.ContainerService": [],
  "Microsoft.CostManagement": [],
  "Microsoft.CustomProviders": [],
  "Microsoft.DBforMariaDB": [],
  "Microsoft.DBforMySQL": [],
  "Microsoft.DBforPostgreSQL": [],
  "Microsoft.DataLakeAnalytics": [],
  "Microsoft.DataLakeStore": [],
  "Microsoft.DataMigration": [],
  "Microsoft.DataProtection": [],
  "Microsoft.Databricks": [],
  "Microsoft.DesktopVirtualization": [],
  "Microsoft.DevTestLab": [],
  "Microsoft.Devices": [],
  "Microsoft.DocumentDB": [],
  "Microsoft.EventGrid": [],
  "Microsoft.EventHub": [],
  "Microsoft.GuestConfiguration": [],
  "Microsoft.HDInsight": [],
  "Microsoft.HealthcareApis": [],
  "Microsoft.KeyVault": [],
  "Microsoft.Kusto": [],
  "Microsoft.Logic": [],
  "Microsoft.MachineLearningServices": [],
  "Microsoft.Maintenance": [],
  "Microsoft.ManagedIdentity": [],
  "Microsoft.ManagedServices": [],
  "Microsoft.Management": [],
  "Microsoft.Maps": [],
  "Microsoft.MarketplaceOrdering": [],
  "Microsoft.Media": [],
  "Microsoft.MixedReality": [],
  "Microsoft.Network": [],
  "Microsoft.NotificationHubs": [],
  "Microsoft.OperationalInsights": [],
  "Microsoft.OperationsManagement": [],
  "Microsoft.PolicyInsights": [],
  "Microsoft.PowerBIDedicated": [],
  "Microsoft.RecoveryServices": [],
  "Microsoft.Relay": [],
  "Microsoft.Resources": [],
  "Microsoft.Search": [],
  "Microsoft.Security": [],
  "Microsoft.SecurityInsights": [],
  "Microsoft.ServiceBus": [],
  "Microsoft.ServiceFabric": [],
  "Microsoft.Sql": [],
  "Microsoft.Storage": [],
  "Microsoft.StreamAnalytics": [],
  "Microsoft.TimeSeriesInsights": [],
  "Microsoft.Web": [],
  "microsoft.insights": []
}

subscription_register_resource_providers_enabled = false

subscription_tags = {}

subscription_update_existing = false

subscription_use_azapi = false

subscription_workload = ""

umi_enabled = false

umi_federated_credentials_advanced = {}

umi_federated_credentials_github = {}

umi_federated_credentials_terraform_cloud = {}

umi_name = ""

umi_resource_group_creation_enabled = true

umi_resource_group_lock_enabled = true

umi_resource_group_lock_name = ""

umi_resource_group_name = ""

umi_resource_group_tags = {}

umi_role_assignments = {}

umi_tags = {}

virtual_network_enabled = false

virtual_networks = {}

wait_for_subscription_before_subscription_operations = {}


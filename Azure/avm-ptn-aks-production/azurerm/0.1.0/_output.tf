
output "resource" {
  description = "This is the full output for the resource."
  value       = module.avm-ptn-aks-production.resource
}

output "resource_id" {
  description = "The `azurerm_kubernetes_cluster`'s resource id."
  value       = module.avm-ptn-aks-production.resource_id
}


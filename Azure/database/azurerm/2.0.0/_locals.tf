
locals {
  collation = var.collation
  db_edition = var.db_edition
  db_name = var.db_name
  end_ip_address = var.end_ip_address
  location = var.location
  resource_group_name = var.resource_group_name
  server_version = var.server_version
  service_objective_name = var.service_objective_name
  sql_admin_username = var.sql_admin_username
  sql_password = var.sql_password
  start_ip_address = var.start_ip_address
  tags = var.tags
  tracing_tags_enabled = var.tracing_tags_enabled
  tracing_tags_prefix = var.tracing_tags_prefix
}

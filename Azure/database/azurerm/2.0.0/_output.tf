
output "connection_string" {
  description = "Connection string for the Azure SQL Database created."
  value       = module.database.connection_string
}

output "database_name" {
  description = "Database name of the Azure SQL Database created."
  value       = module.database.database_name
}

output "sql_server_fqdn" {
  description = "Fully Qualified Domain Name (FQDN) of the Azure SQL Database created."
  value       = module.database.sql_server_fqdn
}

output "sql_server_location" {
  description = "Location of the Azure SQL Database created."
  value       = module.database.sql_server_location
}

output "sql_server_name" {
  description = "Server name of the Azure SQL Database created."
  value       = module.database.sql_server_name
}

output "sql_server_version" {
  description = "Version the Azure SQL Database created."
  value       = module.database.sql_server_version
}


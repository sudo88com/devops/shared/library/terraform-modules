
output "vnet_address_space" {
  description = "The address space of the newly created vNet"
  value       = module.subnets.vnet_address_space
}

output "vnet_id" {
  description = "The id of the newly created vNet"
  value       = module.subnets.vnet_id
}

output "vnet_location" {
  description = "The location of the newly created vNet"
  value       = module.subnets.vnet_location
}

output "vnet_name" {
  description = "The Name of the newly created vNet"
  value       = module.subnets.vnet_name
}

output "vnet_subnets_name_id" {
  description = "Can be queried subnet-id by subnet name by using lookup(module.vnet.vnet_subnets_name_id, subnet1)"
  value       = module.subnets.vnet_subnets_name_id
}


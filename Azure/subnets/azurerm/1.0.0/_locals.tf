
locals {
  resource_group_name = var.resource_group_name
  subnets = var.subnets
  virtual_network_address_space = var.virtual_network_address_space
  virtual_network_bgp_community = var.virtual_network_bgp_community
  virtual_network_ddos_protection_plan = var.virtual_network_ddos_protection_plan
  virtual_network_dns_servers = var.virtual_network_dns_servers
  virtual_network_edge_zone = var.virtual_network_edge_zone
  virtual_network_flow_timeout_in_minutes = var.virtual_network_flow_timeout_in_minutes
  virtual_network_location = var.virtual_network_location
  virtual_network_name = var.virtual_network_name
  virtual_network_tags = var.virtual_network_tags
}

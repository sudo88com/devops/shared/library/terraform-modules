
output "analysis_services_server" {
  description = "Analysis Services Server"
  value       = module.naming.analysis_services_server
}

output "api_management" {
  description = "Api Management"
  value       = module.naming.api_management
}

output "app_configuration" {
  description = "App Configuration"
  value       = module.naming.app_configuration
}

output "app_service" {
  description = "App Service"
  value       = module.naming.app_service
}

output "app_service_environment" {
  description = "App Service Environment"
  value       = module.naming.app_service_environment
}

output "app_service_plan" {
  description = "App Service Plan"
  value       = module.naming.app_service_plan
}

output "application_gateway" {
  description = "Application Gateway"
  value       = module.naming.application_gateway
}

output "application_insights" {
  description = "Application Insights"
  value       = module.naming.application_insights
}

output "application_security_group" {
  description = "Application Security Group"
  value       = module.naming.application_security_group
}

output "automation_account" {
  description = "Automation Account"
  value       = module.naming.automation_account
}

output "automation_certificate" {
  description = "Automation Certificate"
  value       = module.naming.automation_certificate
}

output "automation_credential" {
  description = "Automation Credential"
  value       = module.naming.automation_credential
}

output "automation_runbook" {
  description = "Automation Runbook"
  value       = module.naming.automation_runbook
}

output "automation_schedule" {
  description = "Automation Schedule"
  value       = module.naming.automation_schedule
}

output "automation_variable" {
  description = "Automation Variable"
  value       = module.naming.automation_variable
}

output "availability_set" {
  description = "Availability Set"
  value       = module.naming.availability_set
}

output "bastion_host" {
  description = "Bastion Host"
  value       = module.naming.bastion_host
}

output "batch_account" {
  description = "Batch Account"
  value       = module.naming.batch_account
}

output "batch_application" {
  description = "Batch Application"
  value       = module.naming.batch_application
}

output "batch_certificate" {
  description = "Batch Certificate"
  value       = module.naming.batch_certificate
}

output "batch_pool" {
  description = "Batch Pool"
  value       = module.naming.batch_pool
}

output "bot_channel_directline" {
  description = "Bot Channel Directline"
  value       = module.naming.bot_channel_directline
}

output "bot_channel_email" {
  description = "Bot Channel Email"
  value       = module.naming.bot_channel_email
}

output "bot_channel_ms_teams" {
  description = "Bot Channel Ms Teams"
  value       = module.naming.bot_channel_ms_teams
}

output "bot_channel_slack" {
  description = "Bot Channel Slack"
  value       = module.naming.bot_channel_slack
}

output "bot_channels_registration" {
  description = "Bot Channels Registration"
  value       = module.naming.bot_channels_registration
}

output "bot_connection" {
  description = "Bot Connection"
  value       = module.naming.bot_connection
}

output "bot_web_app" {
  description = "Bot Web App"
  value       = module.naming.bot_web_app
}

output "cdn_endpoint" {
  description = "Cdn Endpoint"
  value       = module.naming.cdn_endpoint
}

output "cdn_profile" {
  description = "Cdn Profile"
  value       = module.naming.cdn_profile
}

output "cognitive_account" {
  description = "Cognitive Account"
  value       = module.naming.cognitive_account
}

output "container_app" {
  description = "Container App"
  value       = module.naming.container_app
}

output "container_app_environment" {
  description = "Container App Environment"
  value       = module.naming.container_app_environment
}

output "container_group" {
  description = "Container Group"
  value       = module.naming.container_group
}

output "container_registry" {
  description = "Container Registry"
  value       = module.naming.container_registry
}

output "container_registry_webhook" {
  description = "Container Registry Webhook"
  value       = module.naming.container_registry_webhook
}

output "cosmosdb_account" {
  description = "Cosmosdb Account"
  value       = module.naming.cosmosdb_account
}

output "cosmosdb_cassandra_cluster" {
  description = "Cosmosdb Cassandra Cluster"
  value       = module.naming.cosmosdb_cassandra_cluster
}

output "cosmosdb_cassandra_datacenter" {
  description = "Cosmosdb Cassandra Datacenter"
  value       = module.naming.cosmosdb_cassandra_datacenter
}

output "cosmosdb_postgres" {
  description = "Cosmosdb Postgres"
  value       = module.naming.cosmosdb_postgres
}

output "custom_provider" {
  description = "Custom Provider"
  value       = module.naming.custom_provider
}

output "dashboard" {
  description = "Dashboard"
  value       = module.naming.dashboard
}

output "data_factory" {
  description = "Data Factory"
  value       = module.naming.data_factory
}

output "data_factory_dataset_mysql" {
  description = "Data Factory Dataset Mysql"
  value       = module.naming.data_factory_dataset_mysql
}

output "data_factory_dataset_postgresql" {
  description = "Data Factory Dataset Postgresql"
  value       = module.naming.data_factory_dataset_postgresql
}

output "data_factory_dataset_sql_server_table" {
  description = "Data Factory Dataset Sql Server Table"
  value       = module.naming.data_factory_dataset_sql_server_table
}

output "data_factory_integration_runtime_managed" {
  description = "Data Factory Integration Runtime Managed"
  value       = module.naming.data_factory_integration_runtime_managed
}

output "data_factory_linked_service_data_lake_storage_gen2" {
  description = "Data Factory Linked Service Data Lake Storage Gen2"
  value       = module.naming.data_factory_linked_service_data_lake_storage_gen2
}

output "data_factory_linked_service_key_vault" {
  description = "Data Factory Linked Service Key Vault"
  value       = module.naming.data_factory_linked_service_key_vault
}

output "data_factory_linked_service_mysql" {
  description = "Data Factory Linked Service Mysql"
  value       = module.naming.data_factory_linked_service_mysql
}

output "data_factory_linked_service_postgresql" {
  description = "Data Factory Linked Service Postgresql"
  value       = module.naming.data_factory_linked_service_postgresql
}

output "data_factory_linked_service_sql_server" {
  description = "Data Factory Linked Service Sql Server"
  value       = module.naming.data_factory_linked_service_sql_server
}

output "data_factory_pipeline" {
  description = "Data Factory Pipeline"
  value       = module.naming.data_factory_pipeline
}

output "data_factory_trigger_schedule" {
  description = "Data Factory Trigger Schedule"
  value       = module.naming.data_factory_trigger_schedule
}

output "data_lake_analytics_account" {
  description = "Data Lake Analytics Account"
  value       = module.naming.data_lake_analytics_account
}

output "data_lake_analytics_firewall_rule" {
  description = "Data Lake Analytics Firewall Rule"
  value       = module.naming.data_lake_analytics_firewall_rule
}

output "data_lake_store" {
  description = "Data Lake Store"
  value       = module.naming.data_lake_store
}

output "data_lake_store_firewall_rule" {
  description = "Data Lake Store Firewall Rule"
  value       = module.naming.data_lake_store_firewall_rule
}

output "database_migration_project" {
  description = "Database Migration Project"
  value       = module.naming.database_migration_project
}

output "database_migration_service" {
  description = "Database Migration Service"
  value       = module.naming.database_migration_service
}

output "databricks_cluster" {
  description = "Databricks Cluster"
  value       = module.naming.databricks_cluster
}

output "databricks_high_concurrency_cluster" {
  description = "Databricks High Concurrency Cluster"
  value       = module.naming.databricks_high_concurrency_cluster
}

output "databricks_standard_cluster" {
  description = "Databricks Standard Cluster"
  value       = module.naming.databricks_standard_cluster
}

output "databricks_workspace" {
  description = "Databricks Workspace"
  value       = module.naming.databricks_workspace
}

output "dev_test_lab" {
  description = "Dev Test Lab"
  value       = module.naming.dev_test_lab
}

output "dev_test_linux_virtual_machine" {
  description = "Dev Test Linux Virtual Machine"
  value       = module.naming.dev_test_linux_virtual_machine
}

output "dev_test_windows_virtual_machine" {
  description = "Dev Test Windows Virtual Machine"
  value       = module.naming.dev_test_windows_virtual_machine
}

output "disk_encryption_set" {
  description = "Disk Encryption Set"
  value       = module.naming.disk_encryption_set
}

output "dns_a_record" {
  description = "Dns A Record"
  value       = module.naming.dns_a_record
}

output "dns_aaaa_record" {
  description = "Dns Aaaa Record"
  value       = module.naming.dns_aaaa_record
}

output "dns_caa_record" {
  description = "Dns Caa Record"
  value       = module.naming.dns_caa_record
}

output "dns_cname_record" {
  description = "Dns Cname Record"
  value       = module.naming.dns_cname_record
}

output "dns_mx_record" {
  description = "Dns Mx Record"
  value       = module.naming.dns_mx_record
}

output "dns_ns_record" {
  description = "Dns Ns Record"
  value       = module.naming.dns_ns_record
}

output "dns_ptr_record" {
  description = "Dns Ptr Record"
  value       = module.naming.dns_ptr_record
}

output "dns_txt_record" {
  description = "Dns Txt Record"
  value       = module.naming.dns_txt_record
}

output "dns_zone" {
  description = "Dns Zone"
  value       = module.naming.dns_zone
}

output "eventgrid_domain" {
  description = "Eventgrid Domain"
  value       = module.naming.eventgrid_domain
}

output "eventgrid_domain_topic" {
  description = "Eventgrid Domain Topic"
  value       = module.naming.eventgrid_domain_topic
}

output "eventgrid_event_subscription" {
  description = "Eventgrid Event Subscription"
  value       = module.naming.eventgrid_event_subscription
}

output "eventgrid_topic" {
  description = "Eventgrid Topic"
  value       = module.naming.eventgrid_topic
}

output "eventhub" {
  description = "Eventhub"
  value       = module.naming.eventhub
}

output "eventhub_authorization_rule" {
  description = "Eventhub Authorization Rule"
  value       = module.naming.eventhub_authorization_rule
}

output "eventhub_consumer_group" {
  description = "Eventhub Consumer Group"
  value       = module.naming.eventhub_consumer_group
}

output "eventhub_namespace" {
  description = "Eventhub Namespace"
  value       = module.naming.eventhub_namespace
}

output "eventhub_namespace_authorization_rule" {
  description = "Eventhub Namespace Authorization Rule"
  value       = module.naming.eventhub_namespace_authorization_rule
}

output "eventhub_namespace_disaster_recovery_config" {
  description = "Eventhub Namespace Disaster Recovery Config"
  value       = module.naming.eventhub_namespace_disaster_recovery_config
}

output "express_route_circuit" {
  description = "Express Route Circuit"
  value       = module.naming.express_route_circuit
}

output "express_route_gateway" {
  description = "Express Route Gateway"
  value       = module.naming.express_route_gateway
}

output "firewall" {
  description = "Firewall"
  value       = module.naming.firewall
}

output "firewall_application_rule_collection" {
  description = "Firewall Application Rule Collection"
  value       = module.naming.firewall_application_rule_collection
}

output "firewall_ip_configuration" {
  description = "Firewall Ip Configuration"
  value       = module.naming.firewall_ip_configuration
}

output "firewall_nat_rule_collection" {
  description = "Firewall Nat Rule Collection"
  value       = module.naming.firewall_nat_rule_collection
}

output "firewall_network_rule_collection" {
  description = "Firewall Network Rule Collection"
  value       = module.naming.firewall_network_rule_collection
}

output "firewall_policy" {
  description = "Firewall Policy"
  value       = module.naming.firewall_policy
}

output "firewall_policy_rule_collection_group" {
  description = "Firewall Policy Rule Collection Group"
  value       = module.naming.firewall_policy_rule_collection_group
}

output "frontdoor" {
  description = "Frontdoor"
  value       = module.naming.frontdoor
}

output "frontdoor_firewall_policy" {
  description = "Frontdoor Firewall Policy"
  value       = module.naming.frontdoor_firewall_policy
}

output "function_app" {
  description = "Function App"
  value       = module.naming.function_app
}

output "hdinsight_hadoop_cluster" {
  description = "Hdinsight Hadoop Cluster"
  value       = module.naming.hdinsight_hadoop_cluster
}

output "hdinsight_hbase_cluster" {
  description = "Hdinsight Hbase Cluster"
  value       = module.naming.hdinsight_hbase_cluster
}

output "hdinsight_interactive_query_cluster" {
  description = "Hdinsight Interactive Query Cluster"
  value       = module.naming.hdinsight_interactive_query_cluster
}

output "hdinsight_kafka_cluster" {
  description = "Hdinsight Kafka Cluster"
  value       = module.naming.hdinsight_kafka_cluster
}

output "hdinsight_ml_services_cluster" {
  description = "Hdinsight Ml Services Cluster"
  value       = module.naming.hdinsight_ml_services_cluster
}

output "hdinsight_rserver_cluster" {
  description = "Hdinsight Rserver Cluster"
  value       = module.naming.hdinsight_rserver_cluster
}

output "hdinsight_spark_cluster" {
  description = "Hdinsight Spark Cluster"
  value       = module.naming.hdinsight_spark_cluster
}

output "hdinsight_storm_cluster" {
  description = "Hdinsight Storm Cluster"
  value       = module.naming.hdinsight_storm_cluster
}

output "image" {
  description = "Image"
  value       = module.naming.image
}

output "iotcentral_application" {
  description = "Iotcentral Application"
  value       = module.naming.iotcentral_application
}

output "iothub" {
  description = "Iothub"
  value       = module.naming.iothub
}

output "iothub_consumer_group" {
  description = "Iothub Consumer Group"
  value       = module.naming.iothub_consumer_group
}

output "iothub_dps" {
  description = "Iothub Dps"
  value       = module.naming.iothub_dps
}

output "iothub_dps_certificate" {
  description = "Iothub Dps Certificate"
  value       = module.naming.iothub_dps_certificate
}

output "key_vault" {
  description = "Key Vault"
  value       = module.naming.key_vault
}

output "key_vault_certificate" {
  description = "Key Vault Certificate"
  value       = module.naming.key_vault_certificate
}

output "key_vault_key" {
  description = "Key Vault Key"
  value       = module.naming.key_vault_key
}

output "key_vault_secret" {
  description = "Key Vault Secret"
  value       = module.naming.key_vault_secret
}

output "kubernetes_cluster" {
  description = "Kubernetes Cluster"
  value       = module.naming.kubernetes_cluster
}

output "kusto_cluster" {
  description = "Kusto Cluster"
  value       = module.naming.kusto_cluster
}

output "kusto_database" {
  description = "Kusto Database"
  value       = module.naming.kusto_database
}

output "kusto_eventhub_data_connection" {
  description = "Kusto Eventhub Data Connection"
  value       = module.naming.kusto_eventhub_data_connection
}

output "lb" {
  description = "Lb"
  value       = module.naming.lb
}

output "lb_nat_rule" {
  description = "Lb Nat Rule"
  value       = module.naming.lb_nat_rule
}

output "linux_virtual_machine" {
  description = "Linux Virtual Machine"
  value       = module.naming.linux_virtual_machine
}

output "linux_virtual_machine_scale_set" {
  description = "Linux Virtual Machine Scale Set"
  value       = module.naming.linux_virtual_machine_scale_set
}

output "load_test" {
  description = "Load Test"
  value       = module.naming.load_test
}

output "local_network_gateway" {
  description = "Local Network Gateway"
  value       = module.naming.local_network_gateway
}

output "log_analytics_workspace" {
  description = "Log Analytics Workspace"
  value       = module.naming.log_analytics_workspace
}

output "logic_app_workflow" {
  description = "Logic App Workflow"
  value       = module.naming.logic_app_workflow
}

output "machine_learning_workspace" {
  description = "Machine Learning Workspace"
  value       = module.naming.machine_learning_workspace
}

output "managed_disk" {
  description = "Managed Disk"
  value       = module.naming.managed_disk
}

output "maps_account" {
  description = "Maps Account"
  value       = module.naming.maps_account
}

output "mariadb_database" {
  description = "Mariadb Database"
  value       = module.naming.mariadb_database
}

output "mariadb_firewall_rule" {
  description = "Mariadb Firewall Rule"
  value       = module.naming.mariadb_firewall_rule
}

output "mariadb_server" {
  description = "Mariadb Server"
  value       = module.naming.mariadb_server
}

output "mariadb_virtual_network_rule" {
  description = "Mariadb Virtual Network Rule"
  value       = module.naming.mariadb_virtual_network_rule
}

output "monitor_action_group" {
  description = "Monitor Action Group"
  value       = module.naming.monitor_action_group
}

output "monitor_autoscale_setting" {
  description = "Monitor Autoscale Setting"
  value       = module.naming.monitor_autoscale_setting
}

output "monitor_diagnostic_setting" {
  description = "Monitor Diagnostic Setting"
  value       = module.naming.monitor_diagnostic_setting
}

output "monitor_scheduled_query_rules_alert" {
  description = "Monitor Scheduled Query Rules Alert"
  value       = module.naming.monitor_scheduled_query_rules_alert
}

output "mssql_database" {
  description = "Mssql Database"
  value       = module.naming.mssql_database
}

output "mssql_elasticpool" {
  description = "Mssql Elasticpool"
  value       = module.naming.mssql_elasticpool
}

output "mssql_managed_instance" {
  description = "Mssql Managed Instance"
  value       = module.naming.mssql_managed_instance
}

output "mssql_server" {
  description = "Mssql Server"
  value       = module.naming.mssql_server
}

output "mysql_database" {
  description = "Mysql Database"
  value       = module.naming.mysql_database
}

output "mysql_firewall_rule" {
  description = "Mysql Firewall Rule"
  value       = module.naming.mysql_firewall_rule
}

output "mysql_server" {
  description = "Mysql Server"
  value       = module.naming.mysql_server
}

output "mysql_virtual_network_rule" {
  description = "Mysql Virtual Network Rule"
  value       = module.naming.mysql_virtual_network_rule
}

output "nat_gateway" {
  description = "Nat Gateway"
  value       = module.naming.nat_gateway
}

output "network_ddos_protection_plan" {
  description = "Network Ddos Protection Plan"
  value       = module.naming.network_ddos_protection_plan
}

output "network_interface" {
  description = "Network Interface"
  value       = module.naming.network_interface
}

output "network_security_group" {
  description = "Network Security Group"
  value       = module.naming.network_security_group
}

output "network_security_group_rule" {
  description = "Network Security Group Rule"
  value       = module.naming.network_security_group_rule
}

output "network_security_rule" {
  description = "Network Security Rule"
  value       = module.naming.network_security_rule
}

output "network_watcher" {
  description = "Network Watcher"
  value       = module.naming.network_watcher
}

output "notification_hub" {
  description = "Notification Hub"
  value       = module.naming.notification_hub
}

output "notification_hub_authorization_rule" {
  description = "Notification Hub Authorization Rule"
  value       = module.naming.notification_hub_authorization_rule
}

output "notification_hub_namespace" {
  description = "Notification Hub Namespace"
  value       = module.naming.notification_hub_namespace
}

output "point_to_site_vpn_gateway" {
  description = "Point To Site Vpn Gateway"
  value       = module.naming.point_to_site_vpn_gateway
}

output "postgresql_database" {
  description = "Postgresql Database"
  value       = module.naming.postgresql_database
}

output "postgresql_firewall_rule" {
  description = "Postgresql Firewall Rule"
  value       = module.naming.postgresql_firewall_rule
}

output "postgresql_server" {
  description = "Postgresql Server"
  value       = module.naming.postgresql_server
}

output "postgresql_virtual_network_rule" {
  description = "Postgresql Virtual Network Rule"
  value       = module.naming.postgresql_virtual_network_rule
}

output "powerbi_embedded" {
  description = "Powerbi Embedded"
  value       = module.naming.powerbi_embedded
}

output "private_dns_a_record" {
  description = "Private Dns A Record"
  value       = module.naming.private_dns_a_record
}

output "private_dns_aaaa_record" {
  description = "Private Dns Aaaa Record"
  value       = module.naming.private_dns_aaaa_record
}

output "private_dns_cname_record" {
  description = "Private Dns Cname Record"
  value       = module.naming.private_dns_cname_record
}

output "private_dns_mx_record" {
  description = "Private Dns Mx Record"
  value       = module.naming.private_dns_mx_record
}

output "private_dns_ptr_record" {
  description = "Private Dns Ptr Record"
  value       = module.naming.private_dns_ptr_record
}

output "private_dns_srv_record" {
  description = "Private Dns Srv Record"
  value       = module.naming.private_dns_srv_record
}

output "private_dns_txt_record" {
  description = "Private Dns Txt Record"
  value       = module.naming.private_dns_txt_record
}

output "private_dns_zone" {
  description = "Private Dns Zone"
  value       = module.naming.private_dns_zone
}

output "private_dns_zone_group" {
  description = "Private Dns Zone Group"
  value       = module.naming.private_dns_zone_group
}

output "private_endpoint" {
  description = "Private Endpoint"
  value       = module.naming.private_endpoint
}

output "private_link_service" {
  description = "Private Link Service"
  value       = module.naming.private_link_service
}

output "private_service_connection" {
  description = "Private Service Connection"
  value       = module.naming.private_service_connection
}

output "proximity_placement_group" {
  description = "Proximity Placement Group"
  value       = module.naming.proximity_placement_group
}

output "public_ip" {
  description = "Public Ip"
  value       = module.naming.public_ip
}

output "public_ip_prefix" {
  description = "Public Ip Prefix"
  value       = module.naming.public_ip_prefix
}

output "recovery_services_vault" {
  description = "Recovery Services Vault"
  value       = module.naming.recovery_services_vault
}

output "redis_cache" {
  description = "Redis Cache"
  value       = module.naming.redis_cache
}

output "redis_firewall_rule" {
  description = "Redis Firewall Rule"
  value       = module.naming.redis_firewall_rule
}

output "relay_hybrid_connection" {
  description = "Relay Hybrid Connection"
  value       = module.naming.relay_hybrid_connection
}

output "relay_namespace" {
  description = "Relay Namespace"
  value       = module.naming.relay_namespace
}

output "resource_group" {
  description = "Resource Group"
  value       = module.naming.resource_group
}

output "role_assignment" {
  description = "Role Assignment"
  value       = module.naming.role_assignment
}

output "role_definition" {
  description = "Role Definition"
  value       = module.naming.role_definition
}

output "route" {
  description = "Route"
  value       = module.naming.route
}

output "route_table" {
  description = "Route Table"
  value       = module.naming.route_table
}

output "search_service" {
  description = "Search Service"
  value       = module.naming.search_service
}

output "service_fabric_cluster" {
  description = "Service Fabric Cluster"
  value       = module.naming.service_fabric_cluster
}

output "servicebus_namespace" {
  description = "Servicebus Namespace"
  value       = module.naming.servicebus_namespace
}

output "servicebus_namespace_authorization_rule" {
  description = "Servicebus Namespace Authorization Rule"
  value       = module.naming.servicebus_namespace_authorization_rule
}

output "servicebus_queue" {
  description = "Servicebus Queue"
  value       = module.naming.servicebus_queue
}

output "servicebus_queue_authorization_rule" {
  description = "Servicebus Queue Authorization Rule"
  value       = module.naming.servicebus_queue_authorization_rule
}

output "servicebus_subscription" {
  description = "Servicebus Subscription"
  value       = module.naming.servicebus_subscription
}

output "servicebus_subscription_rule" {
  description = "Servicebus Subscription Rule"
  value       = module.naming.servicebus_subscription_rule
}

output "servicebus_topic" {
  description = "Servicebus Topic"
  value       = module.naming.servicebus_topic
}

output "servicebus_topic_authorization_rule" {
  description = "Servicebus Topic Authorization Rule"
  value       = module.naming.servicebus_topic_authorization_rule
}

output "shared_image" {
  description = "Shared Image"
  value       = module.naming.shared_image
}

output "shared_image_gallery" {
  description = "Shared Image Gallery"
  value       = module.naming.shared_image_gallery
}

output "signalr_service" {
  description = "Signalr Service"
  value       = module.naming.signalr_service
}

output "snapshots" {
  description = "Snapshots"
  value       = module.naming.snapshots
}

output "sql_elasticpool" {
  description = "Sql Elasticpool"
  value       = module.naming.sql_elasticpool
}

output "sql_failover_group" {
  description = "Sql Failover Group"
  value       = module.naming.sql_failover_group
}

output "sql_firewall_rule" {
  description = "Sql Firewall Rule"
  value       = module.naming.sql_firewall_rule
}

output "sql_server" {
  description = "Sql Server"
  value       = module.naming.sql_server
}

output "static_web_app" {
  description = "Static Web App"
  value       = module.naming.static_web_app
}

output "storage_account" {
  description = "Storage Account"
  value       = module.naming.storage_account
}

output "storage_blob" {
  description = "Storage Blob"
  value       = module.naming.storage_blob
}

output "storage_container" {
  description = "Storage Container"
  value       = module.naming.storage_container
}

output "storage_data_lake_gen2_filesystem" {
  description = "Storage Data Lake Gen2 Filesystem"
  value       = module.naming.storage_data_lake_gen2_filesystem
}

output "storage_queue" {
  description = "Storage Queue"
  value       = module.naming.storage_queue
}

output "storage_share" {
  description = "Storage Share"
  value       = module.naming.storage_share
}

output "storage_share_directory" {
  description = "Storage Share Directory"
  value       = module.naming.storage_share_directory
}

output "storage_table" {
  description = "Storage Table"
  value       = module.naming.storage_table
}

output "stream_analytics_function_javascript_udf" {
  description = "Stream Analytics Function Javascript Udf"
  value       = module.naming.stream_analytics_function_javascript_udf
}

output "stream_analytics_job" {
  description = "Stream Analytics Job"
  value       = module.naming.stream_analytics_job
}

output "stream_analytics_output_blob" {
  description = "Stream Analytics Output Blob"
  value       = module.naming.stream_analytics_output_blob
}

output "stream_analytics_output_eventhub" {
  description = "Stream Analytics Output Eventhub"
  value       = module.naming.stream_analytics_output_eventhub
}

output "stream_analytics_output_mssql" {
  description = "Stream Analytics Output Mssql"
  value       = module.naming.stream_analytics_output_mssql
}

output "stream_analytics_output_servicebus_queue" {
  description = "Stream Analytics Output Servicebus Queue"
  value       = module.naming.stream_analytics_output_servicebus_queue
}

output "stream_analytics_output_servicebus_topic" {
  description = "Stream Analytics Output Servicebus Topic"
  value       = module.naming.stream_analytics_output_servicebus_topic
}

output "stream_analytics_reference_input_blob" {
  description = "Stream Analytics Reference Input Blob"
  value       = module.naming.stream_analytics_reference_input_blob
}

output "stream_analytics_stream_input_blob" {
  description = "Stream Analytics Stream Input Blob"
  value       = module.naming.stream_analytics_stream_input_blob
}

output "stream_analytics_stream_input_eventhub" {
  description = "Stream Analytics Stream Input Eventhub"
  value       = module.naming.stream_analytics_stream_input_eventhub
}

output "stream_analytics_stream_input_iothub" {
  description = "Stream Analytics Stream Input Iothub"
  value       = module.naming.stream_analytics_stream_input_iothub
}

output "subnet" {
  description = "Subnet"
  value       = module.naming.subnet
}

output "template_deployment" {
  description = "Template Deployment"
  value       = module.naming.template_deployment
}

output "traffic_manager_profile" {
  description = "Traffic Manager Profile"
  value       = module.naming.traffic_manager_profile
}

output "unique-seed" {
  description = ""
  value       = module.naming.unique-seed
}

output "user_assigned_identity" {
  description = "User Assigned Identity"
  value       = module.naming.user_assigned_identity
}

output "validation" {
  description = ""
  value       = module.naming.validation
}

output "virtual_machine" {
  description = "Virtual Machine"
  value       = module.naming.virtual_machine
}

output "virtual_machine_extension" {
  description = "Virtual Machine Extension"
  value       = module.naming.virtual_machine_extension
}

output "virtual_machine_scale_set" {
  description = "Virtual Machine Scale Set"
  value       = module.naming.virtual_machine_scale_set
}

output "virtual_machine_scale_set_extension" {
  description = "Virtual Machine Scale Set Extension"
  value       = module.naming.virtual_machine_scale_set_extension
}

output "virtual_network" {
  description = "Virtual Network"
  value       = module.naming.virtual_network
}

output "virtual_network_gateway" {
  description = "Virtual Network Gateway"
  value       = module.naming.virtual_network_gateway
}

output "virtual_network_gateway_connection" {
  description = "Virtual Network Gateway Connection"
  value       = module.naming.virtual_network_gateway_connection
}

output "virtual_network_peering" {
  description = "Virtual Network Peering"
  value       = module.naming.virtual_network_peering
}

output "virtual_wan" {
  description = "Virtual Wan"
  value       = module.naming.virtual_wan
}

output "windows_virtual_machine" {
  description = "Windows Virtual Machine"
  value       = module.naming.windows_virtual_machine
}

output "windows_virtual_machine_scale_set" {
  description = "Windows Virtual Machine Scale Set"
  value       = module.naming.windows_virtual_machine_scale_set
}


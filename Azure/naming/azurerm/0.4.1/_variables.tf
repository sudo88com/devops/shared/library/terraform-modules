
variable "prefix" {
  type        = list(string)
  description = "It is not recommended that you use prefix by azure you should be using a suffix for your resources."
  default     = []
}

variable "suffix" {
  type        = list(string)
  description = "It is recommended that you specify a suffix for consistency. please use only lowercase characters when possible"
  default     = []
}

variable "unique-include-numbers" {
  type        = bool
  description = "If you want to include numbers in the unique generation"
  default     = true
}

variable "unique-length" {
  type        = number
  description = "Max length of the uniqueness suffix to be added"
  default     = 4
}

variable "unique-seed" {
  type        = string
  description = "Custom value for the random characters to be used"
  default     = ""
}


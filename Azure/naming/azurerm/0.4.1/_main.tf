
module "naming" {
  source = "terraform-aws-modules/naming/aws"
  version = "0.4.1"
  prefix = var.prefix
  suffix = var.suffix
  unique-include-numbers = var.unique-include-numbers
  unique-length = var.unique-length
  unique-seed = var.unique-seed
}


customer_managed_key = null

diagnostic_settings = {}

enable_telemetry = true

location = 

lock = null

log_analytics_workspace_allow_resource_only_permissions = null

log_analytics_workspace_cmk_for_query_forced = null

log_analytics_workspace_daily_quota_gb = null

log_analytics_workspace_identity = null

log_analytics_workspace_internet_ingestion_enabled = null

log_analytics_workspace_internet_query_enabled = null

log_analytics_workspace_local_authentication_disabled = null

log_analytics_workspace_reservation_capacity_in_gb_per_day = null

log_analytics_workspace_retention_in_days = null

log_analytics_workspace_sku = null

log_analytics_workspace_timeouts = null

name = 

private_endpoints = {}

private_endpoints_manage_dns_zone_group = true

resource_group_name = 

role_assignments = {}

tags = null



variable "a_records" {
  type        = map(object({
    name                = string
    resource_group_name = string
    zone_name           = string
    ttl                 = number
    records             = list(string)
    tags                = optional(map(string), null)
  }))
  description = "A map of objects where each object contains information to create a A record."
  default     = {}
}

variable "aaaa_records" {
  type        = map(object({
    name                = string
    resource_group_name = string
    zone_name           = string
    ttl                 = number
    records             = list(string)
    tags                = optional(map(string), null)
  }))
  description = "A map of objects where each object contains information to create a AAAA record."
  default     = {}
}

variable "cname_records" {
  type        = map(object({
    name                = string
    resource_group_name = string
    zone_name           = string
    ttl                 = number
    record              = string
    tags                = optional(map(string), null)
  }))
  description = "A map of objects where each object contains information to create a CNAME record."
  default     = {}
}

variable "dns_zone_tags" {
  type        = map(string)
  description = "The tags to associate with your private dns zone."
  default     = {}
}

variable "domain_name" {
  type        = string
  description = "The name of the private dns zone."
  default     = ""
}

variable "enable_telemetry" {
  type        = bool
  description = "This variable controls whether or not telemetry is enabled for the module.\nFor more information see https://aka.ms/avm/telemetryinfo.\nIf it is set to false, then no telemetry will be collected.\n"
  default     = true
}

variable "mx_records" {
  type        = map(object({
    name                = optional(string, "@")
    resource_group_name = string
    zone_name           = string
    ttl                 = number
    records = map(object({
      preference = number
      exchange   = string
    }))
    tags = optional(map(string), null)
  }))
  description = "A map of objects where each object contains information to create a MX record."
  default     = {}
}

variable "ptr_records" {
  type        = map(object({
    name                = string
    resource_group_name = string
    zone_name           = string
    ttl                 = number
    records             = list(string)
    tags                = optional(map(string), null)
  }))
  description = "A map of objects where each object contains information to create a PTR record."
  default     = {}
}

variable "resource_group_name" {
  type        = string
  description = "The resource group where the resources will be deployed."
  default     = ""
}

variable "soa_record" {
  type        = object({
    email        = string
    expire_time  = optional(number, 2419200)
    minimum_ttl  = optional(number, 10)
    refresh_time = optional(number, 3600)
    retry_time   = optional(number, 300)
    ttl          = optional(number, 3600)
    tags         = optional(map(string), null)
  })
  description = "optional soa_record variable, if included only email is required, rest are optional. Email must use username.corp.com and not username@corp.com"
  default     = null
}

variable "srv_records" {
  type        = map(object({
    name                = string
    resource_group_name = string
    zone_name           = string
    ttl                 = number
    records = map(object({
      priority = number
      weight   = number
      port     = number
      target   = string
    }))
    tags = optional(map(string), null)
  }))
  description = "A map of objects where each object contains information to create a SRV record."
  default     = {}
}

variable "telem_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the telemetry resources."
  default     = {}
}

variable "txt_records" {
  type        = map(object({
    name                = string
    resource_group_name = string
    zone_name           = string
    ttl                 = number
    records = map(object({
      value = string
    }))
    tags = optional(map(string), null)
  }))
  description = "A map of objects where each object contains information to create a TXT record."
  default     = {}
}

variable "virtual_network_links" {
  type        = map(object({
    vnetlinkname     = string
    vnetid           = string
    autoregistration = optional(bool, false)
    tags             = optional(map(string), null)
  }))
  description = "A map of objects where each object contains information to create a virtual network link."
  default     = {}
}


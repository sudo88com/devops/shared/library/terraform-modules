
output "application_insights" {
  description = "The application insights resource."
  value       = module.avm-res-web-site.application_insights
}

output "name" {
  description = "The name of the resource."
  value       = module.avm-res-web-site.name
}

output "resource" {
  description = "This is the full output for the resource."
  value       = module.avm-res-web-site.resource
}

output "resource_id" {
  description = "This is the full output for the resource."
  value       = module.avm-res-web-site.resource_id
}

output "resource_private_endpoints" {
  description = "A map of private endpoints. The map key is the supplied input to var.private_endpoints. The map value is the entire azurerm_private_endpoint resource."
  value       = module.avm-res-web-site.resource_private_endpoints
}

output "resource_uri" {
  description = "The default hostname of the resource."
  value       = module.avm-res-web-site.resource_uri
}

output "service_plan" {
  description = "The service plan resource."
  value       = module.avm-res-web-site.service_plan
}

output "storage_account" {
  description = "The storage account resource."
  value       = module.avm-res-web-site.storage_account
}


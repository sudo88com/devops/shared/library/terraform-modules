
all_child_resources_inherit_lock = true

all_child_resources_inherit_tags = true

app_settings = {}

application_insights = {}

auth_settings = {}

auth_settings_v2 = {}

auto_heal_setting = {}

backup = {}

builtin_logging_enabled = true

client_affinity_enabled = false

client_certificate_enabled = false

client_certificate_exclusion_paths = null

client_certificate_mode = "Optional"

connection_strings = {}

content_share_force_disabled = false

create_service_plan = false

custom_domains = {}

daily_memory_time_quota = 0

diagnostic_settings = {}

enable_application_insights = true

enable_telemetry = true

enabled = true

ftp_publish_basic_authentication_enabled = true

function_app_create_storage_account = false

function_app_storage_account = {}

function_app_storage_account_access_key = null

function_app_storage_account_name = null

function_app_storage_uses_managed_identity = false

functions_extension_version = "~4"

https_only = false

key_vault_reference_identity_id = null

kind = 

location = 

lock = null

logs = {}

managed_identities = {}

name = 

new_service_plan = {}

os_type = 

private_endpoints = {}

private_endpoints_inherit_lock = true

private_endpoints_manage_dns_zone_group = true

public_network_access_enabled = true

resource_group_name = 

role_assignments = {}

service_plan_resource_id = null

site_config = {}

sticky_settings = {}

storage_key_vault_secret_id = null

storage_shares_to_mount = {}

tags = null

timeouts = null

virtual_network_subnet_id = null

webdeploy_publish_basic_authentication_enabled = true

zip_deploy_file = null


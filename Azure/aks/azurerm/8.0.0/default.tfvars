
aci_connector_linux_enabled = false

aci_connector_linux_subnet_name = null

admin_username = null

agents_availability_zones = null

agents_count = 2

agents_labels = {}

agents_max_count = null

agents_max_pods = null

agents_min_count = null

agents_pool_kubelet_configs = []

agents_pool_linux_os_configs = []

agents_pool_max_surge = null

agents_pool_name = "nodepool"

agents_proximity_placement_group_id = null

agents_size = "Standard_D2s_v3"

agents_tags = {}

agents_taints = null

agents_type = "VirtualMachineScaleSets"

api_server_authorized_ip_ranges = null

api_server_subnet_id = null

attached_acr_id_map = {}

auto_scaler_profile_balance_similar_node_groups = false

auto_scaler_profile_empty_bulk_delete_max = 10

auto_scaler_profile_enabled = false

auto_scaler_profile_expander = "random"

auto_scaler_profile_max_graceful_termination_sec = "600"

auto_scaler_profile_max_node_provisioning_time = "15m"

auto_scaler_profile_max_unready_nodes = 3

auto_scaler_profile_max_unready_percentage = 45

auto_scaler_profile_new_pod_scale_up_delay = "10s"

auto_scaler_profile_scale_down_delay_after_add = "10m"

auto_scaler_profile_scale_down_delay_after_delete = null

auto_scaler_profile_scale_down_delay_after_failure = "3m"

auto_scaler_profile_scale_down_unneeded = "10m"

auto_scaler_profile_scale_down_unready = "20m"

auto_scaler_profile_scale_down_utilization_threshold = "0.5"

auto_scaler_profile_scan_interval = "10s"

auto_scaler_profile_skip_nodes_with_local_storage = true

auto_scaler_profile_skip_nodes_with_system_pods = true

automatic_channel_upgrade = null

azure_policy_enabled = false

brown_field_application_gateway_for_ingress = null

client_id = ""

client_secret = ""

cluster_log_analytics_workspace_name = null

cluster_name = null

cluster_name_random_suffix = false

confidential_computing = null

create_role_assignment_network_contributor = false

create_role_assignments_for_application_gateway = true

default_node_pool_fips_enabled = null

disk_encryption_set_id = null

ebpf_data_plane = null

enable_auto_scaling = false

enable_host_encryption = false

enable_node_public_ip = false

green_field_application_gateway_for_ingress = null

http_proxy_config = null

identity_ids = null

identity_type = "SystemAssigned"

image_cleaner_enabled = false

image_cleaner_interval_hours = 48

key_vault_secrets_provider_enabled = false

kms_enabled = false

kms_key_vault_key_id = null

kms_key_vault_network_access = "Public"

kubelet_identity = null

kubernetes_version = null

load_balancer_profile_enabled = false

load_balancer_profile_idle_timeout_in_minutes = 30

load_balancer_profile_managed_outbound_ip_count = null

load_balancer_profile_managed_outbound_ipv6_count = null

load_balancer_profile_outbound_ip_address_ids = null

load_balancer_profile_outbound_ip_prefix_ids = null

load_balancer_profile_outbound_ports_allocated = 0

load_balancer_sku = "standard"

local_account_disabled = null

location = null

log_analytics_solution = null

log_analytics_workspace = null

log_analytics_workspace_enabled = true

log_analytics_workspace_resource_group_name = null

log_analytics_workspace_sku = "PerGB2018"

log_retention_in_days = 30

maintenance_window = null

maintenance_window_auto_upgrade = null

maintenance_window_node_os = null

microsoft_defender_enabled = false

monitor_metrics = null

msi_auth_for_monitoring_enabled = null

net_profile_dns_service_ip = null

net_profile_outbound_type = "loadBalancer"

net_profile_pod_cidr = null

net_profile_service_cidr = null

network_contributor_role_assigned_subnet_ids = {}

network_plugin = "kubenet"

network_plugin_mode = null

network_policy = null

node_os_channel_upgrade = null

node_pools = {}

node_resource_group = null

oidc_issuer_enabled = false

only_critical_addons_enabled = null

open_service_mesh_enabled = null

orchestrator_version = null

os_disk_size_gb = 50

os_disk_type = "Managed"

os_sku = null

pod_subnet_id = null

prefix = ""

private_cluster_enabled = false

private_cluster_public_fqdn_enabled = false

private_dns_zone_id = null

public_ssh_key = ""

rbac_aad = true

rbac_aad_admin_group_object_ids = null

rbac_aad_azure_rbac_enabled = null

rbac_aad_client_app_id = null

rbac_aad_managed = false

rbac_aad_server_app_id = null

rbac_aad_server_app_secret = null

rbac_aad_tenant_id = null

resource_group_name = 

role_based_access_control_enabled = false

run_command_enabled = true

scale_down_mode = "Delete"

secret_rotation_enabled = false

secret_rotation_interval = "2m"

service_mesh_profile = null

sku_tier = "Free"

snapshot_id = null

storage_profile_blob_driver_enabled = false

storage_profile_disk_driver_enabled = true

storage_profile_disk_driver_version = "v1"

storage_profile_enabled = false

storage_profile_file_driver_enabled = true

storage_profile_snapshot_controller_enabled = true

support_plan = "KubernetesOfficial"

tags = {}

temporary_name_for_rotation = null

tracing_tags_enabled = false

tracing_tags_prefix = "avm_"

ultra_ssd_enabled = false

vnet_subnet_id = null

web_app_routing = null

workload_autoscaler_profile = null

workload_identity_enabled = false


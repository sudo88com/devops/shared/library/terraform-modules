
output "id" {
  description = "This is the full output for the resource."
  value       = module.avm-res-network-networkmanager.id
}

output "name" {
  description = "The name of the Network Manager."
  value       = module.avm-res-network-networkmanager.name
}


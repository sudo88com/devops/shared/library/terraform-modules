
output "network_security_group_id" {
  description = "The id of newly created network security group"
  value       = module.network-security-group.network_security_group_id
}

output "network_security_group_name" {
  description = "The name of newly created network security group"
  value       = module.network-security-group.network_security_group_name
}


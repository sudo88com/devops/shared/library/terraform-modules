
module "network-security-group" {
  source = "terraform-aws-modules/network-security-group/aws"
  version = "4.1.0"
  custom_rules = var.custom_rules
  destination_address_prefix = var.destination_address_prefix
  destination_address_prefixes = var.destination_address_prefixes
  location = var.location
  predefined_rules = var.predefined_rules
  resource_group_name = var.resource_group_name
  rules = var.rules
  security_group_name = var.security_group_name
  source_address_prefix = var.source_address_prefix
  source_address_prefixes = var.source_address_prefixes
  tags = var.tags
  use_for_each = var.use_for_each
}


custom_rules = []

destination_address_prefix = [
  "*"
]

destination_address_prefixes = null

location = ""

predefined_rules = []

resource_group_name = 

rules = {
  "ActiveDirectory-AllowADDSWebServices": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "9389",
    "AllowADDSWebServices"
  ],
  "ActiveDirectory-AllowADGCReplication": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "3268",
    "AllowADGCReplication"
  ],
  "ActiveDirectory-AllowADGCReplicationSSL": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "3269",
    "AllowADGCReplicationSSL"
  ],
  "ActiveDirectory-AllowADReplication": [
    "Inbound",
    "Allow",
    "*",
    "*",
    "389",
    "AllowADReplication"
  ],
  "ActiveDirectory-AllowADReplicationSSL": [
    "Inbound",
    "Allow",
    "*",
    "*",
    "636",
    "AllowADReplicationSSL"
  ],
  "ActiveDirectory-AllowADReplicationTrust": [
    "Inbound",
    "Allow",
    "*",
    "*",
    "445",
    "AllowADReplicationTrust"
  ],
  "ActiveDirectory-AllowDFSGroupPolicy": [
    "Inbound",
    "Allow",
    "Udp",
    "*",
    "138",
    "AllowDFSGroupPolicy"
  ],
  "ActiveDirectory-AllowDNS": [
    "Inbound",
    "Allow",
    "*",
    "*",
    "53",
    "AllowDNS"
  ],
  "ActiveDirectory-AllowFileReplication": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "5722",
    "AllowFileReplication"
  ],
  "ActiveDirectory-AllowKerberosAuthentication": [
    "Inbound",
    "Allow",
    "*",
    "*",
    "88",
    "AllowKerberosAuthentication"
  ],
  "ActiveDirectory-AllowNETBIOSAuthentication": [
    "Inbound",
    "Allow",
    "Udp",
    "*",
    "137",
    "AllowNETBIOSAuthentication"
  ],
  "ActiveDirectory-AllowNETBIOSReplication": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "139",
    "AllowNETBIOSReplication"
  ],
  "ActiveDirectory-AllowPasswordChangeKerberes": [
    "Inbound",
    "Allow",
    "*",
    "*",
    "464",
    "AllowPasswordChangeKerberes"
  ],
  "ActiveDirectory-AllowRPCReplication": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "135",
    "AllowRPCReplication"
  ],
  "ActiveDirectory-AllowSMTPReplication": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "25",
    "AllowSMTPReplication"
  ],
  "ActiveDirectory-AllowWindowsTime": [
    "Inbound",
    "Allow",
    "Udp",
    "*",
    "123",
    "AllowWindowsTime"
  ],
  "Cassandra": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "9042",
    "Cassandra"
  ],
  "Cassandra-JMX": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "7199",
    "Cassandra-JMX"
  ],
  "Cassandra-Thrift": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "9160",
    "Cassandra-Thrift"
  ],
  "CouchDB": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "5984",
    "CouchDB"
  ],
  "CouchDB-HTTPS": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "6984",
    "CouchDB-HTTPS"
  ],
  "DNS-TCP": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "53",
    "DNS-TCP"
  ],
  "DNS-UDP": [
    "Inbound",
    "Allow",
    "Udp",
    "*",
    "53",
    "DNS-UDP"
  ],
  "DynamicPorts": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "49152-65535",
    "DynamicPorts"
  ],
  "ElasticSearch": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "9200-9300",
    "ElasticSearch"
  ],
  "FTP": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "21",
    "FTP"
  ],
  "HTTP": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "80",
    "HTTP"
  ],
  "HTTPS": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "443",
    "HTTPS"
  ],
  "IMAP": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "143",
    "IMAP"
  ],
  "IMAPS": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "993",
    "IMAPS"
  ],
  "Kestrel": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "22133",
    "Kestrel"
  ],
  "LDAP": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "389",
    "LDAP"
  ],
  "MSSQL": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "1433",
    "MSSQL"
  ],
  "Memcached": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "11211",
    "Memcached"
  ],
  "MongoDB": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "27017",
    "MongoDB"
  ],
  "MySQL": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "3306",
    "MySQL"
  ],
  "Neo4J": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "7474",
    "Neo4J"
  ],
  "POP3": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "110",
    "POP3"
  ],
  "POP3S": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "995",
    "POP3S"
  ],
  "PostgreSQL": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "5432",
    "PostgreSQL"
  ],
  "RDP": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "3389",
    "RDP"
  ],
  "RabbitMQ": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "5672",
    "RabbitMQ"
  ],
  "Redis": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "6379",
    "Redis"
  ],
  "Riak": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "8093",
    "Riak"
  ],
  "Riak-JMX": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "8985",
    "Riak-JMX"
  ],
  "SMTP": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "25",
    "SMTP"
  ],
  "SMTPS": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "465",
    "SMTPS"
  ],
  "SSH": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "22",
    "SSH"
  ],
  "WinRM": [
    "Inbound",
    "Allow",
    "Tcp",
    "*",
    "5986",
    "WinRM"
  ]
}

security_group_name = "nsg"

source_address_prefix = [
  "*"
]

source_address_prefixes = null

tags = {}

use_for_each = false


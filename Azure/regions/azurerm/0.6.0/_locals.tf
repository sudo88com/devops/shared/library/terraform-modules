
locals {
  recommended_regions_only = var.recommended_regions_only
  use_cached_data = var.use_cached_data
}


module "regions" {
  source = "terraform-aws-modules/regions/aws"
  version = "0.6.0"
  recommended_regions_only = var.recommended_regions_only
  use_cached_data = var.use_cached_data
}

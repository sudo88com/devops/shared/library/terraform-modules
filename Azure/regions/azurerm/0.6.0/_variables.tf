
variable "recommended_regions_only" {
  type        = bool
  description = "If true, the module will only return regions that are have the category set to `Recommended` by the locations API.\n"
  default     = true
}

variable "use_cached_data" {
  type        = bool
  description = "If true, the module will use cached data from the data directory. If false, the module will use live data from the Azure API.\n\nThe default is true to avoid unnecessary API calls and provide a guaranteed consistent output.\nSet to false to ensure the latest data is used.\n\nUsing data from the Azure APIs means that if the API response changes, then the module output will change.\nThis may affect deployed resources that rely on this data.\n"
  default     = true
}



variable "container_group_name" {
  type        = 
  description = "The name of the container group"
  default     = "myContGroup"
}

variable "container_name" {
  type        = 
  description = "The name of the container"
  default     = "mycont01"
}

variable "cpu_core_number" {
  type        = 
  description = "The required number of CPU cores of the containers"
  default     = "0.5"
}

variable "dns_name_label" {
  type        = 
  description = "The DNS label/name for the container groups IP"
  default     = ""
}

variable "image_name" {
  type        = 
  description = "The container image name"
  default     = ""
}

variable "location" {
  type        = 
  description = "Azure location"
  default     = "westus"
}

variable "memory_size" {
  type        = 
  description = "The required memory of the containers in GB"
  default     = "1.5"
}

variable "os_type" {
  type        = 
  description = "The OS for the container group. Allowed values are Linux and Windows"
  default     = ""
}

variable "port_number" {
  type        = 
  description = "A public port for the container"
  default     = "80"
}

variable "resource_group_name" {
  type        = 
  description = "The name of the resource group"
  default     = "MyContGroup-RG01"
}



output "containergroup_fqdn" {
  description = "The FQDN of the created container group"
  value       = module.aci.containergroup_fqdn
}

output "containergroup_id" {
  description = "The ID of the created container group"
  value       = module.aci.containergroup_id
}

output "containergroup_ip_address" {
  description = "The IP address of the created container group"
  value       = module.aci.containergroup_ip_address
}


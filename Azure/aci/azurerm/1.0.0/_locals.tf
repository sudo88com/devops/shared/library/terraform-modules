
locals {
  container_group_name = var.container_group_name
  container_name = var.container_name
  cpu_core_number = var.cpu_core_number
  dns_name_label = var.dns_name_label
  image_name = var.image_name
  location = var.location
  memory_size = var.memory_size
  os_type = var.os_type
  port_number = var.port_number
  resource_group_name = var.resource_group_name
}

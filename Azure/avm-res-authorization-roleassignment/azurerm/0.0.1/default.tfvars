
app_registrations_by_client_id = {}

app_registrations_by_display_name = {}

app_registrations_by_object_id = {}

app_registrations_by_principal_id = {}

enable_telemetry = true

entra_id_role_definitions = {}

groups_by_display_name = {}

groups_by_mail_nickname = {}

groups_by_object_id = {}

role_assignments_for_entra_id = {}

role_assignments_for_management_groups = {}

role_assignments_for_resource_groups = {}

role_assignments_for_resources = {}

role_assignments_for_scopes = {}

role_assignments_for_subscriptions = {}

role_definitions = {}

system_assigned_managed_identities_by_client_id = {}

system_assigned_managed_identities_by_display_name = {}

system_assigned_managed_identities_by_principal_id = {}

telemetry_resource_group_name = ""

user_assigned_managed_identities_by_client_id = {}

user_assigned_managed_identities_by_display_name = {}

user_assigned_managed_identities_by_principal_id = {}

user_assigned_managed_identities_by_resource_group_and_name = {}

users_by_employee_id = {}

users_by_mail = {}

users_by_mail_nickname = {}

users_by_object_id = {}

users_by_user_principal_name = {}


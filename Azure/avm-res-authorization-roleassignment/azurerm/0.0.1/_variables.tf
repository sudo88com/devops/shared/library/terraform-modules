
variable "app_registrations_by_client_id" {
  type        = map(string)
  description = "(Optional) A map of Entra ID application registrations to reference in role assignments.\nThe key is something unique to you. The value is the client ID (application ID) of the application registration.\n\nExample Input:\n\n```hcl\napp_registrations_by_client_id = {\n  my-app-1 = \"00000000-0000-0000-0000-000000000001\"\n  my-app-2 = \"00000000-0000-0000-0000-000000000002\"\n}\n```\n"
  default     = {}
}

variable "app_registrations_by_display_name" {
  type        = map(string)
  description = "(Optional) A map of Entra ID application registrations to reference in role assignments.\nThe key is something unique to you. The value is the display name of the application registration.\n\nExample Input:\n\n```hcl\napp_registrations_by_display_name = {\n  my-app-1 = \"My App 1\"\n  my-app-2 = \"My App 2\"\n}\n```\n"
  default     = {}
}

variable "app_registrations_by_object_id" {
  type        = map(string)
  description = "(Optional) A map of Entra ID application registrations to reference in role assignments.\nThe key is something unique to you. The value is the object ID of the application registration.\n\nExample Input:\n\n```hcl\napp_registrations_by_object_id = {\n  my-app-1 = \"00000000-0000-0000-0000-000000000001\"\n  my-app-2 = \"00000000-0000-0000-0000-000000000002\"\n}\n```\n"
  default     = {}
}

variable "app_registrations_by_principal_id" {
  type        = map(string)
  description = "(Optional) A map of Entra ID application registrations to reference in role assignments.\nThe key is something unique to you. The value is the principal ID of the service principal backing the application registration.\n\nExample Input:\n\n```hcl\napp_registrations_by_principal_id = {\n  my-app-1 = \"00000000-0000-0000-0000-000000000001\"\n  my-app-2 = \"00000000-0000-0000-0000-000000000002\"\n}\n```\n"
  default     = {}
}

variable "enable_telemetry" {
  type        = bool
  description = "This variable controls whether or not telemetry is enabled for the module.\nFor more information see https://aka.ms/avm/telemetryinfo.\nIf it is set to false, then no telemetry will be collected.\n"
  default     = true
}

variable "entra_id_role_definitions" {
  type        = map(string)
  description = "(Optional) A map of Entra ID role definitions to reference in role assignments.\nThe key is something unique to you. The value is a built in or custom role definition name.\n\nExample Input:\n\n```hcl\nentra_id_role_definitions = {\n  directory-writer     = \"Directory Writer\"\n  global-administrator = \"Global Administrator\"\n}\n```\n"
  default     = {}
}

variable "groups_by_display_name" {
  type        = map(string)
  description = "(Optional) A map of Entra ID groups to reference in role assignments.\nThe key is something unique to you. The value is the display name of the group.\n\nExample Input:\n\n```hcl\ngroups_by_display_name = {\n  my-group-1 = \"My Group 1\"\n  my-group-2 = \"My Group 2\"\n}\n```\n"
  default     = {}
}

variable "groups_by_mail_nickname" {
  type        = map(string)
  description = "(Optional) A map of Entra ID groups to reference in role assignments.\nThe key is something unique to you. The value is the mail nickname of the group.\n\nExample Input:\n\n```hcl\ngroups_by_mail_nickname = {\n  my-group-1 = \"my-group-1-nickname\"\n  my-group-2 = \"my-group-2-nickname\"\n}\n```\n"
  default     = {}
}

variable "groups_by_object_id" {
  type        = map(string)
  description = "(Optional) A map of Entra ID groups to reference in role assignments.\nThe key is something unique to you. The value is the object ID of the group.\n\nExample Input:\n\n```hcl\ngroups_by_object_id = {\n  my-group-1 = \"00000000-0000-0000-0000-000000000001\"\n  my-group-2 = \"00000000-0000-0000-0000-000000000002\"\n}\n```\n"
  default     = {}
}

variable "role_assignments_for_entra_id" {
  type        = map(object({
    role_assignments = map(object({
      role_definition                    = string
      users                              = optional(set(string), [])
      groups                             = optional(set(string), [])
      app_registrations                  = optional(set(string), [])
      system_assigned_managed_identities = optional(set(string), [])
      user_assigned_managed_identities   = optional(set(string), [])
      any_principals                     = optional(set(string), [])
    }))
  }))
  description = "(Optional) Role assignments to be applied to Entra ID.\nThis variable allows the assignment of Entra ID directory roles outside of the scope of Azure Resource Manager.\nThis variable requires the `entra_id_role_definitions` variable to be populated.\n\n- role_assignments: (Required) The role assignments to be applied to the scope.\n  - role_definition: (Required) The key of the role definition as defined in the `entra_id_role_definitions` variable.\n  - users: (Optional) The keys of the users as defined in one of the `users_by_...` variables.\n  - groups: (Optional) The keys of the groups as defined in one of the `groups_by_...` variables.\n  - app_registrations: (Optional) The keys of the app registrations as defined in one of the `app_registrations_by_...` variables.\n  - system_assigned_managed_identities: (Optional) The keys of the system assigned managed identities as defined in one of the `system_assigned_managed_identities_by_...` variables.\n  - user_assigned_managed_identities: (Optional) The keys of the user assigned managed identities as defined in one of the `user_assigned_managed_identities_by_...` variables.\n  - any_principals: (Optional) The keys of the principals as defined in any of the `[principal_type]_by_...` variables. This is a convenience method that can be used in combination with or instrad of the specific principal type options.\n\nExample Input:\n\n```hcl\nrole_assignments_for_entra_id = {\n  role_assignments    = {\n    role_definition = \"directory-writer\"\n    users = [\n      \"my-user-1\",\n      \"my-user-2\"\n    ]\n    groups = [\n      \"my-group-1\",\n      \"my-group-2\"\n    ]\n    app_registrations = [\n      \"my-app-1\",\n      \"my-app-2\"\n    ]\n    system_assigned_managed_identities = [\n      \"my-vm-1\",\n      \"my-vm-2\"\n    ]\n    user_assigned_managed_identities = [\n      \"my-user-assigned-managed-identity-1\",\n      \"my-user-assigned-managed-identity-2\"\n    ]\n  }\n}\n```\n"
  default     = {}
}

variable "role_assignments_for_management_groups" {
  type        = map(object({
    management_group_id           = optional(string, null)
    management_group_display_name = optional(string, null)
    role_assignments = map(object({
      role_definition                    = string
      users                              = optional(set(string), [])
      groups                             = optional(set(string), [])
      app_registrations                  = optional(set(string), [])
      system_assigned_managed_identities = optional(set(string), [])
      user_assigned_managed_identities   = optional(set(string), [])
      any_principals                     = optional(set(string), [])
    }))
  }))
  description = "(Optional) Role assignments to be applied to management groups.\nThis is a convenience variable that avoids the need to find the resource id of the management group.\n\n- management_group_id: (Optional) The id of the management group (one of `management_group_id` or `management_group_display_name` must be supplied).\n- management_group_display_name: (Optional) The display name of the management group.\n- role_assignments: (Required) The role assignments to be applied to the scope.\n  - role_definition: (Required) The key of the role definition as defined in the `role_definitions` variable.\n  - users: (Optional) The keys of the users as defined in one of the `users_by_...` variables.\n  - groups: (Optional) The keys of the groups as defined in one of the `groups_by_...` variables.\n  - app_registrations: (Optional) The keys of the app registrations as defined in one of the `app_registrations_by_...` variables.\n  - system_assigned_managed_identities: (Optional) The keys of the system assigned managed identities as defined in one of the `system_assigned_managed_identities_by_...` variables.\n  - user_assigned_managed_identities: (Optional) The keys of the user assigned managed identities as defined in one of the `user_assigned_managed_identities_by_...` variables.\n  - any_principals: (Optional) The keys of the principals as defined in any of the `[principal_type]_by_...` variables. This is a convenience method that can be used in combination with or instrad of the specific principal type options.\n\nExample Input:\n\n```hcl\nrole_assignments_for_management_groups = {\n  management_group_id = \"mg-1-id\"\n  role_assignments    = {\n    role_definition = \"contributor\"\n    users = [\n      \"my-user-1\",\n      \"my-user-2\"\n    ]\n    groups = [\n      \"my-group-1\",\n      \"my-group-2\"\n    ]\n    app_registrations = [\n      \"my-app-1\",\n      \"my-app-2\"\n    ]\n    system_assigned_managed_identities = [\n      \"my-vm-1\",\n      \"my-vm-2\"\n    ]\n    user_assigned_managed_identities = [\n      \"my-user-assigned-managed-identity-1\",\n      \"my-user-assigned-managed-identity-2\"\n    ]\n  }\n}\n\nrole_assignments_for_management_groups = {\n  management_group_display_name = \"mg-1-display-name\"\n  role_assignments              = {\n    role_definition = \"contributor\"\n    users = [\n      \"my-user-1\",\n      \"my-user-2\"\n    ]\n    groups = [\n      \"my-group-1\",\n      \"my-group-2\"\n    ]\n    app_registrations = [\n      \"my-app-1\",\n      \"my-app-2\"\n    ]\n    system_assigned_managed_identities = [\n      \"my-vm-1\",\n      \"my-vm-2\"\n    ]\n    user_assigned_managed_identities = [\n      \"my-user-assigned-managed-identity-1\",\n      \"my-user-assigned-managed-identity-2\"\n    ]\n  }\n}\n```\n"
  default     = {}
}

variable "role_assignments_for_resource_groups" {
  type        = map(object({
    resource_group_name = string
    subscription_id     = optional(string, null)
    role_assignments = map(object({
      role_definition                    = string
      users                              = optional(set(string), [])
      groups                             = optional(set(string), [])
      app_registrations                  = optional(set(string), [])
      system_assigned_managed_identities = optional(set(string), [])
      user_assigned_managed_identities   = optional(set(string), [])
      any_principals                     = optional(set(string), [])
    }))
  }))
  description = "(Optional) Role assignments to be applied to resource groups.\nThe resource group can be in the current subscription (default) or a `subscription_id` can be supplied to target a resource group in another subscription.\nThis is a convenience variable that avoids the need to find the resource id of the resource group.\n\n- resource_group_name: (Required) The name of the resource group.\n- subscription_id: (Optional) The id of the subscription. If not supplied the current subscription is used.\n- role_assignments: (Required) The role assignments to be applied to the scope.\n  - role_definition: (Required) The key of the role definition as defined in the `role_definitions` variable.\n  - users: (Optional) The keys of the users as defined in one of the `users_by_...` variables.\n  - groups: (Optional) The keys of the groups as defined in one of the `groups_by_...` variables.\n  - app_registrations: (Optional) The keys of the app registrations as defined in one of the `app_registrations_by_...` variables.\n  - system_assigned_managed_identities: (Optional) The keys of the system assigned managed identities as defined in one of the `system_assigned_managed_identities_by_...` variables.\n  - user_assigned_managed_identities: (Optional) The keys of the user assigned managed identities as defined in one of the `user_assigned_managed_identities_by_...` variables.\n  - any_principals: (Optional) The keys of the principals as defined in any of the `[principal_type]_by_...` variables. This is a convenience method that can be used in combination with or instrad of the specific principal type options.\n\nExample Input:\n\n```hcl\nrole_assignments_for_resource_groups = {\n  resource_group_name = \"my-resource-group-name\"\n  role_assignments    = {\n    role_definition = \"contributor\"\n    users = [\n      \"my-user-1\",\n      \"my-user-2\"\n    ]\n    groups = [\n      \"my-group-1\",\n      \"my-group-2\"\n    ]\n    app_registrations = [\n      \"my-app-1\",\n      \"my-app-2\"\n    ]\n    system_assigned_managed_identities = [\n      \"my-vm-1\",\n      \"my-vm-2\"\n    ]\n    user_assigned_managed_identities = [\n      \"my-user-assigned-managed-identity-1\",\n      \"my-user-assigned-managed-identity-2\"\n    ]\n  }\n}\n```\n"
  default     = {}
}

variable "role_assignments_for_resources" {
  type        = map(object({
    resource_name       = string
    resource_group_name = string
    role_assignments = map(object({
      role_definition                    = string
      users                              = optional(set(string), [])
      groups                             = optional(set(string), [])
      app_registrations                  = optional(set(string), [])
      system_assigned_managed_identities = optional(set(string), [])
      user_assigned_managed_identities   = optional(set(string), [])
      any_principals                     = optional(set(string), [])
    }))
  }))
  description = "(Optional) Role assignments to be applied to resources. The resource is defined by the resource name and the resource group name.\nThis variable only works with the current provider subscription. This is a convenience variable that avoids the need to find the resource id.\n\n- resouce_name: (Required) The names of the resource.\n- resource_group_name: (Required) The name of the resource group.\n- role_assignments: (Required) The role assignments to be applied to the scope.\n  - role_definition: (Required) The key of the role definition as defined in the `role_definitions` variable.\n  - users: (Optional) The keys of the users as defined in one of the `users_by_...` variables.\n  - groups: (Optional) The keys of the groups as defined in one of the `groups_by_...` variables.\n  - app_registrations: (Optional) The keys of the app registrations as defined in one of the `app_registrations_by_...` variables.\n  - system_assigned_managed_identities: (Optional) The keys of the system assigned managed identities as defined in one of the `system_assigned_managed_identities_by_...` variables.\n  - user_assigned_managed_identities: (Optional) The keys of the user assigned managed identities as defined in one of the `user_assigned_managed_identities_by_...` variables.\n  - any_principals: (Optional) The keys of the principals as defined in any of the `[principal_type]_by_...` variables. This is a convenience method that can be used in combination with or instrad of the specific principal type options.\n\nExample Input:\n\n```hcl\nrole_assignments_for_resources = {\n  resource_name       = \"my-resource-name\"\n  resource_group_name = \"my-resource-group-name\"\n  role_assignments    = {\n    role_definition = \"contributor\"\n    users = [\n      \"my-user-1\",\n      \"my-user-2\"\n    ]\n    groups = [\n      \"my-group-1\",\n      \"my-group-2\"\n    ]\n    app_registrations = [\n      \"my-app-1\",\n      \"my-app-2\"\n    ]\n    system_assigned_managed_identities = [\n      \"my-vm-1\",\n      \"my-vm-2\"\n    ]\n    user_assigned_managed_identities = [\n      \"my-user-assigned-managed-identity-1\",\n      \"my-user-assigned-managed-identity-2\"\n    ]\n  }\n}\n```\n"
  default     = {}
}

variable "role_assignments_for_scopes" {
  type        = map(object({
    scope = string
    role_assignments = map(object({
      role_definition                    = string
      users                              = optional(set(string), [])
      groups                             = optional(set(string), [])
      app_registrations                  = optional(set(string), [])
      system_assigned_managed_identities = optional(set(string), [])
      user_assigned_managed_identities   = optional(set(string), [])
      any_principals                     = optional(set(string), [])
    }))
  }))
  description = "(Optional) Role assignments to be applied to specific scope ids. The scope id is the id of the resource, resource group, subscription or management group.\n\n- scope: (Required) The scope / id of the resource, resource group, subscription or management group.\n- role_assignments: (Required) The role assignments to be applied to the scope.\n  - role_definition: (Required) The key of the role definition as defined in the `role_definitions` variable.\n  - users: (Optional) The keys of the users as defined in one of the `users_by_...` variables.\n  - groups: (Optional) The keys of the groups as defined in one of the `groups_by_...` variables.\n  - app_registrations: (Optional) The keys of the app registrations as defined in one of the `app_registrations_by_...` variables.\n  - system_assigned_managed_identities: (Optional) The keys of the system assigned managed identities as defined in one of the `system_assigned_managed_identities_by_...` variables.\n  - user_assigned_managed_identities: (Optional) The keys of the user assigned managed identities as defined in one of the `user_assigned_managed_identities_by_...` variables.\n  - any_principals: (Optional) The keys of the principals as defined in any of the `[principal_type]_by_...` variables. This is a convenience method that can be used in combination with or instrad of the specific principal type options.\n\nExample Input:\n\n```hcl\nrole_assignments_for_scopes = {\n  scope            = \"/subscriptions/00000000-0000-0000-0000-000000000000/resourceGroups/my-resource-group\"\n  role_assignments = {\n    role_definition = \"contributor\"\n    users = [\n      \"my-user-1\",\n      \"my-user-2\"\n    ]\n    groups = [\n      \"my-group-1\",\n      \"my-group-2\"\n    ]\n    app_registrations = [\n      \"my-app-1\",\n      \"my-app-2\"\n    ]\n    system_assigned_managed_identities = [\n      \"my-vm-1\",\n      \"my-vm-2\"\n    ]\n    user_assigned_managed_identities = [\n      \"my-user-assigned-managed-identity-1\",\n      \"my-user-assigned-managed-identity-2\"\n    ]\n  }\n}\n```\n"
  default     = {}
}

variable "role_assignments_for_subscriptions" {
  type        = map(object({
    subscription_id = optional(string, null)
    role_assignments = map(object({
      role_definition                    = string
      users                              = optional(set(string), [])
      groups                             = optional(set(string), [])
      app_registrations                  = optional(set(string), [])
      system_assigned_managed_identities = optional(set(string), [])
      user_assigned_managed_identities   = optional(set(string), [])
      any_principals                     = optional(set(string), [])
    }))
  }))
  description = "(Optional) Role assignments to be applied to subscriptions.\nThis will default to the current subscription (default) or a `subscription_id` can be supplied to target another subscription.\nThis is a convenience variable that avoids the need to find the resource id of the subscription.\n\n- subscription_id: (Optional) The id of the subscription. If not supplied the current subscription is used.\n- role_assignments: (Required) The role assignments to be applied to the scope.\n  - role_definition: (Required) The key of the role definition as defined in the `role_definitions` variable.\n  - users: (Optional) The keys of the users as defined in one of the `users_by_...` variables.\n  - groups: (Optional) The keys of the groups as defined in one of the `groups_by_...` variables.\n  - app_registrations: (Optional) The keys of the app registrations as defined in one of the `app_registrations_by_...` variables.\n  - system_assigned_managed_identities: (Optional) The keys of the system assigned managed identities as defined in one of the `system_assigned_managed_identities_by_...` variables.\n  - user_assigned_managed_identities: (Optional) The keys of the user assigned managed identities as defined in one of the `user_assigned_managed_identities_by_...` variables.\n  - any_principals: (Optional) The keys of the principals as defined in any of the `[principal_type]_by_...` variables. This is a convenience method that can be used in combination with or instrad of the specific principal type options.\n\nExample Input:\n\n```hcl\nrole_assignments_for_subscriptions = {\n  subscription_id     = \"00000000-0000-0000-0000-000000000000\"\n  role_assignments    = {\n    role_definition = \"contributor\"\n    users = [\n      \"my-user-1\",\n      \"my-user-2\"\n    ]\n    groups = [\n      \"my-group-1\",\n      \"my-group-2\"\n    ]\n    app_registrations = [\n      \"my-app-1\",\n      \"my-app-2\"\n    ]\n    system_assigned_managed_identities = [\n      \"my-vm-1\",\n      \"my-vm-2\"\n    ]\n    user_assigned_managed_identities = [\n      \"my-user-assigned-managed-identity-1\",\n      \"my-user-assigned-managed-identity-2\"\n    ]\n  }\n}\n```\n"
  default     = {}
}

variable "role_definitions" {
  type        = map(string)
  description = "(Optional) A map of Azure Resource Manager role definitions to reference in role assignments.\nThe key is something unique to you. The value is a built in or custom role definition name.\n\nExample Input:\n\n```hcl\nrole_definitions = {\n  owner       = \"Owner\"\n  contributor = \"Contributor\"\n  reader      = \"Reader\"\n}\n```\n"
  default     = {}
}

variable "system_assigned_managed_identities_by_client_id" {
  type        = map(string)
  description = "(Optional) A map of system assigned managed identities to reference in role assignments.\nThe key is something unique to you. The value is the client id of the identity.\n\nExample Input:\n\n```hcl\nsystem_assigned_managed_identities_by_client_id = {\n  my-vm-1 = \"00000000-0000-0000-0000-000000000001\"\n  my-vm-2 = \"00000000-0000-0000-0000-000000000002\"\n}\n```\n"
  default     = {}
}

variable "system_assigned_managed_identities_by_display_name" {
  type        = map(string)
  description = "(Optional) A map of system assigned managed identities to reference in role assignments.\nThe key is something unique to you. The value is the display name of the identity / compute instance.\n\nExample Input:\n\n```hcl\nsystem_assigned_managed_identities_by_display_name = {\n  my-vm-1 = \"My VM 1\"\n  my-vm-2 = \"My VM 2\"\n}\n```\n"
  default     = {}
}

variable "system_assigned_managed_identities_by_principal_id" {
  type        = map(string)
  description = "(Optional) A map of system assigned managed identities to reference in role assignments.\nThe key is something unique to you. The value is the principal id of the underying service principalk of the identity.\n\nExample Input:\n\n```hcl\nsystem_assigned_managed_identities_by_principal_id = {\n  my-vm-1 = \"00000000-0000-0000-0000-000000000001\"\n  my-vm-2 = \"00000000-0000-0000-0000-000000000002\"\n}\n```\n"
  default     = {}
}

variable "telemetry_resource_group_name" {
  type        = string
  description = "The resource group where the telemetry will be deployed."
  default     = ""
}

variable "user_assigned_managed_identities_by_client_id" {
  type        = map(string)
  description = "(Optional) A map of system assigned managed identities to reference in role assignments.\nThe key is something unique to you. The value is the client id of the identity.\n\nExample Input:\n\n```hcl\nuser_assigned_managed_identities_by_client_id = {\n  my-identity-1 = \"00000000-0000-0000-0000-000000000001\"\n  my-identity-2 = \"00000000-0000-0000-0000-000000000002\"\n}\n```\n"
  default     = {}
}

variable "user_assigned_managed_identities_by_display_name" {
  type        = map(string)
  description = "(Optional) A map of system assigned managed identities to reference in role assignments.\nThe key is something unique to you. The value is the display name of the identity.\n\nExample Input:\n\n```hcl\nuser_assigned_managed_identities_by_display_name = {\n  my-identity-1 = \"My Identity 1\"\n  my-identity-2 = \"My Identity 2\"\n}\n```\n"
  default     = {}
}

variable "user_assigned_managed_identities_by_principal_id" {
  type        = map(string)
  description = "(Optional) A map of system assigned managed identities to reference in role assignments.\nThe key is something unique to you. The value is the principal id of the underying service principalk of the identity.\n\nExample Input:\n\n```hcl\nuser_assigned_managed_identities_by_principal_id = {\n  my-identity-1 = \"00000000-0000-0000-0000-000000000001\"\n  my-identity-2 = \"00000000-0000-0000-0000-000000000002\"\n}\n```\n"
  default     = {}
}

variable "user_assigned_managed_identities_by_resource_group_and_name" {
  type        = map(object({
    resource_group_name = string
    name                = string
  }))
  description = "(Optional) A map of user assigned managed identities to reference in role assignments.\nThe key is something unique to you. The values are:\n\n- resource_group_name: The name of the resource group the identity is in.\n- name: The name of the identity.\n\nExample Input:\n\n```hcl\nuser_assigned_managed_identities_by_resource_group_and_name = {\n  my-identity-1 = {\n    resource_group_name = \"my-rg-1\"\n    name                = \"my-identity-1\"\n  }\n  my-identity-2 = {\n    resource_group_name = \"my-rg-2\"\n    name                = \"my-identity-2\"\n  }\n}\n```\n"
  default     = {}
}

variable "users_by_employee_id" {
  type        = map(string)
  description = "(Optional) A map of Entra ID users to reference in role assignments.\nThe key is something unique to you. The value is the employee ID of the user.\n\nExample Input:\n\n```hcl\nusers_by_employee_id = {\n  my-user-1 = \"1234567890\"\n  my-user-2 = \"0987654321\"\n}\n```\n"
  default     = {}
}

variable "users_by_mail" {
  type        = map(string)
  description = "(Optional) A map of Entra ID users to reference in role assignments.\nThe key is something unique to you. The value is the mail address of the user.\n\nExample Input:\n\n```hcl\nusers_by_mail = {\n  my-user-1 = \"user.1@example.com\"\n  my-user-2 = \"user.2@example.com\"\n}\n```\n"
  default     = {}
}

variable "users_by_mail_nickname" {
  type        = map(string)
  description = "(Optional) A map of Entra ID users to reference in role assignments.\nThe key is something unique to you. The value is the mail nickname of the user.\n\nExample Input:\n\n```hcl\nusers_by_mail_nickname = {\n  my-user-1 = \"user1-nickname\"\n  my-user-2 = \"user2-nickname\"\n}\n```\n"
  default     = {}
}

variable "users_by_object_id" {
  type        = map(string)
  description = "(Optional) A map of Entra ID users to reference in role assignments.\nThe key is something unique to you. The value is the object ID of the user.\n\nExample Input:\n\n```hcl\nusers_by_object_id = {\n  my-user-1 = \"00000000-0000-0000-0000-000000000001\"\n  my-user-2 = \"00000000-0000-0000-0000-000000000002\"\n}\n```\n"
  default     = {}
}

variable "users_by_user_principal_name" {
  type        = map(string)
  description = "(Optional) A map of Entra ID users to reference in role assignments.\nThe key is something unique to you. The value is the user principal name (UPN) of the user.\n\nExample Input:\n\n```hcl\nusers_by_user_principal_name = {\n  my-user-1 = \"user1@example.com\"\n  my-user-2 = \"user2@example.com\"\n}\n```\n"
  default     = {}
}


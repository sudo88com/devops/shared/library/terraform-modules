
custom_domain_certificate_password = null

custom_domain_dns_suffix = null

dapr_application_insights_connection_string = null

dapr_components = {}

diagnostic_settings = {}

enable_telemetry = true

infrastructure_resource_group_name = null

infrastructure_subnet_id = null

internal_load_balancer_enabled = false

location = null

lock = null

log_analytics_workspace_customer_id = null

log_analytics_workspace_destination = "log-analytics"

log_analytics_workspace_primary_shared_key = null

name = 

peer_authentication_enabled = false

resource_group_name = 

role_assignments = {}

storages = {}

tags = null

timeouts = null

workload_profile = []

zone_redundancy_enabled = true


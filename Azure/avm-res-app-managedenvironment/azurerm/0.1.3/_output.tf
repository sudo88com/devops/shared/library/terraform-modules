
output "dapr_components" {
  description = "A map of dapr components connected to this environment. The map key is the supplied input to var.storages. The map value is the azurerm-formatted version of the entire dapr_components resource."
  value       = module.avm-res-app-managedenvironment.dapr_components
}

output "id" {
  description = "The ID of the resource."
  value       = module.avm-res-app-managedenvironment.id
}

output "name" {
  description = "The name of the resource"
  value       = module.avm-res-app-managedenvironment.name
}

output "resource" {
  description = "The Container Apps Managed Environment resource."
  value       = module.avm-res-app-managedenvironment.resource
}

output "storages" {
  description = "A map of storage shares connected to this environment. The map key is the supplied input to var.storages. The map value is the azurerm-formatted version of the entire storage shares resource."
  value       = module.avm-res-app-managedenvironment.storages
}


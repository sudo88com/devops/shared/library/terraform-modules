
variable "custom_domain_certificate_password" {
  type        = string
  description = "Certificate password for custom domain."
  default     = null
}

variable "custom_domain_dns_suffix" {
  type        = string
  description = "DNS suffix for custom domain."
  default     = null
}

variable "dapr_application_insights_connection_string" {
  type        = string
  description = "Application Insights connection string used by Dapr to export Service to Service communication telemetry."
  default     = null
}

variable "dapr_components" {
  type        = map(object({
    component_type         = string
    ignore_errors          = optional(bool, true)
    init_timeout           = optional(string)
    secret_store_component = optional(string)
    scopes                 = optional(list(string))
    version                = string
    metadata = optional(list(object({
      name        = string
      secret_name = optional(string)
      value       = optional(string)
    })))
    secret = optional(set(object({
      name  = string
      value = string
    })))
    timeouts = optional(object({
      create = optional(string)
      delete = optional(string)
      read   = optional(string)
    }))
  }))
  description = "- `component_type` - (Required) The Dapr Component Type. For example `state.azure.blobstorage`. Changing this forces a new resource to be created.\n- `ignore_errors` - (Optional) Should the Dapr sidecar to continue initialisation if the component fails to load. Defaults to `false`\n- `init_timeout` - (Optional) The timeout for component initialisation as a `ISO8601` formatted string. e.g. `5s`, `2h`, `1m`. Defaults to `5s`.\n- `secret_store_component` - (Optional) Name of a Dapr component to retrieve component secrets from.\n- `scopes` - (Optional) A list of scopes to which this component applies.\n- `version` - (Required) The version of the component.\n\n---\n`metadata` block supports the following:\n- `name` - (Required) The name of the Metadata configuration item.\n- `secret_name` - (Optional) The name of a secret specified in the `secrets` block that contains the value for this metadata configuration item.\n- `value` - (Optional) The value for this metadata configuration item.\n\n---\n`secret` block supports the following:\n- `name` - (Required) The Secret name.\n- `value` - (Required) The value for this secret.\n\n---\n`timeouts` block supports the following:\n- `create` - (Defaults to 30 minutes) Used when creating the Container App Environment Dapr Component.\n- `delete` - (Defaults to 30 minutes) Used when deleting the Container App Environment Dapr Component.\n- `read` - (Defaults to 5 minutes) Used when retrieving the Container App Environment Dapr Component.\n- `update` - (Defaults to 30 minutes) Used when updating the Container App Environment Dapr Component.\n"
  default     = {}
}

variable "diagnostic_settings" {
  type        = map(object({
    name                                     = optional(string, null)
    log_categories                           = optional(set(string), [])
    log_groups                               = optional(set(string), ["allLogs"])
    metric_categories                        = optional(set(string), ["AllMetrics"])
    log_analytics_destination_type           = optional(string, "Dedicated")
    workspace_resource_id                    = optional(string, null)
    storage_account_resource_id              = optional(string, null)
    event_hub_authorization_rule_resource_id = optional(string, null)
    event_hub_name                           = optional(string, null)
    marketplace_partner_resource_id          = optional(string, null)
  }))
  description = "A map of diagnostic settings to create on the Key Vault. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.\n\n- `name` - (Optional) The name of the diagnostic setting. One will be generated if not set, however this will not be unique if you want to create multiple diagnostic setting resources.\n- `log_categories` - (Optional) A set of log categories to send to the log analytics workspace. Defaults to `[]`.\n- `log_groups` - (Optional) A set of log groups to send to the log analytics workspace. Defaults to `[\"allLogs\"]`.\n- `metric_categories` - (Optional) A set of metric categories to send to the log analytics workspace. Defaults to `[\"AllMetrics\"]`.\n- `log_analytics_destination_type` - (Optional) The destination type for the diagnostic setting. Possible values are `Dedicated` and `AzureDiagnostics`. Defaults to `Dedicated`.\n- `workspace_resource_id` - (Optional) The resource ID of the log analytics workspace to send logs and metrics to.\n- `storage_account_resource_id` - (Optional) The resource ID of the storage account to send logs and metrics to.\n- `event_hub_authorization_rule_resource_id` - (Optional) The resource ID of the event hub authorization rule to send logs and metrics to.\n- `event_hub_name` - (Optional) The name of the event hub. If none is specified, the default event hub will be selected.\n- `marketplace_partner_resource_id` - (Optional) The full ARM resource ID of the Marketplace resource to which you would like to send Diagnostic LogsLogs.\n"
  default     = {}
}

variable "enable_telemetry" {
  type        = bool
  description = "This variable controls whether or not telemetry is enabled for the module.\nFor more information see <https://aka.ms/avm/telemetryinfo>.\nIf it is set to false, then no telemetry will be collected.\n"
  default     = true
}

variable "infrastructure_resource_group_name" {
  type        = string
  description = "Name of the platform-managed resource group created for the Managed Environment to host infrastructure resources. \nIf a subnet ID is provided, this resource group will be created in the same subscription as the subnet.\nIf not specified, then one will be generated automatically, in the form ``ME_<app_managed_environment_name>_<resource_group>_<location>``.\n"
  default     = null
}

variable "infrastructure_subnet_id" {
  type        = string
  description = "The existing Subnet to use for the Container Apps Control Plane. **NOTE:** The Subnet must have a `/21` or larger address space."
  default     = null
}

variable "internal_load_balancer_enabled" {
  type        = bool
  description = "Should the Container Environment operate in Internal Load Balancing Mode? Defaults to `false`. **Note:** can only be set to `true` if `infrastructure_subnet_id` is specified."
  default     = false
}

variable "location" {
  type        = string
  description = "Azure region where the resource should be deployed.  If null, the location will be inferred from the resource group location."
  default     = null
}

variable "lock" {
  type        = object({
    kind = string
    name = optional(string, null)
  })
  description = "Controls the Resource Lock configuration for this resource. The following properties can be specified:\n\n- `kind` - (Required) The type of lock. Possible values are `\\"CanNotDelete\\"` and `\\"ReadOnly\\"`.\n- `name` - (Optional) The name of the lock. If not specified, a name will be generated based on the `kind` value. Changing this forces the creation of a new resource.\n"
  default     = null
}

variable "log_analytics_workspace_customer_id" {
  type        = string
  description = "The ID for the Log Analytics Workspace to link this Container Apps Managed Environment to."
  default     = null
}

variable "log_analytics_workspace_destination" {
  type        = string
  description = "Destination for Log Analytics (options: 'log-analytics', 'azuremonitor', 'none')."
  default     = "log-analytics"
}

variable "log_analytics_workspace_primary_shared_key" {
  type        = string
  description = "Primary shared key for Log Analytics."
  default     = null
}

variable "name" {
  type        = string
  description = "The name of the Container Apps Managed Environment."
  default     = ""
}

variable "peer_authentication_enabled" {
  type        = bool
  description = "Enable peer authentication (Mutual TLS)."
  default     = false
}

variable "resource_group_name" {
  type        = string
  description = "(Required) The name of the resource group in which the Container App Environment is to be created. Changing this forces a new resource to be created."
  default     = ""
}

variable "role_assignments" {
  type        = map(object({
    role_definition_id_or_name             = string
    principal_id                           = string
    description                            = optional(string, null)
    skip_service_principal_aad_check       = optional(bool, false)
    condition                              = optional(string, null)
    condition_version                      = optional(string, null)
    delegated_managed_identity_resource_id = optional(string, null)
  }))
  description = "A map of role assignments to create on this resource. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.\n\n- `role_definition_id_or_name` - The ID or name of the role definition to assign to the principal.\n- `principal_id` - The ID of the principal to assign the role to.\n- `description` - The description of the role assignment.\n- `skip_service_principal_aad_check` - If set to true, skips the Azure Active Directory check for the service principal in the tenant. Defaults to false.\n- `condition` - The condition which will be used to scope the role assignment.\n- `condition_version` - The version of the condition syntax. Valid values are '2.0'.\n- `delegated_managed_identity_resource_id` - (Optional) The delegated Azure Resource Id which contains a Managed Identity. Changing this forces a new resource to be created. This field is only used in cross-tenant scenario.\n \n> Note: only set `skip_service_principal_aad_check` to true if you are assigning a role to a service principal.\n"
  default     = {}
}

variable "storages" {
  type        = map(object({
    access_key   = string
    access_mode  = string
    account_name = string
    share_name   = string
    timeouts = optional(object({
      create = optional(string)
      delete = optional(string)
      read   = optional(string)
    }))
  }))
  description = "- `access_key` - (Required) The Storage Account Access Key.\n- `access_mode` - (Required) The access mode to connect this storage to the Container App. Possible values include `ReadOnly` and `ReadWrite`. Changing this forces a new resource to be created.\n- `account_name` - (Required) The Azure Storage Account in which the Share to be used is located. Changing this forces a new resource to be created.\n- `share_name` - (Required) The name of the Azure Storage Share to use. Changing this forces a new resource to be created.\n\n---\n`timeouts` block supports the following:\n- `create` - (Defaults to 30 minutes) Used when creating the Container App Environment Storage.\n- `delete` - (Defaults to 30 minutes) Used when deleting the Container App Environment Storage.\n- `read` - (Defaults to 5 minutes) Used when retrieving the Container App Environment Storage.\n- `update` - (Defaults to 30 minutes) Used when updating the Container App Environment Storage.\n\n"
  default     = {}
}

variable "tags" {
  type        = map(string)
  description = "(Optional) A mapping of tags to assign to the resource."
  default     = null
}

variable "timeouts" {
  type        = object({
    create = optional(string)
    delete = optional(string)
    read   = optional(string)
  })
  description = "- `create` - (Defaults to 30 minutes) Used when creating the Container App Environment.\n- `delete` - (Defaults to 30 minutes) Used when deleting the Container App Environment.\n- `read` - (Defaults to 5 minutes) Used when retrieving the Container App Environment.\n- `update` - (Defaults to 30 minutes) Used when updating the Container App Environment.\n"
  default     = null
}

variable "workload_profile" {
  type        = set(object({
    maximum_count         = optional(number)
    minimum_count         = optional(number)
    name                  = string
    workload_profile_type = string
  }))
  description = "\nThis lists the workload profiles that will be configured for the Managed Environment.\nThis is in addition to the default Consumpion Plan workload profile.\n\n - `maximum_count` - (Optional) The maximum number of instances of workload profile that can be deployed in the Container App Environment.\n - `minimum_count` - (Optional) The minimum number of instances of workload profile that can be deployed in the Container App Environment.\n - `name` - (Required) The name of the workload profile.\n - `workload_profile_type` - (Required) Workload profile type for the workloads to run on. Possible values include `D4`, `D8`, `D16`, `D32`, `E4`, `E8`, `E16` and `E32`.\n"
  default     = []
}

variable "zone_redundancy_enabled" {
  type        = bool
  description = "(Optional) Should the Container App Environment be created with Zone Redundancy enabled? Defaults to `false`. Changing this forces a new resource to be created."
  default     = true
}


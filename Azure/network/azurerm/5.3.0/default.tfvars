
address_space = "10.0.0.0/16"

address_spaces = []

dns_servers = []

resource_group_location = null

resource_group_name = 

subnet_delegation = {}

subnet_enforce_private_link_endpoint_network_policies = {}

subnet_names = [
  "subnet1"
]

subnet_prefixes = [
  "10.0.1.0/24"
]

subnet_service_endpoints = {}

tags = {
  "environment": "dev"
}

tracing_tags_enabled = false

tracing_tags_prefix = "avm_"

use_for_each = 

vnet_name = "acctvnet"


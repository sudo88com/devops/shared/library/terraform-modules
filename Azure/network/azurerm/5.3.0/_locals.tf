
locals {
  address_space = var.address_space
  address_spaces = var.address_spaces
  dns_servers = var.dns_servers
  resource_group_location = var.resource_group_location
  resource_group_name = var.resource_group_name
  subnet_delegation = var.subnet_delegation
  subnet_enforce_private_link_endpoint_network_policies = var.subnet_enforce_private_link_endpoint_network_policies
  subnet_names = var.subnet_names
  subnet_prefixes = var.subnet_prefixes
  subnet_service_endpoints = var.subnet_service_endpoints
  tags = var.tags
  tracing_tags_enabled = var.tracing_tags_enabled
  tracing_tags_prefix = var.tracing_tags_prefix
  use_for_each = var.use_for_each
  vnet_name = var.vnet_name
}


output "vnet_peer_1_id" {
  description = "The id of the newly created virtual network peering in on first virtual netowork. "
  value       = module.vnetpeering.vnet_peer_1_id
}

output "vnet_peer_2_id" {
  description = "The id of the newly created virtual network peering in on second virtual netowork. "
  value       = module.vnetpeering.vnet_peer_2_id
}


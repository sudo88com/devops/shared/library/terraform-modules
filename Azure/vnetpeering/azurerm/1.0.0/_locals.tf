
locals {
  allow_cross_subscription_peering = var.allow_cross_subscription_peering
  allow_forwarded_traffic = var.allow_forwarded_traffic
  allow_gateway_transit = var.allow_gateway_transit
  allow_virtual_network_access = var.allow_virtual_network_access
  resource_group_names = var.resource_group_names
  subscription_ids = var.subscription_ids
  tags = var.tags
  use_remote_gateways = var.use_remote_gateways
  vnet_names = var.vnet_names
  vnet_peering_names = var.vnet_peering_names
}


variable "cdn_endpoint_custom_domains" {
  type        = map(object({
    cdn_endpoint_key = string
    host_name        = string
    name             = string
    cdn_managed_https = optional(object({
      certificate_type = string
      protocol_type    = string
      tls_version      = optional(string, "TLS12")
    }))
    user_managed_https = optional(object({
      key_vault_certificate_id = optional(string)
      key_vault_secret_id      = optional(string)
      tls_version              = optional(string)
    }))
  }))
  description = "- `cdn_endpoint_key` - (Required) key of the endpoint defined in variable cdn_endpoints.\n- `host_name` - (Required) The host name of the custom domain. Changing this forces a new CDN Endpoint Custom Domain to be created.\n- `name` - (Required) The name which should be used for this CDN Endpoint Custom Domain. Changing this forces a new CDN Endpoint Custom Domain to be created.\n\n---\n`cdn_managed_https` block supports the following:\n- `certificate_type` - (Required) The type of HTTPS certificate. Possible values are `Shared` and `Dedicated`.\n- `protocol_type` - (Required) The type of protocol. Possible values are `ServerNameIndication` and `IPBased`.\n- `tls_version` - (Optional) The minimum TLS protocol version that is used for HTTPS. Possible values are `TLS10` (representing TLS 1.0/1.1), `TLS12` (representing TLS 1.2) and `None` (representing no minimums). Defaults to `TLS12`.\n\n`user_managed_https` block supports the following:\n- `key_vault_certificate_id` - (Optional) The ID of the Key Vault Certificate that contains the HTTPS certificate. This is deprecated in favor of `key_vault_secret_id`.\n- `key_vault_secret_id` - (Optional) The ID of the Key Vault Secret that contains the HTTPS certificate.\n- `tls_version` - (Optional) The minimum TLS protocol version that is used for HTTPS. Possible values are `TLS10` (representing TLS 1.0/1.1), `TLS12` (representing TLS 1.2) and `None` (representing no minimums). Defaults to `TLS12`.\n"
  default     = {}
}

variable "cdn_endpoints" {
  type        = map(object({
    name                      = string
    tags                      = optional(map(any))
    is_http_allowed           = optional(bool, false)
    is_https_allowed          = optional(bool, true)
    content_types_to_compress = optional(list(string), [])

    geo_filters = optional(map(object({
      relative_path = string       # must be "/" for Standard_Microsoft. Must be unique across all filters. Only one allowed for Standard_Microsoft
      action        = string       # create a validation: allowed values: Allow or Block
      country_codes = list(string) # Create a validation. Two letter country codes allows e.g. ["US", "CA"]
    })), {})

    is_compression_enabled        = optional(bool)
    querystring_caching_behaviour = optional(string, "IgnoreQueryString") #create a validation: allowed values: IgnoreQueryString,BypassCaching ,UseQueryString,NotSet for premium verizon.
    optimization_type             = optional(string)                      # create a validation: allowed values: DynamicSiteAcceleration,GeneralMediaStreaming,GeneralWebDelivery,LargeFileDownload ,VideoOnDemandMediaStreaming

    origins = map(object({
      name       = string
      host_name  = string
      http_port  = optional(number, 80)
      https_port = optional(number, 443)
    }))

    origin_host_header = optional(string)
    origin_path        = optional(string) # must start with / e.g. "/media"
    probe_path         = optional(string) # must start with / e.g. "/foo.bar"

    global_delivery_rule = optional(object({ #verify structure later
      cache_expiration_action = optional(list(object({
        behavior = string           # Allowed Values: BypassCache, Override and SetIfMissing
        duration = optional(string) # Only allowed when behavior is Override or SetIfMissing. Format: [d.]hh:mm:ss e.g "1.10:30:00"
      })), [])
      cache_key_query_string_action = optional(list(object({
        behavior   = string           # Allowed Values: Exclude, ExcludeAll, Include and IncludeAll
        parameters = optional(string) # Documentation says it is a list but string e.g "*"
      })), [])
      modify_request_header_action = optional(list(object({
        action = string # Allowed Values: Append, Delete and Overwrite
        name   = string
        value  = optional(string) # Only needed if action = Append or Overwrite
      })), [])
      modify_response_header_action = optional(list(object({
        action = string # Allowed Values: Append, Delete and Overwrite
        name   = string
        value  = optional(string) # Only needed if action = Append or Overwrite
      })), [])
      url_redirect_action = optional(list(object({
        redirect_type = string                    # Allowed Values: Found, Moved, PermanentRedirect and TemporaryRedirect
        protocol      = optional(string, "Https") # Allowed Values: MatchRequest, Http and Https
        hostname      = optional(string)
        path          = optional(string) # Should begin with /
        fragment      = optional(string) #Specifies the fragment part of the URL. This value must not start with a #
        query_string  = optional(string) # Specifies the query string part of the URL. This value must not start with a ? or & and must be in <key>=<value> format separated by &.
      })), [])
      url_rewrite_action = optional(list(object({
        source_pattern          = string #(Required) This value must start with a / and can't be longer than 260 characters.
        destination             = string # This value must start with a / and can't be longer than 260 characters.
        preserve_unmatched_path = optional(bool, true)
      })), [])
    }), {})

    delivery_rules = optional(list(object({ #verify structure later
      name  = string
      order = number
      cache_expiration_action = optional(object({
        behavior = string
        duration = optional(string)
      }))
      cache_key_query_string_action = optional(object({
        behavior   = string
        parameters = optional(string)
      }))
      cookies_condition = optional(object({
        selector         = string
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      }))
      device_condition = optional(object({
        operator         = optional(string, "Equal")
        negate_condition = optional(bool, false)
        match_values     = list(string)
      }))
      http_version_condition = optional(object({
        operator         = optional(string, "Equal")
        negate_condition = optional(bool, false)
        match_values     = list(string)
      }))
      modify_request_header_action = optional(object({
        action = string
        name   = string
        value  = optional(string)
      }))
      modify_response_header_action = optional(object({
        action = string
        name   = string
        value  = optional(string)
      }))
      post_arg_condition = optional(object({
        selector         = string
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      }))
      query_string_condition = optional(object({
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      }))
      remote_address_condition = optional(object({
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
      }))

      request_body_condition = optional(object({
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      }))
      request_header_condition = optional(object({
        selector         = string
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      }))
      request_method_condition = optional(object({
        operator         = optional(string, "Equal")
        negate_condition = optional(bool, false)
        match_values     = list(string)
      }))
      request_scheme_condition = optional(object({ #request protocol
        operator         = optional(string, "Equal")
        negate_condition = optional(bool, false)
        match_values     = list(string)
      }))
      request_uri_condition = optional(object({
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      }))
      url_file_extension_condition = optional(object({
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      }))
      url_file_name_condition = optional(object({
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      }))
      url_path_condition = optional(object({
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      }))
      url_redirect_action = optional(object({
        redirect_type = string
        protocol      = optional(string, "MatchRequest")
        hostname      = optional(string)
        path          = optional(string)
        fragment      = optional(string)
        query_string  = optional(string)
      }))
      url_rewrite_action = optional(object({
        source_pattern          = string
        destination             = string
        preserve_unmatched_path = optional(bool, true)
      }))
    })))
    diagnostic_setting = optional(object({
      name                                     = optional(string, null)
      log_categories                           = optional(set(string), [])
      log_groups                               = optional(set(string), [])
      metric_categories                        = optional(set(string), [])
      log_analytics_destination_type           = optional(string, "Dedicated")
      workspace_resource_id                    = optional(string, null)
      storage_account_resource_id              = optional(string, null)
      event_hub_authorization_rule_resource_id = optional(string, null)
      event_hub_name                           = optional(string, null)
      marketplace_partner_resource_id          = optional(string, null)
    }), {})
  }))
  description = "  Manages a CDN Endpoint. A CDN Endpoint is the entity within a CDN Profile containing configuration information regarding caching behaviours and origins. \n  Refer https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/cdn_endpoint#arguments-reference for details and description on the CDN endpoint arguments reference.\n  Example Input:\n\n  ```terraform\n    cdn_endpoints = {\n      ep1 = {\n        name                          = \"endpoint-ex\"\n        is_http_allowed               = true\n        is_https_allowed              = true\n        querystring_caching_behaviour = \"BypassCaching\"\n        is_compression_enabled        = true\n        optimization_type             = \"GeneralWebDelivery\"\n        geo_filters = { # Only one geo filter allowed for Standard_Microsoft sku\n          gf1 = {\n            relative_path = \"/\" \n            action        = \"Block\"\n            country_codes = [\"AF\", \"GB\"]\n          }\n        }\n        content_types_to_compress = [\n          \"application/eot\",\n          \"application/font\",\n          \"application/font-sfnt\",\n          \"application/javascript\",\n          \"application/json\",\n          \"application/opentype\",\n          \"application/otf\",\n          \"application/pkcs7-mime\",\n          \"application/truetype\",\n          \"application/ttf\",\n          \"application/vnd.ms-fontobject\",\n          \"application/xhtml+xml\",\n          \"application/xml\",\n          \"application/xml+rss\",\n          \"application/x-font-opentype\",\n          \"application/x-font-truetype\",\n          \"application/x-font-ttf\",\n          \"application/x-httpd-cgi\",\n          \"application/x-javascript\",\n          \"application/x-mpegurl\",\n          \"application/x-opentype\",\n          \"application/x-otf\",\n          \"application/x-perl\",\n          \"application/x-ttf\",\n          \"font/eot\",\n          \"font/ttf\",\n          \"font/otf\",\n          \"font/opentype\",\n          \"image/svg+xml\",\n          \"text/css\",\n          \"text/csv\",\n          \"text/html\",\n          \"text/javascript\",\n          \"text/js\",\n          \"text/plain\",\n          \"text/richtext\",\n          \"text/tab-separated-values\",\n          \"text/xml\",\n          \"text/x-script\",\n          \"text/x-component\",\n          \"text/x-java-source\",\n        ]\n        origin_host_header = replace(replace(azurerm_storage_account.storage.primary_blob_endpoint, \"https://\", \"\"), \"/\", \"\")\n        origin_path        = \"/media\"\n        probe_path         = \"/foo.bar\"\n        origins = {\n          og1 = { name = \"origin1\"\n            host_name = replace(replace(azurerm_storage_account.storage.primary_blob_endpoint, \"https://\", \"\"), \"/\", \"\")\n          }\n        }\n        diagnostic_setting = {\n          name                        = \"storage_diag\"\n          log_groups                  = [\"allLogs\"] \n          storage_account_resource_id = azurerm_storage_account.storage.id\n          marketplace_partner_resource_id          = \"/subscriptions/{subscriptionId}/resourceGroups/{resourceGroupName}/providers/{partnerResourceProvider}/{partnerResourceType}/{partnerResourceName}\"\n        }\n      }\n    }\n  ```\n"
  default     = {}
}

variable "diagnostic_settings" {
  type        = map(object({
    name                                     = optional(string, null)
    log_categories                           = optional(set(string), [])
    log_groups                               = optional(set(string), ["allLogs"])
    metric_categories                        = optional(set(string), ["AllMetrics"])
    log_analytics_destination_type           = optional(string, "Dedicated")
    workspace_resource_id                    = optional(string, null)
    storage_account_resource_id              = optional(string, null)
    event_hub_authorization_rule_resource_id = optional(string, null)
    event_hub_name                           = optional(string, null)
    marketplace_partner_resource_id          = optional(string, null)
  }))
  description = "  A map of diagnostic settings to create on the Key Vault. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.\n  \n  - `name` - (Optional) The name of the diagnostic setting. One will be generated if not set, however this will not be unique if you want to create multiple diagnostic setting resources.\n  - `log_categories` - (Optional) A set of log categories to send to the log analytics workspace. Defaults to `[]`.\n  - `log_groups` - (Optional) A set of log groups to send to the log analytics workspace. Defaults to `[\"allLogs\"]`.\n  - `metric_categories` - (Optional) A set of metric categories to send to the log analytics workspace. Defaults to `[\"AllMetrics\"]`.\n  - `log_analytics_destination_type` - (Optional) The destination type for the diagnostic setting. Possible values are `Dedicated` and `AzureDiagnostics`. Defaults to `Dedicated`.\n  - `workspace_resource_id` - (Optional) The resource ID of the log analytics workspace to send logs and metrics to.\n  - `storage_account_resource_id` - (Optional) The resource ID of the storage account to send logs and metrics to.\n  - `event_hub_authorization_rule_resource_id` - (Optional) The resource ID of the event hub authorization rule to send logs and metrics to.\n  - `event_hub_name` - (Optional) The name of the event hub. If none is specified, the default event hub will be selected.\n  - `marketplace_partner_resource_id` - (Optional) The full ARM resource ID of the Marketplace resource to which you would like to send Diagnostic LogsLogs.\n  Example Input:\n\n  ```terraform\n    diagnostic_settings = {\n      workspaceandstorage_diag = {\n        name                           = \"workspaceandstorage_diag\"\n        metric_categories              = [\"AllMetrics\"]\n        log_categories                 = [\"FrontDoorAccessLog\", \"FrontDoorHealthProbeLog\", \"FrontDoorWebApplicationFirewallLog\"]\n        log_groups                     = [] #must explicitly set since log_groups defaults to [\"allLogs\"]\n        log_analytics_destination_type = \"Dedicated\"\n        workspace_resource_id          = azurerm_log_analytics_workspace.workspace.id\n        storage_account_resource_id    = azurerm_storage_account.storage.id\n        #marketplace_partner_resource_id          = \"/subscriptions/{subscriptionId}/resourceGroups/{resourceGroupName}/providers/{partnerResourceProvider}/{partnerResourceType}/{partnerResourceName}\"\n      }\n      eventhub_diag = {\n        name                                     = \"eventhubforwarding\"\n        log_groups                               = [\"allLogs\", \"Audit\"] # you can set either log_categories or log_groups.\n        metric_categories                        = [\"AllMetrics\"]\n        event_hub_authorization_rule_resource_id = azurerm_eventhub_namespace_authorization_rule.example.id\n        event_hub_name                           = azurerm_eventhub_namespace.eventhub_namespace.name\n      }\n    }\n  ```\n"
  default     = {}
}

variable "enable_telemetry" {
  type        = bool
  description = "  This variable controls whether or not telemetry is enabled for the module.\n  For more information see https://aka.ms/avm/telemetryinfo.\n  If it is set to false, then no telemetry will be collected.\n"
  default     = true
}

variable "front_door_custom_domains" {
  type        = map(object({
    name        = string
    dns_zone_id = optional(string, null)
    host_name   = string
    tls = object({
      certificate_type         = optional(string, "ManagedCertificate")
      minimum_tls_version      = optional(string, "TLS12") # TLS1.3 is not yet supported in Terraform azurerm_cdn_frontdoor_custom_domain
      cdn_frontdoor_secret_key = optional(string, null)
    })
  }))
  description = "  Manages a Front Door (standard/premium) Custom Domain.\n  \n  - `name` - (Required) The name which should be used for this Front Door Custom Domain. \n  - `dns_zone_id` - (Optional) The ID of the Azure DNS Zone which should be used for this Front Door Custom Domain.\n  - `host_name` - (Required) The host name of the domain. The host_name field must be the FQDN of your domain.\n  - `tls` - (Required) A tls block as defined below : -\n    - 'certificate_type' - (Optional) Defines the source of the SSL certificate. Possible values include 'CustomerCertificate' and 'ManagedCertificate'. Defaults to 'ManagedCertificate'.\n    - 'minimum_tls_version' - (Optional) TLS protocol version that will be used for Https. Possible values include 'TLS10' and 'TLS12'. Defaults to 'TLS12'.\n    - 'cdn_frontdoor_secret_key' - (Optional) Key of the Front Door Secret object. This is required when certificate_type is 'CustomerCertificate'.\n  Example Input:\n\n  ```terraform\n  front_door_custom_domains = {\n    contoso1_key = {\n        name        = \"contoso1\"\n        dns_zone_id = azurerm_dns_zone.dnszone.id\n        host_name   = \"contoso1.fabrikam.com\"\n        tls = {\n          certificate_type    = \"ManagedCertificate\"\n          minimum_tls_version = \"TLS12\" \n          cdn_frontdoor_secret_key = \"Secret1_key\"\n        }\n      }\n    }\n  ```\n"
  default     = {}
}

variable "front_door_endpoints" {
  type        = map(object({
    name    = string
    enabled = optional(bool, true)
    tags    = optional(map(any))
  }))
  description = "  Manages a Front Door (standard/premium) Endpoint.\n  \n  - `name` - (Required) The name which should be used for this Front Door Endpoint.  \n  - `enabled` - (Optional) Specifies if this Front Door Endpoint is enabled? Defaults to true.\n  - 'tags' - (Optional) Specifies a mapping of tags which should be assigned to the Front Door Endpoint.\n  Example Input:\n\n  ```terraform\n  front_door_endpoints = {\n    ep1_key = {\n        name = \"ep1-ex\"\n        enabled = true\n        tags = {\n          environment = \"avm-demo\"\n        }\n      }\n    }\n  ```\n"
  default     = {}
}

variable "front_door_firewall_policies" {
  type        = map(object({
    name                              = string
    resource_group_name               = string
    sku_name                          = string
    enabled                           = optional(bool, true)
    mode                              = string
    request_body_check_enabled        = optional(bool, true)
    redirect_url                      = optional(string)
    custom_block_response_status_code = optional(number)
    custom_block_response_body        = optional(string)
    custom_rules = optional(map(object({
      name                           = string
      enabled                        = optional(bool, true)
      priority                       = optional(number, 1)
      rate_limit_duration_in_minutes = optional(number, 1)
      rate_limit_threshold           = optional(number, 10)
      type                           = string
      action                         = string
      match_conditions = map(object({
        match_variable     = string
        operator           = string
        negation_condition = optional(bool)
        match_values       = list(string)
        selector           = optional(string)
        transforms         = optional(list(string))
      }))
    })), {})
    managed_rules = optional(map(object({
      type    = string
      version = string
      action  = string #default Log
      exclusions = optional(map(object({
        match_variable = string
        operator       = string
        selector       = optional(string)
      })), {})
      overrides = optional(map(object({
        rule_group_name = string
        exclusions = optional(map(object({
          match_variable = string
          operator       = string
          selector       = optional(string)
        })), {})
        rules = optional(map(object({
          rule_id = string
          action  = string
          enabled = optional(bool, false)
          exclusions = optional(map(object({
            match_variable = string
            operator       = string
            selector       = optional(string)
          })), {})
        })), {})
      })), {})
    })), {})
    tags = optional(map(any))
  }))
  description = "  Manages a Front Door (standard/premium) Firewall Policy instance.\n  \n  - `name` - (Required) The name which should be used for this Front Door Security Policy. Possible values must not be an empty string.\n  - `resource_group_name` - (Required) The name of the resource group. Changing this forces a new resource to be created.\n  - 'sku_name' - (Required) The sku's pricing tier for this Front Door Firewall Policy. Possible values include 'Standard_AzureFrontDoor' or 'Premium_AzureFrontDoor'.\n  - 'enabled' - (Optional) Is the Front Door Firewall Policy enabled? Defaults to true.\n  - 'mode' - (Required) The Front Door Firewall Policy mode. Possible values are 'Detection', 'Prevention'.\n  - 'request_body_check_enabled' - (Optional) Should policy managed rules inspect the request body content? Defaults to true.\n  - 'redirect_url' - (Optional) If action type is redirect, this field represents redirect URL for the client.\n  - 'custom_block_response_status_code' - (Optional) If a custom_rule block's action type is block, this is the response status code. Possible values are 200, 403, 405, 406, or 429.\n  - 'custom_block_response_body' - (Optional) If a custom_rule block's action type is block, this is the response body. The body must be specified in base64 encoding.\n  - 'custom_rule' - (Optional) One or more custom_rule blocks as defined below.\n    - 'name' - (Required) Gets name of the resource that is unique within a policy. This name can be used to access the resource.\n    - 'action' - (Required) The action to perform when the rule is matched. Possible values are 'Allow', 'Block', 'Log', or 'Redirect'.\n    - 'enabled' - (Optional) Is the rule is enabled or disabled? Defaults to true.\n    - 'priority' - (Optional) The priority of the rule. Rules with a lower value will be evaluated before rules with a higher value. Defaults to 1.\n    - 'type' - (Required) The type of rule. Possible values are MatchRule or RateLimitRule.\n    - 'rate_limit_duration_in_minutes' - (Optional) The rate limit duration in minutes. Defaults to 1.\n    - 'rate_limit_threshold' - (Optional) The rate limit threshold. Defaults to 10.\n    - 'match_condition' - (Optional) One or more match_condition block defined below. Can support up to 10 match_condition blocks.\n      - 'match_variable' - (Required) The request variable to compare with. Possible values are Cookies, PostArgs, QueryString, RemoteAddr, RequestBody, RequestHeader, RequestMethod, RequestUri, or SocketAddr.\n      - 'match_values' - (Required) Up to 600 possible values to match. Limit is in total across all match_condition blocks and match_values arguments. String value itself can be up to 256 characters in length.\n      - 'operator' - (Required) Comparison type to use for matching with the variable value. Possible values are Any, BeginsWith, Contains, EndsWith, Equal, GeoMatch, GreaterThan, GreaterThanOrEqual, IPMatch, LessThan, LessThanOrEqual or RegEx.\n      - 'selector' - (Optional) Match against a specific key if the match_variable is QueryString, PostArgs, RequestHeader or Cookies\n      - 'negation_condition' - (Optional) Should the result of the condition be negated.\n      - 'transforms' - (Optional) Up to 5 transforms to apply. Possible values are Lowercase, RemoveNulls, Trim, Uppercase, URLDecode or URLEncode.\n  - 'managed_rule' -  (Optional) One or more managed_rule blocks as defined below.\n    - 'type' - (Required) The name of the managed rule to use with this resource. Possible values include DefaultRuleSet, Microsoft_DefaultRuleSet, BotProtection or Microsoft_BotManagerRuleSet.\n    - 'version' - (Required) The version of the managed rule to use with this resource. Possible values depends on which DRS type you are using, for the DefaultRuleSet type the possible values include 1.0 or preview-0.1. For Microsoft_DefaultRuleSet the possible values include 1.1, 2.0 or 2.1. For BotProtection the value must be preview-0.1 and for Microsoft_BotManagerRuleSet the value must be 1.0.\n    - 'action' - (Required) The action to perform for all DRS rules when the managed rule is matched or when the anomaly score is 5 or greater depending on which version of the DRS you are using. Possible values include Allow, Log, Block, and Redirect.\n    - 'exclusion' - (Optional) One or more exclusion blocks as defined below: -\n      - 'match_variable' - (Required) The variable type to be excluded. Possible values are QueryStringArgNames, RequestBodyPostArgNames, RequestCookieNames, RequestHeaderNames, RequestBodyJsonArgNames.\n      - 'operator' - (Required) Comparison operator to apply to the selector when specifying which elements in the collection this exclusion applies to. Possible values are: Equals, Contains, StartsWith, EndsWith, EqualsAny.\n      - 'selector' - (Required) Selector for the value in the match_variable attribute this exclusion applies to.\n    - 'override' - (Optional) One or more override blocks as defined below: -\n      - 'rule_group_name' - (Required) The managed rule group to override.\n      - 'exclusion' - (Optional) One or more exclusion blocks as defined below: -\n      - 'rule' - (Optional) One or more rule blocks as defined below. If none are specified, all of the rules in the group will be disabled: -\n        - 'rule_id' - (Required) Identifier for the managed rule.\n        - 'action' - (Required) The action to be applied when the managed rule matches or when the anomaly score is 5 or greater. Possible values for DRS 1.1 and below are Allow, Log, Block, and Redirect. For DRS 2.0 and above the possible values are Log or AnomalyScoring.\n        - 'enabled' - (Optional) Is the managed rule override enabled or disabled. Defaults to false.\n        - 'exclusion' - (Optional) One or more exclusion blocks as defined below: -\n          - 'match_variable' - (Required) The variable type to be excluded. Possible values are QueryStringArgNames, RequestBodyPostArgNames, RequestCookieNames, RequestHeaderNames, RequestBodyJsonArgNames.\n          - 'operator' - (Required) Comparison operator to apply to the selector when specifying which elements in the collection this exclusion applies to. Possible values are: Equals, Contains, StartsWith, EndsWith, EqualsAny.\n          - 'selector' - (Required) Selector for the value in the match_variable attribute this exclusion applies to.\n  - 'tags' - (Optional) A mapping of tags to assign to the Front Door Firewall Policy.\n  /*\n  Example Input:\n\n  ```terraform\n  front_door_firewall_policies = {\n      fd_waf1_key = {\n      name                              = \"examplecdnfdwafpolicy1\"\n      resource_group_name               = azurerm_resource_group.this.name\n      sku_name                          = \"Premium_AzureFrontDoor\" # Ensure SKU_name for WAF is similar to SKU_name for front door profile.\n      enabled                           = true\n      mode                              = \"Prevention\"\n      redirect_url                      = \"https://www.contoso.com\"\n      custom_block_response_status_code = 405\n      custom_block_response_body        = \"PGh0bWw+CjxoZWFkZXI+PHRpdGxlPkhlbGxvPC90aXRsZT48L2hlYWRlcj4KPGJvZHk+CkhlbGxvIHdvcmxkCjwvYm9keT4KPC9odG1sPg==\"\n\n      custom_rules = {\n        cr1 = {\n          name                           = \"Rule1\"\n          enabled                        = true\n          priority                       = 1\n          rate_limit_duration_in_minutes = 1\n          rate_limit_threshold           = 10\n          type                           = \"MatchRule\"\n          action                         = \"Block\"\n          match_conditions = {\n            m1 = {\n              match_variable     = \"RemoteAddr\"\n              operator           = \"IPMatch\"\n              negation_condition = false\n              match_values       = [\"10.0.1.0/24\", \"10.0.0.0/24\"]\n            }\n          }\n        }\n\n        cr2 = {\n          name                           = \"Rule2\"\n          enabled                        = true\n          priority                       = 2\n          rate_limit_duration_in_minutes = 1\n          rate_limit_threshold           = 10\n          type                           = \"MatchRule\"\n          action                         = \"Block\"\n          match_conditions = {\n            match_condition1 = {\n              match_variable     = \"RemoteAddr\"\n              operator           = \"IPMatch\"\n              negation_condition = false\n              match_values       = [\"192.168.1.0/24\"]\n            }\n\n            match_condition2 = {\n              match_variable     = \"RequestHeader\"\n              selector           = \"UserAgent\"\n              operator           = \"Contains\"\n              negation_condition = false\n              match_values       = [\"windows\"]\n              transforms         = [\"Lowercase\", \"Trim\"]\n            }\n          }\n        }\n      }\n      managed_rules = {\n        mr1 = {\n          type    = \"Microsoft_DefaultRuleSet\"\n          version = \"2.1\"\n          action  = \"Log\"\n          exclusions = {\n            exclusion1 = {\n              match_variable = \"QueryStringArgNames\"\n              operator       = \"Equals\"\n              selector       = \"not_suspicious\"\n            }\n          }\n          overrides = {\n            override1 = {\n              rule_group_name = \"PHP\"\n              rule = {\n                rule1 = {\n                  rule_id = \"933100\"\n                  enabled = false\n                  action  = \"Log\"\n                }\n              }\n            }\n            override2 = {\n              rule_group_name = \"SQLI\"\n              exclusions = {\n                exclusion1 = {\n                  match_variable = \"QueryStringArgNames\"\n                  operator       = \"Equals\"\n                  selector       = \"really_not_suspicious\"\n                }\n              }\n              rules = {\n                rule1 = {\n                  rule_id = \"942200\"\n                  action  = \"Log\"\n                  exclusions = {\n                    exclusion1 = {\n                      match_variable = \"QueryStringArgNames\"\n                      operator       = \"Equals\"\n                      selector       = \"innocent\"\n                    }\n                  }\n                }\n              }\n            }\n          }\n        }\n\n        mr2 = {\n          type    = \"Microsoft_BotManagerRuleSet\"\n          version = \"1.0\"\n          action  = \"Log\"\n        }\n      }\n    }\n  }\n  ```\n"
  default     = {}
}

variable "front_door_origin_groups" {
  type        = map(object({
    name = string
    health_probe = optional(map(object({
      interval_in_seconds = number
      path                = optional(string, "/")
      protocol            = string
      request_type        = optional(string, "HEAD")
    })), {})
    load_balancing = map(object({
      additional_latency_in_milliseconds = optional(number, 50)
      sample_size                        = optional(number, 4)
      successful_samples_required        = optional(number, 3)
    }))
  }))
  description = "  Manages a Front Door (standard/premium) Origin group.\n  \n  - `name` - (Required) The name which should be used for this Front Door Origin Group. \n  - `load_balancing` - (Required) A load_balancing block as defined below:-\n      - 'additional_latency_in_milliseconds' - (Optional) Specifies the additional latency in milliseconds for probes to fall into the lowest latency bucket. Possible values are between 0 and 1000 milliseconds (inclusive). Defaults to 50\n      - 'sample_size' - (Optional) Specifies the number of samples to consider for load balancing decisions. Possible values are between 0 and 255 (inclusive). Defaults to 4.\n      - 'successful_samples_required' - (Optional) Specifies the number of samples within the sample period that must succeed. Possible values are between 0 and 255 (inclusive). Defaults to 3.\n  - 'health_probe' - (Optional) A health_probe block as defined below:-\n      - 'protocol' - (Required) Specifies the protocol to use for health probe. Possible values are Http and Https.\n      - 'interval_in_seconds' - (Required) Specifies the number of seconds between health probes. Possible values are between 5 and 31536000 seconds (inclusive).\n      - 'request_type' - (Optional) Specifies the type of health probe request that is made. Possible values are GET and HEAD. Defaults to HEAD.\n      - 'path' - (Optional) Specifies the path relative to the origin that is used to determine the health of the origin. Defaults to /.\n  Example Input:\n\n  ```terraform\n  front_door_origin_groups = {\n    og1_key = {\n      name = \"og1\"\n      health_probe = {\n        hp1 = {\n          interval_in_seconds = 240\n          path                = \"/healthProbe\"\n          protocol            = \"Https\"\n          request_type        = \"HEAD\"\n        }\n      }\n      load_balancing = {\n        lb1 = {\n          additional_latency_in_milliseconds = 0\n          sample_size                        = 16\n          successful_samples_required        = 3\n        }\n      }\n    }\n  }\n  ```\n"
  default     = {}
}

variable "front_door_origins" {
  type        = map(object({
    name                           = string
    origin_group_key               = string
    host_name                      = string
    certificate_name_check_enabled = string
    enabled                        = optional(bool, true)
    http_port                      = optional(number, 80)
    https_port                     = optional(number, 443)
    host_header                    = optional(string, null)
    priority                       = optional(number, 1)
    weight                         = optional(number, 500)
    private_link = optional(map(object({
      request_message        = string
      target_type            = optional(string, null)
      location               = string
      private_link_target_id = string
    })), null)
  }))
  description = "  Manages a Front Door (standard/premium) Origin.\n  \n  - `name` - (Required) The name which should be used for this Front Door Origin.\n  - 'origin_group_name' - (Required) The name of the origin group to associate the origin with.\n  - `host_name` - (Required) The IPv4 address, IPv6 address or Domain name of the Origin.\n  - 'certificate_name_check_enabled' - (Required) Specifies whether certificate name checks are enabled for this origin.\n  - 'enabled' - (Optional) Should the origin be enabled? Possible values are true or false. Defaults to true.\n  - 'http_port' - (Optional) The value of the HTTP port. Must be between 1 and 65535. Defaults to 80\n  - 'https_port' - (Optional) The value of the HTTPS port. Must be between 1 and 65535. Defaults to 443.\n  - 'origin_host_header' - (Optional) The host header value (an IPv4 address, IPv6 address or Domain name) which is sent to the origin with each request. If unspecified the hostname from the request will be used.\n  - 'priority' - (Optional) Priority of origin in given origin group for load balancing. Higher priorities will not be used for load balancing if any lower priority origin is healthy. Must be between 1 and 5 (inclusive). Defaults to 1\n  - 'private_link' - (Optional) A private_link block as defined below:-\n      - 'request_message' - (Optional) Specifies the request message that will be submitted to the private_link_target_id when requesting the private link endpoint connection. Values must be between 1 and 140 characters in length. Defaults to Access request for CDN FrontDoor Private Link Origin.\n      - 'target_type' - (Optional) Specifies the type of target for this Private Link Endpoint. Possible values are blob, blob_secondary, web and sites.\n      - 'location' - (Required) Specifies the location where the Private Link resource should exist. Changing this forces a new resource to be created.\n  - 'weight' - (Optional) The weight of the origin in a given origin group for load balancing. Must be between 1 and 1000. Defaults to 500.\n  Example Input:\n\n  ```terraform\n  front_door_origins = {\n      origin1_key = {\n        name                           = \"origin1\"\n        origin_group_key               = \"og1_key\"\n        enabled                        = true\n        certificate_name_check_enabled = true\n        host_name                      = replace(replace(azurerm_storage_account.storage.primary_blob_endpoint, \"https://\", \"\"), \"/\", \"\")\n        http_port                      = 80\n        https_port                     = 443\n        host_header                    = replace(replace(azurerm_storage_account.storage.primary_blob_endpoint, \"https://\", \"\"), \"/\", \"\")\n        priority                       = 1\n        weight                         = 1\n        private_link = {\n          pl = {\n            request_message        = \"Please approve this private link connection\"\n            target_type            = \"blob\"\n            location               = azurerm_storage_account.storage.location\n            private_link_target_id = azurerm_storage_account.storage.id\n          }\n        }\n      }\n    }\n  ```\n"
  default     = {}
}

variable "front_door_routes" {
  type        = map(object({
    name                      = string
    origin_group_key          = string
    origin_keys               = list(string)
    endpoint_key              = string
    forwarding_protocol       = optional(string, "HttpsOnly")
    supported_protocols       = list(string)
    patterns_to_match         = list(string)
    link_to_default_domain    = optional(bool, true)
    https_redirect_enabled    = optional(bool, true)
    custom_domain_keys        = optional(list(string), [])
    enabled                   = optional(bool, true)
    rule_set_names            = optional(list(string))
    cdn_frontdoor_origin_path = optional(string, null)
    cache = optional(map(object({
      query_string_caching_behavior = optional(string, "IgnoreQueryString")
      query_strings                 = optional(list(string))
      compression_enabled           = optional(bool, false)
      content_types_to_compress     = optional(list(string))
    })), {})
  }))
  description = "  Manages a Front Door (standard/premium) Route.\n  \n  - `name` - (Required) The name which should be used for this Front Door Route. Valid values must begin with a letter or number, end with a letter or number and may only contain letters, numbers and hyphens with a maximum length of 90 characters.\n  - 'origin_group_name' - (Required) The name of the origin group to associate the route with.\n  - `origin_names` - (Required) The name of the origins to associate the route with.\n  - 'endpoint_name' - (Required) The name of the origins to associate the route with.\n  - 'forwarding_protocol' - (Optional) The Protocol that will be use when forwarding traffic to backends. Possible values are 'HttpOnly', 'HttpsOnly' or 'MatchRequest'. Defaults to 'MatchRequest'.\n  - 'patterns_to_match' - (Required) The route patterns of the rule.\n  - 'supported_protocols' - (Required) One or more Protocols supported by this Front Door Route. Possible values are 'Http' or 'Https'.\n  - 'https_redirect_enabled' - (Optional) Automatically redirect HTTP traffic to HTTPS traffic? Possible values are true or false. Defaults to true.\n  - 'link_to_default_domain' - (Optional) Should this Front Door Route be linked to the default endpoint? Possible values include true or false. Defaults to true.\n  - 'cache' - (Optional) A cache block as defined below:-\n      - 'query_string_caching_behavior' - (Optional) Defines how the Front Door Route will cache requests that include query strings. Possible values include 'IgnoreQueryString', 'IgnoreSpecifiedQueryStrings', 'IncludeSpecifiedQueryStrings' or 'UseQueryString'. Defaults to 'IgnoreQueryString'.\n      - 'query_strings' - (Optional) Query strings to include or ignore.\n      - 'compression_enabled' - (Optional) Is content compression enabled? Possible values are true or false. Defaults to false.\n      - 'content_types_to_compress' - (Optional) A list of one or more Content types (formerly known as MIME types) to compress. Possible values include 'application/eot', 'application/font', 'application/font-sfnt', 'application/javascript', 'application/json', 'application/opentype', 'application/otf', 'application/pkcs7-mime', 'application/truetype', 'application/ttf', 'application/vnd.ms-fontobject', 'application/xhtml+xml', 'application/xml', 'application/xml+rss', 'application/x-font-opentype', 'application/x-font-truetype', 'application/x-font-ttf', 'application/x-httpd-cgi', 'application/x-mpegurl', 'application/x-opentype', 'application/x-otf', 'application/x-perl', 'application/x-ttf', 'application/x-javascript', 'font/eot', 'font/ttf', 'font/otf', 'font/opentype', 'image/svg+xml', 'text/css', 'text/csv', 'text/html', 'text/javascript', 'text/js', 'text/plain', 'text/richtext', 'text/tab-separated-values', 'text/xml', 'text/x-script', 'text/x-component' or 'text/x-java-source'.\n  Example Input:\n\n  ```terraform\n  front_door_routes = {\n    route1_key = {\n      name                   = \"route1\"\n      endpoint_key           = \"ep1_key\"\n      origin_group_key       = \"og1_key\"\n      origin_keys            = [\"origin1_key\"]\n      https_redirect_enabled = true\n      patterns_to_match      = [\"/*\"]\n      supported_protocols    = [\"Http\", \"Https\"]\n      rule_set_names         = [\"ruleset1\"]\n      cache = {\n        cache1 = {\n          query_string_caching_behavior = \"IgnoreSpecifiedQueryStrings\"\n          query_strings                 = [\"account\", \"settings\"]\n          compression_enabled           = true\n          content_types_to_compress     = [\"text/html\", \"text/javascript\", \"text/xml\"]\n        }\n      }\n    }\n  }\n  ```\n"
  default     = {}
}

variable "front_door_rule_sets" {
  type        = set(string)
  description = "  Manages a Front Door (standard/premium) Rule Sets.. The following properties can be specified:\n  - `name` - (Required) The name which should be used for this Front Door Rule Set.\n"
  default     = []
}

variable "front_door_rules" {
  type        = map(object({
    name              = string
    order             = number
    origin_group_key  = string
    rule_set_name     = string
    behavior_on_match = optional(string, "Continue")

    actions = object({
      url_rewrite_actions = optional(list(object({
        source_pattern          = string
        destination             = string
        preserve_unmatched_path = optional(bool, false)
      })), [])
      url_redirect_actions = optional(list(object({
        redirect_type        = string
        destination_hostname = string
        redirect_protocol    = optional(string, "Https")
        destination_path     = optional(string, "")
        query_string         = optional(string, "")
        destination_fragment = optional(string, "")
      })), [])
      route_configuration_override_actions = optional(list(object({
        set_origin_groupid            = bool
        cache_duration                = optional(string) #d.HH:MM:SS (365.23:59:59)
        forwarding_protocol           = optional(string, "HttpsOnly")
        query_string_caching_behavior = optional(string)
        query_string_parameters       = optional(list(string))
        compression_enabled           = optional(bool, false)
        cache_behavior                = optional(string)
      })), [])
      request_header_actions = optional(list(object({
        header_action = string
        header_name   = string
        value         = optional(string)
      })), [])
      response_header_actions = optional(list(object({
        header_action = string
        header_name   = string
        value         = optional(string)
      })), [])
    })
    conditions = optional(object({
      remote_address_conditions = optional(list(object({
        operator         = optional(string, "IPMatch")
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
      })), [])
      request_method_conditions = optional(list(object({
        match_values     = list(string)
        operator         = optional(string, "Equal")
        negate_condition = optional(bool, false)
      })), [])
      query_string_conditions = optional(list(object({
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      })), [])
      post_args_conditions = optional(list(object({
        post_args_name   = string
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      })), [])
      request_uri_conditions = optional(list(object({
        operator         = string
        negate_condition = optional(bool)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      })), [])
      request_header_conditions = optional(list(object({
        header_name      = string
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      })), [])
      request_body_conditions = optional(list(object({
        operator         = string
        match_values     = list(string)
        negate_condition = optional(bool, false)
        transforms       = optional(list(string))
      })), [])
      request_scheme_conditions = optional(list(object({
        operator         = optional(string, "Equal")
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
      })), [])
      url_path_conditions = optional(list(object({
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      })), [])
      url_file_extension_conditions = optional(list(object({
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = list(string)
        transforms       = optional(list(string))
      })), [])
      url_filename_conditions = optional(list(object({
        operator         = string
        match_values     = optional(list(string))
        negate_condition = optional(bool, false)
        transforms       = optional(list(string))
      })), [])
      http_version_conditions = optional(list(object({
        operator         = optional(string, "Equal")
        match_values     = list(string)
        negate_condition = optional(bool, false)
      })), [])
      cookies_conditions = optional(list(object({
        cookie_name      = string
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
        transforms       = optional(list(string))
      })), [])
      is_device_conditions = optional(list(object({
        operator         = optional(string)
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
      })), [])
      socket_address_conditions = optional(list(object({
        operator         = optional(string, "IPMatch")
        negate_condition = optional(bool, false)
        match_values     = optional(list(string))
      })), [])
      client_port_conditions = optional(list(object({
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = optional(list(number))
      })), [])
      server_port_conditions = optional(list(object({
        operator         = string
        negate_condition = optional(bool, false)
        match_values     = list(number)
      })), [])
      host_name_conditions = optional(list(object({
        operator         = string
        match_values     = optional(list(string))
        transforms       = optional(list(string))
        negate_condition = optional(bool, false)
      })), [])
      ssl_protocol_conditions = optional(list(object({
        match_values     = list(string)
        operator         = optional(string, "Equal")
        negate_condition = optional(bool, false)
      })), [])
    }))
  }))
  description = "  Manages a Front Door (standard/premium) Rules. Please review documentation here for usage. https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/cdn_frontdoor_rule\n  \n  Example Input:\n\n  ```terraform\n  front_door_rules = {\n    rule1_key = {\n      name              = \"examplerule1\"\n      order             = 1\n      behavior_on_match = \"Continue\"\n      rule_set_name     = \"ruleset1\"\n      origin_group_key  = \"og1_key\"\n      actions = {\n\n        url_rewrite_actions = [{\n          source_pattern          = \"/\"\n          destination             = \"/index3.html\"\n          preserve_unmatched_path = false\n        }]\n        route_configuration_override_actions = [{\n          set_origin_groupid            = true\n          forwarding_protocol           = \"HttpsOnly\"\n          query_string_caching_behavior = \"IncludeSpecifiedQueryStrings\"\n          query_string_parameters       = [\"foo\", \"clientIp={client_ip}\"]\n          compression_enabled           = true\n          cache_behavior                = \"OverrideIfOriginMissing\"\n          cache_duration                = \"365.23:59:59\"\n        }]\n        response_header_actions = [{\n          header_action = \"Append\"\n          header_name   = \"headername\"\n          value         = \"/abc\"\n        }]\n        request_header_actions = [{\n          header_action = \"Append\"\n          header_name   = \"headername\"\n          value         = \"/abc\"\n        }]\n      }\n\n      conditions = {\n        remote_address_conditions = [{\n          operator         = \"IPMatch\"\n          negate_condition = false\n          match_values     = [\"10.0.0.0/23\"]\n        }]\n\n        query_string_conditions = [{\n          negate_condition = false\n          operator         = \"BeginsWith\"\n          match_values     = [\"Query1\", \"Query2\"]\n          transforms       = [\"Uppercase\"]\n        }]\n\n        request_header_conditions = [{\n          header_name      = \"headername\"\n          negate_condition = false\n          operator         = \"BeginsWith\"\n          match_values     = [\"Header1\", \"Header2\"]\n          transforms       = [\"Uppercase\"]\n        }]\n\n        request_body_conditions = [{\n          negate_condition = false\n          operator         = \"BeginsWith\"\n          match_values     = [\"Body1\", \"Body2\"]\n          transforms       = [\"Uppercase\"]\n        }]\n\n        request_scheme_conditions = [{ #request protocol\n          negate_condition = false\n          operator         = \"Equal\"\n          match_values     = [\"HTTP\"]\n        }]\n\n        url_path_conditions = [{\n          negate_condition = false\n          operator         = \"BeginsWith\"\n          match_values     = [\"UrlPath1\", \"UrlPath2\"]\n          transforms       = [\"Uppercase\"]\n        }]\n\n        url_file_extension_conditions = [{\n          negate_condition = false\n          operator         = \"BeginsWith\"\n          match_values     = [\"ext1\", \"ext2\"]\n          transforms       = [\"Uppercase\"]\n        }]\n\n        url_filename_conditions = [{\n          negate_condition = false\n          operator         = \"BeginsWith\"\n          match_values     = [\"filename1\", \"filename2\"]\n          transforms       = [\"Uppercase\"]\n        }]\n\n        http_version_conditions = [{\n          negate_condition = false\n          operator         = \"Equal\"\n          match_values     = [\"2.0\"]\n        }]\n\n        cookies_conditions = [{\n          cookie_name      = \"cookie\"\n          negate_condition = false\n          operator         = \"BeginsWith\"\n          match_values     = [\"cookie1\", \"cookie2\"]\n          transforms       = [\"Uppercase\"]\n        }]\n      }\n    }\n  }\n  ```\n"
  default     = {}
}

variable "front_door_secrets" {
  type        = map(object({
    name                     = string
    key_vault_certificate_id = string
  }))
  description = "  Manages a Front Door (standard/premium) Secret.\n  \n  - `name` - (Required) The name which should be used for this Front Door Secret. \n  - `key_vault_certificate_id` - (Required) The ID of the Key Vault certificate resource to use.\n  Example Input:\n\n  ```terraform\n  front_door_secrets = {\n    secret1_key = {\n      name                     = \"Front-door-certificate\"\n      key_vault_certificate_id = azurerm_key_vault_certificate.keyvaultcert.versionless_id\n    }\n  }\n  ```\n"
  default     = {}
}

variable "front_door_security_policies" {
  type        = map(object({
    name = string
    firewall = object({
      front_door_firewall_policy_key = string
      association = object({
        domain_keys       = optional(list(string), [])
        endpoint_keys     = optional(list(string), [])
        patterns_to_match = list(string)
      })
    })
  }))
  description = "  Manages a Front Door (standard/premium) Security Policy.\n  \n  - `name` - (Required) The name which should be used for this Front Door Security Policy. Possible values must not be an empty string.\n  - `firewall` - (Required) An firewall block as defined below: -\n  - 'front_door_firewall_policy_name' - (Required) the name of Front Door Firewall Policy that should be linked to this Front Door Security Policy.\n  - 'association' - (Required) An association block as defined below:-\n  - 'domain_names' - (Optional) list of the domain names to associate with the firewall policy. Provide either domain names or endpoint names or both.\n  - 'endpoint_names' - (Optional) list of the endpoint names to associate with the firewall policy. Provide either domain names or endpoint names or both.\n  - 'patterns_to_match' - (Required) The list of paths to match for this firewall policy. Possible value includes /*\n  Example Input:\n\n  ```terraform\n  front_door_security_policies = {\n    secpol1_key = {\n      name = \"firewallpolicyforep1cd1\"\n      firewall = {\n        front_door_firewall_policy_key = \"fd_waf1_key\"\n        association = {\n          endpoint_keys     = [\"ep1_key\"]\n          domain_keys       = [\"cd1_key\"]\n          patterns_to_match = [\"/*\"]\n        }\n      }\n    }\n  }\n  ```\n"
  default     = {}
}

variable "location" {
  type        = string
  description = "The Azure location where the resources will be deployed."
  default     = ""
}

variable "lock" {
  type        = object({
    kind = string
    name = optional(string, null)
  })
  description = "  Controls the Resource Lock configuration for this resource. The following properties can be specified:\n  \n  - `kind` - (Required) The type of lock. Possible values are `\\"CanNotDelete\\"` and `\\"ReadOnly\\"`.\n  - `name` - (Optional) The name of the lock. If not specified, a name will be generated based on the `kind` value. Changing this forces the creation of a new resource.\n  Example Input:\n\n  ```terraform\n  lock = {\n      name = \"lock-cdnprofile\"\n      kind = \"CanNotDelete\"\n  }\n  ```\n"
  default     = null
}

variable "managed_identities" {
  type        = object({
    system_assigned            = optional(bool, false)
    user_assigned_resource_ids = optional(set(string), [])
  })
  description = "  Controls the Managed Identity configuration on this resource. The following properties can be specified:\n  \n  - `system_assigned` - (Optional) Specifies if the System Assigned Managed Identity should be enabled.\n  - `user_assigned_resource_ids` - (Optional) Specifies a list of User Assigned Managed Identity resource IDs to be assigned to this resource.\n  Example Input:\n\n  ```terraform\n  managed_identities = {\n    system_assigned = true\n    user_assigned_resource_ids = [\n      azurerm_user_assigned_identity.identity_for_keyvault.id\n    ]\n  }\n  ```\n"
  default     = {}
}

variable "name" {
  type        = string
  description = "The name of the Azure Front Door."
  default     = ""
}

variable "resource_group_name" {
  type        = string
  description = "The resource group where the resources will be deployed."
  default     = ""
}

variable "response_timeout_seconds" {
  type        = number
  description = "Specifies the maximum response timeout in seconds. Possible values are between 16 and 240 seconds (inclusive). Defaults to 120 seconds. "
  default     = 120
}

variable "role_assignments" {
  type        = map(object({
    role_definition_id_or_name             = string
    principal_id                           = string
    description                            = optional(string, null)
    skip_service_principal_aad_check       = optional(bool, false) #Note: only set `skip_service_principal_aad_check` to true if you are assigning a role to a service principal.
    condition                              = optional(string, null)
    condition_version                      = optional(string, null)
    delegated_managed_identity_resource_id = optional(string, null)
    principal_type                         = optional(string, null)
  }))
  description = "  A map of role assignments to create on the <RESOURCE>. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.\n  \n  - `role_definition_id_or_name` - The ID or name of the role definition to assign to the principal.\n  - `principal_id` - The ID of the principal to assign the role to.\n  - `description` - (Optional) The description of the role assignment.\n  - `skip_service_principal_aad_check` - (Optional) If set to true, skips the Azure Active Directory check for the service principal in the tenant. Defaults to false.\n  - `condition` - (Optional) The condition which will be used to scope the role assignment.\n  - `condition_version` - (Optional) The version of the condition syntax. Leave as `null` if you are not using a condition, if you are then valid values are '2.0'.\n  - `delegated_managed_identity_resource_id` - (Optional) The delegated Azure Resource Id which contains a Managed Identity. Changing this forces a new resource to be created. This field is only used in cross-tenant scenario.\n  - `principal_type` - (Optional) The type of the `principal_id`. Possible values are `User`, `Group` and `ServicePrincipal`. It is necessary to explicitly set this attribute when creating role assignments if the principal creating the assignment is constrained by ABAC rules that filters on the PrincipalType attribute.\n  \n  > Note: only set `skip_service_principal_aad_check` to true if you are assigning a role to a service principal.\n  Example Input:\n\n  ```terraform\n  role_assignments = {\n    role_assignment_2 = {\n      role_definition_id_or_name       = \"Reader\"\n      principal_id                     = data.azurerm_client_config.current.object_id #\"125****-c***-4f**-**0d-******53b5**\" \n      description                      = \"Example role assignment 2 of reader role\"\n      skip_service_principal_aad_check = false\n      principal_type                   = \"ServicePrincipal\"\n      condition                        = \"@Resource[Microsoft.Storage/storageAccounts/blobServices/containers:ContainerName] StringEqualsIgnoreCase 'foo_storage_container'\"\n      condition_version                = \"2.0\"\n    }\n  }\n  ```\n"
  default     = {}
}

variable "sku" {
  type        = string
  description = "The SKU name of the Azure Front Door. Default is `Standard`. Possible values are `standard` and `premium`.SKU name for CDN can be 'Standard_Akamai', 'Standard_ChinaCdn, 'Standard_Microsoft','Standard_Verizon' or 'Premium_Verizon'"
  default     = "Standard_AzureFrontDoor"
}

variable "tags" {
  type        = map(string)
  description = "Map of tags to assign to the Azure Front Door resource."
  default     = null
}


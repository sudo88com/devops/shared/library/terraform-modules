
output "cdn_endpoints" {
  description = "CDN endpoint output object"
  value       = module.avm-res-cdn-profile.cdn_endpoints
}

output "frontdoor_custom_domains" {
  description = "Azure front door custom domains output object"
  value       = module.avm-res-cdn-profile.frontdoor_custom_domains
}

output "frontdoor_endpoints" {
  description = "Azure front door endpoint output object"
  value       = module.avm-res-cdn-profile.frontdoor_endpoints
}

output "frontdoor_firewall_policies" {
  description = "Azure front door firewall policies output object"
  value       = module.avm-res-cdn-profile.frontdoor_firewall_policies
}

output "frontdoor_origin_groups" {
  description = "Azure front door origin groups output object"
  value       = module.avm-res-cdn-profile.frontdoor_origin_groups
}

output "frontdoor_origins" {
  description = "Azure front door origins output object"
  value       = module.avm-res-cdn-profile.frontdoor_origins
}

output "frontdoor_rule_sets" {
  description = "Azure front door rule sets output object"
  value       = module.avm-res-cdn-profile.frontdoor_rule_sets
}

output "frontdoor_rules" {
  description = "Azure front door rules output object"
  value       = module.avm-res-cdn-profile.frontdoor_rules
}

output "frontdoor_security_policies" {
  description = "Azure front door security policies output object"
  value       = module.avm-res-cdn-profile.frontdoor_security_policies
}

output "resource" {
  description = "Full resource output object"
  value       = module.avm-res-cdn-profile.resource
}

output "resource_id" {
  description = "The resource id of the Front door profile"
  value       = module.avm-res-cdn-profile.resource_id
}

output "resource_name" {
  description = "The resource name of the Front door profile"
  value       = module.avm-res-cdn-profile.resource_name
}

output "system_assigned_mi_principal_id" {
  description = "The system assigned managed identity of the front door profile"
  value       = module.avm-res-cdn-profile.system_assigned_mi_principal_id
}


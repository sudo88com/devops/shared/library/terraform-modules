
variable "azure_devops_org_name" {
  type        = string
  description = "The name of the Azure DevOps organization in which the containerized agents will be deployed (e.g. https://dev.azure.com/YOUR_ORGANIZATION_NAME, must exist)"
  default     = ""
}

variable "azure_devops_personal_access_token" {
  type        = string
  description = "The personal access token to use to connect to Azure DevOps (see https://docs.microsoft.com/en-us/azure/devops/pipelines/agents/v2-windows?view=azure-devops#permissions)"
  default     = ""
}

variable "create_resource_group" {
  type        = bool
  description = "(Optional) A flag that indicates if the resource group in which the agents will be deployed must be created (true) or imported (false)."
  default     = true
}

variable "enable_vnet_integration" {
  type        = bool
  description = "(Optional) A flag that indicates if the containerized agents must be deployed into an existing virtual network"
  default     = false
}

variable "image_registry_credential" {
  type        = object({
    username = string,
    password = string,
    server   = string
  })
  description = "(Optional) The credentials to use to connect to the Docker private registry where agent images are stored."
  default     = {
  "password": "",
  "server": "",
  "username": ""
}
}

variable "linux_agents_configuration" {
  type        = object({
    count                        = string,
    docker_image                 = string,
    docker_tag                   = string,
    agent_name_prefix            = string,
    agent_pool_name              = string,
    cpu                          = string,
    memory                       = string,
    user_assigned_identity_ids   = list(string),
    use_system_assigned_identity = bool
  })
  description = "(Optional) The configuration of the Linux agents to deploy"
  default     = {
  "agent_name_prefix": "",
  "agent_pool_name": "",
  "count": 0,
  "cpu": "1",
  "docker_image": "",
  "docker_tag": "",
  "memory": "2",
  "use_system_assigned_identity": false,
  "user_assigned_identity_ids": []
}
}

variable "location" {
  type        = string
  description = "The Azure location to use for deployment"
  default     = ""
}

variable "resource_group_name" {
  type        = string
  description = "The name of the resource group in which the containerized agents will be deployed"
  default     = ""
}

variable "subnet_name" {
  type        = string
  description = "(Optional) The name of the subnet of the vnet in which the containerized agents will be deployed"
  default     = ""
}

variable "vnet_name" {
  type        = string
  description = "(Optional) The name of the virtual network in which the containerized agents will be deployed"
  default     = ""
}

variable "vnet_resource_group_name" {
  type        = string
  description = "(Optional) The name of the resource group that contains the virtual network in which the containerized agents will be deployed"
  default     = ""
}

variable "windows_agents_configuration" {
  type        = object({
    count             = string,
    docker_image      = string,
    docker_tag        = string,
    agent_name_prefix = string,
    agent_pool_name   = string,
    cpu               = string,
    memory            = string
  })
  description = "(Optional) The configuration of the Windows agents to deploy"
  default     = {
  "agent_name_prefix": "",
  "agent_pool_name": "",
  "count": 0,
  "cpu": "1",
  "docker_image": "",
  "docker_tag": "",
  "memory": "2"
}
}


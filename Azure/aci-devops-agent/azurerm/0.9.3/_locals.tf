
locals {
  azure_devops_org_name = var.azure_devops_org_name
  azure_devops_personal_access_token = var.azure_devops_personal_access_token
  create_resource_group = var.create_resource_group
  enable_vnet_integration = var.enable_vnet_integration
  image_registry_credential = var.image_registry_credential
  linux_agents_configuration = var.linux_agents_configuration
  location = var.location
  resource_group_name = var.resource_group_name
  subnet_name = var.subnet_name
  vnet_name = var.vnet_name
  vnet_resource_group_name = var.vnet_resource_group_name
  windows_agents_configuration = var.windows_agents_configuration
}

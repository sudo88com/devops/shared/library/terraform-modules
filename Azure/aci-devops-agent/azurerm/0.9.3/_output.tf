
output "has_linux_agents" {
  description = "A flag that indicates if Linux agents have been deployed with the current configuration."
  value       = module.aci-devops-agent.has_linux_agents
}

output "has_windows_agents" {
  description = "A flag that indicates if Windows agents have been deployed with the current configuration."
  value       = module.aci-devops-agent.has_windows_agents
}

output "linux_agents_count" {
  description = "A number that indicates the number of Linux agents that have been deployed with the current configuration."
  value       = module.aci-devops-agent.linux_agents_count
}

output "linux_agents_names" {
  description = "An array that contains the names of Linux agents that have been deployed with the current configuration."
  value       = module.aci-devops-agent.linux_agents_names
}

output "subnet_id" {
  description = "If the vnet integration is enabled, the id of the subnet in which the agents are deployed."
  value       = module.aci-devops-agent.subnet_id
}

output "vnet_integration_enabled" {
  description = "A flag that indicates if the vnet integration has been enabled with the current configuration."
  value       = module.aci-devops-agent.vnet_integration_enabled
}

output "windows_agents_count" {
  description = "A number that indicates the number of Windows agents that have been deployed with the current configuration."
  value       = module.aci-devops-agent.windows_agents_count
}

output "windows_agents_names" {
  description = "An array that contains the names of Windows agents that have been deployed with the current configuration."
  value       = module.aci-devops-agent.windows_agents_names
}


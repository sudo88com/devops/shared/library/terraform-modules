
azure_devops_org_name = 

azure_devops_personal_access_token = 

create_resource_group = true

enable_vnet_integration = false

image_registry_credential = {
  "password": "",
  "server": "",
  "username": ""
}

linux_agents_configuration = {
  "agent_name_prefix": "",
  "agent_pool_name": "",
  "count": 0,
  "cpu": "1",
  "docker_image": "",
  "docker_tag": "",
  "memory": "2",
  "use_system_assigned_identity": false,
  "user_assigned_identity_ids": []
}

location = 

resource_group_name = 

subnet_name = ""

vnet_name = ""

vnet_resource_group_name = ""

windows_agents_configuration = {
  "agent_name_prefix": "",
  "agent_pool_name": "",
  "count": 0,
  "cpu": "1",
  "docker_image": "",
  "docker_tag": "",
  "memory": "2"
}


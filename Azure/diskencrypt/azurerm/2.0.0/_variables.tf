
variable "encrypt_operation" {
  type        = 
  description = ""
  default     = "EnableEncryption"
}

variable "encryption_algorithm" {
  type        = 
  description = " Algo for encryption"
  default     = "RSA-OAEP"
}

variable "encryption_key_url" {
  type        = 
  description = "URL to encrypt Key"
  default     = ""
}

variable "key_vault_name" {
  type        = 
  description = "Name of the keyVault"
  default     = "testkeyVault123"
}

variable "location" {
  type        = 
  description = "The location/region where the core network will be created. The full list of Azure regions can be found at https://azure.microsoft.com/regions"
  default     = "South Central US"
}

variable "resource_group_name" {
  type        = 
  description = "Default resource group name that the network will be created in"
  default     = "vjresourcegroup"
}

variable "tags" {
  type        = 
  description = "The tags to associate with your resources"
  default     = {
  "tag1": "",
  "tag2": ""
}
}

variable "type_handler_version" {
  type        = 
  description = "Type handler version of the VM extension to use. Defaults to 2.2 on Windows and 1.1 on Linux"
  default     = ""
}

variable "vm_name" {
  type        = 
  description = "Name of the VM to encrypt"
  default     = "ubuntu1"
}

variable "vm_os_type" {
  type        = 
  description = "Type of OS. Allowed values are Windows and Linux. Defaults to Linux"
  default     = "linux"
}

variable "volume_type" {
  type        = 
  description = ""
  default     = "All"
}


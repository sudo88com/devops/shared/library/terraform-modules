
locals {
  encrypt_operation = var.encrypt_operation
  encryption_algorithm = var.encryption_algorithm
  encryption_key_url = var.encryption_key_url
  key_vault_name = var.key_vault_name
  location = var.location
  resource_group_name = var.resource_group_name
  tags = var.tags
  type_handler_version = var.type_handler_version
  vm_name = var.vm_name
  vm_os_type = var.vm_os_type
  volume_type = var.volume_type
}

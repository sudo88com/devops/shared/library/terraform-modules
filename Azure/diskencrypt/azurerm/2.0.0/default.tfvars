
encrypt_operation = "EnableEncryption"

encryption_algorithm = "RSA-OAEP"

encryption_key_url = ""

key_vault_name = "testkeyVault123"

location = "South Central US"

resource_group_name = "vjresourcegroup"

tags = {
  "tag1": "",
  "tag2": ""
}

type_handler_version = ""

vm_name = "ubuntu1"

vm_os_type = "linux"

volume_type = "All"


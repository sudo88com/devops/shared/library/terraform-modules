
output "virtual_id" {
  description = "The id of the disk encryption VM extension applied."
  value       = module.diskencrypt.virtual_id
}


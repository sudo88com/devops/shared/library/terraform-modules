
output "name" {
  description = "The name of the static site."
  value       = module.avm-res-web-staticsite.name
}

output "resource" {
  description = "This is the full output for the resource."
  value       = module.avm-res-web-staticsite.resource
}

output "resource_id" {
  description = "The ID of the static site."
  value       = module.avm-res-web-staticsite.resource_id
}

output "resource_private_endpoints" {
  description = "A map of private endpoints. The map key is the supplied input to var.private_endpoints. The map value is the entire azurerm_private_endpoint resource."
  value       = module.avm-res-web-staticsite.resource_private_endpoints
}

output "resource_uri" {
  description = "The default hostname of the static web app."
  value       = module.avm-res-web-staticsite.resource_uri
}


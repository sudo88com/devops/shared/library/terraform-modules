
account_name = ""

application_name = ""

custom_subdomain_name = ""

customer_managed_key = null

default_tags_enabled = false

deployment = {}

diagnostic_setting = {}

dynamic_throttling_enabled = null

environment = ""

fqdns = null

identity = null

local_auth_enabled = true

location = 

network_acls = null

outbound_network_access_restricted = false

pe_subresource = [
  "account"
]

private_dns_zone = null

private_endpoint = {}

public_network_access_enabled = false

resource_group_name = 

sku_name = "S0"

tags = {}

tracing_tags_enabled = false

tracing_tags_prefix = "avm_"


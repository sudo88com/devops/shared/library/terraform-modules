
variable "account_name" {
  type        = string
  description = "Specifies the name of the Cognitive Service Account. Changing this forces a new resource to be created. Leave this variable as default would use a default name with random suffix."
  default     = ""
}

variable "application_name" {
  type        = string
  description = "Name of the application. A corresponding tag would be created on the created resources if `var.default_tags_enabled` is `true`."
  default     = ""
}

variable "custom_subdomain_name" {
  type        = string
  description = "The subdomain name used for token-based authentication. Changing this forces a new resource to be created. Leave this variable as default would use a default name with random suffix."
  default     = ""
}

variable "customer_managed_key" {
  type        = object({
    key_vault_key_id   = string
    identity_client_id = optional(string)
  })
  description = "type = object({\n  key_vault_key_id   = (Required) The ID of the Key Vault Key which should be used to Encrypt the data in this OpenAI Account.\n  identity_client_id = (Optional) The Client ID of the User Assigned Identity that has access to the key. This property only needs to be specified when there're multiple identities attached to the OpenAI Account.\n})\n"
  default     = null
}

variable "default_tags_enabled" {
  type        = bool
  description = "Determines whether or not default tags are applied to resources. If set to true, tags will be applied. If set to false, tags will not be applied."
  default     = false
}

variable "deployment" {
  type        = map(object({
    name                   = string
    model_format           = string
    model_name             = string
    model_version          = string
    scale_type             = string
    rai_policy_name        = optional(string)
    capacity               = optional(number)
    version_upgrade_option = optional(string)
  }))
  description = "type = map(object({\n  name                 = (Required) The name of the Cognitive Services Account Deployment. Changing this forces a new resource to be created.\n  cognitive_account_id = (Required) The ID of the Cognitive Services Account. Changing this forces a new resource to be created.\n  model = {\n    model_format  = (Required) The format of the Cognitive Services Account Deployment model. Changing this forces a new resource to be created. Possible value is OpenAI.\n    model_name    = (Required) The name of the Cognitive Services Account Deployment model. Changing this forces a new resource to be created.\n    model_version = (Required) The version of Cognitive Services Account Deployment model.\n  }\n  scale = {\n    scale_type = (Required) Deployment scale type. Possible value is Standard. Changing this forces a new resource to be created.\n  }\n  rai_policy_name = (Optional) The name of RAI policy. Changing this forces a new resource to be created.\n  capacity = (Optional) Tokens-per-Minute (TPM). The unit of measure for this field is in the thousands of Tokens-per-Minute. Defaults to 1 which means that the limitation is 1000 tokens per minute.\n  version_upgrade_option = (Optional) Deployment model version upgrade option. Possible values are `OnceNewDefaultVersionAvailable`, `OnceCurrentVersionExpired`, and `NoAutoUpgrade`. Defaults to `OnceNewDefaultVersionAvailable`. Changing this forces a new resource to be created.\n}))\n"
  default     = {}
}

variable "diagnostic_setting" {
  type        = map(object({
    name                           = string
    log_analytics_workspace_id     = optional(string)
    log_analytics_destination_type = optional(string)
    eventhub_name                  = optional(string)
    eventhub_authorization_rule_id = optional(string)
    storage_account_id             = optional(string)
    partner_solution_id            = optional(string)
    audit_log_retention_policy = optional(object({
      enabled = optional(bool, true)
      days    = optional(number, 7)
    }))
    request_response_log_retention_policy = optional(object({
      enabled = optional(bool, true)
      days    = optional(number, 7)
    }))
    trace_log_retention_policy = optional(object({
      enabled = optional(bool, true)
      days    = optional(number, 7)
    }))
    metric_retention_policy = optional(object({
      enabled = optional(bool, true)
      days    = optional(number, 7)
    }))
  }))
  description = "A map of objects that represent the configuration for a diagnostic setting.\"\ntype = map(object({\n  name                                  = (Required) Specifies the name of the diagnostic setting. Changing this forces a new resource to be created.\n  log_analytics_workspace_id            = (Optional) (Optional) Specifies the resource id of an Azure Log Analytics workspace where diagnostics data should be sent.\n  log_analytics_destination_type        = (Optional) Possible values are `AzureDiagnostics` and `Dedicated`. When set to Dedicated, logs sent to a Log Analytics workspace will go into resource specific tables, instead of the legacy `AzureDiagnostics` table.\n  eventhub_name                         = (Optional) Specifies the name of the Event Hub where diagnostics data should be sent.\n  eventhub_authorization_rule_id        = (Optional) Specifies the resource id of an Event Hub Namespace Authorization Rule used to send diagnostics data.\n  storage_account_id                    = (Optional) Specifies the resource id of an Azure storage account where diagnostics data should be sent.\n  partner_solution_id                   = (Optional) The resource id of the market partner solution where diagnostics data should be sent. For potential partner integrations, click to learn more about partner integration.\n  audit_log_retention_policy            = (Optional) Specifies the retention policy for the audit log. This is a block with the following properties:\n    enabled                             = (Optional) Specifies whether the retention policy is enabled. If enabled, `days` must be a positive number.\n    days                                = (Optional) Specifies the number of days to retain trace logs. If `enabled` is set to `true`, this value must be set to a positive number.\n  request_response_log_retention_policy = (Optional) Specifies the retention policy for the request response log. This is a block with the following properties:\n    enabled                             = (Optional) Specifies whether the retention policy is enabled. If enabled, `days` must be a positive number.\n    days                                = (Optional) Specifies the number of days to retain trace logs. If `enabled` is set to `true`, this value must be set to a positive number.\n  trace_log_retention_policy            = (Optional) Specifies the retention policy for the trace log. This is a block with the following properties:\n    enabled                             = (Optional) Specifies whether the retention policy is enabled. If enabled, `days` must be a positive number.\n    days                                = (Optional) Specifies the number of days to retain trace logs. If `enabled` is set to `true`, this value must be set to a positive number.\n  metric_retention_policy               = (Optional) Specifies the retention policy for the metric. This is a block with the following properties:\n    enabled                             = (Optional) Specifies whether the retention policy is enabled. If enabled, `days` must be a positive number.\n    days                                = (Optional) Specifies the number of days to retain trace logs. If `enabled` is set to `true`, this value must be set to a positive number.\n}))\n"
  default     = {}
}

variable "dynamic_throttling_enabled" {
  type        = bool
  description = "Determines whether or not dynamic throttling is enabled. If set to `true`, dynamic throttling will be enabled. If set to `false`, dynamic throttling will not be enabled."
  default     = null
}

variable "environment" {
  type        = string
  description = "Environment of the application. A corresponding tag would be created on the created resources if `var.default_tags_enabled` is `true`."
  default     = ""
}

variable "fqdns" {
  type        = list(string)
  description = "List of FQDNs allowed for the Cognitive Account."
  default     = null
}

variable "identity" {
  type        = object({
    type         = string
    identity_ids = optional(list(string))
  })
  description = "type = object({\n  type         = (Required) The type of the Identity. Possible values are `SystemAssigned`, `UserAssigned`, `SystemAssigned, UserAssigned`.\n  identity_ids = (Optional) Specifies a list of User Assigned Managed Identity IDs to be assigned to this OpenAI Account.\n})\n"
  default     = null
}

variable "local_auth_enabled" {
  type        = bool
  description = "Whether local authentication methods is enabled for the Cognitive Account. Defaults to `true`."
  default     = true
}

variable "location" {
  type        = string
  description = "Azure OpenAI deployment region. Set this variable to `null` would use resource group's location."
  default     = ""
}

variable "network_acls" {
  type        = set(object({
    default_action = string
    ip_rules       = optional(set(string))
    virtual_network_rules = optional(set(object({
      subnet_id                            = string
      ignore_missing_vnet_service_endpoint = optional(bool, false)
    })))
  }))
  description = "type = set(object({\n  default_action = (Required) The Default Action to use when no rules match from ip_rules / virtual_network_rules. Possible values are `Allow` and `Deny`.\n  ip_rules                    = (Optional) One or more IP Addresses, or CIDR Blocks which should be able to access the Cognitive Account.\n  virtual_network_rules = optional(set(object({\n    subnet_id                            = (Required) The ID of a Subnet which should be able to access the OpenAI Account.\n    ignore_missing_vnet_service_endpoint = (Optional) Whether ignore missing vnet service endpoint or not. Default to `false`.\n  })))\n}))\n"
  default     = null
}

variable "outbound_network_access_restricted" {
  type        = bool
  description = "Whether outbound network access is restricted for the Cognitive Account. Defaults to `false`."
  default     = false
}

variable "pe_subresource" {
  type        = list(string)
  description = "A list of subresource names which the Private Endpoint is able to connect to. `subresource_names` corresponds to `group_id`. Possible values are detailed in the product [documentation](https://docs.microsoft.com/azure/private-link/private-endpoint-overview#private-link-resource) in the `Subresources` column. Changing this forces a new resource to be created."
  default     = [
  "account"
]
}

variable "private_dns_zone" {
  type        = object({
    name                = string
    resource_group_name = optional(string)
  })
  description = "A map of object that represents the existing Private DNS Zone you'd like to use. Leave this variable as default would create a new Private DNS Zone.\ntype = object({\n  name                = \"(Required) The name of the Private DNS Zone.\"\n  resource_group_name = \"(Optional) The Name of the Resource Group where the Private DNS Zone exists. If the Name of the Resource Group is not provided, the first Private DNS Zone from the list of Private DNS Zones in your subscription that matches `name` will be returned.\"\n}\n"
  default     = null
}

variable "private_endpoint" {
  type        = map(object({
    name                               = string
    vnet_rg_name                       = string
    vnet_name                          = string
    subnet_name                        = string
    dns_zone_virtual_network_link_name = optional(string, "dns_zone_link")
    private_dns_entry_enabled          = optional(bool, false)
    private_service_connection_name    = optional(string, "privateserviceconnection")
    is_manual_connection               = optional(bool, false)
  }))
  description = "A map of objects that represent the configuration for a private endpoint.\"\ntype = map(object({\n  name                               = (Required) Specifies the Name of the Private Endpoint. Changing this forces a new resource to be created.\n  vnet_rg_name                       = (Required) Specifies the name of the Resource Group where the Private Endpoint's Virtual Network Subnet exists. Changing this forces a new resource to be created.\n  vnet_name                          = (Required) Specifies the name of the Virtual Network where the Private Endpoint's Subnet exists. Changing this forces a new resource to be created.\n  subnet_name                        = (Required) Specifies the name of the Subnet which Private IP Addresses will be allocated for this Private Endpoint. Changing this forces a new resource to be created.\n  dns_zone_virtual_network_link_name = (Optional) The name of the Private DNS Zone Virtual Network Link. Changing this forces a new resource to be created. Default to `dns_zone_link`.\n  private_dns_entry_enabled          = (Optional) Whether or not to create a `private_dns_zone_group` block for the Private Endpoint. Default to `false`.\n  private_service_connection_name    = (Optional) Specifies the Name of the Private Service Connection. Changing this forces a new resource to be created. Default to `privateserviceconnection`.\n  is_manual_connection               = (Optional) Does the Private Endpoint require Manual Approval from the remote resource owner? Changing this forces a new resource to be created. Default to `false`.\n}))\n"
  default     = {}
}

variable "public_network_access_enabled" {
  type        = bool
  description = "Whether public network access is allowed for the Cognitive Account. Defaults to `false`."
  default     = false
}

variable "resource_group_name" {
  type        = string
  description = "Name of the azure resource group to use. The resource group must exist."
  default     = ""
}

variable "sku_name" {
  type        = string
  description = "Specifies the SKU Name for this Cognitive Service Account. Possible values are `F0`, `F1`, `S0`, `S`, `S1`, `S2`, `S3`, `S4`, `S5`, `S6`, `P0`, `P1`, `P2`, `E0` and `DC0`. Default to `S0`."
  default     = "S0"
}

variable "tags" {
  type        = map(string)
  description = "(Optional) A mapping of tags to assign to the resource."
  default     = {}
}

variable "tracing_tags_enabled" {
  type        = bool
  description = "Whether enable tracing tags that generated by BridgeCrew Yor."
  default     = false
}

variable "tracing_tags_prefix" {
  type        = string
  description = "Default prefix for generated tracing tags"
  default     = "avm_"
}


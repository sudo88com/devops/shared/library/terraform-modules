
module "avm-res-desktopvirtualization-scalingplan" {
  source = "terraform-aws-modules/avm-res-desktopvirtualization-scalingplan/aws"
  version = "0.1.2"
  diagnostic_settings = var.diagnostic_settings
  enable_telemetry = var.enable_telemetry
  lock = var.lock
  role_assignments = var.role_assignments
  tags = var.tags
  virtual_desktop_scaling_plan_description = var.virtual_desktop_scaling_plan_description
  virtual_desktop_scaling_plan_exclusion_tag = var.virtual_desktop_scaling_plan_exclusion_tag
  virtual_desktop_scaling_plan_friendly_name = var.virtual_desktop_scaling_plan_friendly_name
  virtual_desktop_scaling_plan_host_pool = var.virtual_desktop_scaling_plan_host_pool
  virtual_desktop_scaling_plan_location = var.virtual_desktop_scaling_plan_location
  virtual_desktop_scaling_plan_name = var.virtual_desktop_scaling_plan_name
  virtual_desktop_scaling_plan_resource_group_name = var.virtual_desktop_scaling_plan_resource_group_name
  virtual_desktop_scaling_plan_schedule = var.virtual_desktop_scaling_plan_schedule
  virtual_desktop_scaling_plan_tags = var.virtual_desktop_scaling_plan_tags
  virtual_desktop_scaling_plan_time_zone = var.virtual_desktop_scaling_plan_time_zone
  virtual_desktop_scaling_plan_timeouts = var.virtual_desktop_scaling_plan_timeouts
}

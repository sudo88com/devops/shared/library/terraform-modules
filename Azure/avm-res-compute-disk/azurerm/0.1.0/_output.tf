
output "location" {
  description = "The deployment region."
  value       = module.avm-res-compute-disk.location
}

output "private_endpoints" {
  description = "A map of the private endpoints created.\n"
  value       = module.avm-res-compute-disk.private_endpoints
}

output "resource" {
  description = "This is the full output for the resource."
  value       = module.avm-res-compute-disk.resource
}

output "resource_group_name" {
  description = "The name of the Resource Group."
  value       = module.avm-res-compute-disk.resource_group_name
}



create_option = 

customer_managed_key = null

disk_access_id = null

disk_encryption_set_id = null

disk_iops_read_only = null

disk_iops_read_write = null

disk_mbps_read_only = null

disk_mbps_read_write = null

disk_size_gb = null

edge_zone = null

enable_telemetry = true

encryption_settings = null

gallery_image_reference_id = null

hyper_v_generation = null

image_reference_id = null

location = 

lock = null

logical_sector_size = null

max_shares = null

name = 

network_access_policy = null

on_demand_bursting_enabled = null

optimized_frequent_attach_enabled = null

os_type = null

performance_plus_enabled = null

private_endpoints = {}

private_endpoints_manage_dns_zone_group = true

public_network_access_enabled = null

resource_group_name = 

role_assignments = {}

secure_vm_disk_encryption_set_id = null

security_type = null

source_resource_id = null

source_uri = null

storage_account_id = null

storage_account_type = 

tags = null

tier = null

trusted_launch_enabled = null

upload_size_bytes = null

zone = null


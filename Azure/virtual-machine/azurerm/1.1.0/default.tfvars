
additional_unattend_contents = []

admin_password = null

admin_ssh_keys = []

admin_username = "azureuser"

allow_extension_operations = false

automatic_updates_enabled = true

availability_set_id = null

boot_diagnostics = false

boot_diagnostics_storage_account_uri = null

bypass_platform_safety_checks_on_user_schedule_enabled = false

capacity_reservation_group_id = null

computer_name = null

custom_data = null

data_disks = []

dedicated_host_group_id = null

dedicated_host_id = null

disable_password_authentication = true

edge_zone = null

encryption_at_host_enabled = null

eviction_policy = null

extensions = []

extensions_time_budget = "PT1H30M"

gallery_application = []

hotpatching_enabled = false

identity = null

image_os = 

license_type = null

location = 

max_bid_price = -1

name = 

network_interface_ids = null

new_boot_diagnostics_storage_account = null

new_network_interface = {
  "accelerated_networking_enabled": null,
  "dns_servers": null,
  "edge_zone": null,
  "internal_dns_name_label": null,
  "ip_configurations": [
    {
      "gateway_load_balancer_frontend_ip_configuration_id": null,
      "name": null,
      "primary": true,
      "private_ip_address": null,
      "private_ip_address_allocation": null,
      "private_ip_address_version": null,
      "public_ip_address_id": null
    }
  ],
  "ip_forwarding_enabled": null,
  "name": null
}

os_disk = 

os_simple = null

os_version = "latest"

patch_assessment_mode = "ImageDefault"

patch_mode = null

plan = null

platform_fault_domain = null

priority = "Regular"

provision_vm_agent = true

proximity_placement_group_id = null

reboot_setting = null

resource_group_name = 

secrets = []

secure_boot_enabled = null

size = 

source_image_id = null

source_image_reference = null

standard_os = {
  "CentOS": {
    "offer": "CentOS",
    "publisher": "OpenLogic",
    "sku": "7.6"
  },
  "CoreOS": {
    "offer": "CoreOS",
    "publisher": "CoreOS",
    "sku": "Stable"
  },
  "Debian": {
    "offer": "Debian",
    "publisher": "credativ",
    "sku": "9"
  },
  "RHEL": {
    "offer": "RHEL",
    "publisher": "RedHat",
    "sku": "8.2"
  },
  "SLES": {
    "offer": "SLES",
    "publisher": "SUSE",
    "sku": "12-SP2"
  },
  "UbuntuServer": {
    "offer": "UbuntuServer",
    "publisher": "Canonical",
    "sku": "18.04-LTS"
  },
  "WindowsServer": {
    "offer": "WindowsServer",
    "publisher": "MicrosoftWindowsServer",
    "sku": "2019-Datacenter"
  },
  "openSUSE-Leap": {
    "offer": "openSUSE-Leap",
    "publisher": "SUSE",
    "sku": "15.1"
  }
}

subnet_id = 

tags = {
  "source": "terraform"
}

termination_notification = null

timezone = null

tracing_tags_enabled = false

tracing_tags_prefix = "avm_"

user_data = null

virtual_machine_scale_set_id = null

vm_additional_capabilities = null

vtpm_enabled = null

winrm_listeners = []

zone = null


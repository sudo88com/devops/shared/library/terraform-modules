[
  {
    "name": "virtual_desktop_application_group_host_pool_id",
    "type": "string",
    "description": "(Required) Resource ID for a Virtual Desktop Host Pool to associate with the Virtual Desktop Application Group. Changing the name forces a new resource to be created.",
    "default": "",
    "required": true
  },
  {
    "name": "virtual_desktop_application_group_location",
    "type": "string",
    "description": "(Required) The location/region where the Virtual Desktop Application Group is located. Changing this forces a new resource to be created.",
    "default": "",
    "required": true
  },
  {
    "name": "virtual_desktop_application_group_resource_group_name",
    "type": "string",
    "description": "(Required) The name of the resource group in which to create the Virtual Desktop Application Group. Changing this forces a new resource to be created.",
    "default": "",
    "required": true
  },
  {
    "name": "virtual_desktop_application_group_tags",
    "type": "map(string)",
    "description": "(Optional) A mapping of tags to assign to the resource.",
    "default": "null",
    "required": false
  },
  {
    "name": "user_group_name",
    "type": "string",
    "description": "Microsoft Entra ID User Group for AVD users",
    "default": "",
    "required": true
  },
  {
    "name": "virtual_desktop_application_group_type",
    "type": "string",
    "description": "(Required) Type of Virtual Desktop Application Group. Valid options are `RemoteApp` or `Desktop` application groups. Changing this forces a new resource to be created.",
    "default": "",
    "required": true
  },
  {
    "name": "diagnostic_settings",
    "type": "map(object({\n    name                                     = optional(string, null)\n    log_categories                           = optional(set(string), [])\n    log_groups                               = optional(set(string), [\"allLogs\"])\n    metric_categories                        = optional(set(string), [\"AllMetrics\"])\n    log_analytics_destination_type           = optional(string, \"Dedicated\")\n    workspace_resource_id                    = optional(string, null)\n    storage_account_resource_id              = optional(string, null)\n    event_hub_authorization_rule_resource_id = optional(string, null)\n    event_hub_name                           = optional(string, null)\n    marketplace_partner_resource_id          = optional(string, null)\n  }))",
    "description": "A map of diagnostic settings to create on the Key Vault. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.\n\n- `name` - (Optional) The name of the diagnostic setting. One will be generated if not set, however this will not be unique if you want to create multiple diagnostic setting resources.\n- `log_categories` - (Optional) A set of log categories to send to the log analytics workspace. Defaults to `[]`.\n- `log_groups` - (Optional) A set of log groups to send to the log analytics workspace. Defaults to `[\"allLogs\"]`.\n- `metric_categories` - (Optional) A set of metric categories to send to the log analytics workspace. Defaults to `[\"AllMetrics\"]`.\n- `log_analytics_destination_type` - (Optional) The destination type for the diagnostic setting. Possible values are `Dedicated` and `AzureDiagnostics`. Defaults to `Dedicated`.\n- `workspace_resource_id` - (Optional) The resource ID of the log analytics workspace to send logs and metrics to.\n- `storage_account_resource_id` - (Optional) The resource ID of the storage account to send logs and metrics to.\n- `event_hub_authorization_rule_resource_id` - (Optional) The resource ID of the event hub authorization rule to send logs and metrics to.\n- `event_hub_name` - (Optional) The name of the event hub. If none is specified, the default event hub will be selected.\n- `marketplace_partner_resource_id` - (Optional) The full ARM resource ID of the Marketplace resource to which you would like to send Diagnostic LogsLogs.\n",
    "default": "{}",
    "required": false
  },
  {
    "name": "role_assignments",
    "type": "map(object({\n    role_definition_id_or_name             = string\n    principal_id                           = string\n    description                            = optional(string, null)\n    skip_service_principal_aad_check       = optional(bool, false)\n    condition                              = optional(string, null)\n    condition_version                      = optional(string, null)\n    delegated_managed_identity_resource_id = optional(string, null)\n  }))",
    "description": "  A map of role assignments to create on the resource. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.\n  \n  - `role_definition_id_or_name` - The ID or name of the role definition to assign to the principal.\n  - `principal_id` - The ID of the principal to assign the role to.\n  - `description` - The description of the role assignment.\n  - `skip_service_principal_aad_check` - If set to true, skips the Azure Active Directory check for the service principal in the tenant. Defaults to false.\n  - `condition` - The condition which will be used to scope the role assignment.\n  - `condition_version` - The version of the condition syntax. Leave as `null` if you are not using a condition, if you are then valid values are '2.0'.\n  \n  > Note: only set `skip_service_principal_aad_check` to true if you are assigning a role to a service principal.\n",
    "default": "{}",
    "required": false
  },
  {
    "name": "virtual_desktop_application_group_description",
    "type": "string",
    "description": "(Optional) Option to set a description for the Virtual Desktop Application Group.",
    "default": "null",
    "required": false
  },
  {
    "name": "virtual_desktop_application_group_friendly_name",
    "type": "string",
    "description": "(Optional) Option to set a friendly name for the Virtual Desktop Application Group.",
    "default": "null",
    "required": false
  },
  {
    "name": "virtual_desktop_application_group_name",
    "type": "string",
    "description": "(Required) The name of the Virtual Desktop Application Group. Changing the name forces a new resource to be created.",
    "default": "",
    "required": true
  },
  {
    "name": "enable_telemetry",
    "type": "bool",
    "description": "This variable controls whether or not telemetry is enabled for the module.\nFor more information see https://aka.ms/avm/telemetryinfo.\nIf it is set to false, then no telemetry will be collected.\n",
    "default": "true",
    "required": false
  },
  {
    "name": "role_assignment_timeouts",
    "type": "object({\n    create = optional(string)\n    delete = optional(string)\n    read   = optional(string)\n  })",
    "description": "- `create` - (Defaults to 30 minutes) Used when creating the Role Assignment.\n- `delete` - (Defaults to 30 minutes) Used when deleting the Role Assignment.\n- `read` - (Defaults to 5 minutes) Used when retrieving the Role Assignment.\n",
    "default": "null",
    "required": false
  },
  {
    "name": "tracing_tags_enabled",
    "type": "bool",
    "description": "Whether enable tracing tags that generated by BridgeCrew Yor.",
    "default": "false",
    "required": false
  },
  {
    "name": "virtual_desktop_application_group_default_desktop_display_name",
    "type": "string",
    "description": "(Optional) Option to set the display name for the default sessionDesktop desktop when `type` is set to `Desktop`.",
    "default": "null",
    "required": false
  },
  {
    "name": "lock",
    "type": "object({\n    kind = string\n    name = optional(string, null)\n  })",
    "description": "  Controls the Resource Lock configuration for this resource. The following properties can be specified:\n  \n  - `kind` - (Required) The type of lock. Possible values are `\\\"CanNotDelete\\\"` and `\\\"ReadOnly\\\"`.\n  - `name` - (Optional) The name of the lock. If not specified, a name will be generated based on the `kind` value. Changing this forces the creation of a new resource.\n",
    "default": "null",
    "required": false
  },
  {
    "name": "tags",
    "type": "map(string)",
    "description": "(Optional) Tags of the resource.",
    "default": "null",
    "required": false
  },
  {
    "name": "tracing_tags_prefix",
    "type": "string",
    "description": "Default prefix for generated tracing tags",
    "default": "\"avm_\"",
    "required": false
  },
  {
    "name": "virtual_desktop_application_group_timeouts",
    "type": "object({\n    create = optional(string)\n    delete = optional(string)\n    read   = optional(string)\n    update = optional(string)\n  })",
    "description": "- `create` - (Defaults to 60 minutes) Used when creating the Virtual Desktop Application Group.\n- `delete` - (Defaults to 60 minutes) Used when deleting the Virtual Desktop Application Group.\n- `read` - (Defaults to 5 minutes) Used when retrieving the Virtual Desktop Application Group.\n- `update` - (Defaults to 60 minutes) Used when updating the Virtual Desktop Application Group.\n",
    "default": "null",
    "required": false
  }
]

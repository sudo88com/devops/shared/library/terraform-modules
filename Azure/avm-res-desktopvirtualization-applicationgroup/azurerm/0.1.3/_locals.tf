
locals {
  diagnostic_settings = var.diagnostic_settings
  enable_telemetry = var.enable_telemetry
  lock = var.lock
  role_assignment_timeouts = var.role_assignment_timeouts
  role_assignments = var.role_assignments
  tags = var.tags
  tracing_tags_enabled = var.tracing_tags_enabled
  tracing_tags_prefix = var.tracing_tags_prefix
  user_group_name = var.user_group_name
  virtual_desktop_application_group_default_desktop_display_name = var.virtual_desktop_application_group_default_desktop_display_name
  virtual_desktop_application_group_description = var.virtual_desktop_application_group_description
  virtual_desktop_application_group_friendly_name = var.virtual_desktop_application_group_friendly_name
  virtual_desktop_application_group_host_pool_id = var.virtual_desktop_application_group_host_pool_id
  virtual_desktop_application_group_location = var.virtual_desktop_application_group_location
  virtual_desktop_application_group_name = var.virtual_desktop_application_group_name
  virtual_desktop_application_group_resource_group_name = var.virtual_desktop_application_group_resource_group_name
  virtual_desktop_application_group_tags = var.virtual_desktop_application_group_tags
  virtual_desktop_application_group_timeouts = var.virtual_desktop_application_group_timeouts
  virtual_desktop_application_group_type = var.virtual_desktop_application_group_type
}

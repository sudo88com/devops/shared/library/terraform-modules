
output "dcr" {
  description = "The full output for the Monitor Data Collection Rule."
  value       = module.avm-ptn-avd-lza-insights.dcr
}


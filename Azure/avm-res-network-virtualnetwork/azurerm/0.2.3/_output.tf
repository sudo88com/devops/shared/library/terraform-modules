
output "name" {
  description = "The resource name of the virtual network."
  value       = module.avm-res-network-virtualnetwork.name
}

output "peerings" {
  description = "Information about the peerings created in the module.\n\nPlease refer to the peering module documentation for details of the outputs\n"
  value       = module.avm-res-network-virtualnetwork.peerings
}

output "resource" {
  description = "The Azure Virtual Network resource.  This will be null if an existing vnet is supplied."
  value       = module.avm-res-network-virtualnetwork.resource
}

output "resource_id" {
  description = "The resource ID of the virtual network."
  value       = module.avm-res-network-virtualnetwork.resource_id
}

output "subnets" {
  description = "Information about the subnets created in the module.\n\nPlease refer to the subnet module documentation for details of the outputs.\n"
  value       = module.avm-res-network-virtualnetwork.subnets
}


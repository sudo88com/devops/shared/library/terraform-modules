
module "avm-res-network-virtualnetwork" {
  source = "terraform-aws-modules/avm-res-network-virtualnetwork/aws"
  version = "0.2.3"
  address_space = var.address_space
  ddos_protection_plan = var.ddos_protection_plan
  diagnostic_settings = var.diagnostic_settings
  dns_servers = var.dns_servers
  enable_telemetry = var.enable_telemetry
  location = var.location
  lock = var.lock
  name = var.name
  peerings = var.peerings
  resource_group_name = var.resource_group_name
  role_assignments = var.role_assignments
  subnets = var.subnets
  subscription_id = var.subscription_id
  tags = var.tags
}


access_connector = {}

custom_parameters = {}

customer_managed_key_enabled = false

dbfs_root_cmk_key_vault_key_id = null

diagnostic_settings = {}

enable_telemetry = true

infrastructure_encryption_enabled = false

load_balancer_backend_address_pool_id = null

location = null

lock = {}

managed_disk_cmk_key_vault_key_id = null

managed_disk_cmk_rotation_to_latest_version_enabled = false

managed_identities = {}

managed_resource_group_name = null

managed_services_cmk_key_vault_key_id = null

name = 

network_security_group_rules_required = null

private_endpoints = {}

public_network_access_enabled = true

resource_group_name = 

role_assignments = {}

sku = 

tags = {}

virtual_network_peering = {}


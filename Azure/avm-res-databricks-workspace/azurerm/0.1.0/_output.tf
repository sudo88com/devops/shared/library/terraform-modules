
output "databricks_id" {
  description = "The ID of the Databricks Workspace in the Azure management plane."
  value       = module.avm-res-databricks-workspace.databricks_id
}

output "databricks_virtual_network_peering_address_space_prefixes" {
  description = "A list of address blocks reserved for this virtual network in CIDR notation."
  value       = module.avm-res-databricks-workspace.databricks_virtual_network_peering_address_space_prefixes
}

output "databricks_virtual_network_peering_id" {
  description = "The IDs of the internal Virtual Networks used by the DataBricks Workspace."
  value       = module.avm-res-databricks-workspace.databricks_virtual_network_peering_id
}

output "databricks_virtual_network_peering_virtual_network_id" {
  description = "The ID of the internal Virtual Network used by the DataBricks Workspace."
  value       = module.avm-res-databricks-workspace.databricks_virtual_network_peering_virtual_network_id
}

output "databricks_workspace_disk_encryption_set_id" {
  description = "The ID of Managed Disk Encryption Set created by the Databricks Workspace."
  value       = module.avm-res-databricks-workspace.databricks_workspace_disk_encryption_set_id
}

output "databricks_workspace_id" {
  description = "The unique identifier of the databricks workspace in Databricks control plane."
  value       = module.avm-res-databricks-workspace.databricks_workspace_id
}

output "databricks_workspace_managed_disk_identity" {
  description = "  A managed_disk_identity block as documented below\n  \n  - `principal_id` - The principal UUID for the internal databricks disks identity needed to provide access to the workspace for enabling Customer Managed Keys.\n  - `tenant_id` - The UUID of the tenant where the internal databricks disks identity was created.\n  - `type` - The type of the internal databricks disks identity.\n"
  value       = module.avm-res-databricks-workspace.databricks_workspace_managed_disk_identity
}

output "databricks_workspace_managed_resource_group_id" {
  description = "The ID of the Managed Resource Group created by the Databricks Workspace."
  value       = module.avm-res-databricks-workspace.databricks_workspace_managed_resource_group_id
}

output "databricks_workspace_storage_account_identity" {
  description = "  A storage_account_identity block as documented below\n  \n  - `principal_id` - The principal UUID for the internal databricks storage account needed to provide access to the workspace for enabling Customer Managed Keys.\n  - `tenant_id` - The UUID of the tenant where the internal databricks storage account was created.\n  - `type` - The type of the internal databricks storage account.\n"
  value       = module.avm-res-databricks-workspace.databricks_workspace_storage_account_identity
}

output "databricks_workspace_url" {
  description = "The workspace URL which is of the format 'adb-{workspaceId}.{random}.azuredatabricks.net'."
  value       = module.avm-res-databricks-workspace.databricks_workspace_url
}

output "private_endpoints" {
  description = "A map of private endpoints. The map key is the supplied input to var.private_endpoints. The map value is the entire azurerm_private_endpoint resource."
  value       = module.avm-res-databricks-workspace.private_endpoints
}

output "resource" {
  description = "This is the full output for the resource."
  value       = module.avm-res-databricks-workspace.resource
}


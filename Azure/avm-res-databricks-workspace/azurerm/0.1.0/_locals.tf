
locals {
  access_connector = var.access_connector
  custom_parameters = var.custom_parameters
  customer_managed_key_enabled = var.customer_managed_key_enabled
  dbfs_root_cmk_key_vault_key_id = var.dbfs_root_cmk_key_vault_key_id
  diagnostic_settings = var.diagnostic_settings
  enable_telemetry = var.enable_telemetry
  infrastructure_encryption_enabled = var.infrastructure_encryption_enabled
  load_balancer_backend_address_pool_id = var.load_balancer_backend_address_pool_id
  location = var.location
  lock = var.lock
  managed_disk_cmk_key_vault_key_id = var.managed_disk_cmk_key_vault_key_id
  managed_disk_cmk_rotation_to_latest_version_enabled = var.managed_disk_cmk_rotation_to_latest_version_enabled
  managed_identities = var.managed_identities
  managed_resource_group_name = var.managed_resource_group_name
  managed_services_cmk_key_vault_key_id = var.managed_services_cmk_key_vault_key_id
  name = var.name
  network_security_group_rules_required = var.network_security_group_rules_required
  private_endpoints = var.private_endpoints
  public_network_access_enabled = var.public_network_access_enabled
  resource_group_name = var.resource_group_name
  role_assignments = var.role_assignments
  sku = var.sku
  tags = var.tags
  virtual_network_peering = var.virtual_network_peering
}


variable "allocated_storage" {
  type        = number
  description = "The amount of storage in gibibytes (GiB) to allocate to each DB instance in the Multi-AZ DB cluster. (This setting is required to create a Multi-AZ DB cluster)"
  default     = null
}

variable "allow_major_version_upgrade" {
  type        = bool
  description = "Enable to allow major engine version upgrades when changing engine versions. Defaults to `false`"
  default     = false
}

variable "apply_immediately" {
  type        = bool
  description = "Specifies whether any cluster modifications are applied immediately, or during the next maintenance window. Default is `false`"
  default     = null
}

variable "auto_minor_version_upgrade" {
  type        = bool
  description = "Indicates that minor engine upgrades will be applied automatically to the DB instance during the maintenance window. Default `true`"
  default     = null
}

variable "autoscaling_enabled" {
  type        = bool
  description = "Determines whether autoscaling of the cluster read replicas is enabled"
  default     = false
}

variable "autoscaling_max_capacity" {
  type        = number
  description = "Maximum number of read replicas permitted when autoscaling is enabled"
  default     = 2
}

variable "autoscaling_min_capacity" {
  type        = number
  description = "Minimum number of read replicas permitted when autoscaling is enabled"
  default     = 0
}

variable "autoscaling_policy_name" {
  type        = string
  description = "Autoscaling policy name"
  default     = "target-metric"
}

variable "autoscaling_scale_in_cooldown" {
  type        = number
  description = "Cooldown in seconds before allowing further scaling operations after a scale in"
  default     = 300
}

variable "autoscaling_scale_out_cooldown" {
  type        = number
  description = "Cooldown in seconds before allowing further scaling operations after a scale out"
  default     = 300
}

variable "autoscaling_target_connections" {
  type        = number
  description = "Average number of connections threshold which will initiate autoscaling. Default value is 70% of db.r4/r5/r6g.large's default max_connections"
  default     = 700
}

variable "autoscaling_target_cpu" {
  type        = number
  description = "CPU threshold which will initiate autoscaling"
  default     = 70
}

variable "availability_zones" {
  type        = list(string)
  description = "List of EC2 Availability Zones for the DB cluster storage where DB cluster instances can be created. RDS automatically assigns 3 AZs if less than 3 AZs are configured, which will show as a difference requiring resource recreation next Terraform apply"
  default     = null
}

variable "backtrack_window" {
  type        = number
  description = "The target backtrack window, in seconds. Only available for `aurora` engine currently. To disable backtracking, set this value to 0. Must be between 0 and 259200 (72 hours)"
  default     = null
}

variable "backup_retention_period" {
  type        = number
  description = "The days to retain backups for"
  default     = null
}

variable "ca_cert_identifier" {
  type        = string
  description = "The identifier of the CA certificate for the DB instance"
  default     = null
}

variable "cloudwatch_log_group_class" {
  type        = string
  description = "Specified the log class of the log group. Possible values are: STANDARD or INFREQUENT_ACCESS"
  default     = null
}

variable "cloudwatch_log_group_kms_key_id" {
  type        = string
  description = "The ARN of the KMS Key to use when encrypting log data"
  default     = null
}

variable "cloudwatch_log_group_retention_in_days" {
  type        = number
  description = "The number of days to retain CloudWatch logs for the DB instance"
  default     = 7
}

variable "cloudwatch_log_group_skip_destroy" {
  type        = bool
  description = "Set to true if you do not wish the log group (and any logs it may contain) to be deleted at destroy time, and instead just remove the log group from the Terraform state"
  default     = null
}

variable "cluster_members" {
  type        = list(string)
  description = "List of RDS Instances that are a part of this cluster"
  default     = null
}

variable "cluster_tags" {
  type        = map(string)
  description = "A map of tags to add to only the cluster. Used for AWS Instance Scheduler tagging"
  default     = {}
}

variable "cluster_timeouts" {
  type        = map(string)
  description = "Create, update, and delete timeout configurations for the cluster"
  default     = {}
}

variable "cluster_use_name_prefix" {
  type        = bool
  description = "Whether to use `name` as a prefix for the cluster"
  default     = false
}

variable "copy_tags_to_snapshot" {
  type        = bool
  description = "Copy all Cluster `tags` to snapshots"
  default     = null
}

variable "create" {
  type        = bool
  description = "Whether cluster should be created (affects nearly all resources)"
  default     = true
}

variable "create_cloudwatch_log_group" {
  type        = bool
  description = "Determines whether a CloudWatch log group is created for each `enabled_cloudwatch_logs_exports`"
  default     = false
}

variable "create_db_cluster_activity_stream" {
  type        = bool
  description = "Determines whether a cluster activity stream is created."
  default     = false
}

variable "create_db_cluster_parameter_group" {
  type        = bool
  description = "Determines whether a cluster parameter should be created or use existing"
  default     = false
}

variable "create_db_parameter_group" {
  type        = bool
  description = "Determines whether a DB parameter should be created or use existing"
  default     = false
}

variable "create_db_subnet_group" {
  type        = bool
  description = "Determines whether to create the database subnet group or use existing"
  default     = false
}

variable "create_monitoring_role" {
  type        = bool
  description = "Determines whether to create the IAM role for RDS enhanced monitoring"
  default     = true
}

variable "create_security_group" {
  type        = bool
  description = "Determines whether to create security group for RDS cluster"
  default     = true
}

variable "database_name" {
  type        = string
  description = "Name for an automatically created database on cluster creation"
  default     = null
}

variable "db_cluster_activity_stream_kms_key_id" {
  type        = string
  description = "The AWS KMS key identifier for encrypting messages in the database activity stream"
  default     = null
}

variable "db_cluster_activity_stream_mode" {
  type        = string
  description = "Specifies the mode of the database activity stream. Database events such as a change or access generate an activity stream event. One of: sync, async"
  default     = null
}

variable "db_cluster_db_instance_parameter_group_name" {
  type        = string
  description = "Instance parameter group to associate with all instances of the DB cluster. The `db_cluster_db_instance_parameter_group_name` is only valid in combination with `allow_major_version_upgrade`"
  default     = null
}

variable "db_cluster_instance_class" {
  type        = string
  description = "The compute and memory capacity of each DB instance in the Multi-AZ DB cluster, for example db.m6g.xlarge. Not all DB instance classes are available in all AWS Regions, or for all database engines"
  default     = null
}

variable "db_cluster_parameter_group_description" {
  type        = string
  description = "The description of the DB cluster parameter group. Defaults to \"Managed by Terraform\""
  default     = null
}

variable "db_cluster_parameter_group_family" {
  type        = string
  description = "The family of the DB cluster parameter group"
  default     = ""
}

variable "db_cluster_parameter_group_name" {
  type        = string
  description = "The name of the DB cluster parameter group"
  default     = null
}

variable "db_cluster_parameter_group_parameters" {
  type        = list(map(string))
  description = "A list of DB cluster parameters to apply. Note that parameters may differ from a family to an other"
  default     = []
}

variable "db_cluster_parameter_group_use_name_prefix" {
  type        = bool
  description = "Determines whether the DB cluster parameter group name is used as a prefix"
  default     = true
}

variable "db_parameter_group_description" {
  type        = string
  description = "The description of the DB parameter group. Defaults to \"Managed by Terraform\""
  default     = null
}

variable "db_parameter_group_family" {
  type        = string
  description = "The family of the DB parameter group"
  default     = ""
}

variable "db_parameter_group_name" {
  type        = string
  description = "The name of the DB parameter group"
  default     = null
}

variable "db_parameter_group_parameters" {
  type        = list(map(string))
  description = "A list of DB parameters to apply. Note that parameters may differ from a family to an other"
  default     = []
}

variable "db_parameter_group_use_name_prefix" {
  type        = bool
  description = "Determines whether the DB parameter group name is used as a prefix"
  default     = true
}

variable "db_subnet_group_name" {
  type        = string
  description = "The name of the subnet group name (existing or created)"
  default     = ""
}

variable "delete_automated_backups" {
  type        = bool
  description = "Specifies whether to remove automated backups immediately after the DB cluster is deleted"
  default     = null
}

variable "deletion_protection" {
  type        = bool
  description = "If the DB instance should have deletion protection enabled. The database can't be deleted when this value is set to `true`. The default is `false`"
  default     = null
}

variable "domain" {
  type        = string
  description = "The ID of the Directory Service Active Directory domain to create the instance in"
  default     = null
}

variable "domain_iam_role_name" {
  type        = string
  description = "(Required if domain is provided) The name of the IAM role to be used when making API calls to the Directory Service"
  default     = null
}

variable "enable_global_write_forwarding" {
  type        = bool
  description = "Whether cluster should forward writes to an associated global cluster. Applied to secondary clusters to enable them to forward writes to an `aws_rds_global_cluster`'s primary cluster"
  default     = null
}

variable "enable_http_endpoint" {
  type        = bool
  description = "Enable HTTP endpoint (data API). Only valid when engine_mode is set to `serverless`"
  default     = null
}

variable "enable_local_write_forwarding" {
  type        = bool
  description = "Whether read replicas can forward write operations to the writer DB instance in the DB cluster. By default, write operations aren't allowed on reader DB instances."
  default     = null
}

variable "enabled_cloudwatch_logs_exports" {
  type        = list(string)
  description = "Set of log types to export to cloudwatch. If omitted, no logs will be exported. The following log types are supported: `audit`, `error`, `general`, `slowquery`, `postgresql`"
  default     = []
}

variable "endpoints" {
  type        = any
  description = "Map of additional cluster endpoints and their attributes to be created"
  default     = {}
}

variable "engine" {
  type        = string
  description = "The name of the database engine to be used for this DB cluster. Defaults to `aurora`. Valid Values: `aurora`, `aurora-mysql`, `aurora-postgresql`"
  default     = null
}

variable "engine_mode" {
  type        = string
  description = "The database engine mode. Valid values: `global`, `multimaster`, `parallelquery`, `provisioned`, `serverless`. Defaults to: `provisioned`"
  default     = "provisioned"
}

variable "engine_native_audit_fields_included" {
  type        = bool
  description = "Specifies whether the database activity stream includes engine-native audit fields. This option only applies to an Oracle DB instance. By default, no engine-native audit fields are included"
  default     = false
}

variable "engine_version" {
  type        = string
  description = "The database engine version. Updating this argument results in an outage"
  default     = null
}

variable "final_snapshot_identifier" {
  type        = string
  description = "The name of your final DB snapshot when this DB cluster is deleted. If omitted, no final snapshot will be made"
  default     = null
}

variable "global_cluster_identifier" {
  type        = string
  description = "The global cluster identifier specified on `aws_rds_global_cluster`"
  default     = null
}

variable "iam_database_authentication_enabled" {
  type        = bool
  description = "Specifies whether or mappings of AWS Identity and Access Management (IAM) accounts to database accounts is enabled"
  default     = null
}

variable "iam_role_description" {
  type        = string
  description = "Description of the monitoring role"
  default     = null
}

variable "iam_role_force_detach_policies" {
  type        = bool
  description = "Whether to force detaching any policies the monitoring role has before destroying it"
  default     = null
}

variable "iam_role_managed_policy_arns" {
  type        = list(string)
  description = "Set of exclusive IAM managed policy ARNs to attach to the monitoring role"
  default     = null
}

variable "iam_role_max_session_duration" {
  type        = number
  description = "Maximum session duration (in seconds) that you want to set for the monitoring role"
  default     = null
}

variable "iam_role_name" {
  type        = string
  description = "Friendly name of the monitoring role"
  default     = null
}

variable "iam_role_path" {
  type        = string
  description = "Path for the monitoring role"
  default     = null
}

variable "iam_role_permissions_boundary" {
  type        = string
  description = "The ARN of the policy that is used to set the permissions boundary for the monitoring role"
  default     = null
}

variable "iam_role_use_name_prefix" {
  type        = bool
  description = "Determines whether to use `iam_role_name` as is or create a unique name beginning with the `iam_role_name` as the prefix"
  default     = false
}

variable "iam_roles" {
  type        = map(map(string))
  description = "Map of IAM roles and supported feature names to associate with the cluster"
  default     = {}
}

variable "instance_class" {
  type        = string
  description = "Instance type to use at master instance. Note: if `autoscaling_enabled` is `true`, this will be the same instance class used on instances created by autoscaling"
  default     = ""
}

variable "instance_timeouts" {
  type        = map(string)
  description = "Create, update, and delete timeout configurations for the cluster instance(s)"
  default     = {}
}

variable "instances" {
  type        = any
  description = "Map of cluster instances and any specific/overriding attributes to be created"
  default     = {}
}

variable "instances_use_identifier_prefix" {
  type        = bool
  description = "Determines whether cluster instance identifiers are used as prefixes"
  default     = false
}

variable "iops" {
  type        = number
  description = "The amount of Provisioned IOPS (input/output operations per second) to be initially allocated for each DB instance in the Multi-AZ DB cluster"
  default     = null
}

variable "is_primary_cluster" {
  type        = bool
  description = "Determines whether cluster is primary cluster with writer instance (set to `false` for global cluster and replica clusters)"
  default     = true
}

variable "kms_key_id" {
  type        = string
  description = "The ARN for the KMS encryption key. When specifying `kms_key_id`, `storage_encrypted` needs to be set to `true`"
  default     = null
}

variable "manage_master_user_password" {
  type        = bool
  description = "Set to true to allow RDS to manage the master user password in Secrets Manager. Cannot be set if `master_password` is provided"
  default     = true
}

variable "manage_master_user_password_rotation" {
  type        = bool
  description = "Whether to manage the master user password rotation. Setting this value to false after previously having been set to true will disable automatic rotation."
  default     = false
}

variable "master_password" {
  type        = string
  description = "Password for the master DB user. Note that this may show up in logs, and it will be stored in the state file. Required unless `manage_master_user_password` is set to `true` or unless `snapshot_identifier` or `replication_source_identifier` is provided or unless a `global_cluster_identifier` is provided when the cluster is the secondary cluster of a global database"
  default     = null
}

variable "master_user_password_rotate_immediately" {
  type        = bool
  description = "Specifies whether to rotate the secret immediately or wait until the next scheduled rotation window."
  default     = null
}

variable "master_user_password_rotation_automatically_after_days" {
  type        = number
  description = "Specifies the number of days between automatic scheduled rotations of the secret. Either `master_user_password_rotation_automatically_after_days` or `master_user_password_rotation_schedule_expression` must be specified"
  default     = null
}

variable "master_user_password_rotation_duration" {
  type        = string
  description = "The length of the rotation window in hours. For example, 3h for a three hour window."
  default     = null
}

variable "master_user_password_rotation_schedule_expression" {
  type        = string
  description = "A cron() or rate() expression that defines the schedule for rotating your secret. Either `master_user_password_rotation_automatically_after_days` or `master_user_password_rotation_schedule_expression` must be specified"
  default     = null
}

variable "master_user_secret_kms_key_id" {
  type        = string
  description = "The Amazon Web Services KMS key identifier is the key ARN, key ID, alias ARN, or alias name for the KMS key"
  default     = null
}

variable "master_username" {
  type        = string
  description = "Username for the master DB user. Required unless `snapshot_identifier` or `replication_source_identifier` is provided or unless a `global_cluster_identifier` is provided when the cluster is the secondary cluster of a global database"
  default     = null
}

variable "monitoring_interval" {
  type        = number
  description = "The interval, in seconds, between points when Enhanced Monitoring metrics are collected for instances. Set to `0` to disable. Default is `0`"
  default     = 0
}

variable "monitoring_role_arn" {
  type        = string
  description = "IAM role used by RDS to send enhanced monitoring metrics to CloudWatch"
  default     = ""
}

variable "name" {
  type        = string
  description = "Name used across resources created"
  default     = ""
}

variable "network_type" {
  type        = string
  description = "The type of network stack to use (IPV4 or DUAL)"
  default     = null
}

variable "performance_insights_enabled" {
  type        = bool
  description = "Specifies whether Performance Insights is enabled or not"
  default     = null
}

variable "performance_insights_kms_key_id" {
  type        = string
  description = "The ARN for the KMS key to encrypt Performance Insights data"
  default     = null
}

variable "performance_insights_retention_period" {
  type        = number
  description = "Amount of time in days to retain Performance Insights data. Either 7 (7 days) or 731 (2 years)"
  default     = null
}

variable "port" {
  type        = string
  description = "The port on which the DB accepts connections"
  default     = null
}

variable "predefined_metric_type" {
  type        = string
  description = "The metric type to scale on. Valid values are `RDSReaderAverageCPUUtilization` and `RDSReaderAverageDatabaseConnections`"
  default     = "RDSReaderAverageCPUUtilization"
}

variable "preferred_backup_window" {
  type        = string
  description = "The daily time range during which automated backups are created if automated backups are enabled using the `backup_retention_period` parameter. Time in UTC"
  default     = "02:00-03:00"
}

variable "preferred_maintenance_window" {
  type        = string
  description = "The weekly time range during which system maintenance can occur, in (UTC)"
  default     = "sun:05:00-sun:06:00"
}

variable "publicly_accessible" {
  type        = bool
  description = "Determines whether instances are publicly accessible. Default `false`"
  default     = null
}

variable "putin_khuylo" {
  type        = bool
  description = "Do you agree that Putin doesn't respect Ukrainian sovereignty and territorial integrity? More info: https://en.wikipedia.org/wiki/Putin_khuylo!"
  default     = true
}

variable "replication_source_identifier" {
  type        = string
  description = "ARN of a source DB cluster or DB instance if this DB cluster is to be created as a Read Replica"
  default     = null
}

variable "restore_to_point_in_time" {
  type        = map(string)
  description = "Map of nested attributes for cloning Aurora cluster"
  default     = {}
}

variable "s3_import" {
  type        = map(string)
  description = "Configuration map used to restore from a Percona Xtrabackup in S3 (only MySQL is supported)"
  default     = {}
}

variable "scaling_configuration" {
  type        = map(string)
  description = "Map of nested attributes with scaling properties. Only valid when `engine_mode` is set to `serverless`"
  default     = {}
}

variable "security_group_description" {
  type        = string
  description = "The description of the security group. If value is set to empty string it will contain cluster name in the description"
  default     = null
}

variable "security_group_name" {
  type        = string
  description = "The security group name. Default value is (`var.name`)"
  default     = ""
}

variable "security_group_rules" {
  type        = any
  description = "Map of security group rules to add to the cluster security group created"
  default     = {}
}

variable "security_group_tags" {
  type        = map(string)
  description = "Additional tags for the security group"
  default     = {}
}

variable "security_group_use_name_prefix" {
  type        = bool
  description = "Determines whether the security group name (`var.name`) is used as a prefix"
  default     = true
}

variable "serverlessv2_scaling_configuration" {
  type        = map(string)
  description = "Map of nested attributes with serverless v2 scaling properties. Only valid when `engine_mode` is set to `provisioned`"
  default     = {}
}

variable "skip_final_snapshot" {
  type        = bool
  description = "Determines whether a final snapshot is created before the cluster is deleted. If true is specified, no snapshot is created"
  default     = false
}

variable "snapshot_identifier" {
  type        = string
  description = "Specifies whether or not to create this cluster from a snapshot. You can use either the name or ARN when specifying a DB cluster snapshot, or the ARN when specifying a DB snapshot"
  default     = null
}

variable "source_region" {
  type        = string
  description = "The source region for an encrypted replica DB cluster"
  default     = null
}

variable "storage_encrypted" {
  type        = bool
  description = "Specifies whether the DB cluster is encrypted. The default is `true`"
  default     = true
}

variable "storage_type" {
  type        = string
  description = "Determines the storage type for the DB cluster. Optional for Single-AZ, required for Multi-AZ DB clusters. Valid values for Single-AZ: `aurora`, `\"\"` (default, both refer to Aurora Standard), `aurora-iopt1` (Aurora I/O Optimized). Valid values for Multi-AZ: `io1` (default)."
  default     = null
}

variable "subnets" {
  type        = list(string)
  description = "List of subnet IDs used by database subnet group created"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "vpc_id" {
  type        = string
  description = "ID of the VPC where to create security group"
  default     = ""
}

variable "vpc_security_group_ids" {
  type        = list(string)
  description = "List of VPC security groups to associate to the cluster in addition to the security group created"
  default     = []
}


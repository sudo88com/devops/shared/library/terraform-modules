
allocated_storage = null

allow_major_version_upgrade = false

apply_immediately = null

auto_minor_version_upgrade = null

autoscaling_enabled = false

autoscaling_max_capacity = 2

autoscaling_min_capacity = 0

autoscaling_policy_name = "target-metric"

autoscaling_scale_in_cooldown = 300

autoscaling_scale_out_cooldown = 300

autoscaling_target_connections = 700

autoscaling_target_cpu = 70

availability_zones = null

backtrack_window = null

backup_retention_period = null

ca_cert_identifier = null

cloudwatch_log_group_class = null

cloudwatch_log_group_kms_key_id = null

cloudwatch_log_group_retention_in_days = 7

cloudwatch_log_group_skip_destroy = null

cluster_members = null

cluster_tags = {}

cluster_timeouts = {}

cluster_use_name_prefix = false

copy_tags_to_snapshot = null

create = true

create_cloudwatch_log_group = false

create_db_cluster_activity_stream = false

create_db_cluster_parameter_group = false

create_db_parameter_group = false

create_db_subnet_group = false

create_monitoring_role = true

create_security_group = true

database_name = null

db_cluster_activity_stream_kms_key_id = null

db_cluster_activity_stream_mode = null

db_cluster_db_instance_parameter_group_name = null

db_cluster_instance_class = null

db_cluster_parameter_group_description = null

db_cluster_parameter_group_family = ""

db_cluster_parameter_group_name = null

db_cluster_parameter_group_parameters = []

db_cluster_parameter_group_use_name_prefix = true

db_parameter_group_description = null

db_parameter_group_family = ""

db_parameter_group_name = null

db_parameter_group_parameters = []

db_parameter_group_use_name_prefix = true

db_subnet_group_name = ""

delete_automated_backups = null

deletion_protection = null

domain = null

domain_iam_role_name = null

enable_global_write_forwarding = null

enable_http_endpoint = null

enable_local_write_forwarding = null

enabled_cloudwatch_logs_exports = []

endpoints = {}

engine = null

engine_mode = "provisioned"

engine_native_audit_fields_included = false

engine_version = null

final_snapshot_identifier = null

global_cluster_identifier = null

iam_database_authentication_enabled = null

iam_role_description = null

iam_role_force_detach_policies = null

iam_role_managed_policy_arns = null

iam_role_max_session_duration = null

iam_role_name = null

iam_role_path = null

iam_role_permissions_boundary = null

iam_role_use_name_prefix = false

iam_roles = {}

instance_class = ""

instance_timeouts = {}

instances = {}

instances_use_identifier_prefix = false

iops = null

is_primary_cluster = true

kms_key_id = null

manage_master_user_password = true

manage_master_user_password_rotation = false

master_password = null

master_user_password_rotate_immediately = null

master_user_password_rotation_automatically_after_days = null

master_user_password_rotation_duration = null

master_user_password_rotation_schedule_expression = null

master_user_secret_kms_key_id = null

master_username = null

monitoring_interval = 0

monitoring_role_arn = ""

name = ""

network_type = null

performance_insights_enabled = null

performance_insights_kms_key_id = null

performance_insights_retention_period = null

port = null

predefined_metric_type = "RDSReaderAverageCPUUtilization"

preferred_backup_window = "02:00-03:00"

preferred_maintenance_window = "sun:05:00-sun:06:00"

publicly_accessible = null

putin_khuylo = true

replication_source_identifier = null

restore_to_point_in_time = {}

s3_import = {}

scaling_configuration = {}

security_group_description = null

security_group_name = ""

security_group_rules = {}

security_group_tags = {}

security_group_use_name_prefix = true

serverlessv2_scaling_configuration = {}

skip_final_snapshot = false

snapshot_identifier = null

source_region = null

storage_encrypted = true

storage_type = null

subnets = []

tags = {}

vpc_id = ""

vpc_security_group_ids = []


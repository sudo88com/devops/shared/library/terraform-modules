
variable "acl_name" {
  type        = string
  description = "Name of ACL to be created if `create_acl` is `true`, otherwise its the name of an existing ACL to use if `create_acl` is `false`"
  default     = null
}

variable "acl_tags" {
  type        = map(string)
  description = "Additional tags for the ACL created"
  default     = {}
}

variable "acl_use_name_prefix" {
  type        = bool
  description = "Determines whether `acl_name` is used as a prefix"
  default     = false
}

variable "acl_user_names" {
  type        = list(string)
  description = "List of externally created user names to associate with the ACL"
  default     = []
}

variable "auto_minor_version_upgrade" {
  type        = bool
  description = "When set to `true`, the cluster will automatically receive minor engine version upgrades after launch. Defaults to `true`"
  default     = null
}

variable "create" {
  type        = bool
  description = "Determines whether resources will be created - affects all resources"
  default     = true
}

variable "create_acl" {
  type        = bool
  description = "Determines whether to create ACL specified"
  default     = true
}

variable "create_parameter_group" {
  type        = bool
  description = "Determines whether to create parameter group specified"
  default     = true
}

variable "create_subnet_group" {
  type        = bool
  description = "Determines whether to create subnet group specified"
  default     = true
}

variable "create_users" {
  type        = bool
  description = "Determines whether to create users specified"
  default     = true
}

variable "data_tiering" {
  type        = bool
  description = "Must be set to `true` when using a data tiering node type"
  default     = null
}

variable "description" {
  type        = string
  description = "Description for the cluster. Defaults to `Managed by Terraform`"
  default     = null
}

variable "engine_version" {
  type        = string
  description = "Version number of the Redis engine to be used for the cluster. Downgrades are not supported"
  default     = null
}

variable "final_snapshot_name" {
  type        = string
  description = "Name of the final cluster snapshot to be created when this resource is deleted. If omitted, no final snapshot will be made"
  default     = null
}

variable "kms_key_arn" {
  type        = string
  description = "ARN of the KMS key used to encrypt the cluster at rest"
  default     = null
}

variable "maintenance_window" {
  type        = string
  description = "Specifies the weekly time range during which maintenance on the cluster is performed. It is specified as a range in the format `ddd:hh24:mi-ddd:hh24:mi`"
  default     = null
}

variable "name" {
  type        = string
  description = "Cluster name - also default name used on all resources if more specific resource names are not provided"
  default     = ""
}

variable "node_type" {
  type        = string
  description = "The compute and memory capacity of the nodes in the cluster. See AWS documentation on [supported node types](https://docs.aws.amazon.com/memorydb/latest/devguide/nodes.supportedtypes.html) as well as [vertical scaling](https://docs.aws.amazon.com/memorydb/latest/devguide/cluster-vertical-scaling.html)"
  default     = null
}

variable "num_replicas_per_shard" {
  type        = number
  description = "The number of replicas to apply to each shard, up to a maximum of 5. Defaults to `1` (i.e. 2 nodes per shard)"
  default     = null
}

variable "num_shards" {
  type        = number
  description = "The number of shards in the cluster. Defaults to `1`"
  default     = null
}

variable "parameter_group_description" {
  type        = string
  description = "Description for the parameter group. Defaults to `Managed by Terraform`"
  default     = null
}

variable "parameter_group_family" {
  type        = string
  description = "The engine version that the parameter group can be used with"
  default     = null
}

variable "parameter_group_name" {
  type        = string
  description = "Name of parameter group to be created if `create_parameter_group` is `true`, otherwise its the name of an existing parameter group to use if `create_parameter_group` is `false`"
  default     = null
}

variable "parameter_group_parameters" {
  type        = list(map(string))
  description = "A list of parameter maps to apply"
  default     = []
}

variable "parameter_group_tags" {
  type        = map(string)
  description = "Additional tags for the parameter group created"
  default     = {}
}

variable "parameter_group_use_name_prefix" {
  type        = bool
  description = "Determines whether `parameter_group_name` is used as a prefix"
  default     = false
}

variable "port" {
  type        = number
  description = "The port number on which each of the nodes accepts connections. Defaults to `6379`"
  default     = null
}

variable "security_group_ids" {
  type        = list(string)
  description = "Set of VPC Security Group ID-s to associate with this cluster"
  default     = null
}

variable "snapshot_arns" {
  type        = list(string)
  description = " List of ARN-s that uniquely identify RDB snapshot files stored in S3. The snapshot files will be used to populate the new cluster"
  default     = null
}

variable "snapshot_name" {
  type        = string
  description = "The name of a snapshot from which to restore data into the new cluster"
  default     = null
}

variable "snapshot_retention_limit" {
  type        = number
  description = "The number of days for which MemoryDB retains automatic snapshots before deleting them. When set to `0`, automatic backups are disabled. Defaults to `0`"
  default     = null
}

variable "snapshot_window" {
  type        = string
  description = "The daily time range (in UTC) during which MemoryDB begins taking a daily snapshot of your shard. Example: `05:00-09:00`"
  default     = null
}

variable "sns_topic_arn" {
  type        = string
  description = "ARN of the SNS topic to which cluster notifications are sent"
  default     = null
}

variable "subnet_group_description" {
  type        = string
  description = "Description for the subnet group. Defaults to `Managed by Terraform`"
  default     = null
}

variable "subnet_group_name" {
  type        = string
  description = "Name of subnet group to be created if `create_subnet_group` is `true`, otherwise its the name of an existing subnet group to use if `create_subnet_group` is `false`"
  default     = null
}

variable "subnet_group_tags" {
  type        = map(string)
  description = "Additional tags for the subnet group created"
  default     = {}
}

variable "subnet_group_use_name_prefix" {
  type        = bool
  description = "Determines whether `subnet_group_name` is used as a prefix"
  default     = false
}

variable "subnet_ids" {
  type        = list(string)
  description = "Set of VPC Subnet ID-s for the subnet group. At least one subnet must be provided"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to use on all resources"
  default     = {}
}

variable "tls_enabled" {
  type        = bool
  description = "A flag to enable in-transit encryption on the cluster. When set to `false`, the `acl_name` must be `open-access`. Defaults to `true`"
  default     = null
}

variable "use_name_prefix" {
  type        = bool
  description = "Determines whether `name` is used as a prefix for the cluster"
  default     = false
}

variable "users" {
  type        = map(any)
  description = "A map of user definitions (maps) to be created"
  default     = {}
}


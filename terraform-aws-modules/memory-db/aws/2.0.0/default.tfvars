
acl_name = null

acl_tags = {}

acl_use_name_prefix = false

acl_user_names = []

auto_minor_version_upgrade = null

create = true

create_acl = true

create_parameter_group = true

create_subnet_group = true

create_users = true

data_tiering = null

description = null

engine_version = null

final_snapshot_name = null

kms_key_arn = null

maintenance_window = null

name = ""

node_type = null

num_replicas_per_shard = null

num_shards = null

parameter_group_description = null

parameter_group_family = null

parameter_group_name = null

parameter_group_parameters = []

parameter_group_tags = {}

parameter_group_use_name_prefix = false

port = null

security_group_ids = null

snapshot_arns = null

snapshot_name = null

snapshot_retention_limit = null

snapshot_window = null

sns_topic_arn = null

subnet_group_description = null

subnet_group_name = null

subnet_group_tags = {}

subnet_group_use_name_prefix = false

subnet_ids = []

tags = {}

tls_enabled = null

use_name_prefix = false

users = {}


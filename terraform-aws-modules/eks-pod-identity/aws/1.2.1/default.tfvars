
additional_policy_arns = {}

amazon_managed_service_prometheus_policy_name = null

amazon_managed_service_prometheus_workspace_arns = []

appmesh_controller_policy_name = null

appmesh_envoy_proxy_policy_name = null

association_defaults = {}

associations = {}

attach_amazon_managed_service_prometheus_policy = false

attach_aws_appmesh_controller_policy = false

attach_aws_appmesh_envoy_proxy_policy = false

attach_aws_cloudwatch_observability_policy = false

attach_aws_ebs_csi_policy = false

attach_aws_efs_csi_policy = false

attach_aws_fsx_lustre_csi_policy = false

attach_aws_gateway_controller_policy = false

attach_aws_lb_controller_policy = false

attach_aws_lb_controller_targetgroup_binding_only_policy = false

attach_aws_node_termination_handler_policy = false

attach_aws_privateca_issuer_policy = false

attach_aws_vpc_cni_policy = false

attach_cert_manager_policy = false

attach_cluster_autoscaler_policy = false

attach_custom_policy = false

attach_external_dns_policy = false

attach_external_secrets_policy = false

attach_mountpoint_s3_csi_policy = false

attach_velero_policy = false

aws_ebs_csi_kms_arns = []

aws_ebs_csi_policy_name = null

aws_efs_csi_policy_name = null

aws_fsx_lustre_csi_policy_name = null

aws_fsx_lustre_csi_service_role_arns = []

aws_gateway_controller_policy_name = null

aws_lb_controller_policy_name = null

aws_lb_controller_targetgroup_arns = []

aws_lb_controller_targetgroup_only_policy_name = null

aws_node_termination_handler_policy_name = null

aws_node_termination_handler_sqs_queue_arns = []

aws_privateca_issuer_acmca_arns = []

aws_privateca_issuer_policy_name = null

aws_vpc_cni_enable_ipv4 = false

aws_vpc_cni_enable_ipv6 = false

aws_vpc_cni_policy_name = null

cert_manager_hosted_zone_arns = []

cert_manager_policy_name = null

cluster_autoscaler_cluster_names = []

cluster_autoscaler_policy_name = null

create = true

custom_policy_description = "Custom IAM Policy"

description = null

external_dns_hosted_zone_arns = []

external_dns_policy_name = null

external_secrets_create_permission = false

external_secrets_kms_key_arns = []

external_secrets_policy_name = null

external_secrets_secrets_manager_arns = []

external_secrets_ssm_parameter_arns = []

max_session_duration = null

mountpoint_s3_csi_bucket_arns = []

mountpoint_s3_csi_bucket_path_arns = []

mountpoint_s3_csi_policy_name = null

name = ""

override_policy_documents = []

path = "/"

permissions_boundary_arn = null

policy_name_prefix = "AmazonEKS_"

policy_statements = []

source_policy_documents = []

tags = {}

trust_policy_statements = []

use_name_prefix = true

velero_policy_name = null

velero_s3_bucket_arns = []

velero_s3_bucket_path_arns = []



variable "additional_policy_arns" {
  type        = map(string)
  description = "ARNs of additional policies to attach to the IAM role"
  default     = {}
}

variable "amazon_managed_service_prometheus_policy_name" {
  type        = string
  description = "Custom name of the Amazon Managed Service for Prometheus IAM policy"
  default     = null
}

variable "amazon_managed_service_prometheus_workspace_arns" {
  type        = list(string)
  description = "List of AMP Workspace ARNs to read and write metrics"
  default     = []
}

variable "appmesh_controller_policy_name" {
  type        = string
  description = "Custom name of the AppMesh Controller IAM policy"
  default     = null
}

variable "appmesh_envoy_proxy_policy_name" {
  type        = string
  description = "Custom name of the AppMesh Envoy Proxy IAM policy"
  default     = null
}

variable "association_defaults" {
  type        = map(string)
  description = "Default values used across all Pod Identity associations created unless a more specific value is provided"
  default     = {}
}

variable "associations" {
  type        = any
  description = "Map of Pod Identity associations to be created (map of maps)"
  default     = {}
}

variable "attach_amazon_managed_service_prometheus_policy" {
  type        = bool
  description = "Determines whether to attach the Amazon Managed Service for Prometheus IAM policy to the role"
  default     = false
}

variable "attach_aws_appmesh_controller_policy" {
  type        = bool
  description = "Determines whether to attach the AppMesh Controller policy to the role"
  default     = false
}

variable "attach_aws_appmesh_envoy_proxy_policy" {
  type        = bool
  description = "Determines whether to attach the AppMesh Envoy Proxy policy to the role"
  default     = false
}

variable "attach_aws_cloudwatch_observability_policy" {
  type        = bool
  description = "Determines whether to attach the AWS Cloudwatch Observability IAM policy to the role"
  default     = false
}

variable "attach_aws_ebs_csi_policy" {
  type        = bool
  description = "Determines whether to attach the EBS CSI IAM policy to the role"
  default     = false
}

variable "attach_aws_efs_csi_policy" {
  type        = bool
  description = "Determines whether to attach the EFS CSI IAM policy to the role"
  default     = false
}

variable "attach_aws_fsx_lustre_csi_policy" {
  type        = bool
  description = "Determines whether to attach the FSx for Lustre CSI Driver IAM policy to the role"
  default     = false
}

variable "attach_aws_gateway_controller_policy" {
  type        = bool
  description = "Determines whether to attach the AWS Gateway Controller IAM policy to the role"
  default     = false
}

variable "attach_aws_lb_controller_policy" {
  type        = bool
  description = "Determines whether to attach the AWS Load Balancer Controller policy to the role"
  default     = false
}

variable "attach_aws_lb_controller_targetgroup_binding_only_policy" {
  type        = bool
  description = "Determines whether to attach the AWS Load Balancer Controller policy for the TargetGroupBinding only"
  default     = false
}

variable "attach_aws_node_termination_handler_policy" {
  type        = bool
  description = "Determines whether to attach the Node Termination Handler policy to the role"
  default     = false
}

variable "attach_aws_privateca_issuer_policy" {
  type        = bool
  description = "Determines whether to attach the AWS Private CA Issuer IAM policy to the role"
  default     = false
}

variable "attach_aws_vpc_cni_policy" {
  type        = bool
  description = "Determines whether to attach the VPC CNI IAM policy to the role"
  default     = false
}

variable "attach_cert_manager_policy" {
  type        = bool
  description = "Determines whether to attach the Cert Manager IAM policy to the role"
  default     = false
}

variable "attach_cluster_autoscaler_policy" {
  type        = bool
  description = "Determines whether to attach the Cluster Autoscaler IAM policy to the role"
  default     = false
}

variable "attach_custom_policy" {
  type        = bool
  description = "Determines whether to attach the custom IAM policy to the role"
  default     = false
}

variable "attach_external_dns_policy" {
  type        = bool
  description = "Determines whether to attach the External DNS IAM policy to the role"
  default     = false
}

variable "attach_external_secrets_policy" {
  type        = bool
  description = "Determines whether to attach the External Secrets policy to the role"
  default     = false
}

variable "attach_mountpoint_s3_csi_policy" {
  type        = bool
  description = "Determines whether to attach the Mountpoint S3 CSI IAM policy to the role"
  default     = false
}

variable "attach_velero_policy" {
  type        = bool
  description = "Determines whether to attach the Velero IAM policy to the role"
  default     = false
}

variable "aws_ebs_csi_kms_arns" {
  type        = list(string)
  description = "KMS key ARNs to allow EBS CSI to manage encrypted volumes"
  default     = []
}

variable "aws_ebs_csi_policy_name" {
  type        = string
  description = "Custom name of the EBS CSI IAM policy"
  default     = null
}

variable "aws_efs_csi_policy_name" {
  type        = string
  description = "Custom name of the EFS CSI IAM policy"
  default     = null
}

variable "aws_fsx_lustre_csi_policy_name" {
  type        = string
  description = "Custom name of the FSx for Lustre CSI Driver IAM policy"
  default     = null
}

variable "aws_fsx_lustre_csi_service_role_arns" {
  type        = list(string)
  description = "Service role ARNs to allow FSx for Lustre CSI create and manage FSX for Lustre service linked roles"
  default     = []
}

variable "aws_gateway_controller_policy_name" {
  type        = string
  description = "Custom name of the AWS Gateway Controller IAM policy"
  default     = null
}

variable "aws_lb_controller_policy_name" {
  type        = string
  description = "Custom name of the AWS Load Balancer Controller IAM policy"
  default     = null
}

variable "aws_lb_controller_targetgroup_arns" {
  type        = list(string)
  description = "List of Target groups ARNs using Load Balancer Controller"
  default     = []
}

variable "aws_lb_controller_targetgroup_only_policy_name" {
  type        = string
  description = "Custom name of the AWS Load Balancer Controller IAM policy for the TargetGroupBinding only"
  default     = null
}

variable "aws_node_termination_handler_policy_name" {
  type        = string
  description = "Custom name of the Node Termination Handler IAM policy"
  default     = null
}

variable "aws_node_termination_handler_sqs_queue_arns" {
  type        = list(string)
  description = "List of SQS ARNs that contain node termination events"
  default     = []
}

variable "aws_privateca_issuer_acmca_arns" {
  type        = list(string)
  description = "List of ACM Private CA ARNs to issue certificates from"
  default     = []
}

variable "aws_privateca_issuer_policy_name" {
  type        = string
  description = "Custom name of the AWS Private CA Issuer IAM policy"
  default     = null
}

variable "aws_vpc_cni_enable_ipv4" {
  type        = bool
  description = "Determines whether to enable IPv4 permissions for VPC CNI policy"
  default     = false
}

variable "aws_vpc_cni_enable_ipv6" {
  type        = bool
  description = "Determines whether to enable IPv6 permissions for VPC CNI policy"
  default     = false
}

variable "aws_vpc_cni_policy_name" {
  type        = string
  description = "Custom name of the VPC CNI IAM policy"
  default     = null
}

variable "cert_manager_hosted_zone_arns" {
  type        = list(string)
  description = "Route53 hosted zone ARNs to allow Cert manager to manage records"
  default     = []
}

variable "cert_manager_policy_name" {
  type        = string
  description = "Custom name of the Cert Manager IAM policy"
  default     = null
}

variable "cluster_autoscaler_cluster_names" {
  type        = list(string)
  description = "List of cluster names to appropriately scope permissions within the Cluster Autoscaler IAM policy"
  default     = []
}

variable "cluster_autoscaler_policy_name" {
  type        = string
  description = "Custom name of the Cluster Autoscaler IAM policy"
  default     = null
}

variable "create" {
  type        = bool
  description = "Determines whether resources will be created (affects all resources)"
  default     = true
}

variable "custom_policy_description" {
  type        = string
  description = "Description of the custom IAM policy"
  default     = "Custom IAM Policy"
}

variable "description" {
  type        = string
  description = "IAM Role description"
  default     = null
}

variable "external_dns_hosted_zone_arns" {
  type        = list(string)
  description = "Route53 hosted zone ARNs to allow External DNS to manage records"
  default     = []
}

variable "external_dns_policy_name" {
  type        = string
  description = "Custom name of the External DNS IAM policy"
  default     = null
}

variable "external_secrets_create_permission" {
  type        = bool
  description = "Determines whether External Secrets has permission to create/delete secrets"
  default     = false
}

variable "external_secrets_kms_key_arns" {
  type        = list(string)
  description = "List of KMS Key ARNs that are used by Secrets Manager that contain secrets to mount using External Secrets"
  default     = []
}

variable "external_secrets_policy_name" {
  type        = string
  description = "Custom name of the External Secrets IAM policy"
  default     = null
}

variable "external_secrets_secrets_manager_arns" {
  type        = list(string)
  description = "List of Secrets Manager ARNs that contain secrets to mount using External Secrets"
  default     = []
}

variable "external_secrets_ssm_parameter_arns" {
  type        = list(string)
  description = "List of Systems Manager Parameter ARNs that contain secrets to mount using External Secrets"
  default     = []
}

variable "max_session_duration" {
  type        = number
  description = "Maximum CLI/API session duration in seconds between 3600 and 43200"
  default     = null
}

variable "mountpoint_s3_csi_bucket_arns" {
  type        = list(string)
  description = "List of S3 Bucket ARNs that Mountpoint S3 CSI needs access to list"
  default     = []
}

variable "mountpoint_s3_csi_bucket_path_arns" {
  type        = list(string)
  description = "S3 path ARNs to allow Mountpoint S3 CSI driver to manage items at the provided path(s). This is required if `attach_mountpoint_s3_csi_policy = true`"
  default     = []
}

variable "mountpoint_s3_csi_policy_name" {
  type        = string
  description = "Custom name of the Mountpoint S3 CSI IAM policy"
  default     = null
}

variable "name" {
  type        = string
  description = "Name of IAM role"
  default     = ""
}

variable "override_policy_documents" {
  type        = list(string)
  description = "List of IAM policy documents that are merged together into the exported document"
  default     = []
}

variable "path" {
  type        = string
  description = "Path of IAM role"
  default     = "/"
}

variable "permissions_boundary_arn" {
  type        = string
  description = "Permissions boundary ARN to use for IAM role"
  default     = null
}

variable "policy_name_prefix" {
  type        = string
  description = "IAM policy name prefix"
  default     = "AmazonEKS_"
}

variable "policy_statements" {
  type        = any
  description = "A list of IAM policy [statements](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document#statement) for custom permission usage"
  default     = []
}

variable "source_policy_documents" {
  type        = list(string)
  description = "List of IAM policy documents that are merged together into the exported document"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "trust_policy_statements" {
  type        = any
  description = "A list of IAM policy [statements](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document#statement) for the role trust policy"
  default     = []
}

variable "use_name_prefix" {
  type        = string
  description = "Determines whether the role name and policy name(s) are used as a prefix"
  default     = true
}

variable "velero_policy_name" {
  type        = string
  description = "Custom name of the Velero IAM policy"
  default     = null
}

variable "velero_s3_bucket_arns" {
  type        = list(string)
  description = "List of S3 Bucket ARNs that Velero needs access to list"
  default     = []
}

variable "velero_s3_bucket_path_arns" {
  type        = list(string)
  description = "S3 path ARNs to allow Velero to manage items at the provided path(s). This is required if `attach_mountpoint_s3_csi_policy = true`"
  default     = []
}


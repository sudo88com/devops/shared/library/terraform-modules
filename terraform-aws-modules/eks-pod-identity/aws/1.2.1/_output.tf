
output "associations" {
  description = "Map of Pod Identity associations created"
  value       = module.eks-pod-identity.associations
}

output "iam_policy_arn" {
  description = "The ARN assigned by AWS to this policy"
  value       = module.eks-pod-identity.iam_policy_arn
}

output "iam_policy_id" {
  description = "The policy's ID"
  value       = module.eks-pod-identity.iam_policy_id
}

output "iam_policy_name" {
  description = "Name of IAM policy"
  value       = module.eks-pod-identity.iam_policy_name
}

output "iam_role_arn" {
  description = "ARN of IAM role"
  value       = module.eks-pod-identity.iam_role_arn
}

output "iam_role_name" {
  description = "Name of IAM role"
  value       = module.eks-pod-identity.iam_role_name
}

output "iam_role_path" {
  description = "Path of IAM role"
  value       = module.eks-pod-identity.iam_role_path
}

output "iam_role_unique_id" {
  description = "Unique ID of IAM role"
  value       = module.eks-pod-identity.iam_role_unique_id
}


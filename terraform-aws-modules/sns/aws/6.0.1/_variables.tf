
variable "application_feedback" {
  type        = map(string)
  description = "Map of IAM role ARNs and sample rate for success and failure feedback"
  default     = {}
}

variable "archive_policy" {
  type        = string
  description = "The message archive policy for FIFO topics."
  default     = null
}

variable "content_based_deduplication" {
  type        = bool
  description = "Boolean indicating whether or not to enable content-based deduplication for FIFO topics."
  default     = false
}

variable "create" {
  type        = bool
  description = "Determines whether resources will be created (affects all resources)"
  default     = true
}

variable "create_subscription" {
  type        = bool
  description = "Determines whether an SNS subscription is created"
  default     = true
}

variable "create_topic_policy" {
  type        = bool
  description = "Determines whether an SNS topic policy is created"
  default     = true
}

variable "data_protection_policy" {
  type        = string
  description = "A map of data protection policy statements"
  default     = null
}

variable "delivery_policy" {
  type        = string
  description = "The SNS delivery policy"
  default     = null
}

variable "display_name" {
  type        = string
  description = "The display name for the SNS topic"
  default     = null
}

variable "enable_default_topic_policy" {
  type        = bool
  description = "Specifies whether to enable the default topic policy. Defaults to `true`"
  default     = true
}

variable "fifo_topic" {
  type        = bool
  description = "Boolean indicating whether or not to create a FIFO (first-in-first-out) topic"
  default     = false
}

variable "firehose_feedback" {
  type        = map(string)
  description = "Map of IAM role ARNs and sample rate for success and failure feedback"
  default     = {}
}

variable "http_feedback" {
  type        = map(string)
  description = "Map of IAM role ARNs and sample rate for success and failure feedback"
  default     = {}
}

variable "kms_master_key_id" {
  type        = string
  description = "The ID of an AWS-managed customer master key (CMK) for Amazon SNS or a custom CMK"
  default     = null
}

variable "lambda_feedback" {
  type        = map(string)
  description = "Map of IAM role ARNs and sample rate for success and failure feedback"
  default     = {}
}

variable "name" {
  type        = string
  description = "The name of the SNS topic to create"
  default     = null
}

variable "override_topic_policy_documents" {
  type        = list(string)
  description = "List of IAM policy documents that are merged together into the exported document. In merging, statements with non-blank `sid`s will override statements with the same `sid`"
  default     = []
}

variable "signature_version" {
  type        = number
  description = "If SignatureVersion should be 1 (SHA1) or 2 (SHA256). The signature version corresponds to the hashing algorithm used while creating the signature of the notifications, subscription confirmations, or unsubscribe confirmation messages sent by Amazon SNS."
  default     = null
}

variable "source_topic_policy_documents" {
  type        = list(string)
  description = "List of IAM policy documents that are merged together into the exported document. Statements must have unique `sid`s"
  default     = []
}

variable "sqs_feedback" {
  type        = map(string)
  description = "Map of IAM role ARNs and sample rate for success and failure feedback"
  default     = {}
}

variable "subscriptions" {
  type        = any
  description = "A map of subscription definitions to create"
  default     = {}
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "topic_policy" {
  type        = string
  description = "An externally created fully-formed AWS policy as JSON"
  default     = null
}

variable "topic_policy_statements" {
  type        = any
  description = "A map of IAM policy [statements](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document#statement) for custom permission usage"
  default     = {}
}

variable "tracing_config" {
  type        = string
  description = "Tracing mode of an Amazon SNS topic. Valid values: PassThrough, Active."
  default     = null
}

variable "use_name_prefix" {
  type        = bool
  description = "Determines whether `name` is used as a prefix"
  default     = false
}


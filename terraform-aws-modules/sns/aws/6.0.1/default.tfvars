
application_feedback = {}

archive_policy = null

content_based_deduplication = false

create = true

create_subscription = true

create_topic_policy = true

data_protection_policy = null

delivery_policy = null

display_name = null

enable_default_topic_policy = true

fifo_topic = false

firehose_feedback = {}

http_feedback = {}

kms_master_key_id = null

lambda_feedback = {}

name = null

override_topic_policy_documents = []

signature_version = null

source_topic_policy_documents = []

sqs_feedback = {}

subscriptions = {}

tags = {}

topic_policy = null

topic_policy_statements = {}

tracing_config = null

use_name_prefix = false



variable "access_entries" {
  type        = any
  description = "Map of access entries to add to the cluster"
  default     = {}
}

variable "attach_cluster_encryption_policy" {
  type        = bool
  description = "Indicates whether or not to attach an additional policy for the cluster IAM role to utilize the encryption key provided"
  default     = true
}

variable "authentication_mode" {
  type        = string
  description = "The authentication mode for the cluster. Valid values are `CONFIG_MAP`, `API` or `API_AND_CONFIG_MAP`"
  default     = "API_AND_CONFIG_MAP"
}

variable "cloudwatch_log_group_class" {
  type        = string
  description = "Specified the log class of the log group. Possible values are: `STANDARD` or `INFREQUENT_ACCESS`"
  default     = null
}

variable "cloudwatch_log_group_kms_key_id" {
  type        = string
  description = "If a KMS Key ARN is set, this key will be used to encrypt the corresponding log group. Please be sure that the KMS Key has an appropriate key policy (https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/encrypt-log-data-kms.html)"
  default     = null
}

variable "cloudwatch_log_group_retention_in_days" {
  type        = number
  description = "Number of days to retain log events. Default retention - 90 days"
  default     = 90
}

variable "cloudwatch_log_group_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the cloudwatch log group created"
  default     = {}
}

variable "cluster_additional_security_group_ids" {
  type        = list(string)
  description = "List of additional, externally created security group IDs to attach to the cluster control plane"
  default     = []
}

variable "cluster_addons" {
  type        = any
  description = "Map of cluster addon configurations to enable for the cluster. Addon name can be the map keys or set with `name`"
  default     = {}
}

variable "cluster_addons_timeouts" {
  type        = map(string)
  description = "Create, update, and delete timeout configurations for the cluster addons"
  default     = {}
}

variable "cluster_enabled_log_types" {
  type        = list(string)
  description = "A list of the desired control plane logs to enable. For more information, see Amazon EKS Control Plane Logging documentation (https://docs.aws.amazon.com/eks/latest/userguide/control-plane-logs.html)"
  default     = [
  "audit",
  "api",
  "authenticator"
]
}

variable "cluster_encryption_config" {
  type        = any
  description = "Configuration block with encryption configuration for the cluster. To disable secret encryption, set this value to `{}`"
  default     = {
  "resources": [
    "secrets"
  ]
}
}

variable "cluster_encryption_policy_description" {
  type        = string
  description = "Description of the cluster encryption policy created"
  default     = "Cluster encryption policy to allow cluster role to utilize CMK provided"
}

variable "cluster_encryption_policy_name" {
  type        = string
  description = "Name to use on cluster encryption policy created"
  default     = null
}

variable "cluster_encryption_policy_path" {
  type        = string
  description = "Cluster encryption policy path"
  default     = null
}

variable "cluster_encryption_policy_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the cluster encryption policy created"
  default     = {}
}

variable "cluster_encryption_policy_use_name_prefix" {
  type        = bool
  description = "Determines whether cluster encryption policy name (`cluster_encryption_policy_name`) is used as a prefix"
  default     = true
}

variable "cluster_endpoint_private_access" {
  type        = bool
  description = "Indicates whether or not the Amazon EKS private API server endpoint is enabled"
  default     = true
}

variable "cluster_endpoint_public_access" {
  type        = bool
  description = "Indicates whether or not the Amazon EKS public API server endpoint is enabled"
  default     = false
}

variable "cluster_endpoint_public_access_cidrs" {
  type        = list(string)
  description = "List of CIDR blocks which can access the Amazon EKS public API server endpoint"
  default     = [
  "0.0.0.0/0"
]
}

variable "cluster_identity_providers" {
  type        = any
  description = "Map of cluster identity provider configurations to enable for the cluster. Note - this is different/separate from IRSA"
  default     = {}
}

variable "cluster_ip_family" {
  type        = string
  description = "The IP family used to assign Kubernetes pod and service addresses. Valid values are `ipv4` (default) and `ipv6`. You can only specify an IP family when you create a cluster, changing this value will force a new cluster to be created"
  default     = "ipv4"
}

variable "cluster_name" {
  type        = string
  description = "Name of the EKS cluster"
  default     = ""
}

variable "cluster_security_group_additional_rules" {
  type        = any
  description = "List of additional security group rules to add to the cluster security group created. Set `source_node_security_group = true` inside rules to set the `node_security_group` as source"
  default     = {}
}

variable "cluster_security_group_description" {
  type        = string
  description = "Description of the cluster security group created"
  default     = "EKS cluster security group"
}

variable "cluster_security_group_id" {
  type        = string
  description = "Existing security group ID to be attached to the cluster"
  default     = ""
}

variable "cluster_security_group_name" {
  type        = string
  description = "Name to use on cluster security group created"
  default     = null
}

variable "cluster_security_group_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the cluster security group created"
  default     = {}
}

variable "cluster_security_group_use_name_prefix" {
  type        = bool
  description = "Determines whether cluster security group name (`cluster_security_group_name`) is used as a prefix"
  default     = true
}

variable "cluster_service_ipv4_cidr" {
  type        = string
  description = "The CIDR block to assign Kubernetes service IP addresses from. If you don't specify a block, Kubernetes assigns addresses from either the 10.100.0.0/16 or 172.20.0.0/16 CIDR blocks"
  default     = null
}

variable "cluster_service_ipv6_cidr" {
  type        = string
  description = "The CIDR block to assign Kubernetes pod and service IP addresses from if `ipv6` was specified when the cluster was created. Kubernetes assigns service addresses from the unique local address range (fc00::/7) because you can't specify a custom IPv6 CIDR block when you create the cluster"
  default     = null
}

variable "cluster_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the cluster"
  default     = {}
}

variable "cluster_timeouts" {
  type        = map(string)
  description = "Create, update, and delete timeout configurations for the cluster"
  default     = {}
}

variable "cluster_version" {
  type        = string
  description = "Kubernetes `<major>.<minor>` version to use for the EKS cluster (i.e.: `1.27`)"
  default     = null
}

variable "control_plane_subnet_ids" {
  type        = list(string)
  description = "A list of subnet IDs where the EKS cluster control plane (ENIs) will be provisioned. Used for expanding the pool of subnets used by nodes/node groups without replacing the EKS control plane"
  default     = []
}

variable "create" {
  type        = bool
  description = "Controls if resources should be created (affects nearly all resources)"
  default     = true
}

variable "create_cloudwatch_log_group" {
  type        = bool
  description = "Determines whether a log group is created by this module for the cluster logs. If not, AWS will automatically create one if logging is enabled"
  default     = true
}

variable "create_cluster_primary_security_group_tags" {
  type        = bool
  description = "Indicates whether or not to tag the cluster's primary security group. This security group is created by the EKS service, not the module, and therefore tagging is handled after cluster creation"
  default     = true
}

variable "create_cluster_security_group" {
  type        = bool
  description = "Determines if a security group is created for the cluster. Note: the EKS service creates a primary security group for the cluster by default"
  default     = true
}

variable "create_cni_ipv6_iam_policy" {
  type        = bool
  description = "Determines whether to create an [`AmazonEKS_CNI_IPv6_Policy`](https://docs.aws.amazon.com/eks/latest/userguide/cni-iam-role.html#cni-iam-role-create-ipv6-policy)"
  default     = false
}

variable "create_iam_role" {
  type        = bool
  description = "Determines whether a an IAM role is created or to use an existing IAM role"
  default     = true
}

variable "create_kms_key" {
  type        = bool
  description = "Controls if a KMS key for cluster encryption should be created"
  default     = true
}

variable "create_node_security_group" {
  type        = bool
  description = "Determines whether to create a security group for the node groups or use the existing `node_security_group_id`"
  default     = true
}

variable "custom_oidc_thumbprints" {
  type        = list(string)
  description = "Additional list of server certificate thumbprints for the OpenID Connect (OIDC) identity provider's server certificate(s)"
  default     = []
}

variable "dataplane_wait_duration" {
  type        = string
  description = "Duration to wait after the EKS cluster has become active before creating the dataplane components (EKS managed nodegroup(s), self-managed nodegroup(s), Fargate profile(s))"
  default     = "30s"
}

variable "eks_managed_node_group_defaults" {
  type        = any
  description = "Map of EKS managed node group default configurations"
  default     = {}
}

variable "eks_managed_node_groups" {
  type        = any
  description = "Map of EKS managed node group definitions to create"
  default     = {}
}

variable "enable_cluster_creator_admin_permissions" {
  type        = bool
  description = "Indicates whether or not to add the cluster creator (the identity used by Terraform) as an administrator via access entry"
  default     = false
}

variable "enable_efa_support" {
  type        = bool
  description = "Determines whether to enable Elastic Fabric Adapter (EFA) support"
  default     = false
}

variable "enable_irsa" {
  type        = bool
  description = "Determines whether to create an OpenID Connect Provider for EKS to enable IRSA"
  default     = true
}

variable "enable_kms_key_rotation" {
  type        = bool
  description = "Specifies whether key rotation is enabled"
  default     = true
}

variable "fargate_profile_defaults" {
  type        = any
  description = "Map of Fargate Profile default configurations"
  default     = {}
}

variable "fargate_profiles" {
  type        = any
  description = "Map of Fargate Profile definitions to create"
  default     = {}
}

variable "iam_role_additional_policies" {
  type        = map(string)
  description = "Additional policies to be added to the IAM role"
  default     = {}
}

variable "iam_role_arn" {
  type        = string
  description = "Existing IAM role ARN for the cluster. Required if `create_iam_role` is set to `false`"
  default     = null
}

variable "iam_role_description" {
  type        = string
  description = "Description of the role"
  default     = null
}

variable "iam_role_name" {
  type        = string
  description = "Name to use on IAM role created"
  default     = null
}

variable "iam_role_path" {
  type        = string
  description = "Cluster IAM role path"
  default     = null
}

variable "iam_role_permissions_boundary" {
  type        = string
  description = "ARN of the policy that is used to set the permissions boundary for the IAM role"
  default     = null
}

variable "iam_role_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the IAM role created"
  default     = {}
}

variable "iam_role_use_name_prefix" {
  type        = bool
  description = "Determines whether the IAM role name (`iam_role_name`) is used as a prefix"
  default     = true
}

variable "include_oidc_root_ca_thumbprint" {
  type        = bool
  description = "Determines whether to include the root CA thumbprint in the OpenID Connect (OIDC) identity provider's server certificate(s)"
  default     = true
}

variable "kms_key_administrators" {
  type        = list(string)
  description = "A list of IAM ARNs for [key administrators](https://docs.aws.amazon.com/kms/latest/developerguide/key-policy-default.html#key-policy-default-allow-administrators). If no value is provided, the current caller identity is used to ensure at least one key admin is available"
  default     = []
}

variable "kms_key_aliases" {
  type        = list(string)
  description = "A list of aliases to create. Note - due to the use of `toset()`, values must be static strings and not computed values"
  default     = []
}

variable "kms_key_deletion_window_in_days" {
  type        = number
  description = "The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `30`"
  default     = null
}

variable "kms_key_description" {
  type        = string
  description = "The description of the key as viewed in AWS console"
  default     = null
}

variable "kms_key_enable_default_policy" {
  type        = bool
  description = "Specifies whether to enable the default key policy"
  default     = true
}

variable "kms_key_override_policy_documents" {
  type        = list(string)
  description = "List of IAM policy documents that are merged together into the exported document. In merging, statements with non-blank `sid`s will override statements with the same `sid`"
  default     = []
}

variable "kms_key_owners" {
  type        = list(string)
  description = "A list of IAM ARNs for those who will have full key permissions (`kms:*`)"
  default     = []
}

variable "kms_key_service_users" {
  type        = list(string)
  description = "A list of IAM ARNs for [key service users](https://docs.aws.amazon.com/kms/latest/developerguide/key-policy-default.html#key-policy-service-integration)"
  default     = []
}

variable "kms_key_source_policy_documents" {
  type        = list(string)
  description = "List of IAM policy documents that are merged together into the exported document. Statements must have unique `sid`s"
  default     = []
}

variable "kms_key_users" {
  type        = list(string)
  description = "A list of IAM ARNs for [key users](https://docs.aws.amazon.com/kms/latest/developerguide/key-policy-default.html#key-policy-default-allow-users)"
  default     = []
}

variable "node_security_group_additional_rules" {
  type        = any
  description = "List of additional security group rules to add to the node security group created. Set `source_cluster_security_group = true` inside rules to set the `cluster_security_group` as source"
  default     = {}
}

variable "node_security_group_description" {
  type        = string
  description = "Description of the node security group created"
  default     = "EKS node shared security group"
}

variable "node_security_group_enable_recommended_rules" {
  type        = bool
  description = "Determines whether to enable recommended security group rules for the node security group created. This includes node-to-node TCP ingress on ephemeral ports and allows all egress traffic"
  default     = true
}

variable "node_security_group_id" {
  type        = string
  description = "ID of an existing security group to attach to the node groups created"
  default     = ""
}

variable "node_security_group_name" {
  type        = string
  description = "Name to use on node security group created"
  default     = null
}

variable "node_security_group_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the node security group created"
  default     = {}
}

variable "node_security_group_use_name_prefix" {
  type        = bool
  description = "Determines whether node security group name (`node_security_group_name`) is used as a prefix"
  default     = true
}

variable "openid_connect_audiences" {
  type        = list(string)
  description = "List of OpenID Connect audience client IDs to add to the IRSA provider"
  default     = []
}

variable "outpost_config" {
  type        = any
  description = "Configuration for the AWS Outpost to provision the cluster on"
  default     = {}
}

variable "prefix_separator" {
  type        = string
  description = "The separator to use between the prefix and the generated timestamp for resource names"
  default     = "-"
}

variable "putin_khuylo" {
  type        = bool
  description = "Do you agree that Putin doesn't respect Ukrainian sovereignty and territorial integrity? More info: https://en.wikipedia.org/wiki/Putin_khuylo!"
  default     = true
}

variable "self_managed_node_group_defaults" {
  type        = any
  description = "Map of self-managed node group default configurations"
  default     = {}
}

variable "self_managed_node_groups" {
  type        = any
  description = "Map of self-managed node group definitions to create"
  default     = {}
}

variable "subnet_ids" {
  type        = list(string)
  description = "A list of subnet IDs where the nodes/node groups will be provisioned. If `control_plane_subnet_ids` is not provided, the EKS cluster control plane (ENIs) will be provisioned in these subnets"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "vpc_id" {
  type        = string
  description = "ID of the VPC where the cluster security group will be provisioned"
  default     = null
}


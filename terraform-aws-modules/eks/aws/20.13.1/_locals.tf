
locals {
  access_entries = var.access_entries
  attach_cluster_encryption_policy = var.attach_cluster_encryption_policy
  authentication_mode = var.authentication_mode
  cloudwatch_log_group_class = var.cloudwatch_log_group_class
  cloudwatch_log_group_kms_key_id = var.cloudwatch_log_group_kms_key_id
  cloudwatch_log_group_retention_in_days = var.cloudwatch_log_group_retention_in_days
  cloudwatch_log_group_tags = var.cloudwatch_log_group_tags
  cluster_additional_security_group_ids = var.cluster_additional_security_group_ids
  cluster_addons = var.cluster_addons
  cluster_addons_timeouts = var.cluster_addons_timeouts
  cluster_enabled_log_types = var.cluster_enabled_log_types
  cluster_encryption_config = var.cluster_encryption_config
  cluster_encryption_policy_description = var.cluster_encryption_policy_description
  cluster_encryption_policy_name = var.cluster_encryption_policy_name
  cluster_encryption_policy_path = var.cluster_encryption_policy_path
  cluster_encryption_policy_tags = var.cluster_encryption_policy_tags
  cluster_encryption_policy_use_name_prefix = var.cluster_encryption_policy_use_name_prefix
  cluster_endpoint_private_access = var.cluster_endpoint_private_access
  cluster_endpoint_public_access = var.cluster_endpoint_public_access
  cluster_endpoint_public_access_cidrs = var.cluster_endpoint_public_access_cidrs
  cluster_identity_providers = var.cluster_identity_providers
  cluster_ip_family = var.cluster_ip_family
  cluster_name = var.cluster_name
  cluster_security_group_additional_rules = var.cluster_security_group_additional_rules
  cluster_security_group_description = var.cluster_security_group_description
  cluster_security_group_id = var.cluster_security_group_id
  cluster_security_group_name = var.cluster_security_group_name
  cluster_security_group_tags = var.cluster_security_group_tags
  cluster_security_group_use_name_prefix = var.cluster_security_group_use_name_prefix
  cluster_service_ipv4_cidr = var.cluster_service_ipv4_cidr
  cluster_service_ipv6_cidr = var.cluster_service_ipv6_cidr
  cluster_tags = var.cluster_tags
  cluster_timeouts = var.cluster_timeouts
  cluster_version = var.cluster_version
  control_plane_subnet_ids = var.control_plane_subnet_ids
  create = var.create
  create_cloudwatch_log_group = var.create_cloudwatch_log_group
  create_cluster_primary_security_group_tags = var.create_cluster_primary_security_group_tags
  create_cluster_security_group = var.create_cluster_security_group
  create_cni_ipv6_iam_policy = var.create_cni_ipv6_iam_policy
  create_iam_role = var.create_iam_role
  create_kms_key = var.create_kms_key
  create_node_security_group = var.create_node_security_group
  custom_oidc_thumbprints = var.custom_oidc_thumbprints
  dataplane_wait_duration = var.dataplane_wait_duration
  eks_managed_node_group_defaults = var.eks_managed_node_group_defaults
  eks_managed_node_groups = var.eks_managed_node_groups
  enable_cluster_creator_admin_permissions = var.enable_cluster_creator_admin_permissions
  enable_efa_support = var.enable_efa_support
  enable_irsa = var.enable_irsa
  enable_kms_key_rotation = var.enable_kms_key_rotation
  fargate_profile_defaults = var.fargate_profile_defaults
  fargate_profiles = var.fargate_profiles
  iam_role_additional_policies = var.iam_role_additional_policies
  iam_role_arn = var.iam_role_arn
  iam_role_description = var.iam_role_description
  iam_role_name = var.iam_role_name
  iam_role_path = var.iam_role_path
  iam_role_permissions_boundary = var.iam_role_permissions_boundary
  iam_role_tags = var.iam_role_tags
  iam_role_use_name_prefix = var.iam_role_use_name_prefix
  include_oidc_root_ca_thumbprint = var.include_oidc_root_ca_thumbprint
  kms_key_administrators = var.kms_key_administrators
  kms_key_aliases = var.kms_key_aliases
  kms_key_deletion_window_in_days = var.kms_key_deletion_window_in_days
  kms_key_description = var.kms_key_description
  kms_key_enable_default_policy = var.kms_key_enable_default_policy
  kms_key_override_policy_documents = var.kms_key_override_policy_documents
  kms_key_owners = var.kms_key_owners
  kms_key_service_users = var.kms_key_service_users
  kms_key_source_policy_documents = var.kms_key_source_policy_documents
  kms_key_users = var.kms_key_users
  node_security_group_additional_rules = var.node_security_group_additional_rules
  node_security_group_description = var.node_security_group_description
  node_security_group_enable_recommended_rules = var.node_security_group_enable_recommended_rules
  node_security_group_id = var.node_security_group_id
  node_security_group_name = var.node_security_group_name
  node_security_group_tags = var.node_security_group_tags
  node_security_group_use_name_prefix = var.node_security_group_use_name_prefix
  openid_connect_audiences = var.openid_connect_audiences
  outpost_config = var.outpost_config
  prefix_separator = var.prefix_separator
  putin_khuylo = var.putin_khuylo
  self_managed_node_group_defaults = var.self_managed_node_group_defaults
  self_managed_node_groups = var.self_managed_node_groups
  subnet_ids = var.subnet_ids
  tags = var.tags
  vpc_id = var.vpc_id
}

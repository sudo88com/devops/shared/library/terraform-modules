
variable "access_logs" {
  type        = map(string)
  description = "Map containing access logging configuration for load balancer"
  default     = {}
}

variable "additional_target_group_attachments" {
  type        = any
  description = "Map of additional target group attachments to create. Use `target_group_key` to attach to the target group created in `target_groups`"
  default     = {}
}

variable "associate_web_acl" {
  type        = bool
  description = "Indicates whether a Web Application Firewall (WAF) ACL should be associated with the load balancer"
  default     = false
}

variable "client_keep_alive" {
  type        = number
  description = "Client keep alive value in seconds. The valid range is 60-604800 seconds. The default is 3600 seconds."
  default     = null
}

variable "connection_logs" {
  type        = map(string)
  description = "Map containing access logging configuration for load balancer"
  default     = {}
}

variable "create" {
  type        = bool
  description = "Controls if resources should be created (affects nearly all resources)"
  default     = true
}

variable "create_security_group" {
  type        = bool
  description = "Determines if a security group is created"
  default     = true
}

variable "customer_owned_ipv4_pool" {
  type        = string
  description = "The ID of the customer owned ipv4 pool to use for this load balancer"
  default     = null
}

variable "default_port" {
  type        = number
  description = "Default port used across the listener and target group"
  default     = 80
}

variable "default_protocol" {
  type        = string
  description = "Default protocol used across the listener and target group"
  default     = "HTTP"
}

variable "desync_mitigation_mode" {
  type        = string
  description = "Determines how the load balancer handles requests that might pose a security risk to an application due to HTTP desync. Valid values are `monitor`, `defensive` (default), `strictest`"
  default     = null
}

variable "dns_record_client_routing_policy" {
  type        = string
  description = "Indicates how traffic is distributed among the load balancer Availability Zones. Possible values are any_availability_zone (default), availability_zone_affinity, or partial_availability_zone_affinity. Only valid for network type load balancers."
  default     = null
}

variable "drop_invalid_header_fields" {
  type        = bool
  description = "Indicates whether HTTP headers with header fields that are not valid are removed by the load balancer (`true`) or routed to targets (`false`). The default is `true`. Elastic Load Balancing requires that message header names contain only alphanumeric characters and hyphens. Only valid for Load Balancers of type `application`"
  default     = true
}

variable "enable_cross_zone_load_balancing" {
  type        = bool
  description = "If `true`, cross-zone load balancing of the load balancer will be enabled. For application load balancer this feature is always enabled (`true`) and cannot be disabled. Defaults to `true`"
  default     = true
}

variable "enable_deletion_protection" {
  type        = bool
  description = "If `true`, deletion of the load balancer will be disabled via the AWS API. This will prevent Terraform from deleting the load balancer. Defaults to `true`"
  default     = true
}

variable "enable_http2" {
  type        = bool
  description = "Indicates whether HTTP/2 is enabled in application load balancers. Defaults to `true`"
  default     = null
}

variable "enable_tls_version_and_cipher_suite_headers" {
  type        = bool
  description = "Indicates whether the two headers (`x-amzn-tls-version` and `x-amzn-tls-cipher-suite`), which contain information about the negotiated TLS version and cipher suite, are added to the client request before sending it to the target. Only valid for Load Balancers of type `application`. Defaults to `false`"
  default     = null
}

variable "enable_waf_fail_open" {
  type        = bool
  description = "Indicates whether to allow a WAF-enabled load balancer to route requests to targets if it is unable to forward the request to AWS WAF. Defaults to `false`"
  default     = null
}

variable "enable_xff_client_port" {
  type        = bool
  description = "Indicates whether the X-Forwarded-For header should preserve the source port that the client used to connect to the load balancer in `application` load balancers. Defaults to `false`"
  default     = null
}

variable "enforce_security_group_inbound_rules_on_private_link_traffic" {
  type        = string
  description = "Indicates whether inbound security group rules are enforced for traffic originating from a PrivateLink. Only valid for Load Balancers of type network. The possible values are on and off."
  default     = null
}

variable "idle_timeout" {
  type        = number
  description = "The time in seconds that the connection is allowed to be idle. Only valid for Load Balancers of type `application`. Default: `60`"
  default     = null
}

variable "internal" {
  type        = bool
  description = "If true, the LB will be internal. Defaults to `false`"
  default     = null
}

variable "ip_address_type" {
  type        = string
  description = "The type of IP addresses used by the subnets for your load balancer. The possible values are `ipv4` and `dualstack`"
  default     = null
}

variable "listeners" {
  type        = any
  description = "Map of listener configurations to create"
  default     = {}
}

variable "load_balancer_type" {
  type        = string
  description = "The type of load balancer to create. Possible values are `application`, `gateway`, or `network`. The default value is `application`"
  default     = "application"
}

variable "name" {
  type        = string
  description = "The name of the LB. This name must be unique within your AWS account, can have a maximum of 32 characters, must contain only alphanumeric characters or hyphens, and must not begin or end with a hyphen"
  default     = null
}

variable "name_prefix" {
  type        = string
  description = "Creates a unique name beginning with the specified prefix. Conflicts with `name`"
  default     = null
}

variable "preserve_host_header" {
  type        = bool
  description = "Indicates whether the Application Load Balancer should preserve the Host header in the HTTP request and send it to the target without any change. Defaults to `false`"
  default     = null
}

variable "putin_khuylo" {
  type        = bool
  description = "Do you agree that Putin doesn't respect Ukrainian sovereignty and territorial integrity? More info: https://en.wikipedia.org/wiki/Putin_khuylo!"
  default     = true
}

variable "route53_records" {
  type        = any
  description = "Map of Route53 records to create. Each record map should contain `zone_id`, `name`, and `type`"
  default     = {}
}

variable "security_group_description" {
  type        = string
  description = "Description of the security group created"
  default     = null
}

variable "security_group_egress_rules" {
  type        = any
  description = "Security group egress rules to add to the security group created"
  default     = {}
}

variable "security_group_ingress_rules" {
  type        = any
  description = "Security group ingress rules to add to the security group created"
  default     = {}
}

variable "security_group_name" {
  type        = string
  description = "Name to use on security group created"
  default     = null
}

variable "security_group_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the security group created"
  default     = {}
}

variable "security_group_use_name_prefix" {
  type        = bool
  description = "Determines whether the security group name (`security_group_name`) is used as a prefix"
  default     = true
}

variable "security_groups" {
  type        = list(string)
  description = "A list of security group IDs to assign to the LB"
  default     = []
}

variable "subnet_mapping" {
  type        = list(map(string))
  description = "A list of subnet mapping blocks describing subnets to attach to load balancer"
  default     = []
}

variable "subnets" {
  type        = list(string)
  description = "A list of subnet IDs to attach to the LB. Subnets cannot be updated for Load Balancers of type `network`. Changing this value for load balancers of type `network` will force a recreation of the resource"
  default     = null
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "target_groups" {
  type        = any
  description = "Map of target group configurations to create"
  default     = {}
}

variable "timeouts" {
  type        = map(string)
  description = "Create, update, and delete timeout configurations for the load balancer"
  default     = {}
}

variable "vpc_id" {
  type        = string
  description = "Identifier of the VPC where the security group will be created"
  default     = null
}

variable "web_acl_arn" {
  type        = string
  description = "Web Application Firewall (WAF) ARN of the resource to associate with the load balancer"
  default     = null
}

variable "xff_header_processing_mode" {
  type        = string
  description = "Determines how the load balancer modifies the X-Forwarded-For header in the HTTP request before sending the request to the target. The possible values are `append`, `preserve`, and `remove`. Only valid for Load Balancers of type `application`. The default is `append`"
  default     = null
}


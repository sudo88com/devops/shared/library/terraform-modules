
access_logs = {}

additional_target_group_attachments = {}

associate_web_acl = false

client_keep_alive = null

connection_logs = {}

create = true

create_security_group = true

customer_owned_ipv4_pool = null

default_port = 80

default_protocol = "HTTP"

desync_mitigation_mode = null

dns_record_client_routing_policy = null

drop_invalid_header_fields = true

enable_cross_zone_load_balancing = true

enable_deletion_protection = true

enable_http2 = null

enable_tls_version_and_cipher_suite_headers = null

enable_waf_fail_open = null

enable_xff_client_port = null

enforce_security_group_inbound_rules_on_private_link_traffic = null

idle_timeout = null

internal = null

ip_address_type = null

listeners = {}

load_balancer_type = "application"

name = null

name_prefix = null

preserve_host_header = null

putin_khuylo = true

route53_records = {}

security_group_description = null

security_group_egress_rules = {}

security_group_ingress_rules = {}

security_group_name = null

security_group_tags = {}

security_group_use_name_prefix = true

security_groups = []

subnet_mapping = []

subnets = null

tags = {}

target_groups = {}

timeouts = {}

vpc_id = null

web_acl_arn = null

xff_header_processing_mode = null


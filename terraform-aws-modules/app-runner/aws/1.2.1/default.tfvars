
access_iam_role_description = null

access_iam_role_name = null

access_iam_role_path = null

access_iam_role_permissions_boundary = null

access_iam_role_policies = {}

access_iam_role_use_name_prefix = true

auto_scaling_configuration_arn = null

auto_scaling_configurations = {}

connections = {}

create = true

create_access_iam_role = false

create_custom_domain_association = false

create_ingress_vpc_connection = false

create_instance_iam_role = true

create_service = true

create_vpc_connector = false

domain_name = ""

enable_observability_configuration = true

enable_www_subdomain = null

encryption_configuration = {}

health_check_configuration = {}

ingress_vpc_endpoint_id = ""

ingress_vpc_id = ""

instance_configuration = {}

instance_iam_role_description = null

instance_iam_role_name = null

instance_iam_role_path = null

instance_iam_role_permissions_boundary = null

instance_iam_role_policies = {}

instance_iam_role_use_name_prefix = true

instance_policy_statements = {}

network_configuration = {}

observability_configuration = {}

private_ecr_arn = null

service_name = ""

source_configuration = {}

tags = {}

vpc_connector_name = ""

vpc_connector_security_groups = []

vpc_connector_subnets = []


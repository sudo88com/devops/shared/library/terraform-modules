
output "access_iam_role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the IAM role"
  value       = module.app-runner.access_iam_role_arn
}

output "access_iam_role_name" {
  description = "The name of the IAM role"
  value       = module.app-runner.access_iam_role_name
}

output "access_iam_role_unique_id" {
  description = "Stable and unique string identifying the IAM role"
  value       = module.app-runner.access_iam_role_unique_id
}

output "auto_scaling_configurations" {
  description = "Map of attribute maps for all autoscaling configurations created"
  value       = module.app-runner.auto_scaling_configurations
}

output "connections" {
  description = "Map of attribute maps for all connections created"
  value       = module.app-runner.connections
}

output "custom_domain_association_certificate_validation_records" {
  description = "A set of certificate CNAME records used for this domain name"
  value       = module.app-runner.custom_domain_association_certificate_validation_records
}

output "custom_domain_association_dns_target" {
  description = "The App Runner subdomain of the App Runner service. The custom domain name is mapped to this target name. Attribute only available if resource created (not imported) with Terraform"
  value       = module.app-runner.custom_domain_association_dns_target
}

output "custom_domain_association_id" {
  description = "The `domain_name` and `service_arn` separated by a comma (`,`)"
  value       = module.app-runner.custom_domain_association_id
}

output "instance_iam_role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the IAM role"
  value       = module.app-runner.instance_iam_role_arn
}

output "instance_iam_role_name" {
  description = "The name of the IAM role"
  value       = module.app-runner.instance_iam_role_name
}

output "instance_iam_role_unique_id" {
  description = "Stable and unique string identifying the IAM role"
  value       = module.app-runner.instance_iam_role_unique_id
}

output "observability_configuration_arn" {
  description = "ARN of this observability configuration"
  value       = module.app-runner.observability_configuration_arn
}

output "observability_configuration_latest" {
  description = "Whether the observability configuration has the highest `observability_configuration_revision` among all configurations that share the same `observability_configuration_name`"
  value       = module.app-runner.observability_configuration_latest
}

output "observability_configuration_revision" {
  description = "The revision of the observability configuration"
  value       = module.app-runner.observability_configuration_revision
}

output "observability_configuration_status" {
  description = "The current state of the observability configuration. An `INACTIVE` configuration revision has been deleted and can't be used. It is permanently removed some time after deletion"
  value       = module.app-runner.observability_configuration_status
}

output "service_arn" {
  description = "The Amazon Resource Name (ARN) of the service"
  value       = module.app-runner.service_arn
}

output "service_id" {
  description = "An alphanumeric ID that App Runner generated for this service. Unique within the AWS Region"
  value       = module.app-runner.service_id
}

output "service_status" {
  description = "The current state of the App Runner service"
  value       = module.app-runner.service_status
}

output "service_url" {
  description = "A subdomain URL that App Runner generated for this service. You can use this URL to access your service web application"
  value       = module.app-runner.service_url
}

output "vpc_connector_arn" {
  description = "The Amazon Resource Name (ARN) of VPC connector"
  value       = module.app-runner.vpc_connector_arn
}

output "vpc_connector_revision" {
  description = "The revision of VPC connector. It's unique among all the active connectors (\"Status\": \"ACTIVE\") that share the same Name"
  value       = module.app-runner.vpc_connector_revision
}

output "vpc_connector_status" {
  description = "The current state of the VPC connector. If the status of a connector revision is INACTIVE, it was deleted and can't be used. Inactive connector revisions are permanently removed some time after they are deleted"
  value       = module.app-runner.vpc_connector_status
}

output "vpc_ingress_connection_arn" {
  description = "The Amazon Resource Name (ARN) of the VPC Ingress Connection"
  value       = module.app-runner.vpc_ingress_connection_arn
}

output "vpc_ingress_connection_domain_name" {
  description = "The domain name associated with the VPC Ingress Connection resource"
  value       = module.app-runner.vpc_ingress_connection_domain_name
}


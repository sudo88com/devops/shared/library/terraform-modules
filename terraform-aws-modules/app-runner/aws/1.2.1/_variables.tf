
variable "access_iam_role_description" {
  type        = string
  description = "Description of the role"
  default     = null
}

variable "access_iam_role_name" {
  type        = string
  description = "Name to use on IAM role created"
  default     = null
}

variable "access_iam_role_path" {
  type        = string
  description = "IAM role path"
  default     = null
}

variable "access_iam_role_permissions_boundary" {
  type        = string
  description = "ARN of the policy that is used to set the permissions boundary for the IAM role"
  default     = null
}

variable "access_iam_role_policies" {
  type        = map(string)
  description = "IAM policies to attach to the IAM role"
  default     = {}
}

variable "access_iam_role_use_name_prefix" {
  type        = bool
  description = "Determines whether the IAM role name (`iam_role_name`) is used as a prefix"
  default     = true
}

variable "auto_scaling_configuration_arn" {
  type        = string
  description = "ARN of an App Runner automatic scaling configuration resource that you want to associate with your service. If not provided, App Runner associates the latest revision of a default auto scaling configuration"
  default     = null
}

variable "auto_scaling_configurations" {
  type        = any
  description = "Map of auto-scaling configuration definitions to create"
  default     = {}
}

variable "connections" {
  type        = any
  description = "Map of connection definitions to create"
  default     = {}
}

variable "create" {
  type        = bool
  description = "Determines whether resources will be created (affects all resources)"
  default     = true
}

variable "create_access_iam_role" {
  type        = bool
  description = "Determines whether an IAM role is created or to use an existing IAM role"
  default     = false
}

variable "create_custom_domain_association" {
  type        = bool
  description = "Determines whether a Custom Domain Association will be created"
  default     = false
}

variable "create_ingress_vpc_connection" {
  type        = bool
  description = "Determines whether a VPC ingress configuration will be created"
  default     = false
}

variable "create_instance_iam_role" {
  type        = bool
  description = "Determines whether an IAM role is created or to use an existing IAM role"
  default     = true
}

variable "create_service" {
  type        = bool
  description = "Determines whether the service will be created"
  default     = true
}

variable "create_vpc_connector" {
  type        = bool
  description = "Determines whether a VPC Connector will be created"
  default     = false
}

variable "domain_name" {
  type        = string
  description = "The custom domain endpoint to association. Specify a base domain e.g., `example.com` or a subdomain e.g., `subdomain.example.com`"
  default     = ""
}

variable "enable_observability_configuration" {
  type        = bool
  description = "Determines whether an X-Ray Observability Configuration will be created and assigned to the service"
  default     = true
}

variable "enable_www_subdomain" {
  type        = bool
  description = "Whether to associate the subdomain with the App Runner service in addition to the base domain. Defaults to `true`"
  default     = null
}

variable "encryption_configuration" {
  type        = any
  description = "The encryption configuration for the service"
  default     = {}
}

variable "health_check_configuration" {
  type        = any
  description = "The health check configuration for the service"
  default     = {}
}

variable "ingress_vpc_endpoint_id" {
  type        = string
  description = "The ID of the VPC endpoint that is used for the VPC ingress configuration"
  default     = ""
}

variable "ingress_vpc_id" {
  type        = string
  description = "The ID of the VPC that is used for the VPC ingress configuration"
  default     = ""
}

variable "instance_configuration" {
  type        = any
  description = "The instance configuration for the service"
  default     = {}
}

variable "instance_iam_role_description" {
  type        = string
  description = "Description of the role"
  default     = null
}

variable "instance_iam_role_name" {
  type        = string
  description = "Name to use on IAM role created"
  default     = null
}

variable "instance_iam_role_path" {
  type        = string
  description = "IAM role path"
  default     = null
}

variable "instance_iam_role_permissions_boundary" {
  type        = string
  description = "ARN of the policy that is used to set the permissions boundary for the IAM role"
  default     = null
}

variable "instance_iam_role_policies" {
  type        = map(string)
  description = "IAM policies to attach to the IAM role"
  default     = {}
}

variable "instance_iam_role_use_name_prefix" {
  type        = bool
  description = "Determines whether the IAM role name (`iam_role_name`) is used as a prefix"
  default     = true
}

variable "instance_policy_statements" {
  type        = any
  description = "A map of IAM policy [statements](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document#statement) for custom permission usage"
  default     = {}
}

variable "network_configuration" {
  type        = any
  description = "The network configuration for the service"
  default     = {}
}

variable "observability_configuration" {
  type        = any
  description = "The observability configuration for the service"
  default     = {}
}

variable "private_ecr_arn" {
  type        = string
  description = "The ARN of the private ECR repository that contains the service image to launch"
  default     = null
}

variable "service_name" {
  type        = string
  description = "The name of the service"
  default     = ""
}

variable "source_configuration" {
  type        = any
  description = "The source configuration for the service"
  default     = {}
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "vpc_connector_name" {
  type        = string
  description = "The name of the VPC Connector"
  default     = ""
}

variable "vpc_connector_security_groups" {
  type        = list(string)
  description = "The security groups to use for the VPC Connector"
  default     = []
}

variable "vpc_connector_subnets" {
  type        = list(string)
  description = "The subnets to use for the VPC Connector"
  default     = []
}



autoscaling_group_tags = {}

availability_zones = null

block_device_mappings = []

capacity_rebalance = null

capacity_reservation_specification = {}

cpu_options = {}

create = true

create_iam_instance_profile = false

create_launch_template = true

create_scaling_policy = true

create_schedule = true

create_traffic_source_attachment = false

credit_specification = {}

default_cooldown = null

default_instance_warmup = null

default_version = null

delete_timeout = null

desired_capacity = null

desired_capacity_type = null

disable_api_stop = null

disable_api_termination = null

ebs_optimized = null

elastic_gpu_specifications = {}

elastic_inference_accelerator = {}

enable_monitoring = true

enabled_metrics = []

enclave_options = {}

force_delete = null

health_check_grace_period = null

health_check_type = null

hibernation_options = {}

iam_instance_profile_arn = null

iam_instance_profile_name = null

iam_role_description = null

iam_role_name = null

iam_role_path = null

iam_role_permissions_boundary = null

iam_role_policies = {}

iam_role_tags = {}

iam_role_use_name_prefix = true

ignore_desired_capacity_changes = false

ignore_failed_scaling_activities = false

image_id = ""

initial_lifecycle_hooks = []

instance_initiated_shutdown_behavior = null

instance_maintenance_policy = {}

instance_market_options = {}

instance_name = ""

instance_refresh = {}

instance_requirements = {}

instance_type = null

kernel_id = null

key_name = null

launch_template_description = null

launch_template_id = null

launch_template_name = ""

launch_template_use_name_prefix = true

launch_template_version = null

license_specifications = {}

load_balancers = []

maintenance_options = {}

max_instance_lifetime = null

max_size = null

metadata_options = {}

metrics_granularity = null

min_elb_capacity = null

min_size = null

mixed_instances_policy = null

name = 

network_interfaces = []

placement = {}

placement_group = null

private_dns_name_options = {}

protect_from_scale_in = false

putin_khuylo = true

ram_disk_id = null

scaling_policies = {}

schedules = {}

security_groups = []

service_linked_role_arn = null

suspended_processes = []

tag_specifications = []

tags = {}

target_group_arns = []

termination_policies = []

traffic_source_identifier = ""

traffic_source_type = "elbv2"

update_default_version = null

use_mixed_instances_policy = false

use_name_prefix = true

user_data = null

vpc_zone_identifier = null

wait_for_capacity_timeout = null

wait_for_elb_capacity = null

warm_pool = {}


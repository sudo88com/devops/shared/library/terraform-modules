
variable "autoscaling_group_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the autoscaling group"
  default     = {}
}

variable "availability_zones" {
  type        = list(string)
  description = "A list of one or more availability zones for the group. Used for EC2-Classic and default subnets when not specified with `vpc_zone_identifier` argument. Conflicts with `vpc_zone_identifier`"
  default     = null
}

variable "block_device_mappings" {
  type        = list(any)
  description = "Specify volumes to attach to the instance besides the volumes specified by the AMI"
  default     = []
}

variable "capacity_rebalance" {
  type        = bool
  description = "Indicates whether capacity rebalance is enabled"
  default     = null
}

variable "capacity_reservation_specification" {
  type        = any
  description = "Targeting for EC2 capacity reservations"
  default     = {}
}

variable "cpu_options" {
  type        = map(string)
  description = "The CPU options for the instance"
  default     = {}
}

variable "create" {
  type        = bool
  description = "Determines whether to create autoscaling group or not"
  default     = true
}

variable "create_iam_instance_profile" {
  type        = bool
  description = "Determines whether an IAM instance profile is created or to use an existing IAM instance profile"
  default     = false
}

variable "create_launch_template" {
  type        = bool
  description = "Determines whether to create launch template or not"
  default     = true
}

variable "create_scaling_policy" {
  type        = bool
  description = "Determines whether to create target scaling policy schedule or not"
  default     = true
}

variable "create_schedule" {
  type        = bool
  description = "Determines whether to create autoscaling group schedule or not"
  default     = true
}

variable "create_traffic_source_attachment" {
  type        = bool
  description = "Determines whether to create autoscaling group traffic source attachment"
  default     = false
}

variable "credit_specification" {
  type        = map(string)
  description = "Customize the credit specification of the instance"
  default     = {}
}

variable "default_cooldown" {
  type        = number
  description = "The amount of time, in seconds, after a scaling activity completes before another scaling activity can start"
  default     = null
}

variable "default_instance_warmup" {
  type        = number
  description = "Amount of time, in seconds, until a newly launched instance can contribute to the Amazon CloudWatch metrics. This delay lets an instance finish initializing before Amazon EC2 Auto Scaling aggregates instance metrics, resulting in more reliable usage data. Set this value equal to the amount of time that it takes for resource consumption to become stable after an instance reaches the InService state."
  default     = null
}

variable "default_version" {
  type        = string
  description = "Default Version of the launch template"
  default     = null
}

variable "delete_timeout" {
  type        = string
  description = "Delete timeout to wait for destroying autoscaling group"
  default     = null
}

variable "desired_capacity" {
  type        = number
  description = "The number of Amazon EC2 instances that should be running in the autoscaling group"
  default     = null
}

variable "desired_capacity_type" {
  type        = string
  description = "The unit of measurement for the value specified for desired_capacity. Supported for attribute-based instance type selection only. Valid values: `units`, `vcpu`, `memory-mib`."
  default     = null
}

variable "disable_api_stop" {
  type        = bool
  description = "If true, enables EC2 instance stop protection"
  default     = null
}

variable "disable_api_termination" {
  type        = bool
  description = "If true, enables EC2 instance termination protection"
  default     = null
}

variable "ebs_optimized" {
  type        = bool
  description = "If true, the launched EC2 instance will be EBS-optimized"
  default     = null
}

variable "elastic_gpu_specifications" {
  type        = map(string)
  description = "The elastic GPU to attach to the instance"
  default     = {}
}

variable "elastic_inference_accelerator" {
  type        = map(string)
  description = "Configuration block containing an Elastic Inference Accelerator to attach to the instance"
  default     = {}
}

variable "enable_monitoring" {
  type        = bool
  description = "Enables/disables detailed monitoring"
  default     = true
}

variable "enabled_metrics" {
  type        = list(string)
  description = "A list of metrics to collect. The allowed values are `GroupDesiredCapacity`, `GroupInServiceCapacity`, `GroupPendingCapacity`, `GroupMinSize`, `GroupMaxSize`, `GroupInServiceInstances`, `GroupPendingInstances`, `GroupStandbyInstances`, `GroupStandbyCapacity`, `GroupTerminatingCapacity`, `GroupTerminatingInstances`, `GroupTotalCapacity`, `GroupTotalInstances`"
  default     = []
}

variable "enclave_options" {
  type        = map(string)
  description = "Enable Nitro Enclaves on launched instances"
  default     = {}
}

variable "force_delete" {
  type        = bool
  description = "Allows deleting the Auto Scaling Group without waiting for all instances in the pool to terminate. You can force an Auto Scaling Group to delete even if it's in the process of scaling a resource. Normally, Terraform drains all the instances before deleting the group. This bypasses that behavior and potentially leaves resources dangling"
  default     = null
}

variable "health_check_grace_period" {
  type        = number
  description = "Time (in seconds) after instance comes into service before checking health"
  default     = null
}

variable "health_check_type" {
  type        = string
  description = "`EC2` or `ELB`. Controls how health checking is done"
  default     = null
}

variable "hibernation_options" {
  type        = map(string)
  description = "The hibernation options for the instance"
  default     = {}
}

variable "iam_instance_profile_arn" {
  type        = string
  description = "Amazon Resource Name (ARN) of an existing IAM instance profile. Used when `create_iam_instance_profile` = `false`"
  default     = null
}

variable "iam_instance_profile_name" {
  type        = string
  description = "The name of the IAM instance profile to be created (`create_iam_instance_profile` = `true`) or existing (`create_iam_instance_profile` = `false`)"
  default     = null
}

variable "iam_role_description" {
  type        = string
  description = "Description of the role"
  default     = null
}

variable "iam_role_name" {
  type        = string
  description = "Name to use on IAM role created"
  default     = null
}

variable "iam_role_path" {
  type        = string
  description = "IAM role path"
  default     = null
}

variable "iam_role_permissions_boundary" {
  type        = string
  description = "ARN of the policy that is used to set the permissions boundary for the IAM role"
  default     = null
}

variable "iam_role_policies" {
  type        = map(string)
  description = "IAM policies to attach to the IAM role"
  default     = {}
}

variable "iam_role_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the IAM role created"
  default     = {}
}

variable "iam_role_use_name_prefix" {
  type        = bool
  description = "Determines whether the IAM role name (`iam_role_name`) is used as a prefix"
  default     = true
}

variable "ignore_desired_capacity_changes" {
  type        = bool
  description = "Determines whether the `desired_capacity` value is ignored after initial apply. See README note for more details"
  default     = false
}

variable "ignore_failed_scaling_activities" {
  type        = bool
  description = "Whether to ignore failed Auto Scaling scaling activities while waiting for capacity. The default is false -- failed scaling activities cause errors to be returned."
  default     = false
}

variable "image_id" {
  type        = string
  description = "The AMI from which to launch the instance"
  default     = ""
}

variable "initial_lifecycle_hooks" {
  type        = list(map(string))
  description = "One or more Lifecycle Hooks to attach to the Auto Scaling Group before instances are launched. The syntax is exactly the same as the separate `aws_autoscaling_lifecycle_hook` resource, without the `autoscaling_group_name` attribute. Please note that this will only work when creating a new Auto Scaling Group. For all other use-cases, please use `aws_autoscaling_lifecycle_hook` resource"
  default     = []
}

variable "instance_initiated_shutdown_behavior" {
  type        = string
  description = "Shutdown behavior for the instance. Can be `stop` or `terminate`. (Default: `stop`)"
  default     = null
}

variable "instance_maintenance_policy" {
  type        = map(any)
  description = "If this block is configured, add a instance maintenance policy to the specified Auto Scaling group"
  default     = {}
}

variable "instance_market_options" {
  type        = any
  description = "The market (purchasing) option for the instance"
  default     = {}
}

variable "instance_name" {
  type        = string
  description = "Name that is propogated to launched EC2 instances via a tag - if not provided, defaults to `var.name`"
  default     = ""
}

variable "instance_refresh" {
  type        = any
  description = "If this block is configured, start an Instance Refresh when this Auto Scaling Group is updated"
  default     = {}
}

variable "instance_requirements" {
  type        = any
  description = "The attribute requirements for the type of instance. If present then `instance_type` cannot be present"
  default     = {}
}

variable "instance_type" {
  type        = string
  description = "The type of the instance. If present then `instance_requirements` cannot be present"
  default     = null
}

variable "kernel_id" {
  type        = string
  description = "The kernel ID"
  default     = null
}

variable "key_name" {
  type        = string
  description = "The key name that should be used for the instance"
  default     = null
}

variable "launch_template_description" {
  type        = string
  description = "Description of the launch template"
  default     = null
}

variable "launch_template_id" {
  type        = string
  description = "ID of an existing launch template to be used (created outside of this module)"
  default     = null
}

variable "launch_template_name" {
  type        = string
  description = "Name of launch template to be created"
  default     = ""
}

variable "launch_template_use_name_prefix" {
  type        = bool
  description = "Determines whether to use `launch_template_name` as is or create a unique name beginning with the `launch_template_name` as the prefix"
  default     = true
}

variable "launch_template_version" {
  type        = string
  description = "Launch template version. Can be version number, `$Latest`, or `$Default`"
  default     = null
}

variable "license_specifications" {
  type        = map(string)
  description = "A list of license specifications to associate with"
  default     = {}
}

variable "load_balancers" {
  type        = list(string)
  description = "A list of elastic load balancer names to add to the autoscaling group names. Only valid for classic load balancers. For ALBs, use `target_group_arns` instead"
  default     = []
}

variable "maintenance_options" {
  type        = any
  description = "The maintenance options for the instance"
  default     = {}
}

variable "max_instance_lifetime" {
  type        = number
  description = "The maximum amount of time, in seconds, that an instance can be in service, values must be either equal to 0 or between 86400 and 31536000 seconds"
  default     = null
}

variable "max_size" {
  type        = number
  description = "The maximum size of the autoscaling group"
  default     = null
}

variable "metadata_options" {
  type        = map(string)
  description = "Customize the metadata options for the instance"
  default     = {}
}

variable "metrics_granularity" {
  type        = string
  description = "The granularity to associate with the metrics to collect. The only valid value is `1Minute`"
  default     = null
}

variable "min_elb_capacity" {
  type        = number
  description = "Setting this causes Terraform to wait for this number of instances to show up healthy in the ELB only on creation. Updates will not wait on ELB instance number changes"
  default     = null
}

variable "min_size" {
  type        = number
  description = "The minimum size of the autoscaling group"
  default     = null
}

variable "mixed_instances_policy" {
  type        = any
  description = "Configuration block containing settings to define launch targets for Auto Scaling groups"
  default     = null
}

variable "name" {
  type        = string
  description = "Name used across the resources created"
  default     = ""
}

variable "network_interfaces" {
  type        = list(any)
  description = "Customize network interfaces to be attached at instance boot time"
  default     = []
}

variable "placement" {
  type        = map(string)
  description = "The placement of the instance"
  default     = {}
}

variable "placement_group" {
  type        = string
  description = "The name of the placement group into which you'll launch your instances, if any"
  default     = null
}

variable "private_dns_name_options" {
  type        = map(string)
  description = "The options for the instance hostname. The default values are inherited from the subnet"
  default     = {}
}

variable "protect_from_scale_in" {
  type        = bool
  description = "Allows setting instance protection. The autoscaling group will not select instances with this setting for termination during scale in events."
  default     = false
}

variable "putin_khuylo" {
  type        = bool
  description = "Do you agree that Putin doesn't respect Ukrainian sovereignty and territorial integrity? More info: https://en.wikipedia.org/wiki/Putin_khuylo!"
  default     = true
}

variable "ram_disk_id" {
  type        = string
  description = "The ID of the ram disk"
  default     = null
}

variable "scaling_policies" {
  type        = any
  description = "Map of target scaling policy schedule to create"
  default     = {}
}

variable "schedules" {
  type        = map(any)
  description = "Map of autoscaling group schedule to create"
  default     = {}
}

variable "security_groups" {
  type        = list(string)
  description = "A list of security group IDs to associate"
  default     = []
}

variable "service_linked_role_arn" {
  type        = string
  description = "The ARN of the service-linked role that the ASG will use to call other AWS services"
  default     = null
}

variable "suspended_processes" {
  type        = list(string)
  description = "A list of processes to suspend for the Auto Scaling Group. The allowed values are `Launch`, `Terminate`, `HealthCheck`, `ReplaceUnhealthy`, `AZRebalance`, `AlarmNotification`, `ScheduledActions`, `AddToLoadBalancer`, `InstanceRefresh`. Note that if you suspend either the `Launch` or `Terminate` process types, it can prevent your Auto Scaling Group from functioning properly"
  default     = []
}

variable "tag_specifications" {
  type        = list(any)
  description = "The tags to apply to the resources during launch"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to assign to resources"
  default     = {}
}

variable "target_group_arns" {
  type        = list(string)
  description = "A set of `aws_alb_target_group` ARNs, for use with Application or Network Load Balancing"
  default     = []
}

variable "termination_policies" {
  type        = list(string)
  description = "A list of policies to decide how the instances in the Auto Scaling Group should be terminated. The allowed values are `OldestInstance`, `NewestInstance`, `OldestLaunchConfiguration`, `ClosestToNextInstanceHour`, `OldestLaunchTemplate`, `AllocationStrategy`, `Default`"
  default     = []
}

variable "traffic_source_identifier" {
  type        = string
  description = "Identifies the traffic source. For Application Load Balancers, Gateway Load Balancers, Network Load Balancers, and VPC Lattice, this will be the Amazon Resource Name (ARN) for a target group in this account and Region. For Classic Load Balancers, this will be the name of the Classic Load Balancer in this account and Region"
  default     = ""
}

variable "traffic_source_type" {
  type        = string
  description = "Provides additional context for the value of identifier. The following lists the valid values: `elb` if `identifier` is the name of a Classic Load Balancer. `elbv2` if `identifier` is the ARN of an Application Load Balancer, Gateway Load Balancer, or Network Load Balancer target group. `vpc-lattice` if `identifier` is the ARN of a VPC Lattice target group"
  default     = "elbv2"
}

variable "update_default_version" {
  type        = string
  description = "Whether to update Default Version each update. Conflicts with `default_version`"
  default     = null
}

variable "use_mixed_instances_policy" {
  type        = bool
  description = "Determines whether to use a mixed instances policy in the autoscaling group or not"
  default     = false
}

variable "use_name_prefix" {
  type        = bool
  description = "Determines whether to use `name` as is or create a unique name beginning with the `name` as the prefix"
  default     = true
}

variable "user_data" {
  type        = string
  description = "The Base64-encoded user data to provide when launching the instance"
  default     = null
}

variable "vpc_zone_identifier" {
  type        = list(string)
  description = "A list of subnet IDs to launch resources in. Subnets automatically determine which availability zones the group will reside. Conflicts with `availability_zones`"
  default     = null
}

variable "wait_for_capacity_timeout" {
  type        = string
  description = "A maximum duration that Terraform should wait for ASG instances to be healthy before timing out. (See also Waiting for Capacity below.) Setting this to '0' causes Terraform to skip all Capacity Waiting behavior."
  default     = null
}

variable "wait_for_elb_capacity" {
  type        = number
  description = "Setting this will cause Terraform to wait for exactly this number of healthy instances in all attached load balancers on both create and update operations. Takes precedence over `min_elb_capacity` behavior."
  default     = null
}

variable "warm_pool" {
  type        = any
  description = "If this block is configured, add a Warm Pool to the specified Auto Scaling group"
  default     = {}
}


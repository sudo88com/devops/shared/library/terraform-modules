
variable "aliases" {
  type        = list(string)
  description = "A list of aliases to create. Note - due to the use of `toset()`, values must be static strings and not computed values"
  default     = []
}

variable "aliases_use_name_prefix" {
  type        = bool
  description = "Determines whether the alias name is used as a prefix"
  default     = false
}

variable "bypass_policy_lockout_safety_check" {
  type        = bool
  description = "A flag to indicate whether to bypass the key policy lockout safety check. Setting this value to true increases the risk that the KMS key becomes unmanageable"
  default     = null
}

variable "computed_aliases" {
  type        = any
  description = "A map of aliases to create. Values provided via the `name` key of the map can be computed from upstream resources"
  default     = {}
}

variable "create" {
  type        = bool
  description = "Determines whether resources will be created (affects all resources)"
  default     = true
}

variable "create_external" {
  type        = bool
  description = "Determines whether an external CMK (externally provided material) will be created or a standard CMK (AWS provided material)"
  default     = false
}

variable "create_replica" {
  type        = bool
  description = "Determines whether a replica standard CMK will be created (AWS provided material)"
  default     = false
}

variable "create_replica_external" {
  type        = bool
  description = "Determines whether a replica external CMK will be created (externally provided material)"
  default     = false
}

variable "custom_key_store_id" {
  type        = string
  description = "ID of the KMS Custom Key Store where the key will be stored instead of KMS (eg CloudHSM)."
  default     = null
}

variable "customer_master_key_spec" {
  type        = string
  description = "Specifies whether the key contains a symmetric key or an asymmetric key pair and the encryption algorithms or signing algorithms that the key supports. Valid values: `SYMMETRIC_DEFAULT`, `RSA_2048`, `RSA_3072`, `RSA_4096`, `HMAC_256`, `ECC_NIST_P256`, `ECC_NIST_P384`, `ECC_NIST_P521`, or `ECC_SECG_P256K1`. Defaults to `SYMMETRIC_DEFAULT`"
  default     = null
}

variable "deletion_window_in_days" {
  type        = number
  description = "The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `30`"
  default     = null
}

variable "description" {
  type        = string
  description = "The description of the key as viewed in AWS console"
  default     = null
}

variable "enable_default_policy" {
  type        = bool
  description = "Specifies whether to enable the default key policy. Defaults to `true`"
  default     = true
}

variable "enable_key_rotation" {
  type        = bool
  description = "Specifies whether key rotation is enabled. Defaults to `true`"
  default     = true
}

variable "enable_route53_dnssec" {
  type        = bool
  description = "Determines whether the KMS policy used for Route53 DNSSEC signing is enabled"
  default     = false
}

variable "grants" {
  type        = any
  description = "A map of grant definitions to create"
  default     = {}
}

variable "is_enabled" {
  type        = bool
  description = "Specifies whether the key is enabled. Defaults to `true`"
  default     = null
}

variable "key_administrators" {
  type        = list(string)
  description = "A list of IAM ARNs for [key administrators](https://docs.aws.amazon.com/kms/latest/developerguide/key-policy-default.html#key-policy-default-allow-administrators)"
  default     = []
}

variable "key_asymmetric_public_encryption_users" {
  type        = list(string)
  description = "A list of IAM ARNs for [key asymmetric public encryption users](https://docs.aws.amazon.com/kms/latest/developerguide/key-policy-default.html#key-policy-users-crypto)"
  default     = []
}

variable "key_asymmetric_sign_verify_users" {
  type        = list(string)
  description = "A list of IAM ARNs for [key asymmetric sign and verify users](https://docs.aws.amazon.com/kms/latest/developerguide/key-policy-default.html#key-policy-users-crypto)"
  default     = []
}

variable "key_hmac_users" {
  type        = list(string)
  description = "A list of IAM ARNs for [key HMAC users](https://docs.aws.amazon.com/kms/latest/developerguide/key-policy-default.html#key-policy-users-crypto)"
  default     = []
}

variable "key_material_base64" {
  type        = string
  description = "Base64 encoded 256-bit symmetric encryption key material to import. The CMK is permanently associated with this key material. External key only"
  default     = null
}

variable "key_owners" {
  type        = list(string)
  description = "A list of IAM ARNs for those who will have full key permissions (`kms:*`)"
  default     = []
}

variable "key_service_roles_for_autoscaling" {
  type        = list(string)
  description = "A list of IAM ARNs for [AWSServiceRoleForAutoScaling roles](https://docs.aws.amazon.com/autoscaling/ec2/userguide/key-policy-requirements-EBS-encryption.html#policy-example-cmk-access)"
  default     = []
}

variable "key_service_users" {
  type        = list(string)
  description = "A list of IAM ARNs for [key service users](https://docs.aws.amazon.com/kms/latest/developerguide/key-policy-default.html#key-policy-service-integration)"
  default     = []
}

variable "key_statements" {
  type        = any
  description = "A map of IAM policy [statements](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document#statement) for custom permission usage"
  default     = {}
}

variable "key_symmetric_encryption_users" {
  type        = list(string)
  description = "A list of IAM ARNs for [key symmetric encryption users](https://docs.aws.amazon.com/kms/latest/developerguide/key-policy-default.html#key-policy-users-crypto)"
  default     = []
}

variable "key_usage" {
  type        = string
  description = "Specifies the intended use of the key. Valid values: `ENCRYPT_DECRYPT` or `SIGN_VERIFY`. Defaults to `ENCRYPT_DECRYPT`"
  default     = null
}

variable "key_users" {
  type        = list(string)
  description = "A list of IAM ARNs for [key users](https://docs.aws.amazon.com/kms/latest/developerguide/key-policy-default.html#key-policy-default-allow-users)"
  default     = []
}

variable "multi_region" {
  type        = bool
  description = "Indicates whether the KMS key is a multi-Region (`true`) or regional (`false`) key. Defaults to `false`"
  default     = false
}

variable "override_policy_documents" {
  type        = list(string)
  description = "List of IAM policy documents that are merged together into the exported document. In merging, statements with non-blank `sid`s will override statements with the same `sid`"
  default     = []
}

variable "policy" {
  type        = string
  description = "A valid policy JSON document. Although this is a key policy, not an IAM policy, an `aws_iam_policy_document`, in the form that designates a principal, can be used"
  default     = null
}

variable "primary_external_key_arn" {
  type        = string
  description = "The primary external key arn of a multi-region replica external key"
  default     = null
}

variable "primary_key_arn" {
  type        = string
  description = "The primary key arn of a multi-region replica key"
  default     = null
}

variable "rotation_period_in_days" {
  type        = number
  description = "Custom period of time between each rotation date. Must be a number between 90 and 2560 (inclusive)"
  default     = null
}

variable "route53_dnssec_sources" {
  type        = list(any)
  description = "A list of maps containing `account_ids` and Route53 `hosted_zone_arn` that will be allowed to sign DNSSEC records"
  default     = []
}

variable "source_policy_documents" {
  type        = list(string)
  description = "List of IAM policy documents that are merged together into the exported document. Statements must have unique `sid`s"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "valid_to" {
  type        = string
  description = "Time at which the imported key material expires. When the key material expires, AWS KMS deletes the key material and the CMK becomes unusable. If not specified, key material does not expire"
  default     = null
}


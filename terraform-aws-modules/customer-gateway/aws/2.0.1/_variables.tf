
variable "create" {
  type        = bool
  description = "Whether to create Customer Gateway resources"
  default     = true
}

variable "customer_gateways" {
  type        = map(map(any))
  description = "Maps of Customer Gateway's attributes (BGP ASN and Gateway's Internet-routable external IP address)"
  default     = {}
}

variable "name" {
  type        = string
  description = "Name to be used on all the resources as identifier"
  default     = ""
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to all resources"
  default     = {}
}


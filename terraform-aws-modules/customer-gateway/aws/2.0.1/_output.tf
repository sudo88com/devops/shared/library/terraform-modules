
output "customer_gateway" {
  description = "Map of Customer Gateway attributes"
  value       = module.customer-gateway.customer_gateway
}

output "ids" {
  description = "List of IDs of Customer Gateway"
  value       = module.customer-gateway.ids
}


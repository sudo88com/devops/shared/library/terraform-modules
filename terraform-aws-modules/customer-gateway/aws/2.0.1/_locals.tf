
locals {
  create = var.create
  customer_gateways = var.customer_gateways
  name = var.name
  tags = var.tags
}

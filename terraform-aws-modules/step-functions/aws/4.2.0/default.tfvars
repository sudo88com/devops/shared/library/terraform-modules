
attach_cloudwatch_logs_policy = true

attach_policies = false

attach_policies_for_integrations = true

attach_policy = false

attach_policy_json = false

attach_policy_jsons = false

attach_policy_statements = false

aws_region_assume_role = ""

cloudwatch_log_group_kms_key_id = null

cloudwatch_log_group_name = null

cloudwatch_log_group_retention_in_days = null

cloudwatch_log_group_tags = {}

create = true

create_role = true

definition = ""

logging_configuration = {}

name = ""

number_of_policies = 0

number_of_policy_jsons = 0

policies = []

policy = null

policy_json = null

policy_jsons = []

policy_path = null

policy_statements = {}

publish = false

role_arn = ""

role_description = null

role_force_detach_policies = true

role_name = null

role_path = null

role_permissions_boundary = null

role_tags = {}

service_integrations = {}

sfn_state_machine_timeouts = {}

tags = {}

trusted_entities = []

type = "STANDARD"

use_existing_cloudwatch_log_group = false

use_existing_role = false


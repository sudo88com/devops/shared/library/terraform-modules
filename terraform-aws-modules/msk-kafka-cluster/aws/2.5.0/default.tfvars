
broker_node_az_distribution = null

broker_node_client_subnets = []

broker_node_connectivity_info = {}

broker_node_instance_type = null

broker_node_security_groups = []

broker_node_storage_info = {}

client_authentication = {}

cloudwatch_log_group_kms_key_id = null

cloudwatch_log_group_name = null

cloudwatch_log_group_retention_in_days = 0

cloudwatch_logs_enabled = false

cluster_override_policy_documents = null

cluster_policy_statements = null

cluster_source_policy_documents = null

configuration_arn = null

configuration_description = null

configuration_name = null

configuration_revision = null

configuration_server_properties = {}

connect_custom_plugin_timeouts = {
  "create": null
}

connect_custom_plugins = {}

connect_worker_config_description = null

connect_worker_config_name = null

connect_worker_config_properties_file_content = null

create = true

create_cloudwatch_log_group = true

create_cluster_policy = false

create_configuration = true

create_connect_worker_configuration = false

create_schema_registry = true

create_scram_secret_association = false

enable_storage_autoscaling = true

encryption_at_rest_kms_key_arn = null

encryption_in_transit_client_broker = null

encryption_in_transit_in_cluster = null

enhanced_monitoring = null

firehose_delivery_stream = null

firehose_logs_enabled = false

jmx_exporter_enabled = false

kafka_version = null

name = "msk"

node_exporter_enabled = false

number_of_broker_nodes = null

s3_logs_bucket = null

s3_logs_enabled = false

s3_logs_prefix = null

scaling_max_capacity = 250

scaling_role_arn = null

scaling_target_value = 70

schema_registries = {}

schemas = {}

scram_secret_association_secret_arn_list = []

storage_mode = null

tags = {}

timeouts = {}

vpc_connections = {}



variable "broker_node_az_distribution" {
  type        = string
  description = "The distribution of broker nodes across availability zones ([documentation](https://docs.aws.amazon.com/msk/1.0/apireference/clusters.html#clusters-model-brokerazdistribution)). Currently the only valid value is `DEFAULT`"
  default     = null
}

variable "broker_node_client_subnets" {
  type        = list(string)
  description = "A list of subnets to connect to in client VPC ([documentation](https://docs.aws.amazon.com/msk/1.0/apireference/clusters.html#clusters-prop-brokernodegroupinfo-clientsubnets))"
  default     = []
}

variable "broker_node_connectivity_info" {
  type        = any
  description = "Information about the cluster access configuration"
  default     = {}
}

variable "broker_node_instance_type" {
  type        = string
  description = "Specify the instance type to use for the kafka brokers. e.g. kafka.m5.large. ([Pricing info](https://aws.amazon.com/msk/pricing/))"
  default     = null
}

variable "broker_node_security_groups" {
  type        = list(string)
  description = "A list of the security groups to associate with the elastic network interfaces to control who can communicate with the cluster"
  default     = []
}

variable "broker_node_storage_info" {
  type        = any
  description = "A block that contains information about storage volumes attached to MSK broker nodes"
  default     = {}
}

variable "client_authentication" {
  type        = any
  description = "Configuration block for specifying a client authentication"
  default     = {}
}

variable "cloudwatch_log_group_kms_key_id" {
  type        = string
  description = "The ARN of the KMS Key to use when encrypting log data"
  default     = null
}

variable "cloudwatch_log_group_name" {
  type        = string
  description = "Name of the Cloudwatch Log Group to deliver logs to"
  default     = null
}

variable "cloudwatch_log_group_retention_in_days" {
  type        = number
  description = "Specifies the number of days you want to retain log events in the log group"
  default     = 0
}

variable "cloudwatch_logs_enabled" {
  type        = bool
  description = "Indicates whether you want to enable or disable streaming broker logs to Cloudwatch Logs"
  default     = false
}

variable "cluster_override_policy_documents" {
  type        = list(string)
  description = "Override policy documents for cluster policy"
  default     = null
}

variable "cluster_policy_statements" {
  type        = any
  description = "Map of policy statements for cluster policy"
  default     = null
}

variable "cluster_source_policy_documents" {
  type        = list(string)
  description = "Source policy documents for cluster policy"
  default     = null
}

variable "configuration_arn" {
  type        = string
  description = "ARN of an externally created configuration to use"
  default     = null
}

variable "configuration_description" {
  type        = string
  description = "Description of the configuration"
  default     = null
}

variable "configuration_name" {
  type        = string
  description = "Name of the configuration"
  default     = null
}

variable "configuration_revision" {
  type        = number
  description = "Revision of the externally created configuration to use"
  default     = null
}

variable "configuration_server_properties" {
  type        = map(string)
  description = "Contents of the server.properties file. Supported properties are documented in the [MSK Developer Guide](https://docs.aws.amazon.com/msk/latest/developerguide/msk-configuration-properties.html)"
  default     = {}
}

variable "connect_custom_plugin_timeouts" {
  type        = map(string)
  description = "Timeout configurations for the connect custom plugins"
  default     = {
  "create": null
}
}

variable "connect_custom_plugins" {
  type        = any
  description = "Map of custom plugin configuration details (map of maps)"
  default     = {}
}

variable "connect_worker_config_description" {
  type        = string
  description = "A summary description of the worker configuration"
  default     = null
}

variable "connect_worker_config_name" {
  type        = string
  description = "The name of the worker configuration"
  default     = null
}

variable "connect_worker_config_properties_file_content" {
  type        = string
  description = "Contents of connect-distributed.properties file. The value can be either base64 encoded or in raw format"
  default     = null
}

variable "create" {
  type        = bool
  description = "Determines whether cluster resources will be created"
  default     = true
}

variable "create_cloudwatch_log_group" {
  type        = bool
  description = "Determines whether to create a CloudWatch log group"
  default     = true
}

variable "create_cluster_policy" {
  type        = bool
  description = "Determines whether to create an MSK cluster policy"
  default     = false
}

variable "create_configuration" {
  type        = bool
  description = "Determines whether to create a configuration"
  default     = true
}

variable "create_connect_worker_configuration" {
  type        = bool
  description = "Determines whether to create connect worker configuration"
  default     = false
}

variable "create_schema_registry" {
  type        = bool
  description = "Determines whether to create a Glue schema registry for managing Avro schemas for the cluster"
  default     = true
}

variable "create_scram_secret_association" {
  type        = bool
  description = "Determines whether to create SASL/SCRAM secret association"
  default     = false
}

variable "enable_storage_autoscaling" {
  type        = bool
  description = "Determines whether autoscaling is enabled for storage"
  default     = true
}

variable "encryption_at_rest_kms_key_arn" {
  type        = string
  description = "You may specify a KMS key short ID or ARN (it will always output an ARN) to use for encrypting your data at rest. If no key is specified, an AWS managed KMS ('aws/msk' managed service) key will be used for encrypting the data at rest"
  default     = null
}

variable "encryption_in_transit_client_broker" {
  type        = string
  description = "Encryption setting for data in transit between clients and brokers. Valid values: `TLS`, `TLS_PLAINTEXT`, and `PLAINTEXT`. Default value is `TLS`"
  default     = null
}

variable "encryption_in_transit_in_cluster" {
  type        = bool
  description = "Whether data communication among broker nodes is encrypted. Default value: `true`"
  default     = null
}

variable "enhanced_monitoring" {
  type        = string
  description = "Specify the desired enhanced MSK CloudWatch monitoring level. See [Monitoring Amazon MSK with Amazon CloudWatch](https://docs.aws.amazon.com/msk/latest/developerguide/monitoring.html)"
  default     = null
}

variable "firehose_delivery_stream" {
  type        = string
  description = "Name of the Kinesis Data Firehose delivery stream to deliver logs to"
  default     = null
}

variable "firehose_logs_enabled" {
  type        = bool
  description = "Indicates whether you want to enable or disable streaming broker logs to Kinesis Data Firehose"
  default     = false
}

variable "jmx_exporter_enabled" {
  type        = bool
  description = "Indicates whether you want to enable or disable the JMX Exporter"
  default     = false
}

variable "kafka_version" {
  type        = string
  description = "Specify the desired Kafka software version"
  default     = null
}

variable "name" {
  type        = string
  description = "Name of the MSK cluster"
  default     = "msk"
}

variable "node_exporter_enabled" {
  type        = bool
  description = "Indicates whether you want to enable or disable the Node Exporter"
  default     = false
}

variable "number_of_broker_nodes" {
  type        = number
  description = "The desired total number of broker nodes in the kafka cluster. It must be a multiple of the number of specified client subnets"
  default     = null
}

variable "s3_logs_bucket" {
  type        = string
  description = "Name of the S3 bucket to deliver logs to"
  default     = null
}

variable "s3_logs_enabled" {
  type        = bool
  description = "Indicates whether you want to enable or disable streaming broker logs to S3"
  default     = false
}

variable "s3_logs_prefix" {
  type        = string
  description = "Prefix to append to the folder name"
  default     = null
}

variable "scaling_max_capacity" {
  type        = number
  description = "Max storage capacity for Kafka broker autoscaling"
  default     = 250
}

variable "scaling_role_arn" {
  type        = string
  description = "The ARN of the IAM role that allows Application AutoScaling to modify your scalable target on your behalf. This defaults to an IAM Service-Linked Role"
  default     = null
}

variable "scaling_target_value" {
  type        = number
  description = "The Kafka broker storage utilization at which scaling is initiated"
  default     = 70
}

variable "schema_registries" {
  type        = map(any)
  description = "A map of schema registries to be created"
  default     = {}
}

variable "schemas" {
  type        = map(any)
  description = "A map schemas to be created within the schema registry"
  default     = {}
}

variable "scram_secret_association_secret_arn_list" {
  type        = list(string)
  description = "List of AWS Secrets Manager secret ARNs to associate with SCRAM"
  default     = []
}

variable "storage_mode" {
  type        = string
  description = "Controls storage mode for supported storage tiers. Valid values are: `LOCAL` or `TIERED`"
  default     = null
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to assign to the resources created"
  default     = {}
}

variable "timeouts" {
  type        = map(string)
  description = "Create, update, and delete timeout configurations for the cluster"
  default     = {}
}

variable "vpc_connections" {
  type        = any
  description = "Map of VPC Connections to create"
  default     = {}
}


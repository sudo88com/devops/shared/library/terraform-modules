
variable "create" {
  type        = bool
  description = "Controls if resources should be created"
  default     = true
}

variable "create_logging_configuration" {
  type        = bool
  description = "Controls if a Logging Configuration should be created"
  default     = false
}

variable "create_policy" {
  type        = bool
  description = "Controls if policy should be created"
  default     = true
}

variable "create_policy_resource_policy" {
  type        = bool
  description = "Controls if a resource policy should be created"
  default     = false
}

variable "delete_protection" {
  type        = bool
  description = "A boolean flag indicating whether it is possible to delete the firewall. Defaults to `true`"
  default     = true
}

variable "description" {
  type        = string
  description = "A friendly description of the firewall"
  default     = ""
}

variable "encryption_configuration" {
  type        = any
  description = "KMS encryption configuration settings"
  default     = {}
}

variable "firewall_policy_arn" {
  type        = string
  description = "The ARN of the Firewall Policy to use"
  default     = ""
}

variable "firewall_policy_change_protection" {
  type        = bool
  description = "A boolean flag indicating whether it is possible to change the associated firewall policy. Defaults to `false`"
  default     = null
}

variable "logging_configuration_destination_config" {
  type        = any
  description = "A list of min 1, max 2 configuration blocks describing the destination for the logging configuration"
  default     = []
}

variable "name" {
  type        = string
  description = "A friendly name of the firewall"
  default     = ""
}

variable "policy_attach_resource_policy" {
  type        = bool
  description = "Controls if a resource policy should be attached to the firewall policy"
  default     = false
}

variable "policy_description" {
  type        = string
  description = "A friendly description of the firewall policy"
  default     = null
}

variable "policy_encryption_configuration" {
  type        = any
  description = "KMS encryption configuration settings"
  default     = {}
}

variable "policy_name" {
  type        = string
  description = "A friendly name of the firewall policy"
  default     = ""
}

variable "policy_ram_resource_associations" {
  type        = map(string)
  description = "A map of RAM resource associations for the created firewall policy"
  default     = {}
}

variable "policy_resource_policy" {
  type        = string
  description = "The policy JSON to use for the resource policy; required when `create_resource_policy` is `false`"
  default     = ""
}

variable "policy_resource_policy_actions" {
  type        = list(string)
  description = "A list of IAM actions allowed in the resource policy"
  default     = []
}

variable "policy_resource_policy_principals" {
  type        = list(string)
  description = "A list of IAM principals allowed in the resource policy"
  default     = []
}

variable "policy_stateful_default_actions" {
  type        = list(string)
  description = "Set of actions to take on a packet if it does not match any stateful rules in the policy. This can only be specified if the policy has a `stateful_engine_options` block with a rule_order value of `STRICT_ORDER`. You can specify one of either or neither values of `aws:drop_strict` or `aws:drop_established`, as well as any combination of `aws:alert_strict` and `aws:alert_established`"
  default     = []
}

variable "policy_stateful_engine_options" {
  type        = any
  description = "A configuration block that defines options on how the policy handles stateful rules. See [Stateful Engine Options](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/networkfirewall_firewall_policy#stateful-engine-options) for details"
  default     = {}
}

variable "policy_stateful_rule_group_reference" {
  type        = any
  description = "Set of configuration blocks containing references to the stateful rule groups that are used in the policy. See [Stateful Rule Group Reference](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/networkfirewall_firewall_policy#stateful-rule-group-reference) for details"
  default     = {}
}

variable "policy_stateless_custom_action" {
  type        = any
  description = "Set of configuration blocks describing the custom action definitions that are available for use in the firewall policy's `stateless_default_actions`"
  default     = {}
}

variable "policy_stateless_default_actions" {
  type        = list(string)
  description = "Set of actions to take on a packet if it does not match any of the stateless rules in the policy. You must specify one of the standard actions including: `aws:drop`, `aws:pass`, or `aws:forward_to_sfe`"
  default     = [
  "aws:pass"
]
}

variable "policy_stateless_fragment_default_actions" {
  type        = list(string)
  description = "Set of actions to take on a fragmented packet if it does not match any of the stateless rules in the policy. You must specify one of the standard actions including: `aws:drop`, `aws:pass`, or `aws:forward_to_sfe`"
  default     = [
  "aws:pass"
]
}

variable "policy_stateless_rule_group_reference" {
  type        = any
  description = "Set of configuration blocks containing references to the stateless rule groups that are used in the policy. See [Stateless Rule Group Reference](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/networkfirewall_firewall_policy#stateless-rule-group-reference) for details"
  default     = {}
}

variable "policy_tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "subnet_change_protection" {
  type        = bool
  description = "A boolean flag indicating whether it is possible to change the associated subnet(s). Defaults to `true`"
  default     = true
}

variable "subnet_mapping" {
  type        = any
  description = "Set of configuration blocks describing the public subnets. Each subnet must belong to a different Availability Zone in the VPC. AWS Network Firewall creates a firewall endpoint in each subnet"
  default     = {}
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "vpc_id" {
  type        = string
  description = "The unique identifier of the VPC where AWS Network Firewall should create the firewall"
  default     = ""
}


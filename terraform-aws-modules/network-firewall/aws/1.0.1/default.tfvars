
create = true

create_logging_configuration = false

create_policy = true

create_policy_resource_policy = false

delete_protection = true

description = ""

encryption_configuration = {}

firewall_policy_arn = ""

firewall_policy_change_protection = null

logging_configuration_destination_config = []

name = ""

policy_attach_resource_policy = false

policy_description = null

policy_encryption_configuration = {}

policy_name = ""

policy_ram_resource_associations = {}

policy_resource_policy = ""

policy_resource_policy_actions = []

policy_resource_policy_principals = []

policy_stateful_default_actions = []

policy_stateful_engine_options = {}

policy_stateful_rule_group_reference = {}

policy_stateless_custom_action = {}

policy_stateless_default_actions = [
  "aws:pass"
]

policy_stateless_fragment_default_actions = [
  "aws:pass"
]

policy_stateless_rule_group_reference = {}

policy_tags = {}

subnet_change_protection = true

subnet_mapping = {}

tags = {}

vpc_id = ""


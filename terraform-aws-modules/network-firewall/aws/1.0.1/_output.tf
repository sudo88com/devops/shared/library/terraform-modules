
output "arn" {
  description = "The Amazon Resource Name (ARN) that identifies the firewall"
  value       = module.network-firewall.arn
}

output "id" {
  description = "The Amazon Resource Name (ARN) that identifies the firewall"
  value       = module.network-firewall.id
}

output "logging_configuration_id" {
  description = "The Amazon Resource Name (ARN) of the associated firewall"
  value       = module.network-firewall.logging_configuration_id
}

output "policy_arn" {
  description = "The Amazon Resource Name (ARN) that identifies the firewall policy"
  value       = module.network-firewall.policy_arn
}

output "policy_id" {
  description = "The Amazon Resource Name (ARN) that identifies the firewall policy"
  value       = module.network-firewall.policy_id
}

output "policy_resource_policy_id" {
  description = "The Amazon Resource Name (ARN) of the firewall policy associated with the resource policy"
  value       = module.network-firewall.policy_resource_policy_id
}

output "policy_update_token" {
  description = "A string token used when updating a firewall policy"
  value       = module.network-firewall.policy_update_token
}

output "status" {
  description = "Nested list of information about the current status of the firewall"
  value       = module.network-firewall.status
}

output "update_token" {
  description = "A string token used when updating a firewall"
  value       = module.network-firewall.update_token
}



variable "alb" {
  type        = any
  description = "Map of values passed to ALB module definition. See the [ALB module](https://github.com/terraform-aws-modules/terraform-aws-alb) for full list of arguments supported"
  default     = {}
}

variable "alb_https_default_action" {
  type        = any
  description = "Default action for the ALB https listener"
  default     = {
  "forward": {
    "target_group_key": "atlantis"
  }
}
}

variable "alb_security_group_id" {
  type        = string
  description = "ID of an existing security group that will be used by ALB. Required if `create_alb` is `false`"
  default     = ""
}

variable "alb_subnets" {
  type        = list(string)
  description = "List of subnets to place ALB in. Required if `create_alb` is `true`"
  default     = []
}

variable "alb_target_group_arn" {
  type        = string
  description = "ARN of an existing ALB target group that will be used to route traffic to the Atlantis service. Required if `create_alb` is `false`"
  default     = ""
}

variable "atlantis" {
  type        = any
  description = "Map of values passed to Atlantis container definition. See the [ECS container definition module](https://github.com/terraform-aws-modules/terraform-aws-ecs/tree/master/modules/container-definition) for full list of arguments supported"
  default     = {}
}

variable "atlantis_gid" {
  type        = number
  description = "GID of the atlantis user"
  default     = 1000
}

variable "atlantis_uid" {
  type        = number
  description = "UID of the atlantis user"
  default     = 100
}

variable "certificate_arn" {
  type        = string
  description = "ARN of certificate issued by AWS ACM. If empty, a new ACM certificate will be created and validated using Route53 DNS"
  default     = ""
}

variable "certificate_domain_name" {
  type        = string
  description = "Route53 domain name to use for ACM certificate. Route53 zone for this domain should be created in advance. Specify if it is different from value in `route53_zone_name`"
  default     = ""
}

variable "cluster" {
  type        = any
  description = "Map of values passed to ECS cluster module definition. See the [ECS cluster module](https://github.com/terraform-aws-modules/terraform-aws-ecs/tree/master/modules/cluster) for full list of arguments supported"
  default     = {}
}

variable "cluster_arn" {
  type        = string
  description = "ARN of an existing ECS cluster where resources will be created. Required when `create_cluster` is `false`"
  default     = ""
}

variable "create" {
  type        = bool
  description = "Controls if resources should be created (affects nearly all resources)"
  default     = true
}

variable "create_alb" {
  type        = bool
  description = "Determines whether to create an ALB or not"
  default     = true
}

variable "create_certificate" {
  type        = bool
  description = "Determines whether to create an ACM certificate or not. If `false`, `certificate_arn` must be provided"
  default     = true
}

variable "create_cluster" {
  type        = bool
  description = "Whether to create an ECS cluster or not"
  default     = true
}

variable "create_route53_records" {
  type        = bool
  description = "Determines whether to create Route53 `A` and `AAAA` records for the loadbalancer"
  default     = true
}

variable "efs" {
  type        = any
  description = "Map of values passed to EFS module definition. See the [EFS module](https://github.com/terraform-aws-modules/terraform-aws-efs) for full list of arguments supported"
  default     = {}
}

variable "enable_efs" {
  type        = bool
  description = "Determines whether to create and utilize an EFS filesystem"
  default     = false
}

variable "name" {
  type        = string
  description = "Common name to use on all resources created unless a more specific name is provided"
  default     = "atlantis"
}

variable "route53_record_name" {
  type        = string
  description = "Name of Route53 record to create ACM certificate in and main A-record. If null is specified, var.name is used instead. Provide empty string to point root domain name to ALB."
  default     = null
}

variable "route53_zone_id" {
  type        = string
  description = "Route53 zone ID to use for ACM certificate and Route53 records"
  default     = ""
}

variable "service" {
  type        = any
  description = "Map of values passed to ECS service module definition. See the [ECS service module](https://github.com/terraform-aws-modules/terraform-aws-ecs/tree/master/modules/service) for full list of arguments supported"
  default     = {}
}

variable "service_subnets" {
  type        = list(string)
  description = "List of subnets to place ECS service within"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "validate_certificate" {
  type        = bool
  description = "Determines whether to validate ACM certificate using Route53 DNS. If `false`, certificate will be created but not validated"
  default     = true
}

variable "vpc_id" {
  type        = string
  description = "ID of the VPC where the resources will be provisioned"
  default     = ""
}


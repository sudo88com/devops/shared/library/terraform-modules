
allow_version_upgrade = null

apply_immediately = null

aqua_configuration_status = null

authentication_profiles = {}

automated_snapshot_retention_period = null

availability_zone = null

availability_zone_relocation_enabled = null

cloudwatch_log_group_kms_key_id = null

cloudwatch_log_group_retention_in_days = 0

cloudwatch_log_group_skip_destroy = null

cloudwatch_log_group_tags = {}

cluster_identifier = ""

cluster_timeouts = {}

cluster_version = null

create = true

create_cloudwatch_log_group = false

create_endpoint_access = false

create_parameter_group = true

create_random_password = true

create_scheduled_action_iam_role = false

create_snapshot_schedule = false

create_subnet_group = true

database_name = null

default_iam_role_arn = null

elastic_ip = null

encrypted = true

endpoint_name = ""

endpoint_resource_owner = null

endpoint_subnet_group_name = ""

endpoint_vpc_security_group_ids = []

enhanced_vpc_routing = null

final_snapshot_identifier = null

iam_role_arns = []

iam_role_description = null

iam_role_name = null

iam_role_path = null

iam_role_permissions_boundary = null

iam_role_tags = {}

iam_role_use_name_prefix = true

kms_key_arn = null

logging = {}

maintenance_track_name = null

manage_master_password = false

manage_master_password_rotation = false

manual_snapshot_retention_period = null

master_password = null

master_password_rotate_immediately = null

master_password_rotation_automatically_after_days = null

master_password_rotation_duration = null

master_password_rotation_schedule_expression = null

master_password_secret_kms_key_id = null

master_username = "awsuser"

multi_az = null

node_type = ""

number_of_nodes = 1

owner_account = null

parameter_group_description = null

parameter_group_family = "redshift-1.0"

parameter_group_name = null

parameter_group_parameters = {}

parameter_group_tags = {}

port = null

preferred_maintenance_window = "sat:10:00-sat:10:30"

publicly_accessible = false

random_password_length = 16

scheduled_actions = {}

skip_final_snapshot = true

snapshot_cluster_identifier = null

snapshot_copy = {}

snapshot_identifier = null

snapshot_schedule_definitions = []

snapshot_schedule_description = null

snapshot_schedule_force_destroy = null

snapshot_schedule_identifier = null

subnet_group_description = null

subnet_group_name = null

subnet_group_tags = {}

subnet_ids = []

tags = {}

usage_limits = {}

use_snapshot_identifier_prefix = true

vpc_security_group_ids = []



variable "allow_version_upgrade" {
  type        = bool
  description = "If `true`, major version upgrades can be applied during the maintenance window to the Amazon Redshift engine that is running on the cluster. Default is `true`"
  default     = null
}

variable "apply_immediately" {
  type        = bool
  description = "Specifies whether any cluster modifications are applied immediately, or during the next maintenance window. Default is `false`"
  default     = null
}

variable "aqua_configuration_status" {
  type        = string
  description = "The value represents how the cluster is configured to use AQUA (Advanced Query Accelerator) after the cluster is restored. Possible values are `enabled`, `disabled`, and `auto`. Requires Cluster reboot"
  default     = null
}

variable "authentication_profiles" {
  type        = any
  description = "Map of authentication profiles to create"
  default     = {}
}

variable "automated_snapshot_retention_period" {
  type        = number
  description = "The number of days that automated snapshots are retained. If the value is 0, automated snapshots are disabled. Even if automated snapshots are disabled, you can still create manual snapshots when you want with create-cluster-snapshot. Default is 1"
  default     = null
}

variable "availability_zone" {
  type        = string
  description = "The EC2 Availability Zone (AZ) in which you want Amazon Redshift to provision the cluster. Can only be changed if `availability_zone_relocation_enabled` is `true`"
  default     = null
}

variable "availability_zone_relocation_enabled" {
  type        = bool
  description = "If `true`, the cluster can be relocated to another availability zone, either automatically by AWS or when requested. Default is `false`. Available for use on clusters from the RA3 instance family"
  default     = null
}

variable "cloudwatch_log_group_kms_key_id" {
  type        = string
  description = "The ARN of the KMS Key to use when encrypting log data"
  default     = null
}

variable "cloudwatch_log_group_retention_in_days" {
  type        = number
  description = "The number of days to retain CloudWatch logs for the redshift cluster"
  default     = 0
}

variable "cloudwatch_log_group_skip_destroy" {
  type        = bool
  description = "Set to true if you do not wish the log group (and any logs it may contain) to be deleted at destroy time, and instead just remove the log group from the Terraform state"
  default     = null
}

variable "cloudwatch_log_group_tags" {
  type        = map(string)
  description = "Additional tags to add to cloudwatch log groups created"
  default     = {}
}

variable "cluster_identifier" {
  type        = string
  description = "The Cluster Identifier. Must be a lower case string"
  default     = ""
}

variable "cluster_timeouts" {
  type        = map(string)
  description = "Create, update, and delete timeout configurations for the cluster"
  default     = {}
}

variable "cluster_version" {
  type        = string
  description = "The version of the Amazon Redshift engine software that you want to deploy on the cluster. The version selected runs on all the nodes in the cluster"
  default     = null
}

variable "create" {
  type        = bool
  description = "Determines whether to create Redshift cluster and resources (affects all resources)"
  default     = true
}

variable "create_cloudwatch_log_group" {
  type        = bool
  description = "Determines whether a CloudWatch log group is created for each `var.logging.log_exports`"
  default     = false
}

variable "create_endpoint_access" {
  type        = bool
  description = "Determines whether to create an endpoint access (managed VPC endpoint)"
  default     = false
}

variable "create_parameter_group" {
  type        = bool
  description = "Determines whether to create a parameter group or use existing"
  default     = true
}

variable "create_random_password" {
  type        = bool
  description = "Determines whether to create random password for cluster `master_password`"
  default     = true
}

variable "create_scheduled_action_iam_role" {
  type        = bool
  description = "Determines whether a scheduled action IAM role is created"
  default     = false
}

variable "create_snapshot_schedule" {
  type        = bool
  description = "Determines whether to create a snapshot schedule"
  default     = false
}

variable "create_subnet_group" {
  type        = bool
  description = "Determines whether to create a subnet group or use existing"
  default     = true
}

variable "database_name" {
  type        = string
  description = "The name of the first database to be created when the cluster is created. If you do not provide a name, Amazon Redshift will create a default database called `dev`"
  default     = null
}

variable "default_iam_role_arn" {
  type        = string
  description = "The Amazon Resource Name (ARN) for the IAM role that was set as default for the cluster when the cluster was created"
  default     = null
}

variable "elastic_ip" {
  type        = string
  description = "The Elastic IP (EIP) address for the cluster"
  default     = null
}

variable "encrypted" {
  type        = bool
  description = "If `true`, the data in the cluster is encrypted at rest"
  default     = true
}

variable "endpoint_name" {
  type        = string
  description = "The Redshift-managed VPC endpoint name"
  default     = ""
}

variable "endpoint_resource_owner" {
  type        = string
  description = "The Amazon Web Services account ID of the owner of the cluster. This is only required if the cluster is in another Amazon Web Services account"
  default     = null
}

variable "endpoint_subnet_group_name" {
  type        = string
  description = "The subnet group from which Amazon Redshift chooses the subnet to deploy the endpoint"
  default     = ""
}

variable "endpoint_vpc_security_group_ids" {
  type        = list(string)
  description = "The security group IDs to use for the endpoint access (managed VPC endpoint)"
  default     = []
}

variable "enhanced_vpc_routing" {
  type        = bool
  description = "If `true`, enhanced VPC routing is enabled"
  default     = null
}

variable "final_snapshot_identifier" {
  type        = string
  description = "The identifier of the final snapshot that is to be created immediately before deleting the cluster. If this parameter is provided, `skip_final_snapshot` must be `false`"
  default     = null
}

variable "iam_role_arns" {
  type        = list(string)
  description = "A list of IAM Role ARNs to associate with the cluster. A Maximum of 10 can be associated to the cluster at any time"
  default     = []
}

variable "iam_role_description" {
  type        = string
  description = "Description of the scheduled action IAM role"
  default     = null
}

variable "iam_role_name" {
  type        = string
  description = "Name to use on scheduled action IAM role created"
  default     = null
}

variable "iam_role_path" {
  type        = string
  description = "Scheduled action IAM role path"
  default     = null
}

variable "iam_role_permissions_boundary" {
  type        = string
  description = "ARN of the policy that is used to set the permissions boundary for the scheduled action IAM role"
  default     = null
}

variable "iam_role_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the scheduled action IAM role created"
  default     = {}
}

variable "iam_role_use_name_prefix" {
  type        = string
  description = "Determines whether scheduled action the IAM role name (`iam_role_name`) is used as a prefix"
  default     = true
}

variable "kms_key_arn" {
  type        = string
  description = "The ARN for the KMS encryption key. When specifying `kms_key_arn`, `encrypted` needs to be set to `true`"
  default     = null
}

variable "logging" {
  type        = any
  description = "Logging configuration for the cluster"
  default     = {}
}

variable "maintenance_track_name" {
  type        = string
  description = "The name of the maintenance track for the restored cluster. When you take a snapshot, the snapshot inherits the MaintenanceTrack value from the cluster. The snapshot might be on a different track than the cluster that was the source for the snapshot. Default value is `current`"
  default     = null
}

variable "manage_master_password" {
  type        = bool
  description = "Whether to use AWS SecretsManager to manage the cluster admin credentials. Conflicts with `master_password`. One of `master_password` or `manage_master_password` is required unless `snapshot_identifier` is provided"
  default     = false
}

variable "manage_master_password_rotation" {
  type        = bool
  description = "Whether to manage the master user password rotation. Setting this value to false after previously having been set to true will disable automatic rotation."
  default     = false
}

variable "manual_snapshot_retention_period" {
  type        = number
  description = "The default number of days to retain a manual snapshot. If the value is -1, the snapshot is retained indefinitely. This setting doesn't change the retention period of existing snapshots. Valid values are between `-1` and `3653`. Default value is `-1`"
  default     = null
}

variable "master_password" {
  type        = string
  description = "Password for the master DB user. (Required unless a `snapshot_identifier` is provided). Must contain at least 8 chars, one uppercase letter, one lowercase letter, and one number"
  default     = null
}

variable "master_password_rotate_immediately" {
  type        = bool
  description = "Specifies whether to rotate the secret immediately or wait until the next scheduled rotation window."
  default     = null
}

variable "master_password_rotation_automatically_after_days" {
  type        = number
  description = "Specifies the number of days between automatic scheduled rotations of the secret. Either `master_user_password_rotation_automatically_after_days` or `master_user_password_rotation_schedule_expression` must be specified."
  default     = null
}

variable "master_password_rotation_duration" {
  type        = string
  description = "The length of the rotation window in hours. For example, 3h for a three hour window."
  default     = null
}

variable "master_password_rotation_schedule_expression" {
  type        = string
  description = "A cron() or rate() expression that defines the schedule for rotating your secret. Either `master_user_password_rotation_automatically_after_days` or `master_user_password_rotation_schedule_expression` must be specified."
  default     = null
}

variable "master_password_secret_kms_key_id" {
  type        = string
  description = "ID of the KMS key used to encrypt the cluster admin credentials secret"
  default     = null
}

variable "master_username" {
  type        = string
  description = "Username for the master DB user (Required unless a `snapshot_identifier` is provided). Defaults to `awsuser`"
  default     = "awsuser"
}

variable "multi_az" {
  type        = bool
  description = "Specifies if the Redshift cluster is multi-AZ"
  default     = null
}

variable "node_type" {
  type        = string
  description = "The node type to be provisioned for the cluster"
  default     = ""
}

variable "number_of_nodes" {
  type        = number
  description = "Number of nodes in the cluster. Defaults to 1. Note: values greater than 1 will trigger `cluster_type` to switch to `multi-node`"
  default     = 1
}

variable "owner_account" {
  type        = string
  description = "The AWS customer account used to create or copy the snapshot. Required if you are restoring a snapshot you do not own, optional if you own the snapshot"
  default     = null
}

variable "parameter_group_description" {
  type        = string
  description = "The description of the Redshift parameter group. Defaults to `Managed by Terraform`"
  default     = null
}

variable "parameter_group_family" {
  type        = string
  description = "The family of the Redshift parameter group"
  default     = "redshift-1.0"
}

variable "parameter_group_name" {
  type        = string
  description = "The name of the Redshift parameter group, existing or to be created"
  default     = null
}

variable "parameter_group_parameters" {
  type        = map(any)
  description = "value"
  default     = {}
}

variable "parameter_group_tags" {
  type        = map(string)
  description = "Additional tags to add to the parameter group"
  default     = {}
}

variable "port" {
  type        = number
  description = "The port number on which the cluster accepts incoming connections. Default port is 5439"
  default     = null
}

variable "preferred_maintenance_window" {
  type        = string
  description = "The weekly time range (in UTC) during which automated cluster maintenance can occur. Format: `ddd:hh24:mi-ddd:hh24:mi`"
  default     = "sat:10:00-sat:10:30"
}

variable "publicly_accessible" {
  type        = bool
  description = "If true, the cluster can be accessed from a public network"
  default     = false
}

variable "random_password_length" {
  type        = number
  description = "Length of random password to create. Defaults to `16`"
  default     = 16
}

variable "scheduled_actions" {
  type        = any
  description = "Map of maps containing scheduled action definitions"
  default     = {}
}

variable "skip_final_snapshot" {
  type        = bool
  description = "Determines whether a final snapshot of the cluster is created before Redshift deletes the cluster. If true, a final cluster snapshot is not created. If false , a final cluster snapshot is created before the cluster is deleted"
  default     = true
}

variable "snapshot_cluster_identifier" {
  type        = string
  description = "The name of the cluster the source snapshot was created from"
  default     = null
}

variable "snapshot_copy" {
  type        = any
  description = "Configuration of automatic copy of snapshots from one region to another"
  default     = {}
}

variable "snapshot_identifier" {
  type        = string
  description = "The name of the snapshot from which to create the new cluster"
  default     = null
}

variable "snapshot_schedule_definitions" {
  type        = list(string)
  description = "The definition of the snapshot schedule. The definition is made up of schedule expressions, for example `cron(30 12 *)` or `rate(12 hours)`"
  default     = []
}

variable "snapshot_schedule_description" {
  type        = string
  description = "The description of the snapshot schedule"
  default     = null
}

variable "snapshot_schedule_force_destroy" {
  type        = bool
  description = "Whether to destroy all associated clusters with this snapshot schedule on deletion. Must be enabled and applied before attempting deletion"
  default     = null
}

variable "snapshot_schedule_identifier" {
  type        = string
  description = "The snapshot schedule identifier"
  default     = null
}

variable "subnet_group_description" {
  type        = string
  description = "The description of the Redshift Subnet group. Defaults to `Managed by Terraform`"
  default     = null
}

variable "subnet_group_name" {
  type        = string
  description = "The name of the Redshift subnet group, existing or to be created"
  default     = null
}

variable "subnet_group_tags" {
  type        = map(string)
  description = "Additional tags to add to the subnet group"
  default     = {}
}

variable "subnet_ids" {
  type        = list(string)
  description = "An array of VPC subnet IDs to use in the subnet group"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "usage_limits" {
  type        = any
  description = "Map of usage limit definitions to create"
  default     = {}
}

variable "use_snapshot_identifier_prefix" {
  type        = bool
  description = "Determines whether the identifier (`snapshot_schedule_identifier`) is used as a prefix"
  default     = true
}

variable "vpc_security_group_ids" {
  type        = list(string)
  description = "A list of Virtual Private Cloud (VPC) security groups to be associated with the cluster"
  default     = []
}



variable "acm_certificate_domain_validation_options" {
  type        = any
  description = "A list of domain_validation_options created by the ACM certificate to create required Route53 records from it (used when create_route53_records_only is set to true)"
  default     = {}
}

variable "certificate_transparency_logging_preference" {
  type        = bool
  description = "Specifies whether certificate details should be added to a certificate transparency log"
  default     = true
}

variable "create_certificate" {
  type        = bool
  description = "Whether to create ACM certificate"
  default     = true
}

variable "create_route53_records" {
  type        = bool
  description = "When validation is set to DNS, define whether to create the DNS records internally via Route53 or externally using any DNS provider"
  default     = true
}

variable "create_route53_records_only" {
  type        = bool
  description = "Whether to create only Route53 records (e.g. using separate AWS provider)"
  default     = false
}

variable "distinct_domain_names" {
  type        = list(string)
  description = "List of distinct domains and SANs (used when create_route53_records_only is set to true)"
  default     = []
}

variable "dns_ttl" {
  type        = number
  description = "The TTL of DNS recursive resolvers to cache information about this record."
  default     = 60
}

variable "domain_name" {
  type        = string
  description = "A domain name for which the certificate should be issued"
  default     = ""
}

variable "key_algorithm" {
  type        = string
  description = "Specifies the algorithm of the public and private key pair that your Amazon issued certificate uses to encrypt data"
  default     = null
}

variable "putin_khuylo" {
  type        = bool
  description = "Do you agree that Putin doesn't respect Ukrainian sovereignty and territorial integrity? More info: https://en.wikipedia.org/wiki/Putin_khuylo!"
  default     = true
}

variable "subject_alternative_names" {
  type        = list(string)
  description = "A list of domains that should be SANs in the issued certificate"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the resource"
  default     = {}
}

variable "validate_certificate" {
  type        = bool
  description = "Whether to validate certificate by creating Route53 record"
  default     = true
}

variable "validation_allow_overwrite_records" {
  type        = bool
  description = "Whether to allow overwrite of Route53 records"
  default     = true
}

variable "validation_method" {
  type        = string
  description = "Which method to use for validation. DNS or EMAIL are valid. This parameter must not be set for certificates that were imported into ACM and then into Terraform."
  default     = null
}

variable "validation_option" {
  type        = any
  description = "The domain name that you want ACM to use to send you validation emails. This domain name is the suffix of the email addresses that you want ACM to use."
  default     = {}
}

variable "validation_record_fqdns" {
  type        = list(string)
  description = "When validation is set to DNS and the DNS validation records are set externally, provide the fqdns for the validation"
  default     = []
}

variable "validation_timeout" {
  type        = string
  description = "Define maximum timeout to wait for the validation to complete"
  default     = null
}

variable "wait_for_validation" {
  type        = bool
  description = "Whether to wait for the validation to complete"
  default     = true
}

variable "zone_id" {
  type        = string
  description = "The ID of the hosted zone to contain this record. Required when validating via Route53"
  default     = ""
}



acm_certificate_domain_validation_options = {}

certificate_transparency_logging_preference = true

create_certificate = true

create_route53_records = true

create_route53_records_only = false

distinct_domain_names = []

dns_ttl = 60

domain_name = ""

key_algorithm = null

putin_khuylo = true

subject_alternative_names = []

tags = {}

validate_certificate = true

validation_allow_overwrite_records = true

validation_method = null

validation_option = {}

validation_record_fqdns = []

validation_timeout = null

wait_for_validation = true

zone_id = ""


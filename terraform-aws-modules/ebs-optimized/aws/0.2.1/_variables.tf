
variable "instance_type" {
  type        = string
  description = "Instance type to evaluate if EBS optimized is an option"
  default     = ""
}


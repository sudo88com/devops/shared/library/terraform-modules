
agent_vpce_policy = null

agent_vpce_security_group_ids = []

agent_vpce_subnet_ids = []

agent_vpce_tags = {}

api_vpce_policy = null

api_vpce_security_group_ids = []

api_vpce_subnet_ids = []

api_vpce_tags = {}

bucket_attach_deny_insecure_transport_policy = true

bucket_encryption_settings = {
  "sse_algorithm": "AES256"
}

bucket_name = ""

create_agent_vpce = false

create_api_vpce = false

create_bucket = true

create_log_forwarder = true

create_log_forwarder_role = true

create_log_forwarder_role_policy = true

create_log_forwarder_vpce = false

create_metrics_vpce = false

create_processes_vpce = false

create_rds_em_forwarder = true

create_rds_em_forwarder_role = true

create_rds_em_forwarder_role_policy = true

create_traces_vpce = false

create_vpc_fl_forwarder = true

create_vpc_fl_forwarder_role = true

create_vpc_fl_forwarder_role_policy = true

dd_api_key = ""

dd_api_key_secret_arn = ""

dd_app_key = ""

dd_site = "datadoghq.com"

kms_alias = 

log_forwarder_architectures = [
  "x86_64"
]

log_forwarder_bucket_prefix = ""

log_forwarder_environment_variables = {}

log_forwarder_kms_key_arn = null

log_forwarder_lambda_tags = {}

log_forwarder_layers = []

log_forwarder_log_kms_key_id = null

log_forwarder_log_retention_days = 7

log_forwarder_memory_size = 1024

log_forwarder_name = "datadog-log-forwarder"

log_forwarder_policy_arn = null

log_forwarder_policy_name = ""

log_forwarder_policy_path = null

log_forwarder_publish = false

log_forwarder_reserved_concurrent_executions = 100

log_forwarder_role_arn = null

log_forwarder_role_max_session_duration = null

log_forwarder_role_name = ""

log_forwarder_role_path = null

log_forwarder_role_permissions_boundary = null

log_forwarder_role_tags = {}

log_forwarder_runtime = "python3.11"

log_forwarder_s3_log_bucket_arns = []

log_forwarder_s3_zip_kms_key_id = null

log_forwarder_s3_zip_metadata = {}

log_forwarder_s3_zip_server_side_encryption = null

log_forwarder_s3_zip_storage_class = null

log_forwarder_s3_zip_tags = {}

log_forwarder_s3_zip_tags_only = false

log_forwarder_security_group_ids = null

log_forwarder_subnet_ids = null

log_forwarder_tags = {}

log_forwarder_timeout = 120

log_forwarder_use_policy_name_prefix = false

log_forwarder_use_role_name_prefix = false

log_forwarder_version = "3.103.0"

log_forwarder_vpce_policy = null

log_forwarder_vpce_security_group_ids = []

log_forwarder_vpce_subnet_ids = []

log_forwarder_vpce_tags = {}

metrics_vpce_policy = null

metrics_vpce_security_group_ids = []

metrics_vpce_subnet_ids = []

metrics_vpce_tags = {}

processes_vpce_policy = null

processes_vpce_security_group_ids = []

processes_vpce_subnet_ids = []

processes_vpce_tags = {}

rds_em_forwarder_architectures = [
  "x86_64"
]

rds_em_forwarder_environment_variables = {}

rds_em_forwarder_kms_key_arn = null

rds_em_forwarder_lambda_tags = {}

rds_em_forwarder_layers = []

rds_em_forwarder_log_kms_key_id = null

rds_em_forwarder_log_retention_days = 7

rds_em_forwarder_memory_size = 256

rds_em_forwarder_name = "datadog-rds-enhanced-monitoring-forwarder"

rds_em_forwarder_policy_arn = null

rds_em_forwarder_policy_name = ""

rds_em_forwarder_policy_path = null

rds_em_forwarder_publish = false

rds_em_forwarder_reserved_concurrent_executions = 10

rds_em_forwarder_role_arn = null

rds_em_forwarder_role_max_session_duration = null

rds_em_forwarder_role_name = ""

rds_em_forwarder_role_path = null

rds_em_forwarder_role_permissions_boundary = null

rds_em_forwarder_role_tags = {}

rds_em_forwarder_runtime = "python3.8"

rds_em_forwarder_security_group_ids = null

rds_em_forwarder_subnet_ids = null

rds_em_forwarder_tags = {}

rds_em_forwarder_timeout = 10

rds_em_forwarder_use_policy_name_prefix = false

rds_em_forwarder_use_role_name_prefix = false

rds_em_forwarder_version = "3.103.0"

tags = {}

traces_vpce_policy = null

traces_vpce_security_group_ids = []

traces_vpce_subnet_ids = []

traces_vpce_tags = {}

vpc_fl_forwarder_architectures = [
  "x86_64"
]

vpc_fl_forwarder_environment_variables = {}

vpc_fl_forwarder_kms_key_arn = null

vpc_fl_forwarder_lambda_tags = {}

vpc_fl_forwarder_layers = []

vpc_fl_forwarder_log_kms_key_id = null

vpc_fl_forwarder_log_retention_days = 7

vpc_fl_forwarder_memory_size = 256

vpc_fl_forwarder_name = "datadog-vpc-flow-log-forwarder"

vpc_fl_forwarder_policy_arn = null

vpc_fl_forwarder_policy_name = ""

vpc_fl_forwarder_policy_path = null

vpc_fl_forwarder_publish = false

vpc_fl_forwarder_read_cloudwatch_logs = false

vpc_fl_forwarder_reserved_concurrent_executions = 10

vpc_fl_forwarder_role_arn = null

vpc_fl_forwarder_role_max_session_duration = null

vpc_fl_forwarder_role_name = ""

vpc_fl_forwarder_role_path = null

vpc_fl_forwarder_role_permissions_boundary = null

vpc_fl_forwarder_role_tags = {}

vpc_fl_forwarder_runtime = "python3.8"

vpc_fl_forwarder_s3_log_bucket_arns = []

vpc_fl_forwarder_security_group_ids = null

vpc_fl_forwarder_subnet_ids = null

vpc_fl_forwarder_tags = {}

vpc_fl_forwarder_timeout = 10

vpc_fl_forwarder_use_policy_name_prefix = false

vpc_fl_forwarder_use_role_name_prefix = false

vpc_fl_forwarder_version = "3.103.0"

vpc_id = null



variable "access_policies" {
  type        = string
  description = "IAM policy document specifying the access policies for the domain. Required if `create_access_policy` is `false`"
  default     = null
}

variable "access_policy_override_policy_documents" {
  type        = list(string)
  description = "List of IAM policy documents that are merged together into the exported document. In merging, statements with non-blank `sid`s will override statements with the same `sid`"
  default     = []
}

variable "access_policy_source_policy_documents" {
  type        = list(string)
  description = "List of IAM policy documents that are merged together into the exported document. Statements must have unique `sid`s"
  default     = []
}

variable "access_policy_statements" {
  type        = any
  description = "A map of IAM policy [statements](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document#statement) for custom permission usage"
  default     = {}
}

variable "advanced_options" {
  type        = map(string)
  description = "Key-value string pairs to specify advanced configuration options. Note that the values for these configuration options must be strings (wrapped in quotes) or they may be wrong and cause a perpetual diff, causing Terraform to want to recreate your Elasticsearch domain on every apply"
  default     = {}
}

variable "advanced_security_options" {
  type        = any
  description = "Configuration block for [fine-grained access control](https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/fgac.html)"
  default     = {
  "anonymous_auth_enabled": false,
  "enabled": true
}
}

variable "auto_tune_options" {
  type        = any
  description = "Configuration block for the Auto-Tune options of the domain"
  default     = {
  "desired_state": "ENABLED",
  "rollback_on_disable": "NO_ROLLBACK"
}
}

variable "cloudwatch_log_group_kms_key_id" {
  type        = string
  description = "If a KMS Key ARN is set, this key will be used to encrypt the corresponding log group. Please be sure that the KMS Key has an appropriate key policy (https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/encrypt-log-data-kms.html)"
  default     = null
}

variable "cloudwatch_log_group_retention_in_days" {
  type        = number
  description = "Number of days to retain log events"
  default     = 60
}

variable "cloudwatch_log_resource_policy_name" {
  type        = string
  description = "Name of the resource policy for OpenSearch to log to CloudWatch"
  default     = null
}

variable "cluster_config" {
  type        = any
  description = "Configuration block for the cluster of the domain"
  default     = {
  "dedicated_master_enabled": true
}
}

variable "cognito_options" {
  type        = any
  description = "Configuration block for authenticating Kibana with Cognito"
  default     = {}
}

variable "create" {
  type        = bool
  description = "Determines whether resources will be created (affects all resources)"
  default     = true
}

variable "create_access_policy" {
  type        = bool
  description = "Determines whether an access policy will be created"
  default     = true
}

variable "create_cloudwatch_log_groups" {
  type        = bool
  description = "Determines whether log groups are created"
  default     = true
}

variable "create_cloudwatch_log_resource_policy" {
  type        = bool
  description = "Determines whether a resource policy will be created for OpenSearch to log to CloudWatch"
  default     = true
}

variable "create_saml_options" {
  type        = bool
  description = "Determines whether SAML options will be created"
  default     = false
}

variable "create_security_group" {
  type        = bool
  description = "Determines if a security group is created"
  default     = true
}

variable "domain_endpoint_options" {
  type        = any
  description = "Configuration block for domain endpoint HTTP(S) related options"
  default     = {
  "enforce_https": true,
  "tls_security_policy": "Policy-Min-TLS-1-2-2019-07"
}
}

variable "domain_name" {
  type        = string
  description = "Name of the domain"
  default     = ""
}

variable "ebs_options" {
  type        = any
  description = "Configuration block for EBS related options, may be required based on chosen [instance size](https://aws.amazon.com/elasticsearch-service/pricing/)"
  default     = {
  "ebs_enabled": true,
  "volume_size": 64,
  "volume_type": "gp3"
}
}

variable "enable_access_policy" {
  type        = bool
  description = "Determines whether an access policy will be applied to the domain"
  default     = true
}

variable "encrypt_at_rest" {
  type        = any
  description = "Configuration block for encrypting at rest"
  default     = {
  "enabled": true
}
}

variable "engine_version" {
  type        = string
  description = "Version of the OpenSearch engine to use"
  default     = null
}

variable "log_publishing_options" {
  type        = any
  description = "Configuration block for publishing slow and application logs to CloudWatch Logs. This block can be declared multiple times, for each log_type, within the same resource"
  default     = [
  {
    "log_type": "INDEX_SLOW_LOGS"
  },
  {
    "log_type": "SEARCH_SLOW_LOGS"
  }
]
}

variable "node_to_node_encryption" {
  type        = any
  description = "Configuration block for node-to-node encryption options"
  default     = {
  "enabled": true
}
}

variable "off_peak_window_options" {
  type        = any
  description = "Configuration to add Off Peak update options"
  default     = {
  "enabled": true,
  "off_peak_window": {
    "hours": 7
  }
}
}

variable "outbound_connections" {
  type        = any
  description = "Map of AWS OpenSearch outbound connections to create"
  default     = {}
}

variable "package_associations" {
  type        = map(string)
  description = "Map of package association IDs to associate with the domain"
  default     = {}
}

variable "saml_options" {
  type        = any
  description = "SAML authentication options for an AWS OpenSearch Domain"
  default     = {}
}

variable "security_group_description" {
  type        = string
  description = "Description of the security group created"
  default     = null
}

variable "security_group_name" {
  type        = string
  description = "Name to use on security group created"
  default     = null
}

variable "security_group_rules" {
  type        = any
  description = "Security group ingress and egress rules to add to the security group created"
  default     = {}
}

variable "security_group_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the security group created"
  default     = {}
}

variable "security_group_use_name_prefix" {
  type        = bool
  description = "Determines whether the security group name (`security_group_name`) is used as a prefix"
  default     = true
}

variable "software_update_options" {
  type        = any
  description = "Software update options for the domain"
  default     = {
  "auto_software_update_enabled": true
}
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "vpc_endpoints" {
  type        = any
  description = "Map of VPC endpoints to create for the domain"
  default     = {}
}

variable "vpc_options" {
  type        = any
  description = "Configuration block for VPC related options. Adding or removing this configuration forces a new resource ([documentation](https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/es-vpc.html#es-vpc-limitations))"
  default     = {}
}


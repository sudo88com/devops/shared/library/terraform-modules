
access_policies = null

access_policy_override_policy_documents = []

access_policy_source_policy_documents = []

access_policy_statements = {}

advanced_options = {}

advanced_security_options = {
  "anonymous_auth_enabled": false,
  "enabled": true
}

auto_tune_options = {
  "desired_state": "ENABLED",
  "rollback_on_disable": "NO_ROLLBACK"
}

cloudwatch_log_group_kms_key_id = null

cloudwatch_log_group_retention_in_days = 60

cloudwatch_log_resource_policy_name = null

cluster_config = {
  "dedicated_master_enabled": true
}

cognito_options = {}

create = true

create_access_policy = true

create_cloudwatch_log_groups = true

create_cloudwatch_log_resource_policy = true

create_saml_options = false

create_security_group = true

domain_endpoint_options = {
  "enforce_https": true,
  "tls_security_policy": "Policy-Min-TLS-1-2-2019-07"
}

domain_name = ""

ebs_options = {
  "ebs_enabled": true,
  "volume_size": 64,
  "volume_type": "gp3"
}

enable_access_policy = true

encrypt_at_rest = {
  "enabled": true
}

engine_version = null

log_publishing_options = [
  {
    "log_type": "INDEX_SLOW_LOGS"
  },
  {
    "log_type": "SEARCH_SLOW_LOGS"
  }
]

node_to_node_encryption = {
  "enabled": true
}

off_peak_window_options = {
  "enabled": true,
  "off_peak_window": {
    "hours": 7
  }
}

outbound_connections = {}

package_associations = {}

saml_options = {}

security_group_description = null

security_group_name = null

security_group_rules = {}

security_group_tags = {}

security_group_use_name_prefix = true

software_update_options = {
  "auto_software_update_enabled": true
}

tags = {}

vpc_endpoints = {}

vpc_options = {}


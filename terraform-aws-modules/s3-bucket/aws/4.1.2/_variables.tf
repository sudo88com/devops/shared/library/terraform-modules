
variable "acceleration_status" {
  type        = string
  description = "(Optional) Sets the accelerate configuration of an existing bucket. Can be Enabled or Suspended."
  default     = null
}

variable "access_log_delivery_policy_source_accounts" {
  type        = list(string)
  description = "(Optional) List of AWS Account IDs should be allowed to deliver access logs to this bucket."
  default     = []
}

variable "access_log_delivery_policy_source_buckets" {
  type        = list(string)
  description = "(Optional) List of S3 bucket ARNs which should be allowed to deliver access logs to this bucket."
  default     = []
}

variable "acl" {
  type        = string
  description = "(Optional) The canned ACL to apply. Conflicts with `grant`"
  default     = null
}

variable "allowed_kms_key_arn" {
  type        = string
  description = "The ARN of KMS key which should be allowed in PutObject"
  default     = null
}

variable "analytics_configuration" {
  type        = any
  description = "Map containing bucket analytics configuration."
  default     = {}
}

variable "analytics_self_source_destination" {
  type        = bool
  description = "Whether or not the analytics source bucket is also the destination bucket."
  default     = false
}

variable "analytics_source_account_id" {
  type        = string
  description = "The analytics source account id."
  default     = null
}

variable "analytics_source_bucket_arn" {
  type        = string
  description = "The analytics source bucket ARN."
  default     = null
}

variable "attach_access_log_delivery_policy" {
  type        = bool
  description = "Controls if S3 bucket should have S3 access log delivery policy attached"
  default     = false
}

variable "attach_analytics_destination_policy" {
  type        = bool
  description = "Controls if S3 bucket should have bucket analytics destination policy attached."
  default     = false
}

variable "attach_deny_incorrect_encryption_headers" {
  type        = bool
  description = "Controls if S3 bucket should deny incorrect encryption headers policy attached."
  default     = false
}

variable "attach_deny_incorrect_kms_key_sse" {
  type        = bool
  description = "Controls if S3 bucket policy should deny usage of incorrect KMS key SSE."
  default     = false
}

variable "attach_deny_insecure_transport_policy" {
  type        = bool
  description = "Controls if S3 bucket should have deny non-SSL transport policy attached"
  default     = false
}

variable "attach_deny_unencrypted_object_uploads" {
  type        = bool
  description = "Controls if S3 bucket should deny unencrypted object uploads policy attached."
  default     = false
}

variable "attach_elb_log_delivery_policy" {
  type        = bool
  description = "Controls if S3 bucket should have ELB log delivery policy attached"
  default     = false
}

variable "attach_inventory_destination_policy" {
  type        = bool
  description = "Controls if S3 bucket should have bucket inventory destination policy attached."
  default     = false
}

variable "attach_lb_log_delivery_policy" {
  type        = bool
  description = "Controls if S3 bucket should have ALB/NLB log delivery policy attached"
  default     = false
}

variable "attach_policy" {
  type        = bool
  description = "Controls if S3 bucket should have bucket policy attached (set to `true` to use value of `policy` as bucket policy)"
  default     = false
}

variable "attach_public_policy" {
  type        = bool
  description = "Controls if a user defined public bucket policy will be attached (set to `false` to allow upstream to apply defaults to the bucket)"
  default     = true
}

variable "attach_require_latest_tls_policy" {
  type        = bool
  description = "Controls if S3 bucket should require the latest version of TLS"
  default     = false
}

variable "block_public_acls" {
  type        = bool
  description = "Whether Amazon S3 should block public ACLs for this bucket."
  default     = true
}

variable "block_public_policy" {
  type        = bool
  description = "Whether Amazon S3 should block public bucket policies for this bucket."
  default     = true
}

variable "bucket" {
  type        = string
  description = "(Optional, Forces new resource) The name of the bucket. If omitted, Terraform will assign a random, unique name."
  default     = null
}

variable "bucket_prefix" {
  type        = string
  description = "(Optional, Forces new resource) Creates a unique bucket name beginning with the specified prefix. Conflicts with bucket."
  default     = null
}

variable "control_object_ownership" {
  type        = bool
  description = "Whether to manage S3 Bucket Ownership Controls on this bucket."
  default     = false
}

variable "cors_rule" {
  type        = any
  description = "List of maps containing rules for Cross-Origin Resource Sharing."
  default     = []
}

variable "create_bucket" {
  type        = bool
  description = "Controls if S3 bucket should be created"
  default     = true
}

variable "expected_bucket_owner" {
  type        = string
  description = "The account ID of the expected bucket owner"
  default     = null
}

variable "force_destroy" {
  type        = bool
  description = "(Optional, Default:false ) A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable."
  default     = false
}

variable "grant" {
  type        = any
  description = "An ACL policy grant. Conflicts with `acl`"
  default     = []
}

variable "ignore_public_acls" {
  type        = bool
  description = "Whether Amazon S3 should ignore public ACLs for this bucket."
  default     = true
}

variable "intelligent_tiering" {
  type        = any
  description = "Map containing intelligent tiering configuration."
  default     = {}
}

variable "inventory_configuration" {
  type        = any
  description = "Map containing S3 inventory configuration."
  default     = {}
}

variable "inventory_self_source_destination" {
  type        = bool
  description = "Whether or not the inventory source bucket is also the destination bucket."
  default     = false
}

variable "inventory_source_account_id" {
  type        = string
  description = "The inventory source account id."
  default     = null
}

variable "inventory_source_bucket_arn" {
  type        = string
  description = "The inventory source bucket ARN."
  default     = null
}

variable "lifecycle_rule" {
  type        = any
  description = "List of maps containing configuration of object lifecycle management."
  default     = []
}

variable "logging" {
  type        = any
  description = "Map containing access bucket logging configuration."
  default     = {}
}

variable "metric_configuration" {
  type        = any
  description = "Map containing bucket metric configuration."
  default     = []
}

variable "object_lock_configuration" {
  type        = any
  description = "Map containing S3 object locking configuration."
  default     = {}
}

variable "object_lock_enabled" {
  type        = bool
  description = "Whether S3 bucket should have an Object Lock configuration enabled."
  default     = false
}

variable "object_ownership" {
  type        = string
  description = "Object ownership. Valid values: BucketOwnerEnforced, BucketOwnerPreferred or ObjectWriter. 'BucketOwnerEnforced': ACLs are disabled, and the bucket owner automatically owns and has full control over every object in the bucket. 'BucketOwnerPreferred': Objects uploaded to the bucket change ownership to the bucket owner if the objects are uploaded with the bucket-owner-full-control canned ACL. 'ObjectWriter': The uploading account will own the object if the object is uploaded with the bucket-owner-full-control canned ACL."
  default     = "BucketOwnerEnforced"
}

variable "owner" {
  type        = map(string)
  description = "Bucket owner's display name and ID. Conflicts with `acl`"
  default     = {}
}

variable "policy" {
  type        = string
  description = "(Optional) A valid bucket policy JSON document. Note that if the policy document is not specific enough (but still valid), Terraform may view the policy as constantly changing in a terraform plan. In this case, please make sure you use the verbose/specific version of the policy. For more information about building AWS IAM policy documents with Terraform, see the AWS IAM Policy Document Guide."
  default     = null
}

variable "putin_khuylo" {
  type        = bool
  description = "Do you agree that Putin doesn't respect Ukrainian sovereignty and territorial integrity? More info: https://en.wikipedia.org/wiki/Putin_khuylo!"
  default     = true
}

variable "replication_configuration" {
  type        = any
  description = "Map containing cross-region replication configuration."
  default     = {}
}

variable "request_payer" {
  type        = string
  description = "(Optional) Specifies who should bear the cost of Amazon S3 data transfer. Can be either BucketOwner or Requester. By default, the owner of the S3 bucket would incur the costs of any data transfer. See Requester Pays Buckets developer guide for more information."
  default     = null
}

variable "restrict_public_buckets" {
  type        = bool
  description = "Whether Amazon S3 should restrict public bucket policies for this bucket."
  default     = true
}

variable "server_side_encryption_configuration" {
  type        = any
  description = "Map containing server-side encryption configuration."
  default     = {}
}

variable "tags" {
  type        = map(string)
  description = "(Optional) A mapping of tags to assign to the bucket."
  default     = {}
}

variable "versioning" {
  type        = map(string)
  description = "Map containing versioning configuration."
  default     = {}
}

variable "website" {
  type        = any
  description = "Map containing static web-site hosting or redirect configuration."
  default     = {}
}



variable "compute_environments" {
  type        = any
  description = "Map of compute environment definitions to create"
  default     = {}
}

variable "create" {
  type        = bool
  description = "Controls if resources should be created (affects nearly all resources)"
  default     = true
}

variable "create_instance_iam_role" {
  type        = bool
  description = "Determines whether a an IAM role is created or to use an existing IAM role"
  default     = true
}

variable "create_job_definitions" {
  type        = bool
  description = "Determines whether to create the job definitions defined"
  default     = true
}

variable "create_job_queues" {
  type        = bool
  description = "Determines whether to create job queues"
  default     = true
}

variable "create_service_iam_role" {
  type        = bool
  description = "Determines whether a an IAM role is created or to use an existing IAM role"
  default     = true
}

variable "create_spot_fleet_iam_role" {
  type        = bool
  description = "Determines whether a an IAM role is created or to use an existing IAM role"
  default     = false
}

variable "instance_iam_role_additional_policies" {
  type        = list(string)
  description = "Additional policies to be added to the IAM role"
  default     = []
}

variable "instance_iam_role_description" {
  type        = string
  description = "Cluster instance IAM role description"
  default     = null
}

variable "instance_iam_role_name" {
  type        = string
  description = "Cluster instance IAM role name"
  default     = null
}

variable "instance_iam_role_path" {
  type        = string
  description = "Cluster instance IAM role path"
  default     = null
}

variable "instance_iam_role_permissions_boundary" {
  type        = string
  description = "ARN of the policy that is used to set the permissions boundary for the IAM role"
  default     = null
}

variable "instance_iam_role_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the IAM role created"
  default     = {}
}

variable "instance_iam_role_use_name_prefix" {
  type        = string
  description = "Determines whether the IAM role name (`instance_iam_role_name`) is used as a prefix"
  default     = true
}

variable "job_definitions" {
  type        = any
  description = "Map of job definitions to create"
  default     = {}
}

variable "job_queues" {
  type        = any
  description = "Map of job queue and scheduling policy defintions to create"
  default     = {}
}

variable "service_iam_role_additional_policies" {
  type        = list(string)
  description = "Additional policies to be added to the IAM role"
  default     = []
}

variable "service_iam_role_description" {
  type        = string
  description = "Batch service IAM role description"
  default     = null
}

variable "service_iam_role_name" {
  type        = string
  description = "Batch service IAM role name"
  default     = null
}

variable "service_iam_role_path" {
  type        = string
  description = "Batch service IAM role path"
  default     = null
}

variable "service_iam_role_permissions_boundary" {
  type        = string
  description = "ARN of the policy that is used to set the permissions boundary for the IAM role"
  default     = null
}

variable "service_iam_role_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the IAM role created"
  default     = {}
}

variable "service_iam_role_use_name_prefix" {
  type        = bool
  description = "Determines whether the IAM role name (`service_iam_role_name`) is used as a prefix"
  default     = true
}

variable "spot_fleet_iam_role_additional_policies" {
  type        = list(string)
  description = "Additional policies to be added to the IAM role"
  default     = []
}

variable "spot_fleet_iam_role_description" {
  type        = string
  description = "Spot fleet IAM role description"
  default     = null
}

variable "spot_fleet_iam_role_name" {
  type        = string
  description = "Spot fleet IAM role name"
  default     = null
}

variable "spot_fleet_iam_role_path" {
  type        = string
  description = "Spot fleet IAM role path"
  default     = null
}

variable "spot_fleet_iam_role_permissions_boundary" {
  type        = string
  description = "ARN of the policy that is used to set the permissions boundary for the IAM role"
  default     = null
}

variable "spot_fleet_iam_role_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the IAM role created"
  default     = {}
}

variable "spot_fleet_iam_role_use_name_prefix" {
  type        = string
  description = "Determines whether the IAM role name (`spot_fleet_iam_role_name`) is used as a prefix"
  default     = true
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}



compute_environments = {}

create = true

create_instance_iam_role = true

create_job_definitions = true

create_job_queues = true

create_service_iam_role = true

create_spot_fleet_iam_role = false

instance_iam_role_additional_policies = []

instance_iam_role_description = null

instance_iam_role_name = null

instance_iam_role_path = null

instance_iam_role_permissions_boundary = null

instance_iam_role_tags = {}

instance_iam_role_use_name_prefix = true

job_definitions = {}

job_queues = {}

service_iam_role_additional_policies = []

service_iam_role_description = null

service_iam_role_name = null

service_iam_role_path = null

service_iam_role_permissions_boundary = null

service_iam_role_tags = {}

service_iam_role_use_name_prefix = true

spot_fleet_iam_role_additional_policies = []

spot_fleet_iam_role_description = null

spot_fleet_iam_role_name = null

spot_fleet_iam_role_path = null

spot_fleet_iam_role_permissions_boundary = null

spot_fleet_iam_role_tags = {}

spot_fleet_iam_role_use_name_prefix = true

tags = {}


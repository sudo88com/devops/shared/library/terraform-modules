
output "insecure_value" {
  description = "Insecure value of the parameter"
  value       = module.ssm-parameter.insecure_value
}

output "raw_value" {
  description = "Raw value of the parameter (as it is stored in SSM). Use 'value' output to get jsondecode'd value"
  value       = module.ssm-parameter.raw_value
}

output "secure_type" {
  description = "Whether SSM parameter is a SecureString or not?"
  value       = module.ssm-parameter.secure_type
}

output "secure_value" {
  description = "Secure value of the parameter"
  value       = module.ssm-parameter.secure_value
}

output "ssm_parameter_arn" {
  description = "The ARN of the parameter"
  value       = module.ssm-parameter.ssm_parameter_arn
}

output "ssm_parameter_name" {
  description = "Name of the parameter"
  value       = module.ssm-parameter.ssm_parameter_name
}

output "ssm_parameter_tags_all" {
  description = "All tags used for the parameter"
  value       = module.ssm-parameter.ssm_parameter_tags_all
}

output "ssm_parameter_type" {
  description = "Type of the parameter"
  value       = module.ssm-parameter.ssm_parameter_type
}

output "ssm_parameter_version" {
  description = "Version of the parameter"
  value       = module.ssm-parameter.ssm_parameter_version
}

output "value" {
  description = "Parameter value after jsondecode(). Probably this is what you are looking for"
  value       = module.ssm-parameter.value
}


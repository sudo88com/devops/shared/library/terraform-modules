
variable "allowed_pattern" {
  type        = string
  description = "Regular expression used to validate the parameter value."
  default     = null
}

variable "create" {
  type        = bool
  description = "Whether to create SSM Parameter"
  default     = true
}

variable "data_type" {
  type        = string
  description = "Data type of the parameter. Valid values: text, aws:ssm:integration and aws:ec2:image for AMI format."
  default     = null
}

variable "description" {
  type        = string
  description = "Description of the parameter"
  default     = null
}

variable "ignore_value_changes" {
  type        = bool
  description = "Whether to create SSM Parameter and ignore changes in value"
  default     = false
}

variable "key_id" {
  type        = string
  description = "KMS key ID or ARN for encrypting a parameter (when type is SecureString)"
  default     = null
}

variable "name" {
  type        = string
  description = "Name of SSM parameter"
  default     = null
}

variable "secure_type" {
  type        = bool
  description = "Whether the type of the value should be considered as secure or not?"
  default     = false
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to resources"
  default     = {}
}

variable "tier" {
  type        = string
  description = "Parameter tier to assign to the parameter. If not specified, will use the default parameter tier for the region. Valid tiers are Standard, Advanced, and Intelligent-Tiering. Downgrading an Advanced tier parameter to Standard will recreate the resource."
  default     = null
}

variable "type" {
  type        = string
  description = "Type of the parameter. Valid types are String, StringList and SecureString."
  default     = null
}

variable "value" {
  type        = string
  description = "Value of the parameter"
  default     = null
}

variable "values" {
  type        = list(string)
  description = "List of values of the parameter (will be jsonencoded to store as string natively in SSM)"
  default     = []
}


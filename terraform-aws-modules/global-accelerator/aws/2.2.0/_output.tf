
output "dns_name" {
  description = "The DNS name of the accelerator"
  value       = module.global-accelerator.dns_name
}

output "dual_stack_dns_name" {
  description = "The DNS name that Global Accelerator creates that points to a dual-stack accelerator's four static IP addresses: two IPv4 addresses and two IPv6 addresses"
  value       = module.global-accelerator.dual_stack_dns_name
}

output "endpoint_groups" {
  description = "Map of endpoints created and their associated attributes"
  value       = module.global-accelerator.endpoint_groups
}

output "hosted_zone_id" {
  description = "The Global Accelerator Route 53 zone ID that can be used to route an Alias Resource Record Set to the Global Accelerator"
  value       = module.global-accelerator.hosted_zone_id
}

output "id" {
  description = "The Amazon Resource Name (ARN) of the accelerator"
  value       = module.global-accelerator.id
}

output "ip_sets" {
  description = "IP address set associated with the accelerator"
  value       = module.global-accelerator.ip_sets
}

output "listeners" {
  description = "Map of listeners created and their associated attributes"
  value       = module.global-accelerator.listeners
}


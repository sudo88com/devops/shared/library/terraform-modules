
variable "access_logs" {
  type        = map(string)
  description = "An access logs block"
  default     = {}
}

variable "connection_draining" {
  type        = bool
  description = "Boolean to enable connection draining"
  default     = false
}

variable "connection_draining_timeout" {
  type        = number
  description = "The time in seconds to allow for connections to drain"
  default     = 300
}

variable "create_elb" {
  type        = bool
  description = "Create the elb or not"
  default     = true
}

variable "cross_zone_load_balancing" {
  type        = bool
  description = "Enable cross-zone load balancing"
  default     = true
}

variable "health_check" {
  type        = map(string)
  description = "A health check block"
  default     = ""
}

variable "idle_timeout" {
  type        = number
  description = "The time in seconds that the connection is allowed to be idle"
  default     = 60
}

variable "instances" {
  type        = list(string)
  description = "List of instances ID to place in the ELB pool"
  default     = []
}

variable "internal" {
  type        = bool
  description = "If true, ELB will be an internal ELB"
  default     = false
}

variable "listener" {
  type        = list(map(string))
  description = "A list of listener blocks"
  default     = ""
}

variable "name" {
  type        = string
  description = "The name of the ELB"
  default     = null
}

variable "name_prefix" {
  type        = string
  description = "The prefix name of the ELB"
  default     = null
}

variable "number_of_instances" {
  type        = number
  description = "Number of instances to attach to ELB"
  default     = 0
}

variable "security_groups" {
  type        = list(string)
  description = "A list of security group IDs to assign to the ELB"
  default     = ""
}

variable "subnets" {
  type        = list(string)
  description = "A list of subnet IDs to attach to the ELB"
  default     = ""
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the resource"
  default     = {}
}



attributes = []

autoscaling_defaults = {
  "scale_in_cooldown": 0,
  "scale_out_cooldown": 0,
  "target_value": 70
}

autoscaling_enabled = false

autoscaling_indexes = {}

autoscaling_read = {}

autoscaling_write = {}

billing_mode = "PAY_PER_REQUEST"

create_table = true

deletion_protection_enabled = null

global_secondary_indexes = []

hash_key = null

ignore_changes_global_secondary_index = false

import_table = {}

local_secondary_indexes = []

name = null

point_in_time_recovery_enabled = false

range_key = null

read_capacity = null

replica_regions = []

server_side_encryption_enabled = false

server_side_encryption_kms_key_arn = null

stream_enabled = false

stream_view_type = null

table_class = null

tags = {}

timeouts = {
  "create": "10m",
  "delete": "10m",
  "update": "60m"
}

ttl_attribute_name = ""

ttl_enabled = false

write_capacity = null


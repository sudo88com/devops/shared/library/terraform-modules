
variable "additional_info" {
  type        = string
  description = "JSON string for selecting additional features such as adding proxy information. Note: Currently there is no API to retrieve the value of this argument after EMR cluster creation from provider, therefore Terraform cannot detect drift from the actual EMR cluster if its value is changed outside Terraform"
  default     = null
}

variable "applications" {
  type        = list(string)
  description = "A case-insensitive list of applications for Amazon EMR to install and configure when launching the cluster"
  default     = []
}

variable "auto_termination_policy" {
  type        = any
  description = "An auto-termination policy for an Amazon EMR cluster. An auto-termination policy defines the amount of idle time in seconds after which a cluster automatically terminates"
  default     = {}
}

variable "autoscaling_iam_role_arn" {
  type        = string
  description = "The ARN of an existing IAM role to use for autoscaling"
  default     = null
}

variable "autoscaling_iam_role_description" {
  type        = string
  description = "Description of the role"
  default     = null
}

variable "autoscaling_iam_role_name" {
  type        = string
  description = "Name to use on IAM role created"
  default     = null
}

variable "bootstrap_action" {
  type        = any
  description = "Ordered list of bootstrap actions that will be run before Hadoop is started on the cluster nodes"
  default     = {}
}

variable "configurations" {
  type        = string
  description = "List of configurations supplied for the EMR cluster you are creating. Supply a configuration object for applications to override their default configuration"
  default     = null
}

variable "configurations_json" {
  type        = string
  description = "JSON string for supplying list of configurations for the EMR cluster"
  default     = null
}

variable "core_instance_fleet" {
  type        = any
  description = "Configuration block to use an [Instance Fleet](https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-instance-fleet.html) for the core node type. Cannot be specified if any `core_instance_group` configuration blocks are set"
  default     = {}
}

variable "core_instance_group" {
  type        = any
  description = "Configuration block to use an [Instance Group] for the core node type"
  default     = {}
}

variable "create" {
  type        = bool
  description = "Controls if resources should be created (affects nearly all resources)"
  default     = true
}

variable "create_autoscaling_iam_role" {
  type        = bool
  description = "Determines whether the autoscaling IAM role should be created"
  default     = true
}

variable "create_iam_instance_profile" {
  type        = bool
  description = "Determines whether the EC2 IAM role/instance profile should be created"
  default     = true
}

variable "create_managed_security_groups" {
  type        = bool
  description = "Determines whether managed security groups are created"
  default     = true
}

variable "create_security_configuration" {
  type        = bool
  description = "Determines whether a security configuration is created"
  default     = false
}

variable "create_service_iam_role" {
  type        = bool
  description = "Determines whether the service IAM role should be created"
  default     = true
}

variable "custom_ami_id" {
  type        = string
  description = "Custom Amazon Linux AMI for the cluster (instead of an EMR-owned AMI). Available in Amazon EMR version 5.7.0 and later"
  default     = null
}

variable "ebs_root_volume_size" {
  type        = number
  description = "Size in GiB of the EBS root device volume of the Linux AMI that is used for each EC2 instance. Available in Amazon EMR version 4.x and later"
  default     = null
}

variable "ec2_attributes" {
  type        = any
  description = "Attributes for the EC2 instances running the job flow"
  default     = {}
}

variable "iam_instance_profile_description" {
  type        = string
  description = "Description of the EC2 IAM role/instance profile"
  default     = null
}

variable "iam_instance_profile_name" {
  type        = string
  description = "Name to use on EC2 IAM role/instance profile created"
  default     = null
}

variable "iam_instance_profile_policies" {
  type        = map(string)
  description = "Map of IAM policies to attach to the EC2 IAM role/instance profile"
  default     = {
  "AmazonElasticMapReduceforEC2Role": "arn:aws:iam::aws:policy/service-role/AmazonElasticMapReduceforEC2Role"
}
}

variable "iam_role_path" {
  type        = string
  description = "IAM role path"
  default     = null
}

variable "iam_role_permissions_boundary" {
  type        = string
  description = "ARN of the policy that is used to set the permissions boundary for the IAM role"
  default     = null
}

variable "iam_role_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the IAM role created"
  default     = {}
}

variable "iam_role_use_name_prefix" {
  type        = bool
  description = "Determines whether the IAM role name is used as a prefix"
  default     = true
}

variable "is_private_cluster" {
  type        = bool
  description = "Identifies whether the cluster is created in a private subnet"
  default     = true
}

variable "keep_job_flow_alive_when_no_steps" {
  type        = bool
  description = "Switch on/off run cluster with no steps or when all steps are complete (default is on)"
  default     = null
}

variable "kerberos_attributes" {
  type        = any
  description = "Kerberos configuration for the cluster"
  default     = {}
}

variable "list_steps_states" {
  type        = list(string)
  description = "List of [step states](https://docs.aws.amazon.com/emr/latest/APIReference/API_StepStatus.html) used to filter returned steps"
  default     = []
}

variable "log_encryption_kms_key_id" {
  type        = string
  description = "AWS KMS customer master key (CMK) key ID or arn used for encrypting log files. This attribute is only available with EMR version 5.30.0 and later, excluding EMR 6.0.0"
  default     = null
}

variable "log_uri" {
  type        = string
  description = "S3 bucket to write the log files of the job flow. If a value is not provided, logs are not created"
  default     = null
}

variable "managed_scaling_policy" {
  type        = any
  description = "Compute limit configuration for a [Managed Scaling Policy](https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-managed-scaling.html)"
  default     = {}
}

variable "managed_security_group_name" {
  type        = string
  description = "Name to use on manged security group created. Note - `-master`, `-slave`, and `-service` will be appended to this name to distinguish"
  default     = null
}

variable "managed_security_group_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the security group created"
  default     = {}
}

variable "managed_security_group_use_name_prefix" {
  type        = bool
  description = "Determines whether the security group name (`security_group_name`) is used as a prefix"
  default     = true
}

variable "master_instance_fleet" {
  type        = any
  description = "Configuration block to use an [Instance Fleet](https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-instance-fleet.html) for the master node type. Cannot be specified if any `master_instance_group` configuration blocks are set"
  default     = {}
}

variable "master_instance_group" {
  type        = any
  description = "Configuration block to use an [Instance Group](https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-instance-group-configuration.html#emr-plan-instance-groups) for the [master node type](https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-master-core-task-nodes.html#emr-plan-master)"
  default     = {}
}

variable "master_security_group_description" {
  type        = string
  description = "Description of the security group created"
  default     = "Managed master security group"
}

variable "master_security_group_rules" {
  type        = any
  description = "Security group rules to add to the security group created"
  default     = {
  "default": {
    "cidr_blocks": [
      "0.0.0.0/0"
    ],
    "description": "Allow all egress traffic",
    "from_port": 0,
    "ipv6_cidr_blocks": [
      "::/0"
    ],
    "protocol": "-1",
    "to_port": 0,
    "type": "egress"
  }
}
}

variable "name" {
  type        = string
  description = "Name of the job flow"
  default     = ""
}

variable "placement_group_config" {
  type        = any
  description = "The specified placement group configuration"
  default     = {}
}

variable "release_label" {
  type        = string
  description = "Release label for the Amazon EMR release"
  default     = null
}

variable "release_label_filters" {
  type        = any
  description = "Map of release label filters use to lookup a release label"
  default     = {
  "default": {
    "prefix": "emr-6"
  }
}
}

variable "scale_down_behavior" {
  type        = string
  description = "Way that individual Amazon EC2 instances terminate when an automatic scale-in activity occurs or an instance group is resized"
  default     = "TERMINATE_AT_TASK_COMPLETION"
}

variable "security_configuration" {
  type        = string
  description = "Security configuration to create, or attach if `create_security_configuration` is `false`. Only valid for EMR clusters with `release_label` 4.8.0 or greater"
  default     = null
}

variable "security_configuration_name" {
  type        = string
  description = "Name of the security configuration to create, or attach if `create_security_configuration` is `false`. Only valid for EMR clusters with `release_label` 4.8.0 or greater"
  default     = null
}

variable "security_configuration_use_name_prefix" {
  type        = bool
  description = "Determines whether `security_configuration_name` is used as a prefix"
  default     = true
}

variable "service_iam_role_arn" {
  type        = string
  description = "The ARN of an existing IAM role to use for the service"
  default     = null
}

variable "service_iam_role_description" {
  type        = string
  description = "Description of the role"
  default     = null
}

variable "service_iam_role_name" {
  type        = string
  description = "Name to use on IAM role created"
  default     = null
}

variable "service_iam_role_policies" {
  type        = map(string)
  description = "Map of IAM policies to attach to the service role"
  default     = {
  "AmazonEMRServicePolicy_v2": "arn:aws:iam::aws:policy/service-role/AmazonEMRServicePolicy_v2"
}
}

variable "service_pass_role_policy_description" {
  type        = string
  description = "Description of the policy"
  default     = null
}

variable "service_pass_role_policy_name" {
  type        = string
  description = "Name to use on IAM policy created"
  default     = null
}

variable "service_security_group_description" {
  type        = string
  description = "Description of the security group created"
  default     = "Managed service access security group"
}

variable "service_security_group_rules" {
  type        = any
  description = "Security group rules to add to the security group created"
  default     = {}
}

variable "slave_security_group_description" {
  type        = string
  description = "Description of the security group created"
  default     = "Managed slave security group"
}

variable "slave_security_group_rules" {
  type        = any
  description = "Security group rules to add to the security group created"
  default     = {
  "default": {
    "cidr_blocks": [
      "0.0.0.0/0"
    ],
    "description": "Allow all egress traffic",
    "from_port": 0,
    "ipv6_cidr_blocks": [
      "::/0"
    ],
    "protocol": "-1",
    "to_port": 0,
    "type": "egress"
  }
}
}

variable "step" {
  type        = any
  description = "Steps to run when creating the cluster"
  default     = {}
}

variable "step_concurrency_level" {
  type        = number
  description = "Number of steps that can be executed concurrently. You can specify a maximum of 256 steps. Only valid for EMR clusters with `release_label` 5.28.0 or greater (default is 1)"
  default     = null
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "task_instance_fleet" {
  type        = any
  description = "Configuration block to use an [Instance Fleet](https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-instance-fleet.html) for the task node type. Cannot be specified if any `task_instance_group` configuration blocks are set"
  default     = {}
}

variable "task_instance_group" {
  type        = any
  description = "Configuration block to use an [Instance Group](https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-instance-group-configuration.html#emr-plan-instance-groups) for the [task node type](https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-master-core-task-nodes.html#emr-plan-master)"
  default     = {}
}

variable "termination_protection" {
  type        = bool
  description = "Switch on/off termination protection (default is `false`, except when using multiple master nodes). Before attempting to destroy the resource when termination protection is enabled, this configuration must be applied with its value set to `false`"
  default     = null
}

variable "unhealthy_node_replacement" {
  type        = bool
  description = "Whether whether Amazon EMR should gracefully replace core nodes that have degraded within the cluster. Default value is `false`"
  default     = null
}

variable "visible_to_all_users" {
  type        = bool
  description = "Whether the job flow is visible to all IAM users of the AWS account associated with the job flow. Default value is `true`"
  default     = null
}

variable "vpc_id" {
  type        = string
  description = "The ID of the Amazon Virtual Private Cloud (Amazon VPC) where the security groups will be created"
  default     = ""
}


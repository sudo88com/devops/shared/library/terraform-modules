
auth = {}

connection_borrow_timeout = null

create = true

create_iam_policy = true

create_iam_role = true

db_cluster_identifier = ""

db_instance_identifier = ""

debug_logging = false

endpoints = {}

engine_family = ""

iam_policy_name = ""

iam_role_description = ""

iam_role_force_detach_policies = true

iam_role_max_session_duration = 43200

iam_role_name = ""

iam_role_path = null

iam_role_permissions_boundary = null

iam_role_tags = {}

idle_client_timeout = 1800

init_query = ""

kms_key_arns = []

log_group_kms_key_id = null

log_group_retention_in_days = 30

log_group_tags = {}

manage_log_group = true

max_connections_percent = 90

max_idle_connections_percent = 50

name = ""

proxy_tags = {}

require_tls = true

role_arn = ""

session_pinning_filters = []

tags = {}

target_db_cluster = false

target_db_instance = false

use_policy_name_prefix = false

use_role_name_prefix = false

vpc_security_group_ids = []

vpc_subnet_ids = []


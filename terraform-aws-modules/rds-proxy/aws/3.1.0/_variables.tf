
variable "auth" {
  type        = any
  description = "Configuration block(s) with authorization mechanisms to connect to the associated instances or clusters"
  default     = {}
}

variable "connection_borrow_timeout" {
  type        = number
  description = "The number of seconds for a proxy to wait for a connection to become available in the connection pool"
  default     = null
}

variable "create" {
  type        = bool
  description = "Whether cluster should be created (affects nearly all resources)"
  default     = true
}

variable "create_iam_policy" {
  type        = bool
  description = "Determines whether an IAM policy is created"
  default     = true
}

variable "create_iam_role" {
  type        = bool
  description = "Determines whether an IAM role is created"
  default     = true
}

variable "db_cluster_identifier" {
  type        = string
  description = "DB cluster identifier"
  default     = ""
}

variable "db_instance_identifier" {
  type        = string
  description = "DB instance identifier"
  default     = ""
}

variable "debug_logging" {
  type        = bool
  description = "Whether the proxy includes detailed information about SQL statements in its logs"
  default     = false
}

variable "endpoints" {
  type        = any
  description = "Map of DB proxy endpoints to create and their attributes (see `aws_db_proxy_endpoint`)"
  default     = {}
}

variable "engine_family" {
  type        = string
  description = "The kind of database engine that the proxy will connect to. Valid values are `MYSQL` or `POSTGRESQL`"
  default     = ""
}

variable "iam_policy_name" {
  type        = string
  description = "The name of the role policy. If omitted, Terraform will assign a random, unique name"
  default     = ""
}

variable "iam_role_description" {
  type        = string
  description = "The description of the role"
  default     = ""
}

variable "iam_role_force_detach_policies" {
  type        = bool
  description = "Specifies to force detaching any policies the role has before destroying it"
  default     = true
}

variable "iam_role_max_session_duration" {
  type        = number
  description = "The maximum session duration (in seconds) that you want to set for the specified role"
  default     = 43200
}

variable "iam_role_name" {
  type        = string
  description = "The name of the role. If omitted, Terraform will assign a random, unique name"
  default     = ""
}

variable "iam_role_path" {
  type        = string
  description = "The path to the role"
  default     = null
}

variable "iam_role_permissions_boundary" {
  type        = string
  description = "The ARN of the policy that is used to set the permissions boundary for the role"
  default     = null
}

variable "iam_role_tags" {
  type        = map(string)
  description = "A map of tags to apply to the IAM role"
  default     = {}
}

variable "idle_client_timeout" {
  type        = number
  description = "The number of seconds that a connection to the proxy can be inactive before the proxy disconnects it"
  default     = 1800
}

variable "init_query" {
  type        = string
  description = "One or more SQL statements for the proxy to run when opening each new database connection"
  default     = ""
}

variable "kms_key_arns" {
  type        = list(string)
  description = "List of KMS Key ARNs to allow access to decrypt SecretsManager secrets"
  default     = []
}

variable "log_group_kms_key_id" {
  type        = string
  description = "The ARN of the KMS Key to use when encrypting log data"
  default     = null
}

variable "log_group_retention_in_days" {
  type        = number
  description = "Specifies the number of days you want to retain log events in the log group"
  default     = 30
}

variable "log_group_tags" {
  type        = map(string)
  description = "A map of tags to apply to the CloudWatch log group"
  default     = {}
}

variable "manage_log_group" {
  type        = bool
  description = "Determines whether Terraform will create/manage the CloudWatch log group or not. Note - this will fail if set to true after the log group has been created as the resource will already exist"
  default     = true
}

variable "max_connections_percent" {
  type        = number
  description = "The maximum size of the connection pool for each target in a target group"
  default     = 90
}

variable "max_idle_connections_percent" {
  type        = number
  description = "Controls how actively the proxy closes idle database connections in the connection pool"
  default     = 50
}

variable "name" {
  type        = string
  description = "The identifier for the proxy. This name must be unique for all proxies owned by your AWS account in the specified AWS Region. An identifier must begin with a letter and must contain only ASCII letters, digits, and hyphens; it can't end with a hyphen or contain two consecutive hyphens"
  default     = ""
}

variable "proxy_tags" {
  type        = map(string)
  description = "A map of tags to apply to the RDS Proxy"
  default     = {}
}

variable "require_tls" {
  type        = bool
  description = "A Boolean parameter that specifies whether Transport Layer Security (TLS) encryption is required for connections to the proxy"
  default     = true
}

variable "role_arn" {
  type        = string
  description = "The Amazon Resource Name (ARN) of the IAM role that the proxy uses to access secrets in AWS Secrets Manager"
  default     = ""
}

variable "session_pinning_filters" {
  type        = list(string)
  description = "Each item in the list represents a class of SQL operations that normally cause all later statements in a session using a proxy to be pinned to the same underlying database connection"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "target_db_cluster" {
  type        = bool
  description = "Determines whether DB cluster is targeted by proxy"
  default     = false
}

variable "target_db_instance" {
  type        = bool
  description = "Determines whether DB instance is targeted by proxy"
  default     = false
}

variable "use_policy_name_prefix" {
  type        = bool
  description = "Whether to use unique name beginning with the specified `iam_policy_name`"
  default     = false
}

variable "use_role_name_prefix" {
  type        = bool
  description = "Whether to use unique name beginning with the specified `iam_role_name`"
  default     = false
}

variable "vpc_security_group_ids" {
  type        = list(string)
  description = "One or more VPC security group IDs to associate with the new proxy"
  default     = []
}

variable "vpc_subnet_ids" {
  type        = list(string)
  description = "One or more VPC subnet IDs to associate with the new proxy"
  default     = []
}


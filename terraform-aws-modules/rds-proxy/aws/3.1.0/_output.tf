
output "db_proxy_endpoints" {
  description = "Array containing the full resource object and attributes for all DB proxy endpoints created"
  value       = module.rds-proxy.db_proxy_endpoints
}

output "iam_role_arn" {
  description = "The Amazon Resource Name (ARN) of the IAM role that the proxy uses to access secrets in AWS Secrets Manager."
  value       = module.rds-proxy.iam_role_arn
}

output "iam_role_name" {
  description = "IAM role name"
  value       = module.rds-proxy.iam_role_name
}

output "iam_role_unique_id" {
  description = "Stable and unique string identifying the IAM role"
  value       = module.rds-proxy.iam_role_unique_id
}

output "log_group_arn" {
  description = "The Amazon Resource Name (ARN) of the CloudWatch log group"
  value       = module.rds-proxy.log_group_arn
}

output "proxy_arn" {
  description = "The Amazon Resource Name (ARN) for the proxy"
  value       = module.rds-proxy.proxy_arn
}

output "proxy_default_target_group_arn" {
  description = "The Amazon Resource Name (ARN) for the default target group"
  value       = module.rds-proxy.proxy_default_target_group_arn
}

output "proxy_default_target_group_id" {
  description = "The ID for the default target group"
  value       = module.rds-proxy.proxy_default_target_group_id
}

output "proxy_default_target_group_name" {
  description = "The name of the default target group"
  value       = module.rds-proxy.proxy_default_target_group_name
}

output "proxy_endpoint" {
  description = "The endpoint that you can use to connect to the proxy"
  value       = module.rds-proxy.proxy_endpoint
}

output "proxy_id" {
  description = "The ID for the proxy"
  value       = module.rds-proxy.proxy_id
}

output "proxy_target_endpoint" {
  description = "Hostname for the target RDS DB Instance. Only returned for `RDS_INSTANCE` type"
  value       = module.rds-proxy.proxy_target_endpoint
}

output "proxy_target_id" {
  description = "Identifier of `db_proxy_name`, `target_group_name`, target type (e.g. `RDS_INSTANCE` or `TRACKED_CLUSTER`), and resource identifier separated by forward slashes (/)"
  value       = module.rds-proxy.proxy_target_id
}

output "proxy_target_port" {
  description = "Port for the target RDS DB Instance or Aurora DB Cluster"
  value       = module.rds-proxy.proxy_target_port
}

output "proxy_target_rds_resource_id" {
  description = "Identifier representing the DB Instance or DB Cluster target"
  value       = module.rds-proxy.proxy_target_rds_resource_id
}

output "proxy_target_target_arn" {
  description = "Amazon Resource Name (ARN) for the DB instance or DB cluster. Currently not returned by the RDS API"
  value       = module.rds-proxy.proxy_target_target_arn
}

output "proxy_target_tracked_cluster_id" {
  description = "DB Cluster identifier for the DB Instance target. Not returned unless manually importing an RDS_INSTANCE target that is part of a DB Cluster"
  value       = module.rds-proxy.proxy_target_tracked_cluster_id
}

output "proxy_target_type" {
  description = "Type of target. e.g. `RDS_INSTANCE` or `TRACKED_CLUSTER`"
  value       = module.rds-proxy.proxy_target_type
}


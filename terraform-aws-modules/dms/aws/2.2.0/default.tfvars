
access_iam_role_description = null

access_iam_role_name = null

access_iam_role_path = null

access_iam_role_permissions_boundary = null

access_iam_role_policies = {}

access_iam_role_tags = {}

access_iam_role_use_name_prefix = true

access_iam_statements = {}

access_kms_key_arns = []

access_secret_arns = []

access_source_s3_bucket_arns = []

access_target_dynamodb_table_arns = []

access_target_elasticsearch_arns = []

access_target_kinesis_arns = []

access_target_s3_bucket_arns = []

certificates = {}

create = true

create_access_iam_role = true

create_access_policy = true

create_iam_roles = true

create_repl_instance = true

create_repl_subnet_group = true

enable_redshift_target_permissions = false

endpoints = {}

event_subscription_timeouts = {}

event_subscriptions = {}

iam_role_permissions_boundary = null

iam_role_tags = {}

repl_instance_allocated_storage = null

repl_instance_allow_major_version_upgrade = true

repl_instance_apply_immediately = null

repl_instance_auto_minor_version_upgrade = true

repl_instance_availability_zone = null

repl_instance_class = null

repl_instance_engine_version = null

repl_instance_id = null

repl_instance_kms_key_arn = null

repl_instance_multi_az = null

repl_instance_network_type = null

repl_instance_preferred_maintenance_window = null

repl_instance_publicly_accessible = null

repl_instance_subnet_group_id = null

repl_instance_tags = {}

repl_instance_timeouts = {}

repl_instance_vpc_security_group_ids = null

repl_subnet_group_description = null

repl_subnet_group_name = null

repl_subnet_group_subnet_ids = []

repl_subnet_group_tags = {}

replication_tasks = {}

s3_endpoints = {}

tags = {}



block_public_policy = null

create = true

create_policy = false

create_random_password = false

description = null

enable_rotation = false

force_overwrite_replica_secret = null

ignore_secret_changes = false

kms_key_id = null

name = null

name_prefix = null

override_policy_documents = []

policy_statements = {}

random_password_length = 32

random_password_override_special = "!@#$%\u0026*()-_=+[]{}\u003c\u003e:?"

recovery_window_in_days = null

replica = {}

rotation_lambda_arn = ""

rotation_rules = {}

secret_binary = null

secret_string = null

source_policy_documents = []

tags = {}

version_stages = null



locals {
  block_public_policy = var.block_public_policy
  create = var.create
  create_policy = var.create_policy
  create_random_password = var.create_random_password
  description = var.description
  enable_rotation = var.enable_rotation
  force_overwrite_replica_secret = var.force_overwrite_replica_secret
  ignore_secret_changes = var.ignore_secret_changes
  kms_key_id = var.kms_key_id
  name = var.name
  name_prefix = var.name_prefix
  override_policy_documents = var.override_policy_documents
  policy_statements = var.policy_statements
  random_password_length = var.random_password_length
  random_password_override_special = var.random_password_override_special
  recovery_window_in_days = var.recovery_window_in_days
  replica = var.replica
  rotation_lambda_arn = var.rotation_lambda_arn
  rotation_rules = var.rotation_rules
  secret_binary = var.secret_binary
  secret_string = var.secret_string
  source_policy_documents = var.source_policy_documents
  tags = var.tags
  version_stages = var.version_stages
}


variable "block_public_policy" {
  type        = bool
  description = "Makes an optional API call to Zelkova to validate the Resource Policy to prevent broad access to your secret"
  default     = null
}

variable "create" {
  type        = bool
  description = "Determines whether resources will be created (affects all resources)"
  default     = true
}

variable "create_policy" {
  type        = bool
  description = "Determines whether a policy will be created"
  default     = false
}

variable "create_random_password" {
  type        = bool
  description = "Determines whether a random password will be generated"
  default     = false
}

variable "description" {
  type        = string
  description = "A description of the secret"
  default     = null
}

variable "enable_rotation" {
  type        = bool
  description = "Determines whether secret rotation is enabled"
  default     = false
}

variable "force_overwrite_replica_secret" {
  type        = bool
  description = "Accepts boolean value to specify whether to overwrite a secret with the same name in the destination Region"
  default     = null
}

variable "ignore_secret_changes" {
  type        = bool
  description = "Determines whether or not Terraform will ignore changes made externally to `secret_string` or `secret_binary`. Changing this value after creation is a destructive operation"
  default     = false
}

variable "kms_key_id" {
  type        = string
  description = "ARN or Id of the AWS KMS key to be used to encrypt the secret values in the versions stored in this secret. If you need to reference a CMK in a different account, you can use only the key ARN. If you don't specify this value, then Secrets Manager defaults to using the AWS account's default KMS key (the one named `aws/secretsmanager`"
  default     = null
}

variable "name" {
  type        = string
  description = "Friendly name of the new secret. The secret name can consist of uppercase letters, lowercase letters, digits, and any of the following characters: `/_+=.@-`"
  default     = null
}

variable "name_prefix" {
  type        = string
  description = "Creates a unique name beginning with the specified prefix"
  default     = null
}

variable "override_policy_documents" {
  type        = list(string)
  description = "List of IAM policy documents that are merged together into the exported document. In merging, statements with non-blank `sid`s will override statements with the same `sid`"
  default     = []
}

variable "policy_statements" {
  type        = map(any)
  description = "A map of IAM policy [statements](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document#statement) for custom permission usage"
  default     = {}
}

variable "random_password_length" {
  type        = number
  description = "The length of the generated random password"
  default     = 32
}

variable "random_password_override_special" {
  type        = string
  description = "Supply your own list of special characters to use for string generation. This overrides the default character list in the special argument"
  default     = "!@#$%\u0026*()-_=+[]{}\u003c\u003e:?"
}

variable "recovery_window_in_days" {
  type        = number
  description = "Number of days that AWS Secrets Manager waits before it can delete the secret. This value can be `0` to force deletion without recovery or range from `7` to `30` days. The default value is `30`"
  default     = null
}

variable "replica" {
  type        = map(any)
  description = "Configuration block to support secret replication"
  default     = {}
}

variable "rotation_lambda_arn" {
  type        = string
  description = "Specifies the ARN of the Lambda function that can rotate the secret"
  default     = ""
}

variable "rotation_rules" {
  type        = map(any)
  description = "A structure that defines the rotation configuration for this secret"
  default     = {}
}

variable "secret_binary" {
  type        = string
  description = "Specifies binary data that you want to encrypt and store in this version of the secret. This is required if `secret_string` is not set. Needs to be encoded to base64"
  default     = null
}

variable "secret_string" {
  type        = string
  description = "Specifies text data that you want to encrypt and store in this version of the secret. This is required if `secret_binary` is not set"
  default     = null
}

variable "source_policy_documents" {
  type        = list(string)
  description = "List of IAM policy documents that are merged together into the exported document. Statements must have unique `sid`s"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "version_stages" {
  type        = list(string)
  description = "Specifies a list of staging labels that are attached to this version of the secret. A staging label must be unique to a single version of the secret"
  default     = null
}


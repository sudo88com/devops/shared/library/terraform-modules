
api_key_selection_expression = null

api_mapping_key = null

api_version = null

authorizers = {}

body = null

cors_configuration = {}

create = true

create_certificate = true

create_domain_name = true

create_domain_records = true

create_routes_and_integrations = true

create_stage = true

credentials_arn = null

deploy_stage = true

description = null

disable_execute_api_endpoint = null

domain_name = ""

domain_name_certificate_arn = null

domain_name_ownership_verification_certificate_arn = null

fail_on_warnings = null

mutual_tls_authentication = {}

name = ""

protocol_type = "HTTP"

route_key = null

route_selection_expression = null

routes = {}

stage_access_log_settings = {}

stage_client_certificate_id = null

stage_default_route_settings = {}

stage_description = null

stage_name = "$default"

stage_tags = {}

stage_variables = {}

subdomain_record_types = [
  "A",
  "AAAA"
]

subdomains = []

tags = {}

target = null

vpc_link_tags = {}

vpc_links = {}


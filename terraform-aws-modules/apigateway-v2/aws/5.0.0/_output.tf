
output "acm_certificate_arn" {
  description = "The ARN of the certificate"
  value       = module.apigateway-v2.acm_certificate_arn
}

output "api_arn" {
  description = "The ARN of the API"
  value       = module.apigateway-v2.api_arn
}

output "api_endpoint" {
  description = "URI of the API, of the form `https://{api-id}.execute-api.{region}.amazonaws.com` for HTTP APIs and `wss://{api-id}.execute-api.{region}.amazonaws.com` for WebSocket APIs"
  value       = module.apigateway-v2.api_endpoint
}

output "api_execution_arn" {
  description = "The ARN prefix to be used in an `aws_lambda_permission`'s `source_arn` attribute or in an `aws_iam_policy` to authorize access to the `@connections` API"
  value       = module.apigateway-v2.api_execution_arn
}

output "api_id" {
  description = "The API identifier"
  value       = module.apigateway-v2.api_id
}

output "authorizers" {
  description = "Map of API Gateway Authorizer(s) created and their attributes"
  value       = module.apigateway-v2.authorizers
}

output "domain_name_api_mapping_selection_expression" {
  description = "The API mapping selection expression for the domain name"
  value       = module.apigateway-v2.domain_name_api_mapping_selection_expression
}

output "domain_name_arn" {
  description = "The ARN of the domain name"
  value       = module.apigateway-v2.domain_name_arn
}

output "domain_name_configuration" {
  description = "The domain name configuration"
  value       = module.apigateway-v2.domain_name_configuration
}

output "domain_name_hosted_zone_id" {
  description = "The Amazon Route 53 Hosted Zone ID of the endpoint"
  value       = module.apigateway-v2.domain_name_hosted_zone_id
}

output "domain_name_id" {
  description = "The domain name identifier"
  value       = module.apigateway-v2.domain_name_id
}

output "domain_name_target_domain_name" {
  description = "The target domain name"
  value       = module.apigateway-v2.domain_name_target_domain_name
}

output "integrations" {
  description = "Map of the integrations created and their attributes"
  value       = module.apigateway-v2.integrations
}

output "routes" {
  description = "Map of the routes created and their attributes"
  value       = module.apigateway-v2.routes
}

output "stage_access_logs_cloudwatch_log_group_arn" {
  description = "Arn of cloudwatch log group created"
  value       = module.apigateway-v2.stage_access_logs_cloudwatch_log_group_arn
}

output "stage_access_logs_cloudwatch_log_group_name" {
  description = "Name of cloudwatch log group created"
  value       = module.apigateway-v2.stage_access_logs_cloudwatch_log_group_name
}

output "stage_arn" {
  description = "The stage ARN"
  value       = module.apigateway-v2.stage_arn
}

output "stage_domain_name" {
  description = "Domain name of the stage (useful for CloudFront distribution)"
  value       = module.apigateway-v2.stage_domain_name
}

output "stage_execution_arn" {
  description = "The ARN prefix to be used in an aws_lambda_permission's source_arn attribute or in an aws_iam_policy to authorize access to the @connections API"
  value       = module.apigateway-v2.stage_execution_arn
}

output "stage_id" {
  description = "The stage identifier"
  value       = module.apigateway-v2.stage_id
}

output "stage_invoke_url" {
  description = "The URL to invoke the API pointing to the stage"
  value       = module.apigateway-v2.stage_invoke_url
}

output "vpc_links" {
  description = "Map of VPC links created and their attributes"
  value       = module.apigateway-v2.vpc_links
}



variable "create" {
  type        = bool
  description = "Determines whether resources will be created (affects all resources)"
  default     = true
}

variable "create_private_key" {
  type        = bool
  description = "Determines whether a private key will be created"
  default     = false
}

variable "key_name" {
  type        = string
  description = "The name for the key pair. Conflicts with `key_name_prefix`"
  default     = null
}

variable "key_name_prefix" {
  type        = string
  description = "Creates a unique name beginning with the specified prefix. Conflicts with `key_name`"
  default     = null
}

variable "private_key_algorithm" {
  type        = string
  description = "Name of the algorithm to use when generating the private key. Currently-supported values are `RSA` and `ED25519`"
  default     = "RSA"
}

variable "private_key_rsa_bits" {
  type        = number
  description = "When algorithm is `RSA`, the size of the generated RSA key, in bits (default: `4096`)"
  default     = 4096
}

variable "public_key" {
  type        = string
  description = "The public key material"
  default     = ""
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}



variable "autoscaling_capacity_providers" {
  type        = any
  description = "Map of autoscaling capacity provider definitions to create for the cluster"
  default     = {}
}

variable "cloudwatch_log_group_kms_key_id" {
  type        = string
  description = "If a KMS Key ARN is set, this key will be used to encrypt the corresponding log group. Please be sure that the KMS Key has an appropriate key policy (https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/encrypt-log-data-kms.html)"
  default     = null
}

variable "cloudwatch_log_group_name" {
  type        = string
  description = "Custom name of CloudWatch Log Group for ECS cluster"
  default     = null
}

variable "cloudwatch_log_group_retention_in_days" {
  type        = number
  description = "Number of days to retain log events"
  default     = 90
}

variable "cloudwatch_log_group_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the log group created"
  default     = {}
}

variable "cluster_configuration" {
  type        = any
  description = "The execute command configuration for the cluster"
  default     = {}
}

variable "cluster_name" {
  type        = string
  description = "Name of the cluster (up to 255 letters, numbers, hyphens, and underscores)"
  default     = ""
}

variable "cluster_service_connect_defaults" {
  type        = map(string)
  description = "Configures a default Service Connect namespace"
  default     = {}
}

variable "cluster_settings" {
  type        = any
  description = "List of configuration block(s) with cluster settings. For example, this can be used to enable CloudWatch Container Insights for a cluster"
  default     = [
  {
    "name": "containerInsights",
    "value": "enabled"
  }
]
}

variable "cluster_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the cluster"
  default     = {}
}

variable "create" {
  type        = bool
  description = "Determines whether resources will be created (affects all resources)"
  default     = true
}

variable "create_cloudwatch_log_group" {
  type        = bool
  description = "Determines whether a log group is created by this module for the cluster logs. If not, AWS will automatically create one if logging is enabled"
  default     = true
}

variable "create_task_exec_iam_role" {
  type        = bool
  description = "Determines whether the ECS task definition IAM role should be created"
  default     = false
}

variable "create_task_exec_policy" {
  type        = bool
  description = "Determines whether the ECS task definition IAM policy should be created. This includes permissions included in AmazonECSTaskExecutionRolePolicy as well as access to secrets and SSM parameters"
  default     = true
}

variable "default_capacity_provider_use_fargate" {
  type        = bool
  description = "Determines whether to use Fargate or autoscaling for default capacity provider strategy"
  default     = true
}

variable "fargate_capacity_providers" {
  type        = any
  description = "Map of Fargate capacity provider definitions to use for the cluster"
  default     = {}
}

variable "services" {
  type        = any
  description = "Map of service definitions to create"
  default     = {}
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "task_exec_iam_role_description" {
  type        = string
  description = "Description of the role"
  default     = null
}

variable "task_exec_iam_role_name" {
  type        = string
  description = "Name to use on IAM role created"
  default     = null
}

variable "task_exec_iam_role_path" {
  type        = string
  description = "IAM role path"
  default     = null
}

variable "task_exec_iam_role_permissions_boundary" {
  type        = string
  description = "ARN of the policy that is used to set the permissions boundary for the IAM role"
  default     = null
}

variable "task_exec_iam_role_policies" {
  type        = map(string)
  description = "Map of IAM role policy ARNs to attach to the IAM role"
  default     = {}
}

variable "task_exec_iam_role_tags" {
  type        = map(string)
  description = "A map of additional tags to add to the IAM role created"
  default     = {}
}

variable "task_exec_iam_role_use_name_prefix" {
  type        = bool
  description = "Determines whether the IAM role name (`task_exec_iam_role_name`) is used as a prefix"
  default     = true
}

variable "task_exec_iam_statements" {
  type        = any
  description = "A map of IAM policy [statements](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document#statement) for custom permission usage"
  default     = {}
}

variable "task_exec_secret_arns" {
  type        = list(string)
  description = "List of SecretsManager secret ARNs the task execution role will be permitted to get/read"
  default     = [
  "arn:aws:secretsmanager:*:*:secret:*"
]
}

variable "task_exec_ssm_param_arns" {
  type        = list(string)
  description = "List of SSM parameter ARNs the task execution role will be permitted to get/read"
  default     = [
  "arn:aws:ssm:*:*:parameter/*"
]
}


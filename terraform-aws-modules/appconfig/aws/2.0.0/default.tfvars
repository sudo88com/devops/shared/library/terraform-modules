
config_profile_description = null

config_profile_location_uri = "hosted"

config_profile_name = null

config_profile_retrieval_role_arn = null

config_profile_tags = {}

config_profile_type = null

config_profile_validator = []

create = true

create_deployment_strategy = true

create_retrieval_role = true

deployment_configuration_version = null

deployment_description = null

deployment_strategy_deployment_duration_in_minutes = 0

deployment_strategy_description = null

deployment_strategy_final_bake_time_in_minutes = 0

deployment_strategy_growth_factor = 100

deployment_strategy_growth_type = null

deployment_strategy_id = null

deployment_strategy_name = null

deployment_strategy_replicate_to = "NONE"

deployment_strategy_tags = {}

deployment_tags = {}

description = null

environments = {}

hosted_config_version_content = null

hosted_config_version_content_type = null

hosted_config_version_description = null

name = ""

retrieval_role_description = null

retrieval_role_name = ""

retrieval_role_path = null

retrieval_role_permissions_boundary = null

retrieval_role_tags = {}

retrieval_role_use_name_prefix = true

s3_configuration_bucket_arn = null

s3_configuration_object_key = "*"

ssm_document_configuration_arn = null

ssm_parameter_configuration_arn = null

tags = {}

use_hosted_configuration = false

use_s3_configuration = false

use_ssm_document_configuration = false

use_ssm_parameter_configuration = false


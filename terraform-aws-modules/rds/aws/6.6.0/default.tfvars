
allocated_storage = null

allow_major_version_upgrade = false

apply_immediately = false

auto_minor_version_upgrade = true

availability_zone = null

backup_retention_period = null

backup_window = null

blue_green_update = {}

ca_cert_identifier = null

character_set_name = null

cloudwatch_log_group_kms_key_id = null

cloudwatch_log_group_retention_in_days = 7

copy_tags_to_snapshot = false

create_cloudwatch_log_group = false

create_db_instance = true

create_db_option_group = true

create_db_parameter_group = true

create_db_subnet_group = false

create_monitoring_role = false

custom_iam_instance_profile = null

db_instance_role_associations = {}

db_instance_tags = {}

db_name = null

db_option_group_tags = {}

db_parameter_group_tags = {}

db_subnet_group_description = null

db_subnet_group_name = null

db_subnet_group_tags = {}

db_subnet_group_use_name_prefix = true

dedicated_log_volume = false

delete_automated_backups = true

deletion_protection = false

domain = null

domain_auth_secret_arn = null

domain_dns_ips = null

domain_fqdn = null

domain_iam_role_name = null

domain_ou = null

enabled_cloudwatch_logs_exports = []

engine = null

engine_version = null

family = null

final_snapshot_identifier_prefix = "final"

iam_database_authentication_enabled = false

identifier = 

instance_class = null

instance_use_identifier_prefix = false

iops = null

kms_key_id = null

license_model = null

maintenance_window = null

major_engine_version = null

manage_master_user_password = true

manage_master_user_password_rotation = false

master_user_password_rotate_immediately = null

master_user_password_rotation_automatically_after_days = null

master_user_password_rotation_duration = null

master_user_password_rotation_schedule_expression = null

master_user_secret_kms_key_id = null

max_allocated_storage = 0

monitoring_interval = 0

monitoring_role_arn = null

monitoring_role_description = null

monitoring_role_name = "rds-monitoring-role"

monitoring_role_permissions_boundary = null

monitoring_role_use_name_prefix = false

multi_az = false

nchar_character_set_name = null

network_type = null

option_group_description = null

option_group_name = null

option_group_timeouts = {}

option_group_use_name_prefix = true

options = []

parameter_group_description = null

parameter_group_name = null

parameter_group_use_name_prefix = true

parameters = []

password = null

performance_insights_enabled = false

performance_insights_kms_key_id = null

performance_insights_retention_period = 7

port = null

publicly_accessible = false

putin_khuylo = true

replica_mode = null

replicate_source_db = null

restore_to_point_in_time = null

s3_import = null

skip_final_snapshot = false

snapshot_identifier = null

storage_encrypted = true

storage_throughput = null

storage_type = null

subnet_ids = []

tags = {}

timeouts = {}

timezone = null

username = null

vpc_security_group_ids = []


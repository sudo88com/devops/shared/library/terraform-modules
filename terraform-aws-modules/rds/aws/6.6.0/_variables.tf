
variable "allocated_storage" {
  type        = number
  description = "The allocated storage in gigabytes"
  default     = null
}

variable "allow_major_version_upgrade" {
  type        = bool
  description = "Indicates that major version upgrades are allowed. Changing this parameter does not result in an outage and the change is asynchronously applied as soon as possible"
  default     = false
}

variable "apply_immediately" {
  type        = bool
  description = "Specifies whether any database modifications are applied immediately, or during the next maintenance window"
  default     = false
}

variable "auto_minor_version_upgrade" {
  type        = bool
  description = "Indicates that minor engine upgrades will be applied automatically to the DB instance during the maintenance window"
  default     = true
}

variable "availability_zone" {
  type        = string
  description = "The Availability Zone of the RDS instance"
  default     = null
}

variable "backup_retention_period" {
  type        = number
  description = "The days to retain backups for"
  default     = null
}

variable "backup_window" {
  type        = string
  description = "The daily time range (in UTC) during which automated backups are created if they are enabled. Example: '09:46-10:16'. Must not overlap with maintenance_window"
  default     = null
}

variable "blue_green_update" {
  type        = map(string)
  description = "Enables low-downtime updates using RDS Blue/Green deployments."
  default     = {}
}

variable "ca_cert_identifier" {
  type        = string
  description = "Specifies the identifier of the CA certificate for the DB instance"
  default     = null
}

variable "character_set_name" {
  type        = string
  description = "The character set name to use for DB encoding in Oracle instances. This can't be changed. See Oracle Character Sets Supported in Amazon RDS and Collations and Character Sets for Microsoft SQL Server for more information. This can only be set on creation"
  default     = null
}

variable "cloudwatch_log_group_kms_key_id" {
  type        = string
  description = "The ARN of the KMS Key to use when encrypting log data"
  default     = null
}

variable "cloudwatch_log_group_retention_in_days" {
  type        = number
  description = "The number of days to retain CloudWatch logs for the DB instance"
  default     = 7
}

variable "copy_tags_to_snapshot" {
  type        = bool
  description = "On delete, copy all Instance tags to the final snapshot"
  default     = false
}

variable "create_cloudwatch_log_group" {
  type        = bool
  description = "Determines whether a CloudWatch log group is created for each `enabled_cloudwatch_logs_exports`"
  default     = false
}

variable "create_db_instance" {
  type        = bool
  description = "Whether to create a database instance"
  default     = true
}

variable "create_db_option_group" {
  type        = bool
  description = "Create a database option group"
  default     = true
}

variable "create_db_parameter_group" {
  type        = bool
  description = "Whether to create a database parameter group"
  default     = true
}

variable "create_db_subnet_group" {
  type        = bool
  description = "Whether to create a database subnet group"
  default     = false
}

variable "create_monitoring_role" {
  type        = bool
  description = "Create IAM role with a defined name that permits RDS to send enhanced monitoring metrics to CloudWatch Logs"
  default     = false
}

variable "custom_iam_instance_profile" {
  type        = string
  description = "RDS custom iam instance profile"
  default     = null
}

variable "db_instance_role_associations" {
  type        = map(any)
  description = "A map of DB instance supported feature name to role association ARNs."
  default     = {}
}

variable "db_instance_tags" {
  type        = map(string)
  description = "Additional tags for the DB instance"
  default     = {}
}

variable "db_name" {
  type        = string
  description = "The DB name to create. If omitted, no database is created initially"
  default     = null
}

variable "db_option_group_tags" {
  type        = map(string)
  description = "Additional tags for the DB option group"
  default     = {}
}

variable "db_parameter_group_tags" {
  type        = map(string)
  description = "Additional tags for the  DB parameter group"
  default     = {}
}

variable "db_subnet_group_description" {
  type        = string
  description = "Description of the DB subnet group to create"
  default     = null
}

variable "db_subnet_group_name" {
  type        = string
  description = "Name of DB subnet group. DB instance will be created in the VPC associated with the DB subnet group. If unspecified, will be created in the default VPC"
  default     = null
}

variable "db_subnet_group_tags" {
  type        = map(string)
  description = "Additional tags for the DB subnet group"
  default     = {}
}

variable "db_subnet_group_use_name_prefix" {
  type        = bool
  description = "Determines whether to use `subnet_group_name` as is or create a unique name beginning with the `subnet_group_name` as the prefix"
  default     = true
}

variable "dedicated_log_volume" {
  type        = bool
  description = "Use a dedicated log volume (DLV) for the DB instance. Requires Provisioned IOPS."
  default     = false
}

variable "delete_automated_backups" {
  type        = bool
  description = "Specifies whether to remove automated backups immediately after the DB instance is deleted"
  default     = true
}

variable "deletion_protection" {
  type        = bool
  description = "The database can't be deleted when this value is set to true"
  default     = false
}

variable "domain" {
  type        = string
  description = "The ID of the Directory Service Active Directory domain to create the instance in"
  default     = null
}

variable "domain_auth_secret_arn" {
  type        = string
  description = "(Optional, but required if domain_fqdn is provided) The ARN for the Secrets Manager secret with the self managed Active Directory credentials for the user joining the domain. Conflicts with domain and domain_iam_role_name."
  default     = null
}

variable "domain_dns_ips" {
  type        = list(string)
  description = "(Optional, but required if domain_fqdn is provided) The IPv4 DNS IP addresses of your primary and secondary self managed Active Directory domain controllers. Two IP addresses must be provided. If there isn't a secondary domain controller, use the IP address of the primary domain controller for both entries in the list. Conflicts with domain and domain_iam_role_name."
  default     = null
}

variable "domain_fqdn" {
  type        = string
  description = "The fully qualified domain name (FQDN) of the self managed Active Directory domain. Conflicts with domain and domain_iam_role_name."
  default     = null
}

variable "domain_iam_role_name" {
  type        = string
  description = "(Required if domain is provided) The name of the IAM role to be used when making API calls to the Directory Service"
  default     = null
}

variable "domain_ou" {
  type        = string
  description = "(Optional, but required if domain_fqdn is provided) The self managed Active Directory organizational unit for your DB instance to join. Conflicts with domain and domain_iam_role_name."
  default     = null
}

variable "enabled_cloudwatch_logs_exports" {
  type        = list(string)
  description = "List of log types to enable for exporting to CloudWatch logs. If omitted, no logs will be exported. Valid values (depending on engine): alert, audit, error, general, listener, slowquery, trace, postgresql (PostgreSQL), upgrade (PostgreSQL)"
  default     = []
}

variable "engine" {
  type        = string
  description = "The database engine to use"
  default     = null
}

variable "engine_version" {
  type        = string
  description = "The engine version to use"
  default     = null
}

variable "family" {
  type        = string
  description = "The family of the DB parameter group"
  default     = null
}

variable "final_snapshot_identifier_prefix" {
  type        = string
  description = "The name which is prefixed to the final snapshot on cluster destroy"
  default     = "final"
}

variable "iam_database_authentication_enabled" {
  type        = bool
  description = "Specifies whether or not the mappings of AWS Identity and Access Management (IAM) accounts to database accounts are enabled"
  default     = false
}

variable "identifier" {
  type        = string
  description = "The name of the RDS instance"
  default     = ""
}

variable "instance_class" {
  type        = string
  description = "The instance type of the RDS instance"
  default     = null
}

variable "instance_use_identifier_prefix" {
  type        = bool
  description = "Determines whether to use `identifier` as is or create a unique identifier beginning with `identifier` as the specified prefix"
  default     = false
}

variable "iops" {
  type        = number
  description = "The amount of provisioned IOPS. Setting this implies a storage_type of 'io1' or `gp3`. See `notes` for limitations regarding this variable for `gp3`"
  default     = null
}

variable "kms_key_id" {
  type        = string
  description = "The ARN for the KMS encryption key. If creating an encrypted replica, set this to the destination KMS ARN. If storage_encrypted is set to true and kms_key_id is not specified the default KMS key created in your account will be used. Be sure to use the full ARN, not a key alias."
  default     = null
}

variable "license_model" {
  type        = string
  description = "License model information for this DB instance. Optional, but required for some DB engines, i.e. Oracle SE1"
  default     = null
}

variable "maintenance_window" {
  type        = string
  description = "The window to perform maintenance in. Syntax: 'ddd:hh24:mi-ddd:hh24:mi'. Eg: 'Mon:00:00-Mon:03:00'"
  default     = null
}

variable "major_engine_version" {
  type        = string
  description = "Specifies the major version of the engine that this option group should be associated with"
  default     = null
}

variable "manage_master_user_password" {
  type        = bool
  description = "Set to true to allow RDS to manage the master user password in Secrets Manager"
  default     = true
}

variable "manage_master_user_password_rotation" {
  type        = bool
  description = "Whether to manage the master user password rotation. By default, false on creation, rotation is managed by RDS. Setting this value to false after previously having been set to true will disable automatic rotation."
  default     = false
}

variable "master_user_password_rotate_immediately" {
  type        = bool
  description = "Specifies whether to rotate the secret immediately or wait until the next scheduled rotation window."
  default     = null
}

variable "master_user_password_rotation_automatically_after_days" {
  type        = number
  description = "Specifies the number of days between automatic scheduled rotations of the secret. Either automatically_after_days or schedule_expression must be specified."
  default     = null
}

variable "master_user_password_rotation_duration" {
  type        = string
  description = "The length of the rotation window in hours. For example, 3h for a three hour window."
  default     = null
}

variable "master_user_password_rotation_schedule_expression" {
  type        = string
  description = "A cron() or rate() expression that defines the schedule for rotating your secret. Either automatically_after_days or schedule_expression must be specified."
  default     = null
}

variable "master_user_secret_kms_key_id" {
  type        = string
  description = "  The key ARN, key ID, alias ARN or alias name for the KMS key to encrypt the master user password secret in Secrets Manager.\n  If not specified, the default KMS key for your Amazon Web Services account is used.\n"
  default     = null
}

variable "max_allocated_storage" {
  type        = number
  description = "Specifies the value for Storage Autoscaling"
  default     = 0
}

variable "monitoring_interval" {
  type        = number
  description = "The interval, in seconds, between points when Enhanced Monitoring metrics are collected for the DB instance. To disable collecting Enhanced Monitoring metrics, specify 0. The default is 0. Valid Values: 0, 1, 5, 10, 15, 30, 60"
  default     = 0
}

variable "monitoring_role_arn" {
  type        = string
  description = "The ARN for the IAM role that permits RDS to send enhanced monitoring metrics to CloudWatch Logs. Must be specified if monitoring_interval is non-zero"
  default     = null
}

variable "monitoring_role_description" {
  type        = string
  description = "Description of the monitoring IAM role"
  default     = null
}

variable "monitoring_role_name" {
  type        = string
  description = "Name of the IAM role which will be created when create_monitoring_role is enabled"
  default     = "rds-monitoring-role"
}

variable "monitoring_role_permissions_boundary" {
  type        = string
  description = "ARN of the policy that is used to set the permissions boundary for the monitoring IAM role"
  default     = null
}

variable "monitoring_role_use_name_prefix" {
  type        = bool
  description = "Determines whether to use `monitoring_role_name` as is or create a unique identifier beginning with `monitoring_role_name` as the specified prefix"
  default     = false
}

variable "multi_az" {
  type        = bool
  description = "Specifies if the RDS instance is multi-AZ"
  default     = false
}

variable "nchar_character_set_name" {
  type        = string
  description = "The national character set is used in the NCHAR, NVARCHAR2, and NCLOB data types for Oracle instances. This can't be changed."
  default     = null
}

variable "network_type" {
  type        = string
  description = "The type of network stack to use"
  default     = null
}

variable "option_group_description" {
  type        = string
  description = "The description of the option group"
  default     = null
}

variable "option_group_name" {
  type        = string
  description = "Name of the option group"
  default     = null
}

variable "option_group_timeouts" {
  type        = map(string)
  description = "Define maximum timeout for deletion of `aws_db_option_group` resource"
  default     = {}
}

variable "option_group_use_name_prefix" {
  type        = bool
  description = "Determines whether to use `option_group_name` as is or create a unique name beginning with the `option_group_name` as the prefix"
  default     = true
}

variable "options" {
  type        = any
  description = "A list of Options to apply"
  default     = []
}

variable "parameter_group_description" {
  type        = string
  description = "Description of the DB parameter group to create"
  default     = null
}

variable "parameter_group_name" {
  type        = string
  description = "Name of the DB parameter group to associate or create"
  default     = null
}

variable "parameter_group_use_name_prefix" {
  type        = bool
  description = "Determines whether to use `parameter_group_name` as is or create a unique name beginning with the `parameter_group_name` as the prefix"
  default     = true
}

variable "parameters" {
  type        = list(map(string))
  description = "A list of DB parameters (map) to apply"
  default     = []
}

variable "password" {
  type        = string
  description = "  Password for the master DB user. Note that this may show up in logs, and it will be stored in the state file.\n  The password provided will not be used if `manage_master_user_password` is set to true.\n"
  default     = null
}

variable "performance_insights_enabled" {
  type        = bool
  description = "Specifies whether Performance Insights are enabled"
  default     = false
}

variable "performance_insights_kms_key_id" {
  type        = string
  description = "The ARN for the KMS key to encrypt Performance Insights data"
  default     = null
}

variable "performance_insights_retention_period" {
  type        = number
  description = "The amount of time in days to retain Performance Insights data. Valid values are `7`, `731` (2 years) or a multiple of `31`"
  default     = 7
}

variable "port" {
  type        = string
  description = "The port on which the DB accepts connections"
  default     = null
}

variable "publicly_accessible" {
  type        = bool
  description = "Bool to control if instance is publicly accessible"
  default     = false
}

variable "putin_khuylo" {
  type        = bool
  description = "Do you agree that Putin doesn't respect Ukrainian sovereignty and territorial integrity? More info: https://en.wikipedia.org/wiki/Putin_khuylo!"
  default     = true
}

variable "replica_mode" {
  type        = string
  description = "Specifies whether the replica is in either mounted or open-read-only mode. This attribute is only supported by Oracle instances. Oracle replicas operate in open-read-only mode unless otherwise specified"
  default     = null
}

variable "replicate_source_db" {
  type        = string
  description = "Specifies that this resource is a Replicate database, and to use this value as the source database. This correlates to the identifier of another Amazon RDS Database to replicate"
  default     = null
}

variable "restore_to_point_in_time" {
  type        = map(string)
  description = "Restore to a point in time (MySQL is NOT supported)"
  default     = null
}

variable "s3_import" {
  type        = map(string)
  description = "Restore from a Percona Xtrabackup in S3 (only MySQL is supported)"
  default     = null
}

variable "skip_final_snapshot" {
  type        = bool
  description = "Determines whether a final DB snapshot is created before the DB instance is deleted. If true is specified, no DBSnapshot is created. If false is specified, a DB snapshot is created before the DB instance is deleted"
  default     = false
}

variable "snapshot_identifier" {
  type        = string
  description = "Specifies whether or not to create this database from a snapshot. This correlates to the snapshot ID you'd find in the RDS console, e.g: rds:production-2015-06-26-06-05"
  default     = null
}

variable "storage_encrypted" {
  type        = bool
  description = "Specifies whether the DB instance is encrypted"
  default     = true
}

variable "storage_throughput" {
  type        = number
  description = "Storage throughput value for the DB instance. See `notes` for limitations regarding this variable for `gp3`"
  default     = null
}

variable "storage_type" {
  type        = string
  description = "One of 'standard' (magnetic), 'gp2' (general purpose SSD), 'gp3' (new generation of general purpose SSD), or 'io1' (provisioned IOPS SSD). The default is 'io1' if iops is specified, 'gp2' if not. If you specify 'io1' or 'gp3' , you must also include a value for the 'iops' parameter"
  default     = null
}

variable "subnet_ids" {
  type        = list(string)
  description = "A list of VPC subnet IDs"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to all resources"
  default     = {}
}

variable "timeouts" {
  type        = map(string)
  description = "Updated Terraform resource management timeouts. Applies to `aws_db_instance` in particular to permit resource management times"
  default     = {}
}

variable "timezone" {
  type        = string
  description = "Time zone of the DB instance. timezone is currently only supported by Microsoft SQL Server. The timezone can only be set on creation. See MSSQL User Guide for more information"
  default     = null
}

variable "username" {
  type        = string
  description = "Username for the master DB user"
  default     = null
}

variable "vpc_security_group_ids" {
  type        = list(string)
  description = "List of VPC security groups to associate"
  default     = []
}


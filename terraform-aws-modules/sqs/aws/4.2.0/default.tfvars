
content_based_deduplication = null

create = true

create_dlq = false

create_dlq_queue_policy = false

create_dlq_redrive_allow_policy = true

create_queue_policy = false

deduplication_scope = null

delay_seconds = null

dlq_content_based_deduplication = null

dlq_deduplication_scope = null

dlq_delay_seconds = null

dlq_kms_data_key_reuse_period_seconds = null

dlq_kms_master_key_id = null

dlq_message_retention_seconds = null

dlq_name = null

dlq_queue_policy_statements = {}

dlq_receive_wait_time_seconds = null

dlq_redrive_allow_policy = {}

dlq_sqs_managed_sse_enabled = true

dlq_tags = {}

dlq_visibility_timeout_seconds = null

fifo_queue = false

fifo_throughput_limit = null

kms_data_key_reuse_period_seconds = null

kms_master_key_id = null

max_message_size = null

message_retention_seconds = null

name = null

override_dlq_queue_policy_documents = []

override_queue_policy_documents = []

queue_policy_statements = {}

receive_wait_time_seconds = null

redrive_allow_policy = {}

redrive_policy = {}

source_dlq_queue_policy_documents = []

source_queue_policy_documents = []

sqs_managed_sse_enabled = true

tags = {}

use_name_prefix = false

visibility_timeout_seconds = null



variable "api_destinations" {
  type        = map(any)
  description = "A map of objects with EventBridge Destination definitions."
  default     = {}
}

variable "append_connection_postfix" {
  type        = bool
  description = "Controls whether to append '-connection' to the name of the connection"
  default     = true
}

variable "append_destination_postfix" {
  type        = bool
  description = "Controls whether to append '-destination' to the name of the destination"
  default     = true
}

variable "append_pipe_postfix" {
  type        = bool
  description = "Controls whether to append '-pipe' to the name of the pipe"
  default     = true
}

variable "append_rule_postfix" {
  type        = bool
  description = "Controls whether to append '-rule' to the name of the rule"
  default     = true
}

variable "append_schedule_group_postfix" {
  type        = bool
  description = "Controls whether to append '-group' to the name of the schedule group"
  default     = true
}

variable "append_schedule_postfix" {
  type        = bool
  description = "Controls whether to append '-schedule' to the name of the schedule"
  default     = true
}

variable "archives" {
  type        = map(any)
  description = "A map of objects with the EventBridge Archive definitions."
  default     = {}
}

variable "attach_api_destination_policy" {
  type        = bool
  description = "Controls whether the API Destination policy should be added to IAM role for EventBridge Target"
  default     = false
}

variable "attach_cloudwatch_policy" {
  type        = bool
  description = "Controls whether the Cloudwatch policy should be added to IAM role for EventBridge Target"
  default     = false
}

variable "attach_ecs_policy" {
  type        = bool
  description = "Controls whether the ECS policy should be added to IAM role for EventBridge Target"
  default     = false
}

variable "attach_kinesis_firehose_policy" {
  type        = bool
  description = "Controls whether the Kinesis Firehose policy should be added to IAM role for EventBridge Target"
  default     = false
}

variable "attach_kinesis_policy" {
  type        = bool
  description = "Controls whether the Kinesis policy should be added to IAM role for EventBridge Target"
  default     = false
}

variable "attach_lambda_policy" {
  type        = bool
  description = "Controls whether the Lambda Function policy should be added to IAM role for EventBridge Target"
  default     = false
}

variable "attach_policies" {
  type        = bool
  description = "Controls whether list of policies should be added to IAM role"
  default     = false
}

variable "attach_policy" {
  type        = bool
  description = "Controls whether policy should be added to IAM role"
  default     = false
}

variable "attach_policy_json" {
  type        = bool
  description = "Controls whether policy_json should be added to IAM role"
  default     = false
}

variable "attach_policy_jsons" {
  type        = bool
  description = "Controls whether policy_jsons should be added to IAM role"
  default     = false
}

variable "attach_policy_statements" {
  type        = bool
  description = "Controls whether policy_statements should be added to IAM role"
  default     = false
}

variable "attach_sfn_policy" {
  type        = bool
  description = "Controls whether the StepFunction policy should be added to IAM role for EventBridge Target"
  default     = false
}

variable "attach_sns_policy" {
  type        = bool
  description = "Controls whether the SNS policy should be added to IAM role for EventBridge Target"
  default     = false
}

variable "attach_sqs_policy" {
  type        = bool
  description = "Controls whether the SQS policy should be added to IAM role for EventBridge Target"
  default     = false
}

variable "attach_tracing_policy" {
  type        = bool
  description = "Controls whether X-Ray tracing policy should be added to IAM role for EventBridge"
  default     = false
}

variable "bus_name" {
  type        = string
  description = "A unique name for your EventBridge Bus"
  default     = "default"
}

variable "cloudwatch_target_arns" {
  type        = list(string)
  description = "The Amazon Resource Name (ARN) of the Cloudwatch Log Streams you want to use as EventBridge targets"
  default     = []
}

variable "connections" {
  type        = any
  description = "A map of objects with EventBridge Connection definitions."
  default     = {}
}

variable "create" {
  type        = bool
  description = "Controls whether resources should be created"
  default     = true
}

variable "create_api_destinations" {
  type        = bool
  description = "Controls whether EventBridge Destination resources should be created"
  default     = false
}

variable "create_archives" {
  type        = bool
  description = "Controls whether EventBridge Archive resources should be created"
  default     = false
}

variable "create_bus" {
  type        = bool
  description = "Controls whether EventBridge Bus resource should be created"
  default     = true
}

variable "create_connections" {
  type        = bool
  description = "Controls whether EventBridge Connection resources should be created"
  default     = false
}

variable "create_permissions" {
  type        = bool
  description = "Controls whether EventBridge Permission resources should be created"
  default     = true
}

variable "create_pipes" {
  type        = bool
  description = "Controls whether EventBridge Pipes resources should be created"
  default     = true
}

variable "create_role" {
  type        = bool
  description = "Controls whether IAM roles should be created"
  default     = true
}

variable "create_rules" {
  type        = bool
  description = "Controls whether EventBridge Rule resources should be created"
  default     = true
}

variable "create_schedule_groups" {
  type        = bool
  description = "Controls whether EventBridge Schedule Group resources should be created"
  default     = true
}

variable "create_schedules" {
  type        = bool
  description = "Controls whether EventBridge Schedule resources should be created"
  default     = true
}

variable "create_schemas_discoverer" {
  type        = bool
  description = "Controls whether default schemas discoverer should be created"
  default     = false
}

variable "create_targets" {
  type        = bool
  description = "Controls whether EventBridge Target resources should be created"
  default     = true
}

variable "ecs_target_arns" {
  type        = list(string)
  description = "The Amazon Resource Name (ARN) of the AWS ECS Tasks you want to use as EventBridge targets"
  default     = []
}

variable "event_source_name" {
  type        = string
  description = "The partner event source that the new event bus will be matched with. Must match name."
  default     = null
}

variable "kinesis_firehose_target_arns" {
  type        = list(string)
  description = "The Amazon Resource Name (ARN) of the Kinesis Firehose Delivery Streams you want to use as EventBridge targets"
  default     = []
}

variable "kinesis_target_arns" {
  type        = list(string)
  description = "The Amazon Resource Name (ARN) of the Kinesis Streams you want to use as EventBridge targets"
  default     = []
}

variable "lambda_target_arns" {
  type        = list(string)
  description = "The Amazon Resource Name (ARN) of the Lambda Functions you want to use as EventBridge targets"
  default     = []
}

variable "number_of_policies" {
  type        = number
  description = "Number of policies to attach to IAM role"
  default     = 0
}

variable "number_of_policy_jsons" {
  type        = number
  description = "Number of policies JSON to attach to IAM role"
  default     = 0
}

variable "permissions" {
  type        = map(any)
  description = "A map of objects with EventBridge Permission definitions."
  default     = {}
}

variable "pipes" {
  type        = any
  description = "A map of objects with EventBridge Pipe definitions."
  default     = {}
}

variable "policies" {
  type        = list(string)
  description = "List of policy statements ARN to attach to IAM role"
  default     = []
}

variable "policy" {
  type        = string
  description = "An additional policy document ARN to attach to IAM role"
  default     = null
}

variable "policy_json" {
  type        = string
  description = "An additional policy document as JSON to attach to IAM role"
  default     = null
}

variable "policy_jsons" {
  type        = list(string)
  description = "List of additional policy documents as JSON to attach to IAM role"
  default     = []
}

variable "policy_statements" {
  type        = any
  description = "Map of dynamic policy statements to attach to IAM role"
  default     = {}
}

variable "role_description" {
  type        = string
  description = "Description of IAM role to use for EventBridge"
  default     = null
}

variable "role_force_detach_policies" {
  type        = bool
  description = "Specifies to force detaching any policies the IAM role has before destroying it."
  default     = true
}

variable "role_name" {
  type        = string
  description = "Name of IAM role to use for EventBridge"
  default     = null
}

variable "role_path" {
  type        = string
  description = "Path of IAM role to use for EventBridge"
  default     = null
}

variable "role_permissions_boundary" {
  type        = string
  description = "The ARN of the policy that is used to set the permissions boundary for the IAM role used by EventBridge"
  default     = null
}

variable "role_tags" {
  type        = map(string)
  description = "A map of tags to assign to IAM role"
  default     = {}
}

variable "rules" {
  type        = map(any)
  description = "A map of objects with EventBridge Rule definitions."
  default     = {}
}

variable "schedule_group_timeouts" {
  type        = map(string)
  description = "A map of objects with EventBridge Schedule Group create and delete timeouts."
  default     = {}
}

variable "schedule_groups" {
  type        = any
  description = "A map of objects with EventBridge Schedule Group definitions."
  default     = {}
}

variable "schedules" {
  type        = map(any)
  description = "A map of objects with EventBridge Schedule definitions."
  default     = {}
}

variable "schemas_discoverer_description" {
  type        = string
  description = "Default schemas discoverer description"
  default     = "Auto schemas discoverer event"
}

variable "sfn_target_arns" {
  type        = list(string)
  description = "The Amazon Resource Name (ARN) of the StepFunctions you want to use as EventBridge targets"
  default     = []
}

variable "sns_target_arns" {
  type        = list(string)
  description = "The Amazon Resource Name (ARN) of the AWS SNS's you want to use as EventBridge targets"
  default     = []
}

variable "sqs_target_arns" {
  type        = list(string)
  description = "The Amazon Resource Name (ARN) of the AWS SQS Queues you want to use as EventBridge targets"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to assign to resources."
  default     = {}
}

variable "targets" {
  type        = any
  description = "A map of objects with EventBridge Target definitions."
  default     = {}
}

variable "trusted_entities" {
  type        = list(string)
  description = "Additional trusted entities for assuming roles (trust relationship)"
  default     = []
}


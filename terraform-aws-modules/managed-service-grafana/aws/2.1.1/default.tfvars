
account_access_type = "CURRENT_ACCOUNT"

associate_license = true

authentication_providers = [
  "AWS_SSO"
]

configuration = null

create = true

create_iam_role = true

create_saml_configuration = true

create_security_group = true

create_workspace = true

data_sources = []

description = null

enable_alerts = false

grafana_version = null

iam_role_arn = null

iam_role_description = null

iam_role_force_detach_policies = true

iam_role_max_session_duration = null

iam_role_name = null

iam_role_path = null

iam_role_permissions_boundary = null

iam_role_policy_arns = []

iam_role_tags = {}

license_type = "ENTERPRISE"

name = null

network_access_control = {}

notification_destinations = []

organization_role_name = null

organizational_units = []

permission_type = "SERVICE_MANAGED"

role_associations = {}

saml_admin_role_values = []

saml_allowed_organizations = []

saml_editor_role_values = []

saml_email_assertion = null

saml_groups_assertion = null

saml_idp_metadata_url = null

saml_idp_metadata_xml = null

saml_login_assertion = null

saml_login_validity_duration = null

saml_name_assertion = null

saml_org_assertion = null

saml_role_assertion = null

security_group_description = null

security_group_name = null

security_group_rules = {}

security_group_tags = {}

security_group_use_name_prefix = true

stack_set_name = null

tags = {}

use_iam_role_name_prefix = true

vpc_configuration = {}

workspace_api_keys = {}

workspace_id = ""


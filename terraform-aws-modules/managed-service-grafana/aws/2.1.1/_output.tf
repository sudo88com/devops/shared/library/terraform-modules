
output "license_expiration" {
  description = "If `license_type` is set to `ENTERPRISE`, this is the expiration date of the enterprise license"
  value       = module.managed-service-grafana.license_expiration
}

output "license_free_trial_expiration" {
  description = "If `license_type` is set to `ENTERPRISE_FREE_TRIAL`, this is the expiration date of the free trial"
  value       = module.managed-service-grafana.license_free_trial_expiration
}

output "saml_configuration_status" {
  description = "Status of the SAML configuration"
  value       = module.managed-service-grafana.saml_configuration_status
}

output "security_group_arn" {
  description = "Amazon Resource Name (ARN) of the security group"
  value       = module.managed-service-grafana.security_group_arn
}

output "security_group_id" {
  description = "ID of the security group"
  value       = module.managed-service-grafana.security_group_id
}

output "workspace_api_keys" {
  description = "The workspace API keys created including their attributes"
  value       = module.managed-service-grafana.workspace_api_keys
}

output "workspace_arn" {
  description = "The Amazon Resource Name (ARN) of the Grafana workspace"
  value       = module.managed-service-grafana.workspace_arn
}

output "workspace_endpoint" {
  description = "The endpoint of the Grafana workspace"
  value       = module.managed-service-grafana.workspace_endpoint
}

output "workspace_grafana_version" {
  description = "The version of Grafana running on the workspace"
  value       = module.managed-service-grafana.workspace_grafana_version
}

output "workspace_iam_role_arn" {
  description = "IAM role ARN of the Grafana workspace"
  value       = module.managed-service-grafana.workspace_iam_role_arn
}

output "workspace_iam_role_name" {
  description = "IAM role name of the Grafana workspace"
  value       = module.managed-service-grafana.workspace_iam_role_name
}

output "workspace_iam_role_policy_arn" {
  description = "IAM Policy ARN of the Grafana workspace IAM role"
  value       = module.managed-service-grafana.workspace_iam_role_policy_arn
}

output "workspace_iam_role_policy_id" {
  description = "Stable and unique string identifying the IAM Policy"
  value       = module.managed-service-grafana.workspace_iam_role_policy_id
}

output "workspace_iam_role_policy_name" {
  description = "IAM Policy name of the Grafana workspace IAM role"
  value       = module.managed-service-grafana.workspace_iam_role_policy_name
}

output "workspace_iam_role_unique_id" {
  description = "Stable and unique string identifying the IAM role"
  value       = module.managed-service-grafana.workspace_iam_role_unique_id
}

output "workspace_id" {
  description = "The ID of the Grafana workspace"
  value       = module.managed-service-grafana.workspace_id
}


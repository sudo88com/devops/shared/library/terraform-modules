
variable "alert_manager_definition" {
  type        = string
  description = "The alert manager definition that you want to be applied. See more in the [AWS Docs](https://docs.aws.amazon.com/prometheus/latest/userguide/AMP-alert-manager.html)"
  default     = "alertmanager_config: |\n  route:\n    receiver: 'default'\n  receivers:\n    - name: 'default'\n"
}

variable "create" {
  type        = bool
  description = "Determines whether a resources will be created"
  default     = true
}

variable "create_workspace" {
  type        = bool
  description = "Determines whether a workspace will be created or to use an existing workspace"
  default     = true
}

variable "logging_configuration" {
  type        = map(string)
  description = "The logging configuration of the prometheus workspace."
  default     = {}
}

variable "rule_group_namespaces" {
  type        = map(any)
  description = "A map of one or more rule group namespace definitions"
  default     = {}
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "workspace_alias" {
  type        = string
  description = "The alias of the prometheus workspace. See more in the [AWS Docs](https://docs.aws.amazon.com/prometheus/latest/userguide/AMP-onboard-create-workspace.html)"
  default     = null
}

variable "workspace_id" {
  type        = string
  description = "The ID of an existing workspace to use when `create_workspace` is `false`"
  default     = ""
}



variable "allowed_triggers" {
  type        = map(any)
  description = "Map of allowed triggers to create Lambda permissions"
  default     = {}
}

variable "architectures" {
  type        = list(string)
  description = "Instruction set architecture for your Lambda function. Valid values are [\"x86_64\"] and [\"arm64\"]."
  default     = null
}

variable "artifacts_dir" {
  type        = string
  description = "Directory name where artifacts should be stored"
  default     = "builds"
}

variable "assume_role_policy_statements" {
  type        = any
  description = "Map of dynamic policy statements for assuming Lambda Function role (trust relationship)"
  default     = {}
}

variable "attach_async_event_policy" {
  type        = bool
  description = "Controls whether async event policy should be added to IAM role for Lambda Function"
  default     = false
}

variable "attach_cloudwatch_logs_policy" {
  type        = bool
  description = "Controls whether CloudWatch Logs policy should be added to IAM role for Lambda Function"
  default     = true
}

variable "attach_create_log_group_permission" {
  type        = bool
  description = "Controls whether to add the create log group permission to the CloudWatch logs policy"
  default     = true
}

variable "attach_dead_letter_policy" {
  type        = bool
  description = "Controls whether SNS/SQS dead letter notification policy should be added to IAM role for Lambda Function"
  default     = false
}

variable "attach_network_policy" {
  type        = bool
  description = "Controls whether VPC/network policy should be added to IAM role for Lambda Function"
  default     = false
}

variable "attach_policies" {
  type        = bool
  description = "Controls whether list of policies should be added to IAM role for Lambda Function"
  default     = false
}

variable "attach_policy" {
  type        = bool
  description = "Controls whether policy should be added to IAM role for Lambda Function"
  default     = false
}

variable "attach_policy_json" {
  type        = bool
  description = "Controls whether policy_json should be added to IAM role for Lambda Function"
  default     = false
}

variable "attach_policy_jsons" {
  type        = bool
  description = "Controls whether policy_jsons should be added to IAM role for Lambda Function"
  default     = false
}

variable "attach_policy_statements" {
  type        = bool
  description = "Controls whether policy_statements should be added to IAM role for Lambda Function"
  default     = false
}

variable "attach_tracing_policy" {
  type        = bool
  description = "Controls whether X-Ray tracing policy should be added to IAM role for Lambda Function"
  default     = false
}

variable "authorization_type" {
  type        = string
  description = "The type of authentication that the Lambda Function URL uses. Set to 'AWS_IAM' to restrict access to authenticated IAM users only. Set to 'NONE' to bypass IAM authentication and create a public endpoint."
  default     = "NONE"
}

variable "build_in_docker" {
  type        = bool
  description = "Whether to build dependencies in Docker"
  default     = false
}

variable "cloudwatch_logs_kms_key_id" {
  type        = string
  description = "The ARN of the KMS Key to use when encrypting log data."
  default     = null
}

variable "cloudwatch_logs_log_group_class" {
  type        = string
  description = "Specified the log class of the log group. Possible values are: `STANDARD` or `INFREQUENT_ACCESS`"
  default     = null
}

variable "cloudwatch_logs_retention_in_days" {
  type        = number
  description = "Specifies the number of days you want to retain log events in the specified log group. Possible values are: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653."
  default     = null
}

variable "cloudwatch_logs_skip_destroy" {
  type        = bool
  description = "Whether to keep the log group (and any logs it may contain) at destroy time."
  default     = false
}

variable "cloudwatch_logs_tags" {
  type        = map(string)
  description = "A map of tags to assign to the resource."
  default     = {}
}

variable "code_signing_config_arn" {
  type        = string
  description = "Amazon Resource Name (ARN) for a Code Signing Configuration"
  default     = null
}

variable "compatible_architectures" {
  type        = list(string)
  description = "A list of Architectures Lambda layer is compatible with. Currently x86_64 and arm64 can be specified."
  default     = null
}

variable "compatible_runtimes" {
  type        = list(string)
  description = "A list of Runtimes this layer is compatible with. Up to 5 runtimes can be specified."
  default     = []
}

variable "cors" {
  type        = any
  description = "CORS settings to be used by the Lambda Function URL"
  default     = {}
}

variable "create" {
  type        = bool
  description = "Controls whether resources should be created"
  default     = true
}

variable "create_async_event_config" {
  type        = bool
  description = "Controls whether async event configuration for Lambda Function/Alias should be created"
  default     = false
}

variable "create_current_version_allowed_triggers" {
  type        = bool
  description = "Whether to allow triggers on current version of Lambda Function (this will revoke permissions from previous version because Terraform manages only current resources)"
  default     = true
}

variable "create_current_version_async_event_config" {
  type        = bool
  description = "Whether to allow async event configuration on current version of Lambda Function (this will revoke permissions from previous version because Terraform manages only current resources)"
  default     = true
}

variable "create_function" {
  type        = bool
  description = "Controls whether Lambda Function resource should be created"
  default     = true
}

variable "create_lambda_function_url" {
  type        = bool
  description = "Controls whether the Lambda Function URL resource should be created"
  default     = false
}

variable "create_layer" {
  type        = bool
  description = "Controls whether Lambda Layer resource should be created"
  default     = false
}

variable "create_package" {
  type        = bool
  description = "Controls whether Lambda package should be created"
  default     = true
}

variable "create_role" {
  type        = bool
  description = "Controls whether IAM role for Lambda Function should be created"
  default     = true
}

variable "create_sam_metadata" {
  type        = bool
  description = "Controls whether the SAM metadata null resource should be created"
  default     = false
}

variable "create_unqualified_alias_allowed_triggers" {
  type        = bool
  description = "Whether to allow triggers on unqualified alias pointing to $LATEST version"
  default     = true
}

variable "create_unqualified_alias_async_event_config" {
  type        = bool
  description = "Whether to allow async event configuration on unqualified alias pointing to $LATEST version"
  default     = true
}

variable "create_unqualified_alias_lambda_function_url" {
  type        = bool
  description = "Whether to use unqualified alias pointing to $LATEST version in Lambda Function URL"
  default     = true
}

variable "dead_letter_target_arn" {
  type        = string
  description = "The ARN of an SNS topic or SQS queue to notify when an invocation fails."
  default     = null
}

variable "description" {
  type        = string
  description = "Description of your Lambda Function (or Layer)"
  default     = ""
}

variable "destination_on_failure" {
  type        = string
  description = "Amazon Resource Name (ARN) of the destination resource for failed asynchronous invocations"
  default     = null
}

variable "destination_on_success" {
  type        = string
  description = "Amazon Resource Name (ARN) of the destination resource for successful asynchronous invocations"
  default     = null
}

variable "docker_additional_options" {
  type        = list(string)
  description = "Additional options to pass to the docker run command (e.g. to set environment variables, volumes, etc.)"
  default     = []
}

variable "docker_build_root" {
  type        = string
  description = "Root dir where to build in Docker"
  default     = ""
}

variable "docker_entrypoint" {
  type        = string
  description = "Path to the Docker entrypoint to use"
  default     = null
}

variable "docker_file" {
  type        = string
  description = "Path to a Dockerfile when building in Docker"
  default     = ""
}

variable "docker_image" {
  type        = string
  description = "Docker image to use for the build"
  default     = ""
}

variable "docker_pip_cache" {
  type        = any
  description = "Whether to mount a shared pip cache folder into docker environment or not"
  default     = null
}

variable "docker_with_ssh_agent" {
  type        = bool
  description = "Whether to pass SSH_AUTH_SOCK into docker environment or not"
  default     = false
}

variable "environment_variables" {
  type        = map(string)
  description = "A map that defines environment variables for the Lambda Function."
  default     = {}
}

variable "ephemeral_storage_size" {
  type        = number
  description = "Amount of ephemeral storage (/tmp) in MB your Lambda Function can use at runtime. Valid value between 512 MB to 10,240 MB (10 GB)."
  default     = 512
}

variable "event_source_mapping" {
  type        = any
  description = "Map of event source mapping"
  default     = {}
}

variable "file_system_arn" {
  type        = string
  description = "The Amazon Resource Name (ARN) of the Amazon EFS Access Point that provides access to the file system."
  default     = null
}

variable "file_system_local_mount_path" {
  type        = string
  description = "The path where the function can access the file system, starting with /mnt/."
  default     = null
}

variable "function_name" {
  type        = string
  description = "A unique name for your Lambda Function"
  default     = ""
}

variable "function_tags" {
  type        = map(string)
  description = "A map of tags to assign only to the lambda function"
  default     = {}
}

variable "handler" {
  type        = string
  description = "Lambda Function entrypoint in your code"
  default     = ""
}

variable "hash_extra" {
  type        = string
  description = "The string to add into hashing function. Useful when building same source path for different functions."
  default     = ""
}

variable "ignore_source_code_hash" {
  type        = bool
  description = "Whether to ignore changes to the function's source code hash. Set to true if you manage infrastructure and code deployments separately."
  default     = false
}

variable "image_config_command" {
  type        = list(string)
  description = "The CMD for the docker image"
  default     = []
}

variable "image_config_entry_point" {
  type        = list(string)
  description = "The ENTRYPOINT for the docker image"
  default     = []
}

variable "image_config_working_directory" {
  type        = string
  description = "The working directory for the docker image"
  default     = null
}

variable "image_uri" {
  type        = string
  description = "The ECR image URI containing the function's deployment package."
  default     = null
}

variable "invoke_mode" {
  type        = string
  description = "Invoke mode of the Lambda Function URL. Valid values are BUFFERED (default) and RESPONSE_STREAM."
  default     = null
}

variable "kms_key_arn" {
  type        = string
  description = "The ARN of KMS key to use by your Lambda Function"
  default     = null
}

variable "lambda_at_edge" {
  type        = bool
  description = "Set this to true if using Lambda@Edge, to enable publishing, limit the timeout, and allow edgelambda.amazonaws.com to invoke the function"
  default     = false
}

variable "lambda_at_edge_logs_all_regions" {
  type        = bool
  description = "Whether to specify a wildcard in IAM policy used by Lambda@Edge to allow logging in all regions"
  default     = true
}

variable "lambda_role" {
  type        = string
  description = " IAM role ARN attached to the Lambda Function. This governs both who / what can invoke your Lambda Function, as well as what resources our Lambda Function has access to. See Lambda Permission Model for more details."
  default     = ""
}

variable "layer_name" {
  type        = string
  description = "Name of Lambda Layer to create"
  default     = ""
}

variable "layer_skip_destroy" {
  type        = bool
  description = "Whether to retain the old version of a previously deployed Lambda Layer."
  default     = false
}

variable "layers" {
  type        = list(string)
  description = "List of Lambda Layer Version ARNs (maximum of 5) to attach to your Lambda Function."
  default     = null
}

variable "license_info" {
  type        = string
  description = "License info for your Lambda Layer. Eg, MIT or full url of a license."
  default     = ""
}

variable "local_existing_package" {
  type        = string
  description = "The absolute path to an existing zip-file to use"
  default     = null
}

variable "logging_application_log_level" {
  type        = string
  description = "The application log level of the Lambda Function. Valid values are \"TRACE\", \"DEBUG\", \"INFO\", \"WARN\", \"ERROR\", or \"FATAL\"."
  default     = "INFO"
}

variable "logging_log_format" {
  type        = string
  description = "The log format of the Lambda Function. Valid values are \"JSON\" or \"Text\"."
  default     = "Text"
}

variable "logging_log_group" {
  type        = string
  description = "The CloudWatch log group to send logs to."
  default     = null
}

variable "logging_system_log_level" {
  type        = string
  description = "The system log level of the Lambda Function. Valid values are \"DEBUG\", \"INFO\", or \"WARN\"."
  default     = "INFO"
}

variable "maximum_event_age_in_seconds" {
  type        = number
  description = "Maximum age of a request that Lambda sends to a function for processing in seconds. Valid values between 60 and 21600."
  default     = null
}

variable "maximum_retry_attempts" {
  type        = number
  description = "Maximum number of times to retry when the function returns an error. Valid values between 0 and 2. Defaults to 2."
  default     = null
}

variable "memory_size" {
  type        = number
  description = "Amount of memory in MB your Lambda Function can use at runtime. Valid value between 128 MB to 10,240 MB (10 GB), in 64 MB increments."
  default     = 128
}

variable "number_of_policies" {
  type        = number
  description = "Number of policies to attach to IAM role for Lambda Function"
  default     = 0
}

variable "number_of_policy_jsons" {
  type        = number
  description = "Number of policies JSON to attach to IAM role for Lambda Function"
  default     = 0
}

variable "package_type" {
  type        = string
  description = "The Lambda deployment package type. Valid options: Zip or Image"
  default     = "Zip"
}

variable "policies" {
  type        = list(string)
  description = "List of policy statements ARN to attach to Lambda Function role"
  default     = []
}

variable "policy" {
  type        = string
  description = "An additional policy document ARN to attach to the Lambda Function role"
  default     = null
}

variable "policy_json" {
  type        = string
  description = "An additional policy document as JSON to attach to the Lambda Function role"
  default     = null
}

variable "policy_jsons" {
  type        = list(string)
  description = "List of additional policy documents as JSON to attach to Lambda Function role"
  default     = []
}

variable "policy_name" {
  type        = string
  description = "IAM policy name. It override the default value, which is the same as role_name"
  default     = null
}

variable "policy_path" {
  type        = string
  description = "Path of policies to that should be added to IAM role for Lambda Function"
  default     = null
}

variable "policy_statements" {
  type        = any
  description = "Map of dynamic policy statements to attach to Lambda Function role"
  default     = {}
}

variable "provisioned_concurrent_executions" {
  type        = number
  description = "Amount of capacity to allocate. Set to 1 or greater to enable, or set to 0 to disable provisioned concurrency."
  default     = -1
}

variable "publish" {
  type        = bool
  description = "Whether to publish creation/change as new Lambda Function Version."
  default     = false
}

variable "putin_khuylo" {
  type        = bool
  description = "Do you agree that Putin doesn't respect Ukrainian sovereignty and territorial integrity? More info: https://en.wikipedia.org/wiki/Putin_khuylo!"
  default     = true
}

variable "recreate_missing_package" {
  type        = bool
  description = "Whether to recreate missing Lambda package if it is missing locally or not"
  default     = true
}

variable "replace_security_groups_on_destroy" {
  type        = bool
  description = "(Optional) When true, all security groups defined in vpc_security_group_ids will be replaced with the default security group after the function is destroyed. Set the replacement_security_group_ids variable to use a custom list of security groups for replacement instead."
  default     = null
}

variable "replacement_security_group_ids" {
  type        = list(string)
  description = "(Optional) List of security group IDs to assign to orphaned Lambda function network interfaces upon destruction. replace_security_groups_on_destroy must be set to true to use this attribute."
  default     = null
}

variable "reserved_concurrent_executions" {
  type        = number
  description = "The amount of reserved concurrent executions for this Lambda Function. A value of 0 disables Lambda Function from being triggered and -1 removes any concurrency limitations. Defaults to Unreserved Concurrency Limits -1."
  default     = -1
}

variable "role_description" {
  type        = string
  description = "Description of IAM role to use for Lambda Function"
  default     = null
}

variable "role_force_detach_policies" {
  type        = bool
  description = "Specifies to force detaching any policies the IAM role has before destroying it."
  default     = true
}

variable "role_maximum_session_duration" {
  type        = number
  description = "Maximum session duration, in seconds, for the IAM role"
  default     = 3600
}

variable "role_name" {
  type        = string
  description = "Name of IAM role to use for Lambda Function"
  default     = null
}

variable "role_path" {
  type        = string
  description = "Path of IAM role to use for Lambda Function"
  default     = null
}

variable "role_permissions_boundary" {
  type        = string
  description = "The ARN of the policy that is used to set the permissions boundary for the IAM role used by Lambda Function"
  default     = null
}

variable "role_tags" {
  type        = map(string)
  description = "A map of tags to assign to IAM role"
  default     = {}
}

variable "runtime" {
  type        = string
  description = "Lambda Function runtime"
  default     = ""
}

variable "s3_acl" {
  type        = string
  description = "The canned ACL to apply. Valid values are private, public-read, public-read-write, aws-exec-read, authenticated-read, bucket-owner-read, and bucket-owner-full-control. Defaults to private."
  default     = "private"
}

variable "s3_bucket" {
  type        = string
  description = "S3 bucket to store artifacts"
  default     = null
}

variable "s3_existing_package" {
  type        = map(string)
  description = "The S3 bucket object with keys bucket, key, version pointing to an existing zip-file to use"
  default     = null
}

variable "s3_kms_key_id" {
  type        = string
  description = "Specifies a custom KMS key to use for S3 object encryption."
  default     = null
}

variable "s3_object_override_default_tags" {
  type        = bool
  description = "Whether to override the default_tags from provider? NB: S3 objects support a maximum of 10 tags."
  default     = false
}

variable "s3_object_storage_class" {
  type        = string
  description = "Specifies the desired Storage Class for the artifact uploaded to S3. Can be either STANDARD, REDUCED_REDUNDANCY, ONEZONE_IA, INTELLIGENT_TIERING, or STANDARD_IA."
  default     = "ONEZONE_IA"
}

variable "s3_object_tags" {
  type        = map(string)
  description = "A map of tags to assign to S3 bucket object."
  default     = {}
}

variable "s3_object_tags_only" {
  type        = bool
  description = "Set to true to not merge tags with s3_object_tags. Useful to avoid breaching S3 Object 10 tag limit."
  default     = false
}

variable "s3_prefix" {
  type        = string
  description = "Directory name where artifacts should be stored in the S3 bucket. If unset, the path from `artifacts_dir` is used"
  default     = null
}

variable "s3_server_side_encryption" {
  type        = string
  description = "Specifies server-side encryption of the object in S3. Valid values are \"AES256\" and \"aws:kms\"."
  default     = null
}

variable "snap_start" {
  type        = bool
  description = "(Optional) Snap start settings for low-latency startups"
  default     = false
}

variable "source_path" {
  type        = any
  description = "The absolute path to a local file or directory containing your Lambda source code"
  default     = null
}

variable "store_on_s3" {
  type        = bool
  description = "Whether to store produced artifacts on S3 or locally."
  default     = false
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to assign to resources."
  default     = {}
}

variable "timeout" {
  type        = number
  description = "The amount of time your Lambda Function has to run in seconds."
  default     = 3
}

variable "timeouts" {
  type        = map(string)
  description = "Define maximum timeout for creating, updating, and deleting Lambda Function resources"
  default     = {}
}

variable "tracing_mode" {
  type        = string
  description = "Tracing mode of the Lambda Function. Valid value can be either PassThrough or Active."
  default     = null
}

variable "trigger_on_package_timestamp" {
  type        = bool
  description = "Whether to recreate the Lambda package if the timestamp changes"
  default     = true
}

variable "trusted_entities" {
  type        = any
  description = "List of additional trusted entities for assuming Lambda Function role (trust relationship)"
  default     = []
}

variable "use_existing_cloudwatch_log_group" {
  type        = bool
  description = "Whether to use an existing CloudWatch log group or create new"
  default     = false
}

variable "vpc_security_group_ids" {
  type        = list(string)
  description = "List of security group ids when Lambda Function should run in the VPC."
  default     = null
}

variable "vpc_subnet_ids" {
  type        = list(string)
  description = "List of subnet ids when Lambda Function should run in the VPC. Usually private or intra subnets."
  default     = null
}


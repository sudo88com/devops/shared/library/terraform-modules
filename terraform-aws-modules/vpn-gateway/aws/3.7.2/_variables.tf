
variable "connect_to_transit_gateway" {
  type        = bool
  description = "Set to false to disable attachment of the VPN connection route to the VPN connection (TGW uses another resource for that)"
  default     = false
}

variable "create_vpn_connection" {
  type        = bool
  description = "Set to false to prevent the creation of a VPN Connection."
  default     = true
}

variable "create_vpn_gateway_attachment" {
  type        = bool
  description = "Set to false to prevent attachment of the VGW to the VPC"
  default     = true
}

variable "customer_gateway_id" {
  type        = string
  description = "The id of the Customer Gateway."
  default     = ""
}

variable "local_ipv4_network_cidr" {
  type        = string
  description = "(Optional) The IPv4 CIDR on the customer gateway (on-premises) side of the VPN connection."
  default     = null
}

variable "local_ipv6_network_cidr" {
  type        = string
  description = "(Optional) The IPv6 CIDR on the customer gateway (on-premises) side of the VPN connection."
  default     = null
}

variable "remote_ipv4_network_cidr" {
  type        = string
  description = "(Optional) The IPv4 CIDR on the AWS side of the VPN connection."
  default     = null
}

variable "remote_ipv6_network_cidr" {
  type        = string
  description = "(Optional) The IPv6 CIDR on AWS side of the VPN connection."
  default     = null
}

variable "tags" {
  type        = map(string)
  description = "Set of tags to be added to the VPN Connection resource (only if `create_vpn_connection = true`)."
  default     = {}
}

variable "transit_gateway_id" {
  type        = string
  description = "The ID of the Transit Gateway."
  default     = null
}

variable "tunnel1_dpd_timeout_action" {
  type        = string
  description = "(Optional, Default clear) The action to take after DPD timeout occurs for the first VPN tunnel. Specify restart to restart the IKE initiation. Specify clear to end the IKE session. Valid values are clear | none | restart"
  default     = null
}

variable "tunnel1_dpd_timeout_seconds" {
  type        = number
  description = "(Optional, Default 30) The number of seconds after which a DPD timeout occurs for the first VPN tunnel. Valid value is equal or higher than 30"
  default     = null
}

variable "tunnel1_enable_tunnel_lifecycle_control" {
  type        = bool
  description = "(Optional) Turn on or off tunnel endpoint lifecycle control feature for the first VPN tunnel. Valid values are true | false"
  default     = null
}

variable "tunnel1_ike_versions" {
  type        = list(string)
  description = "(Optional) The IKE versions that are permitted for the first VPN tunnel. Valid values are ikev1 | ikev2"
  default     = null
}

variable "tunnel1_inside_cidr" {
  type        = string
  description = "The CIDR block of the inside IP addresses for the first VPN tunnel."
  default     = ""
}

variable "tunnel1_log_options" {
  type        = any
  description = "(Optional) Options for sending VPN tunnel logs to CloudWatch."
  default     = {}
}

variable "tunnel1_phase1_dh_group_numbers" {
  type        = list(number)
  description = "(Optional) List of one or more Diffie-Hellman group numbers that are permitted for the first VPN tunnel for phase 1 IKE negotiations. Valid values are 2 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24"
  default     = null
}

variable "tunnel1_phase1_encryption_algorithms" {
  type        = list(string)
  description = "(Optional) List of one or more encryption algorithms that are permitted for the first VPN tunnel for phase 1 IKE negotiations. Valid values are AES128 | AES256 | AES128-GCM-16 | AES256-GCM-16"
  default     = null
}

variable "tunnel1_phase1_integrity_algorithms" {
  type        = list(string)
  description = "(Optional) One or more integrity algorithms that are permitted for the first VPN tunnel for phase 1 IKE negotiations. Valid values are SHA1 | SHA2-256 | SHA2-384 | SHA2-512"
  default     = null
}

variable "tunnel1_phase1_lifetime_seconds" {
  type        = number
  description = "(Optional, Default 28800) The lifetime for phase 1 of the IKE negotiation for the first VPN tunnel, in seconds. Valid value is between 900 and 28800"
  default     = null
}

variable "tunnel1_phase2_dh_group_numbers" {
  type        = list(number)
  description = "(Optional) List of one or more Diffie-Hellman group numbers that are permitted for the first VPN tunnel for phase 2 IKE negotiations. Valid values are 2 | 5 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24"
  default     = null
}

variable "tunnel1_phase2_encryption_algorithms" {
  type        = list(string)
  description = "(Optional) List of one or more encryption algorithms that are permitted for the first VPN tunnel for phase 2 IKE negotiations. Valid values are AES128 | AES256 | AES128-GCM-16 | AES256-GCM-16"
  default     = null
}

variable "tunnel1_phase2_integrity_algorithms" {
  type        = list(string)
  description = "(Optional) List of one or more integrity algorithms that are permitted for the first VPN tunnel for phase 2 IKE negotiations. Valid values are SHA1 | SHA2-256 | SHA2-384 | SHA2-512"
  default     = null
}

variable "tunnel1_phase2_lifetime_seconds" {
  type        = number
  description = "(Optional, Default 3600) The lifetime for phase 2 of the IKE negotiation for the first VPN tunnel, in seconds. Valid value is between 900 and 3600"
  default     = null
}

variable "tunnel1_preshared_key" {
  type        = string
  description = "The preshared key of the first VPN tunnel."
  default     = ""
}

variable "tunnel1_rekey_fuzz_percentage" {
  type        = number
  description = "(Optional, Default 100) The percentage of the rekey window for the first VPN tunnel (determined by tunnel1_rekey_margin_time_seconds) during which the rekey time is randomly selected. Valid value is between 0 and 100"
  default     = null
}

variable "tunnel1_rekey_margin_time_seconds" {
  type        = number
  description = "(Optional, Default 540) The margin time, in seconds, before the phase 2 lifetime expires, during which the AWS side of the first VPN connection performs an IKE rekey. The exact time of the rekey is randomly selected based on the value for tunnel1_rekey_fuzz_percentage. Valid value is between 60 and half of tunnel1_phase2_lifetime_seconds"
  default     = null
}

variable "tunnel1_replay_window_size" {
  type        = number
  description = "(Optional, Default 1024) The number of packets in an IKE replay window for the first VPN tunnel. Valid value is between 64 and 2048."
  default     = null
}

variable "tunnel1_startup_action" {
  type        = string
  description = "(Optional, Default add) The action to take when the establishing the tunnel for the first VPN connection. By default, your customer gateway device must initiate the IKE negotiation and bring up the tunnel. Specify start for AWS to initiate the IKE negotiation. Valid values are add | start"
  default     = null
}

variable "tunnel2_dpd_timeout_action" {
  type        = string
  description = "(Optional, Default clear) The action to take after DPD timeout occurs for the second VPN tunnel. Specify restart to restart the IKE initiation. Specify clear to end the IKE session. Valid values are clear | none | restart"
  default     = null
}

variable "tunnel2_dpd_timeout_seconds" {
  type        = number
  description = "(Optional, Default 30) The number of seconds after which a DPD timeout occurs for the second VPN tunnel. Valid value is equal or higher than 30"
  default     = null
}

variable "tunnel2_enable_tunnel_lifecycle_control" {
  type        = bool
  description = "(Optional) Turn on or off tunnel endpoint lifecycle control feature for the second VPN tunnel. Valid values are true | false"
  default     = null
}

variable "tunnel2_ike_versions" {
  type        = list(string)
  description = "(Optional) The IKE versions that are permitted for the second VPN tunnel. Valid values are ikev1 | ikev2"
  default     = null
}

variable "tunnel2_inside_cidr" {
  type        = string
  description = "The CIDR block of the inside IP addresses for the second VPN tunnel."
  default     = ""
}

variable "tunnel2_log_options" {
  type        = any
  description = "(Optional) Options for sending VPN tunnel logs to CloudWatch."
  default     = {}
}

variable "tunnel2_phase1_dh_group_numbers" {
  type        = list(number)
  description = "(Optional) List of one or more Diffie-Hellman group numbers that are permitted for the second VPN tunnel for phase 1 IKE negotiations. Valid values are 2 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24"
  default     = null
}

variable "tunnel2_phase1_encryption_algorithms" {
  type        = list(string)
  description = "(Optional) List of one or more encryption algorithms that are permitted for the second VPN tunnel for phase 1 IKE negotiations. Valid values are AES128 | AES256 | AES128-GCM-16 | AES256-GCM-16"
  default     = null
}

variable "tunnel2_phase1_integrity_algorithms" {
  type        = list(string)
  description = "(Optional) One or more integrity algorithms that are permitted for the second VPN tunnel for phase 1 IKE negotiations. Valid values are SHA1 | SHA2-256 | SHA2-384 | SHA2-512"
  default     = null
}

variable "tunnel2_phase1_lifetime_seconds" {
  type        = number
  description = "(Optional, Default 28800) The lifetime for phase 1 of the IKE negotiation for the second VPN tunnel, in seconds. Valid value is between 900 and 28800"
  default     = null
}

variable "tunnel2_phase2_dh_group_numbers" {
  type        = list(number)
  description = "(Optional) List of one or more Diffie-Hellman group numbers that are permitted for the second VPN tunnel for phase 2 IKE negotiations. Valid values are 2 | 5 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24"
  default     = null
}

variable "tunnel2_phase2_encryption_algorithms" {
  type        = list(string)
  description = "(Optional) List of one or more encryption algorithms that are permitted for the second VPN tunnel for phase 2 IKE negotiations. Valid values are AES128 | AES256 | AES128-GCM-16 | AES256-GCM-16"
  default     = null
}

variable "tunnel2_phase2_integrity_algorithms" {
  type        = list(string)
  description = "(Optional) List of one or more integrity algorithms that are permitted for the second VPN tunnel for phase 2 IKE negotiations. Valid values are SHA1 | SHA2-256 | SHA2-384 | SHA2-512"
  default     = null
}

variable "tunnel2_phase2_lifetime_seconds" {
  type        = number
  description = "(Optional, Default 3600) The lifetime for phase 2 of the IKE negotiation for the second VPN tunnel, in seconds. Valid value is between 900 and 3600"
  default     = null
}

variable "tunnel2_preshared_key" {
  type        = string
  description = "The preshared key of the second VPN tunnel."
  default     = ""
}

variable "tunnel2_rekey_fuzz_percentage" {
  type        = number
  description = "(Optional, Default 100) The percentage of the rekey window for the second VPN tunnel (determined by tunnel1_rekey_margin_time_seconds) during which the rekey time is randomly selected. Valid value is between 0 and 100"
  default     = null
}

variable "tunnel2_rekey_margin_time_seconds" {
  type        = number
  description = "(Optional, Default 540) The margin time, in seconds, before the phase 2 lifetime expires, during which the AWS side of the second VPN connection performs an IKE rekey. The exact time of the rekey is randomly selected based on the value for tunnel2_rekey_fuzz_percentage. Valid value is between 60 and half of tunnel2_phase2_lifetime_seconds"
  default     = null
}

variable "tunnel2_replay_window_size" {
  type        = number
  description = "(Optional, Default 1024) The number of packets in an IKE replay window for the second VPN tunnel. Valid value is between 64 and 2048."
  default     = null
}

variable "tunnel2_startup_action" {
  type        = string
  description = "(Optional, Default add) The action to take when the establishing the tunnel for the second VPN connection. By default, your customer gateway device must initiate the IKE negotiation and bring up the tunnel. Specify start for AWS to initiate the IKE negotiation. Valid values are add | start"
  default     = null
}

variable "tunnel_inside_ip_version" {
  type        = string
  description = "(Optional) Indicate whether the VPN tunnels process IPv4 or IPv6 traffic. Valid values are ipv4 | ipv6. ipv6 Supports only EC2 Transit Gateway."
  default     = "ipv4"
}

variable "vpc_id" {
  type        = string
  description = "The id of the VPC where the VPN Gateway lives."
  default     = null
}

variable "vpc_subnet_route_table_count" {
  type        = number
  description = "The number of subnet route table ids being passed in via `vpc_subnet_route_table_ids`."
  default     = 0
}

variable "vpc_subnet_route_table_ids" {
  type        = list(string)
  description = "The ids of the VPC subnets for which routes from the VPN Gateway will be propagated."
  default     = []
}

variable "vpn_connection_static_routes_destinations" {
  type        = list(string)
  description = "List of CIDRs to be used as destination for static routes (used with `vpn_connection_static_routes_only = true`). Routes to destinations set here will be propagated to the routing tables of the subnets defined in `vpc_subnet_route_table_ids`."
  default     = []
}

variable "vpn_connection_static_routes_only" {
  type        = bool
  description = "Set to true for the created VPN connection to use static routes exclusively (only if `create_vpn_connection = true`). Static routes must be used for devices that don't support BGP."
  default     = false
}

variable "vpn_gateway_id" {
  type        = string
  description = "The id of the VPN Gateway."
  default     = null
}



connect_to_transit_gateway = false

create_vpn_connection = true

create_vpn_gateway_attachment = true

customer_gateway_id = 

local_ipv4_network_cidr = null

local_ipv6_network_cidr = null

remote_ipv4_network_cidr = null

remote_ipv6_network_cidr = null

tags = {}

transit_gateway_id = null

tunnel1_dpd_timeout_action = null

tunnel1_dpd_timeout_seconds = null

tunnel1_enable_tunnel_lifecycle_control = null

tunnel1_ike_versions = null

tunnel1_inside_cidr = ""

tunnel1_log_options = {}

tunnel1_phase1_dh_group_numbers = null

tunnel1_phase1_encryption_algorithms = null

tunnel1_phase1_integrity_algorithms = null

tunnel1_phase1_lifetime_seconds = null

tunnel1_phase2_dh_group_numbers = null

tunnel1_phase2_encryption_algorithms = null

tunnel1_phase2_integrity_algorithms = null

tunnel1_phase2_lifetime_seconds = null

tunnel1_preshared_key = ""

tunnel1_rekey_fuzz_percentage = null

tunnel1_rekey_margin_time_seconds = null

tunnel1_replay_window_size = null

tunnel1_startup_action = null

tunnel2_dpd_timeout_action = null

tunnel2_dpd_timeout_seconds = null

tunnel2_enable_tunnel_lifecycle_control = null

tunnel2_ike_versions = null

tunnel2_inside_cidr = ""

tunnel2_log_options = {}

tunnel2_phase1_dh_group_numbers = null

tunnel2_phase1_encryption_algorithms = null

tunnel2_phase1_integrity_algorithms = null

tunnel2_phase1_lifetime_seconds = null

tunnel2_phase2_dh_group_numbers = null

tunnel2_phase2_encryption_algorithms = null

tunnel2_phase2_integrity_algorithms = null

tunnel2_phase2_lifetime_seconds = null

tunnel2_preshared_key = ""

tunnel2_rekey_fuzz_percentage = null

tunnel2_rekey_margin_time_seconds = null

tunnel2_replay_window_size = null

tunnel2_startup_action = null

tunnel_inside_ip_version = "ipv4"

vpc_id = null

vpc_subnet_route_table_count = 0

vpc_subnet_route_table_ids = []

vpn_connection_static_routes_destinations = []

vpn_connection_static_routes_only = false

vpn_gateway_id = null


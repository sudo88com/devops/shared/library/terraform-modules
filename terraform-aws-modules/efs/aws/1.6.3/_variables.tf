
variable "access_points" {
  type        = any
  description = "A map of access point definitions to create"
  default     = {}
}

variable "attach_policy" {
  type        = bool
  description = "Determines whether a policy is attached to the file system"
  default     = true
}

variable "availability_zone_name" {
  type        = string
  description = "The AWS Availability Zone in which to create the file system. Used to create a file system that uses One Zone storage classes"
  default     = null
}

variable "bypass_policy_lockout_safety_check" {
  type        = bool
  description = "A flag to indicate whether to bypass the `aws_efs_file_system_policy` lockout safety check. Defaults to `false`"
  default     = null
}

variable "create" {
  type        = bool
  description = "Determines whether resources will be created (affects all resources)"
  default     = true
}

variable "create_backup_policy" {
  type        = bool
  description = "Determines whether a backup policy is created"
  default     = true
}

variable "create_replication_configuration" {
  type        = bool
  description = "Determines whether a replication configuration is created"
  default     = false
}

variable "create_security_group" {
  type        = bool
  description = "Determines whether a security group is created"
  default     = true
}

variable "creation_token" {
  type        = string
  description = "A unique name (a maximum of 64 characters are allowed) used as reference when creating the Elastic File System to ensure idempotent file system creation. By default generated by Terraform"
  default     = null
}

variable "deny_nonsecure_transport" {
  type        = bool
  description = "Determines whether `aws:SecureTransport` is required when connecting to elastic file system"
  default     = true
}

variable "enable_backup_policy" {
  type        = bool
  description = "Determines whether a backup policy is `ENABLED` or `DISABLED`"
  default     = true
}

variable "encrypted" {
  type        = bool
  description = "If `true`, the disk will be encrypted"
  default     = true
}

variable "kms_key_arn" {
  type        = string
  description = "The ARN for the KMS encryption key. When specifying `kms_key_arn`, encrypted needs to be set to `true`"
  default     = null
}

variable "lifecycle_policy" {
  type        = any
  description = "A file system [lifecycle policy](https://docs.aws.amazon.com/efs/latest/ug/API_LifecyclePolicy.html) object"
  default     = {}
}

variable "mount_targets" {
  type        = any
  description = "A map of mount target definitions to create"
  default     = {}
}

variable "name" {
  type        = string
  description = "The name of the file system"
  default     = ""
}

variable "override_policy_documents" {
  type        = list(string)
  description = "List of IAM policy documents that are merged together into the exported document. In merging, statements with non-blank `sid`s will override statements with the same `sid`"
  default     = []
}

variable "performance_mode" {
  type        = string
  description = "The file system performance mode. Can be either `generalPurpose` or `maxIO`. Default is `generalPurpose`"
  default     = null
}

variable "policy_statements" {
  type        = any
  description = "A list of IAM policy [statements](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document#statement) for custom permission usage"
  default     = []
}

variable "provisioned_throughput_in_mibps" {
  type        = number
  description = "The throughput, measured in MiB/s, that you want to provision for the file system. Only applicable with `throughput_mode` set to `provisioned`"
  default     = null
}

variable "replication_configuration_destination" {
  type        = any
  description = "A destination configuration block"
  default     = {}
}

variable "security_group_description" {
  type        = string
  description = "Security group description. Defaults to Managed by Terraform"
  default     = null
}

variable "security_group_name" {
  type        = string
  description = "Name to assign to the security group. If omitted, Terraform will assign a random, unique name"
  default     = null
}

variable "security_group_rules" {
  type        = any
  description = "Map of security group rule definitions to create"
  default     = {}
}

variable "security_group_use_name_prefix" {
  type        = bool
  description = "Determines whether to use a name prefix for the security group. If `true`, the `security_group_name` value will be used as a prefix"
  default     = false
}

variable "security_group_vpc_id" {
  type        = string
  description = "The VPC ID where the security group will be created"
  default     = null
}

variable "source_policy_documents" {
  type        = list(string)
  description = "List of IAM policy documents that are merged together into the exported document. Statements must have unique `sid`s"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "throughput_mode" {
  type        = string
  description = "Throughput mode for the file system. Defaults to `bursting`. Valid values: `bursting`, `elastic`, and `provisioned`. When using `provisioned`, also set `provisioned_throughput_in_mibps`"
  default     = null
}


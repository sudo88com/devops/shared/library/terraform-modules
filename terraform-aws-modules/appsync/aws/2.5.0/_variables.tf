
variable "additional_authentication_provider" {
  type        = any
  description = "One or more additional authentication providers for the GraphqlApi."
  default     = {}
}

variable "api_keys" {
  type        = map(string)
  description = "Map of API keys to create"
  default     = {}
}

variable "authentication_type" {
  type        = string
  description = "The authentication type to use by GraphQL API"
  default     = "API_KEY"
}

variable "cache_at_rest_encryption_enabled" {
  type        = bool
  description = "At-rest encryption flag for cache."
  default     = false
}

variable "cache_transit_encryption_enabled" {
  type        = bool
  description = "Transit encryption flag when connecting to cache."
  default     = false
}

variable "cache_ttl" {
  type        = number
  description = "TTL in seconds for cache entries"
  default     = 1
}

variable "cache_type" {
  type        = string
  description = "The cache instance type."
  default     = "SMALL"
}

variable "caching_behavior" {
  type        = string
  description = "Caching behavior."
  default     = "FULL_REQUEST_CACHING"
}

variable "caching_enabled" {
  type        = bool
  description = "Whether caching with Elasticache is enabled."
  default     = false
}

variable "certificate_arn" {
  type        = string
  description = "The Amazon Resource Name (ARN) of the certificate."
  default     = ""
}

variable "create_graphql_api" {
  type        = bool
  description = "Whether to create GraphQL API"
  default     = true
}

variable "create_logs_role" {
  type        = bool
  description = "Whether to create service role for Cloudwatch logs"
  default     = true
}

variable "datasources" {
  type        = any
  description = "Map of datasources to create"
  default     = {}
}

variable "direct_lambda_request_template" {
  type        = string
  description = "VTL request template for the direct lambda integrations"
  default     = "{\n  \"version\" : \"2017-02-28\",\n  \"operation\": \"Invoke\",\n  \"payload\": {\n    \"arguments\": $util.toJson($ctx.arguments),\n    \"identity\": $util.toJson($ctx.identity),\n    \"source\": $util.toJson($ctx.source),\n    \"request\": $util.toJson($ctx.request),\n    \"prev\": $util.toJson($ctx.prev),\n    \"info\": {\n        \"selectionSetList\": $util.toJson($ctx.info.selectionSetList),\n        \"selectionSetGraphQL\": $util.toJson($ctx.info.selectionSetGraphQL),\n        \"parentTypeName\": $util.toJson($ctx.info.parentTypeName),\n        \"fieldName\": $util.toJson($ctx.info.fieldName),\n        \"variables\": $util.toJson($ctx.info.variables)\n    },\n    \"stash\": $util.toJson($ctx.stash)\n  }\n}\n"
}

variable "direct_lambda_response_template" {
  type        = string
  description = "VTL response template for the direct lambda integrations"
  default     = "$util.toJson($ctx.result)\n"
}

variable "domain_name" {
  type        = string
  description = "The domain name that AppSync gets associated with."
  default     = ""
}

variable "domain_name_association_enabled" {
  type        = bool
  description = "Whether to enable domain name association on GraphQL API"
  default     = false
}

variable "domain_name_description" {
  type        = string
  description = "A description of the Domain Name."
  default     = null
}

variable "dynamodb_allowed_actions" {
  type        = list(string)
  description = "List of allowed IAM actions for datasources type AMAZON_DYNAMODB"
  default     = [
  "dynamodb:GetItem",
  "dynamodb:PutItem",
  "dynamodb:DeleteItem",
  "dynamodb:UpdateItem",
  "dynamodb:Query",
  "dynamodb:Scan",
  "dynamodb:BatchGetItem",
  "dynamodb:BatchWriteItem"
]
}

variable "elasticsearch_allowed_actions" {
  type        = list(string)
  description = "List of allowed IAM actions for datasources type AMAZON_ELASTICSEARCH"
  default     = [
  "es:ESHttpDelete",
  "es:ESHttpHead",
  "es:ESHttpGet",
  "es:ESHttpPost",
  "es:ESHttpPut"
]
}

variable "eventbridge_allowed_actions" {
  type        = list(string)
  description = "List of allowed IAM actions for datasources type AMAZON_EVENTBRIDGE"
  default     = [
  "events:PutEvents"
]
}

variable "functions" {
  type        = any
  description = "Map of functions to create"
  default     = {}
}

variable "graphql_api_tags" {
  type        = map(string)
  description = "Map of tags to add to GraphQL API"
  default     = {}
}

variable "iam_permissions_boundary" {
  type        = string
  description = "ARN for iam permissions boundary"
  default     = null
}

variable "introspection_config" {
  type        = string
  description = "Whether to enable or disable introspection of the GraphQL API."
  default     = null
}

variable "lambda_allowed_actions" {
  type        = list(string)
  description = "List of allowed IAM actions for datasources type AWS_LAMBDA"
  default     = [
  "lambda:invokeFunction"
]
}

variable "lambda_authorizer_config" {
  type        = map(string)
  description = "Nested argument containing Lambda authorizer configuration."
  default     = {}
}

variable "log_cloudwatch_logs_role_arn" {
  type        = string
  description = "Amazon Resource Name of the service role that AWS AppSync will assume to publish to Amazon CloudWatch logs in your account."
  default     = null
}

variable "log_exclude_verbose_content" {
  type        = bool
  description = "Set to TRUE to exclude sections that contain information such as headers, context, and evaluated mapping templates, regardless of logging level."
  default     = false
}

variable "log_field_log_level" {
  type        = string
  description = "Field logging level. Valid values: ALL, ERROR, NONE."
  default     = null
}

variable "logging_enabled" {
  type        = bool
  description = "Whether to enable Cloudwatch logging on GraphQL API"
  default     = false
}

variable "logs_role_name" {
  type        = string
  description = "Name of IAM role to create for Cloudwatch logs"
  default     = null
}

variable "logs_role_tags" {
  type        = map(string)
  description = "Map of tags to add to Cloudwatch logs IAM role"
  default     = {}
}

variable "name" {
  type        = string
  description = "Name of GraphQL API"
  default     = ""
}

variable "openid_connect_config" {
  type        = map(string)
  description = "Nested argument containing OpenID Connect configuration."
  default     = {}
}

variable "opensearchservice_allowed_actions" {
  type        = list(string)
  description = "List of allowed IAM actions for datasources type AMAZON_OPENSEARCH_SERVICE"
  default     = [
  "es:ESHttpDelete",
  "es:ESHttpHead",
  "es:ESHttpGet",
  "es:ESHttpPost",
  "es:ESHttpPut"
]
}

variable "query_depth_limit" {
  type        = number
  description = "The maximum depth a query can have in a single request."
  default     = null
}

variable "relational_database_allowed_actions" {
  type        = list(string)
  description = "List of allowed IAM actions for datasources type RELATIONAL_DATABASE"
  default     = [
  "rds-data:BatchExecuteStatement",
  "rds-data:BeginTransaction",
  "rds-data:CommitTransaction",
  "rds-data:ExecuteStatement",
  "rds-data:RollbackTransaction"
]
}

variable "resolver_caching_ttl" {
  type        = number
  description = "Default caching TTL for resolvers when caching is enabled"
  default     = 60
}

variable "resolver_count_limit" {
  type        = number
  description = "The maximum number of resolvers that can be invoked in a single request."
  default     = null
}

variable "resolvers" {
  type        = any
  description = "Map of resolvers to create"
  default     = {}
}

variable "schema" {
  type        = string
  description = "The schema definition, in GraphQL schema language format. Terraform cannot perform drift detection of this configuration."
  default     = ""
}

variable "secrets_manager_allowed_actions" {
  type        = list(string)
  description = "List of allowed IAM actions for secrets manager datasources type RELATIONAL_DATABASE"
  default     = [
  "secretsmanager:GetSecretValue"
]
}

variable "tags" {
  type        = map(string)
  description = "Map of tags to add to all GraphQL resources created by this module"
  default     = {}
}

variable "user_pool_config" {
  type        = map(string)
  description = "The Amazon Cognito User Pool configuration."
  default     = {}
}

variable "visibility" {
  type        = string
  description = "The API visibility. Valid values: GLOBAL, PRIVATE."
  default     = null
}

variable "xray_enabled" {
  type        = bool
  description = "Whether tracing with X-ray is enabled."
  default     = false
}



architectures = null

cloudwatch_log_group_kms_key_id = null

cloudwatch_log_group_retention_in_days = 0

cloudwatch_log_group_tags = {}

create = true

create_sns_topic = true

enable_sns_topic_delivery_status_logs = false

hash_extra = ""

iam_policy_path = null

iam_role_boundary_policy_arn = null

iam_role_name_prefix = "lambda"

iam_role_path = null

iam_role_tags = {}

kms_key_arn = ""

lambda_attach_dead_letter_policy = false

lambda_dead_letter_target_arn = null

lambda_description = null

lambda_function_ephemeral_storage_size = 512

lambda_function_name = "notify_slack"

lambda_function_s3_bucket = null

lambda_function_store_on_s3 = false

lambda_function_tags = {}

lambda_function_vpc_security_group_ids = null

lambda_function_vpc_subnet_ids = null

lambda_role = ""

lambda_source_path = null

log_events = false

putin_khuylo = true

recreate_missing_package = true

reserved_concurrent_executions = -1

slack_channel = 

slack_emoji = ":aws:"

slack_username = 

slack_webhook_url = 

sns_topic_feedback_role_description = null

sns_topic_feedback_role_force_detach_policies = true

sns_topic_feedback_role_name = null

sns_topic_feedback_role_path = null

sns_topic_feedback_role_permissions_boundary = null

sns_topic_feedback_role_tags = {}

sns_topic_kms_key_id = ""

sns_topic_lambda_feedback_role_arn = ""

sns_topic_lambda_feedback_sample_rate = 100

sns_topic_name = 

sns_topic_tags = {}

subscription_filter_policy = null

subscription_filter_policy_scope = null

tags = {}


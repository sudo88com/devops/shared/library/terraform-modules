
variable "architectures" {
  type        = list(string)
  description = "Instruction set architecture for your Lambda function. Valid values are [\"x86_64\"] and [\"arm64\"]."
  default     = null
}

variable "cloudwatch_log_group_kms_key_id" {
  type        = string
  description = "The ARN of the KMS Key to use when encrypting log data for Lambda"
  default     = null
}

variable "cloudwatch_log_group_retention_in_days" {
  type        = number
  description = "Specifies the number of days you want to retain log events in log group for Lambda."
  default     = 0
}

variable "cloudwatch_log_group_tags" {
  type        = map(string)
  description = "Additional tags for the Cloudwatch log group"
  default     = {}
}

variable "create" {
  type        = bool
  description = "Whether to create all resources"
  default     = true
}

variable "create_sns_topic" {
  type        = bool
  description = "Whether to create new SNS topic"
  default     = true
}

variable "enable_sns_topic_delivery_status_logs" {
  type        = bool
  description = "Whether to enable SNS topic delivery status logs"
  default     = false
}

variable "hash_extra" {
  type        = string
  description = "The string to add into hashing function. Useful when building same source path for different functions."
  default     = ""
}

variable "iam_policy_path" {
  type        = string
  description = "Path of policies to that should be added to IAM role for Lambda Function"
  default     = null
}

variable "iam_role_boundary_policy_arn" {
  type        = string
  description = "The ARN of the policy that is used to set the permissions boundary for the role"
  default     = null
}

variable "iam_role_name_prefix" {
  type        = string
  description = "A unique role name beginning with the specified prefix"
  default     = "lambda"
}

variable "iam_role_path" {
  type        = string
  description = "Path of IAM role to use for Lambda Function"
  default     = null
}

variable "iam_role_tags" {
  type        = map(string)
  description = "Additional tags for the IAM role"
  default     = {}
}

variable "kms_key_arn" {
  type        = string
  description = "ARN of the KMS key used for decrypting slack webhook url"
  default     = ""
}

variable "lambda_attach_dead_letter_policy" {
  type        = bool
  description = "Controls whether SNS/SQS dead letter notification policy should be added to IAM role for Lambda Function"
  default     = false
}

variable "lambda_dead_letter_target_arn" {
  type        = string
  description = "The ARN of an SNS topic or SQS queue to notify when an invocation fails."
  default     = null
}

variable "lambda_description" {
  type        = string
  description = "The description of the Lambda function"
  default     = null
}

variable "lambda_function_ephemeral_storage_size" {
  type        = number
  description = "Amount of ephemeral storage (/tmp) in MB your Lambda Function can use at runtime. Valid value between 512 MB to 10,240 MB (10 GB)."
  default     = 512
}

variable "lambda_function_name" {
  type        = string
  description = "The name of the Lambda function to create"
  default     = "notify_slack"
}

variable "lambda_function_s3_bucket" {
  type        = string
  description = "S3 bucket to store artifacts"
  default     = null
}

variable "lambda_function_store_on_s3" {
  type        = bool
  description = "Whether to store produced artifacts on S3 or locally."
  default     = false
}

variable "lambda_function_tags" {
  type        = map(string)
  description = "Additional tags for the Lambda function"
  default     = {}
}

variable "lambda_function_vpc_security_group_ids" {
  type        = list(string)
  description = "List of security group ids when Lambda Function should run in the VPC."
  default     = null
}

variable "lambda_function_vpc_subnet_ids" {
  type        = list(string)
  description = "List of subnet ids when Lambda Function should run in the VPC. Usually private or intra subnets."
  default     = null
}

variable "lambda_role" {
  type        = string
  description = "IAM role attached to the Lambda Function.  If this is set then a role will not be created for you."
  default     = ""
}

variable "lambda_source_path" {
  type        = string
  description = "The source path of the custom Lambda function"
  default     = null
}

variable "log_events" {
  type        = bool
  description = "Boolean flag to enabled/disable logging of incoming events"
  default     = false
}

variable "putin_khuylo" {
  type        = bool
  description = "Do you agree that Putin doesn't respect Ukrainian sovereignty and territorial integrity? More info: https://en.wikipedia.org/wiki/Putin_khuylo!"
  default     = true
}

variable "recreate_missing_package" {
  type        = bool
  description = "Whether to recreate missing Lambda package if it is missing locally or not"
  default     = true
}

variable "reserved_concurrent_executions" {
  type        = number
  description = "The amount of reserved concurrent executions for this lambda function. A value of 0 disables lambda from being triggered and -1 removes any concurrency limitations"
  default     = -1
}

variable "slack_channel" {
  type        = string
  description = "The name of the channel in Slack for notifications"
  default     = ""
}

variable "slack_emoji" {
  type        = string
  description = "A custom emoji that will appear on Slack messages"
  default     = ":aws:"
}

variable "slack_username" {
  type        = string
  description = "The username that will appear on Slack messages"
  default     = ""
}

variable "slack_webhook_url" {
  type        = string
  description = "The URL of Slack webhook"
  default     = ""
}

variable "sns_topic_feedback_role_description" {
  type        = string
  description = "Description of IAM role to use for SNS topic delivery status logging"
  default     = null
}

variable "sns_topic_feedback_role_force_detach_policies" {
  type        = bool
  description = "Specifies to force detaching any policies the IAM role has before destroying it."
  default     = true
}

variable "sns_topic_feedback_role_name" {
  type        = string
  description = "Name of the IAM role to use for SNS topic delivery status logging"
  default     = null
}

variable "sns_topic_feedback_role_path" {
  type        = string
  description = "Path of IAM role to use for SNS topic delivery status logging"
  default     = null
}

variable "sns_topic_feedback_role_permissions_boundary" {
  type        = string
  description = "The ARN of the policy that is used to set the permissions boundary for the IAM role used by SNS topic delivery status logging"
  default     = null
}

variable "sns_topic_feedback_role_tags" {
  type        = map(string)
  description = "A map of tags to assign to IAM the SNS topic feedback role"
  default     = {}
}

variable "sns_topic_kms_key_id" {
  type        = string
  description = "ARN of the KMS key used for enabling SSE on the topic"
  default     = ""
}

variable "sns_topic_lambda_feedback_role_arn" {
  type        = string
  description = "IAM role for SNS topic delivery status logs.  If this is set then a role will not be created for you."
  default     = ""
}

variable "sns_topic_lambda_feedback_sample_rate" {
  type        = number
  description = "The percentage of successful deliveries to log"
  default     = 100
}

variable "sns_topic_name" {
  type        = string
  description = "The name of the SNS topic to create"
  default     = ""
}

variable "sns_topic_tags" {
  type        = map(string)
  description = "Additional tags for the SNS topic"
  default     = {}
}

variable "subscription_filter_policy" {
  type        = string
  description = "(Optional) A valid filter policy that will be used in the subscription to filter messages seen by the target resource."
  default     = null
}

variable "subscription_filter_policy_scope" {
  type        = string
  description = "(Optional) A valid filter policy scope MessageAttributes|MessageBody"
  default     = null
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}


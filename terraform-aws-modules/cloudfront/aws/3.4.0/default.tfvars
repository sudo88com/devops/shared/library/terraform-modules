
aliases = null

comment = null

continuous_deployment_policy_id = null

create_distribution = true

create_monitoring_subscription = false

create_origin_access_control = false

create_origin_access_identity = false

custom_error_response = {}

default_cache_behavior = null

default_root_object = null

enabled = true

geo_restriction = {}

http_version = "http2"

is_ipv6_enabled = null

logging_config = {}

ordered_cache_behavior = []

origin = null

origin_access_control = {
  "s3": {
    "description": "",
    "origin_type": "s3",
    "signing_behavior": "always",
    "signing_protocol": "sigv4"
  }
}

origin_access_identities = {}

origin_group = {}

price_class = null

realtime_metrics_subscription_status = "Enabled"

retain_on_delete = false

staging = false

tags = null

viewer_certificate = {
  "cloudfront_default_certificate": true,
  "minimum_protocol_version": "TLSv1"
}

wait_for_deployment = true

web_acl_id = null


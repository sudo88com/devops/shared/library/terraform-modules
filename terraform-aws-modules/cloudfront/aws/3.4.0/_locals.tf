
locals {
  aliases = var.aliases
  comment = var.comment
  continuous_deployment_policy_id = var.continuous_deployment_policy_id
  create_distribution = var.create_distribution
  create_monitoring_subscription = var.create_monitoring_subscription
  create_origin_access_control = var.create_origin_access_control
  create_origin_access_identity = var.create_origin_access_identity
  custom_error_response = var.custom_error_response
  default_cache_behavior = var.default_cache_behavior
  default_root_object = var.default_root_object
  enabled = var.enabled
  geo_restriction = var.geo_restriction
  http_version = var.http_version
  is_ipv6_enabled = var.is_ipv6_enabled
  logging_config = var.logging_config
  ordered_cache_behavior = var.ordered_cache_behavior
  origin = var.origin
  origin_access_control = var.origin_access_control
  origin_access_identities = var.origin_access_identities
  origin_group = var.origin_group
  price_class = var.price_class
  realtime_metrics_subscription_status = var.realtime_metrics_subscription_status
  retain_on_delete = var.retain_on_delete
  staging = var.staging
  tags = var.tags
  viewer_certificate = var.viewer_certificate
  wait_for_deployment = var.wait_for_deployment
  web_acl_id = var.web_acl_id
}


variable "aliases" {
  type        = list(string)
  description = "Extra CNAMEs (alternate domain names), if any, for this distribution."
  default     = null
}

variable "comment" {
  type        = string
  description = "Any comments you want to include about the distribution."
  default     = null
}

variable "continuous_deployment_policy_id" {
  type        = string
  description = "Identifier of a continuous deployment policy. This argument should only be set on a production distribution."
  default     = null
}

variable "create_distribution" {
  type        = bool
  description = "Controls if CloudFront distribution should be created"
  default     = true
}

variable "create_monitoring_subscription" {
  type        = bool
  description = "If enabled, the resource for monitoring subscription will created."
  default     = false
}

variable "create_origin_access_control" {
  type        = bool
  description = "Controls if CloudFront origin access control should be created"
  default     = false
}

variable "create_origin_access_identity" {
  type        = bool
  description = "Controls if CloudFront origin access identity should be created"
  default     = false
}

variable "custom_error_response" {
  type        = any
  description = "One or more custom error response elements"
  default     = {}
}

variable "default_cache_behavior" {
  type        = any
  description = "The default cache behavior for this distribution"
  default     = null
}

variable "default_root_object" {
  type        = string
  description = "The object that you want CloudFront to return (for example, index.html) when an end user requests the root URL."
  default     = null
}

variable "enabled" {
  type        = bool
  description = "Whether the distribution is enabled to accept end user requests for content."
  default     = true
}

variable "geo_restriction" {
  type        = any
  description = "The restriction configuration for this distribution (geo_restrictions)"
  default     = {}
}

variable "http_version" {
  type        = string
  description = "The maximum HTTP version to support on the distribution. Allowed values are http1.1, http2, http2and3, and http3. The default is http2."
  default     = "http2"
}

variable "is_ipv6_enabled" {
  type        = bool
  description = "Whether the IPv6 is enabled for the distribution."
  default     = null
}

variable "logging_config" {
  type        = any
  description = "The logging configuration that controls how logs are written to your distribution (maximum one)."
  default     = {}
}

variable "ordered_cache_behavior" {
  type        = any
  description = "An ordered list of cache behaviors resource for this distribution. List from top to bottom in order of precedence. The topmost cache behavior will have precedence 0."
  default     = []
}

variable "origin" {
  type        = any
  description = "One or more origins for this distribution (multiples allowed)."
  default     = null
}

variable "origin_access_control" {
  type        = map(object({
    description      = string
    origin_type      = string
    signing_behavior = string
    signing_protocol = string
  }))
  description = "Map of CloudFront origin access control"
  default     = {
  "s3": {
    "description": "",
    "origin_type": "s3",
    "signing_behavior": "always",
    "signing_protocol": "sigv4"
  }
}
}

variable "origin_access_identities" {
  type        = map(string)
  description = "Map of CloudFront origin access identities (value as a comment)"
  default     = {}
}

variable "origin_group" {
  type        = any
  description = "One or more origin_group for this distribution (multiples allowed)."
  default     = {}
}

variable "price_class" {
  type        = string
  description = "The price class for this distribution. One of PriceClass_All, PriceClass_200, PriceClass_100"
  default     = null
}

variable "realtime_metrics_subscription_status" {
  type        = string
  description = "A flag that indicates whether additional CloudWatch metrics are enabled for a given CloudFront distribution. Valid values are `Enabled` and `Disabled`."
  default     = "Enabled"
}

variable "retain_on_delete" {
  type        = bool
  description = "Disables the distribution instead of deleting it when destroying the resource through Terraform. If this is set, the distribution needs to be deleted manually afterwards."
  default     = false
}

variable "staging" {
  type        = bool
  description = "Whether the distribution is a staging distribution."
  default     = false
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to assign to the resource."
  default     = null
}

variable "viewer_certificate" {
  type        = any
  description = "The SSL configuration for this distribution"
  default     = {
  "cloudfront_default_certificate": true,
  "minimum_protocol_version": "TLSv1"
}
}

variable "wait_for_deployment" {
  type        = bool
  description = "If enabled, the resource will wait for the distribution status to change from InProgress to Deployed. Setting this to false will skip the process."
  default     = true
}

variable "web_acl_id" {
  type        = string
  description = "If you're using AWS WAF to filter CloudFront requests, the Id of the AWS WAF web ACL that is associated with the distribution. The WAF Web ACL must exist in the WAF Global (CloudFront) region and the credentials configuring this argument must have waf:GetWebACL permissions assigned. If using WAFv2, provide the ARN of the web ACL."
  default     = null
}


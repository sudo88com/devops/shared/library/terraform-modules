
amazon_side_asn = null

create_tgw = true

create_tgw_routes = true

description = null

enable_auto_accept_shared_attachments = false

enable_default_route_table_association = true

enable_default_route_table_propagation = true

enable_dns_support = true

enable_multicast_support = false

enable_vpn_ecmp_support = true

name = ""

ram_allow_external_principals = false

ram_name = ""

ram_principals = []

ram_resource_share_arn = ""

ram_tags = {}

share_tgw = true

tags = {}

tgw_default_route_table_tags = {}

tgw_route_table_tags = {}

tgw_tags = {}

tgw_vpc_attachment_tags = {}

timeouts = {}

transit_gateway_cidr_blocks = []

transit_gateway_route_table_id = null

vpc_attachments = {}


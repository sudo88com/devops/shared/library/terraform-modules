
locals {
  ami_id = var.ami_id
  cluster_name = var.cluster_name
  cluster_tag_key = var.cluster_tag_key
  cluster_tag_value = var.cluster_tag_value
  instance_type = var.instance_type
  num_clients = var.num_clients
  num_servers = var.num_servers
  server_instance_type = var.server_instance_type
  ssh_key_name = var.ssh_key_name
  vpc_id = var.vpc_id
}

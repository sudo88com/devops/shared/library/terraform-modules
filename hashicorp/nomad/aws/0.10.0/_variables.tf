
variable "ami_id" {
  type        = string
  description = "The ID of the AMI to run in the cluster. This should be an AMI built from the Packer template under examples/nomad-consul-ami/nomad-consul.json. If no AMI is specified, the template will 'just work' by using the example public AMIs. WARNING! Do not use the example AMIs in a production setting!"
  default     = ""
}

variable "cluster_name" {
  type        = string
  description = "What to name the cluster and all of its associated resources"
  default     = "nomad-example"
}

variable "cluster_tag_key" {
  type        = string
  description = "The tag the EC2 Instances will look for to automatically discover each other and form a cluster."
  default     = "nomad-servers"
}

variable "cluster_tag_value" {
  type        = string
  description = "Add a tag with key var.cluster_tag_key and this value to each Instance in the ASG. This can be used to automatically find other Consul nodes and form a cluster."
  default     = "auto-join"
}

variable "instance_type" {
  type        = string
  description = "What kind of instance type to use for the nomad clients"
  default     = "t2.micro"
}

variable "num_clients" {
  type        = number
  description = "The number of client nodes to deploy. You can deploy as many as you need to run your jobs."
  default     = 6
}

variable "num_servers" {
  type        = number
  description = "The number of server nodes to deploy. We strongly recommend using 3 or 5."
  default     = 3
}

variable "server_instance_type" {
  type        = string
  description = "What kind of instance type to use for the nomad servers"
  default     = "t2.micro"
}

variable "ssh_key_name" {
  type        = string
  description = "The name of an EC2 Key Pair that can be used to SSH to the EC2 Instances in this cluster. Set to an empty string to not associate a Key Pair."
  default     = ""
}

variable "vpc_id" {
  type        = string
  description = "The ID of the VPC in which the nodes will be deployed.  Uses default VPC if not supplied."
  default     = ""
}


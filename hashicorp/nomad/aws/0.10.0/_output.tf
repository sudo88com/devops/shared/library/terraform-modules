
output "asg_name_clients" {
  description = ""
  value       = module.nomad.asg_name_clients
}

output "asg_name_servers" {
  description = ""
  value       = module.nomad.asg_name_servers
}

output "aws_region" {
  description = ""
  value       = module.nomad.aws_region
}

output "iam_role_arn_clients" {
  description = ""
  value       = module.nomad.iam_role_arn_clients
}

output "iam_role_arn_servers" {
  description = ""
  value       = module.nomad.iam_role_arn_servers
}

output "iam_role_id_clients" {
  description = ""
  value       = module.nomad.iam_role_id_clients
}

output "iam_role_id_servers" {
  description = ""
  value       = module.nomad.iam_role_id_servers
}

output "launch_config_name_clients" {
  description = ""
  value       = module.nomad.launch_config_name_clients
}

output "launch_config_name_servers" {
  description = ""
  value       = module.nomad.launch_config_name_servers
}

output "nomad_servers_cluster_tag_key" {
  description = ""
  value       = module.nomad.nomad_servers_cluster_tag_key
}

output "nomad_servers_cluster_tag_value" {
  description = ""
  value       = module.nomad.nomad_servers_cluster_tag_value
}

output "num_clients" {
  description = ""
  value       = module.nomad.num_clients
}

output "num_nomad_servers" {
  description = ""
  value       = module.nomad.num_nomad_servers
}

output "security_group_id_clients" {
  description = ""
  value       = module.nomad.security_group_id_clients
}

output "security_group_id_servers" {
  description = ""
  value       = module.nomad.security_group_id_servers
}


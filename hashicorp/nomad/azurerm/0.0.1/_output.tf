
output "load_balancer_ip_address_clients" {
  description = ""
  value       = module.nomad.load_balancer_ip_address_clients
}

output "load_balancer_ip_address_servers" {
  description = ""
  value       = module.nomad.load_balancer_ip_address_servers
}

output "num_servers" {
  description = ""
  value       = module.nomad.num_servers
}

output "scale_set_name_servers" {
  description = ""
  value       = module.nomad.scale_set_name_servers
}


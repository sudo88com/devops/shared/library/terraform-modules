
variable "address_space" {
  type        = string
  description = "The supernet for the resources that will be created"
  default     = "10.0.0.0/16"
}

variable "allowed_ssh_cidr_blocks" {
  type        = list
  description = "A list of CIDR-formatted IP address ranges from which the Azure Instances will allow SSH connections"
  default     = []
}

variable "client_id" {
  type        = string
  description = "The Azure client ID"
  default     = ""
}

variable "cluster_name" {
  type        = string
  description = "What to name the cluster and all of its associated resources"
  default     = "nomad-example"
}

variable "cluster_tag_key" {
  type        = string
  description = "The tag the Azure Instances will look for to automatically discover each other and form a cluster."
  default     = "nomad-servers"
}

variable "image_uri" {
  type        = string
  description = "The URI of the Azure Image to run in the cluster. This should be an Azure Image built from the Packer template under examples/nomad-consul-ami/nomad-consul.json."
  default     = ""
}

variable "instance_size" {
  type        = string
  description = "The instance size for the servers"
  default     = "Standard_A0"
}

variable "key_data" {
  type        = string
  description = "The SSH public key that will be added to SSH authorized_users on the consul instances"
  default     = ""
}

variable "location" {
  type        = string
  description = "The Azure region the consul cluster will be deployed in"
  default     = "East US"
}

variable "num_clients" {
  type        = string
  description = "The number of client nodes to deploy. You can deploy as many as you need to run your jobs."
  default     = 6
}

variable "num_servers" {
  type        = string
  description = "The number of server nodes to deploy. We strongly recommend using 3 or 5."
  default     = 3
}

variable "resource_group_name" {
  type        = string
  description = "The name of the Azure resource group consul will be deployed into. This RG should already exist"
  default     = ""
}

variable "secret_access_key" {
  type        = string
  description = "The Azure secret access key"
  default     = ""
}

variable "ssh_key_name" {
  type        = string
  description = "The name of an EC2 Key Pair that can be used to SSH to the Azure Instances in this cluster. Set to an empty string to not associate a Key Pair."
  default     = ""
}

variable "storage_account_name" {
  type        = string
  description = "The name of an Azure Storage Account. This SA should already exist"
  default     = ""
}

variable "subnet_address" {
  type        = string
  description = "The subnet that consul resources will be deployed into"
  default     = "10.0.10.0/24"
}

variable "subscription_id" {
  type        = string
  description = "The Azure subscription ID"
  default     = ""
}

variable "tenant_id" {
  type        = string
  description = "The Azure tenant ID"
  default     = ""
}



locals {
  address_space = var.address_space
  allowed_ssh_cidr_blocks = var.allowed_ssh_cidr_blocks
  client_id = var.client_id
  cluster_name = var.cluster_name
  cluster_tag_key = var.cluster_tag_key
  image_uri = var.image_uri
  instance_size = var.instance_size
  key_data = var.key_data
  location = var.location
  num_clients = var.num_clients
  num_servers = var.num_servers
  resource_group_name = var.resource_group_name
  secret_access_key = var.secret_access_key
  ssh_key_name = var.ssh_key_name
  storage_account_name = var.storage_account_name
  subnet_address = var.subnet_address
  subscription_id = var.subscription_id
  tenant_id = var.tenant_id
}

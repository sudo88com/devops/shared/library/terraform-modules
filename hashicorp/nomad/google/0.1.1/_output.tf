
output "gcp_project" {
  description = ""
  value       = module.nomad.gcp_project
}

output "gcp_region" {
  description = ""
  value       = module.nomad.gcp_region
}

output "nomad_client_cluster_size" {
  description = ""
  value       = module.nomad.nomad_client_cluster_size
}

output "nomad_client_cluster_tag_name" {
  description = ""
  value       = module.nomad.nomad_client_cluster_tag_name
}

output "nomad_client_instance_group_id" {
  description = ""
  value       = module.nomad.nomad_client_instance_group_id
}

output "nomad_client_instance_group_name" {
  description = ""
  value       = module.nomad.nomad_client_instance_group_name
}

output "nomad_client_instance_group_url" {
  description = ""
  value       = module.nomad.nomad_client_instance_group_url
}

output "nomad_server_cluster_size" {
  description = ""
  value       = module.nomad.nomad_server_cluster_size
}

output "nomad_server_cluster_tag_name" {
  description = ""
  value       = module.nomad.nomad_server_cluster_tag_name
}

output "nomad_server_instance_group_name" {
  description = ""
  value       = module.nomad.nomad_server_instance_group_name
}

output "nomad_server_instance_group_url" {
  description = ""
  value       = module.nomad.nomad_server_instance_group_url
}


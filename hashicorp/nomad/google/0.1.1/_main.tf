
module "nomad" {
  source = "terraform-aws-modules/nomad/aws"
  version = "0.1.1"
  gcp_project = var.gcp_project
  gcp_region = var.gcp_region
  gcp_zone = var.gcp_zone
  nomad_client_cluster_name = var.nomad_client_cluster_name
  nomad_client_cluster_size = var.nomad_client_cluster_size
  nomad_client_machine_type = var.nomad_client_machine_type
  nomad_client_source_image = var.nomad_client_source_image
  nomad_consul_server_cluster_machine_type = var.nomad_consul_server_cluster_machine_type
  nomad_consul_server_cluster_name = var.nomad_consul_server_cluster_name
  nomad_consul_server_cluster_size = var.nomad_consul_server_cluster_size
  nomad_consul_server_source_image = var.nomad_consul_server_source_image
}

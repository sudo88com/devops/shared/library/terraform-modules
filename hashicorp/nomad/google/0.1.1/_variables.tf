
variable "gcp_project" {
  type        = string
  description = "The name of the GCP Project where all resources will be launched."
  default     = ""
}

variable "gcp_region" {
  type        = string
  description = "The region in which all GCP resources will be launched."
  default     = ""
}

variable "gcp_zone" {
  type        = string
  description = "The region in which all GCP resources will be launched."
  default     = ""
}

variable "nomad_client_cluster_name" {
  type        = string
  description = "The name of the Nomad client cluster. All resources will be namespaced by this value. E.g. consul-server-prod"
  default     = ""
}

variable "nomad_client_cluster_size" {
  type        = number
  description = "The number of nodes to have in the Nomad client cluster. This number is arbitrary."
  default     = 3
}

variable "nomad_client_machine_type" {
  type        = string
  description = "The machine type of the Compute Instance to run for each node in the Nomad client cluster (e.g. n1-standard-1)."
  default     = "g1-small"
}

variable "nomad_client_source_image" {
  type        = string
  description = "The Google Image used to launch each node in the Nomad client cluster."
  default     = ""
}

variable "nomad_consul_server_cluster_machine_type" {
  type        = string
  description = "The machine type of the Compute Instance to run for each node in the Vault cluster (e.g. n1-standard-1)."
  default     = "g1-small"
}

variable "nomad_consul_server_cluster_name" {
  type        = string
  description = "The name of the Nomad/Consul Server cluster. All resources will be namespaced by this value. E.g. nomad-server-prod"
  default     = ""
}

variable "nomad_consul_server_cluster_size" {
  type        = number
  description = "The number of nodes to have in the Nomad Server cluster. We strongly recommended that you use either 3 or 5."
  default     = 3
}

variable "nomad_consul_server_source_image" {
  type        = string
  description = "The Google Image used to launch each node in the Nomad/Consul Server cluster."
  default     = ""
}


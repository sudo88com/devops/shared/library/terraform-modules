
module "hashicorp-consul-eks-addon" {
  source = "terraform-aws-modules/hashicorp-consul-eks-addon/aws"
  version = "1.0.0-rc.2"
  helm_config = var.helm_config
  manage_via_gitops = var.manage_via_gitops
}

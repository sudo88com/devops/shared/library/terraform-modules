
variable "helm_config" {
  type        = any
  description = "HashiCorp Consul Helm chart configuration"
  default     = {}
}

variable "manage_via_gitops" {
  type        = bool
  description = "Determines if the add-on should be managed via GitOps"
  default     = false
}


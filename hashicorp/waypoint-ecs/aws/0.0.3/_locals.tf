
locals {
  alb_internal = var.alb_internal
  alb_port = var.alb_port
  application_port = var.application_port
  aws_region = var.aws_region
  create_ecr = var.create_ecr
  ecs_cluster_name = var.ecs_cluster_name
  force_delete_ecr = var.force_delete_ecr
  log_group_name = var.log_group_name
  private_subnets = var.private_subnets
  public_subnets = var.public_subnets
  tags = var.tags
  task_role_custom_policy_arns = var.task_role_custom_policy_arns
  vpc_id = var.vpc_id
  waypoint_project = var.waypoint_project
  waypoint_workspace = var.waypoint_workspace
}


variable "alb_internal" {
  type        = bool
  description = "    Whether or not the created ALB should be internal.\n    If set, the created ALB will have a scheme of internal,\n    otherwise by default it has a scheme of internet-facing.\n"
  default     = false
}

variable "alb_port" {
  type        = number
  description = "Port on which the load balancer should listen."
  default     = 80
}

variable "application_port" {
  type        = number
  description = "Port on which the application will be listening."
  default     = ""
}

variable "aws_region" {
  type        = string
  description = "    Optional: AWS region for the application to deploy into, e.g. 'us-east-1'.\n\n    If blank, the region output will also be blank.\n"
  default     = ""
}

variable "create_ecr" {
  type        = bool
  description = "    Controls if an ECR repository should be created.\n    Generally, apps have one ECR repository for images,\n    and all environments pull from that same repository. When using\n    this module to create multiple environments (e.g. multiple\n    workspaces), only one environment should create the registry.\n"
  default     = true
}

variable "ecs_cluster_name" {
  type        = string
  description = "    Optional: Name of the ecs cluster that the application should be deployed into.\n    This module will not create an ECS cluster, but it will output this\n    name for the Waypoint ECS Platform plugin to consume.\n\n    If blank, the ecs_cluster_name output will also be blank.\n"
  default     = ""
}

variable "force_delete_ecr" {
  type        = bool
  description = "    Controls if an ECR repository should be forcibly deleted.\n    Waypoint doesn't delete images from a registry. If there\n    have been Waypoint builds and you try to `terraform destroy`\n    the ECR repository resource, and this variable is false, the\n    destroy will fail citing existing images. If this variable\n    is true, it will forcibly delete all the images\n"
  default     = false
}

variable "log_group_name" {
  type        = string
  description = "    Optional: Name of the AWS CloudWatch log group for this application to submit logs to.\n    This module will not create an ECS cluster, but it will output this\n    name for the Waypoint ECS Platform plugin to consume.\n\n    If blank, the log_group_name output will also be blank.\n"
  default     = ""
}

variable "private_subnets" {
  type        = list(string)
  description = "    List of private subnet IDs to assign the the ENIs of ECS tasks, and internal ALBs.\n    Only required if alb_internal is true, or if you wish to rely on this module's\n    private_subnets output.\n"
  default     = ""
}

variable "public_subnets" {
  type        = list(string)
  description = "    List of public subnet IDs to assign to internet-facing ALB for this application.\n    Only required if alb_internal is false, or if wish to rely on this module's\n    public_subnets output.\n"
  default     = ""
}

variable "tags" {
  type        = map(string)
  description = "Tags to apply to resources created by this module."
  default     = {}
}

variable "task_role_custom_policy_arns" {
  type        = list(string)
  description = "A list of IAM policy ARNs which are desired to be attached to the IAM task role.\n"
  default     = []
}

variable "vpc_id" {
  type        = string
  description = "    ID of the VPC for the application to deploy into.\n    Application and load balancer security groups will be created in this VPC.\n"
  default     = ""
}

variable "waypoint_project" {
  type        = string
  description = "Name of the waypoint project, to be used in all resource names."
  default     = ""
}

variable "waypoint_workspace" {
  type        = string
  description = "Name of the waypoint workspace. If set, this workspace name will be included in resource names."
  default     = null
}



output "alb_arn" {
  description = "Arn of the AWS ALB that will serve traffic for the application."
  value       = module.waypoint-ecs.alb_arn
}

output "alb_security_group_id" {
  description = "    ID of the security group created for the ALB.\n    This module will not create , but it will output this\n    name for the Waypoint ECS Platform plugin to consume.\n"
  value       = module.waypoint-ecs.alb_security_group_id
}

output "ecr_repository_name" {
  description = "Name of the ECR registry for this application. Note: "
  value       = module.waypoint-ecs.ecr_repository_name
}

output "ecs_cluster_name" {
  description = "Name of the ecs cluster that the application should be deployed into."
  value       = module.waypoint-ecs.ecs_cluster_name
}

output "execution_role_name" {
  description = "The name of the IAM role to use for ECS execution."
  value       = module.waypoint-ecs.execution_role_name
}

output "log_group_name" {
  description = "Name of the AWS CloudWatch log group for this application to submit logs to."
  value       = module.waypoint-ecs.log_group_name
}

output "private_subnets" {
  description = "    List of private subnet IDs to assign the the ENIs of ECS tasks.\n    Same as input variable private_subnets.\n"
  value       = module.waypoint-ecs.private_subnets
}

output "public_subnets" {
  description = "List of public subnets ids, same as variable public_subnets."
  value       = module.waypoint-ecs.public_subnets
}

output "region" {
  description = "AWS region for the application to deploy into, e.g. 'us-east-1'."
  value       = module.waypoint-ecs.region
}

output "security_group_id" {
  description = "ID of the security group intended for use by ECS tasks"
  value       = module.waypoint-ecs.security_group_id
}

output "task_role_name" {
  description = "The name of the IAM role to assign to the application's ECS tasks."
  value       = module.waypoint-ecs.task_role_name
}


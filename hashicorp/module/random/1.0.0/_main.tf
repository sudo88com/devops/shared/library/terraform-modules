
module "module" {
  source = "terraform-aws-modules/module/aws"
  version = "1.0.0"
  string_length = var.string_length
}

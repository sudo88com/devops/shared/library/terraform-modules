
output "vault_version" {
  description = "Vault version"
  value       = module.vault-ent-starter.vault_version
}

output "vm_scale_set_name" {
  description = "Name of Virtual Machine Scale Set"
  value       = module.vault-ent-starter.vm_scale_set_name
}


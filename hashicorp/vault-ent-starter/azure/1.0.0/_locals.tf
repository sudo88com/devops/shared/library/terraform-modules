
locals {
  common_tags = var.common_tags
  health_check_path = var.health_check_path
  instance_count = var.instance_count
  instance_type = var.instance_type
  key_vault_id = var.key_vault_id
  key_vault_ssl_cert_secret_id = var.key_vault_ssl_cert_secret_id
  key_vault_vm_tls_secret_id = var.key_vault_vm_tls_secret_id
  lb_autoscale_max_capacity = var.lb_autoscale_max_capacity
  lb_autoscale_min_capacity = var.lb_autoscale_min_capacity
  lb_backend_ca_cert = var.lb_backend_ca_cert
  lb_private_ip_address = var.lb_private_ip_address
  lb_sku_capacity = var.lb_sku_capacity
  lb_subnet_id = var.lb_subnet_id
  leader_tls_servername = var.leader_tls_servername
  resource_group = var.resource_group
  resource_name_prefix = var.resource_name_prefix
  ssh_public_key = var.ssh_public_key
  ssh_username = var.ssh_username
  ultra_ssd_enabled = var.ultra_ssd_enabled
  user_supplied_key_vault_key_name = var.user_supplied_key_vault_key_name
  user_supplied_lb_identity_id = var.user_supplied_lb_identity_id
  user_supplied_source_image_id = var.user_supplied_source_image_id
  user_supplied_userdata_path = var.user_supplied_userdata_path
  user_supplied_vm_identity_id = var.user_supplied_vm_identity_id
  vault_application_security_group_ids = var.vault_application_security_group_ids
  vault_license_filepath = var.vault_license_filepath
  vault_subnet_id = var.vault_subnet_id
  vault_version = var.vault_version
  zones = var.zones
}

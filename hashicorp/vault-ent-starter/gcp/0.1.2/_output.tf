
output "lb_address" {
  description = "Load Balancer Address"
  value       = module.vault-ent-starter.lb_address
}


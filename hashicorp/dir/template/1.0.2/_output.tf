
output "files" {
  description = "Map from relative file paths to objects describing all of the files. See the module README for more information."
  value       = module.dir.files
}

output "files_in_memory" {
  description = "A filtered version of the files output that includes only entries that have rendered content in memory."
  value       = module.dir.files_in_memory
}

output "files_on_disk" {
  description = "A filtered version of the files output that includes only entries that point to static files on disk."
  value       = module.dir.files_on_disk
}


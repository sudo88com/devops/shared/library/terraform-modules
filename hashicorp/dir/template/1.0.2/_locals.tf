
locals {
  base_dir = var.base_dir
  default_file_type = var.default_file_type
  file_types = var.file_types
  template_file_suffix = var.template_file_suffix
  template_vars = var.template_vars
}

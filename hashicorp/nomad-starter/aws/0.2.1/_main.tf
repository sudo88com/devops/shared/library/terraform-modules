
module "nomad-starter" {
  source = "terraform-aws-modules/nomad-starter/aws"
  version = "0.2.1"
  allowed_inbound_cidrs = var.allowed_inbound_cidrs
  bootstrap = var.bootstrap
  consul_cluster_version = var.consul_cluster_version
  consul_config = var.consul_config
  consul_version = var.consul_version
  enable_connect = var.enable_connect
  instance_type = var.instance_type
  key_name = var.key_name
  name_prefix = var.name_prefix
  nomad_clients = var.nomad_clients
  nomad_servers = var.nomad_servers
  nomad_version = var.nomad_version
  owner = var.owner
  public_ip = var.public_ip
  vpc_id = var.vpc_id
}

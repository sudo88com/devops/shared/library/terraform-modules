
module "vault" {
  source = "terraform-aws-modules/vault/aws"
  version = "0.0.2"
  address_space = var.address_space
  allowed_inbound_cidr_blocks = var.allowed_inbound_cidr_blocks
  client_id = var.client_id
  consul_cluster_name = var.consul_cluster_name
  image_uri = var.image_uri
  instance_size = var.instance_size
  key_data = var.key_data
  location = var.location
  num_consul_servers = var.num_consul_servers
  num_vault_servers = var.num_vault_servers
  resource_group_name = var.resource_group_name
  secret_access_key = var.secret_access_key
  storage_account_key = var.storage_account_key
  storage_account_name = var.storage_account_name
  subnet_address = var.subnet_address
  subscription_id = var.subscription_id
  tenant_id = var.tenant_id
  vault_cluster_name = var.vault_cluster_name
}


output "load_balancer_ip_address" {
  description = ""
  value       = module.vault.load_balancer_ip_address
}

output "vault_admin_user_name" {
  description = ""
  value       = module.vault.vault_admin_user_name
}

output "vault_cluster_size" {
  description = ""
  value       = module.vault.vault_cluster_size
}


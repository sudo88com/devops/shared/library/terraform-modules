
variable "address_space" {
  type        = string
  description = "The supernet for the resources that will be created"
  default     = "10.0.0.0/16"
}

variable "allowed_inbound_cidr_blocks" {
  type        = list
  description = "A list of CIDR-formatted IP address ranges from which the Azure Instances will allow connections to Consul"
  default     = ""
}

variable "client_id" {
  type        = string
  description = "The Azure client ID"
  default     = ""
}

variable "consul_cluster_name" {
  type        = string
  description = "What to name the Consul cluster and all of its associated resources"
  default     = "consul-example"
}

variable "image_uri" {
  type        = string
  description = "The URI to the Azure image that should be deployed to the consul cluster."
  default     = ""
}

variable "instance_size" {
  type        = string
  description = "The instance size for the servers"
  default     = "Standard_A0"
}

variable "key_data" {
  type        = string
  description = "The SSH public key that will be added to SSH authorized_users on the consul instances"
  default     = ""
}

variable "location" {
  type        = string
  description = "The Azure region the consul cluster will be deployed in"
  default     = "East US"
}

variable "num_consul_servers" {
  type        = string
  description = "The number of Consul server nodes to deploy. We strongly recommend using 3 or 5."
  default     = 3
}

variable "num_vault_servers" {
  type        = string
  description = "The number of Vault server nodes to deploy. We strongly recommend using 3 or 5."
  default     = 3
}

variable "resource_group_name" {
  type        = string
  description = "The name of the Azure resource group consul will be deployed into. This RG should already exist"
  default     = ""
}

variable "secret_access_key" {
  type        = string
  description = "The Azure secret access key"
  default     = ""
}

variable "storage_account_key" {
  type        = string
  description = "The key for storage_account_name."
  default     = ""
}

variable "storage_account_name" {
  type        = string
  description = "The name of an Azure Storage Account. This SA should already exist"
  default     = ""
}

variable "subnet_address" {
  type        = string
  description = "The subnet that consul resources will be deployed into"
  default     = "10.0.10.0/24"
}

variable "subscription_id" {
  type        = string
  description = "The Azure subscription ID"
  default     = ""
}

variable "tenant_id" {
  type        = string
  description = "The Azure tenant ID"
  default     = ""
}

variable "vault_cluster_name" {
  type        = string
  description = "What to name the Vault cluster and all of its associated resources"
  default     = "vault-example"
}


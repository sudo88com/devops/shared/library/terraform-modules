
output "bucket_name_id" {
  description = ""
  value       = module.vault.bucket_name_id
}

output "bucket_name_url" {
  description = ""
  value       = module.vault.bucket_name_url
}

output "cluster_tag_name" {
  description = ""
  value       = module.vault.cluster_tag_name
}

output "firewall_rule_allow_inbound_api_id" {
  description = ""
  value       = module.vault.firewall_rule_allow_inbound_api_id
}

output "firewall_rule_allow_inbound_api_url" {
  description = ""
  value       = module.vault.firewall_rule_allow_inbound_api_url
}

output "firewall_rule_allow_inbound_health_check_id" {
  description = ""
  value       = module.vault.firewall_rule_allow_inbound_health_check_id
}

output "firewall_rule_allow_inbound_health_check_url" {
  description = ""
  value       = module.vault.firewall_rule_allow_inbound_health_check_url
}

output "firewall_rule_allow_intracluster_vault_id" {
  description = ""
  value       = module.vault.firewall_rule_allow_intracluster_vault_id
}

output "firewall_rule_allow_intracluster_vault_url" {
  description = ""
  value       = module.vault.firewall_rule_allow_intracluster_vault_url
}

output "gcp_project_id" {
  description = ""
  value       = module.vault.gcp_project_id
}

output "instance_group_id" {
  description = ""
  value       = module.vault.instance_group_id
}

output "instance_group_name" {
  description = ""
  value       = module.vault.instance_group_name
}

output "instance_group_url" {
  description = ""
  value       = module.vault.instance_group_url
}

output "instance_template_url" {
  description = ""
  value       = module.vault.instance_template_url
}

output "vault_cluster_size" {
  description = ""
  value       = module.vault.vault_cluster_size
}


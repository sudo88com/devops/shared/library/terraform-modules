
variable "consul_server_cluster_name" {
  type        = string
  description = "The name of the Consul Server cluster. All resources will be namespaced by this value. E.g. consul-server-prod"
  default     = ""
}

variable "consul_server_cluster_size" {
  type        = number
  description = "The number of nodes to have in the Consul Server cluster. We strongly recommended that you use either 3 or 5."
  default     = 3
}

variable "consul_server_machine_type" {
  type        = string
  description = "The machine type of the Compute Instance to run for each node in the Consul Server cluster (e.g. n1-standard-1)."
  default     = "g1-small"
}

variable "consul_server_source_image" {
  type        = string
  description = "The Google Image used to launch each node in the Consul Server cluster. You can build this Google Image yourself at /examples/vault-consul-image."
  default     = ""
}

variable "enable_vault_ui" {
  type        = bool
  description = "If true, enable the Vault UI"
  default     = true
}

variable "gcp_project_id" {
  type        = string
  description = "The name of the GCP Project where all resources will be launched."
  default     = ""
}

variable "gcp_region" {
  type        = string
  description = "The region in which all GCP resources will be launched."
  default     = ""
}

variable "gcs_bucket_class" {
  type        = string
  description = "The Storage Class of the Google Cloud Storage Bucket where Vault secrets will be stored. Must be one of MULTI_REGIONAL, REGIONAL, NEARLINE, or COLDLINE. For details, see https://goo.gl/hk63jH."
  default     = "MULTI_REGIONAL"
}

variable "gcs_bucket_force_destroy" {
  type        = bool
  description = "If true, Terraform will delete the Google Cloud Storage Bucket even if it's non-empty. WARNING! Never set this to true in a production setting. We only have this option here to facilitate testing."
  default     = true
}

variable "gcs_bucket_location" {
  type        = string
  description = "The location of the Google Cloud Storage Bucket where Vault secrets will be stored. For details, see https://goo.gl/hk63jH."
  default     = "US"
}

variable "image_project_id" {
  type        = string
  description = "The name of the GCP Project where the image is located. Useful when using a separate project for custom images. If empty, var.gcp_project_id will be used."
  default     = ""
}

variable "network_project_id" {
  type        = string
  description = "The name of the GCP Project where the network is located. Useful when using networks shared between projects. If empty, var.gcp_project_id will be used."
  default     = ""
}

variable "root_volume_disk_size_gb" {
  type        = number
  description = "The size, in GB, of the root disk volume on each Consul node."
  default     = 30
}

variable "root_volume_disk_type" {
  type        = string
  description = "The GCE disk type. Can be either pd-ssd, local-ssd, or pd-standard"
  default     = "pd-standard"
}

variable "vault_cluster_machine_type" {
  type        = string
  description = "The machine type of the Compute Instance to run for each node in the Vault cluster (e.g. n1-standard-1)."
  default     = "g1-small"
}

variable "vault_cluster_name" {
  type        = string
  description = "The name of the Vault Server cluster. All resources will be namespaced by this value. E.g. vault-server-prod"
  default     = ""
}

variable "vault_cluster_size" {
  type        = number
  description = "The number of nodes to have in the Vault Server cluster. We strongly recommended that you use either 3 or 5."
  default     = 3
}

variable "vault_source_image" {
  type        = string
  description = "The Google Image used to launch each node in the Vault Server cluster. You can build this Google Image yourself at /examples/vault-consul-image."
  default     = ""
}

variable "web_proxy_port" {
  type        = number
  description = "The port at which the HTTP proxy server will listen for incoming HTTP requests that will be forwarded to the Vault Health Check URL. We must have an HTTP proxy server to work around the limitation that GCP only permits Health Checks via HTTP, not HTTPS."
  default     = 8000
}


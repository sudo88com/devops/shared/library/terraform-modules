
consul_server_cluster_name = 

consul_server_cluster_size = 3

consul_server_machine_type = "g1-small"

consul_server_source_image = 

enable_vault_ui = true

gcp_project_id = 

gcp_region = 

gcs_bucket_class = "MULTI_REGIONAL"

gcs_bucket_force_destroy = true

gcs_bucket_location = "US"

image_project_id = 

network_project_id = 

root_volume_disk_size_gb = 30

root_volume_disk_type = "pd-standard"

vault_cluster_machine_type = "g1-small"

vault_cluster_name = 

vault_cluster_size = 3

vault_source_image = 

web_proxy_port = 8000



module "vault" {
  source = "terraform-aws-modules/vault/aws"
  version = "0.2.0"
  consul_server_cluster_name = var.consul_server_cluster_name
  consul_server_cluster_size = var.consul_server_cluster_size
  consul_server_machine_type = var.consul_server_machine_type
  consul_server_source_image = var.consul_server_source_image
  enable_vault_ui = var.enable_vault_ui
  gcp_project_id = var.gcp_project_id
  gcp_region = var.gcp_region
  gcs_bucket_class = var.gcs_bucket_class
  gcs_bucket_force_destroy = var.gcs_bucket_force_destroy
  gcs_bucket_location = var.gcs_bucket_location
  image_project_id = var.image_project_id
  network_project_id = var.network_project_id
  root_volume_disk_size_gb = var.root_volume_disk_size_gb
  root_volume_disk_type = var.root_volume_disk_type
  vault_cluster_machine_type = var.vault_cluster_machine_type
  vault_cluster_name = var.vault_cluster_name
  vault_cluster_size = var.vault_cluster_size
  vault_source_image = var.vault_source_image
  web_proxy_port = var.web_proxy_port
}

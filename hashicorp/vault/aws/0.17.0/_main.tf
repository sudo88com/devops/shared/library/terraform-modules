
module "vault" {
  source = "terraform-aws-modules/vault/aws"
  version = "0.17.0"
  ami_id = var.ami_id
  consul_cluster_name = var.consul_cluster_name
  consul_cluster_size = var.consul_cluster_size
  consul_cluster_tag_key = var.consul_cluster_tag_key
  consul_instance_type = var.consul_instance_type
  create_dns_entry = var.create_dns_entry
  hosted_zone_domain_name = var.hosted_zone_domain_name
  ssh_key_name = var.ssh_key_name
  subnet_tags = var.subnet_tags
  use_default_vpc = var.use_default_vpc
  vault_cluster_name = var.vault_cluster_name
  vault_cluster_size = var.vault_cluster_size
  vault_domain_name = var.vault_domain_name
  vault_instance_type = var.vault_instance_type
  vpc_tags = var.vpc_tags
}

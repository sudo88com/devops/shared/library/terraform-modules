
variable "ami_id" {
  type        = string
  description = "The ID of the AMI to run in the cluster. This should be an AMI built from the Packer template under examples/vault-consul-ami/vault-consul.json. If no AMI is specified, the template will 'just work' by using the example public AMIs. WARNING! Do not use the example AMIs in a production setting!"
  default     = ""
}

variable "consul_cluster_name" {
  type        = string
  description = "What to name the Consul server cluster and all of its associated resources"
  default     = "consul-example"
}

variable "consul_cluster_size" {
  type        = number
  description = "The number of Consul server nodes to deploy. We strongly recommend using 3 or 5."
  default     = 3
}

variable "consul_cluster_tag_key" {
  type        = string
  description = "The tag the Consul EC2 Instances will look for to automatically discover each other and form a cluster."
  default     = "consul-servers"
}

variable "consul_instance_type" {
  type        = string
  description = "The type of EC2 Instance to run in the Consul ASG"
  default     = "t2.nano"
}

variable "create_dns_entry" {
  type        = bool
  description = "If set to true, this module will create a Route 53 DNS A record for the ELB in the var.hosted_zone_id hosted zone with the domain name in var.vault_domain_name."
  default     = false
}

variable "hosted_zone_domain_name" {
  type        = string
  description = "The domain name of the Route 53 Hosted Zone in which to add a DNS entry for Vault (e.g. example.com). Only used if var.create_dns_entry is true."
  default     = ""
}

variable "ssh_key_name" {
  type        = string
  description = "The name of an EC2 Key Pair that can be used to SSH to the EC2 Instances in this cluster. Set to an empty string to not associate a Key Pair."
  default     = ""
}

variable "subnet_tags" {
  type        = map(string)
  description = "Tags used to find subnets for vault and consul servers"
  default     = {}
}

variable "use_default_vpc" {
  type        = bool
  description = "Whether to use the default VPC - NOT recommended for production! - should more likely change this to false and use the vpc_tags to find your vpc"
  default     = true
}

variable "vault_cluster_name" {
  type        = string
  description = "What to name the Vault server cluster and all of its associated resources"
  default     = "vault-example"
}

variable "vault_cluster_size" {
  type        = number
  description = "The number of Vault server nodes to deploy. We strongly recommend using 3 or 5."
  default     = 3
}

variable "vault_domain_name" {
  type        = string
  description = "The domain name to use in the DNS A record for the Vault ELB (e.g. vault.example.com). Make sure that a) this is a domain within the var.hosted_zone_domain_name hosted zone and b) this is the same domain name you used in the TLS certificates for Vault. Only used if var.create_dns_entry is true."
  default     = ""
}

variable "vault_instance_type" {
  type        = string
  description = "The type of EC2 Instance to run in the Vault ASG"
  default     = "t2.micro"
}

variable "vpc_tags" {
  type        = map(string)
  description = "Tags used to find a vpc for building resources in"
  default     = {}
}


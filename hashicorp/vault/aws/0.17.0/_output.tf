
output "asg_name_consul_cluster" {
  description = ""
  value       = module.vault.asg_name_consul_cluster
}

output "asg_name_vault_cluster" {
  description = ""
  value       = module.vault.asg_name_vault_cluster
}

output "aws_region" {
  description = ""
  value       = module.vault.aws_region
}

output "iam_role_arn_consul_cluster" {
  description = ""
  value       = module.vault.iam_role_arn_consul_cluster
}

output "iam_role_arn_vault_cluster" {
  description = ""
  value       = module.vault.iam_role_arn_vault_cluster
}

output "iam_role_id_consul_cluster" {
  description = ""
  value       = module.vault.iam_role_id_consul_cluster
}

output "iam_role_id_vault_cluster" {
  description = ""
  value       = module.vault.iam_role_id_vault_cluster
}

output "launch_config_name_consul_cluster" {
  description = ""
  value       = module.vault.launch_config_name_consul_cluster
}

output "launch_config_name_vault_cluster" {
  description = ""
  value       = module.vault.launch_config_name_vault_cluster
}

output "security_group_id_consul_cluster" {
  description = ""
  value       = module.vault.security_group_id_consul_cluster
}

output "security_group_id_vault_cluster" {
  description = ""
  value       = module.vault.security_group_id_vault_cluster
}

output "ssh_key_name" {
  description = ""
  value       = module.vault.ssh_key_name
}

output "vault_cluster_size" {
  description = ""
  value       = module.vault.vault_cluster_size
}

output "vault_elb_dns_name" {
  description = ""
  value       = module.vault.vault_elb_dns_name
}

output "vault_fully_qualified_domain_name" {
  description = ""
  value       = module.vault.vault_fully_qualified_domain_name
}

output "vault_servers_cluster_tag_key" {
  description = ""
  value       = module.vault.vault_servers_cluster_tag_key
}

output "vault_servers_cluster_tag_value" {
  description = ""
  value       = module.vault.vault_servers_cluster_tag_value
}


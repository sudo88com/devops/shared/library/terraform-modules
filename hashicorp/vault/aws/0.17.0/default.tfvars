
ami_id = 

consul_cluster_name = "consul-example"

consul_cluster_size = 3

consul_cluster_tag_key = "consul-servers"

consul_instance_type = "t2.nano"

create_dns_entry = false

hosted_zone_domain_name = 

ssh_key_name = 

subnet_tags = {}

use_default_vpc = true

vault_cluster_name = "vault-example"

vault_cluster_size = 3

vault_domain_name = 

vault_instance_type = "t2.micro"

vpc_tags = {}


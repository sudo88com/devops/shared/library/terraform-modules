
create_namespace = true

create_secrets = true

image_k8s = "hashicorp/terraform-k8s:1.0.0"

insecure = false

k8_watch_namespace = "null"

log_level = "null"

operator_namespace = 

terraform_credentials_path = "credentials"

terraform_version = "latest"

tfe_address = "https://app.terraform.io"

workspace_secrets = {}


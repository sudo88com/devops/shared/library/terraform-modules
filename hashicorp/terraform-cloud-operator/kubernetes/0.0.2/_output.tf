
output "deployment_name" {
  description = "https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/deployment#name"
  value       = module.terraform-cloud-operator.deployment_name
}


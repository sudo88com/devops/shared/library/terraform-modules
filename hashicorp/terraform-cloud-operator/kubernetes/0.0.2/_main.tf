
module "terraform-cloud-operator" {
  source = "terraform-aws-modules/terraform-cloud-operator/aws"
  version = "0.0.2"
  create_namespace = var.create_namespace
  create_secrets = var.create_secrets
  image_k8s = var.image_k8s
  insecure = var.insecure
  k8_watch_namespace = var.k8_watch_namespace
  log_level = var.log_level
  operator_namespace = var.operator_namespace
  terraform_credentials_path = var.terraform_credentials_path
  terraform_version = var.terraform_version
  tfe_address = var.tfe_address
  workspace_secrets = var.workspace_secrets
}

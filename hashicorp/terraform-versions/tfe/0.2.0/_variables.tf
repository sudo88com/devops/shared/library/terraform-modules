
variable "enabled_versions" {
  type        = list(string)
  description = "List of official versions to enable. By default all are enabled."
  default     = ""
}



module "terraform-versions" {
  source = "terraform-aws-modules/terraform-versions/aws"
  version = "0.2.0"
  enabled_versions = var.enabled_versions
}

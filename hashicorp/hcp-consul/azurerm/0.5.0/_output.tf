
output "nsg" {
  description = "Newly created azure nsg if 'security_group_names' was not provided"
  value       = module.hcp-consul.nsg
}


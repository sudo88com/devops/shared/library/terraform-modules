
locals {
  hvn = var.hvn
  prefix = var.prefix
  security_group_names = var.security_group_names
  subnet_ids = var.subnet_ids
  subscription_id = var.subscription_id
  tenant_id = var.tenant_id
  vnet_id = var.vnet_id
  vnet_rg = var.vnet_rg
}


locals {
  hvn = var.hvn
  route_table_ids = var.route_table_ids
  security_group_ids = var.security_group_ids
  subnet_ids = var.subnet_ids
  vpc_id = var.vpc_id
}

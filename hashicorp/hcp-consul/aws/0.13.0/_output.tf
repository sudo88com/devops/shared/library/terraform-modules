
output "security_group_id" {
  description = "Newly created AWS security group that allow Consul client communication, if 'security_group_ids' was not provided."
  value       = module.hcp-consul.security_group_id
}


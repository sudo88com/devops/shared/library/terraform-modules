
module "hcp-consul" {
  source = "terraform-aws-modules/hcp-consul/aws"
  version = "0.13.0"
  hvn = var.hvn
  route_table_ids = var.route_table_ids
  security_group_ids = var.security_group_ids
  subnet_ids = var.subnet_ids
  vpc_id = var.vpc_id
}


locals {
  helm_config = var.helm_config
  manage_via_gitops = var.manage_via_gitops
}

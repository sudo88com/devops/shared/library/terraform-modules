
module "hashicorp-vault-eks-addon" {
  source = "terraform-aws-modules/hashicorp-vault-eks-addon/aws"
  version = "1.0.0-rc2"
  helm_config = var.helm_config
  manage_via_gitops = var.manage_via_gitops
}

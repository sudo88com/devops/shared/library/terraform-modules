
output "argocd_gitops_config" {
  description = "Configuration used for managing the add-on with ArgoCD"
  value       = module.hashicorp-vault-eks-addon.argocd_gitops_config
}

output "merged_helm_config" {
  description = "(merged) Helm Config for HashiCorp Vault"
  value       = module.hashicorp-vault-eks-addon.merged_helm_config
}


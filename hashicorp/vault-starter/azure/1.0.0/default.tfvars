
common_tags = {}

health_check_path = "/v1/sys/health?activecode=200\u0026standbycode=200\u0026sealedcode=200\u0026uninitcode=200"

instance_count = 5

instance_type = "Standard_D2s_v3"

key_vault_id = 

key_vault_ssl_cert_secret_id = 

key_vault_vm_tls_secret_id = 

lb_autoscale_max_capacity = null

lb_autoscale_min_capacity = 0

lb_backend_ca_cert = 

lb_private_ip_address = null

lb_sku_capacity = null

lb_subnet_id = 

leader_tls_servername = 

resource_group = 

resource_name_prefix = 

ssh_public_key = ""

ssh_username = "azureuser"

ultra_ssd_enabled = true

user_supplied_key_vault_key_name = null

user_supplied_lb_identity_id = null

user_supplied_source_image_id = null

user_supplied_userdata_path = null

user_supplied_vm_identity_id = null

vault_application_security_group_ids = 

vault_subnet_id = 

vault_version = "1.8.2"

zones = [
  "1",
  "2",
  "3"
]


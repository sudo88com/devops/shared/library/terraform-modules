
module "vault-starter" {
  source = "terraform-aws-modules/vault-starter/aws"
  version = "1.0.0"
  allowed_inbound_cidrs_lb = var.allowed_inbound_cidrs_lb
  allowed_inbound_cidrs_ssh = var.allowed_inbound_cidrs_ssh
  common_tags = var.common_tags
  instance_type = var.instance_type
  key_name = var.key_name
  kms_key_deletion_window = var.kms_key_deletion_window
  lb_certificate_arn = var.lb_certificate_arn
  lb_health_check_path = var.lb_health_check_path
  lb_type = var.lb_type
  leader_tls_servername = var.leader_tls_servername
  node_count = var.node_count
  private_subnet_tags = var.private_subnet_tags
  resource_name_prefix = var.resource_name_prefix
  secrets_manager_arn = var.secrets_manager_arn
  ssl_policy = var.ssl_policy
  user_supplied_ami_id = var.user_supplied_ami_id
  user_supplied_iam_role_name = var.user_supplied_iam_role_name
  user_supplied_kms_key_arn = var.user_supplied_kms_key_arn
  user_supplied_userdata_path = var.user_supplied_userdata_path
  vault_version = var.vault_version
  vpc_id = var.vpc_id
}

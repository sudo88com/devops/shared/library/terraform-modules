
module "vault-starter" {
  source = "terraform-aws-modules/vault-starter/aws"
  version = "0.1.0"
  create_load_balancer = var.create_load_balancer
  leader_tls_servername = var.leader_tls_servername
  location = var.location
  networking_firewall_ports = var.networking_firewall_ports
  networking_healthcheck_ips = var.networking_healthcheck_ips
  networking_ip_allow_list = var.networking_ip_allow_list
  node_count = var.node_count
  project_id = var.project_id
  reserve_subnet_range = var.reserve_subnet_range
  resource_name_prefix = var.resource_name_prefix
  ssh_source_ranges = var.ssh_source_ranges
  ssl_certificate_name = var.ssl_certificate_name
  subnetwork = var.subnetwork
  tls_secret_id = var.tls_secret_id
  user_supplied_kms_crypto_key_self_link = var.user_supplied_kms_crypto_key_self_link
  user_supplied_kms_key_ring_self_link = var.user_supplied_kms_key_ring_self_link
  user_supplied_userdata_path = var.user_supplied_userdata_path
  vault_lb_health_check = var.vault_lb_health_check
  vault_version = var.vault_version
  vm_disk_size = var.vm_disk_size
  vm_disk_source_image = var.vm_disk_source_image
  vm_disk_type = var.vm_disk_type
  vm_machine_type = var.vm_machine_type
}

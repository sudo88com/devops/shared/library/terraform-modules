
variable "create_load_balancer" {
  type        = bool
  description = "If true, creates a private load balancer. Set to false to disable the load balancer submodule"
  default     = true
}

variable "leader_tls_servername" {
  type        = string
  description = "One of the shared DNS SAN used to create the certs used for mTLS"
  default     = ""
}

variable "location" {
  type        = string
  description = "Location of the kms key ring"
  default     = "global"
}

variable "networking_firewall_ports" {
  type        = list(string)
  description = "Additional ports to open in the firewall"
  default     = []
}

variable "networking_healthcheck_ips" {
  type        = list(string)
  description = "Allowed IPs required for healthcheck. Provided by GCP"
  default     = [
  "35.191.0.0/16",
  "130.211.0.0/22"
]
}

variable "networking_ip_allow_list" {
  type        = list(string)
  description = "List of allowed IPs for the firewall"
  default     = [
  "0.0.0.0/0"
]
}

variable "node_count" {
  type        = number
  description = "Number of Vault nodes to deploy"
  default     = 5
}

variable "project_id" {
  type        = string
  description = "GCP project in which to launch resources"
  default     = ""
}

variable "reserve_subnet_range" {
  type        = string
  description = "The IP address ranges for the https proxy range for the load balancer"
  default     = "10.1.0.0/16"
}

variable "resource_name_prefix" {
  type        = string
  description = "Prefix for naming resources"
  default     = ""
}

variable "ssh_source_ranges" {
  type        = list(string)
  description = "The source IP address ranges from which SSH traffic will be permitted; these ranges must be expressed in CIDR format. The default value permits traffic from GCP's Identity-Aware Proxy"
  default     = [
  "35.235.240.0/20"
]
}

variable "ssl_certificate_name" {
  type        = string
  description = "Name of the created managed SSL certificate. Required when create_load_balancer is true"
  default     = ""
}

variable "subnetwork" {
  type        = string
  description = "The self link of the subnetwork in which to deploy resources"
  default     = ""
}

variable "tls_secret_id" {
  type        = string
  description = "Secret id/name given to the Google Secret Manager secret"
  default     = ""
}

variable "user_supplied_kms_crypto_key_self_link" {
  type        = string
  description = "(Optional) Self link to user created kms crypto key"
  default     = ""
}

variable "user_supplied_kms_key_ring_self_link" {
  type        = string
  description = "(Optional) Self link to user created kms key ring"
  default     = ""
}

variable "user_supplied_userdata_path" {
  type        = string
  description = "(Optional) File path to custom userdata script being supplied by the user"
  default     = ""
}

variable "vault_lb_health_check" {
  type        = string
  description = "The endpoint to check for Vault's health status"
  default     = "/v1/sys/health?activecode=200\u0026standbycode=200\u0026sealedcode=200\u0026uninitcode=200"
}

variable "vault_version" {
  type        = string
  description = "Vault version"
  default     = "1.8.2"
}

variable "vm_disk_size" {
  type        = number
  description = "VM Disk size"
  default     = 500
}

variable "vm_disk_source_image" {
  type        = string
  description = "VM Disk source image"
  default     = "projects/ubuntu-os-cloud/global/images/family/ubuntu-2004-lts"
}

variable "vm_disk_type" {
  type        = string
  description = "VM Disk type. SSD recommended"
  default     = "pd-ssd"
}

variable "vm_machine_type" {
  type        = string
  description = "VM Machine Type"
  default     = "n2-standard-4"
}


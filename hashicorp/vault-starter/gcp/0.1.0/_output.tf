
output "lb_address" {
  description = "Load Balancer Address"
  value       = module.vault-starter.lb_address
}



create_load_balancer = true

leader_tls_servername = 

location = "global"

networking_firewall_ports = []

networking_healthcheck_ips = [
  "35.191.0.0/16",
  "130.211.0.0/22"
]

networking_ip_allow_list = [
  "0.0.0.0/0"
]

node_count = 5

project_id = 

reserve_subnet_range = "10.1.0.0/16"

resource_name_prefix = 

ssh_source_ranges = [
  "35.235.240.0/20"
]

ssl_certificate_name = 

subnetwork = 

tls_secret_id = 

user_supplied_kms_crypto_key_self_link = 

user_supplied_kms_key_ring_self_link = 

user_supplied_userdata_path = 

vault_lb_health_check = "/v1/sys/health?activecode=200\u0026standbycode=200\u0026sealedcode=200\u0026uninitcode=200"

vault_version = "1.8.2"

vm_disk_size = 500

vm_disk_source_image = "projects/ubuntu-os-cloud/global/images/family/ubuntu-2004-lts"

vm_disk_type = "pd-ssd"

vm_machine_type = "n2-standard-4"



azure_key_vault_id = 

azure_key_vault_name = 

azure_key_vault_secret_name = "federationsecret"

chart_name = "consul"

chart_repository = "https://helm.releases.hashicorp.com"

cluster_name = 

consul_helm_chart_version = "0.41.0"

consul_license = 

consul_namespace = "consul"

consul_version = "1.11.5"

create_namespace = true

kubernetes_namespace = "consul"

primary_datacenter = 

release_name = "consul-release"

resource_group_name = 

server_replicas = 5



module "consul-ent-k8s" {
  source = "terraform-aws-modules/consul-ent-k8s/aws"
  version = "0.1.0"
  aws_secrets_manager_name = var.aws_secrets_manager_name
  chart_name = var.chart_name
  chart_repository = var.chart_repository
  cluster_name = var.cluster_name
  consul_helm_chart_version = var.consul_helm_chart_version
  consul_license = var.consul_license
  consul_namespace = var.consul_namespace
  consul_version = var.consul_version
  create_namespace = var.create_namespace
  kubernetes_namespace = var.kubernetes_namespace
  primary_datacenter = var.primary_datacenter
  release_name = var.release_name
  server_replicas = var.server_replicas
}

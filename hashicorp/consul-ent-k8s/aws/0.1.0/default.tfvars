
aws_secrets_manager_name = 

chart_name = "consul"

chart_repository = "https://helm.releases.hashicorp.com"

cluster_name = 

consul_helm_chart_version = "0.41.0"

consul_license = 

consul_namespace = "consul"

consul_version = "1.11.5"

create_namespace = true

kubernetes_namespace = "consul"

primary_datacenter = 

release_name = "consul-release"

server_replicas = 5


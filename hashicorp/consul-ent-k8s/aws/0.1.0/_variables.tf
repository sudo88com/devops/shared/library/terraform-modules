
variable "aws_secrets_manager_name" {
  type        = string
  description = "Name of AWS Secrets Manager secret that will store Consul federation data"
  default     = ""
}

variable "chart_name" {
  type        = string
  description = "Chart name to be installed"
  default     = "consul"
}

variable "chart_repository" {
  type        = string
  description = "Repository URL where to locate the requested chart"
  default     = "https://helm.releases.hashicorp.com"
}

variable "cluster_name" {
  type        = string
  description = "Name of EKS cluster"
  default     = ""
}

variable "consul_helm_chart_version" {
  type        = string
  description = "Version of Consul helm chart."
  default     = "0.41.0"
}

variable "consul_license" {
  type        = string
  description = "Consul license"
  default     = ""
}

variable "consul_namespace" {
  type        = string
  description = "The namespace to install the release into"
  default     = "consul"
}

variable "consul_version" {
  type        = string
  description = "Version of Consul Enterprise to install"
  default     = "1.11.5"
}

variable "create_namespace" {
  type        = bool
  description = "Create the k8s namespace if it does not yet exist"
  default     = true
}

variable "kubernetes_namespace" {
  type        = string
  description = "The namespace to install the k8s resources into"
  default     = "consul"
}

variable "primary_datacenter" {
  type        = bool
  description = "If true, installs Consul with a primary datacenter configuration. Set to false for secondary datacenters"
  default     = ""
}

variable "release_name" {
  type        = string
  description = "The helm release name"
  default     = "consul-release"
}

variable "server_replicas" {
  type        = number
  description = "The number of Consul server replicas"
  default     = 5
}


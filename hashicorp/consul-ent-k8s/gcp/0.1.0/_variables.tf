
variable "chart_name" {
  type        = string
  description = "Chart name to be installed"
  default     = "consul"
}

variable "chart_repository" {
  type        = string
  description = "Repository URL where to locate the requested chart"
  default     = "https://helm.releases.hashicorp.com"
}

variable "cluster_location" {
  type        = string
  description = "Kubernetes cluster region"
  default     = ""
}

variable "cluster_name" {
  type        = string
  description = "Kubernetes cluster name on which to install Consul"
  default     = ""
}

variable "consul_helm_chart_version" {
  type        = string
  description = "Version of Consul helm chart"
  default     = "0.41.0"
}

variable "consul_license" {
  type        = string
  description = "Path to your Consul license file"
  default     = ""
}

variable "consul_version" {
  type        = string
  description = "Version of Consul Enterprise to install"
  default     = "1.11.5"
}

variable "create_namespace" {
  type        = bool
  description = "Create the k8s namespace if it does not yet exist"
  default     = true
}

variable "federation_secret_id" {
  type        = string
  description = "Secret id/name given to the google secrets manager secret for the Consul federation secret"
  default     = ""
}

variable "kubernetes_namespace" {
  type        = string
  description = "The namespace to install the release into"
  default     = "consul"
}

variable "primary_datacenter" {
  type        = bool
  description = "If true, installs Consul with a primary datacenter configuration. Set to false for secondary datacenters"
  default     = ""
}

variable "release_name" {
  type        = string
  description = "The helm release name"
  default     = "consul-release"
}

variable "server_replicas" {
  type        = number
  description = "The number of Consul server replicas"
  default     = 5
}



chart_name = "consul"

chart_repository = "https://helm.releases.hashicorp.com"

cluster_location = 

cluster_name = 

consul_helm_chart_version = "0.41.0"

consul_license = 

consul_version = "1.11.5"

create_namespace = true

federation_secret_id = 

kubernetes_namespace = "consul"

primary_datacenter = 

release_name = "consul-release"

server_replicas = 5


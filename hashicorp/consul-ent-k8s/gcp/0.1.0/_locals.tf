
locals {
  chart_name = var.chart_name
  chart_repository = var.chart_repository
  cluster_location = var.cluster_location
  cluster_name = var.cluster_name
  consul_helm_chart_version = var.consul_helm_chart_version
  consul_license = var.consul_license
  consul_version = var.consul_version
  create_namespace = var.create_namespace
  federation_secret_id = var.federation_secret_id
  kubernetes_namespace = var.kubernetes_namespace
  primary_datacenter = var.primary_datacenter
  release_name = var.release_name
  server_replicas = var.server_replicas
}

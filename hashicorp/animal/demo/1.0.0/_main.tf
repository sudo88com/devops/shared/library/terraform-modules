
module "animal" {
  source = "terraform-aws-modules/animal/aws"
  version = "1.0.0"
  name = var.name
}


locals {
  base_cidr_block = var.base_cidr_block
  networks = var.networks
}

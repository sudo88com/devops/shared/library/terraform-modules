
module "subnets" {
  source = "terraform-aws-modules/subnets/aws"
  version = "1.0.0"
  base_cidr_block = var.base_cidr_block
  networks = var.networks
}


locals {
  acl_bootstrap_bool = var.acl_bootstrap_bool
  allowed_inbound_cidrs = var.allowed_inbound_cidrs
  consul_clients = var.consul_clients
  consul_cluster_version = var.consul_cluster_version
  consul_config = var.consul_config
  consul_servers = var.consul_servers
  consul_version = var.consul_version
  enable_connect = var.enable_connect
  instance_type = var.instance_type
  key_name = var.key_name
  name_prefix = var.name_prefix
  owner = var.owner
  public_ip = var.public_ip
  vpc_id = var.vpc_id
}


variable "acl_bootstrap_bool" {
  type        = bool
  description = "Initial ACL Bootstrap configurations"
  default     = true
}

variable "allowed_inbound_cidrs" {
  type        = list(string)
  description = "List of CIDR blocks to permit inbound Consul access from"
  default     = ""
}

variable "consul_clients" {
  type        = string
  description = "number of Consul instances"
  default     = "3"
}

variable "consul_cluster_version" {
  type        = string
  description = "Custom Version Tag for Upgrade Migrations"
  default     = "0.0.1"
}

variable "consul_config" {
  type        = map
  description = "HCL Object with additional configuration overrides supplied to the consul servers.  This is converted to JSON before rendering via the template."
  default     = {}
}

variable "consul_servers" {
  type        = string
  description = "number of Consul instances"
  default     = "5"
}

variable "consul_version" {
  type        = string
  description = "Consul version"
  default     = ""
}

variable "enable_connect" {
  type        = bool
  description = "Whether Consul Connect should be enabled on the cluster"
  default     = false
}

variable "instance_type" {
  type        = string
  description = "Instance type for Consul instances"
  default     = "m5.large"
}

variable "key_name" {
  type        = string
  description = "SSH key name for Consul instances"
  default     = "default"
}

variable "name_prefix" {
  type        = string
  description = "prefix used in resource names"
  default     = ""
}

variable "owner" {
  type        = string
  description = "value of owner tag on EC2 instances"
  default     = ""
}

variable "public_ip" {
  type        = bool
  description = "should ec2 instance have public ip?"
  default     = false
}

variable "vpc_id" {
  type        = string
  description = "VPC ID"
  default     = ""
}


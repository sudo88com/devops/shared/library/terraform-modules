
module "consul" {
  source = "terraform-aws-modules/consul/aws"
  version = "0.5.0"
  consul_client_allowed_inbound_cidr_blocks_dns = var.consul_client_allowed_inbound_cidr_blocks_dns
  consul_client_allowed_inbound_cidr_blocks_http_api = var.consul_client_allowed_inbound_cidr_blocks_http_api
  consul_client_cluster_name = var.consul_client_cluster_name
  consul_client_cluster_size = var.consul_client_cluster_size
  consul_client_cluster_tag_name = var.consul_client_cluster_tag_name
  consul_client_source_image = var.consul_client_source_image
  consul_server_allowed_inbound_cidr_blocks_dns = var.consul_server_allowed_inbound_cidr_blocks_dns
  consul_server_allowed_inbound_cidr_blocks_http_api = var.consul_server_allowed_inbound_cidr_blocks_http_api
  consul_server_cluster_name = var.consul_server_cluster_name
  consul_server_cluster_size = var.consul_server_cluster_size
  consul_server_cluster_tag_name = var.consul_server_cluster_tag_name
  consul_server_source_image = var.consul_server_source_image
  gcp_project_id = var.gcp_project_id
  gcp_region = var.gcp_region
  image_project_id = var.image_project_id
  network_project_id = var.network_project_id
}

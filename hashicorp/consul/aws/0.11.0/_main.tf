
module "consul" {
  source = "terraform-aws-modules/consul/aws"
  version = "0.11.0"
  ami_id = var.ami_id
  cluster_name = var.cluster_name
  cluster_tag_key = var.cluster_tag_key
  enable_https_port = var.enable_https_port
  num_clients = var.num_clients
  num_servers = var.num_servers
  spot_price = var.spot_price
  ssh_key_name = var.ssh_key_name
  vpc_id = var.vpc_id
}

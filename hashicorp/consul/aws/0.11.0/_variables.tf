
variable "ami_id" {
  type        = string
  description = "The ID of the AMI to run in the cluster. This should be an AMI built from the Packer template under examples/consul-ami/consul.json. To keep this example simple, we run the same AMI on both server and client nodes, but in real-world usage, your client nodes would also run your apps. If the default value is used, Terraform will look up the latest AMI build automatically."
  default     = ""
}

variable "cluster_name" {
  type        = string
  description = "What to name the Consul cluster and all of its associated resources"
  default     = "consul-example"
}

variable "cluster_tag_key" {
  type        = string
  description = "The tag the EC2 Instances will look for to automatically discover each other and form a cluster."
  default     = "consul-servers"
}

variable "enable_https_port" {
  type        = bool
  description = "If set to true, allow access to the Consul HTTPS port defined via the https_api_port variable."
  default     = false
}

variable "num_clients" {
  type        = number
  description = "The number of Consul client nodes to deploy. You typically run the Consul client alongside your apps, so set this value to however many Instances make sense for your app code."
  default     = 6
}

variable "num_servers" {
  type        = number
  description = "The number of Consul server nodes to deploy. We strongly recommend using 3 or 5."
  default     = 3
}

variable "spot_price" {
  type        = number
  description = "The maximum hourly price to pay for EC2 Spot Instances."
  default     = ""
}

variable "ssh_key_name" {
  type        = string
  description = "The name of an EC2 Key Pair that can be used to SSH to the EC2 Instances in this cluster. Set to an empty string to not associate a Key Pair."
  default     = ""
}

variable "vpc_id" {
  type        = string
  description = "The ID of the VPC in which the nodes will be deployed.  Uses default VPC if not supplied."
  default     = ""
}


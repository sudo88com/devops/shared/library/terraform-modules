
output "asg_name_clients" {
  description = ""
  value       = module.consul.asg_name_clients
}

output "asg_name_servers" {
  description = ""
  value       = module.consul.asg_name_servers
}

output "aws_region" {
  description = ""
  value       = module.consul.aws_region
}

output "consul_servers_cluster_tag_key" {
  description = ""
  value       = module.consul.consul_servers_cluster_tag_key
}

output "consul_servers_cluster_tag_value" {
  description = ""
  value       = module.consul.consul_servers_cluster_tag_value
}

output "iam_role_arn_clients" {
  description = ""
  value       = module.consul.iam_role_arn_clients
}

output "iam_role_arn_servers" {
  description = ""
  value       = module.consul.iam_role_arn_servers
}

output "iam_role_id_clients" {
  description = ""
  value       = module.consul.iam_role_id_clients
}

output "iam_role_id_servers" {
  description = ""
  value       = module.consul.iam_role_id_servers
}

output "launch_config_name_clients" {
  description = ""
  value       = module.consul.launch_config_name_clients
}

output "launch_config_name_servers" {
  description = ""
  value       = module.consul.launch_config_name_servers
}

output "num_clients" {
  description = ""
  value       = module.consul.num_clients
}

output "num_servers" {
  description = ""
  value       = module.consul.num_servers
}

output "security_group_id_clients" {
  description = ""
  value       = module.consul.security_group_id_clients
}

output "security_group_id_servers" {
  description = ""
  value       = module.consul.security_group_id_servers
}


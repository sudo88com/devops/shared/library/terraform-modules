
variable "address_space" {
  type        = string
  description = "The supernet for the resources that will be created"
  default     = "10.0.0.0/16"
}

variable "allowed_inbound_cidr_blocks" {
  type        = list
  description = "A list of CIDR-formatted IP address ranges from which the Azure Instances will allow connections to Consul"
  default     = ""
}

variable "allowed_ssh_cidr_blocks" {
  type        = list
  description = "A list of CIDR-formatted IP address ranges from which the Azure Instances will allow SSH connections"
  default     = []
}

variable "client_id" {
  type        = string
  description = "The Azure client ID"
  default     = ""
}

variable "cluster_name" {
  type        = string
  description = "What to name the Consul cluster and all of its associated resources"
  default     = "consul-example"
}

variable "cluster_tag_key" {
  type        = string
  description = "The tag the Azure Instances will look for to automatically discover each other and form a cluster."
  default     = "consul-servers"
}

variable "image_uri" {
  type        = string
  description = "The URI to the Azure image that should be deployed to the consul cluster."
  default     = ""
}

variable "instance_size" {
  type        = string
  description = "The instance size for the servers"
  default     = "Standard_A0"
}

variable "key_data" {
  type        = string
  description = "The SSH public key that will be added to SSH authorized_users on the consul instances"
  default     = ""
}

variable "location" {
  type        = string
  description = "The Azure region the consul cluster will be deployed in"
  default     = "East US"
}

variable "num_clients" {
  type        = string
  description = "The number of Consul client nodes to deploy. You typically run the Consul client alongside your apps, so set this value to however many Instances make sense for your app code."
  default     = 1
}

variable "num_servers" {
  type        = string
  description = "The number of Consul server nodes to deploy. We strongly recommend using 3 or 5."
  default     = 3
}

variable "resource_group_name" {
  type        = string
  description = "The name of the Azure resource group consul will be deployed into. This RG should already exist"
  default     = ""
}

variable "secret_access_key" {
  type        = string
  description = "The Azure secret access key"
  default     = ""
}

variable "storage_account_name" {
  type        = string
  description = "The name of an Azure Storage Account. This SA should already exist"
  default     = ""
}

variable "subnet_address" {
  type        = string
  description = "The subnet that consul resources will be deployed into"
  default     = "10.0.10.0/24"
}

variable "subscription_id" {
  type        = string
  description = "The Azure subscription ID"
  default     = ""
}

variable "tenant_id" {
  type        = string
  description = "The Azure tenant ID"
  default     = ""
}


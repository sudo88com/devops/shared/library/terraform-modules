
output "load_balancer_ip_address_clients" {
  description = ""
  value       = module.consul.load_balancer_ip_address_clients
}

output "load_balancer_ip_address_servers" {
  description = ""
  value       = module.consul.load_balancer_ip_address_servers
}

output "num_servers" {
  description = ""
  value       = module.consul.num_servers
}

output "scale_set_name_servers" {
  description = ""
  value       = module.consul.scale_set_name_servers
}


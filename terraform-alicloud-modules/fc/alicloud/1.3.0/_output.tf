
output "this_events_function_id" {
  description = ""
  value       = module.fc.this_events_function_id
}

output "this_events_function_name" {
  description = ""
  value       = module.fc.this_events_function_name
}

output "this_events_trigger_ids" {
  description = ""
  value       = module.fc.this_events_trigger_ids
}

output "this_events_trigger_names" {
  description = ""
  value       = module.fc.this_events_trigger_names
}

output "this_http_function_id" {
  description = ""
  value       = module.fc.this_http_function_id
}

output "this_http_function_name" {
  description = ""
  value       = module.fc.this_http_function_name
}

output "this_http_trigger_ids" {
  description = ""
  value       = module.fc.this_http_trigger_ids
}

output "this_http_trigger_names" {
  description = ""
  value       = module.fc.this_http_trigger_names
}

output "this_service_id" {
  description = ""
  value       = module.fc.this_service_id
}

output "this_service_name" {
  description = ""
  value       = module.fc.this_service_name
}



create_event_function = false

create_http_function = false

create_service = true

events_function_filename = ""

events_function_handler = "index.handler"

events_function_name = "terraform-fc-events-function"

events_function_oss_bucket = ""

events_function_oss_key = ""

events_function_runtime = "nodejs6"

events_trigger_name = "terraform-events-trigger"

events_triggers = []

filter_service_with_name_regex = ""

function_memory_size = 128

function_timeout = 60

http_function_filename = ""

http_function_handler = "index.handler"

http_function_name = "terraform-fc-http-function"

http_function_oss_bucket = ""

http_function_oss_key = ""

http_function_runtime = "nodejs6"

http_trigger_name = "terraform-http-trigger"

http_triggers = []

profile = ""

region = ""

service_internet_access = true

service_log_config = []

service_name = "terraform-fc-service"

service_role = ""

service_role_name_regex = ""

service_role_policy_name = ""

service_role_policy_type = ""

service_vpc_config = []

shared_credentials_file = ""

skip_region_validation = false

source_role_name_regex = ""

source_role_policy_name = ""

source_role_policy_type = ""

trigger_role = ""

trigger_role_name_regex = ""

trigger_role_policy_name = ""

trigger_role_policy_type = ""

trigger_source_arn = ""



locals {
  access_group_description = var.access_group_description
  access_group_name = var.access_group_name
  access_group_type = var.access_group_type
  access_rule_priority = var.access_rule_priority
  create_access_group = var.create_access_group
  create_access_rule = var.create_access_rule
  create_file_system = var.create_file_system
  create_mount_target = var.create_mount_target
  file_system_description = var.file_system_description
  file_system_id = var.file_system_id
  file_system_protocol_type = var.file_system_protocol_type
  file_system_storage_type = var.file_system_storage_type
  file_system_type = var.file_system_type
  region = var.region
  rw_access_type = var.rw_access_type
  source_cidr_ip = var.source_cidr_ip
  user_access_type = var.user_access_type
  vswitch_id = var.vswitch_id
}

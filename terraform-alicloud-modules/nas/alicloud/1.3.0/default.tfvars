
access_group_description = ""

access_group_name = ""

access_group_type = "Vpc"

access_rule_priority = 1

create_access_group = false

create_access_rule = false

create_file_system = false

create_mount_target = false

file_system_description = ""

file_system_id = ""

file_system_protocol_type = "NFS"

file_system_storage_type = "Performance"

file_system_type = "standard"

region = ""

rw_access_type = "RDWR"

source_cidr_ip = ""

user_access_type = "no_squash"

vswitch_id = ""


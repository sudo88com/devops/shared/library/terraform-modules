
output "this_access_group_name" {
  description = ""
  value       = module.nas.this_access_group_name
}

output "this_access_group_type" {
  description = ""
  value       = module.nas.this_access_group_type
}

output "this_access_rule_id" {
  description = ""
  value       = module.nas.this_access_rule_id
}

output "this_access_rule_priority" {
  description = ""
  value       = module.nas.this_access_rule_priority
}

output "this_file_system_id" {
  description = ""
  value       = module.nas.this_file_system_id
}

output "this_mount_target_domain" {
  description = ""
  value       = module.nas.this_mount_target_domain
}

output "this_protocol_type" {
  description = ""
  value       = module.nas.this_protocol_type
}

output "this_rw_access_type" {
  description = ""
  value       = module.nas.this_rw_access_type
}

output "this_source_cidr_ip" {
  description = ""
  value       = module.nas.this_source_cidr_ip
}

output "this_storage_type" {
  description = ""
  value       = module.nas.this_storage_type
}

output "this_user_access_type" {
  description = ""
  value       = module.nas.this_user_access_type
}



variable "access_group_description" {
  type        = string
  description = "The description of nas access group."
  default     = ""
}

variable "access_group_name" {
  type        = string
  description = "The name of nas access group."
  default     = ""
}

variable "access_group_type" {
  type        = string
  description = "The type of nas access group."
  default     = "Vpc"
}

variable "access_rule_priority" {
  type        = number
  description = "The priority of access rule."
  default     = 1
}

variable "create_access_group" {
  type        = bool
  description = "Determine whether a permission group exists of access group."
  default     = false
}

variable "create_access_rule" {
  type        = bool
  description = "Judging whether permission rules exist of access rule."
  default     = false
}

variable "create_file_system" {
  type        = bool
  description = "Judging whether file system exist of file sytem."
  default     = false
}

variable "create_mount_target" {
  type        = bool
  description = "Judging whether mount target exist of mount target."
  default     = false
}

variable "file_system_description" {
  type        = string
  description = "The description of nas file system."
  default     = ""
}

variable "file_system_id" {
  type        = string
  description = "The ID of the file system."
  default     = ""
}

variable "file_system_protocol_type" {
  type        = string
  description = "The protocol_type of file system."
  default     = "NFS"
}

variable "file_system_storage_type" {
  type        = string
  description = "The storage_type of file system."
  default     = "Performance"
}

variable "file_system_type" {
  type        = string
  description = "The type of the file system."
  default     = "standard"
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "rw_access_type" {
  type        = string
  description = "The rw_access_type of access rule."
  default     = "RDWR"
}

variable "source_cidr_ip" {
  type        = string
  description = "The source_cidr_ip of an existing access rule."
  default     = ""
}

variable "user_access_type" {
  type        = string
  description = "The user_access_type of access rule."
  default     = "no_squash"
}

variable "vswitch_id" {
  type        = string
  description = "The ID of the VSwitch in the VPC where the mount target resides."
  default     = ""
}


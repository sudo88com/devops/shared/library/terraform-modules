
module "tensorflow" {
  source = "terraform-aws-modules/tensorflow/aws"
  version = "1.1.0"
  data_disks = var.data_disks
  deletion_protection = var.deletion_protection
  image_id = var.image_id
  instance_name = var.instance_name
  instance_password = var.instance_password
  instance_type = var.instance_type
  internet_charge_type = var.internet_charge_type
  internet_max_bandwidth_out = var.internet_max_bandwidth_out
  private_ip = var.private_ip
  profile = var.profile
  region = var.region
  resource_group_id = var.resource_group_id
  security_group_ids = var.security_group_ids
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  system_disk_category = var.system_disk_category
  system_disk_size = var.system_disk_size
  tags = var.tags
  volume_tags = var.volume_tags
  vswitch_id = var.vswitch_id
}

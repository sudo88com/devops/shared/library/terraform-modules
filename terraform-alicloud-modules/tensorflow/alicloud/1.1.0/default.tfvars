
data_disks = []

deletion_protection = false

image_id = ""

instance_name = "TF-Tensorflow"

instance_password = ""

instance_type = "ecs.sn1ne.large"

internet_charge_type = "PayByTraffic"

internet_max_bandwidth_out = 10

private_ip = ""

profile = ""

region = ""

resource_group_id = ""

security_group_ids = []

shared_credentials_file = ""

skip_region_validation = false

system_disk_category = "cloud_ssd"

system_disk_size = 40

tags = {}

volume_tags = {}

vswitch_id = ""


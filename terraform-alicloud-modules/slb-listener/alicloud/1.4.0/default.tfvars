
advanced_setting = {}

cookie = ""

cookie_timeout = 86400

create = true

enable_gzip = false

enable_health_check = false

enable_sticky_session = false

health_check = {}

health_check_connect_port = ""

health_check_domain = ""

health_check_http_code = "http_2xx"

health_check_interval = 2

health_check_timeout = 5

health_check_type = "tcp"

health_check_uri = ""

healthy_threshold = 3

listeners = []

persistence_timeout = 0

profile = ""

region = ""

retrive_slb_id = false

retrive_slb_ip = false

retrive_slb_proto = false

shared_credentials_file = ""

skip_region_validation = false

slb = ""

ssl_certificates = {}

sticky_session_type = "server"

unhealthy_threshold = 3

x_forwarded_for = {}


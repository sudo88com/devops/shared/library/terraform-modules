
variable "advanced_setting" {
  type        = map(string)
  description = "The slb listener advanced settings to use on listeners. It's supports fields 'sticky_session', 'sticky_session_type', 'cookie', 'cookie_timeout', 'gzip', 'persistence_timeout', 'acl_status', 'acl_type', 'acl_id', 'idle_timeout' and 'request_timeout'."
  default     = {}
}

variable "cookie" {
  type        = string
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = ""
}

variable "cookie_timeout" {
  type        = number
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = 86400
}

variable "create" {
  type        = bool
  description = "Whether to create load balancer listeners."
  default     = true
}

variable "enable_gzip" {
  type        = bool
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = false
}

variable "enable_health_check" {
  type        = bool
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = false
}

variable "enable_sticky_session" {
  type        = bool
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = false
}

variable "health_check" {
  type        = map(string)
  description = "The slb listener health check settings to use on listeners. It's supports fields 'healthy_threshold','unhealthy_threshold','health_check_timeout', 'health_check', 'health_check_type', 'health_check_connect_port', 'health_check_domain', 'health_check_uri', 'health_check_http_code', 'health_check_method' and 'health_check_interval'"
  default     = {}
}

variable "health_check_connect_port" {
  type        = string
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = ""
}

variable "health_check_domain" {
  type        = string
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = ""
}

variable "health_check_http_code" {
  type        = string
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = "http_2xx"
}

variable "health_check_interval" {
  type        = number
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'health_check' instead."
  default     = 2
}

variable "health_check_timeout" {
  type        = number
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'health_check' instead."
  default     = 5
}

variable "health_check_type" {
  type        = string
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = "tcp"
}

variable "health_check_uri" {
  type        = string
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = ""
}

variable "healthy_threshold" {
  type        = number
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'health_check' instead."
  default     = 3
}

variable "listeners" {
  type        = list(map(string))
  description = "List of slb listeners. Each item can set all or part fields of alicloud_slb_listener resource."
  default     = []
}

variable "persistence_timeout" {
  type        = number
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = 0
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.3.0)The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.3.0)The region used to launch this module resources."
  default     = ""
}

variable "retrive_slb_id" {
  type        = bool
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = false
}

variable "retrive_slb_ip" {
  type        = bool
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = false
}

variable "retrive_slb_proto" {
  type        = bool
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = false
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.3.0)This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.3.0)Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "slb" {
  type        = string
  description = "The load balancer ID used to add one or more listeners."
  default     = ""
}

variable "ssl_certificates" {
  type        = map(string)
  description = "SLB Server certificate settings to use on listeners. It's supports fields 'tls_cipher_policy', 'server_certificate_id' and 'enable_http2'"
  default     = {}
}

variable "sticky_session_type" {
  type        = string
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'advance_setting' instead."
  default     = "server"
}

variable "unhealthy_threshold" {
  type        = number
  description = "(Deprecated) It has been deprecated from 1.2.0, use 'listeners' and 'health_check' instead."
  default     = 3
}

variable "x_forwarded_for" {
  type        = map(bool)
  description = "Additional HTTP Header field 'X-Forwarded-For' to use on listeners. It's supports fields 'retrive_slb_ip', 'retrive_slb_id' and 'retrive_slb_proto'"
  default     = {}
}



variable "client_cert" {
  type        = string
  description = "The path of client certificate, like `~/.kube/client-cert.pem`."
  default     = ""
}

variable "client_key" {
  type        = string
  description = "The path of client key, like `~/.kube/client-key.pem`."
  default     = ""
}

variable "cluster_ca_cert" {
  type        = string
  description = "The path of cluster ca certificate, like `~/.kube/cluster-ca-cert.pem`"
  default     = ""
}

variable "cpu_core_count" {
  type        = number
  description = "CPU core count is used to fetch instance types."
  default     = 1
}

variable "create" {
  type        = bool
  description = "Whether to create serverless kubernetes cluster."
  default     = true
}

variable "deletion_protection" {
  type        = bool
  description = "Whether enable the deletion protection or not."
  default     = false
}

variable "endpoint_public_access_enabled" {
  type        = bool
  description = "Whether to create internet eip for API Server."
  default     = true
}

variable "force_update" {
  type        = bool
  description = "Then you want to change `vpc_id` and `vswitch_id`, you have to set this field to true, then the cluster will be recreated."
  default     = false
}

variable "kube_config" {
  type        = string
  description = "The path of kube config, like `~/.kube/config`."
  default     = ""
}

variable "memory_size" {
  type        = number
  description = "Memory size used to fetch instance types."
  default     = 2
}

variable "new_nat_gateway" {
  type        = bool
  description = "Whether to create a new nat gateway. In this template, a new nat gateway will create a nat gateway, eip and server snat entries."
  default     = true
}

variable "private_zone" {
  type        = bool
  description = "Whether to create internet eip for API Server."
  default     = false
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "serverless_cluster_name" {
  type        = string
  description = "The cluster name used to create a new cluster"
  default     = "tf-serverless-demo-0001"
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "tags" {
  type        = map(string)
  description = "A map of tags assigned to the kubernetes cluster."
  default     = {}
}

variable "vpc_id" {
  type        = string
  description = "An existing vpc id."
  default     = ""
}

variable "vswitch_id" {
  type        = string
  description = "An existing vswitch id."
  default     = ""
}


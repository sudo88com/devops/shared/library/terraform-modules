
client_cert = ""

client_key = ""

cluster_ca_cert = ""

cpu_core_count = 1

create = true

deletion_protection = false

endpoint_public_access_enabled = true

force_update = false

kube_config = ""

memory_size = 2

new_nat_gateway = true

private_zone = false

profile = ""

region = ""

serverless_cluster_name = "tf-serverless-demo-0001"

shared_credentials_file = ""

skip_region_validation = false

tags = {}

vpc_id = ""

vswitch_id = ""


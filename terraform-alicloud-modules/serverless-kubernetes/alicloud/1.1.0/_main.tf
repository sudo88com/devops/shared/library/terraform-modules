
module "serverless-kubernetes" {
  source = "terraform-aws-modules/serverless-kubernetes/aws"
  version = "1.1.0"
  client_cert = var.client_cert
  client_key = var.client_key
  cluster_ca_cert = var.cluster_ca_cert
  cpu_core_count = var.cpu_core_count
  create = var.create
  deletion_protection = var.deletion_protection
  endpoint_public_access_enabled = var.endpoint_public_access_enabled
  force_update = var.force_update
  kube_config = var.kube_config
  memory_size = var.memory_size
  new_nat_gateway = var.new_nat_gateway
  private_zone = var.private_zone
  profile = var.profile
  region = var.region
  serverless_cluster_name = var.serverless_cluster_name
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  tags = var.tags
  vpc_id = var.vpc_id
  vswitch_id = var.vswitch_id
}

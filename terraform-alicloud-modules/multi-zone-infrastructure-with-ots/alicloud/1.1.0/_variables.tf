
variable "availability_zones" {
  type        = list(string)
  description = "List available zones to launch several VSwitches and other resources. If not set, a list zones will be fetched and returned by data source."
  default     = []
}

variable "create_eip" {
  type        = bool
  description = "Whether to create new EIP and bind it to this Nat gateway."
  default     = true
}

variable "create_nat" {
  type        = bool
  description = "Whether to create nat gateway."
  default     = true
}

variable "create_ots_instance" {
  type        = bool
  description = "Whether to create ots instance. if true, a new ots instance will be created."
  default     = true
}

variable "create_ots_table" {
  type        = bool
  description = "Whether to create ots table. If true, a new ots table will be created"
  default     = false
}

variable "create_rds_account" {
  type        = bool
  description = "Whether to create a new account. If true, the 'rds_account_name' should not be empty."
  default     = true
}

variable "create_rds_database" {
  type        = bool
  description = "Whether to create multiple databases. If true, the `databases` should not be empty."
  default     = true
}

variable "create_rds_instance" {
  type        = bool
  description = "Whether to create security group."
  default     = true
}

variable "create_security_group" {
  type        = bool
  description = "Whether to create security group."
  default     = true
}

variable "create_slb" {
  type        = bool
  description = "Whether to create load balancer instance."
  default     = true
}

variable "create_vpc" {
  type        = bool
  description = "Whether to create vpc. If false, you can specify an existing vpc by setting 'use_existing_vpc=true' and 'existing_vpc_id'."
  default     = true
}

variable "default_egress_priority" {
  type        = number
  description = "A default egress priority."
  default     = 50
}

variable "default_ingress_priority" {
  type        = number
  description = "A default ingress priority."
  default     = 50
}

variable "ecs_credit_specification" {
  type        = string
  description = "Performance mode of the t5 burstable instance. Valid values: 'Standard', 'Unlimited'."
  default     = ""
}

variable "ecs_data_disks" {
  type        = list(map(string))
  description = "Additional data disks to attach to the scaled ECS instance"
  default     = []
}

variable "ecs_deletion_protection" {
  type        = bool
  description = "Whether enable the deletion protection or not. 'true': Enable deletion protection. 'false': Disable deletion protection."
  default     = false
}

variable "ecs_host_name" {
  type        = string
  description = "Host name used on all instances as prefix. Like if the value is TF-ECS-Host-Name and then the final host name would be TF-ECS-Host-Name001, TF-ECS-Host-Name002 and so on."
  default     = ""
}

variable "ecs_image_id" {
  type        = string
  description = "The image id used to launch one or more ecs instances."
  default     = "ubuntu_18_04_x64_20G_alibase_20200220.vhd"
}

variable "ecs_instance_charge_type" {
  type        = string
  description = "The charge type of instance. Choices are 'PostPaid' and 'PrePaid'."
  default     = "PostPaid"
}

variable "ecs_instance_name" {
  type        = string
  description = "Name to be used on all resources as prefix. Default to 'tf-multi-zone-infrastructure-with-ots'. The final default name would be tf-multi-zone-infrastructure-with-ots001, tf-multi-zone-infrastructure-with-ots002 and so on."
  default     = "tf-multi-zone-infrastructure-with-ots"
}

variable "ecs_instance_type" {
  type        = string
  description = "The instance type used to launch one or more ecs instances."
  default     = "ecs.sn1ne.large"
}

variable "ecs_key_name" {
  type        = string
  description = "The name of SSH key pair that can login ECS instance successfully without password. If it is specified, the password would be invalid."
  default     = ""
}

variable "ecs_password" {
  type        = string
  description = "The password of instance."
  default     = ""
}

variable "ecs_role_name" {
  type        = string
  description = "Instance RAM role name. The name is provided and maintained by RAM. You can use `alicloud_ram_role` to create a new one."
  default     = ""
}

variable "ecs_security_enhancement_strategy" {
  type        = string
  description = "The security enhancement strategy."
  default     = "Active"
}

variable "ecs_spot_price_limit" {
  type        = number
  description = "The hourly price threshold of a instance, and it takes effect only when parameter 'spot_strategy' is 'SpotWithPriceLimit'. Three decimals is allowed at most."
  default     = 0
}

variable "ecs_spot_strategy" {
  type        = string
  description = "The spot strategy of a Pay-As-You-Go instance, and it takes effect only when parameter `instance_charge_type` is 'PostPaid'. Value range: 'NoSpot': A regular Pay-As-You-Go instance. 'SpotWithPriceLimit': A price threshold for a spot instance. 'SpotAsPriceGo': A price that is based on the highest Pay-As-You-Go instance"
  default     = "NoSpot"
}

variable "ecs_subscription" {
  type        = map(string)
  description = "A mapping of fields for Prepaid ECS instances created. "
  default     = {
  "auto_renew_period": 1,
  "include_data_disks": true,
  "period": 1,
  "period_unit": "Month",
  "renewal_status": "Normal"
}
}

variable "ecs_system_disk_category" {
  type        = string
  description = "The system disk category used to launch one or more ecs instances."
  default     = "cloud_efficiency"
}

variable "ecs_system_disk_size" {
  type        = number
  description = "The system disk size used to launch one or more ecs instances."
  default     = 40
}

variable "ecs_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the ecs instances."
  default     = {}
}

variable "ecs_user_data" {
  type        = string
  description = "User data to pass to instance on boot"
  default     = ""
}

variable "ecs_volume_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the devices created by the instance at launch time."
  default     = {}
}

variable "egress_cidr_blocks" {
  type        = list(string)
  description = "The IPv4 CIDR ranges list to use on egress cidrs rules."
  default     = []
}

variable "egress_ports" {
  type        = list(number)
  description = "The port list used on 'egress_with_cidr_blocks_and_ports' ports rules."
  default     = []
}

variable "egress_rules" {
  type        = list(string)
  description = "List of egress rules to create by name."
  default     = []
}

variable "egress_with_cidr_blocks" {
  type        = list(map(string))
  description = "List of egress rules to create where 'cidr_blocks' is used. The valid keys contains 'cidr_blocks', 'from_port', 'to_port', 'protocol', 'description' and 'priority'."
  default     = []
}

variable "egress_with_cidr_blocks_and_ports" {
  type        = list(map(string))
  description = "List of egress rules to create where 'cidr_blocks' and 'ports' is used. The valid keys contains 'cidr_blocks', 'ports', 'protocol', 'description' and 'priority'. The ports item's 'from' and 'to' have the same port. Example: '80,443' means 80/80 and 443/443."
  default     = []
}

variable "egress_with_source_security_group_id" {
  type        = list(map(string))
  description = "List of egress rules to create where 'source_security_group_id' is used."
  default     = []
}

variable "eip_bandwidth" {
  type        = number
  description = "Maximum bandwidth to the elastic public network, measured in Mbps (Mega bit per second)."
  default     = 5
}

variable "eip_instance_charge_type" {
  type        = string
  description = "Elastic IP instance charge type."
  default     = "PostPaid"
}

variable "eip_internet_charge_type" {
  type        = string
  description = "Internet charge type of the EIP, Valid values are `PayByBandwidth`, `PayByTraffic`. "
  default     = "PayByTraffic"
}

variable "eip_name" {
  type        = string
  description = "Name to be used on all eip as prefix."
  default     = "tf-multi-zone-infrastructure-with-ots"
}

variable "eip_period" {
  type        = number
  description = "The duration that you will buy the EIP, in month."
  default     = 1
}

variable "eip_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the EIP instance resource."
  default     = {}
}

variable "existing_vpc_id" {
  type        = string
  description = "The vpc id used to launch several vswitches and other resources."
  default     = ""
}

variable "ingress_cidr_blocks" {
  type        = list(string)
  description = "The IPv4 CIDR ranges list to use on ingress cidrs rules."
  default     = []
}

variable "ingress_ports" {
  type        = list(number)
  description = "The port list used on 'ingress_with_cidr_blocks_and_ports' ports rules."
  default     = []
}

variable "ingress_rules" {
  type        = list(string)
  description = "List of ingress rules to create by name."
  default     = []
}

variable "ingress_with_cidr_blocks" {
  type        = list(map(string))
  description = "List of ingress rules to create where 'cidr_blocks' is used. The valid keys contains 'cidr_blocks', 'from_port', 'to_port', 'protocol', 'description', 'priority' and 'rule'."
  default     = []
}

variable "ingress_with_cidr_blocks_and_ports" {
  type        = list(map(string))
  description = "List of ingress rules to create where 'cidr_blocks' and 'ports' is used. The valid keys contains 'cidr_blocks', 'ports', 'protocol', 'description' and 'priority'. The ports item's 'from' and 'to' have the same port. Example: '80,443' means 80/80 and 443/443."
  default     = []
}

variable "ingress_with_source_security_group_id" {
  type        = list(map(string))
  description = "List of ingress rules to create where `source_security_group_id` is used."
  default     = []
}

variable "nat_instance_charge_type" {
  type        = string
  description = "The charge type of the nat gateway. Choices are 'PostPaid' and 'PrePaid'."
  default     = "PostPaid"
}

variable "nat_name" {
  type        = string
  description = "Name of a new nat gateway."
  default     = "tf-multi-zone-infrastructure-with-ots"
}

variable "nat_period" {
  type        = number
  description = "The charge duration of the PrePaid nat gateway, in month."
  default     = 1
}

variable "nat_specification" {
  type        = string
  description = "The specification of nat gateway."
  default     = "Small"
}

variable "number_of_eip" {
  type        = number
  description = "Number of EIP instance used to bind with this Nat gateway."
  default     = 2
}

variable "number_of_zone_a_instances" {
  type        = number
  description = "The number of instances to be created."
  default     = 2
}

variable "number_of_zone_b_instances" {
  type        = number
  description = "The number of instances to be created."
  default     = 2
}

variable "ots_accessed_by" {
  type        = string
  description = "The network limitation of accessing instance. Valid values:[\"Any\", \"Vpc\", \"ConsoleOrVpc\"]."
  default     = "Any"
}

variable "ots_instance_bind_vpc" {
  type        = bool
  description = "Whether to create ots instance attachment. If true, the ots instance will be attached with vpc and vswitch."
  default     = true
}

variable "ots_instance_name" {
  type        = string
  description = "The name of the instance."
  default     = "tf-created-ots"
}

variable "ots_instance_type" {
  type        = string
  description = "The type of instance. Valid values: [\"Capacity\", \"HighPerformance\"]."
  default     = "HighPerformance"
}

variable "ots_table_max_version" {
  type        = number
  description = "The maximum number of versions stored in this table. The valid value is 1-2147483647."
  default     = 1
}

variable "ots_table_name" {
  type        = string
  description = "(Required, ForceNew) The table name of the OTS instance. If changed, a new table would be created."
  default     = ""
}

variable "ots_table_primary_key" {
  type        = list(object({
    name = string
    type = string
  }))
  description = "The property of TableMeta which indicates the structure information of a table. It describes the attribute value of primary key. The number of primary_key should not be less than one and not be more than four."
  default     = []
}

variable "ots_table_time_to_live" {
  type        = number
  description = "The retention time of data stored in this table (unit: second). The value maximum is 2147483647 and -1 means never expired."
  default     = -1
}

variable "ots_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the instance."
  default     = {}
}

variable "priority_for_egress_rules" {
  type        = number
  description = "A priority where 'egress_rules' is used. Default to 'default_egress_priority'."
  default     = 1
}

variable "priority_for_ingress_rules" {
  type        = number
  description = "A priority is used when setting 'ingress_rules'. Default to 'default_ingress_priority'."
  default     = 1
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "rds_account_name" {
  type        = string
  description = "Name of a new database account. It should be set when create_rds_account = true."
  default     = ""
}

variable "rds_account_privilege" {
  type        = string
  description = "The privilege of one account access database."
  default     = "ReadOnly"
}

variable "rds_account_type" {
  type        = string
  description = "Privilege type of account. Normal: Common privilege. Super: High privilege.Default to Normal."
  default     = "Normal"
}

variable "rds_allocate_public_connection" {
  type        = bool
  description = "Whether to allocate public connection for a RDS instance. If true, the connection_prefix can not be empty."
  default     = false
}

variable "rds_backup_retention_period" {
  type        = number
  description = "Instance backup retention days. Valid values: [7-730]. Default to 7."
  default     = 7
}

variable "rds_connection_port" {
  type        = number
  description = " Internet connection port. Valid value: [3001-3999]. Default to 3306."
  default     = 3306
}

variable "rds_connection_prefix" {
  type        = string
  description = "Prefix of an Internet connection string."
  default     = ""
}

variable "rds_databases" {
  type        = list(map(string))
  description = "A list mapping used to add multiple databases. Each item supports keys: name, character_set and description. It should be set when create_database = true."
  default     = []
}

variable "rds_enable_backup_log" {
  type        = bool
  description = "Whether to backup instance log. Default to true."
  default     = true
}

variable "rds_engine" {
  type        = string
  description = "RDS Database type. Value options: MySQL, SQLServer, PostgreSQL, and PPAS"
  default     = "MySQL"
}

variable "rds_engine_version" {
  type        = string
  description = "RDS Database version. Value options can refer to the latest docs [CreateDBInstance](https://www.alibabacloud.com/help/doc-detail/26228.htm) `EngineVersion`"
  default     = "5.7"
}

variable "rds_instance_charge_type" {
  type        = string
  description = "The instance charge type. Valid values: Prepaid and Postpaid. Default to Postpaid."
  default     = "Postpaid"
}

variable "rds_instance_name" {
  type        = string
  description = "The name of DB instance."
  default     = "tf-multi-zone-infrastructure-with-ots"
}

variable "rds_instance_storage" {
  type        = number
  description = "The storage capacity of the instance. Unit: GB. The storage capacity increases at increments of 5 GB. For more information, see [Instance Types](https://www.alibabacloud.com/help/doc-detail/26312.htm)."
  default     = 20
}

variable "rds_instance_type" {
  type        = string
  description = "DB Instance type, for example: mysql.n1.micro.1. full list is : https://www.alibabacloud.com/help/zh/doc-detail/26312.htm"
  default     = ""
}

variable "rds_log_backup_retention_period" {
  type        = number
  description = "Instance log backup retention days. Valid values: [7-730]. Default to 7. It can be larger than 'retention_period'."
  default     = 7
}

variable "rds_password" {
  type        = string
  description = "Operation database account password. It may consist of letters, digits, or underlines, with a length of 6 to 32 characters."
  default     = ""
}

variable "rds_period" {
  type        = number
  description = "The duration that you will buy DB instance (in month). It is valid when instance_charge_type is PrePaid. Valid values: [1~9], 12, 24, 36. Default to 1"
  default     = 1
}

variable "rds_preferred_backup_period" {
  type        = list(string)
  description = "DB Instance backup period."
  default     = []
}

variable "rds_preferred_backup_time" {
  type        = string
  description = " DB instance backup time, in the format of HH:mmZ- HH:mmZ. "
  default     = "02:00Z-03:00Z"
}

variable "rds_security_group_ids" {
  type        = list(string)
  description = "List of VPC security group ids to associate with rds instance."
  default     = []
}

variable "rds_security_ips" {
  type        = list(string)
  description = " List of IP addresses allowed to access all databases of an instance. The list contains up to 1,000 IP addresses, separated by commas. Supported formats include 0.0.0.0/0, 10.23.12.24 (IP), and 10.23.12.24/24 (Classless Inter-Domain Routing (CIDR) mode. /24 represents the length of the prefix in an IP address. The range of the prefix length is [1,32])."
  default     = [
  "127.0.0.1"
]
}

variable "rds_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the rds instances."
  default     = {}
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "resource_group_id" {
  type        = string
  description = "The Id of resource group which the instance belongs."
  default     = ""
}

variable "security_group_name" {
  type        = string
  description = "Name of security group. It is used to create a new security group."
  default     = "tf-multi-zone-infrastructure-with-ots"
}

variable "security_group_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to security group."
  default     = {}
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "slb_bandwidth" {
  type        = number
  description = "The load balancer instance bandwidth."
  default     = 10
}

variable "slb_internet_charge_type" {
  type        = string
  description = "The charge type of load balancer instance internet network."
  default     = "PayByTraffic"
}

variable "slb_name" {
  type        = string
  description = "The name of a new load balancer."
  default     = "tf-multi-zone-infrastructure-with-ots"
}

variable "slb_spec" {
  type        = string
  description = "The specification of the SLB instance."
  default     = ""
}

variable "slb_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the slb instances."
  default     = {}
}

variable "tags" {
  type        = map(string)
  description = "The common tags will apply to all of resources."
  default     = {}
}

variable "use_existing_vpc" {
  type        = bool
  description = "The vpc id used to launch several vswitches. If set, the 'create_vpc' will be ignored."
  default     = false
}

variable "vpc_cidr" {
  type        = string
  description = "The cidr block used to launch a new vpc."
  default     = "172.16.0.0/16"
}

variable "vpc_name" {
  type        = string
  description = "The vpc name used to launch a new vpc."
  default     = "tf-multi-zone-infrastructure-with-ots"
}

variable "vpc_tags" {
  type        = map(string)
  description = "The tags used to launch a new vpc."
  default     = {}
}

variable "vswitch_cidrs" {
  type        = list(string)
  description = "List of cidr blocks used to launch several new vswitches. If not set, there is no new vswitches will be created."
  default     = [
  "172.16.10.0/24",
  "172.16.20.0/24"
]
}

variable "vswitch_name" {
  type        = string
  description = "The vswitch name prefix used to launch several new vswitches."
  default     = "tf-multi-zone-infrastructure-with-ots"
}

variable "vswitch_tags" {
  type        = map(string)
  description = "The tags used to launch serveral vswitches."
  default     = {}
}



availability_zones = []

create_eip = true

create_nat = true

create_ots_instance = true

create_ots_table = false

create_rds_account = true

create_rds_database = true

create_rds_instance = true

create_security_group = true

create_slb = true

create_vpc = true

default_egress_priority = 50

default_ingress_priority = 50

ecs_credit_specification = ""

ecs_data_disks = []

ecs_deletion_protection = false

ecs_host_name = ""

ecs_image_id = "ubuntu_18_04_x64_20G_alibase_20200220.vhd"

ecs_instance_charge_type = "PostPaid"

ecs_instance_name = "tf-multi-zone-infrastructure-with-ots"

ecs_instance_type = "ecs.sn1ne.large"

ecs_key_name = ""

ecs_password = ""

ecs_role_name = ""

ecs_security_enhancement_strategy = "Active"

ecs_spot_price_limit = 0

ecs_spot_strategy = "NoSpot"

ecs_subscription = {
  "auto_renew_period": 1,
  "include_data_disks": true,
  "period": 1,
  "period_unit": "Month",
  "renewal_status": "Normal"
}

ecs_system_disk_category = "cloud_efficiency"

ecs_system_disk_size = 40

ecs_tags = {}

ecs_user_data = ""

ecs_volume_tags = {}

egress_cidr_blocks = []

egress_ports = []

egress_rules = []

egress_with_cidr_blocks = []

egress_with_cidr_blocks_and_ports = []

egress_with_source_security_group_id = []

eip_bandwidth = 5

eip_instance_charge_type = "PostPaid"

eip_internet_charge_type = "PayByTraffic"

eip_name = "tf-multi-zone-infrastructure-with-ots"

eip_period = 1

eip_tags = {}

existing_vpc_id = ""

ingress_cidr_blocks = []

ingress_ports = []

ingress_rules = []

ingress_with_cidr_blocks = []

ingress_with_cidr_blocks_and_ports = []

ingress_with_source_security_group_id = []

nat_instance_charge_type = "PostPaid"

nat_name = "tf-multi-zone-infrastructure-with-ots"

nat_period = 1

nat_specification = "Small"

number_of_eip = 2

number_of_zone_a_instances = 2

number_of_zone_b_instances = 2

ots_accessed_by = "Any"

ots_instance_bind_vpc = true

ots_instance_name = "tf-created-ots"

ots_instance_type = "HighPerformance"

ots_table_max_version = 1

ots_table_name = ""

ots_table_primary_key = []

ots_table_time_to_live = -1

ots_tags = {}

priority_for_egress_rules = 1

priority_for_ingress_rules = 1

profile = ""

rds_account_name = ""

rds_account_privilege = "ReadOnly"

rds_account_type = "Normal"

rds_allocate_public_connection = false

rds_backup_retention_period = 7

rds_connection_port = 3306

rds_connection_prefix = ""

rds_databases = []

rds_enable_backup_log = true

rds_engine = "MySQL"

rds_engine_version = "5.7"

rds_instance_charge_type = "Postpaid"

rds_instance_name = "tf-multi-zone-infrastructure-with-ots"

rds_instance_storage = 20

rds_instance_type = ""

rds_log_backup_retention_period = 7

rds_password = ""

rds_period = 1

rds_preferred_backup_period = []

rds_preferred_backup_time = "02:00Z-03:00Z"

rds_security_group_ids = []

rds_security_ips = [
  "127.0.0.1"
]

rds_tags = {}

region = ""

resource_group_id = ""

security_group_name = "tf-multi-zone-infrastructure-with-ots"

security_group_tags = {}

shared_credentials_file = ""

skip_region_validation = false

slb_bandwidth = 10

slb_internet_charge_type = "PayByTraffic"

slb_name = "tf-multi-zone-infrastructure-with-ots"

slb_spec = ""

slb_tags = {}

tags = {}

use_existing_vpc = false

vpc_cidr = "172.16.0.0/16"

vpc_name = "tf-multi-zone-infrastructure-with-ots"

vpc_tags = {}

vswitch_cidrs = [
  "172.16.10.0/24",
  "172.16.20.0/24"
]

vswitch_name = "tf-multi-zone-infrastructure-with-ots"

vswitch_tags = {}



address_type = "internet"

allocate_public_ip = false

bandwidth = 10

bind_domain = false

create_instance = true

create_slb = false

data_disks = []

deletion_protection = false

dns_record = {}

domain_name = ""

ecs_instance_name = ""

ecs_instance_password = ""

ecs_instance_type = ""

existing_slb_id = ""

frontend_port = 80

image_id = ""

instance_port = 8080

instance_weight_in_server_group = ""

internet_charge_type = "PayByTraffic"

internet_max_bandwidth_out = 10

master_zone_id = ""

number_of_instance = 1

private_ip = ""

product_keyword = ""

product_suggested_price = 0

product_supplier_name_keyword = ""

profile = ""

region = ""

resource_group_id = ""

security_group_ids = []

shared_credentials_file = ""

skip_region_validation = false

slave_zone_id = ""

slb_internet_charge_type = "PayByTraffic"

slb_name = "tf-module-slb"

slb_spec = "slb.s1.small"

slb_tags = {}

system_disk_category = "cloud_ssd"

system_disk_size = 40

tags = {}

use_existing_slb = false

virtual_server_group_name = ""

volume_tags = {}

vswitch_id = ""


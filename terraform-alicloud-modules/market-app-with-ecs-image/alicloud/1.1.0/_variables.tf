
variable "address_type" {
  type        = string
  description = "The type if address. Choices are 'intranet' and 'internet'. Default to 'internet'."
  default     = "internet"
}

variable "allocate_public_ip" {
  type        = bool
  description = "Whether to allocate public ip for ECS instance. If 'create_slb' is true, it will be ignore."
  default     = false
}

variable "bandwidth" {
  type        = number
  description = "The load balancer instance bandwidth."
  default     = 10
}

variable "bind_domain" {
  type        = bool
  description = "Whether to bind domain."
  default     = false
}

variable "create_instance" {
  type        = bool
  description = "Whether to create ecs instance."
  default     = true
}

variable "create_slb" {
  type        = bool
  description = "Whether to create a balancer instance."
  default     = false
}

variable "data_disks" {
  type        = list(map(string))
  description = "Additional data disks to attach to the scaled ECS instance."
  default     = []
}

variable "deletion_protection" {
  type        = bool
  description = "Whether enable the deletion protection or not. 'true': Enable deletion protection. 'false': Disable deletion protection."
  default     = false
}

variable "dns_record" {
  type        = map(string)
  description = "DNS record. Each item can contains keys: 'host_record'(The host record of the domain record.),'type'(The type of the domain. Valid values: A, NS, MX, TXT, CNAME, SRV, AAAA, CAA, REDIRECT_URL, FORWORD_URL. Default to A.),'priority'(The priority of domain record. Valid values are `[1-10]`. When the `type` is `MX`, this parameter is required.),'ttl'(The ttl of the domain record. Default to 600.),'line'(The resolution line of domain record. Default value is default.)."
  default     = {}
}

variable "domain_name" {
  type        = string
  description = "The name of domain."
  default     = ""
}

variable "ecs_instance_name" {
  type        = string
  description = "The name of ECS Instance."
  default     = ""
}

variable "ecs_instance_password" {
  type        = string
  description = "The password of ECS instance."
  default     = ""
}

variable "ecs_instance_type" {
  type        = string
  description = "The instance type used to launch ecs instance."
  default     = ""
}

variable "existing_slb_id" {
  type        = string
  description = "An existing load balancer instance id."
  default     = ""
}

variable "frontend_port" {
  type        = number
  description = "The fronted port of balancer."
  default     = 80
}

variable "image_id" {
  type        = string
  description = "The image id used to launch one ecs instance. If not set, a fetched market place image by product_keyword will be used."
  default     = ""
}

variable "instance_port" {
  type        = number
  description = "The port of instance."
  default     = 8080
}

variable "instance_weight_in_server_group" {
  type        = string
  description = "The weight of instance in server group."
  default     = ""
}

variable "internet_charge_type" {
  type        = string
  description = "The internet charge type of ECS instance. Choices are 'PayByTraffic' and 'PayByBandwidth'."
  default     = "PayByTraffic"
}

variable "internet_max_bandwidth_out" {
  type        = number
  description = "The maximum internet out bandwidth of ECS instance."
  default     = 10
}

variable "master_zone_id" {
  type        = string
  description = "The primary zone ID of the SLB instance. If not specified, the system will be randomly assigned."
  default     = ""
}

variable "number_of_instance" {
  type        = number
  description = "The number of ecs to be created."
  default     = 1
}

variable "private_ip" {
  type        = string
  description = "Configure ECS Instance private IP address"
  default     = ""
}

variable "product_keyword" {
  type        = string
  description = "The name keyword of Market Product used to fetch the specified product image."
  default     = ""
}

variable "product_suggested_price" {
  type        = number
  description = "The suggested price of Market Product used to fetch the specified product image."
  default     = 0
}

variable "product_supplier_name_keyword" {
  type        = string
  description = "The name keyword of Market Product supplier name used to fetch the specified product image."
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "resource_group_id" {
  type        = string
  description = "The Id of resource group which the ECS instance belongs."
  default     = ""
}

variable "security_group_ids" {
  type        = list(string)
  description = "A list of security group ids to associate with ECS Instance."
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "slave_zone_id" {
  type        = string
  description = "The standby zone ID of the SLB instance. If not specified, the system will be randomly assigned."
  default     = ""
}

variable "slb_internet_charge_type" {
  type        = string
  description = "The charge type of load balancer instance internet network."
  default     = "PayByTraffic"
}

variable "slb_name" {
  type        = string
  description = "The name of a new load balancer."
  default     = "tf-module-slb"
}

variable "slb_spec" {
  type        = string
  description = "The specification of the SLB instance."
  default     = "slb.s1.small"
}

variable "slb_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the resource."
  default     = {}
}

variable "system_disk_category" {
  type        = string
  description = "The system disk category used to launch one ecs instance."
  default     = "cloud_ssd"
}

variable "system_disk_size" {
  type        = number
  description = "The system disk size used to launch ecs instance."
  default     = 40
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the ECS and SLB."
  default     = {}
}

variable "use_existing_slb" {
  type        = bool
  description = "Whether to use an existing load balancer instance. If true, 'existing_slb_id' should not be empty. Also, you can create a new one by setting 'create = true'."
  default     = false
}

variable "virtual_server_group_name" {
  type        = string
  description = "The name virtual server group. If not set, the 'name' and adding suffix '-virtual' will return."
  default     = ""
}

variable "volume_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the devices created by the instance at launch time."
  default     = {}
}

variable "vswitch_id" {
  type        = string
  description = "The virtual switch ID to launch ECS instance in VPC."
  default     = ""
}


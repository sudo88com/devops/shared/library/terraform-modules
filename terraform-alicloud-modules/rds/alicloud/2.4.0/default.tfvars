
account_name = ""

allocate_public_connection = true

backup_period = []

backup_retention_period = 7

backup_time = "02:00Z-03:00Z"

character_set = ""

connection_prefix = ""

create_account = true

create_database = true

create_instance = true

databases = []

db_name = ""

db_names = []

enable_backup_log = true

engine = ""

engine_version = ""

existing_instance_id = ""

instance_charge_type = "Postpaid"

instance_id = ""

instance_name = ""

instance_storage = 20

instance_storage_type = "local_ssd"

instance_type = ""

log_backup_retention_period = 7

log_retention_period = 7

password = ""

period = 1

port = 3306

preferred_backup_period = []

preferred_backup_time = "02:00Z-03:00Z"

privilege = "ReadOnly"

profile = ""

region = ""

retention_period = 7

security_group_ids = []

security_ips = []

shared_credentials_file = ""

skip_region_validation = false

sql_collector_config_value = 30

sql_collector_status = "Disabled"

tags = {}

type = "Normal"

vpc_security_group_ids = []

vswitch_id = ""

zone_id = ""


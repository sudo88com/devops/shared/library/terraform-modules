
variable "account_name" {
  type        = string
  description = "Name of a new database account. It should be set when create_account = true."
  default     = ""
}

variable "allocate_public_connection" {
  type        = bool
  description = "Whether to allocate public connection for a RDS instance. If true, the connection_prefix can not be empty."
  default     = true
}

variable "backup_period" {
  type        = list(string)
  description = "`(Deprecated)` It has been deprecated from version 2.0.0 and use `preferred_backup_period` instead."
  default     = []
}

variable "backup_retention_period" {
  type        = number
  description = "Instance backup retention days. Valid values: [7-730]. Default to 7."
  default     = 7
}

variable "backup_time" {
  type        = string
  description = "`(Deprecated)` It has been deprecated from version 2.0.0 and use `preferred_backup_time` instead."
  default     = "02:00Z-03:00Z"
}

variable "character_set" {
  type        = string
  description = "`(Deprecated)` It has been deprecated from version 2.0.0 and use `databases` instead."
  default     = ""
}

variable "connection_prefix" {
  type        = string
  description = "Prefix of an Internet connection string. A random name prefixed with 'tf-rds-' will be set if it is empty."
  default     = ""
}

variable "create_account" {
  type        = bool
  description = "Whether to create a new account. If true, the `account_name` should not be empty."
  default     = true
}

variable "create_database" {
  type        = bool
  description = "Whether to create multiple databases. If true, the `databases` should not be empty."
  default     = true
}

variable "create_instance" {
  type        = bool
  description = "Whether to create security group. If false, you can use a existing RDS instance by setting `existing_instance_id`."
  default     = true
}

variable "databases" {
  type        = list(map(string))
  description = "A list mapping used to add multiple databases. Each item supports keys: name, character_set and description. It should be set when create_database = true."
  default     = []
}

variable "db_name" {
  type        = string
  description = "`(Deprecated)` It has been deprecated from version 2.0.0 and use `databases` instead."
  default     = ""
}

variable "db_names" {
  type        = list(string)
  description = "`(Deprecated)` It has been deprecated from version 2.0.0 and use `databases` instead."
  default     = []
}

variable "enable_backup_log" {
  type        = bool
  description = "Whether to backup instance log. Default to true."
  default     = true
}

variable "engine" {
  type        = string
  description = "RDS Database type. Value options: MySQL, SQLServer, PostgreSQL, and PPAS"
  default     = ""
}

variable "engine_version" {
  type        = string
  description = "RDS Database version. Value options can refer to the latest docs [CreateDBInstance](https://www.alibabacloud.com/help/doc-detail/26228.htm) `EngineVersion`"
  default     = ""
}

variable "existing_instance_id" {
  type        = string
  description = "The Id of an existing RDS instance. If set, the `create_instance` will be ignored."
  default     = ""
}

variable "instance_charge_type" {
  type        = string
  description = "The instance charge type. Valid values: Prepaid and Postpaid. Default to Postpaid."
  default     = "Postpaid"
}

variable "instance_id" {
  type        = string
  description = "`(Deprecated)` It has been deprecated from version 2.0.0 and use `existing_instance_id` instead."
  default     = ""
}

variable "instance_name" {
  type        = string
  description = "The name of DB instance. A random name prefixed with 'terraform-rds-' will be set if it is empty."
  default     = ""
}

variable "instance_storage" {
  type        = number
  description = "The storage capacity of the instance. Unit: GB. The storage capacity increases at increments of 5 GB. For more information, see [Instance Types](https://www.alibabacloud.com/help/doc-detail/26312.htm)."
  default     = 20
}

variable "instance_storage_type" {
  type        = string
  description = "The storage type of DB instance"
  default     = "local_ssd"
}

variable "instance_type" {
  type        = string
  description = "DB Instance type, for example: mysql.n1.micro.1. full list is : https://www.alibabacloud.com/help/zh/doc-detail/26312.htm"
  default     = ""
}

variable "log_backup_retention_period" {
  type        = number
  description = "Instance log backup retention days. Valid values: [7-730]. Default to 7. It can be larger than 'retention_period'."
  default     = 7
}

variable "log_retention_period" {
  type        = number
  description = "`(Deprecated)` It has been deprecated from version 2.0.0 and use `log_backup_retention_period` instead."
  default     = 7
}

variable "password" {
  type        = string
  description = "Operation database account password. It may consist of letters, digits, or underlines, with a length of 6 to 32 characters."
  default     = ""
}

variable "period" {
  type        = number
  description = "The duration that you will buy DB instance (in month). It is valid when instance_charge_type is PrePaid. Valid values: [1~9], 12, 24, 36. Default to 1"
  default     = 1
}

variable "port" {
  type        = number
  description = " Internet connection port. Valid value: [3001-3999]. Default to 3306."
  default     = 3306
}

variable "preferred_backup_period" {
  type        = list(string)
  description = "DB Instance backup period."
  default     = []
}

variable "preferred_backup_time" {
  type        = string
  description = " DB instance backup time, in the format of HH:mmZ- HH:mmZ. "
  default     = "02:00Z-03:00Z"
}

variable "privilege" {
  type        = string
  description = "The privilege of one account access database."
  default     = "ReadOnly"
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 2.4.0)The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 2.4.0)The region used to launch this module resources."
  default     = ""
}

variable "retention_period" {
  type        = number
  description = "`(Deprecated)` It has been deprecated from version 2.0.0 and use `backup_retention_period` instead."
  default     = 7
}

variable "security_group_ids" {
  type        = list(string)
  description = "List of VPC security group ids to associate with rds instance."
  default     = []
}

variable "security_ips" {
  type        = list(string)
  description = " List of IP addresses allowed to access all databases of an instance. The list contains up to 1,000 IP addresses, separated by commas. Supported formats include 0.0.0.0/0, 10.23.12.24 (IP), and 10.23.12.24/24 (Classless Inter-Domain Routing (CIDR) mode. /24 represents the length of the prefix in an IP address. The range of the prefix length is [1,32])."
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 2.4.0)This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 2.4.0)Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "sql_collector_config_value" {
  type        = number
  description = "The sql collector keep time of the instance. Valid values are `30`, `180`, `365`, `1095`, `1825`, Default to `30`."
  default     = 30
}

variable "sql_collector_status" {
  type        = string
  description = "The sql collector status of the instance. Valid values are `Enabled`, `Disabled`, Default to `Disabled`."
  default     = "Disabled"
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the rds."
  default     = {}
}

variable "type" {
  type        = string
  description = "Privilege type of account. Normal: Common privilege. Super: High privilege.Default to Normal."
  default     = "Normal"
}

variable "vpc_security_group_ids" {
  type        = list(string)
  description = "`(Deprecated)` It has been deprecated from version 2.0.0 and use `security_group_ids` instead."
  default     = []
}

variable "vswitch_id" {
  type        = string
  description = "The virtual switch ID to launch DB instances in one VPC."
  default     = ""
}

variable "zone_id" {
  type        = string
  description = "`(Deprecated)` It has been deprecated from version 2.0.0 ."
  default     = ""
}


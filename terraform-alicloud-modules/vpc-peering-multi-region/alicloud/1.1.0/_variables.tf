
variable "bandwidth" {
  type        = number
  description = "The bandwidth in Mbps of the bandwidth package. Cannot be less than 2Mbps."
  default     = 10
}

variable "bandwidth_limit" {
  type        = number
  description = " The bandwidth configured for the interconnected regions communication."
  default     = 4
}

variable "bandwidth_package_name" {
  type        = string
  description = "The name of the bandwidth package. Defaults to null."
  default     = ""
}

variable "banwidth_description" {
  type        = string
  description = "The description of the bandwidth package. Default to null."
  default     = "null"
}

variable "cen_description" {
  type        = string
  description = " The description of the CEN instance. Defaults to null. The description must be 2 to 256 characters in length. It must start with a letter, and cannot start with http:// or https://."
  default     = ""
}

variable "cen_name" {
  type        = string
  description = "The name of the CEN instance. Defaults to null. The name must be 2 to 128 characters in length and can contain letters, numbers, periods (.), underscores (_), and hyphens (-). The name must start with a letter, but cannot start with http:// or https://."
  default     = ""
}

variable "charge_type" {
  type        = string
  description = "The billing method. Valid value: PostPaid | PrePaid. Default to PostPaid. If set to PrePaid, the bandwidth package can't be deleted before expired time."
  default     = "PostPaid"
}

variable "child_instance_region_id_1" {
  type        = string
  description = "The region ID of the child instance to attach."
  default     = ""
}

variable "child_instance_region_id_2" {
  type        = string
  description = "The region ID of the child instance to attach."
  default     = ""
}

variable "create_bandwidth_limit" {
  type        = bool
  description = "Wether to create bandwidth limit,Default to true."
  default     = true
}

variable "create_bandwidth_package" {
  type        = bool
  description = "Whether to create bandwidth package."
  default     = true
}

variable "create_bandwidth_package_attachment" {
  type        = bool
  description = "Wether to create bandwidth package attachment,Default to true."
  default     = true
}

variable "create_cen" {
  type        = bool
  description = "Whether to create cen. If false, you can specify an existing cen by setting 'cen_id'."
  default     = true
}

variable "create_cen_instance_attachment" {
  type        = bool
  description = "Wether to create bandwidth package attachment,Default to true."
  default     = true
}

variable "existing_cen_id" {
  type        = string
  description = "The cen id used to launch several vswitches. If set, the 'create_cen' will be ignored."
  default     = ""
}

variable "geographic_region_ids" {
  type        = set(string)
  description = "List of the two areas to connect. Valid value: China | North-America | Asia-Pacific | Europe | Middle-East."
  default     = []
}

variable "period" {
  type        = number
  description = "The purchase period in month. Valid value: 1, 2, 3, 6, 12. Default to 1."
  default     = 1
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip staticvalidation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "using_existing_cen_id" {
  type        = bool
  description = "Whether to create instance. If false, you can specify an existing cen instance."
  default     = false
}

variable "vpc_id_1" {
  type        = string
  description = "The vpc id."
  default     = ""
}

variable "vpc_id_2" {
  type        = string
  description = "The vpc id."
  default     = ""
}


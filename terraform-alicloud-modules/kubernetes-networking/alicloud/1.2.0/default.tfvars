
availability_zones = []

create = true

eip_bandwidth = 10

eip_instance_charge_type = "PostPaid"

eip_internet_charge_type = "PayByTraffic"

eip_name = ""

eip_period = 1

eip_tags = {}

existing_vpc_id = ""

nat_gateway_name = ""

nat_instance_charge_type = "PostPaid"

nat_period = 1

nat_specification = "Small"

profile = ""

region = ""

shared_credentials_file = ""

skip_region_validation = false

vpc_cidr = "10.1.0.0/21"

vpc_name = ""

vpc_tags = {}

vswitch_cidrs = []

vswitch_name = ""

vswitch_tags = {}



variable "availability_zones" {
  type        = list(string)
  description = "List available zones to launch several VSwitches."
  default     = []
}

variable "create" {
  type        = bool
  description = "Whether to create kubernetes networking resources."
  default     = true
}

variable "eip_bandwidth" {
  type        = number
  description = "The eip bandwidth."
  default     = 10
}

variable "eip_instance_charge_type" {
  type        = string
  description = "Elastic IP instance charge type."
  default     = "PostPaid"
}

variable "eip_internet_charge_type" {
  type        = string
  description = "Internet charge type of the EIP, Valid values are `PayByBandwidth`, `PayByTraffic`. "
  default     = "PayByTraffic"
}

variable "eip_name" {
  type        = string
  description = "The name prefix used to launch the eip. "
  default     = ""
}

variable "eip_period" {
  type        = number
  description = "The duration that you will buy the EIP, in month."
  default     = 1
}

variable "eip_tags" {
  type        = map(string)
  description = "The tags used to launch the eip."
  default     = {}
}

variable "existing_vpc_id" {
  type        = string
  description = "An existing vpc id used to create several vswitches and other resources."
  default     = ""
}

variable "nat_gateway_name" {
  type        = string
  description = "The name prefix used to launch the nat gateway. "
  default     = ""
}

variable "nat_instance_charge_type" {
  type        = string
  description = "The charge type of the nat gateway. Choices are 'PostPaid' and 'PrePaid'."
  default     = "PostPaid"
}

variable "nat_period" {
  type        = number
  description = "The charge duration of the PrePaid nat gateway, in month."
  default     = 1
}

variable "nat_specification" {
  type        = string
  description = "The specification of nat gateway."
  default     = "Small"
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.2.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.2.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.2.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "vpc_cidr" {
  type        = string
  description = "The cidr block used to launch a new vpc when 'vpc_id' is not specified."
  default     = "10.1.0.0/21"
}

variable "vpc_name" {
  type        = string
  description = "The vpc name used to launch a new vpc."
  default     = ""
}

variable "vpc_tags" {
  type        = map(string)
  description = "The tags used to launch a new vpc."
  default     = {}
}

variable "vswitch_cidrs" {
  type        = list(string)
  description = "List of cidr blocks used to create several new vswitches when 'vswitch_ids' is not specified."
  default     = []
}

variable "vswitch_name" {
  type        = string
  description = "The name prefix used to launch the vswitch. "
  default     = ""
}

variable "vswitch_tags" {
  type        = map(string)
  description = "The tags used to launch serveral vswitches."
  default     = {}
}



locals {
  availability_zones = var.availability_zones
  create = var.create
  eip_bandwidth = var.eip_bandwidth
  eip_instance_charge_type = var.eip_instance_charge_type
  eip_internet_charge_type = var.eip_internet_charge_type
  eip_name = var.eip_name
  eip_period = var.eip_period
  eip_tags = var.eip_tags
  existing_vpc_id = var.existing_vpc_id
  nat_gateway_name = var.nat_gateway_name
  nat_instance_charge_type = var.nat_instance_charge_type
  nat_period = var.nat_period
  nat_specification = var.nat_specification
  profile = var.profile
  region = var.region
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  vpc_cidr = var.vpc_cidr
  vpc_name = var.vpc_name
  vpc_tags = var.vpc_tags
  vswitch_cidrs = var.vswitch_cidrs
  vswitch_name = var.vswitch_name
  vswitch_tags = var.vswitch_tags
}


create_instance = true

deletion_protection = false

ecs_instance_name = "TF-Tensorflow"

ecs_instance_password = ""

ecs_instance_type = ""

image_id = ""

internet_charge_type = "PayByTraffic"

internet_max_bandwidth_out = 10

private_ip = ""

product_keyword = "深度学习框架"

product_suggested_price = 0

product_supplier_name_keyword = ""

profile = ""

region = ""

resource_group_id = ""

security_group_ids = []

shared_credentials_file = ""

skip_region_validation = false

system_disk_category = "cloud_ssd"

system_disk_size = 40

tags = {}

vswitch_id = ""


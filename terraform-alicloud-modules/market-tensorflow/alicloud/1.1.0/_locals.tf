
locals {
  create_instance = var.create_instance
  deletion_protection = var.deletion_protection
  ecs_instance_name = var.ecs_instance_name
  ecs_instance_password = var.ecs_instance_password
  ecs_instance_type = var.ecs_instance_type
  image_id = var.image_id
  internet_charge_type = var.internet_charge_type
  internet_max_bandwidth_out = var.internet_max_bandwidth_out
  private_ip = var.private_ip
  product_keyword = var.product_keyword
  product_suggested_price = var.product_suggested_price
  product_supplier_name_keyword = var.product_supplier_name_keyword
  profile = var.profile
  region = var.region
  resource_group_id = var.resource_group_id
  security_group_ids = var.security_group_ids
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  system_disk_category = var.system_disk_category
  system_disk_size = var.system_disk_size
  tags = var.tags
  vswitch_id = var.vswitch_id
}

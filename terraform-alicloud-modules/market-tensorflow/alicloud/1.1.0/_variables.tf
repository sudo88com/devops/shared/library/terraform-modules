
variable "create_instance" {
  type        = bool
  description = "Whether to create ecs instance."
  default     = true
}

variable "deletion_protection" {
  type        = bool
  description = "Whether enable the deletion protection or not. 'true': Enable deletion protection. 'false': Disable deletion protection."
  default     = false
}

variable "ecs_instance_name" {
  type        = string
  description = "The name of ECS Instance."
  default     = "TF-Tensorflow"
}

variable "ecs_instance_password" {
  type        = string
  description = "The password of ECS instance."
  default     = ""
}

variable "ecs_instance_type" {
  type        = string
  description = "The instance type used to launch ecs instance."
  default     = ""
}

variable "image_id" {
  type        = string
  description = "The image id used to launch one ecs instance. If not set, a fetched market place image by product_keyword will be used."
  default     = ""
}

variable "internet_charge_type" {
  type        = string
  description = "The internet charge type of ECS instance. Choices are 'PayByTraffic' and 'PayByBandwidth'."
  default     = "PayByTraffic"
}

variable "internet_max_bandwidth_out" {
  type        = number
  description = "The maximum internet out bandwidth of ECS instance."
  default     = 10
}

variable "private_ip" {
  type        = string
  description = "Configure ECS Instance private IP address"
  default     = ""
}

variable "product_keyword" {
  type        = string
  description = "The name keyword of Market Product used to fetch the specified product image."
  default     = "深度学习框架"
}

variable "product_suggested_price" {
  type        = number
  description = "The suggested price of Market Product used to fetch the specified product image."
  default     = 0
}

variable "product_supplier_name_keyword" {
  type        = string
  description = "The name keyword of Market Product supplier name used to fetch the specified product image."
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "resource_group_id" {
  type        = string
  description = "The Id of resource group which the ECS instance belongs."
  default     = ""
}

variable "security_group_ids" {
  type        = list(string)
  description = "A list of security group ids to associate with ECS."
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "system_disk_category" {
  type        = string
  description = "The system disk category used to launch one ecs instance."
  default     = "cloud_ssd"
}

variable "system_disk_size" {
  type        = number
  description = "The system disk size used to launch ecs instance."
  default     = 40
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the ECS."
  default     = {}
}

variable "vswitch_id" {
  type        = string
  description = "The virtual switch ID to launch ECS instance in VPC."
  default     = ""
}


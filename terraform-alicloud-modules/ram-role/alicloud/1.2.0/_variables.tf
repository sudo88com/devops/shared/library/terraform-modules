
variable "create" {
  type        = bool
  description = "Whether to create ram role. If true, the 'users' or 'services' can not be empty."
  default     = true
}

variable "defined_services" {
  type        = map(list(string))
  description = "Trusted physical user who can play ram_role"
  default     = {
  "actiontrail": [
    "actiontrail.aliyuncs.com"
  ],
  "adb": [
    "adb.aliyuncs.com"
  ],
  "alikafka": [
    "alikafka.aliyuncs.com"
  ],
  "apigateway": [
    "apigateway.aliyuncs.com"
  ],
  "appms": [
    "appms.aliyuncs.com"
  ],
  "arms": [
    "arms.aliyuncs.com"
  ],
  "baas": [
    "baas.aliyuncs.com"
  ],
  "business": [
    "business.aliyuncs.com"
  ],
  "ccc": [
    "ccc.aliyuncs.com"
  ],
  "cloudpush": [
    "cloudpush.aliyuncs.com"
  ],
  "cusanalytic": [
    "cusanalytic.aliyuncs.com"
  ],
  "dcdn": [
    "dcdn.aliyuncs.com"
  ],
  "ddosbgp": [
    "ddosbgp.aliyuncs.com"
  ],
  "dns": [
    "dns.aliyuncs.com"
  ],
  "drds": [
    "drds.aliyuncs.com"
  ],
  "ecs": [
    "ecs.aliyuncs.com"
  ],
  "elasticsearch": [
    "elasticsearch.aliyuncs.com"
  ],
  "emr": [
    "emr.aliyuncs.com"
  ],
  "ess": [
    "ess.aliyuncs.com"
  ],
  "foas": [
    "foas.aliyuncs.com"
  ],
  "green": [
    "green.aliyuncs.com"
  ],
  "hbase": [
    "hbase.aliyuncs.com"
  ],
  "iot": [
    "iot.aliyuncs.com"
  ],
  "live": [
    "live.aliyuncs.com"
  ],
  "market": [
    "market.aliyuncs.com"
  ],
  "maxcompute": [
    "maxcompute.aliyuncs.com"
  ],
  "mongodb": [
    "mongodb.aliyuncs.com"
  ],
  "ons": [
    "ons.aliyuncs.com"
  ],
  "polardb": [
    "polardb.aliyuncs.com"
  ],
  "qualitycheck": [
    "qualitycheck.aliyuncs.com"
  ],
  "r-kvstore": [
    "r-kvstore.aliyuncs.com"
  ],
  "rds": [
    "rds.aliyuncs.com"
  ],
  "reid": [
    "reid.aliyuncs.com"
  ],
  "scdn": [
    "scdn.aliyuncs.com"
  ],
  "slb": [
    "slb.aliyuncs.com"
  ],
  "vod": [
    "vod.aliyuncs.com"
  ],
  "vpc": [
    "vpc.aliyuncs.com"
  ],
  "webplus": [
    "webplus.aliyuncs.com"
  ]
}
}

variable "existing_role_name" {
  type        = string
  description = "The name of an existing RAM role. If set, 'create' will be ignored."
  default     = ""
}

variable "force" {
  type        = bool
  description = "Whether to delete ram policy forcibly, default to true."
  default     = true
}

variable "policies" {
  type        = list(map(string))
  description = "List of the policies that binds the role. Each item can contains keys: 'policy_name'(the name of policy that used to bind the role), 'policy_type'(the type of ram policies, System or Custom, default to Custom.)."
  default     = []
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "ram_role_description" {
  type        = string
  description = "Description of the RAM role."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "role_name" {
  type        = string
  description = "The name of role. If not set, a default name with prefix 'terraform-ram-role-' will be returned. "
  default     = ""
}

variable "services" {
  type        = list(string)
  description = "List of the predefined and custom services used to play the ram role."
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "users" {
  type        = list(map(string))
  description = "List of the trusted users. Each item can contains keys: 'user_names'(list name of RAM users), 'account_id'(the account id of ram users). If not set 'account_id', the default is the current account. It will ignored when setting services."
  default     = []
}


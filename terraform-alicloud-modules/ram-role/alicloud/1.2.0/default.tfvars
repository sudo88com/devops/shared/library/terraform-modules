
create = true

defined_services = {
  "actiontrail": [
    "actiontrail.aliyuncs.com"
  ],
  "adb": [
    "adb.aliyuncs.com"
  ],
  "alikafka": [
    "alikafka.aliyuncs.com"
  ],
  "apigateway": [
    "apigateway.aliyuncs.com"
  ],
  "appms": [
    "appms.aliyuncs.com"
  ],
  "arms": [
    "arms.aliyuncs.com"
  ],
  "baas": [
    "baas.aliyuncs.com"
  ],
  "business": [
    "business.aliyuncs.com"
  ],
  "ccc": [
    "ccc.aliyuncs.com"
  ],
  "cloudpush": [
    "cloudpush.aliyuncs.com"
  ],
  "cusanalytic": [
    "cusanalytic.aliyuncs.com"
  ],
  "dcdn": [
    "dcdn.aliyuncs.com"
  ],
  "ddosbgp": [
    "ddosbgp.aliyuncs.com"
  ],
  "dns": [
    "dns.aliyuncs.com"
  ],
  "drds": [
    "drds.aliyuncs.com"
  ],
  "ecs": [
    "ecs.aliyuncs.com"
  ],
  "elasticsearch": [
    "elasticsearch.aliyuncs.com"
  ],
  "emr": [
    "emr.aliyuncs.com"
  ],
  "ess": [
    "ess.aliyuncs.com"
  ],
  "foas": [
    "foas.aliyuncs.com"
  ],
  "green": [
    "green.aliyuncs.com"
  ],
  "hbase": [
    "hbase.aliyuncs.com"
  ],
  "iot": [
    "iot.aliyuncs.com"
  ],
  "live": [
    "live.aliyuncs.com"
  ],
  "market": [
    "market.aliyuncs.com"
  ],
  "maxcompute": [
    "maxcompute.aliyuncs.com"
  ],
  "mongodb": [
    "mongodb.aliyuncs.com"
  ],
  "ons": [
    "ons.aliyuncs.com"
  ],
  "polardb": [
    "polardb.aliyuncs.com"
  ],
  "qualitycheck": [
    "qualitycheck.aliyuncs.com"
  ],
  "r-kvstore": [
    "r-kvstore.aliyuncs.com"
  ],
  "rds": [
    "rds.aliyuncs.com"
  ],
  "reid": [
    "reid.aliyuncs.com"
  ],
  "scdn": [
    "scdn.aliyuncs.com"
  ],
  "slb": [
    "slb.aliyuncs.com"
  ],
  "vod": [
    "vod.aliyuncs.com"
  ],
  "vpc": [
    "vpc.aliyuncs.com"
  ],
  "webplus": [
    "webplus.aliyuncs.com"
  ]
}

existing_role_name = ""

force = true

policies = []

profile = ""

ram_role_description = ""

region = ""

role_name = ""

services = []

shared_credentials_file = ""

skip_region_validation = false

users = []


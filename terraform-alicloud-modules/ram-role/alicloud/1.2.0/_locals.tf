
locals {
  create = var.create
  defined_services = var.defined_services
  existing_role_name = var.existing_role_name
  force = var.force
  policies = var.policies
  profile = var.profile
  ram_role_description = var.ram_role_description
  region = var.region
  role_name = var.role_name
  services = var.services
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  users = var.users
}

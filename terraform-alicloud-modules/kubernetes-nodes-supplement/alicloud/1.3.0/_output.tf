
output "this_common_bandwidth_package_id" {
  description = "The id of the common bandwidth package."
  value       = module.kubernetes-nodes-supplement.this_common_bandwidth_package_id
}

output "this_eip_ids" {
  description = "The list ids of the eip."
  value       = module.kubernetes-nodes-supplement.this_eip_ids
}


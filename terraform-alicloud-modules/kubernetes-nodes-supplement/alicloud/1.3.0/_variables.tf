
variable "cbp_bandwidth" {
  type        = number
  description = "The bandwidth of the common bandwidth package, in Mbps."
  default     = 10
}

variable "cbp_internet_charge_type" {
  type        = string
  description = "The billing method of the common bandwidth package. Valid values are 'PayByBandwidth' and 'PayBy95' and 'PayByTraffic'. 'PayBy95' is pay by classic 95th percentile pricing. International Account doesn't supports 'PayByBandwidth' and 'PayBy95'. Default to 'PayByTraffic'."
  default     = "PayByTraffic"
}

variable "cbp_ratio" {
  type        = string
  description = "Ratio of the common bandwidth package."
  default     = 100
}

variable "create_cbp" {
  type        = bool
  description = "Whether to create a common bandwidth package."
  default     = true
}

variable "create_eip" {
  type        = bool
  description = "Whether to create new EIP and bind it to this Nat gateway. If true, the 'number_of_dnat_eip' or 'number_of_snat_eip' should not be empty."
  default     = true
}

variable "eip_bandwidth" {
  type        = number
  description = "Maximum bandwidth to the elastic public network, measured in Mbps (Mega bit per second). If 'create_cbp' value is true, the value is same with 'cbp_bandwidth', this parameter will be ignore."
  default     = 5
}

variable "eip_instance_charge_type" {
  type        = string
  description = "Elastic IP instance charge type."
  default     = "PostPaid"
}

variable "eip_internet_charge_type" {
  type        = string
  description = "Internet charge type of the EIP, Valid values are 'PayByBandwidth', 'PayByTraffic'. "
  default     = "PayByTraffic"
}

variable "eip_isp" {
  type        = string
  description = "The line type of the Elastic IP instance."
  default     = ""
}

variable "eip_name" {
  type        = string
  description = "Name to be used on all eip as prefix. Default to 'TF-EIP-for-Nat'. The final default name would be TF-EIP-for-Nat001, TF-EIP-for-Nat002 and so on."
  default     = "tf-module-network-with-nat"
}

variable "eip_period" {
  type        = number
  description = "The duration that you will buy the EIP, in month."
  default     = 1
}

variable "eip_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the EIP instance resource."
  default     = {}
}

variable "kubernetes_cluster_id" {
  type        = string
  description = "The id of Kubernetes cluster."
  default     = ""
}

variable "kubernetes_node_ids" {
  type        = list(string)
  description = "The list ids of kubernetes node."
  default     = []
}

variable "number_of_kubernetes_nodes" {
  type        = number
  description = "The number of the nodes within Kubernetes Cluster."
  default     = 0
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.3.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.3.0) The region used to launch this module resources."
  default     = ""
}

variable "resource_group_id" {
  type        = string
  description = "The Id of resource group which the eip belongs."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.3.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.3.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}


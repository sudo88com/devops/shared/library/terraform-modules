
cbp_bandwidth = 10

cbp_internet_charge_type = "PayByTraffic"

cbp_ratio = 100

create_cbp = true

create_eip = true

eip_bandwidth = 5

eip_instance_charge_type = "PostPaid"

eip_internet_charge_type = "PayByTraffic"

eip_isp = ""

eip_name = "tf-module-network-with-nat"

eip_period = 1

eip_tags = {}

kubernetes_cluster_id = ""

kubernetes_node_ids = []

number_of_kubernetes_nodes = 0

profile = ""

region = ""

resource_group_id = ""

shared_credentials_file = ""

skip_region_validation = false


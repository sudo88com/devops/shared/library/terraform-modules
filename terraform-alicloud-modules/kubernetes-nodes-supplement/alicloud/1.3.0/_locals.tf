
locals {
  cbp_bandwidth = var.cbp_bandwidth
  cbp_internet_charge_type = var.cbp_internet_charge_type
  cbp_ratio = var.cbp_ratio
  create_cbp = var.create_cbp
  create_eip = var.create_eip
  eip_bandwidth = var.eip_bandwidth
  eip_instance_charge_type = var.eip_instance_charge_type
  eip_internet_charge_type = var.eip_internet_charge_type
  eip_isp = var.eip_isp
  eip_name = var.eip_name
  eip_period = var.eip_period
  eip_tags = var.eip_tags
  kubernetes_cluster_id = var.kubernetes_cluster_id
  kubernetes_node_ids = var.kubernetes_node_ids
  number_of_kubernetes_nodes = var.number_of_kubernetes_nodes
  profile = var.profile
  region = var.region
  resource_group_id = var.resource_group_id
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
}

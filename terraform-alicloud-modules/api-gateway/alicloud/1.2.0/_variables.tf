
variable "api_auth_type" {
  type        = string
  description = ""
  default     = "APP"
}

variable "api_description" {
  type        = string
  description = ""
  default     = "description"
}

variable "api_group_description" {
  type        = string
  description = "The name of a api group."
  default     = "Module of APi gateway Group"
}

variable "api_group_name" {
  type        = string
  description = ""
  default     = "TerraformApiGatewayGroup"
}

variable "api_http_service_config" {
  type        = list(object({
    address   = string
    method    = string
    path      = string
    timeout   = number
    aone_name = string
  }))
  description = ""
  default     = [
  {
    "address": "http://apigateway-backend.alicloudapi.com:8080",
    "aone_name": "cloudapi-openapi",
    "method": "GET",
    "path": "/web/cloudapi",
    "timeout": 12
  }
]
}

variable "api_name" {
  type        = string
  description = ""
  default     = "TerraformApiGatewayApi"
}

variable "api_request_config" {
  type        = list(object({
    protocol = string
    method   = string
    path     = string
    mode     = string
  }))
  description = ""
  default     = [
  {
    "method": "GET",
    "mode": "MAPPING",
    "path": "/test/path1",
    "protocol": "HTTP"
  }
]
}

variable "api_request_parameters" {
  type        = list(object({
    name         = string
    type         = string
    required     = string
    in           = string
    in_service   = string
    name_service = string
  }))
  description = ""
  default     = [
  {
    "in": "QUERY",
    "in_service": "QUERY",
    "name": "test",
    "name_service": "testparams",
    "required": "REQUIRED",
    "type": "STRING"
  }
]
}

variable "api_service_type" {
  type        = string
  description = ""
  default     = "HTTP"
}

variable "api_stage_name" {
  type        = list(string)
  description = ""
  default     = [
  "RELEASE",
  "PRE",
  "TEST"
]
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}


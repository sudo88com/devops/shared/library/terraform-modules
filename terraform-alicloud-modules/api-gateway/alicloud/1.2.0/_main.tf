
module "api-gateway" {
  source = "terraform-aws-modules/api-gateway/aws"
  version = "1.2.0"
  api_auth_type = var.api_auth_type
  api_description = var.api_description
  api_group_description = var.api_group_description
  api_group_name = var.api_group_name
  api_http_service_config = var.api_http_service_config
  api_name = var.api_name
  api_request_config = var.api_request_config
  api_request_parameters = var.api_request_parameters
  api_service_type = var.api_service_type
  api_stage_name = var.api_stage_name
  region = var.region
}

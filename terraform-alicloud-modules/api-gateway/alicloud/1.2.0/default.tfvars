
api_auth_type = "APP"

api_description = "description"

api_group_description = "Module of APi gateway Group"

api_group_name = "TerraformApiGatewayGroup"

api_http_service_config = [
  {
    "address": "http://apigateway-backend.alicloudapi.com:8080",
    "aone_name": "cloudapi-openapi",
    "method": "GET",
    "path": "/web/cloudapi",
    "timeout": 12
  }
]

api_name = "TerraformApiGatewayApi"

api_request_config = [
  {
    "method": "GET",
    "mode": "MAPPING",
    "path": "/test/path1",
    "protocol": "HTTP"
  }
]

api_request_parameters = [
  {
    "in": "QUERY",
    "in_service": "QUERY",
    "name": "test",
    "name_service": "testparams",
    "required": "REQUIRED",
    "type": "STRING"
  }
]

api_service_type = "HTTP"

api_stage_name = [
  "RELEASE",
  "PRE",
  "TEST"
]

region = ""


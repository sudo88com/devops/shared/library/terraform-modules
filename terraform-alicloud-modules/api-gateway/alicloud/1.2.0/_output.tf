
output "this_api_auth_type" {
  description = ""
  value       = module.api-gateway.this_api_auth_type
}

output "this_api_description" {
  description = ""
  value       = module.api-gateway.this_api_description
}

output "this_api_group_description" {
  description = ""
  value       = module.api-gateway.this_api_group_description
}

output "this_api_group_id" {
  description = ""
  value       = module.api-gateway.this_api_group_id
}

output "this_api_group_name" {
  description = ""
  value       = module.api-gateway.this_api_group_name
}

output "this_api_http_service_config" {
  description = ""
  value       = module.api-gateway.this_api_http_service_config
}

output "this_api_id" {
  description = ""
  value       = module.api-gateway.this_api_id
}

output "this_api_name" {
  description = ""
  value       = module.api-gateway.this_api_name
}

output "this_api_request_config" {
  description = ""
  value       = module.api-gateway.this_api_request_config
}

output "this_api_request_parameters" {
  description = ""
  value       = module.api-gateway.this_api_request_parameters
}

output "this_api_service_type" {
  description = ""
  value       = module.api-gateway.this_api_service_type
}

output "this_api_stage_name" {
  description = ""
  value       = module.api-gateway.this_api_stage_name
}



output "this_db_id" {
  description = ""
  value       = module.dms-rds.this_db_id
}

output "this_dms_id" {
  description = ""
  value       = module.dms-rds.this_dms_id
}

output "this_vpc_id" {
  description = ""
  value       = module.dms-rds.this_vpc_id
}

output "this_vswitch_id" {
  description = ""
  value       = module.dms-rds.this_vswitch_id
}


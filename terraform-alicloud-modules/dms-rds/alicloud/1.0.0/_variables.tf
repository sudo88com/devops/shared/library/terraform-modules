
variable "db_account" {
  type        = string
  description = "The account of db"
  default     = "tftestnormal"
}

variable "db_account_type" {
  type        = string
  description = "The account requiring a uniqueness check. "
  default     = "Normal"
}

variable "db_engine" {
  type        = string
  description = "Database type. Value options: MySQL, SQLServer, PostgreSQL, and PPAS."
  default     = "MySQL"
}

variable "db_engine_version" {
  type        = string
  description = "Database version."
  default     = "5.7"
}

variable "db_name" {
  type        = string
  description = "The name of DB instance. It a string of 2 to 256 characters."
  default     = "tf-testAccDbName"
}

variable "db_password" {
  type        = string
  description = "The password of db."
  default     = "Helloworld996!"
}

variable "db_security_ips" {
  type        = list(string)
  description = "List of IP addresses allowed to access all databases of an instance. "
  default     = [
  "100.104.5.0/24",
  "192.168.0.6",
  "100.104.205.0/24"
]
}

variable "db_storyge" {
  type        = string
  description = "User-defined DB instance storage space."
  default     = "10"
}

variable "db_type" {
  type        = string
  description = "DB Instance type. For details, see [Instance type table](https://www.alibabacloud.com/help/doc-detail/26312.htm)."
  default     = "rds.mysql.t1.small"
}

variable "dms_data_link_name" {
  type        = string
  description = "Cross-database query datalink name."
  default     = ""
}

variable "dms_db_alias" {
  type        = string
  description = "It has been deprecated from provider version 1.100.0 and 'instance_name' instead."
  default     = "tf-testDmsDbAlias"
}

variable "dms_ddl_online" {
  type        = string
  description = "Whether to use online services, currently only supports MySQL and PolarDB. Valid values: `0` Not used, `1` Native online DDL priority, `2` DMS lock-free table structure change priority."
  default     = "0"
}

variable "dms_env_type" {
  type        = string
  description = "Environment type. Valid values: `product` production environment, `dev` development environment, `pre` pre-release environment, `test` test environment, `sit` SIT environment, `uat` UAT environment, `pet` pressure test environment, `stag` STAG environment."
  default     = "test"
}

variable "dms_export_timeout" {
  type        = string
  description = "Export timeout, unit: s (seconds)."
  default     = "2000"
}

variable "dms_query_time" {
  type        = string
  description = "Query timeout time, unit: s (seconds)."
  default     = "70"
}

variable "dms_safe_rule" {
  type        = string
  description = "The security rule of the instance is passed into the name of the security rule in the enterprise."
  default     = "自由操作"
}

variable "dms_tid" {
  type        = string
  description = "The tenant ID. "
  default     = "13429"
}

variable "dms_use_dsql" {
  type        = string
  description = "Whether to enable cross-instance query. Valid values: `0` not open, `1` open."
  default     = "0"
}

variable "region" {
  type        = string
  description = "The region of resources"
  default     = ""
}

variable "vpc_cidr_block" {
  type        = string
  description = "The secondary CIDR blocks for the VPC."
  default     = "172.16.0.0/12"
}

variable "vpc_name" {
  type        = string
  description = "The name of the VPC."
  default     = "tf-testAccVpcName"
}

variable "vswitch_cidr_block" {
  type        = string
  description = "The secondary CIDR blocks for the vswtich."
  default     = "172.16.0.0/16"
}

variable "vswtich_name" {
  type        = string
  description = "The name of vswtich."
  default     = "tf-testAccVswtich"
}


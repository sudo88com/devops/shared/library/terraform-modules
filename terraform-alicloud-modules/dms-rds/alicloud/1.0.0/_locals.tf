
locals {
  db_account = var.db_account
  db_account_type = var.db_account_type
  db_engine = var.db_engine
  db_engine_version = var.db_engine_version
  db_name = var.db_name
  db_password = var.db_password
  db_security_ips = var.db_security_ips
  db_storyge = var.db_storyge
  db_type = var.db_type
  dms_data_link_name = var.dms_data_link_name
  dms_db_alias = var.dms_db_alias
  dms_ddl_online = var.dms_ddl_online
  dms_env_type = var.dms_env_type
  dms_export_timeout = var.dms_export_timeout
  dms_query_time = var.dms_query_time
  dms_safe_rule = var.dms_safe_rule
  dms_tid = var.dms_tid
  dms_use_dsql = var.dms_use_dsql
  region = var.region
  vpc_cidr_block = var.vpc_cidr_block
  vpc_name = var.vpc_name
  vswitch_cidr_block = var.vswitch_cidr_block
  vswtich_name = var.vswtich_name
}


availability_zone = ""

category = "cloud_efficiency"

description = "tf-eip-slb-ecs-rds-description"

ecs_size = 1200

eip_bandwidth = "10"

eip_internet_charge_type = "PayByBandwidth"

engine = "MySQL"

engine_version = "5.6"

image_id = "ubuntu_18_04_64_20G_alibase_20190624.vhd"

instance_charge_type = "Postpaid"

instance_storage = "30"

instance_type = "ecs.n4.large"

internet_max_bandwidth_out = 10

monitoring_period = "60"

name = "tf-eip-slb-ecs-redis-rds"

rds_instance_type = "rds.mysql.s2.large"

redis_appendonly = "redis.master.large.default"

redis_engine_version = "4.0"

redis_instance_class = "redis.master.large.default"

redis_instance_name = "tf-test-redis_name"

redis_instance_type = "Redis"

redis_lazyfree-lazy-eviction = "redis.master.large.default"

security_group_ids = []

security_ips = [
  "127.0.0.1"
]

slb_address_type = "intranet"

slb_spec = "slb.s2.small"

slb_tags_info = "create for internet"

system_disk_category = "cloud_efficiency"

system_disk_description = "system_disk_description"

system_disk_name = "system_disk"

vpc_cidr_block = "192.168.0.0/16"

vswitch_cidr_block = "192.168.1.0/24"

vswitch_id = ""

zone_id = "cn-hangzhou-e"


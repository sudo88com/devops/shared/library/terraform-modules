
variable "create" {
  type        = bool
  description = "Whether to create nat gateway. If false, you can specify an existing nat gateway by setting 'nat_gateway_id'."
  default     = true
}

variable "create_eip" {
  type        = bool
  description = "Whether to create new EIP and bind it to this Nat gateway. If true, the 'number_of_eip' should not be empty."
  default     = false
}

variable "description" {
  type        = string
  description = "The description of nat gateway."
  default     = ""
}

variable "dnat_count" {
  type        = number
  description = "(Deprecated) It has been deprecated from 1.2.0. Number of dnat entry."
  default     = 0
}

variable "eip_bandwidth" {
  type        = number
  description = "Maximum bandwidth to the elastic public network, measured in Mbps (Mega bit per second)."
  default     = 5
}

variable "eip_instance_charge_type" {
  type        = string
  description = "Elastic IP instance charge type."
  default     = "PostPaid"
}

variable "eip_internet_charge_type" {
  type        = string
  description = "Internet charge type of the EIP, Valid values are `PayByBandwidth`, `PayByTraffic`. "
  default     = "PayByTraffic"
}

variable "eip_isp" {
  type        = string
  description = "The line type of the Elastic IP instance."
  default     = "BGP"
}

variable "eip_name" {
  type        = string
  description = "Name to be used on all eip as prefix. Default to 'TF-EIP-for-Nat'. The final default name would be TF-EIP-for-Nat001, TF-EIP-for-Nat002 and so on."
  default     = ""
}

variable "eip_period" {
  type        = number
  description = "The duration that you will buy the EIP, in month."
  default     = 1
}

variable "eip_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the EIP instance resource."
  default     = {}
}

variable "existing_nat_gateway_id" {
  type        = string
  description = "The id of an existing nat gateway."
  default     = ""
}

variable "external_ips" {
  type        = list(string)
  description = "(Deprecated) It has been deprecated from 1.2.0. The external ip id of dnat entry. The length should be equal to dnat_count."
  default     = []
}

variable "external_ports" {
  type        = list(string)
  description = "(Deprecated) It has been deprecated from 1.2.0. The external port of dnat entry. The length should be equal to dnat_count."
  default     = []
}

variable "forward_table_id" {
  type        = string
  description = "(Deprecated) It has been deprecated from 1.2.0. The forward table id of dnat entry."
  default     = ""
}

variable "instance_charge_type" {
  type        = string
  description = "(Deprecated from version 1.4.0) The charge type of the nat gateway. Choices are 'PostPaid' and 'PrePaid'. Use payment_type instead."
  default     = "PostPaid"
}

variable "internal_ips" {
  type        = list(string)
  description = "(Deprecated) It has been deprecated from 1.2.0. The internal ip of dnat entry. The length should be equal to dnat_count."
  default     = []
}

variable "internal_ports" {
  type        = list(string)
  description = "(Deprecated) It has been deprecated from 1.2.0. The internal port of dnat entry. The length should be equal to dnat_count."
  default     = []
}

variable "internet_charge_type" {
  type        = string
  description = "The internet charge type."
  default     = "PayByLcu"
}

variable "ip_protocols" {
  type        = list(string)
  description = "(Deprecated) It has been deprecated from 1.2.0. The ip protocols of dnat entry. The length should be equal to dnat_count."
  default     = []
}

variable "name" {
  type        = string
  description = "Name of a new nat gateway."
  default     = ""
}

variable "nat_gateway_id" {
  type        = string
  description = "(Deprecated) It has been deprecated from 1.2.0 and use 'existing_nat_gateway_id' instead."
  default     = ""
}

variable "nat_type" {
  type        = string
  description = "The type of NAT gateway."
  default     = "Enhanced"
}

variable "number_of_eip" {
  type        = number
  description = "Number of EIP instance used to bind with this Nat gateway."
  default     = 1
}

variable "payment_type" {
  type        = string
  description = "The billing method of the NAT gateway."
  default     = "PayAsYouGo"
}

variable "period" {
  type        = number
  description = "The charge duration of the PrePaid nat gateway, in month."
  default     = 1
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.3.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.3.0) The region used to launch this module resources."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.3.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.3.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "snat_count" {
  type        = number
  description = "(Deprecated) It has been deprecated from 1.2.0. Number of snat entry."
  default     = 0
}

variable "snat_ips" {
  type        = list(string)
  description = "(Deprecated) It has been deprecated from 1.2.0. The snat ips of the nat gateway. The length should be equal to snat_count."
  default     = []
}

variable "snat_table_id" {
  type        = string
  description = "(Deprecated) It has been deprecated from 1.2.0. The snat table id of nat gateway."
  default     = ""
}

variable "source_vswitch_ids" {
  type        = list(string)
  description = "(Deprecated) It has been deprecated from 1.2.0. The vswitch ids. The length should be equal to snat_count."
  default     = []
}

variable "specification" {
  type        = string
  description = "The specification of nat gateway."
  default     = "Small"
}

variable "use_existing_nat_gateway" {
  type        = bool
  description = "Whether to create nat gateway. If true, you can specify an existing nat gateway by setting 'nat_gateway_id'."
  default     = false
}

variable "use_num_suffix" {
  type        = bool
  description = "Always append numerical suffix to instance name, even if number_of_instances is 1"
  default     = false
}

variable "vpc_id" {
  type        = string
  description = "ID of the VPC where to create nat gateway."
  default     = ""
}

variable "vswitch_cidrs" {
  type        = list(string)
  description = "(Deprecated) It has been deprecated from 1.2.0. List of cidr blocks used to launch several new vswitches."
  default     = []
}

variable "vswitch_id" {
  type        = string
  description = "ID of the vswitch where to create nat gateway."
  default     = ""
}


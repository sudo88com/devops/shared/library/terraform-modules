
create = true

create_eip = false

description = ""

dnat_count = 0

eip_bandwidth = 5

eip_instance_charge_type = "PostPaid"

eip_internet_charge_type = "PayByTraffic"

eip_isp = "BGP"

eip_name = ""

eip_period = 1

eip_tags = {}

existing_nat_gateway_id = ""

external_ips = []

external_ports = []

forward_table_id = ""

instance_charge_type = "PostPaid"

internal_ips = []

internal_ports = []

internet_charge_type = "PayByLcu"

ip_protocols = []

name = ""

nat_gateway_id = ""

nat_type = "Enhanced"

number_of_eip = 1

payment_type = "PayAsYouGo"

period = 1

profile = ""

region = ""

shared_credentials_file = ""

skip_region_validation = false

snat_count = 0

snat_ips = []

snat_table_id = ""

source_vswitch_ids = []

specification = "Small"

use_existing_nat_gateway = false

use_num_suffix = false

vpc_id = ""

vswitch_cidrs = []

vswitch_id = ""


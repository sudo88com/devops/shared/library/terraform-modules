
adjustment_type = "TotalCapacity"

adjustment_value = 0

alarm_task_metric_name = "CpuUtilization"

alarm_task_metric_type = "system"

alarm_task_name = ""

alarm_task_setting = {}

cooldown = ""

create_alarm_task = false

create_scheduled_task = false

create_simple_rule = false

create_step_rule = false

create_target_tracking_rule = false

disable_scale_in = false

enable_alarm_task = true

enable_scheduled_task = true

estimated_instance_warmup = 300

metric_name = "CpuUtilization"

profile = ""

region = ""

scaling_group_id = ""

scaling_group_name_regex = ""

scaling_rule_name = ""

scheduled_task_name = ""

scheduled_task_setting = {}

shared_credentials_file = ""

skip_region_validation = false

step_adjustments = []

target_value = "80.5"


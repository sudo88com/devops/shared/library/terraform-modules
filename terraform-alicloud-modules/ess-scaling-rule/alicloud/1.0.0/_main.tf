
module "ess-scaling-rule" {
  source = "terraform-aws-modules/ess-scaling-rule/aws"
  version = "1.0.0"
  adjustment_type = var.adjustment_type
  availability_zone = var.availability_zone
  force_delete = var.force_delete
  name = var.name
  name_regex = var.name_regex
  nic_type = var.nic_type
  removal_policies = var.removal_policies
  rule_cidr_ip = var.rule_cidr_ip
  rule_ip_protocol = var.rule_ip_protocol
  rule_policy = var.rule_policy
  rule_port_range = var.rule_port_range
  rule_type = var.rule_type
  security_group_id = var.security_group_id
  vswitch_id = var.vswitch_id
}

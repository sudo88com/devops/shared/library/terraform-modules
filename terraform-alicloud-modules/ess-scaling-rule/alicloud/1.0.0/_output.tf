
output "this_ess_scaling_group_id" {
  description = "The ID of the ess scaling group."
  value       = module.ess-scaling-rule.this_ess_scaling_group_id
}

output "this_ess_scaling_group_name" {
  description = "The ID of the ess scaling group name."
  value       = module.ess-scaling-rule.this_ess_scaling_group_name
}

output "this_ess_scaling_rule_id" {
  description = "The ID of the ess scaling rule."
  value       = module.ess-scaling-rule.this_ess_scaling_rule_id
}


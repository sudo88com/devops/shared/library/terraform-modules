
adjustment_type = "TotalCapacity"

availability_zone = ""

force_delete = "true"

name = "ess-scaling-rule-config"

name_regex = "^ubuntu_18.*64"

nic_type = "intranet"

removal_policies = [
  "OldestInstance",
  "NewestInstance"
]

rule_cidr_ip = "172.16.0.0/24"

rule_ip_protocol = "tcp"

rule_policy = "accept"

rule_port_range = "22/22"

rule_type = "ingress"

security_group_id = ""

vswitch_id = ""


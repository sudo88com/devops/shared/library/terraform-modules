
variable "adjustment_type" {
  type        = string
  description = "Scaling Total Capacity."
  default     = "TotalCapacity"
}

variable "availability_zone" {
  type        = string
  description = "The available zone to launch modules."
  default     = ""
}

variable "force_delete" {
  type        = string
  description = "Rule force delete."
  default     = "true"
}

variable "name" {
  type        = string
  description = "The ESS Scaling rule name."
  default     = "ess-scaling-rule-config"
}

variable "name_regex" {
  type        = string
  description = "Scaling name regex."
  default     = "^ubuntu_18.*64"
}

variable "nic_type" {
  type        = string
  description = "Rule nic type."
  default     = "intranet"
}

variable "removal_policies" {
  type        = list(string)
  description = "Rule removal policies."
  default     = [
  "OldestInstance",
  "NewestInstance"
]
}

variable "rule_cidr_ip" {
  type        = string
  description = "Rule cidr ip."
  default     = "172.16.0.0/24"
}

variable "rule_ip_protocol" {
  type        = string
  description = "Rule ip protocol."
  default     = "tcp"
}

variable "rule_policy" {
  type        = string
  description = "Rule policy."
  default     = "accept"
}

variable "rule_port_range" {
  type        = string
  description = "Rule port range."
  default     = "22/22"
}

variable "rule_type" {
  type        = string
  description = "Rule type."
  default     = "ingress"
}

variable "security_group_id" {
  type        = string
  description = "A list of security group ids to associate with."
  default     = ""
}

variable "vswitch_id" {
  type        = string
  description = "VSwitch variables, if vswitch_id is empty, then the net_type = classic."
  default     = ""
}


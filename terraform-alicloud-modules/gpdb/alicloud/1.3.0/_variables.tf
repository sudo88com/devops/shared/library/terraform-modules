
variable "availability_zone" {
  type        = string
  description = "The available zone to launch ecs instance and other resources."
  default     = ""
}

variable "connection_prefix" {
  type        = string
  description = "Prefix of an Internet connection string. It must be checked for uniqueness. It may consist of lowercase letters, numbers, and underlines, and must start with a letter and have no more than 30 characters. Default to + 'tf'."
  default     = ""
}

variable "db_instance_category" {
  type        = string
  description = "The db instance category."
  default     = "Basic"
}

variable "db_instance_mode" {
  type        = string
  description = "The db instance mode."
  default     = "StorageElastic"
}

variable "description" {
  type        = string
  description = "The name of DB instance. It a string of 2 to 256 characters"
  default     = ""
}

variable "engine" {
  type        = string
  description = "The database engine to use"
  default     = "gpdb"
}

variable "engine_version" {
  type        = string
  description = "The engine version to use"
  default     = "6.0"
}

variable "instance_class" {
  type        = string
  description = "DB Instance type, for example: gpdb.group.segsdx2. fall list is : https://www.alibabacloud.com/help/doc-detail/35406.htm"
  default     = "gpdb.group.segsdx1"
}

variable "instance_group_count" {
  type        = string
  description = "The number of instance group."
  default     = "2"
}

variable "instance_spec" {
  type        = string
  description = "The specification of segment nodes."
  default     = "2C16G"
}

variable "number_of_instances" {
  type        = number
  description = "The number of launching instances one time."
  default     = 1
}

variable "port" {
  type        = number
  description = "Internet connection port. Valid value: [3200-3999]. Default to 3306."
  default     = 3306
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "security_ip_list" {
  type        = list(string)
  description = "List of IP addresses allowed to access all databases of an instance. The list contains up to 1,000 IP addresses, separated by commas. Supported formats include 0.0.0.0/0, 10.23.12.24 (IP), and 10.23.12.24/24 (Classless Inter-Domain Routing (CIDR) mode. /24 represents the length of the prefix in an IP address. The range of the prefix length is [1,32])."
  default     = []
}

variable "seg_node_num" {
  type        = number
  description = "Calculate the number of nodes."
  default     = 4
}

variable "seg_storage_type" {
  type        = string
  description = "The seg storage type."
  default     = "cloud_essd"
}

variable "storage_size" {
  type        = number
  description = "The storage capacity."
  default     = 50
}

variable "vswitch_id" {
  type        = string
  description = "The vswitch id used to launch one or more instances."
  default     = ""
}


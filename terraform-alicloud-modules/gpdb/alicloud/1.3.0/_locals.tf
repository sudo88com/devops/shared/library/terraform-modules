
locals {
  availability_zone = var.availability_zone
  connection_prefix = var.connection_prefix
  db_instance_category = var.db_instance_category
  db_instance_mode = var.db_instance_mode
  description = var.description
  engine = var.engine
  engine_version = var.engine_version
  instance_class = var.instance_class
  instance_group_count = var.instance_group_count
  instance_spec = var.instance_spec
  number_of_instances = var.number_of_instances
  port = var.port
  region = var.region
  security_ip_list = var.security_ip_list
  seg_node_num = var.seg_node_num
  seg_storage_type = var.seg_storage_type
  storage_size = var.storage_size
  vswitch_id = var.vswitch_id
}

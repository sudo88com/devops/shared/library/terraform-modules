
availability_zone = ""

connection_prefix = ""

db_instance_category = "Basic"

db_instance_mode = "StorageElastic"

description = ""

engine = "gpdb"

engine_version = "6.0"

instance_class = "gpdb.group.segsdx1"

instance_group_count = "2"

instance_spec = "2C16G"

number_of_instances = 1

port = 3306

region = ""

security_ip_list = []

seg_node_num = 4

seg_storage_type = "cloud_essd"

storage_size = 50

vswitch_id = ""



output "this_gpdb_connection_id" {
  description = ""
  value       = module.gpdb.this_gpdb_connection_id
}

output "this_gpdb_instance_id" {
  description = ""
  value       = module.gpdb.this_gpdb_instance_id
}


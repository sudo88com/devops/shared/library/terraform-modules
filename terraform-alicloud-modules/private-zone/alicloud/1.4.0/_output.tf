
output "this_private_zone_name" {
  description = "The name of Private Zone"
  value       = module.private-zone.this_private_zone_name
}

output "this_private_zone_records" {
  description = "List of the Private Zone records"
  value       = module.private-zone.this_private_zone_records
}


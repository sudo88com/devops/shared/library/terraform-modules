
variable "add_records" {
  type        = bool
  description = "Whether to add records to Private Zone."
  default     = true
}

variable "attach_vpc" {
  type        = bool
  description = "Whether to associate VPC to Private zone."
  default     = true
}

variable "create" {
  type        = bool
  description = "Whether to create private zone."
  default     = true
}

variable "existing_zone_name" {
  type        = string
  description = "The name of an existing private zone. If set, 'create' will be ignored."
  default     = ""
}

variable "lang" {
  type        = string
  description = "The language of code. Valid values: 'zh', 'en', 'jp."
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.4.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "record_list" {
  type        = list(map(string))
  description = "(Deprecated) It has been deprecated from 1.2.0, and use 'records' instead."
  default     = []
}

variable "records" {
  type        = list(map(string))
  description = "PVT Zone record list.Each item can contains keys: 'rr'(The resource record of the Private Zone Record. 'name' has been deprecated from 1.3.0, and use 'rr' instead.),'type'(The type of the Private Zone Record. Valid values: A, CNAME, TXT, MX, PTR. Default to A.),'value'(The value of the Private Zone Record),'priority'(The priority of the Private Zone Record. At present, only can 'MX' record support it. Valid values: [1-50]. Default to 1.),'ttl'(The ttl of the Private Zone Record. Default to 60.)."
  default     = []
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.4.0) The region used to launch this module resources."
  default     = ""
}

variable "remark" {
  type        = string
  description = "The remark of the Private Zone"
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.4.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.4.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "user_client_ip" {
  type        = string
  description = "The user custom IP address"
  default     = ""
}

variable "vpc_id_list" {
  type        = list(string)
  description = "(Deprecated) It has been deprecated from 1.2.0, and use 'vpc_ids' instead."
  default     = []
}

variable "vpc_ids" {
  type        = list(string)
  description = "The id List of the VPC with the same region, for example:['vpc-1','vpc-2']."
  default     = []
}

variable "zone_name" {
  type        = string
  description = "The name of zone. If not set, a default name with prefix 'terraform-zone-' will be returned. "
  default     = ""
}


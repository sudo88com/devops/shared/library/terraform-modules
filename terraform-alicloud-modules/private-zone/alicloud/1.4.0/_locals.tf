
locals {
  add_records = var.add_records
  attach_vpc = var.attach_vpc
  create = var.create
  existing_zone_name = var.existing_zone_name
  lang = var.lang
  profile = var.profile
  record_list = var.record_list
  records = var.records
  region = var.region
  remark = var.remark
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  user_client_ip = var.user_client_ip
  vpc_id_list = var.vpc_id_list
  vpc_ids = var.vpc_ids
  zone_name = var.zone_name
}

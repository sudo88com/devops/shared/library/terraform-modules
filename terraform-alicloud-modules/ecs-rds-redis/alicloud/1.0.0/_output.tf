
output "this_ecs_id" {
  description = ""
  value       = module.ecs-rds-redis.this_ecs_id
}

output "this_ecs_name" {
  description = ""
  value       = module.ecs-rds-redis.this_ecs_name
}

output "this_rds_id" {
  description = ""
  value       = module.ecs-rds-redis.this_rds_id
}

output "this_rds_name" {
  description = ""
  value       = module.ecs-rds-redis.this_rds_name
}

output "this_redis_id" {
  description = ""
  value       = module.ecs-rds-redis.this_redis_id
}

output "this_redis_name" {
  description = ""
  value       = module.ecs-rds-redis.this_redis_name
}


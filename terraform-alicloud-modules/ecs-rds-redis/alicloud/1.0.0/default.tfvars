
availability_zone = ""

category = "cloud_efficiency"

description = "tf-ecs-rds-redis-description"

ecs_size = 1200

engine_version = "5.6"

image_id = "ubuntu_18_04_64_20G_alibase_20190624.vhd"

instance_charge_type = "Postpaid"

instance_storage = "30"

instance_type = "ecs.n4.large"

internet_max_bandwidth_out = 10

monitoring_period = "60"

name = "tf-ecs-rds-redis"

rds_instance_type = "rds.mysql.s2.large"

redis_appendonly = "yes"

redis_engine_version = "4.0"

redis_instance_class = "redis.master.large.default"

redis_lazyfree_lazy_eviction = "yes"

redis_resource_group_id = "rg-123456"

security_group_ids = []

security_ips = [
  "127.0.0.1"
]

system_disk_category = "cloud_efficiency"

system_disk_description = "system_disk_description"

system_disk_name = "system_disk"

vswitch_id = ""


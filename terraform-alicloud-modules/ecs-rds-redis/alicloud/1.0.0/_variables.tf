
variable "availability_zone" {
  type        = string
  description = "The available zone to launch modules."
  default     = ""
}

variable "category" {
  type        = string
  description = "The specification of the category."
  default     = "cloud_efficiency"
}

variable "description" {
  type        = string
  description = "The specification of module description."
  default     = "tf-ecs-rds-redis-description"
}

variable "ecs_size" {
  type        = number
  description = "The specification of the ecs size."
  default     = 1200
}

variable "engine_version" {
  type        = string
  description = "The specification of the engine version."
  default     = "5.6"
}

variable "image_id" {
  type        = string
  description = "The specification of the image id."
  default     = "ubuntu_18_04_64_20G_alibase_20190624.vhd"
}

variable "instance_charge_type" {
  type        = string
  description = "The specification of the instance charge type."
  default     = "Postpaid"
}

variable "instance_storage" {
  type        = string
  description = "The specification of the instance storage."
  default     = "30"
}

variable "instance_type" {
  type        = string
  description = "The specification of the instance type."
  default     = "ecs.n4.large"
}

variable "internet_max_bandwidth_out" {
  type        = number
  description = "The specification of the internet max bandwidth out."
  default     = 10
}

variable "monitoring_period" {
  type        = string
  description = "The specification of the monitoring period."
  default     = "60"
}

variable "name" {
  type        = string
  description = "The specification of module name."
  default     = "tf-ecs-rds-redis"
}

variable "rds_instance_type" {
  type        = string
  description = "The specification of the rds instance type."
  default     = "rds.mysql.s2.large"
}

variable "redis_appendonly" {
  type        = string
  description = "The specification of the redis appendonly."
  default     = "yes"
}

variable "redis_engine_version" {
  type        = string
  description = "The specification of the redis engine version."
  default     = "4.0"
}

variable "redis_instance_class" {
  type        = string
  description = "The specification of the redis resource instance class."
  default     = "redis.master.large.default"
}

variable "redis_lazyfree_lazy_eviction" {
  type        = string
  description = "The specification of the redis lazyfree-lazy-eviction."
  default     = "yes"
}

variable "redis_resource_group_id" {
  type        = string
  description = "The specification of the redis resource group id."
  default     = "rg-123456"
}

variable "security_group_ids" {
  type        = list(string)
  description = "A list of security group ids to associate with."
  default     = []
}

variable "security_ips" {
  type        = list(string)
  description = "The specification of the security ips."
  default     = [
  "127.0.0.1"
]
}

variable "system_disk_category" {
  type        = string
  description = "The specification of the system disk category."
  default     = "cloud_efficiency"
}

variable "system_disk_description" {
  type        = string
  description = "The specification of the system disk description."
  default     = "system_disk_description"
}

variable "system_disk_name" {
  type        = string
  description = "The specification of the system disk name."
  default     = "system_disk"
}

variable "vswitch_id" {
  type        = string
  description = "VSwitch variables, if vswitch_id is empty, then the net_type = classic."
  default     = ""
}


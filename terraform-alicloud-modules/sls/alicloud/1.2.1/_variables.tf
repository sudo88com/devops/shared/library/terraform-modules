
variable "create" {
  type        = bool
  description = "Whether to create log resources"
  default     = true
}

variable "description" {
  type        = string
  description = "Description of the log project."
  default     = ""
}

variable "index_field_search" {
  type        = set(object({
    // The field name, which is unique in the same log store.
    name = string
    // The type of one field. Valid values: ["long", "text", "double", "json"]. Default to "long".
    type = string
    // The alias of one field
    alias = string
    // Whether the case sensitive for the field. Default to false. It is valid when "type" is "text" or "json".
    case_sensitive = bool
    // Whether includes the chinese for the field. Default to false. It is valid when "type" is "text" or "json".
    include_chinese = bool
    // The string of several split words, like "\r", "#". It is valid when "type" is "text" or "json".
    token = string
    // Whether to enable field analytics. Default to true.
    enable_analytics = bool
    // Use nested index when type is json
    json_keys = list(object({
      // When using the json_keys field, this field is required.
      name = string
      // The type of one field. Valid values: ["long", "text", "double"]. Default to "long"
      type = string
      // The alias of one field.
      alias = string
      // Whether to enable statistics. default to true.
      doc_value = bool
    }))
  }))
  description = "List configurations of field search index."
  default     = []
}

variable "index_full_text" {
  type        = set(map(string))
  description = "The configuration of full text index."
  default     = [
  {
    "case_sensitive": false,
    "include_chinese": false,
    "token": "#"
  }
]
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "project_name" {
  type        = string
  description = "Log project name. If not set, a default name with prefix `sls-module-project-` will be returned."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "store_append_meta" {
  type        = bool
  description = "Determines whether to append log meta automatically. The meta includes log receive time and client IP address. Default to true."
  default     = true
}

variable "store_auto_split" {
  type        = bool
  description = "Determines whether to automatically split a shard. Default to true."
  default     = true
}

variable "store_enable_web_tracking" {
  type        = bool
  description = "Determines whether to enable Web Tracking. Default false."
  default     = false
}

variable "store_max_split_shard_count" {
  type        = number
  description = "The maximum number of shards for automatic split, which is in the range of 1 to 64. You must specify this parameter when autoSplit is true."
  default     = 1
}

variable "store_name" {
  type        = string
  description = "Log store name. If not set, a default name with prefix `sls-module-store-` will be returned."
  default     = ""
}

variable "store_retention_period" {
  type        = number
  description = "The data retention time (in days). Valid values: [1-3650]. Default to 30. Log store data will be stored permanently when the value is '3650'."
  default     = 30
}

variable "store_shard_count" {
  type        = number
  description = "The number of shards in this log store. Default to 2. You can modify it by 'Split' or 'Merge' operations."
  default     = 2
}

variable "use_random_suffix" {
  type        = bool
  description = "Whether to add a suffix for project name"
  default     = true
}



create = true

description = ""

index_field_search = []

index_full_text = [
  {
    "case_sensitive": false,
    "include_chinese": false,
    "token": "#"
  }
]

profile = ""

project_name = ""

region = ""

shared_credentials_file = ""

skip_region_validation = false

store_append_meta = true

store_auto_split = true

store_enable_web_tracking = false

store_max_split_shard_count = 1

store_name = ""

store_retention_period = 30

store_shard_count = 2

use_random_suffix = true



locals {
  create = var.create
  description = var.description
  index_field_search = var.index_field_search
  index_full_text = var.index_full_text
  profile = var.profile
  project_name = var.project_name
  region = var.region
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  store_append_meta = var.store_append_meta
  store_auto_split = var.store_auto_split
  store_enable_web_tracking = var.store_enable_web_tracking
  store_max_split_shard_count = var.store_max_split_shard_count
  store_name = var.store_name
  store_retention_period = var.store_retention_period
  store_shard_count = var.store_shard_count
  use_random_suffix = var.use_random_suffix
}

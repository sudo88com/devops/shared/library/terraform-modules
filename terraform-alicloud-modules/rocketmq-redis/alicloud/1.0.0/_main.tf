
module "rocketmq-redis" {
  source = "terraform-aws-modules/rocketmq-redis/aws"
  version = "1.0.0"
  ons_instance_name = var.ons_instance_name
  ons_instance_remark = var.ons_instance_remark
  ons_topic_remark = var.ons_topic_remark
  redis_appendonly = var.redis_appendonly
  redis_engine_version = var.redis_engine_version
  redis_instance_class = var.redis_instance_class
  redis_instance_name = var.redis_instance_name
  redis_instance_type = var.redis_instance_type
  redis_lazyfree-lazy-eviction = var.redis_lazyfree-lazy-eviction
  redis_tags_created = var.redis_tags_created
  redis_tags_for = var.redis_tags_for
  security_ips = var.security_ips
  topic = var.topic
  vswitch_id = var.vswitch_id
  zone_id = var.zone_id
}

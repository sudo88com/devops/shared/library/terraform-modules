
output "this_alicloud_ons_instance_id" {
  description = ""
  value       = module.rocketmq-redis.this_alicloud_ons_instance_id
}

output "this_alicloud_ons_topic_id" {
  description = ""
  value       = module.rocketmq-redis.this_alicloud_ons_topic_id
}

output "this_redis_instance_id" {
  description = ""
  value       = module.rocketmq-redis.this_redis_instance_id
}

output "this_vswitch_id" {
  description = ""
  value       = module.rocketmq-redis.this_vswitch_id
}


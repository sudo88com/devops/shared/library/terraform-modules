
ons_instance_name = "tf-onsInstanceName123"

ons_instance_remark = "tf-ons_instance_remark"

ons_topic_remark = "tf-ons_topic_remark"

redis_appendonly = "yes"

redis_engine_version = "4.0"

redis_instance_class = "redis.master.large.default"

redis_instance_name = "tf-test-redis_name"

redis_instance_type = "Redis"

redis_lazyfree-lazy-eviction = "yes"

redis_tags_created = "TF"

redis_tags_for = "test"

security_ips = [
  "127.0.0.1"
]

topic = "onsTopicName1"

vswitch_id = ""

zone_id = "cn-hangzhou-e"


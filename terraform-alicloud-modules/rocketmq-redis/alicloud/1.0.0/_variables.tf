
variable "ons_instance_name" {
  type        = string
  description = "The specification of ons instance name."
  default     = "tf-onsInstanceName123"
}

variable "ons_instance_remark" {
  type        = string
  description = "The specification of ons instance remark."
  default     = "tf-ons_instance_remark"
}

variable "ons_topic_remark" {
  type        = string
  description = "The specification of ons topic remark."
  default     = "tf-ons_topic_remark"
}

variable "redis_appendonly" {
  type        = string
  description = "The specification of the redis is append only."
  default     = "yes"
}

variable "redis_engine_version" {
  type        = string
  description = "The specification of the redis engine version."
  default     = "4.0"
}

variable "redis_instance_class" {
  type        = string
  description = "The specification of the redis instance class."
  default     = "redis.master.large.default"
}

variable "redis_instance_name" {
  type        = string
  description = "The specification of the redis instance name."
  default     = "tf-test-redis_name"
}

variable "redis_instance_type" {
  type        = string
  description = "The specification of the redis instance type."
  default     = "Redis"
}

variable "redis_lazyfree-lazy-eviction" {
  type        = string
  description = "The specification of the redis lazy eviction."
  default     = "yes"
}

variable "redis_tags_created" {
  type        = string
  description = "The specification of the redis tags Created value."
  default     = "TF"
}

variable "redis_tags_for" {
  type        = string
  description = "The specification of the redis tags For value."
  default     = "test"
}

variable "security_ips" {
  type        = list(string)
  description = "The specification of the security ips."
  default     = [
  "127.0.0.1"
]
}

variable "topic" {
  type        = string
  description = "The specification of ons topic name."
  default     = "onsTopicName1"
}

variable "vswitch_id" {
  type        = string
  description = "VSwitch variables, if vswitch_id is empty, then the net_type = classic."
  default     = ""
}

variable "zone_id" {
  type        = string
  description = "The specification of zone msg."
  default     = "cn-hangzhou-e"
}


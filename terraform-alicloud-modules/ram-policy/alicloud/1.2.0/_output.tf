
output "this_policy_id" {
  description = "Id of the custom policy."
  value       = module.ram-policy.this_policy_id
}

output "this_policy_name" {
  description = "Name of the custom policy."
  value       = module.ram-policy.this_policy_name
}



variable "create" {
  type        = bool
  description = "Whether to create RAM policies. If true, the policies should not be empty."
  default     = true
}

variable "defined_actions" {
  type        = map(list(string))
  description = "Map of several defined actions"
  default     = {
  "db-instance-all": [
    "rds:*Instance*",
    "rds:ModifyParameter",
    "rds:UntagResources",
    "rds:TagResources",
    "rds:ModifySecurityGroupConfiguration",
    "rds:DescribeTags",
    "rds:DescribeSQLCollector*",
    "rds:DescribeParameters",
    "rds:DescribeSecurityGroupConfiguration"
  ],
  "db-instance-create": [
    "rds:CreateDBInstance",
    "vpc:DescribeVSwitchAttributes",
    "rds:ModifyParameter",
    "rds:UntagResources",
    "rds:TagResources",
    "rds:ModifyInstanceAutoRenewalAttribute",
    "rds:ModifySecurityGroupConfiguration",
    "rds:ModifyDBInstance*",
    "rds:DescribeDBInstance*",
    "rds:DescribeTags",
    "rds:DescribeSQLCollector*",
    "rds:DescribeParameters",
    "rds:DescribeInstanceAutoRenewalAttribute",
    "rds:DescribeSecurityGroupConfiguration"
  ],
  "db-instance-delete": [
    "rds:DeleteDBInstance",
    "rds:DescribeDBInstanceAttribute"
  ],
  "db-instance-read": [
    "rds:DescribeDBInstance*",
    "rds:DescribeTags",
    "rds:DescribeSQLCollector*",
    "rds:DescribeParameters",
    "rds:DescribeInstanceAutoRenewalAttribute",
    "rds:DescribeSecurityGroupConfiguration"
  ],
  "db-instance-update": [
    "rds:ModifyParameter",
    "rds:UntagResources",
    "rds:TagResources",
    "rds:ModifyInstanceAutoRenewalAttribute",
    "rds:ModifySecurityGroupConfiguration",
    "rds:ModifyDBInstance*",
    "rds:DescribeDBInstance*",
    "rds:DescribeTags",
    "rds:DescribeSQLCollector*",
    "rds:DescribeParameters",
    "rds:DescribeInstanceAutoRenewalAttribute",
    "rds:DescribeSecurityGroupConfiguration"
  ],
  "disk-all": [
    "ecs:*Disk*",
    "ecs:UntagResources",
    "ecs:TagResources",
    "ecs:DescribeZones"
  ],
  "disk-attach": [
    "ecs:AttachDisk",
    "ecs:DescribeDisks",
    "ecs:ModifyDiskAttribute"
  ],
  "disk-create": [
    "ecs:CreateDisk",
    "ecs:UntagResources",
    "ecs:TagResources",
    "ecs:ModifyDiskAttribute",
    "ecs:DescribeDisks",
    "ecs:DescribeZones"
  ],
  "disk-delete": [
    "ecs:DeleteDisk",
    "ecs:DescribeDisks"
  ],
  "disk-detach": [
    "ecs:DetachDisk",
    "ecs:DescribeDisks"
  ],
  "disk-read": [
    "ecs:DescribeDisks"
  ],
  "disk-update": [
    "ecs:UntagResources",
    "ecs:TagResources",
    "ecs:ModifyDiskAttribute",
    "ecs:ResizeDisk",
    "ecs:DescribeDisks"
  ],
  "eip-all": [
    "vpc:*EipAddress*",
    "vpc:UntagResources",
    "vpc:TagResources"
  ],
  "eip-associate": [
    "vpc:AssociateEipAddress",
    "vpc:DescribeEipAddresses"
  ],
  "eip-create": [
    "vpc:AllocateEipAddress",
    "vpc:UntagResources",
    "vpc:TagResources",
    "vpc:ModifyEipAddressAttribute",
    "vpc:DescribeEipAddresses"
  ],
  "eip-delete": [
    "vpc:ReleaseEipAddress",
    "vpc:DescribeEipAddresses"
  ],
  "eip-read": [
    "vpc:DescribeEipAddresses"
  ],
  "eip-unassociate": [
    "vpc:UnassociateEipAddress",
    "vpc:DescribeEipAddresses"
  ],
  "eip-update": [
    "vpc:UntagResources",
    "vpc:TagResources",
    "vpc:ModifyEipAddressAttribute",
    "vpc:DescribeEipAddresses"
  ],
  "image-all": [
    "ecs:*Image*",
    "ecs:DescribeInstances",
    "ecs:DescribeSnapshots"
  ],
  "image-copy": [
    "ecs:CopyImage",
    "ecs:DescribeImages"
  ],
  "image-create": [
    "ecs:DescribeInstances",
    "ecs:DescribeSnapshots",
    "ecs:CreateImage",
    "ecs:DescribeImages"
  ],
  "image-delete": [
    "ecs:DeleteImage",
    "ecs:DescribeImages"
  ],
  "image-export": [
    "ecs:ExportImage",
    "ecs:DescribeImages"
  ],
  "image-import": [
    "ecs:ImportImage",
    "ecs:DescribeImages"
  ],
  "image-read": [
    "ecs:DescribeImages"
  ],
  "image-share": [
    "ecs:*ImageSharePermission"
  ],
  "image-update": [
    "ecs:ModifyImageAttribute",
    "ecs:DescribeImages"
  ],
  "instance-all": [
    "ecs:*Instance*",
    "ecs:TagResources",
    "ecs:UntagResources",
    "ecs:DecribeInstance*",
    "ecs:JoinSecurityGroup",
    "ecs:LeaveSecurityGroup",
    "ecs:AttachKeyPair",
    "ecs:ReplaceSystemDisk",
    "ecs:AllocatePublicIpAddress",
    "ecs:DescribeUserData"
  ],
  "instance-create": [
    "ecs:DescribeAvailableResource",
    "vpc:DescribeVSwitchAttributes",
    "ecs:DescribeZones",
    "ecs:DescribeSecurityGroupAttribute",
    "ecs:RunInstances",
    "ecs:UntagResources",
    "ecs:TagResources",
    "ecs:DescribeDisks",
    "ecs:JoinSecurityGroup",
    "ecs:LeaveSecurityGroup",
    "kms:Decrypt",
    "ecs:ModifyInstanceAutoReleaseTime",
    "ecs:ModifyInstanceAutoRenewAttribute",
    "ecs:DescribeInstances",
    "ecs:DescribeUserData",
    "ecs:DescribeInstanceRamRole",
    "ecs:DescribeInstanceAutoRenewAttribute"
  ],
  "instance-delete": [
    "ecs:DeleteInstance",
    "ecs:ModifyInstanceChargeType",
    "ecs:StopInstance",
    "ecs:DescribeInstances"
  ],
  "instance-read": [
    "ecs:DescribeInstances",
    "ecs:DescribeUserData",
    "ecs:DescribeInstanceRamRole",
    "ecs:DescribeInstanceAutoRenewAttribute"
  ],
  "instance-update": [
    "ecs:UntagResources",
    "ecs:TagResources",
    "ecs:DescribeDisks",
    "ecs:JoinSecurityGroup",
    "ecs:LeaveSecurityGroup",
    "ecs:AttachKeyPair",
    "ecs:ModifyInstance*",
    "ecs:ReplaceSystemDisk",
    "ecs:ModifyPrepayInstanceSpec",
    "ecs:StopInstance",
    "ecs:StartInstance",
    "ecs:AllocatePublicIpAddress",
    "ecs:DescribeInstances",
    "ecs:DescribeUserData",
    "ecs:DescribeInstanceRamRole",
    "ecs:DescribeInstanceAutoRenewAttribute"
  ],
  "oss-bucket-all": [
    "oss:*Bucket*"
  ],
  "oss-bucket-create": [
    "oss:ListBuckets",
    "oss:CreateBucket",
    "oss:SetBucketACL",
    "oss:*BucketCORS",
    "oss:*BucketWebsite",
    "oss:*BucketLogging",
    "oss:*BucketReferer",
    "oss:*BucketLifecycle",
    "oss:*BucketEncryption",
    "oss:*BucketTagging",
    "oss:SetBucketVersioning",
    "oss:GetBucketInfo"
  ],
  "oss-bucket-delete": [
    "oss:ListBuckets",
    "oss:DeleteBucket",
    "oss:GetBucketInfo"
  ],
  "oss-bucket-read": [
    "oss:GetBucket*"
  ],
  "oss-bucket-update": [
    "oss:SetBucketACL",
    "oss:*BucketCORS",
    "oss:*BucketWebsite",
    "oss:*BucketLogging",
    "oss:*BucketReferer",
    "oss:*BucketLifecycle",
    "oss:*BucketEncryption",
    "oss:*BucketTagging",
    "oss:SetBucketVersioning",
    "oss:GetBucketInfo"
  ],
  "security-group-all": [
    "ecs:*SecurityGroup*",
    "ecs:UntagResources",
    "ecs:TagResources"
  ],
  "security-group-create": [
    "ecs:CreateSecurityGroup",
    "ecs:UntagResources",
    "ecs:TagResources",
    "ecs:ModifySecurityGroupPolicy",
    "ecs:Describe*"
  ],
  "security-group-delete": [
    "ecs:DeleteSecurityGroup",
    "ecs:DescribeSecurityGroupAttribute"
  ],
  "security-group-read": [
    "ecs:DescribeSecurityGroupAttribute",
    "ecs:DescribeSecurityGroups",
    "ecs:DescribeTags"
  ],
  "security-group-rule-all": [
    "ecs:AuthorizeSecurityGroup*",
    "ecs:ModifySecurityGroupRule",
    "ecs:ModifySecurityGroupEgressRule",
    "ecs:RevokeSecurityGroup*",
    "ecs:DescribeSecurityGroupAttribute"
  ],
  "security-group-rule-create": [
    "ecs:AuthorizeSecurityGroup*",
    "ecs:DescribeSecurityGroupAttribute"
  ],
  "security-group-rule-delete": [
    "ecs:RevokeSecurityGroup*",
    "ecs:DescribeSecurityGroupAttribute"
  ],
  "security-group-rule-read": [
    "ecs:DescribeSecurityGroupAttribute"
  ],
  "security-group-rule-update": [
    "ecs:ModifySecurityGroupRule",
    "ecs:ModifySecurityGroupEgressRule",
    "ecs:DescribeSecurityGroupAttribute"
  ],
  "security-group-update": [
    "ecs:UntagResources",
    "ecs:TagResources",
    "ecs:ModifySecurityGroupPolicy",
    "ecs:Describe*"
  ],
  "slb-all": [
    "slb:*LoadBalancer*",
    "slb:UntagResources",
    "slb:TagResources",
    "slb:ListTagResources"
  ],
  "slb-create": [
    "slb:CreateLoadBalancer",
    "slb:UntagResources",
    "slb:TagResources",
    "slb:DescribeLoadBalancerAttribute",
    "slb:ListTagResources"
  ],
  "slb-delete": [
    "slb:DeleteLoadBalancer",
    "slb:DescribeLoadBalancerAttribute"
  ],
  "slb-read": [
    "slb:DescribeLoadBalancerAttribute",
    "slb:ListTagResources"
  ],
  "slb-update": [
    "slb:UntagResources",
    "slb:TagResources",
    "slb:SetLoadBalancer*",
    "slb:ModifyLoadBalancer*",
    "slb:DescribeLoadBalancerAttribute",
    "slb:ListTagResources"
  ],
  "vpc-all": [
    "vpc:*Vpc*",
    "vpc:UntagResources",
    "vpc:TagResources",
    "vpc:ListTagResources",
    "vpc:DescribeVpcAttribute",
    "vpc:DescribeRouteTables"
  ],
  "vpc-create": [
    "vpc:CreateVpc",
    "vpc:UntagResources",
    "vpc:TagResources",
    "vpc:ModifyVpcAttribute",
    "vpc:DescribeVpcAttribute",
    "vpc:ListTagResources",
    "vpc:DescribeRouteTables"
  ],
  "vpc-delete": [
    "vpc:DeleteVpc",
    "vpc:DescribeVpcAttribute"
  ],
  "vpc-read": [
    "vpc:DescribeVpcAttribute",
    "vpc:ListTagResources",
    "vpc:DescribeRouteTables"
  ],
  "vpc-update": [
    "vpc:UntagResources",
    "vpc:TagResources",
    "vpc:ModifyVpcAttribute",
    "vpc:DescribeVpcAttribute",
    "vpc:ListTagResources",
    "vpc:DescribeRouteTables"
  ],
  "vswitch-create": [
    "vpc:CreateVSwitch",
    "vpc:UntagResources",
    "vpc:TagResources",
    "vpc:DescribeVSwitchAttributes",
    "vpc:ListTagResources"
  ],
  "vswitch-delete": [
    "vpc:DeleteVSwitch",
    "vpc:DescribeVSwitchAttributes"
  ],
  "vswitch-read": [
    "vpc:DescribeVSwitchAttributes",
    "vpc:ListTagResources"
  ],
  "vswitch-update": [
    "vpc:UntagResources",
    "vpc:TagResources",
    "vpc:ModifyVSwitchAttribute",
    "vpc:DescribeVSwitchAttributes",
    "vpc:ListTagResources"
  ],
  "vswithch-all": [
    "vpc:*VSwitch*",
    "vpc:UntagResources",
    "vpc:TagResources",
    "vpc:ListTagResources"
  ]
}
}

variable "policies" {
  type        = list(map(string))
  description = "List Map of known policy actions. Each item can include the following field: `name`(the policy name prefix, default to a name with 'terraform-ram-policy-' prefix), `actions`(list of the custom actions used to create a ram policy), `defined_actions`(list of the defined actions used to create a ram policy), `resources`(list of the resources used to create a ram policy, default to [*]), `effect`(Allow or Deny, default to Allow), `force`(whether to delete ram policy forcibly, default to true)."
  default     = []
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}



module "ram-policy" {
  source = "terraform-aws-modules/ram-policy/aws"
  version = "1.2.0"
  create = var.create
  defined_actions = var.defined_actions
  policies = var.policies
  profile = var.profile
  region = var.region
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
}

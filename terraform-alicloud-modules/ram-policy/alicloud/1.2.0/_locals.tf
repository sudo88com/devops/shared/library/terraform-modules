
locals {
  create = var.create
  defined_actions = var.defined_actions
  policies = var.policies
  profile = var.profile
  region = var.region
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
}

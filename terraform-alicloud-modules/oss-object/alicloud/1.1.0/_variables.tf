
variable "bucketName" {
  type        = string
  description = "The name of the bucket to put the file in."
  default     = ""
}

variable "object-key" {
  type        = string
  description = "The name of the object once it is in the bucket."
  default     = ""
}

variable "sourceFile" {
  type        = string
  description = "The path to the source file being uploaded to the bucket."
  default     = ""
}


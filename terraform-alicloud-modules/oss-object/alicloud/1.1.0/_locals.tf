
locals {
  bucketName = var.bucketName
  object-key = var.object-key
  sourceFile = var.sourceFile
}

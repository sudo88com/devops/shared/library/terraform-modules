
module "oss-object" {
  source = "terraform-aws-modules/oss-object/aws"
  version = "1.1.0"
  bucketName = var.bucketName
  object-key = var.object-key
  sourceFile = var.sourceFile
}

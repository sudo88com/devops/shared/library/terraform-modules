
module "hbase" {
  source = "terraform-aws-modules/hbase/aws"
  version = "1.1.0"
  auto_renew = var.auto_renew
  availability_zone = var.availability_zone
  core_disk_size = var.core_disk_size
  core_disk_type = var.core_disk_type
  core_instance_type = var.core_instance_type
  duration = var.duration
  engine_version = var.engine_version
  existing_instance_id = var.existing_instance_id
  instance_name = var.instance_name
  master_instance_type = var.master_instance_type
  pay_type = var.pay_type
  profile = var.profile
  region = var.region
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  vswitch_id = var.vswitch_id
}


variable "auto_renew" {
  type        = string
  description = "Auto to renew cluster, valid when pay_type = PrePaid."
  default     = "false"
}

variable "availability_zone" {
  type        = string
  description = "The available zone to launch hbase instance, Valid value: cn-hangzhou-f, cn-shanghai-d, cn-beijing-e, cn-shenzhen-b, cn-zhangjiakou-a, cn-hongkong-b, ap-southeast-1a, ap-northeast-1b, eu-central-1a"
  default     = ""
}

variable "core_disk_size" {
  type        = number
  description = "Core disk size, unit: GB, disk size per core node; all disk size = 1 * core_disk_size(1 * 400 =400GB)."
  default     = 400
}

variable "core_disk_type" {
  type        = string
  description = "Core node disk type. Valid value:cloud_ssd, cloud_efficiency."
  default     = "cloud_ssd"
}

variable "core_instance_type" {
  type        = string
  description = "The master instance type. Valid value: 'hbase.n1.small', 'hbase.sn1ne.large', 'hbase.sn1ne.xlarge' and so on."
  default     = "hbase.sn1ne.large"
}

variable "duration" {
  type        = string
  description = "The duration that you will buy HBase cluster, valid when pay_type = PrePaid."
  default     = 1
}

variable "engine_version" {
  type        = string
  description = "The version number of the database. Valid value: hbase:'1.1', '2.0'."
  default     = "2.0"
}

variable "existing_instance_id" {
  type        = string
  description = "The Id of an existing HBase instance. If set, the `create` will be ignored. "
  default     = ""
}

variable "instance_name" {
  type        = string
  description = "Display name of the instance, [2, 128] English or Chinese characters, must start with a letter or Chinese in size, can contain numbers, '_' or '.', '-'."
  default     = "tf-module-hbase-single"
}

variable "master_instance_type" {
  type        = string
  description = "The master instance type. Valid value: 'hbase.n1.small', 'hbase.sn1ne.large', 'hbase.sn1ne.xlarge' and so on."
  default     = "hbase.sn1ne.large"
}

variable "pay_type" {
  type        = string
  description = "Pay type, Valid value:Prepaid: The subscription billing method is used, Postpaid: The pay-as-you-go billing method is used."
  default     = "PostPaid"
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "vswitch_id" {
  type        = string
  description = "VSwitch variables, if vswitch_id is empty, then the net_type = classic."
  default     = ""
}


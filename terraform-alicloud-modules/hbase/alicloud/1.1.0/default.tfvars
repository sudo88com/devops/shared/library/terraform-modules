
auto_renew = "false"

availability_zone = ""

core_disk_size = 400

core_disk_type = "cloud_ssd"

core_instance_type = "hbase.sn1ne.large"

duration = 1

engine_version = "2.0"

existing_instance_id = ""

instance_name = "tf-module-hbase-single"

master_instance_type = "hbase.sn1ne.large"

pay_type = "PostPaid"

profile = ""

region = ""

shared_credentials_file = ""

skip_region_validation = false

vswitch_id = ""


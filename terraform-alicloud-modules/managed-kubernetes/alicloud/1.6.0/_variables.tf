
variable "availability_zones" {
  type        = list(string)
  description = "List available zone ids used to create several new vswitches when 'vswitch_ids' is not specified. If not set, data source `alicloud_zones` will return one automatically."
  default     = []
}

variable "client_cert_path" {
  type        = string
  description = "The path of client certificate, like ~/.kube/client-cert.pem"
  default     = ""
}

variable "client_key_path" {
  type        = string
  description = "The path of client key, like ~/.kube/client-key.pem"
  default     = ""
}

variable "cluster_addons" {
  type        = list(object({
    name   = string
    config = string
  }))
  description = "Addon components in kubernetes cluster"
  default     = []
}

variable "cluster_ca_cert_path" {
  type        = string
  description = "The path of cluster ca certificate, like ~/.kube/cluster-ca-cert.pem"
  default     = ""
}

variable "cluster_spec" {
  type        = string
  description = "The cluster specifications of kubernetes cluster. Valid values: ack.standard | ack.pro.small "
  default     = ""
}

variable "cpu_core_count" {
  type        = number
  description = "CPU core count is used to fetch instance types."
  default     = 1
}

variable "k8s_name_prefix" {
  type        = string
  description = "The name prefix used to create managed kubernetes cluster."
  default     = "terraform-alicloud-managed-kubernetes"
}

variable "k8s_pod_cidr" {
  type        = string
  description = "The kubernetes pod cidr block. It cannot be equals to vpc's or vswitch's and cannot be in them. If vpc's cidr block is `172.16.XX.XX/XX`, it had better to `192.168.XX.XX/XX` or `10.XX.XX.XX/XX`."
  default     = "172.20.0.0/16"
}

variable "k8s_service_cidr" {
  type        = string
  description = "The kubernetes service cidr block. It cannot be equals to vpc's or vswitch's or pod's and cannot be in them. Its setting rule is same as `k8s_pod_cidr`."
  default     = "172.21.0.0/20"
}

variable "kubernetes_version" {
  type        = string
  description = "Desired Kubernetes version"
  default     = ""
}

variable "memory_size" {
  type        = number
  description = "Memory size used to fetch instance types."
  default     = 2
}

variable "new_eip_bandwidth" {
  type        = number
  description = "The bandwidth used to create a new EIP when 'new_vpc' is true."
  default     = 50
}

variable "new_nat_gateway" {
  type        = bool
  description = "Seting it to true can create a new nat gateway automatically in a existing VPC. If 'new_vpc' is true, it will be ignored."
  default     = false
}

variable "new_vpc" {
  type        = bool
  description = "Create a new vpc for this module."
  default     = false
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.5.0)The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.5.0)The region ID used to launch this module resources. If not set, it will be sourced from followed by ALICLOUD_REGION environment variable and profile."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.5.0)This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.5.0)Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "vpc_cidr" {
  type        = string
  description = "The cidr block used to launch a new vpc."
  default     = "192.168.0.0/16"
}

variable "vswitch_cidrs" {
  type        = list(string)
  description = "List cidr blocks used to create several new vswitches when 'new_vpc' is true."
  default     = [
  "192.168.1.0/24"
]
}

variable "vswitch_ids" {
  type        = list(string)
  description = "List Ids of existing vswitch."
  default     = []
}

variable "worker_disk_category" {
  type        = string
  description = "The system disk category used to launch one or more worker nodes."
  default     = "cloud_efficiency"
}

variable "worker_disk_size" {
  type        = number
  description = "The system disk size used to launch one or more worker nodes."
  default     = 40
}

variable "worker_instance_types" {
  type        = list(string)
  description = "The ecs instance type used to launch worker nodes. If not set, data source `alicloud_instance_types` will return one based on `cpu_core_count` and `memory_size`."
  default     = [
  "ecs.c7.xlarge"
]
}

variable "worker_number" {
  type        = number
  description = "The number of kubernetes cluster work nodes."
  default     = 2
}



availability_zones = []

client_cert_path = ""

client_key_path = ""

cluster_addons = []

cluster_ca_cert_path = ""

cluster_spec = ""

cpu_core_count = 1

k8s_name_prefix = "terraform-alicloud-managed-kubernetes"

k8s_pod_cidr = "172.20.0.0/16"

k8s_service_cidr = "172.21.0.0/20"

kubernetes_version = ""

memory_size = 2

new_eip_bandwidth = 50

new_nat_gateway = false

new_vpc = false

profile = ""

region = ""

shared_credentials_file = ""

skip_region_validation = false

vpc_cidr = "192.168.0.0/16"

vswitch_cidrs = [
  "192.168.1.0/24"
]

vswitch_ids = []

worker_disk_category = "cloud_efficiency"

worker_disk_size = 40

worker_instance_types = [
  "ecs.c7.xlarge"
]

worker_number = 2



variable "account_password" {
  type        = string
  description = "Password of the root account. It is a string of 6 to 32 characters and is composed of letters, numbers, and underlines"
  default     = ""
}

variable "alarm_rule_contact_groups" {
  type        = list(string)
  description = "List contact groups of the alarm rule, which must have been created on the console. "
  default     = []
}

variable "alarm_rule_effective_interval" {
  type        = string
  description = "The interval of effecting alarm rule. It foramt as 'hh:mm-hh:mm', like '0:00-4:00'."
  default     = "0:00-2:00"
}

variable "alarm_rule_name" {
  type        = string
  description = "The alarm rule name. "
  default     = ""
}

variable "alarm_rule_operator" {
  type        = string
  description = "Alarm comparison operator. Valid values: ['<=', '<', '>', '>=', '==', '!=']. Default to '=='. "
  default     = "=="
}

variable "alarm_rule_period" {
  type        = number
  description = "Index query cycle, which must be consistent with that defined for metrics. Default to 300, in seconds. "
  default     = 300
}

variable "alarm_rule_silence_time" {
  type        = number
  description = "Notification silence period in the alarm state, in seconds. Valid value range: [300, 86400]. Default to 86400. "
  default     = 86400
}

variable "alarm_rule_statistics" {
  type        = string
  description = "Statistical method. It must be consistent with that defined for metrics. Valid values: ['Average', 'Minimum', 'Maximum']. Default to 'Average'. "
  default     = "Average"
}

variable "alarm_rule_threshold" {
  type        = string
  description = "Alarm threshold value, which must be a numeric value currently. "
  default     = ""
}

variable "alarm_rule_triggered_count" {
  type        = number
  description = "Number of consecutive times it has been detected that the values exceed the threshold. Default to 3. "
  default     = 3
}

variable "backup_period" {
  type        = list(string)
  description = "MongoDB Instance backup period. It is required when backup_time was existed. Valid values: [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday]. Default to [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday]. "
  default     = []
}

variable "backup_time" {
  type        = string
  description = "MongoDB instance backup time. It is required when backup_period was existed. In the format of HH:mmZ- HH:mmZ. Time setting interval is one hour. Default to a random time, like '23:00Z-24:00Z'. "
  default     = "23:00Z-24:00Z"
}

variable "create" {
  type        = bool
  description = "Whether to use an existing MongoDB. If false, you can use a existing Mongodb instance by setting `existing_instance_id`. "
  default     = true
}

variable "db_instance_class" {
  type        = string
  description = "The specification of the instance. For more information about the value, see https://www.alibabacloud.com/help/doc-detail/57141.htm"
  default     = ""
}

variable "db_instance_storage" {
  type        = number
  description = "The storage space of the instance. Valid values: 10 to 3000. Unit: GB. You can only specify this value in 10 GB increments. "
  default     = 10
}

variable "enable_alarm_rule" {
  type        = bool
  description = "Whether to enable alarm rule. Default to true. "
  default     = true
}

variable "engine_version" {
  type        = string
  description = "The version number of the database. Valid value: 3.2, 3.4, 4.0. "
  default     = ""
}

variable "existing_instance_id" {
  type        = string
  description = "The Id of an existing Mongodb instance. If set, the `create` will be ignored. "
  default     = ""
}

variable "instance_charge_type" {
  type        = string
  description = "The billing method of the instance. Valid values are Prepaid, PostPaid, Default to PostPaid"
  default     = "PostPaid"
}

variable "instance_id" {
  type        = string
  description = "`(Deprecated)` It has been deprecated from version 1.2.0 and use `existing_instance_id` instead. "
  default     = ""
}

variable "name" {
  type        = string
  description = " The name of DB instance. It a string of 2 to 256 characters"
  default     = ""
}

variable "period" {
  type        = string
  description = "The duration that you will buy DB instance (in month). It is valid when instance_charge_type is PrePaid. Valid values: [1~9], 12, 24, 36. Default to 1"
  default     = 1
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.4.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable. "
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.4.0) The region used to launch this module resources. "
  default     = ""
}

variable "replication_factor" {
  type        = number
  description = "The number of nodes in the replica set instance. Valid values: 3, 5, 7. Default value: 3. "
  default     = 3
}

variable "security_ip_list" {
  type        = list(string)
  description = " List of IP addresses allowed to access all databases of an instance. The list contains up to 1,000 IP addresses, separated by commas. Supported formats include 0.0.0.0/0, 10.23.12.24 (IP), and 10.23.12.24/24 (Classless Inter-Domain Routing (CIDR) mode. /24 represents the length of the prefix in an IP address. The range of the prefix length is [1,32]). "
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.4.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used. "
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.4.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet). "
  default     = false
}

variable "storage_engine" {
  type        = string
  description = "The MongoDB storage engine, WiredTiger or RocksDB. Default value: WiredTiger. "
  default     = "WiredTiger"
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the mongodb instance resource. "
  default     = {}
}

variable "vswitch_id" {
  type        = string
  description = "The virtual switch ID to launch DB instances in one VPC. "
  default     = ""
}

variable "zone_id" {
  type        = string
  description = "The ID of the zone. You can refer to https://www.alibabacloud.com/help/doc-detail/61933.htm. "
  default     = ""
}



active = true

associate_public_ip_address = false

cpu_core_count = 2

create_lifecycle_hook = false

create_scaling_configuration = true

data_disk_category = ""

data_disk_size = ""

data_disks = []

db_instance_ids = []

default_cooldown = 300

enable = true

filter_with_name_regex = ""

filter_with_tags = {}

force_delete = false

heartbeat_timeout = 600

hook_action_policy = "CONTINUE"

image_id = ""

image_name_regex = "^ubuntu_18.*64"

image_owners = "system"

instance_name = ""

instance_type = ""

instance_types = []

internet_charge_type = "PayByTraffic"

internet_max_bandwidth_in = 200

internet_max_bandwidth_out = 0

key_name = ""

kms_encrypted_password = ""

kms_encryption_context = {}

lifecycle_hook_name = ""

lifecycle_transition = "SCALE_IN"

loadbalancer_ids = []

max_size = 3

memory_size = 4

min_size = 1

mns_queue_name = ""

mns_topic_name = ""

multi_az_policy = 

notification_metadata = ""

on_demand_base_capacity = 

on_demand_percentage_above_base_capacity = 

password = ""

password_inherit = false

profile = ""

rds_name_regex = ""

rds_tags = {}

region = ""

removal_policies = [
  "OldestScalingConfiguration",
  "OldestInstance"
]

role_name = ""

scaling_configuration_name = ""

scaling_group_id = ""

scaling_group_name = ""

security_group_id = ""

security_group_ids = []

sg_name_regex = ""

sg_tags = {}

shared_credentials_file = ""

skip_region_validation = false

slb_name_regex = ""

slb_tags = {}

spot_instance_pools = 

spot_instance_remedy = 

system_disk_category = "cloud_efficiency"

system_disk_size = 40

tags = {}

user_data = ""

vswitch_ids = []

vswitch_name_regex = ""

vswitch_tags = {}


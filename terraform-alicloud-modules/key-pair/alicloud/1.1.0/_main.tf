
module "key-pair" {
  source = "terraform-aws-modules/key-pair/aws"
  version = "1.1.0"
  attach = var.attach
  create = var.create
  force = var.force
  instance_ids = var.instance_ids
  key_file = var.key_file
  key_name = var.key_name
  profile = var.profile
  public_key = var.public_key
  region = var.region
  resource_group_id = var.resource_group_id
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  tags = var.tags
}

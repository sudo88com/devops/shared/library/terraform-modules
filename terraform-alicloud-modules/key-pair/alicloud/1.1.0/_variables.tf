
variable "attach" {
  type        = bool
  description = "Whether to create key pair attachment. Default to true."
  default     = true
}

variable "create" {
  type        = bool
  description = "Whether to create key pair. Default to true."
  default     = true
}

variable "force" {
  type        = bool
  description = "Set it to true and it will reboot instances which attached with the key pair to make key pair affect immediately."
  default     = true
}

variable "instance_ids" {
  type        = list(string)
  description = "The list of ECS instance's IDs."
  default     = []
}

variable "key_file" {
  type        = string
  description = "The name of file to save your new key pair's private key. Strongly suggest you to specified it when you creating key pair, otherwise, you wouldn't get its private key ever."
  default     = ""
}

variable "key_name" {
  type        = string
  description = "The key pair's name. It is the only in one Alicloud account."
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "public_key" {
  type        = string
  description = "You can import an existing public key and using Alicloud key pair to manage it."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "resource_group_id" {
  type        = string
  description = "The Id of resource group which the key pair belongs."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip staticvalidation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "tags" {
  type        = map(any)
  description = "A mapping of tags to assign to the resource."
  default     = {}
}


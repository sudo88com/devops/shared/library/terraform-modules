
output "this_disk_capacity_alarm_rule_contact_groups" {
  description = "List contact groups of the alarm rule. "
  value       = module.kafka-cloudmonitor.this_disk_capacity_alarm_rule_contact_groups
}

output "this_disk_capacity_alarm_rule_dimensions" {
  description = "Map of the resources associated with the alarm rule. "
  value       = module.kafka-cloudmonitor.this_disk_capacity_alarm_rule_dimensions
}

output "this_disk_capacity_alarm_rule_effective_interval" {
  description = "The interval of effecting alarm rule. "
  value       = module.kafka-cloudmonitor.this_disk_capacity_alarm_rule_effective_interval
}

output "this_disk_capacity_alarm_rule_enabled" {
  description = "Whether to enable alarm rule. "
  value       = module.kafka-cloudmonitor.this_disk_capacity_alarm_rule_enabled
}

output "this_disk_capacity_alarm_rule_id" {
  description = "The ID of the alarm rule. "
  value       = module.kafka-cloudmonitor.this_disk_capacity_alarm_rule_id
}

output "this_disk_capacity_alarm_rule_metric" {
  description = "Name of the monitoring metrics. "
  value       = module.kafka-cloudmonitor.this_disk_capacity_alarm_rule_metric
}

output "this_disk_capacity_alarm_rule_name" {
  description = "The alarm name. "
  value       = module.kafka-cloudmonitor.this_disk_capacity_alarm_rule_name
}

output "this_disk_capacity_alarm_rule_operator" {
  description = "Alarm comparison operator. "
  value       = module.kafka-cloudmonitor.this_disk_capacity_alarm_rule_operator
}

output "this_disk_capacity_alarm_rule_period" {
  description = "Index query cycle. "
  value       = module.kafka-cloudmonitor.this_disk_capacity_alarm_rule_period
}

output "this_disk_capacity_alarm_rule_project" {
  description = "Monitor project name. "
  value       = module.kafka-cloudmonitor.this_disk_capacity_alarm_rule_project
}

output "this_disk_capacity_alarm_rule_silence_time" {
  description = " Notification silence period in the alarm state. "
  value       = module.kafka-cloudmonitor.this_disk_capacity_alarm_rule_silence_time
}

output "this_disk_capacity_alarm_rule_statistics" {
  description = "Statistical method. "
  value       = module.kafka-cloudmonitor.this_disk_capacity_alarm_rule_statistics
}

output "this_disk_capacity_alarm_rule_threshold" {
  description = "Alarm threshold value."
  value       = module.kafka-cloudmonitor.this_disk_capacity_alarm_rule_threshold
}

output "this_disk_capacity_alarm_rule_triggered_count" {
  description = "Number of trigger alarm. "
  value       = module.kafka-cloudmonitor.this_disk_capacity_alarm_rule_triggered_count
}

output "this_instance_message_input_alarm_rule_contact_groups" {
  description = "List contact groups of the alarm rule. "
  value       = module.kafka-cloudmonitor.this_instance_message_input_alarm_rule_contact_groups
}

output "this_instance_message_input_alarm_rule_dimensions" {
  description = "Map of the resources associated with the alarm rule. "
  value       = module.kafka-cloudmonitor.this_instance_message_input_alarm_rule_dimensions
}

output "this_instance_message_input_alarm_rule_effective_interval" {
  description = "The interval of effecting alarm rule. "
  value       = module.kafka-cloudmonitor.this_instance_message_input_alarm_rule_effective_interval
}

output "this_instance_message_input_alarm_rule_enabled" {
  description = "Whether to enable alarm rule. "
  value       = module.kafka-cloudmonitor.this_instance_message_input_alarm_rule_enabled
}

output "this_instance_message_input_alarm_rule_id" {
  description = "The ID of the alarm rule. "
  value       = module.kafka-cloudmonitor.this_instance_message_input_alarm_rule_id
}

output "this_instance_message_input_alarm_rule_metric" {
  description = "Name of the monitoring metrics. "
  value       = module.kafka-cloudmonitor.this_instance_message_input_alarm_rule_metric
}

output "this_instance_message_input_alarm_rule_name" {
  description = "The alarm name. "
  value       = module.kafka-cloudmonitor.this_instance_message_input_alarm_rule_name
}

output "this_instance_message_input_alarm_rule_operator" {
  description = "Alarm comparison operator. "
  value       = module.kafka-cloudmonitor.this_instance_message_input_alarm_rule_operator
}

output "this_instance_message_input_alarm_rule_period" {
  description = "Index query cycle. "
  value       = module.kafka-cloudmonitor.this_instance_message_input_alarm_rule_period
}

output "this_instance_message_input_alarm_rule_project" {
  description = "Monitor project name. "
  value       = module.kafka-cloudmonitor.this_instance_message_input_alarm_rule_project
}

output "this_instance_message_input_alarm_rule_silence_time" {
  description = " Notification silence period in the alarm state. "
  value       = module.kafka-cloudmonitor.this_instance_message_input_alarm_rule_silence_time
}

output "this_instance_message_input_alarm_rule_statistics" {
  description = "Statistical method. "
  value       = module.kafka-cloudmonitor.this_instance_message_input_alarm_rule_statistics
}

output "this_instance_message_input_alarm_rule_threshold" {
  description = "Alarm threshold value."
  value       = module.kafka-cloudmonitor.this_instance_message_input_alarm_rule_threshold
}

output "this_instance_message_input_alarm_rule_triggered_count" {
  description = "Number of trigger alarm. "
  value       = module.kafka-cloudmonitor.this_instance_message_input_alarm_rule_triggered_count
}

output "this_instance_message_output_alarm_rule_contact_groups" {
  description = "List contact groups of the alarm rule. "
  value       = module.kafka-cloudmonitor.this_instance_message_output_alarm_rule_contact_groups
}

output "this_instance_message_output_alarm_rule_dimensions" {
  description = "Map of the resources associated with the alarm rule. "
  value       = module.kafka-cloudmonitor.this_instance_message_output_alarm_rule_dimensions
}

output "this_instance_message_output_alarm_rule_effective_interval" {
  description = "The interval of effecting alarm rule. "
  value       = module.kafka-cloudmonitor.this_instance_message_output_alarm_rule_effective_interval
}

output "this_instance_message_output_alarm_rule_enabled" {
  description = "Whether to enable alarm rule. "
  value       = module.kafka-cloudmonitor.this_instance_message_output_alarm_rule_enabled
}

output "this_instance_message_output_alarm_rule_id" {
  description = "The ID of the alarm rule. "
  value       = module.kafka-cloudmonitor.this_instance_message_output_alarm_rule_id
}

output "this_instance_message_output_alarm_rule_metric" {
  description = "Name of the monitoring metrics. "
  value       = module.kafka-cloudmonitor.this_instance_message_output_alarm_rule_metric
}

output "this_instance_message_output_alarm_rule_name" {
  description = "The alarm name. "
  value       = module.kafka-cloudmonitor.this_instance_message_output_alarm_rule_name
}

output "this_instance_message_output_alarm_rule_operator" {
  description = "Alarm comparison operator. "
  value       = module.kafka-cloudmonitor.this_instance_message_output_alarm_rule_operator
}

output "this_instance_message_output_alarm_rule_period" {
  description = "Index query cycle. "
  value       = module.kafka-cloudmonitor.this_instance_message_output_alarm_rule_period
}

output "this_instance_message_output_alarm_rule_project" {
  description = "Monitor project name. "
  value       = module.kafka-cloudmonitor.this_instance_message_output_alarm_rule_project
}

output "this_instance_message_output_alarm_rule_silence_time" {
  description = " Notification silence period in the alarm state. "
  value       = module.kafka-cloudmonitor.this_instance_message_output_alarm_rule_silence_time
}

output "this_instance_message_output_alarm_rule_statistics" {
  description = "Statistical method. "
  value       = module.kafka-cloudmonitor.this_instance_message_output_alarm_rule_statistics
}

output "this_instance_message_output_alarm_rule_threshold" {
  description = "Alarm threshold value."
  value       = module.kafka-cloudmonitor.this_instance_message_output_alarm_rule_threshold
}

output "this_instance_message_output_alarm_rule_triggered_count" {
  description = "Number of trigger alarm. "
  value       = module.kafka-cloudmonitor.this_instance_message_output_alarm_rule_triggered_count
}

output "this_kafka_consumer_group_id" {
  description = ""
  value       = module.kafka-cloudmonitor.this_kafka_consumer_group_id
}

output "this_kafka_instance_id" {
  description = ""
  value       = module.kafka-cloudmonitor.this_kafka_instance_id
}

output "this_vswitch_id" {
  description = ""
  value       = module.kafka-cloudmonitor.this_vswitch_id
}

output "this_zone_id" {
  description = ""
  value       = module.kafka-cloudmonitor.this_zone_id
}


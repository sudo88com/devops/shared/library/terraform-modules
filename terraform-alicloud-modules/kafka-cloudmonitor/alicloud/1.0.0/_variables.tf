
variable "alarm_disk_capacity_metric" {
  type        = string
  description = "Name of the monitoring metrics corresponding to a project, such as 'CPUUtilization' and so on. "
  default     = "instance_disk_capacity"
}

variable "alarm_instance_message_input_metric" {
  type        = string
  description = "Name of the monitoring metrics corresponding to a project, such as 'CPUUtilization' and so on. "
  default     = "instance_disk_capacity"
}

variable "alarm_instance_message_output_metric" {
  type        = string
  description = "Name of the monitoring metrics corresponding to a project, such as 'CPUUtilization' and so on. "
  default     = "instance_disk_capacity"
}

variable "alarm_user_id" {
  type        = string
  description = "The alarm of userId."
  default     = "test"
}

variable "consumer_id" {
  type        = string
  description = "The specification of kafka consumer_id."
  default     = "CID-alikafkaGroupDatasourceName"
}

variable "deploy_type" {
  type        = string
  description = "The deploy type of the instance. Currently only support two deploy type, 4: eip/vpc instance, 5: vpc instance."
  default     = "5"
}

variable "disk_capacity_alarm_rule_contact_groups" {
  type        = list(string)
  description = "List contact groups of the alarm rule, which must have been created on the console. "
  default     = [
  "test-web-server"
]
}

variable "disk_capacity_alarm_rule_effective_interval" {
  type        = string
  description = "The interval of effecting alarm rule. It foramt as 'hh:mm-hh:mm', like '0:00-4:00'."
  default     = "0:00-2:00"
}

variable "disk_capacity_alarm_rule_name" {
  type        = string
  description = "The alarm rule name. "
  default     = "disk_capacity_test_rule_name"
}

variable "disk_capacity_alarm_rule_operator" {
  type        = string
  description = "Alarm comparison operator. Valid values: ['<=', '<', '>', '>=', '==', '!=']. Default to '=='. "
  default     = "\u003e="
}

variable "disk_capacity_alarm_rule_period" {
  type        = number
  description = "Index query cycle, which must be consistent with that defined for metrics. Default to 300, in seconds. "
  default     = 300
}

variable "disk_capacity_alarm_rule_silence_time" {
  type        = number
  description = "Notification silence period in the alarm state, in seconds. Valid value range: [300, 86400]. Default to 86400. "
  default     = 86400
}

variable "disk_capacity_alarm_rule_statistics" {
  type        = string
  description = "Statistical method. It must be consistent with that defined for metrics. Valid values: ['Average', 'Minimum', 'Maximum']. Default to 'Average'. "
  default     = "Maximum"
}

variable "disk_capacity_alarm_rule_threshold" {
  type        = string
  description = "Alarm threshold value, which must be a numeric value currently. "
  default     = "90"
}

variable "disk_capacity_enable_alarm_rule" {
  type        = bool
  description = "Whether to enable alarm rule. Default to true. "
  default     = true
}

variable "disk_capacity_times" {
  type        = string
  description = "Alarm times value, which must be a numeric value currently. "
  default     = "3"
}

variable "disk_size" {
  type        = string
  description = "The disk size of the instance. When modify this value, it only support adjust to a greater value."
  default     = "500"
}

variable "disk_type" {
  type        = string
  description = "The disk type of the instance. 0: efficient cloud disk,1: SSD."
  default     = "1"
}

variable "instance_message_input_alarm_rule_contact_groups" {
  type        = list(string)
  description = "List contact groups of the alarm rule, which must have been created on the console. "
  default     = [
  "test-web-server"
]
}

variable "instance_message_input_alarm_rule_effective_interval" {
  type        = string
  description = "The interval of effecting alarm rule. It foramt as 'hh:mm-hh:mm', like '0:00-4:00'."
  default     = "0:00-2:00"
}

variable "instance_message_input_alarm_rule_name" {
  type        = string
  description = "The alarm rule name. "
  default     = "disk_capacity_test_rule_name"
}

variable "instance_message_input_alarm_rule_operator" {
  type        = string
  description = "Alarm comparison operator. Valid values: ['<=', '<', '>', '>=', '==', '!=']. Default to '=='. "
  default     = "\u003e="
}

variable "instance_message_input_alarm_rule_period" {
  type        = number
  description = "Index query cycle, which must be consistent with that defined for metrics. Default to 300, in seconds. "
  default     = 300
}

variable "instance_message_input_alarm_rule_silence_time" {
  type        = number
  description = "Notification silence period in the alarm state, in seconds. Valid value range: [300, 86400]. Default to 86400. "
  default     = 86400
}

variable "instance_message_input_alarm_rule_statistics" {
  type        = string
  description = "Statistical method. It must be consistent with that defined for metrics. Valid values: ['Average', 'Minimum', 'Maximum']. Default to 'Average'. "
  default     = "Value"
}

variable "instance_message_input_alarm_rule_threshold" {
  type        = string
  description = "Alarm threshold value, which must be a numeric value currently. "
  default     = "90"
}

variable "instance_message_input_enable_alarm_rule" {
  type        = bool
  description = "Whether to enable alarm rule. Default to true. "
  default     = true
}

variable "instance_message_input_times" {
  type        = string
  description = "Alarm times value, which must be a numeric value currently. "
  default     = "3"
}

variable "instance_message_output_alarm_rule_contact_groups" {
  type        = list(string)
  description = "List contact groups of the alarm rule, which must have been created on the console. "
  default     = [
  "test-web-server"
]
}

variable "instance_message_output_alarm_rule_effective_interval" {
  type        = string
  description = "The interval of effecting alarm rule. It foramt as 'hh:mm-hh:mm', like '0:00-4:00'."
  default     = "0:00-2:00"
}

variable "instance_message_output_alarm_rule_name" {
  type        = string
  description = "The alarm rule name. "
  default     = "disk_capacity_test_rule_name"
}

variable "instance_message_output_alarm_rule_operator" {
  type        = string
  description = "Alarm comparison operator. Valid values: ['<=', '<', '>', '>=', '==', '!=']. Default to '=='. "
  default     = "\u003e="
}

variable "instance_message_output_alarm_rule_period" {
  type        = number
  description = "Index query cycle, which must be consistent with that defined for metrics. Default to 300, in seconds. "
  default     = 300
}

variable "instance_message_output_alarm_rule_silence_time" {
  type        = number
  description = "Notification silence period in the alarm state, in seconds. Valid value range: [300, 86400]. Default to 86400. "
  default     = 86400
}

variable "instance_message_output_alarm_rule_statistics" {
  type        = string
  description = "Statistical method. It must be consistent with that defined for metrics. Valid values: ['Average', 'Minimum', 'Maximum']. Default to 'Average'. "
  default     = "Value"
}

variable "instance_message_output_alarm_rule_threshold" {
  type        = string
  description = "Alarm threshold value, which must be a numeric value currently. "
  default     = "90"
}

variable "instance_message_output_enable_alarm_rule" {
  type        = bool
  description = "Whether to enable alarm rule. Default to true. "
  default     = true
}

variable "instance_message_output_times" {
  type        = string
  description = "Alarm times value, which must be a numeric value currently. "
  default     = "3"
}

variable "internet_max_bandwidth_out" {
  type        = number
  description = "The specification of the internet max bandwidth out."
  default     = 10
}

variable "io_max" {
  type        = string
  description = "The max value of io of the instance. When modify this value, it only support adjust to a greater value."
  default     = "20"
}

variable "kafka_instance_name" {
  type        = string
  description = "The specification of kafka instance name."
  default     = "kafka_instance_name_test"
}

variable "topic_quota" {
  type        = string
  description = "The max num of topic can be create of the instance. When modify this value, it only adjust to a greater value."
  default     = "50"
}

variable "vswitch_id" {
  type        = string
  description = "The specification of vswitch_id."
  default     = "vsw-bp1lah4d567n4wwd59zle"
}

variable "zone_id" {
  type        = string
  description = "The specification of zone msg."
  default     = "cn-hangzhou-e"
}



alarm_disk_capacity_metric = "instance_disk_capacity"

alarm_instance_message_input_metric = "instance_disk_capacity"

alarm_instance_message_output_metric = "instance_disk_capacity"

alarm_user_id = "test"

consumer_id = "CID-alikafkaGroupDatasourceName"

deploy_type = "5"

disk_capacity_alarm_rule_contact_groups = [
  "test-web-server"
]

disk_capacity_alarm_rule_effective_interval = "0:00-2:00"

disk_capacity_alarm_rule_name = "disk_capacity_test_rule_name"

disk_capacity_alarm_rule_operator = "\u003e="

disk_capacity_alarm_rule_period = 300

disk_capacity_alarm_rule_silence_time = 86400

disk_capacity_alarm_rule_statistics = "Maximum"

disk_capacity_alarm_rule_threshold = "90"

disk_capacity_enable_alarm_rule = true

disk_capacity_times = "3"

disk_size = "500"

disk_type = "1"

instance_message_input_alarm_rule_contact_groups = [
  "test-web-server"
]

instance_message_input_alarm_rule_effective_interval = "0:00-2:00"

instance_message_input_alarm_rule_name = "disk_capacity_test_rule_name"

instance_message_input_alarm_rule_operator = "\u003e="

instance_message_input_alarm_rule_period = 300

instance_message_input_alarm_rule_silence_time = 86400

instance_message_input_alarm_rule_statistics = "Value"

instance_message_input_alarm_rule_threshold = "90"

instance_message_input_enable_alarm_rule = true

instance_message_input_times = "3"

instance_message_output_alarm_rule_contact_groups = [
  "test-web-server"
]

instance_message_output_alarm_rule_effective_interval = "0:00-2:00"

instance_message_output_alarm_rule_name = "disk_capacity_test_rule_name"

instance_message_output_alarm_rule_operator = "\u003e="

instance_message_output_alarm_rule_period = 300

instance_message_output_alarm_rule_silence_time = 86400

instance_message_output_alarm_rule_statistics = "Value"

instance_message_output_alarm_rule_threshold = "90"

instance_message_output_enable_alarm_rule = true

instance_message_output_times = "3"

internet_max_bandwidth_out = 10

io_max = "20"

kafka_instance_name = "kafka_instance_name_test"

topic_quota = "50"

vswitch_id = "vsw-bp1lah4d567n4wwd59zle"

zone_id = "cn-hangzhou-e"


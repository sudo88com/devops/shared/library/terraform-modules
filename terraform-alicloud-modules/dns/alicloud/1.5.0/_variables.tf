
variable "add_records" {
  type        = bool
  description = "Whether to add records to dns. Default to true."
  default     = true
}

variable "create" {
  type        = bool
  description = "Whether to create a domain. Default to true."
  default     = true
}

variable "create_group" {
  type        = bool
  description = "Whether to create a NDS group. Default to false."
  default     = false
}

variable "domain_name" {
  type        = string
  description = "The name of domain."
  default     = ""
}

variable "existing_domain_name" {
  type        = string
  description = "The name of an existing domain. If set, 'create' will be ignored."
  default     = ""
}

variable "existing_group_name" {
  type        = string
  description = "Id of the group in which the domain will add. If not supplied, then use default group."
  default     = ""
}

variable "group_name" {
  type        = string
  description = "DNS domain's parrent group name, If not set, a default name with prefix 'terraform-dns-group-' will be returned."
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.5.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "record_list" {
  type        = list(object({
    name        = string
    host_record = string
    type        = string
    ttl         = number
    value       = string
    priority    = number
  }))
  description = "(Deprecated) It has been deprecated from 1.3.0, and use 'records' instead."
  default     = []
}

variable "records" {
  type        = list(map(string))
  description = "DNS record list.Each item can contains keys: 'rr'(The host record of the domain record. 'name' has been deprecated from 1.3.0, and use 'rr' instead.),'type'(The type of the domain. Valid values: A, NS, MX, TXT, CNAME, SRV, AAAA, CAA, REDIRECT_URL, FORWORD_URL. Default to A.),'value'(The value of domain record),'priority'(The priority of domain record. Valid values are `[1-10]`. When the `type` is `MX`, this parameter is required.),'ttl'(The ttl of the domain record. Default to 600.),'line'(The resolution line of domain record. Default value is default.)."
  default     = []
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.5.0) The region used to launch this module resources."
  default     = ""
}

variable "resource_group_id" {
  type        = string
  description = "The Id of resource group which the NDS belongs."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.5.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.5.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}



module "dns" {
  source = "terraform-aws-modules/dns/aws"
  version = "1.5.0"
  add_records = var.add_records
  create = var.create
  create_group = var.create_group
  domain_name = var.domain_name
  existing_domain_name = var.existing_domain_name
  existing_group_name = var.existing_group_name
  group_name = var.group_name
  profile = var.profile
  record_list = var.record_list
  records = var.records
  region = var.region
  resource_group_id = var.resource_group_id
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
}

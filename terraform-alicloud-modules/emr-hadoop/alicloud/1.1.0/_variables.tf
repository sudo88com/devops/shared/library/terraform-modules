
variable "charge_type" {
  type        = string
  description = "The instance charge type. Valid values: Prepaid and PostPaid. Default to PostPaid."
  default     = "PostPaid"
}

variable "create" {
  type        = bool
  description = "The switch to control creation of the emr cluster or not."
  default     = false
}

variable "disk_capacity" {
  type        = number
  description = "The host group of ecs mount disk capacity to create emr cluster instance."
  default     = 0
}

variable "disk_type" {
  type        = string
  description = "The host group of ecs mount disk type to create emr cluster instance. Supported value: cloud, cloud_efficiency, cloud_ssd, local_disk, cloud_essd."
  default     = ""
}

variable "emr_cluster_name" {
  type        = string
  description = "The name of this new created emr cluster instance."
  default     = "terraform-test-hadoop-module"
}

variable "emr_version" {
  type        = string
  description = "The version of this new created emr hadoop cluster."
  default     = "EMR-3.24.0"
}

variable "high_availability_enable" {
  type        = bool
  description = "The high availability of this emr cluster is enable or not."
  default     = true
}

variable "host_groups" {
  type        = list(map(string))
  description = "Host groups to attach to the emr cluster instance."
  default     = [
  {
    "disk_count": "1",
    "host_group_name": "master_group",
    "host_group_type": "MASTER",
    "node_count": "2"
  },
  {
    "disk_count": "4",
    "host_group_name": "core_group",
    "host_group_type": "CORE",
    "node_count": "3"
  },
  {
    "disk_count": "4",
    "host_group_name": "task_group",
    "host_group_type": "TASK",
    "node_count": "1"
  }
]
}

variable "instance_type" {
  type        = string
  description = "The host group of ecs instance type to create emr cluster instance."
  default     = ""
}

variable "is_open_public_ip" {
  type        = bool
  description = "The emr cluster ecs instance is open public ip or not."
  default     = true
}

variable "master_pwd" {
  type        = string
  description = "The master password of this new created emr cluster."
  default     = "YourPassword123!"
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "ram_role_name" {
  type        = string
  description = "The role name to bound by this new created emr cluster."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "security_group_id" {
  type        = string
  description = "The security group id used to created emr cluster."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "ssh_enable" {
  type        = bool
  description = "The emr cluster ecs instance is ssh enable or not."
  default     = true
}

variable "support_local_storage" {
  type        = bool
  description = "Whether to support local storage."
  default     = false
}

variable "system_disk_capacity" {
  type        = number
  description = "The host group of ecs mount system disk capacity to create emr cluster instance."
  default     = 0
}

variable "system_disk_type" {
  type        = string
  description = "The host group of ecs mount system disk type to create emr cluster instance. Supported value: cloud, cloud_efficiency, cloud_ssd, local_disk, cloud_essd."
  default     = ""
}

variable "vswitch_id" {
  type        = string
  description = "The vswitch id used to created emr cluster."
  default     = ""
}

variable "zone_id" {
  type        = string
  description = "The zone id used to created emr cluster."
  default     = ""
}


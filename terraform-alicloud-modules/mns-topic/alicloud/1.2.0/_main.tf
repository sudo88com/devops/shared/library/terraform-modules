
module "mns-topic" {
  source = "terraform-aws-modules/mns-topic/aws"
  version = "1.2.0"
  endpoint = var.endpoint
  filter_tag = var.filter_tag
  logging_enabled = var.logging_enabled
  maximum_message_size = var.maximum_message_size
  notify_content_format = var.notify_content_format
  notify_strategy = var.notify_strategy
  region = var.region
  subscription_name = var.subscription_name
  topic_name = var.topic_name
}


variable "endpoint" {
  type        = string
  description = "Describe the terminal address of the message received in this subscription. email format: mail:directmail:XXX@YYY.com ,   queue format: http(s)://AccountId.mns.regionId.aliyuncs.com/, http format: http(s)://www.xxx.com/xxx"
  default     = ""
}

variable "filter_tag" {
  type        = string
  description = "Message Filter Label"
  default     = ""
}

variable "logging_enabled" {
  type        = string
  description = "is log enabled ?"
  default     = false
}

variable "maximum_message_size" {
  type        = string
  description = "This indicates the maximum length, in bytes, of any message body sent to the topic. Valid value range: 1024-65536, i.e., 1K to 64K."
  default     = 65536
}

variable "notify_content_format" {
  type        = string
  description = "The NotifyContentFormat attribute of Subscription. This attribute specifies the content format of the messages pushed to users. the attribute has two value SIMPLIFIED or XML"
  default     = "XML"
}

variable "notify_strategy" {
  type        = string
  description = "The NotifyStrategy attribute of Subscription. This attribute specifies the retry strategy when message sending fails. the attribute has two value EXPONENTIAL_DECAY_RETR or BACKOFF_RETRY "
  default     = "BACKOFF_RETRY"
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "subscription_name" {
  type        = string
  description = "the Subscription Name,Two subcription Name on a single topic in the same region cannot have the same name.A subscription name must start with an English letter or a digit, and can contain English letters, digits, and hyphens, with the length not exceeding 256 characters."
  default     = ""
}

variable "topic_name" {
  type        = string
  description = "Two topics on a single account in the same region cannot have the same name. A topic name must start with an English letter or a digit, and can contain English letters, digits, and hyphens, with the length not exceeding 256 characters."
  default     = ""
}


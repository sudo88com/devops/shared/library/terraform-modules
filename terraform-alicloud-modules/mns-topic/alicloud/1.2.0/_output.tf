
output "subscription_name" {
  description = ""
  value       = module.mns-topic.subscription_name
}

output "topic_name" {
  description = ""
  value       = module.mns-topic.topic_name
}



module "dcdn" {
  source = "terraform-aws-modules/dcdn/aws"
  version = "1.1.0"
  check_url = var.check_url
  create_domain = var.create_domain
  domain_configs = var.domain_configs
  domain_name = var.domain_name
  profile = var.profile
  region = var.region
  resource_group_id = var.resource_group_id
  scope = var.scope
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  sources = var.sources
  status = var.status
}

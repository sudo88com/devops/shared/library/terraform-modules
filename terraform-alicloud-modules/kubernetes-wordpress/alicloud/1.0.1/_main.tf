
module "kubernetes-wordpress" {
  source = "terraform-aws-modules/kubernetes-wordpress/aws"
  version = "1.0.1"
  mysql_password = var.mysql_password
  mysql_version = var.mysql_version
  wordpress_version = var.wordpress_version
}

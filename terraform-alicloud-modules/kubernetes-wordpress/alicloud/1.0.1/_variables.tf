
variable "mysql_password" {
  type        = 
  description = "Please input mysql password."
  default     = ""
}

variable "mysql_version" {
  type        = 
  description = "The version of mysql which wordpress used. Default to 5.6."
  default     = "5.6"
}

variable "wordpress_version" {
  type        = 
  description = "The version of wordpress. Default to 4.7.3."
  default     = "4.7.3"
}


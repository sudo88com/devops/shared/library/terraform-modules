
locals {
  mysql_password = var.mysql_password
  mysql_version = var.mysql_version
  wordpress_version = var.wordpress_version
}

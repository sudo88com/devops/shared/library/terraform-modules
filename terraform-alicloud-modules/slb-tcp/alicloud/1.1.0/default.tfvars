
address_type = "internet"

advanced_setting = {}

bandwidth = 10

create_slb = true

create_tcp_listener = true

existing_slb_id = ""

health_check = {}

internet_charge_type = "PayByTraffic"

listeners = []

master_zone_id = ""

profile = ""

region = ""

servers_of_virtual_server_group = []

shared_credentials_file = ""

skip_region_validation = false

slave_zone_id = ""

specification = ""

tags = {}

use_existing_slb = false

virtual_server_group_name = ""



locals {
  address_type = var.address_type
  advanced_setting = var.advanced_setting
  bandwidth = var.bandwidth
  create_slb = var.create_slb
  create_tcp_listener = var.create_tcp_listener
  existing_slb_id = var.existing_slb_id
  health_check = var.health_check
  internet_charge_type = var.internet_charge_type
  listeners = var.listeners
  master_zone_id = var.master_zone_id
  profile = var.profile
  region = var.region
  servers_of_virtual_server_group = var.servers_of_virtual_server_group
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  slave_zone_id = var.slave_zone_id
  specification = var.specification
  tags = var.tags
  use_existing_slb = var.use_existing_slb
  virtual_server_group_name = var.virtual_server_group_name
}

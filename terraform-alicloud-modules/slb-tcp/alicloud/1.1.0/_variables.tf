
variable "address_type" {
  type        = string
  description = "The type of address. Choices are 'intranet' and 'internet'. Default to 'internet'."
  default     = "internet"
}

variable "advanced_setting" {
  type        = map(string)
  description = "The slb listener advanced settings to use on listeners. It's supports fields 'sticky_session', 'sticky_session_type', 'cookie', 'cookie_timeout', 'gzip', 'persistence_timeout', 'acl_status', 'acl_type', 'acl_id', 'idle_timeout' and 'request_timeout'."
  default     = {}
}

variable "bandwidth" {
  type        = number
  description = "The load balancer instance bandwidth."
  default     = 10
}

variable "create_slb" {
  type        = bool
  description = "Whether to create load balancer instance. If setting 'use_existing_slb = true' and 'existing_slb_id', it will be ignored."
  default     = true
}

variable "create_tcp_listener" {
  type        = bool
  description = "Whether to create load balancer listeners."
  default     = true
}

variable "existing_slb_id" {
  type        = string
  description = "An existing load balancer instance id."
  default     = ""
}

variable "health_check" {
  type        = map(string)
  description = "The slb listener health check settings to use on listeners. It's supports fields 'healthy_threshold','unhealthy_threshold','health_check_timeout', 'health_check', 'health_check_type', 'health_check_connect_port', 'health_check_domain', 'health_check_uri', 'health_check_http_code', 'health_check_method' and 'health_check_interval'"
  default     = {}
}

variable "internet_charge_type" {
  type        = string
  description = "The charge type of load balancer instance internet network."
  default     = "PayByTraffic"
}

variable "listeners" {
  type        = list(map(string))
  description = "List of slb listeners. Each item can set all or part fields of alicloud_slb_listener resource."
  default     = []
}

variable "master_zone_id" {
  type        = string
  description = "The primary zone ID of the SLB instance. If not specified, the system will be randomly assigned."
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "servers_of_virtual_server_group" {
  type        = list(map(string))
  description = "A list of servers attaching to virtual server group, it's supports fields 'server_ids', 'weight'(default to 100), 'port' and 'type'(default to 'ecs')."
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "slave_zone_id" {
  type        = string
  description = "The standby zone ID of the SLB instance. If not specified, the system will be randomly assigned."
  default     = ""
}

variable "specification" {
  type        = string
  description = "The specification of the SLB instance."
  default     = ""
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the resource."
  default     = {}
}

variable "use_existing_slb" {
  type        = bool
  description = "Whether to use an existing load balancer instance. If true, 'existing_slb_id' should not be empty. Also, you can create a new one by setting 'create = true'."
  default     = false
}

variable "virtual_server_group_name" {
  type        = string
  description = "The name virtual server group. If not set, the 'name' and adding suffix '-virtual' will return."
  default     = ""
}



variable "comments" {
  type        = string
  description = "Comment of the RAM user. This parameter can have a string of 1 to 128 characters."
  default     = ""
}

variable "create" {
  type        = bool
  description = "Whether to create ram user."
  default     = true
}

variable "create_ram_access_key" {
  type        = bool
  description = "Whether to create ram access key. Default value is 'false'."
  default     = false
}

variable "create_ram_user_login_profile" {
  type        = bool
  description = "Whether to create ram user login profile"
  default     = false
}

variable "create_user_attachment" {
  type        = bool
  description = "Whether to attach RAM policy to RAM user. Default value is 'false'."
  default     = false
}

variable "display_name" {
  type        = string
  description = "Name of the RAM user which for display"
  default     = ""
}

variable "email" {
  type        = string
  description = "Email of the RAM user."
  default     = ""
}

variable "existing_user_name" {
  type        = string
  description = "The name of an existing RAM group. If set, 'create' will be ignored."
  default     = ""
}

variable "force_destroy_user" {
  type        = bool
  description = "When destroying this user, destroy even if it has non-Terraform-managed ram access keys, login profile or MFA devices. Without force_destroy a user with non-Terraform-managed access keys and login profile will fail to be destroyed."
  default     = false
}

variable "mfa_bind_required" {
  type        = bool
  description = "This parameter indicates whether the MFA needs to be bind when the user first logs in. Default value is 'false'."
  default     = false
}

variable "mobile" {
  type        = string
  description = "Phone number of the RAM user. This number must contain an international area code prefix, just look like this: 86-18600008888."
  default     = ""
}

variable "password" {
  type        = string
  description = "Login password of the user"
  default     = ""
}

variable "password_reset_required" {
  type        = bool
  description = "This parameter indicates whether the password needs to be reset when the user first logs in. Default value is 'false'."
  default     = false
}

variable "policies" {
  type        = list(map(string))
  description = "List of the policies that binds the role. Each item can contains keys: 'policy_name'(the name of policy that used to bind the role), 'policy_type'(the type of ram policies, System or Custom, default to Custom.)."
  default     = []
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "secret_file" {
  type        = string
  description = "A file used to store access key and secret key of ther user."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "user_name" {
  type        = string
  description = "Desired name for the ram user. If not set, a default name with prefix 'ram-user-' will be returned."
  default     = ""
}


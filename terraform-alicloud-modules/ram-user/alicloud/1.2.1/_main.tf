
module "ram-user" {
  source = "terraform-aws-modules/ram-user/aws"
  version = "1.2.1"
  comments = var.comments
  create = var.create
  create_ram_access_key = var.create_ram_access_key
  create_ram_user_login_profile = var.create_ram_user_login_profile
  create_user_attachment = var.create_user_attachment
  display_name = var.display_name
  email = var.email
  existing_user_name = var.existing_user_name
  force_destroy_user = var.force_destroy_user
  mfa_bind_required = var.mfa_bind_required
  mobile = var.mobile
  password = var.password
  password_reset_required = var.password_reset_required
  policies = var.policies
  profile = var.profile
  region = var.region
  secret_file = var.secret_file
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  user_name = var.user_name
}

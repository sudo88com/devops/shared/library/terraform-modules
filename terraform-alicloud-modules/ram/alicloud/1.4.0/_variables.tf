
variable "admin_name_regex" {
  type        = string
  description = "A regex string to filter resulting policies by name."
  default     = ""
}

variable "create_ram_access_key" {
  type        = bool
  description = "Whether to create ram access key"
  default     = false
}

variable "create_ram_user_login_profile" {
  type        = bool
  description = "Whether to create ram user login profile"
  default     = false
}

variable "create_user" {
  type        = bool
  description = "Whether to create ram user."
  default     = true
}

variable "force_destroy" {
  type        = bool
  description = "When destroying this user, destroy even if it has non-Terraform-managed ram access keys, login profile or MFA devices. Without force_destroy a user with non-Terraform-managed access keys and login profile will fail to be destroyed."
  default     = false
}

variable "is_admin" {
  type        = bool
  description = "Whether to grant admin permission"
  default     = false
}

variable "is_reader" {
  type        = bool
  description = "Whether to grant reader permission"
  default     = false
}

variable "name" {
  type        = string
  description = "Desired name for the ram user. If not set, a default name with prefix `ram-user-` will be returned."
  default     = ""
}

variable "password" {
  type        = string
  description = "Login password of the user"
  default     = ""
}

variable "policy_type" {
  type        = string
  description = "Type of the RAM policy. It must be Custom or System."
  default     = "System"
}

variable "reader_name_regex" {
  type        = string
  description = "A regex string to filter resulting policies by name."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.3.0)The region used to launch this module resources."
  default     = ""
}

variable "secret_file" {
  type        = string
  description = "A file used to store access key and secret key of ther user "
  default     = ""
}


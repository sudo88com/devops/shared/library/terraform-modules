
module "ram" {
  source = "terraform-aws-modules/ram/aws"
  version = "1.4.0"
  admin_name_regex = var.admin_name_regex
  create_ram_access_key = var.create_ram_access_key
  create_ram_user_login_profile = var.create_ram_user_login_profile
  create_user = var.create_user
  force_destroy = var.force_destroy
  is_admin = var.is_admin
  is_reader = var.is_reader
  name = var.name
  password = var.password
  policy_type = var.policy_type
  reader_name_regex = var.reader_name_regex
  region = var.region
  secret_file = var.secret_file
}

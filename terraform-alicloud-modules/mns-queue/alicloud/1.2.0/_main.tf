
module "mns-queue" {
  source = "terraform-aws-modules/mns-queue/aws"
  version = "1.2.0"
  delay_seconds = var.delay_seconds
  maximum_message_size = var.maximum_message_size
  message_retention_period = var.message_retention_period
  name = var.name
  polling_wait_seconds = var.polling_wait_seconds
  region = var.region
  visibility_timeout = var.visibility_timeout
}


create_rds_mysql = false

data_disks = []

deletion_protection = false

ecs_instance_name = "TF-Wordpress"

ecs_instance_password = ""

ecs_instance_type = ""

image_id = ""

internet_charge_type = "PayByTraffic"

internet_max_bandwidth_out = 10

mysql_account_name = ""

mysql_account_password = ""

mysql_account_privilege = "ReadWrite"

mysql_account_type = "Normal"

mysql_database_character_set = "utf8"

mysql_database_name = ""

mysql_engine_version = "5.6"

mysql_instance_name = "tf-mysql-for-wordpress"

mysql_instance_storage = 20

mysql_instance_type = ""

private_ip = ""

profile = ""

region = ""

resource_group_id = ""

security_group_ids = []

shared_credentials_file = ""

skip_region_validation = false

system_disk_category = ""

system_disk_size = 40

tags = {}

volume_tags = {}

vswitch_id = ""


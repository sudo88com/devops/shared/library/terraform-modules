
variable "create_rds_mysql" {
  type        = bool
  description = "Whether to create RDS MySql database. If false, this module will create MySql database on ECS."
  default     = false
}

variable "data_disks" {
  type        = list(map(string))
  description = "Additional data disks to attach to the scaled ECS instance."
  default     = []
}

variable "deletion_protection" {
  type        = bool
  description = "Whether enable the deletion protection or not. 'true': Enable deletion protection. 'false': Disable deletion protection."
  default     = false
}

variable "ecs_instance_name" {
  type        = string
  description = "The name of ECS Instance."
  default     = "TF-Wordpress"
}

variable "ecs_instance_password" {
  type        = string
  description = "The password of ECS instance."
  default     = ""
}

variable "ecs_instance_type" {
  type        = string
  description = "The instance type used to launch ecs instance."
  default     = ""
}

variable "image_id" {
  type        = string
  description = "The image id used to launch one ecs instance. It only support CentOS_7."
  default     = ""
}

variable "internet_charge_type" {
  type        = string
  description = "The internet charge type of ECS instance. Choices are 'PayByTraffic' and 'PayByBandwidth'."
  default     = "PayByTraffic"
}

variable "internet_max_bandwidth_out" {
  type        = number
  description = "The maximum internet out bandwidth of ECS instance."
  default     = 10
}

variable "mysql_account_name" {
  type        = string
  description = "Name of a new database account. It should be set when create_account = true."
  default     = ""
}

variable "mysql_account_password" {
  type        = string
  description = "Operation database account password. It may consist of letters, digits, or underlines, with a length of 6 to 32 characters."
  default     = ""
}

variable "mysql_account_privilege" {
  type        = string
  description = "The privilege of one account access database."
  default     = "ReadWrite"
}

variable "mysql_account_type" {
  type        = string
  description = "Privilege type of account. Normal: Common privilege. Super: High privilege.Default to Normal."
  default     = "Normal"
}

variable "mysql_database_character_set" {
  type        = string
  description = "The value range is limited to the following."
  default     = "utf8"
}

variable "mysql_database_name" {
  type        = string
  description = "Name of a new database . It should be set when create_databases = true."
  default     = ""
}

variable "mysql_engine_version" {
  type        = string
  description = "RDS MySql instance version."
  default     = "5.6"
}

variable "mysql_instance_name" {
  type        = string
  description = "The name of Rds MySQL Instance."
  default     = "tf-mysql-for-wordpress"
}

variable "mysql_instance_storage" {
  type        = number
  description = "The storage capacity of the instance. Unit: GB. The storage capacity increases at increments of 5 GB. For more information, see [Instance Types](https://www.alibabacloud.com/help/doc-detail/26312.htm)."
  default     = 20
}

variable "mysql_instance_type" {
  type        = string
  description = "MySQL Instance type, for example: mysql.n1.micro.1. full list is : https://www.alibabacloud.com/help/zh/doc-detail/26312.htm"
  default     = ""
}

variable "private_ip" {
  type        = string
  description = "Configure ECS Instance private IP address"
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "resource_group_id" {
  type        = string
  description = "The Id of resource group which the ECS instance belongs."
  default     = ""
}

variable "security_group_ids" {
  type        = list(string)
  description = "A list of security group ids to associate with ECS and RDS Mysql Instance."
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "system_disk_category" {
  type        = string
  description = "The system disk category used to launch one ecs instance."
  default     = ""
}

variable "system_disk_size" {
  type        = number
  description = "The system disk size used to launch ecs instance."
  default     = 40
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the ECS and RDS Instance."
  default     = {}
}

variable "volume_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the devices created by the instance at launch time."
  default     = {}
}

variable "vswitch_id" {
  type        = string
  description = "The virtual switch ID to launch ECS and RDS MySql instance in VPC."
  default     = ""
}


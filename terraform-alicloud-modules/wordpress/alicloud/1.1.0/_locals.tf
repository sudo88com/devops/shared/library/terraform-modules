
locals {
  create_rds_mysql = var.create_rds_mysql
  data_disks = var.data_disks
  deletion_protection = var.deletion_protection
  ecs_instance_name = var.ecs_instance_name
  ecs_instance_password = var.ecs_instance_password
  ecs_instance_type = var.ecs_instance_type
  image_id = var.image_id
  internet_charge_type = var.internet_charge_type
  internet_max_bandwidth_out = var.internet_max_bandwidth_out
  mysql_account_name = var.mysql_account_name
  mysql_account_password = var.mysql_account_password
  mysql_account_privilege = var.mysql_account_privilege
  mysql_account_type = var.mysql_account_type
  mysql_database_character_set = var.mysql_database_character_set
  mysql_database_name = var.mysql_database_name
  mysql_engine_version = var.mysql_engine_version
  mysql_instance_name = var.mysql_instance_name
  mysql_instance_storage = var.mysql_instance_storage
  mysql_instance_type = var.mysql_instance_type
  private_ip = var.private_ip
  profile = var.profile
  region = var.region
  resource_group_id = var.resource_group_id
  security_group_ids = var.security_group_ids
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  system_disk_category = var.system_disk_category
  system_disk_size = var.system_disk_size
  tags = var.tags
  volume_tags = var.volume_tags
  vswitch_id = var.vswitch_id
}

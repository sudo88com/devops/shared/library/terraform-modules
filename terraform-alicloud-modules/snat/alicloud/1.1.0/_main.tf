
module "snat" {
  source = "terraform-aws-modules/snat/aws"
  version = "1.1.0"
  computed_snat_with_source_cidr = var.computed_snat_with_source_cidr
  computed_snat_with_vswitch_id = var.computed_snat_with_vswitch_id
  create = var.create
  nat_gateway_id = var.nat_gateway_id
  profile = var.profile
  region = var.region
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  snat_ips = var.snat_ips
  snat_table_id = var.snat_table_id
  snat_with_instance_ids = var.snat_with_instance_ids
  snat_with_source_cidrs = var.snat_with_source_cidrs
  snat_with_vswitch_ids = var.snat_with_vswitch_ids
}

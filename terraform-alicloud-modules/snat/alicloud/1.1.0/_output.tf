
output "this_snat_entry_id_of_computed_snat_with_source_cidrs" {
  description = "List of ids creating by computed_snat_with_source_cidrs."
  value       = module.snat.this_snat_entry_id_of_computed_snat_with_source_cidrs
}

output "this_snat_entry_id_of_computed_snat_with_vswitch_ids" {
  description = "List of ids creating by computed_snat_with_vswitch_ids."
  value       = module.snat.this_snat_entry_id_of_computed_snat_with_vswitch_ids
}

output "this_snat_entry_id_of_snat_with_instance_ids" {
  description = "List of ids creating by snat_with_instance_ids."
  value       = module.snat.this_snat_entry_id_of_snat_with_instance_ids
}

output "this_snat_entry_id_of_snat_with_source_cidrs" {
  description = "List of ids creating by snat_with_source_cidrs."
  value       = module.snat.this_snat_entry_id_of_snat_with_source_cidrs
}

output "this_snat_entry_id_of_snat_with_vswitch_ids" {
  description = "List of ids creating by snat_with_vswitch_ids."
  value       = module.snat.this_snat_entry_id_of_snat_with_vswitch_ids
}

output "this_snat_entry_name_of_computed_snat_with_source_cidrs" {
  description = "List of names creating by computed_snat_with_source_cidrs."
  value       = module.snat.this_snat_entry_name_of_computed_snat_with_source_cidrs
}

output "this_snat_entry_name_of_computed_snat_with_vswitch_ids" {
  description = "List of names creating by computed_snat_with_vswitch_ids."
  value       = module.snat.this_snat_entry_name_of_computed_snat_with_vswitch_ids
}

output "this_snat_entry_name_of_snat_with_instance_ids" {
  description = "List of names creating by snat_with_instance_ids."
  value       = module.snat.this_snat_entry_name_of_snat_with_instance_ids
}

output "this_snat_entry_name_of_snat_with_source_cidrs" {
  description = "List of names creating by snat_with_source_cidrs."
  value       = module.snat.this_snat_entry_name_of_snat_with_source_cidrs
}

output "this_snat_entry_name_of_snat_with_vswitch_ids" {
  description = "List of names creating by snat_with_vswitch_ids."
  value       = module.snat.this_snat_entry_name_of_snat_with_vswitch_ids
}


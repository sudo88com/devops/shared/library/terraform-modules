
variable "accessed_by" {
  type        = string
  description = "The network limitation of accessing instance. Valid values:[\"Any\", \"Vpc\", \"ConsoleOrVpc\"]."
  default     = "Any"
}

variable "bind_vpc" {
  type        = bool
  description = "Whether to create ots instance attachment. If true, the ots instance will be attached with vpc and vswitch."
  default     = true
}

variable "create_instance" {
  type        = bool
  description = "Whether to create ots instance. if true, a new ots instance will be created. It will be ignore if 'use_existing_instance = true'"
  default     = true
}

variable "create_table" {
  type        = bool
  description = "Whether to create ots table. If true, a new ots table will be created"
  default     = true
}

variable "description" {
  type        = string
  description = "The description of the instance."
  default     = ""
}

variable "existing_ots_instance_name" {
  type        = string
  description = "An existing ots instance name, if 'use_existing_instance = true', this field is required. "
  default     = ""
}

variable "instance_name" {
  type        = string
  description = "The name of the instance."
  default     = ""
}

variable "instance_type" {
  type        = string
  description = "The type of instance. Valid values: [\"Capacity\", \"HighPerformance\"]."
  default     = "HighPerformance"
}

variable "max_version" {
  type        = number
  description = "The maximum number of versions stored in this table. The valid value is 1-2147483647."
  default     = 1
}

variable "primary_key" {
  type        = list(object({
    name = string
    type = string
  }))
  description = "The property of TableMeta which indicates the structure information of a table. It describes the attribute value of primary key. The number of primary_key should not be less than one and not be more than four."
  default     = []
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.2.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.2.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.2.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "table_name" {
  type        = string
  description = "(Required, ForceNew) The table name of the OTS instance. If changed, a new table would be created."
  default     = ""
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the instance."
  default     = {}
}

variable "time_to_live" {
  type        = number
  description = "The retention time of data stored in this table (unit: second). The value maximum is 2147483647 and -1 means never expired."
  default     = -1
}

variable "use_existing_instance" {
  type        = bool
  description = "Whether to use existing ots instance."
  default     = false
}

variable "vpc_name" {
  type        = string
  description = "The name of attaching VPC to instance."
  default     = ""
}

variable "vswitch_id" {
  type        = string
  description = "The ID of attaching VSwitch to instance."
  default     = ""
}


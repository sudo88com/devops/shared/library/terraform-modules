
accessed_by = "Any"

bind_vpc = true

create_instance = true

create_table = true

description = ""

existing_ots_instance_name = ""

instance_name = ""

instance_type = "HighPerformance"

max_version = 1

primary_key = []

profile = ""

region = ""

shared_credentials_file = ""

skip_region_validation = false

table_name = ""

tags = {}

time_to_live = -1

use_existing_instance = false

vpc_name = ""

vswitch_id = ""


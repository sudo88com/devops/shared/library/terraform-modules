
locals {
  accessed_by = var.accessed_by
  bind_vpc = var.bind_vpc
  create_instance = var.create_instance
  create_table = var.create_table
  description = var.description
  existing_ots_instance_name = var.existing_ots_instance_name
  instance_name = var.instance_name
  instance_type = var.instance_type
  max_version = var.max_version
  primary_key = var.primary_key
  profile = var.profile
  region = var.region
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  table_name = var.table_name
  tags = var.tags
  time_to_live = var.time_to_live
  use_existing_instance = var.use_existing_instance
  vpc_name = var.vpc_name
  vswitch_id = var.vswitch_id
}

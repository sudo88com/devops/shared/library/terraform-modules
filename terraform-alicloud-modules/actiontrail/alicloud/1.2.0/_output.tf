
output "this_actiontrail_id" {
  description = "The actiontrail ID"
  value       = module.actiontrail.this_actiontrail_id
}

output "this_event_rw" {
  description = ""
  value       = module.actiontrail.this_event_rw
}

output "this_log_project_arn" {
  description = ""
  value       = module.actiontrail.this_log_project_arn
}

output "this_log_project_id" {
  description = "The log project id used to launch actiontrail"
  value       = module.actiontrail.this_log_project_id
}

output "this_log_project_name" {
  description = ""
  value       = module.actiontrail.this_log_project_name
}

output "this_module_name" {
  description = ""
  value       = module.actiontrail.this_module_name
}

output "this_oss_bucket_name" {
  description = "The OSS bucket id used to launch actiontrail"
  value       = module.actiontrail.this_oss_bucket_name
}

output "this_oss_key_prefix" {
  description = ""
  value       = module.actiontrail.this_oss_key_prefix
}



locals {
  create_actiontrail_default_role = var.create_actiontrail_default_role
  create_oss_bucket = var.create_oss_bucket
  event_rw = var.event_rw
  log_project_arn = var.log_project_arn
  log_project_name = var.log_project_name
  oss_bucket_name = var.oss_bucket_name
  oss_key_prefix = var.oss_key_prefix
  ram_policy_default_name = var.ram_policy_default_name
  ram_role_default_name = var.ram_role_default_name
  region = var.region
  this_module_name = var.this_module_name
}

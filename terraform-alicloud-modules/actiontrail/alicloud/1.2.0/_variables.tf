
variable "create_actiontrail_default_role" {
  type        = string
  description = "Create a default ram role used to access OSS and SLS before launching a new CreateTrail. Default using the existed ram role."
  default     = false
}

variable "create_oss_bucket" {
  type        = string
  description = "Whether to create a new OSS bucket based on variable oss_bucket_name."
  default     = false
}

variable "event_rw" {
  type        = string
  description = "Indicates whether the event is a read or a write event. Valid values: Read, Write, and All. Default value: Write."
  default     = "Write"
}

variable "log_project_arn" {
  type        = string
  description = "The unique ARN of the Log Service project used to the trail."
  default     = ""
}

variable "log_project_name" {
  type        = string
  description = "A name used to create a new log project when log_project_arn is not set."
  default     = ""
}

variable "oss_bucket_name" {
  type        = string
  description = "A name of OSS bucket used to the trail delivers logs."
  default     = ""
}

variable "oss_key_prefix" {
  type        = string
  description = "The prefix of the specified OSS bucket name. Default to empty."
  default     = ""
}

variable "ram_policy_default_name" {
  type        = string
  description = "A default ram policy used to ActionTrail default grant policy. It is valid when create_actiontrail_default_role is true."
  default     = "AliyunActionTrailRolePolicy"
}

variable "ram_role_default_name" {
  type        = string
  description = "A default ram role used to grant ActionTrail to access OSS and SLS. It is valid when create_actiontrail_default_role is true."
  default     = "AliyunActionTrailDefaultRole"
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "this_module_name" {
  type        = string
  description = "Name used on ActionTrail"
  default     = ""
}



output "this_ssl_vpn_client_cert_ids" {
  description = "The IDs of SSL-VPN client certs."
  value       = module.vpn-gateway.this_ssl_vpn_client_cert_ids
}

output "this_ssl_vpn_server_id" {
  description = "The ID of SSL-VPN server instance."
  value       = module.vpn-gateway.this_ssl_vpn_server_id
}

output "this_vpn_connection_id" {
  description = "The ID of VPN connection."
  value       = module.vpn-gateway.this_vpn_connection_id
}

output "this_vpn_customer_gateway_id" {
  description = "The ID of customer gateway instance."
  value       = module.vpn-gateway.this_vpn_customer_gateway_id
}

output "this_vpn_gateway_id" {
  description = "The ID of VPN gateway instance."
  value       = module.vpn-gateway.this_vpn_gateway_id
}


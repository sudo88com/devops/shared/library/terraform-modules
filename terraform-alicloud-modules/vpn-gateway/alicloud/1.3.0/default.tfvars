
cgw_description = ""

cgw_id = ""

cgw_ip_address = ""

cgw_name = ""

ike_auth_alg = "sha1"

ike_enc_alg = "aes"

ike_lifetime = 86400

ike_local_id = ""

ike_mode = "main"

ike_pfs = "group2"

ike_remote_id = ""

ike_version = "ikev1"

ipsec_auth_alg = "sha1"

ipsec_connection_name = ""

ipsec_effect_immediately = false

ipsec_enc_alg = "aes"

ipsec_lifetime = 86400

ipsec_local_subnet = []

ipsec_pfs = "group2"

ipsec_remote_subnet = []

psk = ""

region = ""

ssl_cipher = "AES-128-CBC"

ssl_client_cert_names = []

ssl_client_ip_pool = ""

ssl_compress = true

ssl_local_subnet = ""

ssl_port = 1194

ssl_protocol = "UDP"

ssl_vpn_server_name = ""

vpc_id = ""

vpn_bandwidth = 10

vpn_charge_type = "PostPaid"

vpn_description = ""

vpn_enable_ipsec = true

vpn_enable_ssl = false

vpn_name = ""

vpn_period = 1

vpn_ssl_connections = 5


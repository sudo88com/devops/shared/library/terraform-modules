
variable "cgw_description" {
  type        = string
  description = "The description of the VPN customer gateway instance."
  default     = ""
}

variable "cgw_id" {
  type        = string
  description = "The customer gateway id used to connect with vpn gateway."
  default     = ""
}

variable "cgw_ip_address" {
  type        = string
  description = "The IP address of the customer gateway."
  default     = ""
}

variable "cgw_name" {
  type        = string
  description = "The name of the VPN customer gateway. Defaults to null."
  default     = ""
}

variable "ike_auth_alg" {
  type        = string
  description = "The authentication algorithm of phase-one negotiation. Valid value: md5 | sha1. Default value: sha1."
  default     = "sha1"
}

variable "ike_enc_alg" {
  type        = string
  description = "The encryption algorithm of phase-one negotiation. Valid value: aes | aes192 | aes256 | des | 3des. Default Valid value: aes."
  default     = "aes"
}

variable "ike_lifetime" {
  type        = number
  description = "The SA lifecycle as the result of phase-one negotiation. The valid value of n is [0, 86400], the unit is second and the default value is 86400."
  default     = 86400
}

variable "ike_local_id" {
  type        = string
  description = "The identification of the VPN gateway."
  default     = ""
}

variable "ike_mode" {
  type        = string
  description = "The negotiation mode of IKE V1. Valid value: main (main mode) | aggressive (aggressive mode). Default value: main."
  default     = "main"
}

variable "ike_pfs" {
  type        = string
  description = "The Diffie-Hellman key exchange algorithm used by phase-one negotiation. Valid value: group1 | group2 | group5 | group14 | group24. Default value: group2."
  default     = "group2"
}

variable "ike_remote_id" {
  type        = string
  description = "The identification of the customer gateway."
  default     = ""
}

variable "ike_version" {
  type        = string
  description = "The version of the IKE protocol. Valid value: ikev1 | ikev2. Default value: ikev1."
  default     = "ikev1"
}

variable "ipsec_auth_alg" {
  type        = string
  description = "The authentication algorithm of phase-two negotiation. Valid value: md5 | sha1. Default value: sha1."
  default     = "sha1"
}

variable "ipsec_connection_name" {
  type        = string
  description = "The name of the IPsec connection."
  default     = ""
}

variable "ipsec_effect_immediately" {
  type        = bool
  description = "Whether to delete a successfully negotiated IPsec tunnel and initiate a negotiation again. Valid value:true,false."
  default     = false
}

variable "ipsec_enc_alg" {
  type        = string
  description = "The encryption algorithm of phase-two negotiation. Valid value: aes | aes192 | aes256 | des | 3des. Default value: aes."
  default     = "aes"
}

variable "ipsec_lifetime" {
  type        = number
  description = "The SA lifecycle as the result of phase-two negotiation. The valid value is [0, 86400], the unit is second and the default value is 86400."
  default     = 86400
}

variable "ipsec_local_subnet" {
  type        = list(string)
  description = "The CIDR block of the VPC to be connected with the local data center. This parameter is used for phase-two negotiation."
  default     = []
}

variable "ipsec_pfs" {
  type        = string
  description = "The Diffie-Hellman key exchange algorithm used by phase-two negotiation. Valid value: group1 | group2 | group5 | group14 | group24. Default value: group2."
  default     = "group2"
}

variable "ipsec_remote_subnet" {
  type        = list(string)
  description = "The CIDR block of the local data center. This parameter is used for phase-two negotiation."
  default     = []
}

variable "psk" {
  type        = string
  description = "Used for authentication between the IPsec VPN gateway and the customer gateway."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "ssl_cipher" {
  type        = string
  description = "The encryption algorithm used by the SSL-VPN server. Valid value: AES-128-CBC (default)| AES-192-CBC | AES-256-CBC | none."
  default     = "AES-128-CBC"
}

variable "ssl_client_cert_names" {
  type        = list(string)
  description = "The names of the client certificates."
  default     = []
}

variable "ssl_client_ip_pool" {
  type        = string
  description = "The CIDR block from which access addresses are allocated to the virtual network interface card of the client."
  default     = ""
}

variable "ssl_compress" {
  type        = bool
  description = "Specify whether to compress the communication. Valid value: true (default) | false."
  default     = true
}

variable "ssl_local_subnet" {
  type        = string
  description = "The CIDR block to be accessed by the client through the SSL-VPN connection."
  default     = ""
}

variable "ssl_port" {
  type        = number
  description = "The port used by the SSL-VPN server. The default value is 1194.The following ports cannot be used: [22, 2222, 22222, 9000, 9001, 9002, 7505, 80, 443, 53, 68, 123, 4510, 4560, 500, 4500]."
  default     = 1194
}

variable "ssl_protocol" {
  type        = string
  description = "The protocol used by the SSL-VPN server. Valid value: UDP(default) |TCP."
  default     = "UDP"
}

variable "ssl_vpn_server_name" {
  type        = string
  description = "The name of the SSL-VPN server."
  default     = ""
}

variable "vpc_id" {
  type        = string
  description = "The VPN belongs the vpc_id, the field can't be changed."
  default     = ""
}

variable "vpn_bandwidth" {
  type        = number
  description = "The value should be 10, 100, 200, 500, 1000 if the user is postpaid, otherwise it can be 5, 10, 20, 50, 100, 200, 500, 1000."
  default     = 10
}

variable "vpn_charge_type" {
  type        = string
  description = "The charge type for instance. Valid value: PostPaid, PrePaid. Default to PostPaid."
  default     = "PostPaid"
}

variable "vpn_description" {
  type        = string
  description = "The description of the VPN instance."
  default     = ""
}

variable "vpn_enable_ipsec" {
  type        = bool
  description = "Enable or Disable IPSec VPN. At least one type of VPN should be enabled."
  default     = true
}

variable "vpn_enable_ssl" {
  type        = bool
  description = "Enable or Disable SSL VPN.  At least one type of VPN should be enabled."
  default     = false
}

variable "vpn_name" {
  type        = string
  description = "Name of the VPN gateway."
  default     = ""
}

variable "vpn_period" {
  type        = number
  description = "The filed is only required while the InstanceChargeType is prepaid."
  default     = 1
}

variable "vpn_ssl_connections" {
  type        = number
  description = "The max connections of SSL VPN. Default to 5. This field is ignored when enable_ssl is false."
  default     = 5
}


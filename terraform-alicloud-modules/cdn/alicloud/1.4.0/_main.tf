
module "cdn" {
  source = "terraform-aws-modules/cdn/aws"
  version = "1.4.0"
  add_cdn_domain = var.add_cdn_domain
  cdn_type = var.cdn_type
  domain_name = var.domain_name
  existing_domain_names = var.existing_domain_names
  function_arg = var.function_arg
  function_name = var.function_name
  profile = var.profile
  region = var.region
  scope = var.scope
  set_config = var.set_config
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  sources = var.sources
  tags = var.tags
}

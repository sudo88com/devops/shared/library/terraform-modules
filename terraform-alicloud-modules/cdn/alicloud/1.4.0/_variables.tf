
variable "add_cdn_domain" {
  type        = bool
  description = "Whether to add a Cdn domain. Default to true."
  default     = true
}

variable "cdn_type" {
  type        = string
  description = "Cdn type of the cdn domain. Valid values are 'web', 'download', 'video'."
  default     = "web"
}

variable "domain_name" {
  type        = string
  description = "Name of a new cdn domain."
  default     = ""
}

variable "existing_domain_names" {
  type        = list(string)
  description = "List of added cdn domain names for batch configuration. If set, the 'set_config' should be true."
  default     = []
}

variable "function_arg" {
  type        = list(object({
    arg_name  = string
    arg_value = string
  }))
  description = "The args of the domain config."
  default     = []
}

variable "function_name" {
  type        = string
  description = "The name of the domain config."
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.4.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.4.0) The region used to launch this module resources."
  default     = ""
}

variable "scope" {
  type        = string
  description = "Scope of the cdn domain. Valid values are 'domestic', 'overseas', global'. Default value is 'domestic'. This parameter's setting is valid Only for the international users and domestic L3 and above users ."
  default     = "domestic"
}

variable "set_config" {
  type        = bool
  description = "Whether to batch set config for new or existing domain names. Default to true."
  default     = true
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.4.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.4.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "sources" {
  type        = list(map(string))
  description = "The source address list of the cdn domain. Each item can contains keys:'content'(The adress of source. Valid values can be ip or doaminName.),'type'(The type of the source. Valid values are 'ipaddr', 'domain' and 'oss'. Default to 'ipaddr'),'port'(The port of source. Valid values are '443' and '80'. Default value is '80'.),'priority'(Priority of the source. Valid values are '0' and '100'. Default value is '20'.),'weight'(Weight of the source. Valid values are from '0' to '100'. Default value is '10', but if type is 'ipaddr', the value can only be '10'.)."
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the cdn domain."
  default     = {}
}


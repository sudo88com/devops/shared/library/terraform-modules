
data_disks = []

db_name = ""

db_password = ""

db_root_password = ""

db_user = ""

deletion_protection = false

image_id = ""

instance_name = "TF-LNMP"

instance_password = ""

instance_type = "ecs.c5.large"

internet_charge_type = "PayByTraffic"

internet_max_bandwidth_out = 10

number_of_instances = 1

private_ip = ""

profile = ""

region = ""

resource_group_id = ""

security_group_ids = []

shared_credentials_file = ""

skip_region_validation = false

system_disk_category = "cloud_ssd"

system_disk_size = 40

tags = {}

use_num_suffix = false

volume_tags = {}

vswitch_id = ""



variable "data_disks" {
  type        = list(map(string))
  description = "Additional data disks to attach to the scaled instance."
  default     = []
}

variable "db_name" {
  type        = string
  description = "The mysql db name."
  default     = ""
}

variable "db_password" {
  type        = string
  description = "The mysql db password."
  default     = ""
}

variable "db_root_password" {
  type        = string
  description = "The mysql db password."
  default     = ""
}

variable "db_user" {
  type        = string
  description = "The mysql db user name."
  default     = ""
}

variable "deletion_protection" {
  type        = bool
  description = "Whether enable the deletion protection or not. 'true': Enable deletion protection. 'false': Disable deletion protection."
  default     = false
}

variable "image_id" {
  type        = string
  description = "The image id used to launch one ecs instance. If not specified, it will be obtained through the data source of Centos 7."
  default     = ""
}

variable "instance_name" {
  type        = string
  description = "The name of ECS Instance."
  default     = "TF-LNMP"
}

variable "instance_password" {
  type        = string
  description = "The password of instance."
  default     = ""
}

variable "instance_type" {
  type        = string
  description = "The instance type used to launch instance."
  default     = "ecs.c5.large"
}

variable "internet_charge_type" {
  type        = string
  description = "The internet charge type of ECS instance. Choices are 'PayByTraffic' and 'PayByBandwidth'."
  default     = "PayByTraffic"
}

variable "internet_max_bandwidth_out" {
  type        = number
  description = "The maximum internet out bandwidth of instance."
  default     = 10
}

variable "number_of_instances" {
  type        = number
  description = "The number of instances to be created."
  default     = 1
}

variable "private_ip" {
  type        = string
  description = "Configure Instance private IP address."
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "resource_group_id" {
  type        = string
  description = "The Id of resource group which the instance belongs."
  default     = ""
}

variable "security_group_ids" {
  type        = list(string)
  description = "A list of security group ids to associate with Instance."
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "system_disk_category" {
  type        = string
  description = "The system disk category used to launch instance."
  default     = "cloud_ssd"
}

variable "system_disk_size" {
  type        = number
  description = "The system disk size used to launch instance.Default to '40'."
  default     = 40
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the Instance."
  default     = {}
}

variable "use_num_suffix" {
  type        = bool
  description = "Always append numerical suffix(like 001, 002 and so on) to instance name and host name, even if number_of_instances is 1"
  default     = false
}

variable "volume_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the devices created by the instance at launch time."
  default     = {}
}

variable "vswitch_id" {
  type        = string
  description = "The virtual switch ID to launch instance in VPC."
  default     = ""
}


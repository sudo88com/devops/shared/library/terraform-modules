
module "emr-druid" {
  source = "terraform-aws-modules/emr-druid/aws"
  version = "1.1.0"
  charge_type = var.charge_type
  create = var.create
  disk_capacity = var.disk_capacity
  disk_type = var.disk_type
  emr_cluster_name = var.emr_cluster_name
  emr_version = var.emr_version
  high_availability_enable = var.high_availability_enable
  host_groups = var.host_groups
  instance_type = var.instance_type
  is_open_public_ip = var.is_open_public_ip
  master_pwd = var.master_pwd
  profile = var.profile
  ram_role_name = var.ram_role_name
  region = var.region
  security_group_id = var.security_group_id
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  ssh_enable = var.ssh_enable
  support_local_storage = var.support_local_storage
  system_disk_capacity = var.system_disk_capacity
  system_disk_type = var.system_disk_type
  vswitch_id = var.vswitch_id
  zone_id = var.zone_id
}

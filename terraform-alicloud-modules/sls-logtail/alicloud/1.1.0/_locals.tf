
locals {
  associate_public_ip_address = var.associate_public_ip_address
  config_input_detail = var.config_input_detail
  config_input_type = var.config_input_type
  config_log_sample = var.config_log_sample
  config_name = var.config_name
  config_output_type = var.config_output_type
  create_instance = var.create_instance
  create_log_service = var.create_log_service
  existing_instance_private_ips = var.existing_instance_private_ips
  image_id = var.image_id
  instance_password = var.instance_password
  instance_type = var.instance_type
  internet_max_bandwidth_out = var.internet_max_bandwidth_out
  log_machine_group_name = var.log_machine_group_name
  log_machine_identify_type = var.log_machine_identify_type
  log_machine_topic = var.log_machine_topic
  logstore_name = var.logstore_name
  number_of_instance = var.number_of_instance
  profile = var.profile
  project_name = var.project_name
  region = var.region
  security_groups = var.security_groups
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  vswitch_id = var.vswitch_id
}

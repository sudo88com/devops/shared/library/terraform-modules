
associate_public_ip_address = false

config_input_detail = ""

config_input_type = "file"

config_log_sample = ""

config_name = ""

config_output_type = "LogService"

create_instance = true

create_log_service = true

existing_instance_private_ips = []

image_id = ""

instance_password = ""

instance_type = ""

internet_max_bandwidth_out = 10

log_machine_group_name = ""

log_machine_identify_type = "ip"

log_machine_topic = ""

logstore_name = ""

number_of_instance = 1

profile = ""

project_name = ""

region = ""

security_groups = []

shared_credentials_file = ""

skip_region_validation = false

vswitch_id = ""


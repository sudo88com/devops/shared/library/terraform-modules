
variable "associate_public_ip_address" {
  type        = bool
  description = "Whether to associate a public ip address with an instance in a VPC. If true, the `internet_max_bandwidth_out` should be greater than 0."
  default     = false
}

variable "config_input_detail" {
  type        = string
  description = "The logtail configure the required JSON files, this parameter is required if you will use this module to create logtail config. ([Refer to details](https://www.alibabacloud.com/help/doc-detail/29058.htm))"
  default     = ""
}

variable "config_input_type" {
  type        = string
  description = "The input type. Currently only two types are supported: `file` and `plugin`."
  default     = "file"
}

variable "config_log_sample" {
  type        = string
  description = "The log sample of the Logtail configuration. The log size cannot exceed 1,000 bytes."
  default     = ""
}

variable "config_name" {
  type        = string
  description = "The Logtail configuration name. If not set, a default name with prefix `sls-logtail-module-config-` will be returned."
  default     = ""
}

variable "config_output_type" {
  type        = string
  description = "The output type. Currently, only LogService is supported."
  default     = "LogService"
}

variable "create_instance" {
  type        = bool
  description = "Whether to create ECS instances"
  default     = true
}

variable "create_log_service" {
  type        = bool
  description = "Whether to create log resources. If true, a new machine group and logtail config will be created and associate them."
  default     = true
}

variable "existing_instance_private_ips" {
  type        = list(string)
  description = "The private IP list of existing ECS instances used to join log machine group."
  default     = []
}

variable "image_id" {
  type        = string
  description = "The Image to use for the instance, this parameter is required if you will use this module to create instances."
  default     = ""
}

variable "instance_password" {
  type        = string
  description = "Password to an instance is a string of 8 to 30 characters. It must contain uppercase/lowercase letters and numerals, but cannot contain special symbols. When it is changed, the instance will reboot to make the change take effect."
  default     = ""
}

variable "instance_type" {
  type        = string
  description = "The type of instance to start, this parameter is required if you will use this module to create instances."
  default     = ""
}

variable "internet_max_bandwidth_out" {
  type        = number
  description = "Maximum outgoing bandwidth to the public network, measured in Mbps (Mega bit per second). Value range:  [0, 100]."
  default     = 10
}

variable "log_machine_group_name" {
  type        = string
  description = "Log machine group name. If not set, a default name with prefix `sls-logtail-module-group-` will be returned."
  default     = ""
}

variable "log_machine_identify_type" {
  type        = string
  description = "The machine identification type, including IP and user-defined identity. Valid values are 'ip' and 'userdefined'. Default to 'ip'."
  default     = "ip"
}

variable "log_machine_topic" {
  type        = string
  description = "The topic of a machine group."
  default     = ""
}

variable "logstore_name" {
  type        = string
  description = "The log store name used to create a new logtail config, this parameter is required if you will use this module to create logtail resources."
  default     = ""
}

variable "number_of_instance" {
  type        = number
  description = "The number of ECS instances"
  default     = 1
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "project_name" {
  type        = string
  description = "A log project name used to create a new machine group and logtail config, this parameter is required if you will use this module to create logtail resources."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "security_groups" {
  type        = list(string)
  description = "Existing security group ids used to create ECS instances, this parameter is required if you will use this module to create instances."
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "vswitch_id" {
  type        = string
  description = "A existing vswitch id used to create ECS instances, this parameter is required if you will use this module to create instances."
  default     = ""
}


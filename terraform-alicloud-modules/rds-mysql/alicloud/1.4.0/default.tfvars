
account_name = ""

account_password = ""

account_privilege = "ReadOnly"

account_type = "Normal"

alarm_rule_contact_groups = []

alarm_rule_effective_interval = "0:00-2:00"

alarm_rule_name = ""

alarm_rule_operator = "=="

alarm_rule_period = 300

alarm_rule_silence_time = 86400

alarm_rule_statistics = "Average"

alarm_rule_threshold = ""

alarm_rule_triggered_count = 3

allocate_public_connection = false

backup_retention_period = 7

connection_prefix = "tf-mysql"

create_account = true

create_backup_policy = true

create_database = true

create_instance = true

databases = []

enable_alarm_rule = true

enable_backup_log = false

engine_version = ""

existing_instance_id = ""

instance_charge_type = "Postpaid"

instance_name = "tf-rds-instance-for-mysql"

instance_storage = 20

instance_type = ""

log_backup_retention_period = 7

period = 1

port = 3306

preferred_backup_period = []

preferred_backup_time = "02:00Z-03:00Z"

profile = ""

region = ""

security_group_ids = []

security_ips = []

shared_credentials_file = ""

skip_region_validation = false

tags = {}

vswitch_id = ""


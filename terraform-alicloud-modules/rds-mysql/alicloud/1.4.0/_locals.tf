
locals {
  account_name = var.account_name
  account_password = var.account_password
  account_privilege = var.account_privilege
  account_type = var.account_type
  alarm_rule_contact_groups = var.alarm_rule_contact_groups
  alarm_rule_effective_interval = var.alarm_rule_effective_interval
  alarm_rule_name = var.alarm_rule_name
  alarm_rule_operator = var.alarm_rule_operator
  alarm_rule_period = var.alarm_rule_period
  alarm_rule_silence_time = var.alarm_rule_silence_time
  alarm_rule_statistics = var.alarm_rule_statistics
  alarm_rule_threshold = var.alarm_rule_threshold
  alarm_rule_triggered_count = var.alarm_rule_triggered_count
  allocate_public_connection = var.allocate_public_connection
  backup_retention_period = var.backup_retention_period
  connection_prefix = var.connection_prefix
  create_account = var.create_account
  create_backup_policy = var.create_backup_policy
  create_database = var.create_database
  create_instance = var.create_instance
  databases = var.databases
  enable_alarm_rule = var.enable_alarm_rule
  enable_backup_log = var.enable_backup_log
  engine_version = var.engine_version
  existing_instance_id = var.existing_instance_id
  instance_charge_type = var.instance_charge_type
  instance_name = var.instance_name
  instance_storage = var.instance_storage
  instance_type = var.instance_type
  log_backup_retention_period = var.log_backup_retention_period
  period = var.period
  port = var.port
  preferred_backup_period = var.preferred_backup_period
  preferred_backup_time = var.preferred_backup_time
  profile = var.profile
  region = var.region
  security_group_ids = var.security_group_ids
  security_ips = var.security_ips
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  tags = var.tags
  vswitch_id = var.vswitch_id
}

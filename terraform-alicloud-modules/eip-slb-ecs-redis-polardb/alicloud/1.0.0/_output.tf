
output "this_ecs_id" {
  description = ""
  value       = module.eip-slb-ecs-redis-polardb.this_ecs_id
}

output "this_eip_id" {
  description = ""
  value       = module.eip-slb-ecs-redis-polardb.this_eip_id
}

output "this_polar_db_instance_id" {
  description = ""
  value       = module.eip-slb-ecs-redis-polardb.this_polar_db_instance_id
}

output "this_redis_instance_id" {
  description = ""
  value       = module.eip-slb-ecs-redis-polardb.this_redis_instance_id
}

output "this_security_group_id" {
  description = ""
  value       = module.eip-slb-ecs-redis-polardb.this_security_group_id
}

output "this_slb_id" {
  description = ""
  value       = module.eip-slb-ecs-redis-polardb.this_slb_id
}

output "this_vpc_id" {
  description = ""
  value       = module.eip-slb-ecs-redis-polardb.this_vpc_id
}

output "this_vswitch_id" {
  description = ""
  value       = module.eip-slb-ecs-redis-polardb.this_vswitch_id
}



output "this_bandwidth_limit" {
  description = "The bandwidth configured for the interconnected regions communication."
  value       = module.cen-bandwidth.this_bandwidth_limit
}

output "this_bandwidth_package_id" {
  description = "The ID of the bandwidth package"
  value       = module.cen-bandwidth.this_bandwidth_package_id
}

output "this_instance_id" {
  description = "The ID of the CEN instance"
  value       = module.cen-bandwidth.this_instance_id
}

output "this_region_ids" {
  description = "List of the two regions to interconnect."
  value       = module.cen-bandwidth.this_region_ids
}


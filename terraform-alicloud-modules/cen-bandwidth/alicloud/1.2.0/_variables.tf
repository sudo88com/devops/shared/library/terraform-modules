
variable "attach_bandwidth_package" {
  type        = string
  description = "Whether to attach the CEN bandwidth package to one CEN instance."
  default     = false
}

variable "bandwidth" {
  type        = string
  description = "The bandwidth in Mbps of the bandwidth package."
  default     = ""
}

variable "bandwidth_limit" {
  type        = string
  description = " The bandwidth configured for the interconnected regions communication."
  default     = ""
}

variable "bandwidth_package_id" {
  type        = string
  description = "The ID of the CEN bandwidth package."
  default     = ""
}

variable "charge_type" {
  type        = string
  description = "The billing method. Valid value: PostPaid, PrePaid."
  default     = ""
}

variable "geographic_region_ids" {
  type        = list(string)
  description = "List of the two areas to connect, like [\"China\", \"China\"]."
  default     = ""
}

variable "instance_id" {
  type        = string
  description = "The ID of the CEN instance."
  default     = ""
}

variable "name" {
  type        = string
  description = "Name of the CEN bandwidth package."
  default     = ""
}

variable "new_bandwidth_package" {
  type        = string
  description = "Whether to create a CEN bandwidth package."
  default     = false
}

variable "period" {
  type        = string
  description = "The purchase period in month."
  default     = 1
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "region_ids" {
  type        = list(string)
  description = "List of the two regions to interconnect."
  default     = ""
}

variable "set_bandwidth_limit" {
  type        = string
  description = "Whether to set the bandwidth limit."
  default     = false
}


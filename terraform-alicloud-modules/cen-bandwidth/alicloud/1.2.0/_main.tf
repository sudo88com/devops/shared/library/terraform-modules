
module "cen-bandwidth" {
  source = "terraform-aws-modules/cen-bandwidth/aws"
  version = "1.2.0"
  attach_bandwidth_package = var.attach_bandwidth_package
  bandwidth = var.bandwidth
  bandwidth_limit = var.bandwidth_limit
  bandwidth_package_id = var.bandwidth_package_id
  charge_type = var.charge_type
  geographic_region_ids = var.geographic_region_ids
  instance_id = var.instance_id
  name = var.name
  new_bandwidth_package = var.new_bandwidth_package
  period = var.period
  region = var.region
  region_ids = var.region_ids
  set_bandwidth_limit = var.set_bandwidth_limit
}


module "slb-acl" {
  source = "terraform-aws-modules/slb-acl/aws"
  version = "1.3.0"
  entry_list = var.entry_list
  ip_version = var.ip_version
  name = var.name
  region = var.region
}

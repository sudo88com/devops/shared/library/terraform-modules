
variable "entry_list" {
  type        = list(object({
    entry   = string
    comment = string
  }))
  description = " A list of entry (IP addresses or CIDR blocks) to be added. At most 50 etnry can be supported in one resource. It contains two sub-fields as: entry(IP addresses or CIDR blocks), comment(the comment of the entry)"
  default     = ""
}

variable "ip_version" {
  type        = string
  description = "The IP Version of access control list is the type of its entry (IP addresses or CIDR blocks). It values ipv4/ipv6. Our plugin provides a default ip_version: ipv4."
  default     = "ipv4"
}

variable "name" {
  type        = string
  description = "the Name of the access control list."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}


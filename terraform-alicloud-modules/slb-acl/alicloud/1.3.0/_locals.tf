
locals {
  entry_list = var.entry_list
  ip_version = var.ip_version
  name = var.name
  region = var.region
}

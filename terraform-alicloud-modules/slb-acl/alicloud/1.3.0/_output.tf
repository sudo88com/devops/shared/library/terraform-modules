
output "this_slb_acl_entry_list" {
  description = ""
  value       = module.slb-acl.this_slb_acl_entry_list
}

output "this_slb_acl_id" {
  description = ""
  value       = module.slb-acl.this_slb_acl_id
}

output "this_slb_acl_ip_version" {
  description = ""
  value       = module.slb-acl.this_slb_acl_ip_version
}

output "this_slb_acl_name" {
  description = ""
  value       = module.slb-acl.this_slb_acl_name
}


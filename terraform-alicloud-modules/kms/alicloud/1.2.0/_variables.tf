
variable "ciphertext_blob" {
  type        = string
  description = "The ciphertext of the data key encrypted with the primary CMK version."
  default     = ""
}

variable "create_kms" {
  type        = bool
  description = "Whether to create kms key resource. Default to true."
  default     = true
}

variable "decrypt" {
  type        = bool
  description = "Whether to decrypt ciphertext. Default to true.."
  default     = true
}

variable "deletion_window_in_days" {
  type        = string
  description = "Duration in days after which the key is deleted after destruction of the resource, must be between 7 and 30 days. Default to 30 days."
  default     = 30
}

variable "description" {
  type        = string
  description = "The description of the key as viewed in Alicloud console. Default to 'From Terraform'."
  default     = "From Terraform"
}

variable "encrypt" {
  type        = bool
  description = "Whether to encrypt plaintext. Default to true.."
  default     = true
}

variable "encryption_context" {
  type        = map(string)
  description = "The Encryption context. If you specify this parameter here, it is also required when you call the Decrypt API operation. For more information, see [Encryption Context](https://www.alibabacloud.com/help/doc-detail/42975.htm)."
  default     = {}
}

variable "existing_key_id" {
  type        = string
  description = "The globally unique ID of the CMK."
  default     = ""
}

variable "is_enabled" {
  type        = bool
  description = "Specifies whether the key is enabled. Default to true."
  default     = true
}

variable "key_usage" {
  type        = string
  description = "Specifies the usage of CMK. Currently, default to 'ENCRYPT/DECRYPT', indicating that CMK is used for encryption and decryption."
  default     = "ENCRYPT/DECRYPT"
}

variable "plaintext" {
  type        = string
  description = "The plaintext to be encrypted which must be encoded in Base64."
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip staticvalidation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "use_existing_key" {
  type        = bool
  description = "Whether to create key. If false, you can specify an existing key."
  default     = false
}


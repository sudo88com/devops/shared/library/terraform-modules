
locals {
  ciphertext_blob = var.ciphertext_blob
  create_kms = var.create_kms
  decrypt = var.decrypt
  deletion_window_in_days = var.deletion_window_in_days
  description = var.description
  encrypt = var.encrypt
  encryption_context = var.encryption_context
  existing_key_id = var.existing_key_id
  is_enabled = var.is_enabled
  key_usage = var.key_usage
  plaintext = var.plaintext
  profile = var.profile
  region = var.region
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  use_existing_key = var.use_existing_key
}

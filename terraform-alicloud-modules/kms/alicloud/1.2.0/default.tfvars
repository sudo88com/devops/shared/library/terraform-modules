
ciphertext_blob = ""

create_kms = true

decrypt = true

deletion_window_in_days = 30

description = "From Terraform"

encrypt = true

encryption_context = {}

existing_key_id = ""

is_enabled = true

key_usage = "ENCRYPT/DECRYPT"

plaintext = ""

profile = ""

region = ""

shared_credentials_file = ""

skip_region_validation = false

use_existing_key = false


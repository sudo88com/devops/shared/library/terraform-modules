
availability_zone = ""

available_disk_category = "cloud_efficiency"

category = "cloud_efficiency"

description = "tf-eip-slb-ecs-rds-description"

ecs_size = 1200

eip_bandwidth = "10"

eip_internet_charge_type = "PayByBandwidth"

engine = "MySQL"

engine_version = "5.6"

image_id = "ubuntu_18_04_64_20G_alibase_20190624.vhd"

instance_charge_type = "Postpaid"

instance_storage = "30"

instance_type = "ecs.n4.large"

internet_max_bandwidth_out = 10

monitoring_period = "60"

name = "tf-eip-slb-ecs-rds"

rds_instance_type = "rds.mysql.s2.large"

security_group_ids = []

slb_address_type = "intranet"

slb_spec = "slb.s2.small"

slb_tags_info = "create for internet"

system_disk_category = "cloud_efficiency"

system_disk_description = "system_disk_description"

system_disk_name = "system_disk"

vswitch_id = ""


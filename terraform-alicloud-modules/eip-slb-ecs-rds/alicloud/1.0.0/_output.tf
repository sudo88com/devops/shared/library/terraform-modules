
output "this_ecs_id" {
  description = ""
  value       = module.eip-slb-ecs-rds.this_ecs_id
}

output "this_ecs_name" {
  description = ""
  value       = module.eip-slb-ecs-rds.this_ecs_name
}

output "this_eip_id" {
  description = ""
  value       = module.eip-slb-ecs-rds.this_eip_id
}

output "this_rds_id" {
  description = ""
  value       = module.eip-slb-ecs-rds.this_rds_id
}

output "this_slb_id" {
  description = ""
  value       = module.eip-slb-ecs-rds.this_slb_id
}


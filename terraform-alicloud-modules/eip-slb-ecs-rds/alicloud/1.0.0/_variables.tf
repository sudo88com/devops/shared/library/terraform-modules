
variable "availability_zone" {
  type        = string
  description = "The available zone to launch modules."
  default     = ""
}

variable "available_disk_category" {
  type        = string
  description = "The specification of available disk category."
  default     = "cloud_efficiency"
}

variable "category" {
  type        = string
  description = "The specification of the category."
  default     = "cloud_efficiency"
}

variable "description" {
  type        = string
  description = "The specification of module description."
  default     = "tf-eip-slb-ecs-rds-description"
}

variable "ecs_size" {
  type        = number
  description = "The specification of the ecs size."
  default     = 1200
}

variable "eip_bandwidth" {
  type        = string
  description = "The specification of the eip bandwidth."
  default     = "10"
}

variable "eip_internet_charge_type" {
  type        = string
  description = "The specification of the eip internet charge type."
  default     = "PayByBandwidth"
}

variable "engine" {
  type        = string
  description = "The specification of the engine."
  default     = "MySQL"
}

variable "engine_version" {
  type        = string
  description = "The specification of the engine version."
  default     = "5.6"
}

variable "image_id" {
  type        = string
  description = "The specification of the image id."
  default     = "ubuntu_18_04_64_20G_alibase_20190624.vhd"
}

variable "instance_charge_type" {
  type        = string
  description = "The specification of the instance charge type."
  default     = "Postpaid"
}

variable "instance_storage" {
  type        = string
  description = "The specification of the instance storage."
  default     = "30"
}

variable "instance_type" {
  type        = string
  description = "The specification of the instance type."
  default     = "ecs.n4.large"
}

variable "internet_max_bandwidth_out" {
  type        = number
  description = "The specification of the internet max bandwidth out."
  default     = 10
}

variable "monitoring_period" {
  type        = string
  description = "The specification of the monitoring period."
  default     = "60"
}

variable "name" {
  type        = string
  description = "The specification of module name."
  default     = "tf-eip-slb-ecs-rds"
}

variable "rds_instance_type" {
  type        = string
  description = "The specification of the rds instance type."
  default     = "rds.mysql.s2.large"
}

variable "security_group_ids" {
  type        = list(string)
  description = "A list of security group ids to associate with."
  default     = []
}

variable "slb_address_type" {
  type        = string
  description = "The specification of the slb intranet."
  default     = "intranet"
}

variable "slb_spec" {
  type        = string
  description = "The specification of the slb spec."
  default     = "slb.s2.small"
}

variable "slb_tags_info" {
  type        = string
  description = "The specification of the slb tags info."
  default     = "create for internet"
}

variable "system_disk_category" {
  type        = string
  description = "The specification of the system disk category."
  default     = "cloud_efficiency"
}

variable "system_disk_description" {
  type        = string
  description = "The specification of the system disk description."
  default     = "system_disk_description"
}

variable "system_disk_name" {
  type        = string
  description = "The specification of the system disk name."
  default     = "system_disk"
}

variable "vswitch_id" {
  type        = string
  description = "VSwitch variables, if vswitch_id is empty, then the net_type = classic."
  default     = ""
}


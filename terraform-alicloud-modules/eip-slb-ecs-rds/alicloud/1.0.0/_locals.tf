
locals {
  availability_zone = var.availability_zone
  available_disk_category = var.available_disk_category
  category = var.category
  description = var.description
  ecs_size = var.ecs_size
  eip_bandwidth = var.eip_bandwidth
  eip_internet_charge_type = var.eip_internet_charge_type
  engine = var.engine
  engine_version = var.engine_version
  image_id = var.image_id
  instance_charge_type = var.instance_charge_type
  instance_storage = var.instance_storage
  instance_type = var.instance_type
  internet_max_bandwidth_out = var.internet_max_bandwidth_out
  monitoring_period = var.monitoring_period
  name = var.name
  rds_instance_type = var.rds_instance_type
  security_group_ids = var.security_group_ids
  slb_address_type = var.slb_address_type
  slb_spec = var.slb_spec
  slb_tags_info = var.slb_tags_info
  system_disk_category = var.system_disk_category
  system_disk_description = var.system_disk_description
  system_disk_name = var.system_disk_name
  vswitch_id = var.vswitch_id
}

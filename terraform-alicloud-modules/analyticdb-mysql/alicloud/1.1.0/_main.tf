
module "analyticdb-mysql" {
  source = "terraform-aws-modules/analyticdb-mysql/aws"
  version = "1.1.0"
  availability_zone = var.availability_zone
  create_cluster = var.create_cluster
  db_cluster_category = var.db_cluster_category
  db_cluster_version = var.db_cluster_version
  db_node_class = var.db_node_class
  db_node_count = var.db_node_count
  db_node_storage = var.db_node_storage
  description = var.description
  existing_cluster_id = var.existing_cluster_id
  mode = var.mode
  pay_type = var.pay_type
  profile = var.profile
  region = var.region
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  vswitch_id = var.vswitch_id
}

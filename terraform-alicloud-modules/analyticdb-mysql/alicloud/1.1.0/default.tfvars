
availability_zone = ""

create_cluster = true

db_cluster_category = "Cluster"

db_cluster_version = "3.0"

db_node_class = "C8"

db_node_count = 2

db_node_storage = 100

description = "tf-module-adb"

existing_cluster_id = ""

mode = "reserver"

pay_type = "PostPaid"

profile = ""

region = ""

shared_credentials_file = ""

skip_region_validation = false

vswitch_id = ""


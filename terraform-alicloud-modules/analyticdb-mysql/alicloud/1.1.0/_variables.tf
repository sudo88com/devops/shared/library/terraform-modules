
variable "availability_zone" {
  type        = string
  description = "The available zone to launch ADB cluster."
  default     = ""
}

variable "create_cluster" {
  type        = bool
  description = "Whether to create ADB cluster. If false, you can use a existing ADB cluster by setting 'existing_cluster_id'."
  default     = true
}

variable "db_cluster_category" {
  type        = string
  description = "The category of the cluster. Valid value: Basic, Cluster."
  default     = "Cluster"
}

variable "db_cluster_version" {
  type        = string
  description = "The version number of the cluster. Valid value: 3.0."
  default     = "3.0"
}

variable "db_node_class" {
  type        = string
  description = "The node class of the cluster instance type. Valid value: C8, C24 and so on."
  default     = "C8"
}

variable "db_node_count" {
  type        = number
  description = "cluster node count, [2-128]."
  default     = 2
}

variable "db_node_storage" {
  type        = number
  description = "cluster node disk storage size, unit: GB. [100-1000]. all storage size = db_node_count * db_node_storage(2 * 100 = 200GB)."
  default     = 100
}

variable "description" {
  type        = string
  description = "Display name of the cluster, [2, 128] English or Chinese characters, must start with a letter or Chinese in size, can contain numbers, '_' or '.', '-'."
  default     = "tf-module-adb"
}

variable "existing_cluster_id" {
  type        = string
  description = "The Id of an existing ADB cluster. If set, the 'create_cluster' will be ignored."
  default     = ""
}

variable "mode" {
  type        = string
  description = " The mode of the cluster. Valid values: reserver, flexible."
  default     = "reserver"
}

variable "pay_type" {
  type        = string
  description = "pay type, Valid value:Prepaid: The subscription billing method is used, Postpaid: The pay-as-you-go billing method is used."
  default     = "PostPaid"
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "vswitch_id" {
  type        = string
  description = "VSwitch variables, if vswitch_id is empty, then the net_type = classic."
  default     = ""
}


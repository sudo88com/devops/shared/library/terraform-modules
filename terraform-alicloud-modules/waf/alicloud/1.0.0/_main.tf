
module "waf" {
  source = "terraform-aws-modules/waf/aws"
  version = "1.0.0"
  big_screen = var.big_screen
  exclusive_ip_package = var.exclusive_ip_package
  ext_bandwidth = var.ext_bandwidth
  ext_domain_package = var.ext_domain_package
  log_storage = var.log_storage
  log_time = var.log_time
  package_code = var.package_code
  period = var.period
  prefessional_service = var.prefessional_service
  resource_group_id = var.resource_group_id
  subscription_type = var.subscription_type
  waf_log = var.waf_log
}

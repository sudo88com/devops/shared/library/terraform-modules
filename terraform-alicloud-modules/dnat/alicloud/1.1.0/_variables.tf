
variable "create" {
  type        = bool
  description = "Whether to create dnat entries. If true, the 'entries' should be set."
  default     = true
}

variable "dnat_table_id" {
  type        = string
  description = "The dnat table id to use on all dnat entries."
  default     = ""
}

variable "entries" {
  type        = list(map(string))
  description = "A list of entries to create. Each item valid keys: 'name'(default to a string with prefix 'tf-dnat-entry' and numerical suffix), 'ip_protocol'(default to 'any'), 'external_ip'(if not, use root parameter 'external_ip'), 'external_port'(default to 'any'), 'internal_ip'(required), 'internal_port'(default to the 'external_port')."
  default     = []
}

variable "external_ip" {
  type        = string
  description = "The public ip address to use on all dnat entries."
  default     = ""
}

variable "nat_gateway_id" {
  type        = string
  description = "The id of a nat gateway used to fetch the 'dnat_table_id'."
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}



locals {
  create = var.create
  dnat_table_id = var.dnat_table_id
  entries = var.entries
  external_ip = var.external_ip
  nat_gateway_id = var.nat_gateway_id
  profile = var.profile
  region = var.region
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
}

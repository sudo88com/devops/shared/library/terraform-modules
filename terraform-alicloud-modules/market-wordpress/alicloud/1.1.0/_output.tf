
output "this_dns_record_host_record" {
  description = "The host_record of dns record."
  value       = module.market-wordpress.this_dns_record_host_record
}

output "this_dns_record_id" {
  description = "The id of dns record."
  value       = module.market-wordpress.this_dns_record_id
}

output "this_dns_record_name" {
  description = "The domain of dns record."
  value       = module.market-wordpress.this_dns_record_name
}

output "this_ecs_instance_availability_zone" {
  description = "The ecs instance avalability zone."
  value       = module.market-wordpress.this_ecs_instance_availability_zone
}

output "this_ecs_instance_deletion_protection" {
  description = "Whether enable the deletion protection or not."
  value       = module.market-wordpress.this_ecs_instance_deletion_protection
}

output "this_ecs_instance_description" {
  description = "The description of ecs instance."
  value       = module.market-wordpress.this_ecs_instance_description
}

output "this_ecs_instance_host_name" {
  description = "The host name of ecs instance."
  value       = module.market-wordpress.this_ecs_instance_host_name
}

output "this_ecs_instance_id" {
  description = "The id of ecs instance."
  value       = module.market-wordpress.this_ecs_instance_id
}

output "this_ecs_instance_image_id" {
  description = "The ID of the image used to launch the instance."
  value       = module.market-wordpress.this_ecs_instance_image_id
}

output "this_ecs_instance_instance_charge_type" {
  description = "The charge type of ecs instance."
  value       = module.market-wordpress.this_ecs_instance_instance_charge_type
}

output "this_ecs_instance_internet_charge_type" {
  description = "Internet charge type of the instance."
  value       = module.market-wordpress.this_ecs_instance_internet_charge_type
}

output "this_ecs_instance_internet_max_bandwidth_out" {
  description = "Maximum outgoing bandwidth to the public network."
  value       = module.market-wordpress.this_ecs_instance_internet_max_bandwidth_out
}

output "this_ecs_instance_name" {
  description = "The name of ecs instance."
  value       = module.market-wordpress.this_ecs_instance_name
}

output "this_ecs_instance_period" {
  description = "The duration that buy the resource."
  value       = module.market-wordpress.this_ecs_instance_period
}

output "this_ecs_instance_private_ip" {
  description = "The private ip of ecs instance."
  value       = module.market-wordpress.this_ecs_instance_private_ip
}

output "this_ecs_instance_public_ip" {
  description = "The ecs instance public ip."
  value       = module.market-wordpress.this_ecs_instance_public_ip
}

output "this_ecs_instance_resource_group_id" {
  description = "The Id of resource group which the ECS instance belongs."
  value       = module.market-wordpress.this_ecs_instance_resource_group_id
}

output "this_ecs_instance_system_disk_category" {
  description = "The system disk category used to launch one ecs instance."
  value       = module.market-wordpress.this_ecs_instance_system_disk_category
}

output "this_ecs_instance_system_disk_size" {
  description = "Size of the system disk, in GB."
  value       = module.market-wordpress.this_ecs_instance_system_disk_size
}

output "this_ecs_instance_tags" {
  description = "The tags of ecs instance."
  value       = module.market-wordpress.this_ecs_instance_tags
}

output "this_ecs_instance_type" {
  description = "The type of instance."
  value       = module.market-wordpress.this_ecs_instance_type
}

output "this_security_group_ids" {
  description = "A list of security group ids to associate with."
  value       = module.market-wordpress.this_security_group_ids
}

output "this_slb_id" {
  description = "The slb id."
  value       = module.market-wordpress.this_slb_id
}

output "this_slb_listener_frontend_port" {
  description = "The frontend_port of slb listener."
  value       = module.market-wordpress.this_slb_listener_frontend_port
}

output "this_slb_listener_protocol" {
  description = "The protocol of slb listener."
  value       = module.market-wordpress.this_slb_listener_protocol
}

output "this_slb_listener_server_group_id" {
  description = "The backend server group id of slb listener."
  value       = module.market-wordpress.this_slb_listener_server_group_id
}

output "this_slb_name" {
  description = "The slb name."
  value       = module.market-wordpress.this_slb_name
}

output "this_slb_public_address" {
  description = "The slb public address."
  value       = module.market-wordpress.this_slb_public_address
}

output "this_vswitch_id" {
  description = "The virtual switch ID to launch ECS instance in VPC."
  value       = module.market-wordpress.this_vswitch_id
}

output "this_wordpress_url" {
  description = "The url of wordpress."
  value       = module.market-wordpress.this_wordpress_url
}


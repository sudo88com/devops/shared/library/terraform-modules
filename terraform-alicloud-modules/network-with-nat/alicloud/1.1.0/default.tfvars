
availability_zones = []

cbp_bandwidth = 10

cbp_internet_charge_type = "PayByTraffic"

cbp_ratio = 100

computed_snat_with_source_cidr = []

computed_snat_with_vswitch_id = []

create_dnat = false

create_eip = false

create_nat = true

create_snat = false

create_vpc = true

dnat_entries = []

dnat_external_ip = ""

eip_bandwidth = 5

eip_instance_charge_type = "PostPaid"

eip_internet_charge_type = "PayByTraffic"

eip_isp = ""

eip_name = "tf-module-network-with-nat"

eip_period = 1

eip_tags = {}

existing_vpc_id = ""

nat_instance_charge_type = "PostPaid"

nat_name = "tf-module-network-with-nat"

nat_period = 1

nat_specification = "Small"

number_of_dnat_eip = 0

number_of_snat_eip = 0

profile = ""

region = ""

shared_credentials_file = ""

skip_region_validation = false

snat_ips = []

snat_with_instance_ids = []

snat_with_source_cidrs = []

snat_with_vswitch_ids = []

tags = {}

use_existing_vpc = false

vpc_cidr = "172.16.0.0/12"

vpc_name = "tf-module-network-with-nat"

vpc_tags = {}

vswitch_cidrs = []

vswitch_name = "tf-module-network-with-nat"

vswitch_tags = {}


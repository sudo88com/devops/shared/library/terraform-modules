
module "network-with-nat" {
  source = "terraform-aws-modules/network-with-nat/aws"
  version = "1.1.0"
  availability_zones = var.availability_zones
  cbp_bandwidth = var.cbp_bandwidth
  cbp_internet_charge_type = var.cbp_internet_charge_type
  cbp_ratio = var.cbp_ratio
  computed_snat_with_source_cidr = var.computed_snat_with_source_cidr
  computed_snat_with_vswitch_id = var.computed_snat_with_vswitch_id
  create_dnat = var.create_dnat
  create_eip = var.create_eip
  create_nat = var.create_nat
  create_snat = var.create_snat
  create_vpc = var.create_vpc
  dnat_entries = var.dnat_entries
  dnat_external_ip = var.dnat_external_ip
  eip_bandwidth = var.eip_bandwidth
  eip_instance_charge_type = var.eip_instance_charge_type
  eip_internet_charge_type = var.eip_internet_charge_type
  eip_isp = var.eip_isp
  eip_name = var.eip_name
  eip_period = var.eip_period
  eip_tags = var.eip_tags
  existing_vpc_id = var.existing_vpc_id
  nat_instance_charge_type = var.nat_instance_charge_type
  nat_name = var.nat_name
  nat_period = var.nat_period
  nat_specification = var.nat_specification
  number_of_dnat_eip = var.number_of_dnat_eip
  number_of_snat_eip = var.number_of_snat_eip
  profile = var.profile
  region = var.region
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  snat_ips = var.snat_ips
  snat_with_instance_ids = var.snat_with_instance_ids
  snat_with_source_cidrs = var.snat_with_source_cidrs
  snat_with_vswitch_ids = var.snat_with_vswitch_ids
  tags = var.tags
  use_existing_vpc = var.use_existing_vpc
  vpc_cidr = var.vpc_cidr
  vpc_name = var.vpc_name
  vpc_tags = var.vpc_tags
  vswitch_cidrs = var.vswitch_cidrs
  vswitch_name = var.vswitch_name
  vswitch_tags = var.vswitch_tags
}

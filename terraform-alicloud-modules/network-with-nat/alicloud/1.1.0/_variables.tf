
variable "availability_zones" {
  type        = list(string)
  description = "List available zones to launch several VSwitches."
  default     = []
}

variable "cbp_bandwidth" {
  type        = number
  description = "The bandwidth of the common bandwidth package, in Mbps."
  default     = 10
}

variable "cbp_internet_charge_type" {
  type        = string
  description = "The billing method of the common bandwidth package. Valid values are 'PayByBandwidth' and 'PayBy95' and 'PayByTraffic'. 'PayBy95' is pay by classic 95th percentile pricing. International Account doesn't supports 'PayByBandwidth' and 'PayBy95'. Default to 'PayByTraffic'."
  default     = "PayByTraffic"
}

variable "cbp_ratio" {
  type        = string
  description = "Ratio of the common bandwidth package."
  default     = 100
}

variable "computed_snat_with_source_cidr" {
  type        = list(map(string))
  description = "List of computed snat entries to create by cidr blocks. Each item valid keys: 'source_cidr'(required), 'snat_ip'(if not, use root parameter 'snat_ips', using comma joinor to set multi ips), 'name'(if not, will return one automatically)."
  default     = []
}

variable "computed_snat_with_vswitch_id" {
  type        = list(map(string))
  description = "List of computed snat entries to create by vswitch ids. Each item valid keys: 'vswitch_id'(required), 'snat_ip'(if not, use root parameter 'snat_ips', using comma joinor to set multi ips), 'name'(if not, will return one automatically)."
  default     = []
}

variable "create_dnat" {
  type        = bool
  description = "Whether to create dnat entries. If true, the 'entries' should be set."
  default     = false
}

variable "create_eip" {
  type        = bool
  description = "Whether to create new EIP and bind it to this Nat gateway. If true, the 'number_of_dnat_eip' or 'number_of_snat_eip' should not be empty."
  default     = false
}

variable "create_nat" {
  type        = bool
  description = "Whether to create nat gateway."
  default     = true
}

variable "create_snat" {
  type        = bool
  description = "Whether to create snat entries. If true, the 'snat_with_source_cidrs' or 'snat_with_vswitch_ids' or 'snat_with_instance_ids' should be set."
  default     = false
}

variable "create_vpc" {
  type        = bool
  description = "Whether to create vpc. If false, you can specify an existing vpc by setting 'existing_vpc_id'."
  default     = true
}

variable "dnat_entries" {
  type        = list(map(string))
  description = "A list of entries to create. Each item valid keys: 'name'(default to a string with prefix 'tf-dnat-entry' and numerical suffix), 'ip_protocol'(default to 'any'), 'external_ip'(if not, use root parameter 'external_ip'), 'external_port'(default to 'any'), 'internal_ip'(required), 'internal_port'(default to the 'external_port')."
  default     = []
}

variable "dnat_external_ip" {
  type        = string
  description = "The public ip address to use on all dnat entries."
  default     = ""
}

variable "eip_bandwidth" {
  type        = number
  description = "Maximum bandwidth to the elastic public network, measured in Mbps (Mega bit per second)."
  default     = 5
}

variable "eip_instance_charge_type" {
  type        = string
  description = "Elastic IP instance charge type."
  default     = "PostPaid"
}

variable "eip_internet_charge_type" {
  type        = string
  description = "Internet charge type of the EIP, Valid values are 'PayByBandwidth', 'PayByTraffic'. "
  default     = "PayByTraffic"
}

variable "eip_isp" {
  type        = string
  description = "The line type of the Elastic IP instance."
  default     = ""
}

variable "eip_name" {
  type        = string
  description = "Name to be used on all eip as prefix. Default to 'TF-EIP-for-Nat'. The final default name would be TF-EIP-for-Nat001, TF-EIP-for-Nat002 and so on."
  default     = "tf-module-network-with-nat"
}

variable "eip_period" {
  type        = number
  description = "The duration that you will buy the EIP, in month."
  default     = 1
}

variable "eip_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the EIP instance resource."
  default     = {}
}

variable "existing_vpc_id" {
  type        = string
  description = "The vpc id used to launch several vswitches."
  default     = ""
}

variable "nat_instance_charge_type" {
  type        = string
  description = "The charge type of the nat gateway. Choices are 'PostPaid' and 'PrePaid'."
  default     = "PostPaid"
}

variable "nat_name" {
  type        = string
  description = "Name of a new nat gateway."
  default     = "tf-module-network-with-nat"
}

variable "nat_period" {
  type        = number
  description = "The charge duration of the PrePaid nat gateway, in month."
  default     = 1
}

variable "nat_specification" {
  type        = string
  description = "The specification of nat gateway."
  default     = "Small"
}

variable "number_of_dnat_eip" {
  type        = number
  description = "Number of EIP instance used to bind with this Dnat."
  default     = 0
}

variable "number_of_snat_eip" {
  type        = number
  description = "Number of EIP instance used to bind with this Snat."
  default     = 0
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "snat_ips" {
  type        = list(string)
  description = "The public ip addresses to use on all snat entries."
  default     = []
}

variable "snat_with_instance_ids" {
  type        = list(map(string))
  description = "List of snat entries to create by ecs instance ids. Each item valid keys: 'instance_ids'(required, using comma joinor to set multi instance ids), 'snat_ip'(if not, use root parameter 'snat_ips', using comma joinor to set multi ips), 'name'(if not, will return one automatically)."
  default     = []
}

variable "snat_with_source_cidrs" {
  type        = list(map(string))
  description = "List of snat entries to create by cidr blocks. Each item valid keys: 'source_cidrs'(required, using comma joinor to set multi cidrs), 'snat_ip'(if not, use root parameter 'snat_ips', using comma joinor to set multi ips), 'name'(if not, will return one automatically)."
  default     = []
}

variable "snat_with_vswitch_ids" {
  type        = list(map(string))
  description = "List of snat entries to create by vswitch ids. Each item valid keys: 'vswitch_ids'(required, using comma joinor to set multi vswitch ids), 'snat_ip'(if not, use root parameter 'snat_ips', using comma joinor to set multi ips), 'name'(if not, will return one automatically)."
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "The common tags will apply to all of resources."
  default     = {}
}

variable "use_existing_vpc" {
  type        = bool
  description = "The vpc id used to launch several vswitches. If set, the 'create_vpc' will be ignored."
  default     = false
}

variable "vpc_cidr" {
  type        = string
  description = "The cidr block used to launch a new vpc."
  default     = "172.16.0.0/12"
}

variable "vpc_name" {
  type        = string
  description = "The vpc name used to launch a new vpc."
  default     = "tf-module-network-with-nat"
}

variable "vpc_tags" {
  type        = map(string)
  description = "The tags used to launch a new vpc."
  default     = {}
}

variable "vswitch_cidrs" {
  type        = list(string)
  description = "List of cidr blocks used to launch several new vswitches. If not set, there is no new vswitches will be created."
  default     = []
}

variable "vswitch_name" {
  type        = string
  description = "The vswitch name prefix used to launch several new vswitches."
  default     = "tf-module-network-with-nat"
}

variable "vswitch_tags" {
  type        = map(string)
  description = "The tags used to launch serveral vswitches."
  default     = {}
}


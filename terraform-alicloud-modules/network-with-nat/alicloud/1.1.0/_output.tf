
output "this_availability_zones" {
  description = "List of availability zones in which vswitches launched."
  value       = module.network-with-nat.this_availability_zones
}

output "this_eip_dnat_ids" {
  description = "The id of new eips."
  value       = module.network-with-nat.this_eip_dnat_ids
}

output "this_eip_dnat_ips" {
  description = "The id of new eip addresses."
  value       = module.network-with-nat.this_eip_dnat_ips
}

output "this_eip_snat_ids" {
  description = "The id of new eips."
  value       = module.network-with-nat.this_eip_snat_ids
}

output "this_eip_snat_ips" {
  description = "The id of new eip addresses."
  value       = module.network-with-nat.this_eip_snat_ips
}

output "this_forward_table_id" {
  description = "The forward table id in this nat gateway. Seem as 'this_dnat_table_id'."
  value       = module.network-with-nat.this_forward_table_id
}

output "this_nat_gateway_description" {
  description = "The nat gateway id."
  value       = module.network-with-nat.this_nat_gateway_description
}

output "this_nat_gateway_id" {
  description = "The nat gateway id."
  value       = module.network-with-nat.this_nat_gateway_id
}

output "this_nat_gateway_name" {
  description = "The nat gateway name."
  value       = module.network-with-nat.this_nat_gateway_name
}

output "this_nat_gateway_spec" {
  description = "The nat gateway spec."
  value       = module.network-with-nat.this_nat_gateway_spec
}

output "this_snat_entry_id_of_computed_snat_with_source_cidrs" {
  description = "List of ids creating by computed_snat_with_source_cidrs."
  value       = module.network-with-nat.this_snat_entry_id_of_computed_snat_with_source_cidrs
}

output "this_snat_entry_id_of_computed_snat_with_vswitch_ids" {
  description = "List of ids creating by computed_snat_with_vswitch_ids."
  value       = module.network-with-nat.this_snat_entry_id_of_computed_snat_with_vswitch_ids
}

output "this_snat_entry_id_of_snat_with_instance_ids" {
  description = "List of ids creating by snat_with_instance_ids."
  value       = module.network-with-nat.this_snat_entry_id_of_snat_with_instance_ids
}

output "this_snat_entry_id_of_snat_with_source_cidrs" {
  description = "List of ids creating by snat_with_source_cidrs."
  value       = module.network-with-nat.this_snat_entry_id_of_snat_with_source_cidrs
}

output "this_snat_entry_id_of_snat_with_vswitch_ids" {
  description = "List of ids creating by snat_with_vswitch_ids."
  value       = module.network-with-nat.this_snat_entry_id_of_snat_with_vswitch_ids
}

output "this_snat_entry_name_of_computed_snat_with_source_cidrs" {
  description = "List of names creating by computed_snat_with_source_cidrs."
  value       = module.network-with-nat.this_snat_entry_name_of_computed_snat_with_source_cidrs
}

output "this_snat_entry_name_of_computed_snat_with_vswitch_ids" {
  description = "List of names creating by computed_snat_with_vswitch_ids."
  value       = module.network-with-nat.this_snat_entry_name_of_computed_snat_with_vswitch_ids
}

output "this_snat_entry_name_of_snat_with_instance_ids" {
  description = "List of names creating by snat_with_instance_ids."
  value       = module.network-with-nat.this_snat_entry_name_of_snat_with_instance_ids
}

output "this_snat_entry_name_of_snat_with_source_cidrs" {
  description = "List of names creating by snat_with_source_cidrs."
  value       = module.network-with-nat.this_snat_entry_name_of_snat_with_source_cidrs
}

output "this_snat_entry_name_of_snat_with_vswitch_ids" {
  description = "List of names creating by snat_with_vswitch_ids."
  value       = module.network-with-nat.this_snat_entry_name_of_snat_with_vswitch_ids
}

output "this_snat_table_id" {
  description = "The snat table id in this nat gateway."
  value       = module.network-with-nat.this_snat_table_id
}

output "this_vpc_cidr_block" {
  description = "The VPC cidr block."
  value       = module.network-with-nat.this_vpc_cidr_block
}

output "this_vpc_id" {
  description = "The ID of the VPC."
  value       = module.network-with-nat.this_vpc_id
}

output "this_vpc_name" {
  description = "The VPC name."
  value       = module.network-with-nat.this_vpc_name
}

output "this_vpc_tags" {
  description = "The tags of the VPC."
  value       = module.network-with-nat.this_vpc_tags
}

output "this_vswitch_cidr_block" {
  description = "The vswitch cidr block."
  value       = module.network-with-nat.this_vswitch_cidr_block
}

output "this_vswitch_ids" {
  description = "List of IDs of vswitch."
  value       = module.network-with-nat.this_vswitch_ids
}

output "this_vswitch_name" {
  description = "The name of vswitch."
  value       = module.network-with-nat.this_vswitch_name
}

output "this_vswitch_tags" {
  description = "List of IDs of vswitch."
  value       = module.network-with-nat.this_vswitch_tags
}


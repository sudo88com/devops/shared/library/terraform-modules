
output "this_ecs_id" {
  description = ""
  value       = module.eip-slb-ecs-polardb.this_ecs_id
}

output "this_ecs_name" {
  description = ""
  value       = module.eip-slb-ecs-polardb.this_ecs_name
}

output "this_eip_id" {
  description = ""
  value       = module.eip-slb-ecs-polardb.this_eip_id
}

output "this_polardb_database_id" {
  description = ""
  value       = module.eip-slb-ecs-polardb.this_polardb_database_id
}

output "this_polardb_database_name" {
  description = ""
  value       = module.eip-slb-ecs-polardb.this_polardb_database_name
}

output "this_slb_id" {
  description = ""
  value       = module.eip-slb-ecs-polardb.this_slb_id
}

output "this_slb_name" {
  description = ""
  value       = module.eip-slb-ecs-polardb.this_slb_name
}


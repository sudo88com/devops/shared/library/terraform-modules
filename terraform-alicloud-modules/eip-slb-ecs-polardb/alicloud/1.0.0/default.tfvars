
availability_zone = ""

category = "cloud_efficiency"

db_node_class = "polar.mysql.x4.large"

db_version = "8.0"

description = "tf-eip-slb-ecs-polardb-description"

ecs_size = 1200

eip_bandwidth = "10"

eip_internet_charge_type = "PayByBandwidth"

engine_version = "5.6"

image_id = "ubuntu_18_04_64_20G_alibase_20190624.vhd"

instance_charge_type = "Postpaid"

instance_storage = "30"

instance_type = "ecs.t1.xsmall"

internet_max_bandwidth_out = 10

monitoring_period = "60"

name = "tf-eip-slb-ecs-polardb"

pay_type = "PostPaid"

security_group_ids = []

slb_address_type = "intranet"

slb_spec = "slb.s2.small"

slb_tags_info = "create for internet"

system_disk_category = "cloud_efficiency"

system_disk_description = "system_disk_description"

system_disk_name = "system_disk"

vswitch_id = ""



variable "auto_renew" {
  type        = string
  description = "auto to renew cluster, valid when pay_type = PrePaid."
  default     = "false"
}

variable "availability_zone" {
  type        = string
  description = "The available zone to launch hbase instance."
  default     = ""
}

variable "cold_storage_size" {
  type        = number
  description = "cold storage disk size, 0 mean is_cold_storage = false."
  default     = 0
}

variable "core_disk_size" {
  type        = number
  description = "one disk size, unit: GB, default 4 disk per core node; all disk size = coreNodeSize * 4 * core_disk_size(2 * 4 * 100 =800GB)."
  default     = 100
}

variable "core_disk_type" {
  type        = string
  description = "core node disk type. Valid value:cloud_ssd, cloud_efficiency, cloud_essd, local_hdd_pro, local_ssd_pro."
  default     = "cloud_ssd"
}

variable "core_instance_quantity" {
  type        = number
  description = "core instance node count, [3-20]."
  default     = 3
}

variable "core_instance_type" {
  type        = string
  description = "The master instance type. Valid value: hbase.sn1.large, hbase.sn1.2xlarge, hbase.sn1.4xlarge, hbase.sn2.large, hbase.sn2.2xlarge, hbase.sn2.4xlarge and so on."
  default     = ""
}

variable "create" {
  type        = bool
  description = "Whether to create HBase instance."
  default     = true
}

variable "duration" {
  type        = string
  description = "The duration that you will buy HBase cluster, valid when pay_type = PrePaid."
  default     = 1
}

variable "engine_version" {
  type        = string
  description = "The version number of the database. Valid value: hbase:1.1, 2.0."
  default     = "2.0"
}

variable "instance_name" {
  type        = string
  description = "Display name of the instance, [2, 128] English or Chinese characters, must start with a letter or Chinese in size, can contain numbers, '_' or '.', '-'."
  default     = "tf-module-hbase"
}

variable "master_instance_type" {
  type        = string
  description = "The master instance type. Valid value: hbase.n1.medium, hbase.sn1.large, hbase.sn1.2xlarge, hbase.sn1.4xlarge and so on."
  default     = ""
}

variable "pay_type" {
  type        = string
  description = "pay type, Valid value:Prepaid: The subscription billing method is used, Postpaid: The pay-as-you-go billing method is used."
  default     = "PostPaid"
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "vswitch_id" {
  type        = string
  description = "VSwitch variables, if vswitch_id is empty, then the net_type = classic."
  default     = ""
}


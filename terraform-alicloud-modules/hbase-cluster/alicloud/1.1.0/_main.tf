
module "hbase-cluster" {
  source = "terraform-aws-modules/hbase-cluster/aws"
  version = "1.1.0"
  auto_renew = var.auto_renew
  availability_zone = var.availability_zone
  cold_storage_size = var.cold_storage_size
  core_disk_size = var.core_disk_size
  core_disk_type = var.core_disk_type
  core_instance_quantity = var.core_instance_quantity
  core_instance_type = var.core_instance_type
  create = var.create
  duration = var.duration
  engine_version = var.engine_version
  instance_name = var.instance_name
  master_instance_type = var.master_instance_type
  pay_type = var.pay_type
  profile = var.profile
  region = var.region
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  vswitch_id = var.vswitch_id
}

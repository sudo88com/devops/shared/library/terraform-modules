
auto_renew = "false"

availability_zone = ""

cold_storage_size = 0

core_disk_size = 100

core_disk_type = "cloud_ssd"

core_instance_quantity = 3

core_instance_type = ""

create = true

duration = 1

engine_version = "2.0"

instance_name = "tf-module-hbase"

master_instance_type = ""

pay_type = "PostPaid"

profile = ""

region = ""

shared_credentials_file = ""

skip_region_validation = false

vswitch_id = ""


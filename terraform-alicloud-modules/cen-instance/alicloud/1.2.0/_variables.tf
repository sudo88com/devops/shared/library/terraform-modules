
variable "cen_tags" {
  type        = map(string)
  description = "Additional tags for the CEN"
  default     = {}
}

variable "create_cen" {
  type        = bool
  description = "Controls if CEN should be created"
  default     = true
}

variable "description" {
  type        = string
  description = "The description of the CEN instance"
  default     = "TF"
}

variable "instances_attachment" {
  type        = map(object({ vpc_id = string, vpc_region_id = string, vpc_owner_id = string }))
  description = "Map of VPCs to grant access to the Alibaba account which hosts the CEN"
  default     = {}
}

variable "name" {
  type        = string
  description = "Name of the CEN instance"
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to add to all resources"
  default     = {}
}



module "cen-instance" {
  source = "terraform-aws-modules/cen-instance/aws"
  version = "1.2.0"
  cen_tags = var.cen_tags
  create_cen = var.create_cen
  description = var.description
  instances_attachment = var.instances_attachment
  name = var.name
  region = var.region
  tags = var.tags
}

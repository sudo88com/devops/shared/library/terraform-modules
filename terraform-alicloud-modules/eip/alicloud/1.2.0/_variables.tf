
variable "bandwidth" {
  type        = number
  description = "Maximum bandwidth to the elastic public network, measured in Mbps (Mega bit per second)."
  default     = 5
}

variable "computed_instances" {
  type        = list(object({
    instance_type = string
    instance_ids  = list(string)
    private_ips   = list(string)
  }))
  description = "List of ECS, NAT, SLB or NetworkInterface instances created by calling Corresponding ​​resource."
  default     = []
}

variable "create" {
  type        = string
  description = "Whether to create an EIP instance and whether to associate EIP with other resources."
  default     = true
}

variable "description" {
  type        = string
  description = " Description of the EIP, This description can have a string of 2 to 256 characters, It cannot begin with http:// or https://. Default value is null."
  default     = "An EIP came from terraform-alicloud-modules/eip"
}

variable "instance_charge_type" {
  type        = string
  description = "Elastic IP instance charge type."
  default     = "PostPaid"
}

variable "instances" {
  type        = list(object({
    instance_type = string
    instance_ids  = list(string)
    private_ips   = list(string)
  }))
  description = "A list of instances found by the condition. If this parameter is used, `number_of_eips` will be ignored."
  default     = []
}

variable "internet_charge_type" {
  type        = string
  description = "Internet charge type of the EIP, Valid values are `PayByBandwidth`, `PayByTraffic`. "
  default     = "PayByTraffic"
}

variable "isp" {
  type        = string
  description = "The line type of the Elastic IP instance."
  default     = "BGP"
}

variable "name" {
  type        = string
  description = "Name to be used on all resources as prefix. Default to 'TF-Module-EIP'. The final default name would be TF-Module-EIP001, TF-Module-EIP002 and so on."
  default     = "TF-Module-EIP"
}

variable "number_of_computed_instances" {
  type        = number
  description = "The number of instances created by calling the API. If this parameter is used, `number_of_eips` will be ignored."
  default     = 0
}

variable "number_of_eips" {
  type        = number
  description = "The number of eip to be created. This parameter will be ignored if `number_of_computed_instances` and `instances` is used."
  default     = 1
}

variable "period" {
  type        = number
  description = "The duration that you will buy the resource, in month."
  default     = 1
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.2.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "resource_group_id" {
  type        = string
  description = "The Id of resource group which the eip belongs."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.2.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = string
  description = "(Deprecated from version 1.2.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the EIP instance resource."
  default     = {}
}

variable "use_num_suffix" {
  type        = bool
  description = "Always append numerical suffix to instance name, even if number_of_instances is 1"
  default     = false
}


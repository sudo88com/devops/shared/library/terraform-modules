
module "eip" {
  source = "terraform-aws-modules/eip/aws"
  version = "1.2.0"
  bandwidth = var.bandwidth
  computed_instances = var.computed_instances
  create = var.create
  description = var.description
  instance_charge_type = var.instance_charge_type
  instances = var.instances
  internet_charge_type = var.internet_charge_type
  isp = var.isp
  name = var.name
  number_of_computed_instances = var.number_of_computed_instances
  number_of_eips = var.number_of_eips
  period = var.period
  profile = var.profile
  region = var.region
  resource_group_id = var.resource_group_id
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  tags = var.tags
  use_num_suffix = var.use_num_suffix
}

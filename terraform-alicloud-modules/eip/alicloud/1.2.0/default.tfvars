
bandwidth = 5

computed_instances = []

create = true

description = "An EIP came from terraform-alicloud-modules/eip"

instance_charge_type = "PostPaid"

instances = []

internet_charge_type = "PayByTraffic"

isp = "BGP"

name = "TF-Module-EIP"

number_of_computed_instances = 0

number_of_eips = 1

period = 1

profile = ""

region = ""

resource_group_id = ""

shared_credentials_file = ""

skip_region_validation = false

tags = {}

use_num_suffix = false


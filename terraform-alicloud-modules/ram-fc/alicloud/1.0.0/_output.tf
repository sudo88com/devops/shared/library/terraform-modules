
output "this_fc_service_id" {
  description = ""
  value       = module.ram-fc.this_fc_service_id
}

output "this_fc_service_name" {
  description = ""
  value       = module.ram-fc.this_fc_service_name
}

output "this_ram_id" {
  description = ""
  value       = module.ram-fc.this_ram_id
}

output "this_ram_name" {
  description = ""
  value       = module.ram-fc.this_ram_name
}


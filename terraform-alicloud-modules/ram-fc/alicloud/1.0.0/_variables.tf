
variable "fc_service_description" {
  type        = string
  description = "The specification of module fc service description."
  default     = "tf unit test"
}

variable "logstore" {
  type        = string
  description = "The specification of logstore."
  default     = ""
}

variable "name" {
  type        = string
  description = "The specification of module name."
  default     = "role_name"
}

variable "policy_name" {
  type        = string
  description = "The specification of module ram role description."
  default     = "AliyunLogFullAccess"
}

variable "policy_type" {
  type        = string
  description = "The specification of module policy type."
  default     = "System"
}

variable "project" {
  type        = string
  description = "The specification of project."
  default     = ""
}

variable "ram_role_description" {
  type        = string
  description = "The specification of module ram role description."
  default     = "this is a ram role test"
}


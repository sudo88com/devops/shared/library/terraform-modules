
locals {
  fc_service_description = var.fc_service_description
  logstore = var.logstore
  name = var.name
  policy_name = var.policy_name
  policy_type = var.policy_type
  project = var.project
  ram_role_description = var.ram_role_description
}

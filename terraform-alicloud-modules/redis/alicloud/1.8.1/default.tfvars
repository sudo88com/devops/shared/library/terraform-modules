
accounts = []

alarm_rule_contact_groups = []

alarm_rule_effective_interval = "0:00-2:00"

alarm_rule_name = ""

alarm_rule_operator = "=="

alarm_rule_period = 300

alarm_rule_silence_time = 86400

alarm_rule_statistics = "Average"

alarm_rule_threshold = ""

alarm_rule_triggered_count = 3

auto_renew = false

auto_renew_period = 1

availability_zone = ""

backup_policy_backup_period = []

backup_policy_backup_time = "02:00Z-03:00Z"

config = {}

create_account = true

create_instance = true

enable_alarm_rule = true

engine_version = ""

existing_instance_id = ""

instance_backup_id = ""

instance_charge_type = "PostPaid"

instance_class = ""

instance_name = ""

instance_release_protection = false

kms_encrypted_password = ""

kms_encryption_context = {}

maintain_end_time = "03:00Z"

maintain_start_time = "02:00Z"

password = ""

period = 1

private_ip = ""

profile = ""

region = ""

secondary_zone_id = ""

security_group_id = ""

security_ips = []

shared_credentials_file = ""

skip_region_validation = false

ssl_enable = ""

tags = {}

vpc_auth_mode = "Open"

vswitch_id = ""


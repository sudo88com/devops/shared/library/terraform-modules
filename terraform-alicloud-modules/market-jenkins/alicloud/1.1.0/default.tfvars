
allocate_public_ip = false

bandwidth = 10

bind_domain = false

create_instance = true

create_slb = false

data_disks = []

deletion_protection = false

domain_name = ""

ecs_instance_name = "TF-Jenkins"

ecs_instance_password = ""

ecs_instance_type = ""

host_record = ""

image_id = ""

internet_charge_type = "PayByTraffic"

internet_max_bandwidth_out = 10

private_ip = ""

product_keyword = "Jenkins"

product_suggested_price = 0

product_supplier_name_keyword = ""

profile = ""

region = ""

resource_group_id = ""

security_group_ids = []

shared_credentials_file = ""

skip_region_validation = false

slb_internet_charge_type = "PayByTraffic"

slb_name = "module-slb"

spec = ""

system_disk_category = "cloud_ssd"

system_disk_size = 40

tags = {}

type = ""

volume_tags = {}

vswitch_id = ""


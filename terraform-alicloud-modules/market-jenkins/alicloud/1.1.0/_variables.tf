
variable "allocate_public_ip" {
  type        = bool
  description = "Whether to allocate public ip for ECS instance. If 'create_slb' is true, it will be ignore."
  default     = false
}

variable "bandwidth" {
  type        = number
  description = "The load balancer instance bandwidth."
  default     = 10
}

variable "bind_domain" {
  type        = bool
  description = "Whether to bind domain."
  default     = false
}

variable "create_instance" {
  type        = bool
  description = "Whether to create ecs instance."
  default     = true
}

variable "create_slb" {
  type        = bool
  description = "Whether to create a slb instance and attach the Ecs instance. If true, a new Slb instance and listener will be created and the Ecs instance will be attached to the Slb."
  default     = false
}

variable "data_disks" {
  type        = list(map(string))
  description = "Additional data disks to attach to the scaled ECS instance."
  default     = []
}

variable "deletion_protection" {
  type        = bool
  description = "Whether enable the deletion protection or not. 'true': Enable deletion protection. 'false': Disable deletion protection."
  default     = false
}

variable "domain_name" {
  type        = string
  description = "The name of domain."
  default     = ""
}

variable "ecs_instance_name" {
  type        = string
  description = "The name of ECS Instance."
  default     = "TF-Jenkins"
}

variable "ecs_instance_password" {
  type        = string
  description = "The password of ECS instance."
  default     = ""
}

variable "ecs_instance_type" {
  type        = string
  description = "The instance type used to launch ecs instance."
  default     = ""
}

variable "host_record" {
  type        = string
  description = "Host record for the domain record."
  default     = ""
}

variable "image_id" {
  type        = string
  description = "The image id used to launch one ecs instance. If not set, a fetched market place image by product_keyword will be used."
  default     = ""
}

variable "internet_charge_type" {
  type        = string
  description = "The internet charge type of ECS instance. Choices are 'PayByTraffic' and 'PayByBandwidth'."
  default     = "PayByTraffic"
}

variable "internet_max_bandwidth_out" {
  type        = number
  description = "The maximum internet out bandwidth of ECS instance."
  default     = 10
}

variable "private_ip" {
  type        = string
  description = "Configure ECS Instance private IP address"
  default     = ""
}

variable "product_keyword" {
  type        = string
  description = "The name keyword of Market Product used to fetch the specified product image."
  default     = "Jenkins"
}

variable "product_suggested_price" {
  type        = number
  description = "The suggested price of Market Product used to fetch the specified product image."
  default     = 0
}

variable "product_supplier_name_keyword" {
  type        = string
  description = "The name keyword of Market Product supplier name used to fetch the specified product image."
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "resource_group_id" {
  type        = string
  description = "The Id of resource group which the ECS instance belongs."
  default     = ""
}

variable "security_group_ids" {
  type        = list(string)
  description = "A list of security group ids to associate with ECS Instance."
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "slb_internet_charge_type" {
  type        = string
  description = "The charge type of load balancer instance internet network."
  default     = "PayByTraffic"
}

variable "slb_name" {
  type        = string
  description = "The name of a new load balancer."
  default     = "module-slb"
}

variable "spec" {
  type        = string
  description = "The specification of the SLB instance."
  default     = ""
}

variable "system_disk_category" {
  type        = string
  description = "The system disk category used to launch one ecs instance."
  default     = "cloud_ssd"
}

variable "system_disk_size" {
  type        = number
  description = "The system disk size used to launch ecs instance."
  default     = 40
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the ECS and SLB."
  default     = {}
}

variable "type" {
  type        = string
  description = "The type of domain record."
  default     = ""
}

variable "volume_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the devices created by the instance at launch time."
  default     = {}
}

variable "vswitch_id" {
  type        = string
  description = "The virtual switch ID to launch ECS instance in VPC."
  default     = ""
}


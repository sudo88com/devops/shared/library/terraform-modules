
associate_public_ip_address = false

cpu_core_count = 0

data_disks = []

deletion_protection = false

dry_run = false

force_delete = false

host_name = ""

image_id = ""

image_ids = []

image_name_regex = "^ubuntu_18.*64"

instance_charge_type = "PostPaid"

instance_name = "TF-ECS-Instance"

instance_type = ""

instance_type_family = "ecs.ebmc6"

internet_charge_type = "PayByTraffic"

internet_max_bandwidth_out = 0

key_name = ""

kms_encrypted_password = ""

kms_encryption_context = {}

memory_size = 0

most_recent = true

number_of_instances = 1

owners = "system"

password = ""

private_ip = ""

private_ips = []

profile = ""

region = ""

resource_group_id = ""

role_name = ""

security_enhancement_strategy = "Active"

security_group_ids = []

shared_credentials_file = ""

skip_region_validation = false

spot_price_limit = 0

spot_strategy = "NoSpot"

subscription = {
  "auto_renew_period": 1,
  "include_data_disks": true,
  "period": 1,
  "period_unit": "Month",
  "renewal_status": "Normal"
}

system_disk_category = "cloud_efficiency"

system_disk_size = 40

tags = {
  "Created": "Terraform",
  "Source": "alibaba/ecs-instance/alicloud//modules/ecs-ecs-gpu-instance."
}

user_data = ""

volume_tags = {}

vswitch_id = ""

vswitch_ids = []


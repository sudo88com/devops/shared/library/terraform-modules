
variable "associate_public_ip_address" {
  type        = bool
  description = "Whether to associate a public ip address with an instance in a VPC."
  default     = false
}

variable "cpu_core_count" {
  type        = number
  description = "core count used to retrieve instance types."
  default     = 0
}

variable "data_disks" {
  type        = list(map(string))
  description = "Additional data disks to attach to the scaled ECS instance."
  default     = []
}

variable "deletion_protection" {
  type        = bool
  description = "Whether enable the deletion protection or not. 'true': Enable deletion protection. 'false': Disable deletion protection."
  default     = false
}

variable "dry_run" {
  type        = bool
  description = "Whether to pre-detection. When it is true, only pre-detection and not actually modify the payment type operation. Default to false."
  default     = false
}

variable "force_delete" {
  type        = bool
  description = "If it is true, the 'PrePaid' instance will be change to 'PostPaid' and then deleted forcibly. However, because of changing instance charge type has CPU core count quota limitation, so strongly recommand that 'Don't modify instance charge type frequentlly in one month'."
  default     = false
}

variable "host_name" {
  type        = string
  description = "Host name used on all instances as prefix. Like TF-ECS-Host-Name-1, TF-ECS-Host-Name-2."
  default     = ""
}

variable "image_id" {
  type        = string
  description = "The image id used to launch one or more ecs instances."
  default     = ""
}

variable "image_ids" {
  type        = list(string)
  description = "A list of ecs image IDs to launch one or more ecs instances."
  default     = []
}

variable "image_name_regex" {
  type        = string
  description = "A regex string to filter resulting images by name."
  default     = "^ubuntu_18.*64"
}

variable "instance_charge_type" {
  type        = string
  description = "The charge type of instance. Choices are 'PostPaid' and 'PrePaid'."
  default     = "PostPaid"
}

variable "instance_name" {
  type        = string
  description = "Name used on all instances as prefix. Like TF-ECS-Instance-1, TF-ECS-Instance-2."
  default     = "TF-ECS-Instance"
}

variable "instance_type" {
  type        = string
  description = "The instance type used to launch one or more ecs instances."
  default     = ""
}

variable "instance_type_family" {
  type        = string
  description = "The instance type family used to retrieve bare metal CPU instance type."
  default     = "ecs.ebmc6"
}

variable "internet_charge_type" {
  type        = string
  description = "The internet charge type of instance. Choices are 'PayByTraffic' and 'PayByBandwidth'."
  default     = "PayByTraffic"
}

variable "internet_max_bandwidth_out" {
  type        = number
  description = "The maximum internet out bandwidth of instance."
  default     = 0
}

variable "key_name" {
  type        = string
  description = "The name of SSH key pair that can login ECS instance successfully without password. If it is specified, the password would be invalid."
  default     = ""
}

variable "kms_encrypted_password" {
  type        = string
  description = "An KMS encrypts password used to an instance. It is conflicted with 'password'."
  default     = ""
}

variable "kms_encryption_context" {
  type        = map(string)
  description = "An KMS encryption context used to decrypt 'kms_encrypted_password' before creating or updating an instance with 'kms_encrypted_password'."
  default     = {}
}

variable "memory_size" {
  type        = number
  description = "Memory size used to retrieve instance types."
  default     = 0
}

variable "most_recent" {
  type        = bool
  description = "If more than one result are returned, select the most recent one."
  default     = true
}

variable "number_of_instances" {
  type        = number
  description = "The number of instances to be created."
  default     = 1
}

variable "owners" {
  type        = string
  description = "Filter results by a specific image owner. Valid items are 'system', 'self', 'others', 'marketplace'."
  default     = "system"
}

variable "password" {
  type        = string
  description = "The password of instance."
  default     = ""
}

variable "private_ip" {
  type        = string
  description = "Configure Instance private IP address."
  default     = ""
}

variable "private_ips" {
  type        = list(string)
  description = "A list to configure Instance private IP address."
  default     = []
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.2.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "resource_group_id" {
  type        = string
  description = "The Id of resource group which the instance belongs."
  default     = ""
}

variable "role_name" {
  type        = string
  description = "Instance RAM role name. The name is provided and maintained by RAM. You can use 'alicloud_ram_role' to create a new one."
  default     = ""
}

variable "security_enhancement_strategy" {
  type        = string
  description = "The security enhancement strategy."
  default     = "Active"
}

variable "security_group_ids" {
  type        = list(string)
  description = "A list of security group ids to associate with."
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.2.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.2.0) Skip staticvalidation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "spot_price_limit" {
  type        = number
  description = "The hourly price threshold of a instance, and it takes effect only when parameter 'spot_strategy' is 'SpotWithPriceLimit'. Three decimals is allowed at most."
  default     = 0
}

variable "spot_strategy" {
  type        = string
  description = "The spot strategy of a Pay-As-You-Go instance, and it takes effect only when parameter 'instance_charge_type' is 'PostPaid'. Value range: 'NoSpot': A regular Pay-As-You-Go instance. 'SpotWithPriceLimit': A price threshold for a spot instance. 'SpotAsPriceGo': A price that is based on the highest Pay-As-You-Go instance."
  default     = "NoSpot"
}

variable "subscription" {
  type        = map(any)
  description = "A mapping of fields for Prepaid ECS instances created."
  default     = {
  "auto_renew_period": 1,
  "include_data_disks": true,
  "period": 1,
  "period_unit": "Month",
  "renewal_status": "Normal"
}
}

variable "system_disk_category" {
  type        = string
  description = "The system disk category used to launch one or more ecs instances."
  default     = "cloud_efficiency"
}

variable "system_disk_size" {
  type        = number
  description = "The system disk size used to launch one or more ecs instances."
  default     = 40
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the resource."
  default     = {
  "Created": "Terraform",
  "Source": "alibaba/ecs-instance/alicloud//modules/ecs-ecs-gpu-instance."
}
}

variable "user_data" {
  type        = string
  description = "User data to pass to instance on boot."
  default     = ""
}

variable "volume_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the devices created by the instance at launch time."
  default     = {}
}

variable "vswitch_id" {
  type        = string
  description = "The virtual switch ID to launch in VPC."
  default     = ""
}

variable "vswitch_ids" {
  type        = list(string)
  description = "A list of virtual switch IDs to launch in."
  default     = []
}


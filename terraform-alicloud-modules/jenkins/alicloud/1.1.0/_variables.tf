
variable "data_disks" {
  type        = list(map(string))
  description = "Additional data disks to attach to the scaled instance."
  default     = []
}

variable "deletion_protection" {
  type        = bool
  description = "Whether enable the deletion protection or not. 'true': Enable deletion protection. 'false': Disable deletion protection."
  default     = false
}

variable "image_id" {
  type        = string
  description = "The image id used to launch one instance. It only support CentOS_7."
  default     = ""
}

variable "image_name_regex" {
  type        = string
  description = "The ECS image's name regex used to fetch specified image."
  default     = "^ubuntu_18"
}

variable "instance_charge_type" {
  type        = string
  description = "The charge type of instance. Choices are 'PostPaid' and 'PrePaid'."
  default     = "PostPaid"
}

variable "instance_name" {
  type        = string
  description = "The name of instance."
  default     = "TF-Jenkins"
}

variable "instance_password" {
  type        = string
  description = "The password of instance."
  default     = ""
}

variable "instance_type" {
  type        = string
  description = "The instance type used to launch instance."
  default     = ""
}

variable "internet_charge_type" {
  type        = string
  description = "The internet charge type of instance. Choices are 'PayByTraffic' and 'PayByBandwidth'."
  default     = "PayByTraffic"
}

variable "internet_max_bandwidth_out" {
  type        = number
  description = "The maximum internet out bandwidth of instance."
  default     = 10
}

variable "private_ip" {
  type        = string
  description = "Configure instance private IP address."
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "resource_group_id" {
  type        = string
  description = "The Id of resource group which the instance belongs."
  default     = ""
}

variable "security_group_ids" {
  type        = list(string)
  description = "A list of security group ids to associate with ECS and RDS Mysql Instance."
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "system_disk_category" {
  type        = string
  description = "The system disk category used to launch one instance."
  default     = ""
}

variable "system_disk_size" {
  type        = number
  description = "The system disk size used to launch instance.Default to '40'."
  default     = 40
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the ECS and RDS Instance."
  default     = {}
}

variable "volume_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the devices created by the instance at launch time."
  default     = {}
}

variable "vswitch_id" {
  type        = string
  description = "The virtual switch ID to launch ECS and RDS MySql instance in VPC."
  default     = ""
}


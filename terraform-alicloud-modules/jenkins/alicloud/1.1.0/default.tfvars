
data_disks = []

deletion_protection = false

image_id = ""

image_name_regex = "^ubuntu_18"

instance_charge_type = "PostPaid"

instance_name = "TF-Jenkins"

instance_password = ""

instance_type = ""

internet_charge_type = "PayByTraffic"

internet_max_bandwidth_out = 10

private_ip = ""

profile = ""

region = ""

resource_group_id = ""

security_group_ids = []

shared_credentials_file = ""

skip_region_validation = false

system_disk_category = ""

system_disk_size = 40

tags = {}

volume_tags = {}

vswitch_id = ""



variable "availability_zone" {
  type        = string
  description = "The available zone to launch modules."
  default     = ""
}

variable "category" {
  type        = string
  description = "The specification of the category."
  default     = "Cluster"
}

variable "class" {
  type        = string
  description = "The specification of the class."
  default     = "C8"
}

variable "cluster_version" {
  type        = string
  description = "The specification of the cluster version."
  default     = "3.0"
}

variable "description" {
  type        = string
  description = "The name of a new description."
  default     = "Test new adb."
}

variable "maintain_time" {
  type        = string
  description = "The specification of the maintain time."
  default     = "23:00Z-00:00Z"
}

variable "mode" {
  type        = string
  description = "The specification of the mode."
  default     = "reserver"
}

variable "node_count" {
  type        = string
  description = "The specification of the node count."
  default     = "4"
}

variable "node_storage" {
  type        = string
  description = "The specification of the node storage."
  default     = "400"
}

variable "payment_type" {
  type        = string
  description = "The specification of the payment type."
  default     = "PayAsYouGo"
}

variable "security_ips" {
  type        = list(string)
  description = "The specification of the security_ips."
  default     = [
  "10.168.1.12",
  "10.168.1.11"
]
}

variable "tags_created" {
  type        = string
  description = "The specification of the tags created."
  default     = "TF-update"
}

variable "tags_for" {
  type        = string
  description = "The specification of the tags for."
  default     = "acceptance-test-update"
}

variable "vswitch_id" {
  type        = string
  description = "VSwitch variables, if vswitch_id is empty, then the net_type = classic."
  default     = ""
}


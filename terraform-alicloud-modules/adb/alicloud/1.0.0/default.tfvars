
availability_zone = ""

category = "Cluster"

class = "C8"

cluster_version = "3.0"

description = "Test new adb."

maintain_time = "23:00Z-00:00Z"

mode = "reserver"

node_count = "4"

node_storage = "400"

payment_type = "PayAsYouGo"

security_ips = [
  "10.168.1.12",
  "10.168.1.11"
]

tags_created = "TF-update"

tags_for = "acceptance-test-update"

vswitch_id = ""


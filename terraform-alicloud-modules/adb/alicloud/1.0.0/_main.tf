
module "adb" {
  source = "terraform-aws-modules/adb/aws"
  version = "1.0.0"
  availability_zone = var.availability_zone
  category = var.category
  class = var.class
  cluster_version = var.cluster_version
  description = var.description
  maintain_time = var.maintain_time
  mode = var.mode
  node_count = var.node_count
  node_storage = var.node_storage
  payment_type = var.payment_type
  security_ips = var.security_ips
  tags_created = var.tags_created
  tags_for = var.tags_for
  vswitch_id = var.vswitch_id
}


api_auth_type = "APP"

api_http_service_config = [
  {
    "address": "http://apigateway-backend.alicloudapi.com:8080",
    "aone_name": "cloudapi-openapi",
    "method": "GET",
    "path": "/web/cloudapi",
    "timeout": 12
  }
]

api_name = ""

api_request_config = [
  {
    "method": "GET",
    "mode": "MAPPING",
    "path": "/test/path1",
    "protocol": "HTTP"
  }
]

api_request_parameters = [
  {
    "default_value": "testDefaultValue",
    "in": "QUERY",
    "in_service": "QUERY",
    "name": "test",
    "name_service": "testparams",
    "required": "REQUIRED",
    "type": "STRING"
  }
]

api_service_type = "HTTP"

api_stage_name = [
  "RELEASE",
  "PRE",
  "TEST"
]

app_name = ""

create_api_gateway = true

create_api_gateway_group = true

fc_file_name = "index.py"

fc_runtime = "python2.7"

fc_service_name = "terraform-fc-service-for-serverless-webapp"

group_id = ""

group_name = ""

profile = ""

region = ""

shared_credentials_file = ""

skip_region_validation = false


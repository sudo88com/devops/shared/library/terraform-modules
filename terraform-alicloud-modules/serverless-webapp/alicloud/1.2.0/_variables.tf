
variable "api_auth_type" {
  type        = string
  description = "The authorization Type including APP and ANONYMOUS."
  default     = "APP"
}

variable "api_http_service_config" {
  type        = list(object({
    address   = string
    method    = string
    path      = string
    timeout   = number
    aone_name = string
  }))
  description = "http_service_config defines the config when service_type selected 'HTTP'."
  default     = [
  {
    "address": "http://apigateway-backend.alicloudapi.com:8080",
    "aone_name": "cloudapi-openapi",
    "method": "GET",
    "path": "/web/cloudapi",
    "timeout": 12
  }
]
}

variable "api_name" {
  type        = string
  description = "The name of the api gateway api"
  default     = ""
}

variable "api_request_config" {
  type        = list(object({
    protocol = string
    method   = string
    path     = string
    mode     = string
  }))
  description = "Request_config defines how users can send requests to your API."
  default     = [
  {
    "method": "GET",
    "mode": "MAPPING",
    "path": "/test/path1",
    "protocol": "HTTP"
  }
]
}

variable "api_request_parameters" {
  type        = list(object({
    name          = string
    type          = string
    required      = string
    in            = string
    in_service    = string
    name_service  = string
    default_value = string
  }))
  description = "equest_parameters defines the request parameters of the api."
  default     = [
  {
    "default_value": "testDefaultValue",
    "in": "QUERY",
    "in_service": "QUERY",
    "name": "test",
    "name_service": "testparams",
    "required": "REQUIRED",
    "type": "STRING"
  }
]
}

variable "api_service_type" {
  type        = string
  description = "The type of backend service. Type including HTTP,VPC and MOCK."
  default     = "HTTP"
}

variable "api_stage_name" {
  type        = list(string)
  description = "Stages that the api need to be deployed. Valid value: RELEASE | PRE | TEST."
  default     = [
  "RELEASE",
  "PRE",
  "TEST"
]
}

variable "app_name" {
  type        = string
  description = "The name of the api gateway app"
  default     = ""
}

variable "create_api_gateway" {
  type        = string
  description = "Whether to create api gateway resources"
  default     = true
}

variable "create_api_gateway_group" {
  type        = string
  description = "Whether to create an new api gateway group with Terraform"
  default     = true
}

variable "fc_file_name" {
  type        = string
  description = "The file name of fc function"
  default     = "index.py"
}

variable "fc_runtime" {
  type        = string
  description = "The runtime of fc function, its supports nodejs6, nodejs8, nodejs10, python2.7, python3, java8, php7.2 and dotnatcore2.1"
  default     = "python2.7"
}

variable "fc_service_name" {
  type        = string
  description = "The name of fc service"
  default     = "terraform-fc-service-for-serverless-webapp"
}

variable "group_id" {
  type        = string
  description = "The id of the api gateway group, if you set 'create_api_gateway_group' false, this field must be specified"
  default     = ""
}

variable "group_name" {
  type        = string
  description = "The name of the api gateway group"
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.2.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.2.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = string
  description = "(Deprecated from version 1.2.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}


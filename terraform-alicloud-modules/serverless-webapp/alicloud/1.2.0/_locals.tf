
locals {
  api_auth_type = var.api_auth_type
  api_http_service_config = var.api_http_service_config
  api_name = var.api_name
  api_request_config = var.api_request_config
  api_request_parameters = var.api_request_parameters
  api_service_type = var.api_service_type
  api_stage_name = var.api_stage_name
  app_name = var.app_name
  create_api_gateway = var.create_api_gateway
  create_api_gateway_group = var.create_api_gateway_group
  fc_file_name = var.fc_file_name
  fc_runtime = var.fc_runtime
  fc_service_name = var.fc_service_name
  group_id = var.group_id
  group_name = var.group_name
  profile = var.profile
  region = var.region
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
}

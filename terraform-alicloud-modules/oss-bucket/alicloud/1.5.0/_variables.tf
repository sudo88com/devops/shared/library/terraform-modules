
variable "acl" {
  type        = string
  description = ""
  default     = "private"
}

variable "bucket_name" {
  type        = string
  description = ""
  default     = ""
}

variable "lifecycle_rule" {
  type        = list(object({
    id         = string
    prefix     = string
    enabled    = bool
    expiration = list(map(string))
  }))
  description = ""
  default     = []
}

variable "logging" {
  type        = list(object({
    target_bucket = string
    target_prefix = string
  }))
  description = ""
  default     = []
}

variable "logging_isenable" {
  type        = bool
  description = ""
  default     = false
}

variable "policy" {
  type        = string
  description = ""
  default     = ""
}

variable "referer_config" {
  type        = list(object({
    allow_empty = bool
    referers    = list(string)
  }))
  description = ""
  default     = []
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.5.0) The region used to launch this module resources."
  default     = ""
}

variable "website" {
  type        = list(object({
    index_document = string
    error_document = string
  }))
  description = ""
  default     = []
}



module "oss-bucket" {
  source = "terraform-aws-modules/oss-bucket/aws"
  version = "1.5.0"
  acl = var.acl
  bucket_name = var.bucket_name
  lifecycle_rule = var.lifecycle_rule
  logging = var.logging
  logging_isenable = var.logging_isenable
  policy = var.policy
  referer_config = var.referer_config
  region = var.region
  website = var.website
}

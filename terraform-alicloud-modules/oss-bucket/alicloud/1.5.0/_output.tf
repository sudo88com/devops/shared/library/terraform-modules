
output "this_oss_bucket_id" {
  description = "The name of the bucket"
  value       = module.oss-bucket.this_oss_bucket_id
}


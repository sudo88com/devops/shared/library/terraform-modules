
attach_disk = false

availability_zone = ""

category = "cloud_efficiency"

disk_count = 1

encrypted = false

instance_id = ""

name = "TF_ECS_Disk"

region = ""

size = "40"

tags = {
  "created_by": "Terraform",
  "created_from": "module-terraform-alicloud-disk"
}


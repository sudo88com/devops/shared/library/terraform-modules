
output "this_availability_zone" {
  description = "The zone ID of the ECS disks belongs to"
  value       = module.disk.this_availability_zone
}

output "this_category" {
  description = ""
  value       = module.disk.this_category
}

output "this_disk_count" {
  description = ""
  value       = module.disk.this_disk_count
}

output "this_disk_ids" {
  description = "List of disks ID"
  value       = module.disk.this_disk_ids
}

output "this_name" {
  description = ""
  value       = module.disk.this_name
}

output "this_size" {
  description = ""
  value       = module.disk.this_size
}

output "this_tags" {
  description = ""
  value       = module.disk.this_tags
}


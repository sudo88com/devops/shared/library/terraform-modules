
module "disk" {
  source = "terraform-aws-modules/disk/aws"
  version = "1.5.0"
  attach_disk = var.attach_disk
  availability_zone = var.availability_zone
  category = var.category
  disk_count = var.disk_count
  encrypted = var.encrypted
  instance_id = var.instance_id
  name = var.name
  region = var.region
  size = var.size
  tags = var.tags
}


variable "attach_disk" {
  type        = string
  description = "Whether to attach disks to one instance"
  default     = false
}

variable "availability_zone" {
  type        = string
  description = "The available zone to launch ecs disks. Default from data source `alicloud_zones`."
  default     = ""
}

variable "category" {
  type        = string
  description = "The data disk category used to launch one or more data disks."
  default     = "cloud_efficiency"
}

variable "disk_count" {
  type        = string
  description = "Number of disks to launch."
  default     = 1
}

variable "encrypted" {
  type        = string
  description = "Whether to encrypt the disks"
  default     = false
}

variable "instance_id" {
  type        = string
  description = "The ID of ECS instance."
  default     = ""
}

variable "name" {
  type        = string
  description = "Name used on all disks as prefix. Like TF_ECS_Disk-1, TF_ECS_Disk-2."
  default     = "TF_ECS_Disk"
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.5.0) The region used to launch this module resources."
  default     = ""
}

variable "size" {
  type        = string
  description = "The data disk size used to launch one or more data disks."
  default     = "40"
}

variable "tags" {
  type        = map(string)
  description = "Used to mark specified ecs data disks."
  default     = {
  "created_by": "Terraform",
  "created_from": "module-terraform-alicloud-disk"
}
}


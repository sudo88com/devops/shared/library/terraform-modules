
variable "account_password" {
  type        = string
  description = "Password of the root account. It is a string of 6 to 32 characters and is composed of letters, numbers, and underlines"
  default     = "test123456_"
}

variable "backup_period" {
  type        = list(string)
  description = "MongoDB Instance backup period. It is required when backup_time was existed. Valid values: [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday]. Default to [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday]. "
  default     = [
  "Monday"
]
}

variable "backup_time" {
  type        = string
  description = "MongoDB instance backup time. It is required when backup_period was existed. In the format of HH:mmZ- HH:mmZ. Time setting interval is one hour. Default to a random time, like '23:00Z-24:00Z'. "
  default     = "01:00Z-02:00Z"
}

variable "create_resources_size" {
  type        = string
  description = "The specification of the monitoring region."
  default     = "1"
}

variable "db_instance_class" {
  type        = string
  description = "The specification of the instance. For more information about the value, see https://www.alibabacloud.com/help/doc-detail/57141.htm"
  default     = "dds.mongo.mid"
}

variable "db_instance_storage" {
  type        = number
  description = "The storage space of the instance. Valid values: 10 to 3000. Unit: GB. You can only specify this value in 10 GB increments. "
  default     = 10
}

variable "engine_version" {
  type        = string
  description = "The version number of the database. Valid value: 3.2, 3.4, 4.0. "
  default     = "4.0"
}

variable "instance_charge_type" {
  type        = string
  description = "The billing method of the instance. Valid values are Prepaid, PostPaid, Default to PostPaid"
  default     = "PostPaid"
}

variable "instance_id" {
  type        = string
  description = "`(Deprecated)` It has been deprecated from version 1.2.0 and use `existing_instance_id` instead. "
  default     = ""
}

variable "name" {
  type        = string
  description = " The name of DB instance. It a string of 2 to 256 characters"
  default     = "testname"
}

variable "period" {
  type        = string
  description = "The duration that you will buy DB instance (in month). It is valid when instance_charge_type is PrePaid. Valid values: [1~9], 12, 24, 36. Default to 1"
  default     = 1
}

variable "region" {
  type        = string
  description = "The specification of the monitoring region."
  default     = ""
}

variable "replication_factor" {
  type        = number
  description = "The number of nodes in the replica set instance. Valid values: 3, 5, 7. Default value: 3. "
  default     = 3
}

variable "security_ip_list" {
  type        = list(string)
  description = " List of IP addresses allowed to access all databases of an instance. The list contains up to 1,000 IP addresses, separated by commas. Supported formats include 0.0.0.0/0, 10.23.12.24 (IP), and 10.23.12.24/24 (Classless Inter-Domain Routing (CIDR) mode. /24 represents the length of the prefix in an IP address. The range of the prefix length is [1,32]). "
  default     = []
}

variable "storage_engine" {
  type        = string
  description = "The MongoDB storage engine, WiredTiger or RocksDB. Default value: WiredTiger. "
  default     = "WiredTiger"
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the mongodb instance resource. "
  default     = {}
}

variable "vswitch_id" {
  type        = string
  description = "The virtual switch ID to launch DB instances in one VPC. "
  default     = ""
}

variable "zone_id" {
  type        = string
  description = "The ID of the zone. You can refer to https://www.alibabacloud.com/help/doc-detail/61933.htm. "
  default     = ""
}


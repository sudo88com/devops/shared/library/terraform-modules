
module "mongodb-multi" {
  source = "terraform-aws-modules/mongodb-multi/aws"
  version = "1.0.0"
  account_password = var.account_password
  backup_period = var.backup_period
  backup_time = var.backup_time
  create_resources_size = var.create_resources_size
  db_instance_class = var.db_instance_class
  db_instance_storage = var.db_instance_storage
  engine_version = var.engine_version
  instance_charge_type = var.instance_charge_type
  instance_id = var.instance_id
  name = var.name
  period = var.period
  region = var.region
  replication_factor = var.replication_factor
  security_ip_list = var.security_ip_list
  storage_engine = var.storage_engine
  tags = var.tags
  vswitch_id = var.vswitch_id
  zone_id = var.zone_id
}


account_password = "test123456_"

backup_period = [
  "Monday"
]

backup_time = "01:00Z-02:00Z"

create_resources_size = "1"

db_instance_class = "dds.mongo.mid"

db_instance_storage = 10

engine_version = "4.0"

instance_charge_type = "PostPaid"

instance_id = ""

name = "testname"

period = 1

region = ""

replication_factor = 3

security_ip_list = []

storage_engine = "WiredTiger"

tags = {}

vswitch_id = ""

zone_id = ""


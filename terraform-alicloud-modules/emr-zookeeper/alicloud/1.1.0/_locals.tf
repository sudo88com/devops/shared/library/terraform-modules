
locals {
  charge_type = var.charge_type
  create = var.create
  disk_capacity = var.disk_capacity
  disk_type = var.disk_type
  emr_cluster_name = var.emr_cluster_name
  emr_version = var.emr_version
  host_groups = var.host_groups
  instance_type = var.instance_type
  profile = var.profile
  ram_role_name = var.ram_role_name
  region = var.region
  security_group_id = var.security_group_id
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  support_local_storage = var.support_local_storage
  system_disk_capacity = var.system_disk_capacity
  system_disk_type = var.system_disk_type
  vswitch_id = var.vswitch_id
  zone_id = var.zone_id
}


charge_type = "PostPaid"

create = false

disk_capacity = 0

disk_type = ""

emr_cluster_name = "terraform-test-zookeeper-module"

emr_version = "EMR-3.24.0"

host_groups = [
  {
    "disk_count": "1",
    "host_group_name": "core_group",
    "host_group_type": "CORE",
    "node_count": "3"
  }
]

instance_type = ""

profile = ""

ram_role_name = ""

region = ""

security_group_id = ""

shared_credentials_file = ""

skip_region_validation = false

support_local_storage = false

system_disk_capacity = 0

system_disk_type = ""

vswitch_id = ""

zone_id = ""


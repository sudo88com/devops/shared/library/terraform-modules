
account_name = "miniapp"

account_privilege = "ReadWrite"

account_type = "Super"

address_type = "intranet"

ak_status = "Active"

availability_zones = {
  "az0": "cn-shanghai-e",
  "az1": "cn-shanghai-f",
  "az2": "cn-shanghai-g"
}

bandwidth = "2"

bucket_acls = {
  "buc0": "private"
}

bucket_names = {
  "buc0": "apptest-xy1234"
}

bucket_storage_classes = {
  "buc0": "Standard"
}

character_set = "utf8"

cidr_blocks = {
  "az0": "10.99.0.0/21",
  "az1": "10.99.8.0/21",
  "az2": "10.99.16.0/21"
}

count_format = "%02d"

db_description = ""

delete_protection = "off"

deletion_protection = false

deletion_window_in_days = "7"

description = "KMS for OSS"

disk_category = "cloud_efficiency"

disk_size = "0"

display_name = "test01"

ecs_count = 2

ecs_count_format = "%02d"

ecs_instance_charge_type = "PostPaid"

ecs_internet_charge_type = "PayByTraffic"

ecs_name = "test"

ecs_type = "ecs.c5.large"

eip_instance_charge_type = "PostPaid"

eip_internet_charge_type = "PayByTraffic"

engine = "MySQL"

engine_version = "5.7"

force = true

group_comments = "app开发用户组"

group_name = "app_dev_xy"

image_name = "^centos_7_06_64"

image_owners = "system"

instance_charge_type = "Postpaid"

instance_storage = "100"

instance_type = "rds.mysql.s3.large"

internet_charge_type = "PayByTraffic"

internet_max_bandwidth_out = 0

is_enabled = true

isp = "BGP"

key_name = "xianwang_key_pair_1121"

logging_target_prefix = "log/"

master_zone_id = "cn-shanghai-f"

mfa_bind_required = false

nic_type = "intranet"

object_key = {
  "key1": ""
}

object_source = {
  "source1": ""
}

password = "Test1234!"

password_reset_required = true

policy_name = {
  "policy_name1": "AliyunOSSFullAccess",
  "policy_name2": "AliyunECSFullAccess"
}

policy_type = {
  "policy_type1": "System",
  "policy_type2": "System"
}

profile = "default"

rds_account_name = "myuser"

rds_account_pwd = "Test1234"

rds_count = 2

rds_name = "rds"

rds_zone_id = "cn-shanghai-MAZ5(f,g)"

region = ""

secret_file = ""

security_group_name = "ali-sg-ec-sz"

slave_zone_id = "cn-shanghai-g"

slb_name = "auto_named_slb"

specification = "slb.s2.small"

sse_algorithm = "AES256"

system_disk_size = "40"

tags = {
  "app": "客户端",
  "name": "arthur",
  "owner": "bestpractice",
  "team": "rds"
}

use_ecs_module = true

use_eip_module = true

use_kms_module = true

use_oss_module = true

use_ram_module = true

use_rds_module = true

use_slb_module = true

use_vpc_module = true

user_name = "test1121"

vpc_cidr = "10.99.0.0/19"

vpc_name = "webserver"

which_bucket_for_uploading = 1



variable "account_name" {
  type        = string
  description = ""
  default     = "miniapp"
}

variable "account_privilege" {
  type        = string
  description = ""
  default     = "ReadWrite"
}

variable "account_type" {
  type        = string
  description = ""
  default     = "Super"
}

variable "address_type" {
  type        = string
  description = ""
  default     = "intranet"
}

variable "ak_status" {
  type        = string
  description = ""
  default     = "Active"
}

variable "availability_zones" {
  type        = map
  description = ""
  default     = {
  "az0": "cn-shanghai-e",
  "az1": "cn-shanghai-f",
  "az2": "cn-shanghai-g"
}
}

variable "bandwidth" {
  type        = string
  description = ""
  default     = "2"
}

variable "bucket_acls" {
  type        = map
  description = ""
  default     = {
  "buc0": "private"
}
}

variable "bucket_names" {
  type        = map
  description = ""
  default     = {
  "buc0": "apptest-xy1234"
}
}

variable "bucket_storage_classes" {
  type        = map
  description = ""
  default     = {
  "buc0": "Standard"
}
}

variable "character_set" {
  type        = string
  description = ""
  default     = "utf8"
}

variable "cidr_blocks" {
  type        = map
  description = ""
  default     = {
  "az0": "10.99.0.0/21",
  "az1": "10.99.8.0/21",
  "az2": "10.99.16.0/21"
}
}

variable "count_format" {
  type        = string
  description = ""
  default     = "%02d"
}

variable "db_description" {
  type        = string
  description = ""
  default     = ""
}

variable "delete_protection" {
  type        = string
  description = ""
  default     = "off"
}

variable "deletion_protection" {
  type        = string
  description = ""
  default     = false
}

variable "deletion_window_in_days" {
  type        = string
  description = ""
  default     = "7"
}

variable "description" {
  type        = string
  description = ""
  default     = "KMS for OSS"
}

variable "disk_category" {
  type        = string
  description = ""
  default     = "cloud_efficiency"
}

variable "disk_size" {
  type        = string
  description = ""
  default     = "0"
}

variable "display_name" {
  type        = string
  description = ""
  default     = "test01"
}

variable "ecs_count" {
  type        = string
  description = ""
  default     = 2
}

variable "ecs_count_format" {
  type        = string
  description = ""
  default     = "%02d"
}

variable "ecs_instance_charge_type" {
  type        = string
  description = ""
  default     = "PostPaid"
}

variable "ecs_internet_charge_type" {
  type        = string
  description = ""
  default     = "PayByTraffic"
}

variable "ecs_name" {
  type        = string
  description = ""
  default     = "test"
}

variable "ecs_type" {
  type        = string
  description = ""
  default     = "ecs.c5.large"
}

variable "eip_instance_charge_type" {
  type        = string
  description = ""
  default     = "PostPaid"
}

variable "eip_internet_charge_type" {
  type        = string
  description = ""
  default     = "PayByTraffic"
}

variable "engine" {
  type        = string
  description = ""
  default     = "MySQL"
}

variable "engine_version" {
  type        = string
  description = ""
  default     = "5.7"
}

variable "force" {
  type        = string
  description = ""
  default     = true
}

variable "group_comments" {
  type        = string
  description = ""
  default     = "app开发用户组"
}

variable "group_name" {
  type        = string
  description = ""
  default     = "app_dev_xy"
}

variable "image_name" {
  type        = string
  description = ""
  default     = "^centos_7_06_64"
}

variable "image_owners" {
  type        = string
  description = ""
  default     = "system"
}

variable "instance_charge_type" {
  type        = string
  description = ""
  default     = "Postpaid"
}

variable "instance_storage" {
  type        = string
  description = ""
  default     = "100"
}

variable "instance_type" {
  type        = string
  description = ""
  default     = "rds.mysql.s3.large"
}

variable "internet_charge_type" {
  type        = string
  description = ""
  default     = "PayByTraffic"
}

variable "internet_max_bandwidth_out" {
  type        = string
  description = ""
  default     = 0
}

variable "is_enabled" {
  type        = string
  description = ""
  default     = true
}

variable "isp" {
  type        = string
  description = ""
  default     = "BGP"
}

variable "key_name" {
  type        = string
  description = ""
  default     = "xianwang_key_pair_1121"
}

variable "logging_target_prefix" {
  type        = string
  description = ""
  default     = "log/"
}

variable "master_zone_id" {
  type        = string
  description = ""
  default     = "cn-shanghai-f"
}

variable "mfa_bind_required" {
  type        = string
  description = ""
  default     = false
}

variable "nic_type" {
  type        = string
  description = ""
  default     = "intranet"
}

variable "object_key" {
  type        = map
  description = ""
  default     = {
  "key1": ""
}
}

variable "object_source" {
  type        = map
  description = ""
  default     = {
  "source1": ""
}
}

variable "password" {
  type        = string
  description = ""
  default     = "Test1234!"
}

variable "password_reset_required" {
  type        = string
  description = ""
  default     = true
}

variable "policy_name" {
  type        = map
  description = ""
  default     = {
  "policy_name1": "AliyunOSSFullAccess",
  "policy_name2": "AliyunECSFullAccess"
}
}

variable "policy_type" {
  type        = map
  description = ""
  default     = {
  "policy_type1": "System",
  "policy_type2": "System"
}
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = "default"
}

variable "rds_account_name" {
  type        = string
  description = ""
  default     = "myuser"
}

variable "rds_account_pwd" {
  type        = string
  description = ""
  default     = "Test1234"
}

variable "rds_count" {
  type        = string
  description = ""
  default     = 2
}

variable "rds_name" {
  type        = string
  description = ""
  default     = "rds"
}

variable "rds_zone_id" {
  type        = string
  description = ""
  default     = "cn-shanghai-MAZ5(f,g)"
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "secret_file" {
  type        = string
  description = ""
  default     = ""
}

variable "security_group_name" {
  type        = string
  description = ""
  default     = "ali-sg-ec-sz"
}

variable "slave_zone_id" {
  type        = string
  description = ""
  default     = "cn-shanghai-g"
}

variable "slb_name" {
  type        = string
  description = ""
  default     = "auto_named_slb"
}

variable "specification" {
  type        = string
  description = ""
  default     = "slb.s2.small"
}

variable "sse_algorithm" {
  type        = string
  description = ""
  default     = "AES256"
}

variable "system_disk_size" {
  type        = string
  description = ""
  default     = "40"
}

variable "tags" {
  type        = map
  description = ""
  default     = {
  "app": "客户端",
  "name": "arthur",
  "owner": "bestpractice",
  "team": "rds"
}
}

variable "use_ecs_module" {
  type        = string
  description = ""
  default     = true
}

variable "use_eip_module" {
  type        = string
  description = ""
  default     = true
}

variable "use_kms_module" {
  type        = string
  description = ""
  default     = true
}

variable "use_oss_module" {
  type        = string
  description = ""
  default     = true
}

variable "use_ram_module" {
  type        = string
  description = ""
  default     = true
}

variable "use_rds_module" {
  type        = string
  description = ""
  default     = true
}

variable "use_slb_module" {
  type        = string
  description = ""
  default     = true
}

variable "use_vpc_module" {
  type        = string
  description = ""
  default     = true
}

variable "user_name" {
  type        = string
  description = ""
  default     = "test1121"
}

variable "vpc_cidr" {
  type        = string
  description = ""
  default     = "10.99.0.0/19"
}

variable "vpc_name" {
  type        = string
  description = ""
  default     = "webserver"
}

variable "which_bucket_for_uploading" {
  type        = string
  description = "1 means the first bucket,such as bucket name buc0"
  default     = 1
}


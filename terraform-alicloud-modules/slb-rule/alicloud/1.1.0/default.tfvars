
address_type = "internet"

availability_zone = ""

available_disk_category = "cloud_efficiency"

available_resource_creation = "VSwitch"

backend_port = 22

bandwidth = 5

cidr_block = "172.16.0.0/16"

cookie = "cookie_test"

cookie_timeout = 100

cpu_core_count = 1

domain = "*.aliyun.com"

frontend_port = 22

health_check = "on"

health_check_connect_port = "20"

health_check_domain = "test"

health_check_http_code = "http_2xx"

health_check_interval = 2

health_check_timeout = 30

health_check_uri = "/test"

healthy_threshold = 3

images_most_recent = true

images_name_regex = "^ubuntu_18.*64"

images_owners = "system"

internal = false

listener_sync = "off"

memory_size = 2

name = ""

protocol = "http"

rule_health_check_connect_port = 80

scheduler = "rr"

spec = "slb.s1.small"

sticky_session = "on"

sticky_session_type = "server"

tags = {}

unhealthy_threshold = 3

url = ""

vswitch_id = ""


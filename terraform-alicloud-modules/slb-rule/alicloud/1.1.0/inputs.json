[
  {
    "name": "protocol",
    "type": "string",
    "description": "The protocol to listen on.",
    "default": "\"http\"",
    "required": false
  },
  {
    "name": "health_check_interval",
    "type": "number",
    "description": "Time interval of health checks. It is required when `health_check` is on. Valid value range: [1-50] in seconds. Default to 2.",
    "default": "2",
    "required": false
  },
  {
    "name": "scheduler",
    "type": "string",
    "description": "Scheduling algorithm, Valid values are `wrr`, `rr` and `wlc`. Default to `wrr`. This parameter is required and takes effect only when ListenerSync is set to `off`.",
    "default": "\"rr\"",
    "required": false
  },
  {
    "name": "images_owners",
    "type": "string",
    "description": "Filter results by a specific image owner. Valid items are `system`, `self`, `others`, `marketplace`.",
    "default": "\"system\"",
    "required": false
  },
  {
    "name": "cpu_core_count",
    "type": "number",
    "description": "Number of CPU cores.",
    "default": "1",
    "required": false
  },
  {
    "name": "url",
    "type": "string",
    "description": "Domain of the forwarding rule. It must be 2-80 characters in length. Only letters a-z, numbers 0-9, and characters '-' '/' '?' '%' '#' and '&' are allowed. URLs must be started with the character '/', but cannot be '/' alone.",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "health_check_http_code",
    "type": "string",
    "description": "Regular health check HTTP status code. Multiple codes are segmented by “,”. It is required when health_check is on. Default to `http_2xx`. Valid values are: `http_2xx`, `http_3xx`, `http_4xx` and `http_5xx`.",
    "default": "\"http_2xx\"",
    "required": false
  },
  {
    "name": "health_check_domain",
    "type": "string",
    "description": "Domain name used for health check. When it used to launch TCP listener, health_check_type must be `http`. Its length is limited to 1-80 and only characters such as letters, digits, ‘-‘ and ‘.’ are allowed. When it is not set or empty, Server Load Balancer uses the private network IP address of each backend server as Domain used for health check.",
    "default": "\"test\"",
    "required": false
  },
  {
    "name": "availability_zone",
    "type": "string",
    "description": "The available zone to launch modules.",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "available_disk_category",
    "type": "string",
    "description": "Filter the results by a specific disk category. Can be either `cloud`, `cloud_efficiency`, `cloud_ssd`, `ephemeral_ssd`.",
    "default": "\"cloud_efficiency\"",
    "required": false
  },
  {
    "name": "backend_port",
    "type": "number",
    "description": "Port used by the Server Load Balancer instance backend. Valid value range: [1-65535].",
    "default": "22",
    "required": false
  },
  {
    "name": "cidr_block",
    "type": "string",
    "description": "The CIDR block for the VPC. The cidr_block is Optional and default value is `172.16.0.0/12` after `v1.119.0+`.",
    "default": "\"172.16.0.0/16\"",
    "required": false
  },
  {
    "name": "images_most_recent",
    "type": "bool",
    "description": "If more than one result are returned, select the most recent one.",
    "default": "true",
    "required": false
  },
  {
    "name": "unhealthy_threshold",
    "type": "number",
    "description": "Threshold determining the result of the health check is fail. It is required when `health_check` is on. Valid value range: [1-10] in seconds. Default to 3.",
    "default": "3",
    "required": false
  },
  {
    "name": "listener_sync",
    "type": "string",
    "description": "Indicates whether a forwarding rule inherits the settings of a health check , session persistence, and scheduling algorithm from a listener. Default to on.",
    "default": "\"off\"",
    "required": false
  },
  {
    "name": "name",
    "type": "string",
    "description": "The name of a new load balancer.",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "address_type",
    "type": "string",
    "description": "The type of address. Choices are 'intranet' and 'internet'. Default to 'internet'.",
    "default": "\"internet\"",
    "required": false
  },
  {
    "name": "health_check_timeout",
    "type": "number",
    "description": "Maximum timeout of each health check response. It is required when `health_check` is on. Valid value range: [1-300] in seconds. Default to 5. Note: If `health_check_timeout` < `health_check_interval`, its will be replaced by `health_check_interval`.",
    "default": "30",
    "required": false
  },
  {
    "name": "sticky_session",
    "type": "string",
    "description": "Whether to enable session persistence, Valid values are `on` and `off`. Default to `off`. This parameter is required and takes effect only when ListenerSync is set to `off`.",
    "default": "\"on\"",
    "required": false
  },
  {
    "name": "internal",
    "type": "bool",
    "description": "It has been deprecated from 1.6.0 and 'address_type' instead. If true, SLB instance will be an internal SLB.",
    "default": "false",
    "required": false
  },
  {
    "name": "health_check_connect_port",
    "type": "string",
    "description": "Port used for health check. Valid value range: [1-65535]. Default to `None` means the backend server port is used.",
    "default": "\"20\"",
    "required": false
  },
  {
    "name": "healthy_threshold",
    "type": "number",
    "description": "Threshold determining the result of the health check is success. It is required when `health_check` is on. Valid value range: [1-10] in seconds. Default to 3.",
    "default": "3",
    "required": false
  },
  {
    "name": "vswitch_id",
    "type": "string",
    "description": "VSwitch variables, if vswitch_id is empty, then the net_type = classic.",
    "default": "\"\"",
    "required": false
  },
  {
    "name": "images_name_regex",
    "type": "string",
    "description": "A regex string to filter resulting images by name.",
    "default": "\"^ubuntu_18.*64\"",
    "required": false
  },
  {
    "name": "frontend_port",
    "type": "number",
    "description": "Port used by the Server Load Balancer instance frontend.",
    "default": "22",
    "required": false
  },
  {
    "name": "spec",
    "type": "string",
    "description": "The specification of the SLB instance.",
    "default": "\"slb.s1.small\"",
    "required": false
  },
  {
    "name": "domain",
    "type": "string",
    "description": "Domain name of the forwarding rule. It can contain letters a-z, numbers 0-9, hyphens (-), and periods (.), and wildcard characters.",
    "default": "\"*.aliyun.com\"",
    "required": false
  },
  {
    "name": "available_resource_creation",
    "type": "string",
    "description": "Type of resources that can be created.",
    "default": "\"VSwitch\"",
    "required": false
  },
  {
    "name": "bandwidth",
    "type": "number",
    "description": "Bandwidth peak of Listener.",
    "default": "5",
    "required": false
  },
  {
    "name": "cookie",
    "type": "string",
    "description": "The cookie configured on the server. It is mandatory when `sticky_session` is `on` and `sticky_session_type` is `server`. Otherwise, it will be ignored. Valid value：String in line with RFC 2965, with length being 1- 200. It only contains characters such as ASCII codes, English letters and digits instead of the comma, semicolon or spacing, and it cannot start with $.",
    "default": "\"cookie_test\"",
    "required": false
  },
  {
    "name": "cookie_timeout",
    "type": "number",
    "description": "Cookie timeout. It is mandatory when sticky_session is `on` and sticky_session_type is `insert`. Otherwise, it will be ignored. Valid value range: [1-86400] in seconds.",
    "default": "100",
    "required": false
  },
  {
    "name": "health_check_uri",
    "type": "string",
    "description": "URI used for health check. When it used to launch TCP listener, health_check_type must be `http`. Its length is limited to 1-80 and it must start with /. Only characters such as letters, digits, ‘-’, ‘/’, ‘.’, ‘%’, ‘?’, #’ and ‘&’ are allowed.",
    "default": "\"/test\"",
    "required": false
  },
  {
    "name": "rule_health_check_connect_port",
    "type": "number",
    "description": "Port used for health check. Valid value range: [1-65535]. Default to `None` means the backend server port is used.",
    "default": "80",
    "required": false
  },
  {
    "name": "tags",
    "type": "map(string)",
    "description": "A mapping of tags to assign to the resource.",
    "default": "{}",
    "required": false
  },
  {
    "name": "memory_size",
    "type": "number",
    "description": "Size of memory, measured in GB.",
    "default": "2",
    "required": false
  },
  {
    "name": "sticky_session_type",
    "type": "string",
    "description": "Mode for handling the cookie. If sticky_session is `on`, it is mandatory. Otherwise, it will be ignored. Valid values are insert and server. insert means it is inserted from Server Load Balancer; server means the Server Load Balancer learns from the backend server.",
    "default": "\"server\"",
    "required": false
  },
  {
    "name": "health_check",
    "type": "string",
    "description": "Whether to enable health check. Valid values are `on` and `off`. TCP and UDP listener's HealthCheck is always on, so it will be ignore when launching TCP or UDP listener. This parameter is required and takes effect only when ListenerSync is set to off.",
    "default": "\"on\"",
    "required": false
  }
]


output "this_load_balancer_id" {
  description = ""
  value       = module.slb-rule.this_load_balancer_id
}

output "this_rule_id" {
  description = ""
  value       = module.slb-rule.this_rule_id
}

output "this_rule_name" {
  description = ""
  value       = module.slb-rule.this_rule_name
}



variable "availability_zones" {
  type        = list(string)
  description = "List available zones to launch several VSwitches."
  default     = []
}

variable "create_security_group" {
  type        = bool
  description = "Whether to create security group. If false, you can specify an existing vpc by setting 'existing_group_id'."
  default     = true
}

variable "create_vpc" {
  type        = bool
  description = "Whether to create vpc. If false, you can specify an existing vpc by setting 'vpc_id'."
  default     = true
}

variable "default_egress_priority" {
  type        = number
  description = "A default egress priority."
  default     = 50
}

variable "default_ingress_priority" {
  type        = number
  description = "A default ingress priority."
  default     = 50
}

variable "description" {
  type        = string
  description = "Description of security group."
  default     = "Security Group managed by Terraform"
}

variable "egress_cidr_blocks" {
  type        = list(string)
  description = "The IPv4 CIDR ranges list to use on egress cidrs rules."
  default     = []
}

variable "egress_ports" {
  type        = list(number)
  description = "The port list used on 'egress_with_cidr_blocks_and_ports' ports rules."
  default     = []
}

variable "egress_rules" {
  type        = list(string)
  description = "List of egress rules to create by name."
  default     = []
}

variable "egress_with_cidr_blocks" {
  type        = list(map(string))
  description = "List of egress rules to create where 'cidr_blocks' is used. The valid keys contains 'cidr_blocks', 'from_port', 'to_port', 'protocol', 'description' and 'priority'."
  default     = []
}

variable "egress_with_cidr_blocks_and_ports" {
  type        = list(map(string))
  description = "List of egress rules to create where 'cidr_blocks' and 'ports' is used. The valid keys contains 'cidr_blocks', 'ports', 'protocol', 'description' and 'priority'. The ports item's 'from' and 'to' have the same port. Example: '80,443' means 80/80 and 443/443."
  default     = []
}

variable "egress_with_source_security_group_id" {
  type        = list(map(string))
  description = "List of egress rules to create where 'source_security_group_id' is used."
  default     = []
}

variable "existing_vpc_id" {
  type        = string
  description = "The vpc id used to launch several vswitches."
  default     = ""
}

variable "ingress_cidr_blocks" {
  type        = list(string)
  description = "The IPv4 CIDR ranges list to use on ingress cidrs rules."
  default     = []
}

variable "ingress_ports" {
  type        = list(number)
  description = "The port list used on 'ingress_with_cidr_blocks_and_ports' ports rules."
  default     = []
}

variable "ingress_rules" {
  type        = list(string)
  description = "List of ingress rules to create by name."
  default     = []
}

variable "ingress_with_cidr_blocks" {
  type        = list(map(string))
  description = "List of ingress rules to create where 'cidr_blocks' is used. The valid keys contains 'cidr_blocks', 'from_port', 'to_port', 'protocol', 'description', 'priority' and 'rule'."
  default     = []
}

variable "ingress_with_cidr_blocks_and_ports" {
  type        = list(map(string))
  description = "List of ingress rules to create where 'cidr_blocks' and 'ports' is used. The valid keys contains 'cidr_blocks', 'ports', 'protocol', 'description' and 'priority'. The ports item's 'from' and 'to' have the same port. Example: '80,443' means 80/80 and 443/443."
  default     = []
}

variable "ingress_with_source_security_group_id" {
  type        = list(map(string))
  description = "List of ingress rules to create where `source_security_group_id` is used."
  default     = []
}

variable "name" {
  type        = string
  description = "Name of security group. It is used to create a new security group. A random name prefixed with 'terraform-sg-' will be set if it is empty."
  default     = ""
}

variable "priority_for_egress_rules" {
  type        = number
  description = "A priority where 'egress_rules' is used. Default to 'default_egress_priority'."
  default     = 1
}

variable "priority_for_ingress_rules" {
  type        = number
  description = "A priority is used when setting 'ingress_rules'. Default to 'default_ingress_priority'."
  default     = 1
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.2.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "security_group_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to security group."
  default     = {}
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.2.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.2.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "use_existing_vpc" {
  type        = bool
  description = "The vpc id used to launch several vswitches. If set, the 'create_vpc' will be ignored."
  default     = true
}

variable "use_num_suffix" {
  type        = bool
  description = "Always append numerical suffix(like 001, 002 and so on) to vswitch name, even if the length of 'vswitch_cidrs' is 1. Default to false."
  default     = false
}

variable "vpc_cidr" {
  type        = string
  description = "The cidr block used to launch a new vpc."
  default     = "172.16.0.0/12"
}

variable "vpc_description" {
  type        = string
  description = "The vpc description used to launch a new vpc."
  default     = "A new VPC created by Terrafrom module terraform-alicloud-vpc"
}

variable "vpc_name" {
  type        = string
  description = "The vpc name used to launch a new vpc."
  default     = "TF-VPC"
}

variable "vpc_tags" {
  type        = map(string)
  description = "The tags used to launch a new vpc. Before 1.5.0, it used to retrieve existing VPC."
  default     = {}
}

variable "vswitch_cidrs" {
  type        = list(string)
  description = "List of cidr blocks used to launch several new vswitches. If not set, there is no new vswitches will be created."
  default     = []
}

variable "vswitch_description" {
  type        = string
  description = "The vswitch description used to launch several new vswitch."
  default     = "New VSwitch created by Terrafrom module terraform-alicloud-vpc."
}

variable "vswitch_name" {
  type        = string
  description = "The vswitch name prefix used to launch several new vswitches."
  default     = "TF-VSwitch"
}

variable "vswitch_tags" {
  type        = map(string)
  description = "The tags used to launch serveral vswitches."
  default     = {}
}


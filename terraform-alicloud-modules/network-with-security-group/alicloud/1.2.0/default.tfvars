
availability_zones = []

create_security_group = true

create_vpc = true

default_egress_priority = 50

default_ingress_priority = 50

description = "Security Group managed by Terraform"

egress_cidr_blocks = []

egress_ports = []

egress_rules = []

egress_with_cidr_blocks = []

egress_with_cidr_blocks_and_ports = []

egress_with_source_security_group_id = []

existing_vpc_id = ""

ingress_cidr_blocks = []

ingress_ports = []

ingress_rules = []

ingress_with_cidr_blocks = []

ingress_with_cidr_blocks_and_ports = []

ingress_with_source_security_group_id = []

name = ""

priority_for_egress_rules = 1

priority_for_ingress_rules = 1

profile = ""

region = ""

security_group_tags = {}

shared_credentials_file = ""

skip_region_validation = false

use_existing_vpc = true

use_num_suffix = false

vpc_cidr = "172.16.0.0/12"

vpc_description = "A new VPC created by Terrafrom module terraform-alicloud-vpc"

vpc_name = "TF-VPC"

vpc_tags = {}

vswitch_cidrs = []

vswitch_description = "New VSwitch created by Terrafrom module terraform-alicloud-vpc."

vswitch_name = "TF-VSwitch"

vswitch_tags = {}


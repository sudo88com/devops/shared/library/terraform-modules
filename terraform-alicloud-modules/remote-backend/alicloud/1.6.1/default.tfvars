
backend_oss_bucket = ""

backend_ots_lock_instance = "tf-oss-backend"

backend_ots_lock_table = ""

bucket_logging = []

bucket_server_side_encryption = []

bucket_versioning_status = ""

create_backend_bucket = false

create_ots_lock_instance = false

create_ots_lock_table = false

encrypt_state = true

ots_instance_type = null

region = ""

state_acl = "private"

state_name = ""

state_path = ""



variable "backend_oss_bucket" {
  type        = string
  description = "Name of OSS bucket prepared to hold your terraform state(s). If not set, the module will craete one with prefix `terraform-remote-backend`."
  default     = ""
}

variable "backend_ots_lock_instance" {
  type        = string
  description = "The name of OTS instance to which table belongs."
  default     = "tf-oss-backend"
}

variable "backend_ots_lock_table" {
  type        = string
  description = "OTS table to hold state lock when updating. If not set, the module will craete one with prefix `terraform-remote-backend`."
  default     = ""
}

variable "bucket_logging" {
  type        = list(map(string))
  description = "The logging configuration of the bucket. Supports arguments: target_bucket, target_prefix. The target_bucket is the name of the bucket that will receive the log objects, which is required. The target_prefix is the key prefix for log objects, which is optional."
  default     = []
}

variable "bucket_server_side_encryption" {
  type        = list(map(string))
  description = "The server-side encryption configuration of the bucket. Supports arguments: kms_master_key_id, sse_algorithm, kms_data_encryption. The sse_algorith is required."
  default     = []
}

variable "bucket_versioning_status" {
  type        = string
  description = "Specifies the versioning state of a bucket. Valid values: `Enabled` and `Suspended`."
  default     = ""
}

variable "create_backend_bucket" {
  type        = bool
  description = "Boolean.  If you have a OSS bucket already, use that one, else make this true and one will be created."
  default     = false
}

variable "create_ots_lock_instance" {
  type        = bool
  description = "Boolean:  If you have a OTS instance already, use that one, else make this true and one will be created."
  default     = false
}

variable "create_ots_lock_table" {
  type        = bool
  description = "Boolean:  If you have a ots table already, use that one, else make this true and one will be created."
  default     = false
}

variable "encrypt_state" {
  type        = bool
  description = "Boolean. Whether to encrypt terraform state."
  default     = true
}

variable "ots_instance_type" {
  type        = string
  description = "The type of instance. Valid values are Capacity and HighPerformance. Default to HighPerformance."
  default     = null
}

variable "region" {
  type        = string
  description = "The region used to launch this module resources."
  default     = ""
}

variable "state_acl" {
  type        = string
  description = "Canned ACL applied to bucket."
  default     = "private"
}

variable "state_name" {
  type        = string
  description = "The name of the state file. Examples: dev/tf.state, dev/frontend/tf.tfstate, etc.."
  default     = ""
}

variable "state_path" {
  type        = string
  description = "The path directory of the state file will be stored. Examples: dev/frontend, prod/db, etc.."
  default     = ""
}


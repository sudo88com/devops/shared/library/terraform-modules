
locals {
  backend_oss_bucket = var.backend_oss_bucket
  backend_ots_lock_instance = var.backend_ots_lock_instance
  backend_ots_lock_table = var.backend_ots_lock_table
  bucket_logging = var.bucket_logging
  bucket_server_side_encryption = var.bucket_server_side_encryption
  bucket_versioning_status = var.bucket_versioning_status
  create_backend_bucket = var.create_backend_bucket
  create_ots_lock_instance = var.create_ots_lock_instance
  create_ots_lock_table = var.create_ots_lock_table
  encrypt_state = var.encrypt_state
  ots_instance_type = var.ots_instance_type
  region = var.region
  state_acl = var.state_acl
  state_name = var.state_name
  state_path = var.state_path
}

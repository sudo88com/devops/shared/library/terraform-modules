
variable "comment" {
  type        = string
  description = "Comments of project"
  default     = null
}

variable "default_quota" {
  type        = string
  description = "Default Computing Resource Group"
  default     = null
}

variable "ip_white_list" {
  type        = list(object({
    ip_list     = string
    vpc_ip_list = string
  }))
  description = "IP whitelist. ip_list: Classic network IP white list. vpc_ip_list: VPC network whitelist."
  default     = []
}

variable "name" {
  type        = string
  description = "The maxcompute project name."
  default     = "From_Terraform"
}

variable "order_type" {
  type        = string
  description = "(Deprecated from version 1.2.0) The type of payment, only `PayAsYouGo` supported currently."
  default     = "PayAsYouGo"
}

variable "product_type" {
  type        = string
  description = "Quota payment type. Supported values: `PayAsYouGo`, `Subscription` and `Dev`."
  default     = null
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0) Skip staticvalidation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "specification_type" {
  type        = string
  description = "(Deprecated from version 1.2.0) The type of resource Specification, only `OdpsStandard` supported currently."
  default     = "OdpsStandard"
}


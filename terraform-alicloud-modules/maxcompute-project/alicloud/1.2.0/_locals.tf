
locals {
  comment = var.comment
  default_quota = var.default_quota
  ip_white_list = var.ip_white_list
  name = var.name
  order_type = var.order_type
  product_type = var.product_type
  profile = var.profile
  region = var.region
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  specification_type = var.specification_type
}

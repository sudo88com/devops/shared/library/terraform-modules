
output "this_maxcompute_project_id" {
  description = "The ID of the project."
  value       = module.maxcompute-project.this_maxcompute_project_id
}

output "this_maxcompute_project_name" {
  description = "The name of the project."
  value       = module.maxcompute-project.this_maxcompute_project_name
}

output "this_maxcompute_project_order_type" {
  description = "The order_type of the project."
  value       = module.maxcompute-project.this_maxcompute_project_order_type
}


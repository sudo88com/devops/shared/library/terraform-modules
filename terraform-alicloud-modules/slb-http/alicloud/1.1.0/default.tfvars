
address_type = "internet"

advanced_setting = {}

bandwidth = 10

create_http_listener = true

create_https_listener = true

create_rule = true

create_server_certificate = true

create_slb = true

existing_slb_id = ""

health_check = {}

http_listeners = []

https_listeners = []

internet_charge_type = "PayByTraffic"

master_zone_id = ""

profile = ""

region = ""

rules = []

servers_of_virtual_server_group = []

shared_credentials_file = ""

skip_region_validation = false

slave_zone_id = ""

spec = ""

ssl_certificates = {}

tags = {}

use_existing_slb = false

virtual_server_group_name = ""

x_forwarded_for = {}


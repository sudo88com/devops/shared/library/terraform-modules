
variable "account_ids" {
  type        = list(string)
  description = "Alibaba Cloud account id used to share images."
  default     = []
}

variable "create" {
  type        = bool
  description = "Whether to create image."
  default     = true
}

variable "description" {
  type        = string
  description = "The description of the image."
  default     = "Image created by terraform"
}

variable "disk_device_mapping" {
  type        = list(map(string))
  description = "List of disk device mapping used to create image. Each item can contains keys:'size'(The size of a diskin the combined custom image. It's not empty),'device'(The name of a disk in the combined custom image. Value range: /dev/xvda to /dev/xvdz.),'snapshot_id'(The id of snapshot that is used to create a combined custom image.),'disk_type'(The type of a disk in the combined custom image.)."
  default     = []
}

variable "disk_image_create_platform" {
  type        = string
  description = "The operating system platform of the system disk created by disk. Default to 'Ubuntu'."
  default     = "Ubuntu"
}

variable "export" {
  type        = bool
  description = "Whether to export the image. Default to 'false'."
  default     = false
}

variable "export_oss_bucket" {
  type        = string
  description = "Save the exported OSS bucket."
  default     = ""
}

variable "force" {
  type        = bool
  description = "Indicates whether to force delete the custom image, Default is 'false'."
  default     = false
}

variable "image_create_architecture" {
  type        = string
  description = "The architecture of the system disk. Default to 'x86_64'."
  default     = "x86_64"
}

variable "image_import_architecture" {
  type        = string
  description = "The architecture of the system disk. Default to 'x86_64'."
  default     = "x86_64"
}

variable "image_import_platform" {
  type        = string
  description = "The operating system platform of the system disk. Default to 'Ubuntu'."
  default     = "Ubuntu"
}

variable "image_name" {
  type        = string
  description = "The name of image."
  default     = ""
}

variable "import" {
  type        = bool
  description = "Whether to import the image. Default to 'false'."
  default     = false
}

variable "import_disk_device_mapping" {
  type        = map(string)
  description = "List of disk device mapping. Each item can contains keys:'import_oss_bucket'(The bucket where imported image belongs.),'oss_object'(The name of image in oss bucket.)."
  default     = {}
}

variable "import_image_name" {
  type        = string
  description = "The name of imported image. If not set, a default name with prefix 'tf-import-image-' will be returned."
  default     = ""
}

variable "instance_id" {
  type        = string
  description = "The id of instance."
  default     = ""
}

variable "instance_image_create_platform" {
  type        = string
  description = "The operating system platform of the system disk created by instance. Default to 'Ubuntu'."
  default     = "Ubuntu"
}

variable "license_type" {
  type        = string
  description = "Activate the type of license used by the operating system after importing the image. Default to 'Auto'."
  default     = "Auto"
}

variable "os_type" {
  type        = string
  description = "Operating system platform type. Default to 'linux'."
  default     = "linux"
}

variable "oss_prefix" {
  type        = string
  description = "The prefix of your OSS Object. Default to 'tf'."
  default     = "tf"
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.2.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "resource_group_id" {
  type        = string
  description = "The ID of the enterprise resource group to which a custom image belongs."
  default     = ""
}

variable "share" {
  type        = bool
  description = "Whether to share the image. Default to 'false'."
  default     = false
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.2.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.2.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "snapshot_id" {
  type        = string
  description = "The id of snapshot."
  default     = ""
}

variable "snapshot_image_create_platform" {
  type        = string
  description = "The operating system platform of the system disk created by snapshot. Default to 'Ubuntu'."
  default     = "Ubuntu"
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the image."
  default     = {}
}



locals {
  account_ids = var.account_ids
  create = var.create
  description = var.description
  disk_device_mapping = var.disk_device_mapping
  disk_image_create_platform = var.disk_image_create_platform
  export = var.export
  export_oss_bucket = var.export_oss_bucket
  force = var.force
  image_create_architecture = var.image_create_architecture
  image_import_architecture = var.image_import_architecture
  image_import_platform = var.image_import_platform
  image_name = var.image_name
  import = var.import
  import_disk_device_mapping = var.import_disk_device_mapping
  import_image_name = var.import_image_name
  instance_id = var.instance_id
  instance_image_create_platform = var.instance_image_create_platform
  license_type = var.license_type
  os_type = var.os_type
  oss_prefix = var.oss_prefix
  profile = var.profile
  region = var.region
  resource_group_id = var.resource_group_id
  share = var.share
  shared_credentials_file = var.shared_credentials_file
  skip_region_validation = var.skip_region_validation
  snapshot_id = var.snapshot_id
  snapshot_image_create_platform = var.snapshot_image_create_platform
  tags = var.tags
}

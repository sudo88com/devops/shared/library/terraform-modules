
account_ids = []

create = true

description = "Image created by terraform"

disk_device_mapping = []

disk_image_create_platform = "Ubuntu"

export = false

export_oss_bucket = ""

force = false

image_create_architecture = "x86_64"

image_import_architecture = "x86_64"

image_import_platform = "Ubuntu"

image_name = ""

import = false

import_disk_device_mapping = {}

import_image_name = ""

instance_id = ""

instance_image_create_platform = "Ubuntu"

license_type = "Auto"

os_type = "linux"

oss_prefix = "tf"

profile = ""

region = ""

resource_group_id = ""

share = false

shared_credentials_file = ""

skip_region_validation = false

snapshot_id = ""

snapshot_image_create_platform = "Ubuntu"

tags = {}


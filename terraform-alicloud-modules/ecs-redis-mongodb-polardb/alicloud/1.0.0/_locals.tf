
locals {
  availability_zone = var.availability_zone
  available_resource_creation = var.available_resource_creation
  category = var.category
  db_instance_class = var.db_instance_class
  db_instance_storage = var.db_instance_storage
  db_node_class = var.db_node_class
  db_version = var.db_version
  description = var.description
  ecs_size = var.ecs_size
  image_id = var.image_id
  instance_type = var.instance_type
  internet_max_bandwidth_out = var.internet_max_bandwidth_out
  mongodb_engine_version = var.mongodb_engine_version
  name = var.name
  pay_type = var.pay_type
  redis_appendonly = var.redis_appendonly
  redis_engine_version = var.redis_engine_version
  redis_instance_class = var.redis_instance_class
  redis_instance_type = var.redis_instance_type
  redis_lazyfree_lazy_eviction = var.redis_lazyfree_lazy_eviction
  redis_resource_group_id = var.redis_resource_group_id
  security_group_ids = var.security_group_ids
  security_ips = var.security_ips
  system_disk_category = var.system_disk_category
  system_disk_description = var.system_disk_description
  system_disk_name = var.system_disk_name
  vswitch_id = var.vswitch_id
}

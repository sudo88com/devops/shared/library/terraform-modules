
availability_zone = ""

available_resource_creation = "PolarDB"

category = "cloud_efficiency"

db_instance_class = "dds.mongo.mid"

db_instance_storage = 10

db_node_class = "polar.mysql.x4.large"

db_version = "8.0"

description = "tf-ecs-redis-mongodb-polardb-description"

ecs_size = 1200

image_id = "ubuntu_18_04_64_20G_alibase_20190624.vhd"

instance_type = "ecs.n4.large"

internet_max_bandwidth_out = 10

mongodb_engine_version = "3.4"

name = "tf-ecs-redis-mongodb-polardb"

pay_type = "PostPaid"

redis_appendonly = "yes"

redis_engine_version = "4.0"

redis_instance_class = "redis.master.large.default"

redis_instance_type = "Redis"

redis_lazyfree_lazy_eviction = "yes"

redis_resource_group_id = "rg-123456"

security_group_ids = []

security_ips = [
  "127.0.0.1"
]

system_disk_category = "cloud_efficiency"

system_disk_description = "system_disk_description"

system_disk_name = "system_disk"

vswitch_id = ""


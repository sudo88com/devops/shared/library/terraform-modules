
output "this_ecs_id" {
  description = ""
  value       = module.ecs-redis-mongodb-polardb.this_ecs_id
}

output "this_ecs_name" {
  description = ""
  value       = module.ecs-redis-mongodb-polardb.this_ecs_name
}

output "this_mongodb_id" {
  description = ""
  value       = module.ecs-redis-mongodb-polardb.this_mongodb_id
}

output "this_polardb_cluster_id" {
  description = ""
  value       = module.ecs-redis-mongodb-polardb.this_polardb_cluster_id
}

output "this_polardb_database_id" {
  description = ""
  value       = module.ecs-redis-mongodb-polardb.this_polardb_database_id
}

output "this_redis_id" {
  description = ""
  value       = module.ecs-redis-mongodb-polardb.this_redis_id
}


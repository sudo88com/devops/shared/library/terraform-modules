
account_password = ""

alarm_rule_contact_groups = []

alarm_rule_effective_interval = "0:00-2:00"

alarm_rule_name = ""

alarm_rule_operator = "=="

alarm_rule_period = 300

alarm_rule_silence_time = 86400

alarm_rule_statistics = "Average"

alarm_rule_threshold = ""

alarm_rule_triggered_count = 3

backup_period = []

backup_time = ""

create = true

enable_alarm_rule = true

engine_version = ""

existing_instance_id = ""

instance_charge_type = "PostPaid"

mongo_list = []

name = ""

period = 1

profile = ""

region = ""

security_ip_list = []

shard_list = []

shared_credentials_file = ""

skip_region_validation = false

storage_engine = "WiredTiger"

vswitch_id = ""

zone_id = ""


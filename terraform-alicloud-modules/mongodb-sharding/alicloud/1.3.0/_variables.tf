
variable "account_password" {
  type        = string
  description = "Password of the root account. It is a string of 6 to 32 characters and is composed of letters, numbers, and underlines. "
  default     = ""
}

variable "alarm_rule_contact_groups" {
  type        = list(string)
  description = "List contact groups of the alarm rule, which must have been created on the console. "
  default     = []
}

variable "alarm_rule_effective_interval" {
  type        = string
  description = "The interval of effecting alarm rule. It foramt as 'hh:mm-hh:mm', like '0:00-4:00'."
  default     = "0:00-2:00"
}

variable "alarm_rule_name" {
  type        = string
  description = "The alarm rule name. "
  default     = ""
}

variable "alarm_rule_operator" {
  type        = string
  description = "Alarm comparison operator. Valid values: ['<=', '<', '>', '>=', '==', '!=']. Default to '=='. "
  default     = "=="
}

variable "alarm_rule_period" {
  type        = number
  description = "Index query cycle, which must be consistent with that defined for metrics. Default to 300, in seconds. "
  default     = 300
}

variable "alarm_rule_silence_time" {
  type        = number
  description = "Notification silence period in the alarm state, in seconds. Valid value range: [300, 86400]. Default to 86400. "
  default     = 86400
}

variable "alarm_rule_statistics" {
  type        = string
  description = "Statistical method. It must be consistent with that defined for metrics. Valid values: ['Average', 'Minimum', 'Maximum']. Default to 'Average'. "
  default     = "Average"
}

variable "alarm_rule_threshold" {
  type        = string
  description = "Alarm threshold value, which must be a numeric value currently. "
  default     = ""
}

variable "alarm_rule_triggered_count" {
  type        = number
  description = "Number of consecutive times it has been detected that the values exceed the threshold. Default to 3. "
  default     = 3
}

variable "backup_period" {
  type        = list(any)
  description = "MongoDB sharding Instance backup period. It is required when backup_time was existed. Valid values: [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday]. Default to [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday] "
  default     = []
}

variable "backup_time" {
  type        = string
  description = "MongoDB sharding instance backup time. It is required when backup_period was existed. In the format of HH:mmZ- HH:mmZ. Time setting interval is one hour. If not specified, the system will generate a default, like `23:00Z-24:00Z`. "
  default     = ""
}

variable "create" {
  type        = bool
  description = "Whether to use an existing MongoDB sharding. If false, you can use a existing Mongodb sharding instance by setting `existing_instance_id`. "
  default     = true
}

variable "enable_alarm_rule" {
  type        = bool
  description = "Whether to enable alarm rule. Default to true. "
  default     = true
}

variable "engine_version" {
  type        = string
  description = "The version number of the database. Valid value: 3.4, 4.0. "
  default     = ""
}

variable "existing_instance_id" {
  type        = string
  description = "The Id of an existing Mongodb sharding instance. If set, the `create` will be ignored. "
  default     = ""
}

variable "instance_charge_type" {
  type        = string
  description = "The billing method of the instance. Valid values are Prepaid, PostPaid, Default to PostPaid. "
  default     = "PostPaid"
}

variable "mongo_list" {
  type        = list(map(string))
  description = "The mongo-node count can be purchased is in range of [2, 32]. "
  default     = []
}

variable "name" {
  type        = string
  description = "The name of DB instance. It a string of 2 to 256 characters. "
  default     = ""
}

variable "period" {
  type        = number
  description = "The duration that you will buy DB instance (in month). It is valid when instance_charge_type is PrePaid. Valid values: [1~9], 12, 24, 36. System default to 1. "
  default     = 1
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.3.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable. "
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.3.0) The region used to launch this module resources. "
  default     = ""
}

variable "security_ip_list" {
  type        = list(string)
  description = "List of IP addresses allowed to access all databases of an instance. The list contains up to 1,000 IP addresses, separated by commas. Supported formats include 0.0.0.0/0, 10.23.12.24 (IP), and 10.23.12.24/24 (Classless Inter-Domain Routing (CIDR) mode. /24 represents the length of the prefix in an IP address. The range of the prefix length is [1,32]). System default to [`127.0.0.1`]. "
  default     = []
}

variable "shard_list" {
  type        = list(map(string))
  description = "The shard-node count can be purchased is in range of [2, 32]. "
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.3.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used. "
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.3.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet). "
  default     = false
}

variable "storage_engine" {
  type        = string
  description = "Storage engine: WiredTiger or RocksDB. System Default value: WiredTiger. "
  default     = "WiredTiger"
}

variable "vswitch_id" {
  type        = string
  description = "The virtual switch ID to launch DB instances in one VPC. "
  default     = ""
}

variable "zone_id" {
  type        = string
  description = "The Zone to launch the DB instance. MongoDB sharding instance does not support multiple-zone. If it is a multi-zone and vswitch_id is specified, the vswitch must in one of them. "
  default     = ""
}


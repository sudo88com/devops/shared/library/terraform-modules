
allocate_public_ip = false

backend_port = 8080

bandwidth = 10

bind_domain = false

create_autoscaling = true

create_slb = false

data_disks = []

db_instance_ids = []

default_cooldown = 300

dns_record = {}

domain_name = ""

ecs_instance_password = ""

existing_slb_id = ""

force_delete = true

frontend_port = 8080

heartbeat_timeout = 600

hook_action_policy = "CONTINUE"

image_id = ""

instance_port = 8080

instance_types = []

internet_charge_type = "PayByTraffic"

internet_max_bandwidth_in = 200

internet_max_bandwidth_out = 0

key_name = ""

kms_encrypted_password = ""

kms_encryption_context = {}

lifecycle_hook_name = ""

lifecycle_transition = "SCALE_IN"

master_zone_id = ""

max_size = 3

min_size = 1

mns_queue_name = ""

mns_topic_name = ""

multi_az_policy = "PRIORITY"

notification_metadata = ""

on_demand_base_capacity = 0

on_demand_percentage_above_base_capacity = 0

password_inherit = false

product_keyword = ""

product_suggested_price = 0

product_supplier_name_keyword = ""

profile = ""

region = ""

removal_policies = [
  "OldestScalingConfiguration",
  "OldestInstance"
]

role_name = ""

scaling_configuration_name = ""

scaling_group_id = ""

scaling_group_name = ""

security_group_ids = []

shared_credentials_file = ""

skip_region_validation = false

slave_zone_id = ""

slb_internet_charge_type = "PayByTraffic"

slb_name = "tf-module-slb"

slb_spec = "slb.s1.small"

slb_tags = {}

spot_instance_pools = 0

spot_instance_remedy = true

system_disk_category = "cloud_efficiency"

system_disk_size = 50

use_existing_slb = false

user_data = ""

virtual_server_group_name = ""

vswitch_ids = []

weight = 50


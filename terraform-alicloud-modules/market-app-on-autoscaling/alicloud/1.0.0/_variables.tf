
variable "allocate_public_ip" {
  type        = bool
  description = "Whether to associate a public ip address with an instance in a VPC."
  default     = false
}

variable "backend_port" {
  type        = number
  description = "The backend port of balancer."
  default     = 8080
}

variable "bandwidth" {
  type        = number
  description = "The load balancer instance bandwidth."
  default     = 10
}

variable "bind_domain" {
  type        = bool
  description = "Whether to bind domain."
  default     = false
}

variable "create_autoscaling" {
  type        = bool
  description = "Whether to create scaling resource."
  default     = true
}

variable "create_slb" {
  type        = bool
  description = "Whether to create a balancer instance."
  default     = false
}

variable "data_disks" {
  type        = list(map(string))
  description = "Additional data disks to attach to the scaled ECS instance."
  default     = []
}

variable "db_instance_ids" {
  type        = list(string)
  description = "A list of rds instance ids to add to the autoscaling group. If not set, it can be retrieved automatically by specifying filter 'rds_name_regex' or 'rds_tags'."
  default     = []
}

variable "default_cooldown" {
  type        = string
  description = "The amount of time (in seconds),after a scaling activity completes before another scaling activity can start."
  default     = 300
}

variable "dns_record" {
  type        = map(string)
  description = "DNS record. Each item can contains keys: 'host_record'(The host record of the domain record.),'type'(The type of the domain. Valid values: A, NS, MX, TXT, CNAME, SRV, AAAA, CAA, REDIRECT_URL, FORWORD_URL. Default to A.),'priority'(The priority of domain record. Valid values are `[1-10]`. When the `type` is `MX`, this parameter is required.),'ttl'(The ttl of the domain record. Default to 600.),'line'(The resolution line of domain record. Default value is default.)."
  default     = {}
}

variable "domain_name" {
  type        = string
  description = "The name of domain."
  default     = ""
}

variable "ecs_instance_password" {
  type        = string
  description = "The password of the ECS instance. It is valid when 'password_inherit' is false"
  default     = ""
}

variable "existing_slb_id" {
  type        = string
  description = "An existing load balancer instance id."
  default     = ""
}

variable "force_delete" {
  type        = bool
  description = "The last scaling configuration will be deleted forcibly with deleting its scaling group."
  default     = true
}

variable "frontend_port" {
  type        = number
  description = "The fronted port of balancer."
  default     = 8080
}

variable "heartbeat_timeout" {
  type        = number
  description = "Defines the amount of time, in seconds, that can elapse before the lifecycle hook times out. When the lifecycle hook times out, Auto Scaling performs the action defined in the default_result parameter."
  default     = 600
}

variable "hook_action_policy" {
  type        = string
  description = "Defines the action which scaling group should take when the lifecycle hook timeout elapses. Valid value: CONTINUE, ABANDON."
  default     = "CONTINUE"
}

variable "image_id" {
  type        = string
  description = "The image id used to launch ecs instances."
  default     = ""
}

variable "instance_port" {
  type        = number
  description = "The port will be used for VServer Group backend server."
  default     = 8080
}

variable "instance_types" {
  type        = list(string)
  description = "A list of ECS instance types. If not set, one will be returned automatically by specifying 'cpu_core_count' and 'memory_size'. If it is set, 'instance_type' will be ignored."
  default     = []
}

variable "internet_charge_type" {
  type        = string
  description = "The ECS instance network billing type: PayByTraffic or PayByBandwidth."
  default     = "PayByTraffic"
}

variable "internet_max_bandwidth_in" {
  type        = number
  description = "Maximum incoming bandwidth from the public network."
  default     = 200
}

variable "internet_max_bandwidth_out" {
  type        = number
  description = "Maximum outgoing bandwidth from the public network. It will be ignored when 'associate_public_ip_address' is false."
  default     = 0
}

variable "key_name" {
  type        = string
  description = "The name of key pair that login ECS."
  default     = ""
}

variable "kms_encrypted_password" {
  type        = string
  description = "An KMS encrypts password used to a db account. If 'password_inherit' and 'password' is set, this field will be ignored."
  default     = ""
}

variable "kms_encryption_context" {
  type        = map(string)
  description = "An KMS encryption context used to decrypt 'kms_encrypted_password' before creating ECS instance. See Encryption Context: https://www.alibabacloud.com/help/doc-detail/42975.htm. It is valid when kms_encrypted_password is set."
  default     = {}
}

variable "lifecycle_hook_name" {
  type        = string
  description = "The name for lifecyle hook. Default to a random string prefixed with 'terraform-ess-hook-'."
  default     = ""
}

variable "lifecycle_transition" {
  type        = string
  description = "Type of Scaling activity attached to lifecycle hook. Supported value: SCALE_OUT, SCALE_IN."
  default     = "SCALE_IN"
}

variable "master_zone_id" {
  type        = string
  description = "The primary zone ID of the SLB instance. If not specified, the system will be randomly assigned."
  default     = ""
}

variable "max_size" {
  type        = number
  description = "Maximum number of ECS instance in the scaling group."
  default     = 3
}

variable "min_size" {
  type        = string
  description = "Minimum number of ECS instances in the scaling group."
  default     = 1
}

variable "mns_queue_name" {
  type        = string
  description = "Specify a MNS queue to send notification. It will be ignored when 'mns_topic_name' is set."
  default     = ""
}

variable "mns_topic_name" {
  type        = string
  description = "Specify a MNS topic to send notification."
  default     = ""
}

variable "multi_az_policy" {
  type        = string
  description = "Multi-AZ scaling group ECS instance expansion and contraction strategy. PRIORITY, BALANCE or COST_OPTIMIZED."
  default     = "PRIORITY"
}

variable "notification_metadata" {
  type        = string
  description = "Additional information that you want to include when Auto Scaling sends a message to the notification target."
  default     = ""
}

variable "on_demand_base_capacity" {
  type        = number
  description = "The minimum amount of the Auto Scaling group's capacity that must be fulfilled by On-Demand Instances. This base portion is provisioned first as your group scales."
  default     = 0
}

variable "on_demand_percentage_above_base_capacity" {
  type        = number
  description = "Controls the percentages of On-Demand Instances and Spot Instances for your additional capacity beyond OnDemandBaseCapacity."
  default     = 0
}

variable "password_inherit" {
  type        = bool
  description = "Specifies whether to use the password that is predefined in the image. If true, the 'password' and 'kms_encrypted_password' will be ignored. You must ensure that the selected image has a password configured."
  default     = false
}

variable "product_keyword" {
  type        = string
  description = "The name keyword of Market Product used to fetch the specified product image."
  default     = ""
}

variable "product_suggested_price" {
  type        = number
  description = "The suggested price of Market Product used to fetch the specified product image."
  default     = 0
}

variable "product_supplier_name_keyword" {
  type        = string
  description = "The name keyword of Market Product supplier name used to fetch the specified product image."
  default     = ""
}

variable "profile" {
  type        = string
  description = "The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "The region used to launch this module resources."
  default     = ""
}

variable "removal_policies" {
  type        = list(string)
  description = "RemovalPolicy is used to select the ECS instances you want to remove from the scaling group when multiple candidates for removal exist."
  default     = [
  "OldestScalingConfiguration",
  "OldestInstance"
]
}

variable "role_name" {
  type        = string
  description = "Instance RAM role name."
  default     = ""
}

variable "scaling_configuration_name" {
  type        = string
  description = "Name for the autoscaling configuration. Default to a random string prefixed with `terraform-ess-configuration-`."
  default     = ""
}

variable "scaling_group_id" {
  type        = string
  description = "Specifying existing autoscaling group ID. If not set, a new one will be created named with 'scaling_group_name'."
  default     = ""
}

variable "scaling_group_name" {
  type        = string
  description = "The name for autoscaling group. Default to a random string prefixed with 'terraform-ess-group-'."
  default     = ""
}

variable "security_group_ids" {
  type        = list(string)
  description = "List IDs of the security group to which a newly created instance belongs."
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "slave_zone_id" {
  type        = string
  description = "The standby zone ID of the SLB instance. If not specified, the system will be randomly assigned."
  default     = ""
}

variable "slb_internet_charge_type" {
  type        = string
  description = "The charge type of load balancer instance internet network."
  default     = "PayByTraffic"
}

variable "slb_name" {
  type        = string
  description = "The name of a new load balancer."
  default     = "tf-module-slb"
}

variable "slb_spec" {
  type        = string
  description = "The specification of the SLB instance."
  default     = "slb.s1.small"
}

variable "slb_tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the resource."
  default     = {}
}

variable "spot_instance_pools" {
  type        = number
  description = "The number of Spot pools to use to allocate your Spot capacity. The Spot pools is composed of instance types of lowest price."
  default     = 0
}

variable "spot_instance_remedy" {
  type        = bool
  description = "Whether to replace spot instances with newly created spot/onDemand instance when receive a spot recycling message."
  default     = true
}

variable "system_disk_category" {
  type        = string
  description = "Category of the system disk."
  default     = "cloud_efficiency"
}

variable "system_disk_size" {
  type        = number
  description = "Size of the system disk."
  default     = 50
}

variable "use_existing_slb" {
  type        = bool
  description = "Whether to use an existing load balancer instance. If true, 'existing_slb_id' should not be empty. Also, you can create a new one by setting 'create = true'."
  default     = false
}

variable "user_data" {
  type        = string
  description = "User-defined data to customize the startup behaviors of the ECS instance and to pass data into the ECS instance."
  default     = ""
}

variable "virtual_server_group_name" {
  type        = string
  description = "The name virtual server group. If not set, the 'name' and adding suffix '-virtual' will return."
  default     = ""
}

variable "vswitch_ids" {
  type        = list(string)
  description = "Virtual switch IDs in which the ecs instances to be launched."
  default     = []
}

variable "weight" {
  type        = number
  description = "The weight of an ECS instance attached to the VServer Group."
  default     = 50
}


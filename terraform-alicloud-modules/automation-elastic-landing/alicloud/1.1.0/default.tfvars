
application_number = 1

application_path = "applications/appcenter/kustomize/base"

application_target_revision = "main"

application_url = "https://github.com/terraform-alicloud-modules/terraform-alicloud-automation-elastic-landing"

apply_reserved_instance = false

autoscaling_node_bind_eip = false

autoscaling_node_max_number = 10

autoscaling_node_min_number = 0

cluster_addons = []

enable_service = "Off"

environment = "prod"

k8s_name_prefix = "terraform-alicloud-managed-kubernetes"

k8s_pod_cidr = "172.20.0.0/16"

k8s_service_cidr = "172.21.0.0/20"

kube_config_path = ".kubeconfig"

new_vpc = true

node_password = "YourPassword123"

region = "ap-southeast-1"

reserved_instance_number = 1

resource_group_id = ""

tags = {}

vpc_cidr = "10.1.0.0/21"

vswitch_cidrs = [
  "10.1.1.0/24"
]

vswitch_ids = []

worker_disk_category = "cloud_efficiency"

worker_disk_size = 40

worker_instance_types = [
  "ecs.sn1ne.large",
  "ecs.sn1ne.xlarge",
  "ecs.c6.xlarge"
]

worker_number = 1

zone_ids = [
  "ap-southeast-1a"
]



variable "application_number" {
  type        = number
  description = "The number of the application."
  default     = 1
}

variable "application_path" {
  type        = string
  description = "The application deployment template directory in the application project."
  default     = "applications/appcenter/kustomize/base"
}

variable "application_target_revision" {
  type        = string
  description = "Defines the commit, tag, or branch in which to sync the application to."
  default     = "main"
}

variable "application_url" {
  type        = string
  description = "The url of the application."
  default     = "https://github.com/terraform-alicloud-modules/terraform-alicloud-automation-elastic-landing"
}

variable "apply_reserved_instance" {
  type        = bool
  description = "Whether apply reserved instance to avoid the lack of resource quota"
  default     = false
}

variable "autoscaling_node_bind_eip" {
  type        = bool
  description = "Whether bind an eip address to auto scaling node"
  default     = false
}

variable "autoscaling_node_max_number" {
  type        = number
  description = "The maximize number of autoscaling nodes. Default to 10"
  default     = 10
}

variable "autoscaling_node_min_number" {
  type        = number
  description = "The minimize number of autoscaling nodes. Default to 0"
  default     = 0
}

variable "cluster_addons" {
  type        = list(object({
    name   = string
    config = string
  }))
  description = "Addon components in kubernetes cluster"
  default     = []
}

variable "enable_service" {
  type        = string
  description = "Whether to enable the releated service automatically, including ACK, SLS and CMS."
  default     = "Off"
}

variable "environment" {
  type        = string
  description = "The kubernetes cluster environment label, like daily, pre, prod and so on."
  default     = "prod"
}

variable "k8s_name_prefix" {
  type        = string
  description = "The name prefix used to create managed kubernetes cluster."
  default     = "terraform-alicloud-managed-kubernetes"
}

variable "k8s_pod_cidr" {
  type        = string
  description = "The kubernetes pod cidr block. It cannot be equals to vpc's or vswitch's and cannot be in them. If vpc's cidr block is `172.16.XX.XX/XX`, it had better to `192.168.XX.XX/XX` or `10.XX.XX.XX/XX`."
  default     = "172.20.0.0/16"
}

variable "k8s_service_cidr" {
  type        = string
  description = "The kubernetes service cidr block. It cannot be equals to vpc's or vswitch's or pod's and cannot be in them. Its setting rule is same as `k8s_pod_cidr`."
  default     = "172.21.0.0/20"
}

variable "kube_config_path" {
  type        = string
  description = "The path of kube config, like ~/.kube/config"
  default     = ".kubeconfig"
}

variable "new_vpc" {
  type        = bool
  description = "Create a new vpc for this module."
  default     = true
}

variable "node_password" {
  type        = string
  description = "The password of worker nodes."
  default     = "YourPassword123"
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region id in which kubernetes cluster. Recommand the international region because of needing loading application template from github."
  default     = "ap-southeast-1"
}

variable "reserved_instance_number" {
  type        = number
  description = "The number of reserved instance. It is valid when `apply_reserved_instance` is true."
  default     = 1
}

variable "resource_group_id" {
  type        = string
  description = "The resource group id."
  default     = ""
}

variable "tags" {
  type        = map(string)
  description = "The resource tags"
  default     = {}
}

variable "vpc_cidr" {
  type        = string
  description = "The cidr block used to launch a new vpc."
  default     = "10.1.0.0/21"
}

variable "vswitch_cidrs" {
  type        = list(string)
  description = "List cidr blocks used to create several new vswitches when 'new_vpc' is true."
  default     = [
  "10.1.1.0/24"
]
}

variable "vswitch_ids" {
  type        = list(string)
  description = "List Ids of existing vswitch."
  default     = []
}

variable "worker_disk_category" {
  type        = string
  description = "The system disk category used to launch one or more worker nodes."
  default     = "cloud_efficiency"
}

variable "worker_disk_size" {
  type        = number
  description = "The system disk size used to launch one or more worker nodes."
  default     = 40
}

variable "worker_instance_types" {
  type        = list(string)
  description = "The ecs instance type used to launch worker nodes. If not set, data source `alicloud_instance_types` will return one based on `cpu_core_count` and `memory_size`."
  default     = [
  "ecs.sn1ne.large",
  "ecs.sn1ne.xlarge",
  "ecs.c6.xlarge"
]
}

variable "worker_number" {
  type        = number
  description = "The number of kubernetes cluster work nodes. The value does not contain the auto-scaled nodes."
  default     = 1
}

variable "zone_ids" {
  type        = list(string)
  description = "List available zone ids used to create several new vswitches and kubernetes cluster."
  default     = [
  "ap-southeast-1a"
]
}



availability_zone = ""

description = "tf-rds-to-gpdb-description"

engine = "MySQL"

engine_version = "5.6"

gpdb_engine_version = "6.0"

instance_charge_type = "Postpaid"

instance_spec = "2C16G"

instance_storage = "30"

monitoring_period = "60"

name = "tf-rds-to-gpdb"

payment_type = "PayAsYouGo"

rds_instance_type = "rds.mysql.s3.large"

seg_node_num = 4

seg_storage_type = "cloud_essd"

storage_size = 50

vpc_cidr_block = "192.168.1.0/24"

vs_cidr_block = "192.168.1.0/24"


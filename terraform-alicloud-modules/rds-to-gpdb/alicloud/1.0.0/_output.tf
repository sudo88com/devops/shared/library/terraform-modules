
output "this_db_id" {
  description = ""
  value       = module.rds-to-gpdb.this_db_id
}

output "this_db_name" {
  description = ""
  value       = module.rds-to-gpdb.this_db_name
}

output "this_gpdb_elastic_id" {
  description = ""
  value       = module.rds-to-gpdb.this_gpdb_elastic_id
}


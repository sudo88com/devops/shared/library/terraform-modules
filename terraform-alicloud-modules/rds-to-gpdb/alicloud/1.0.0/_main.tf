
module "rds-to-gpdb" {
  source = "terraform-aws-modules/rds-to-gpdb/aws"
  version = "1.0.0"
  availability_zone = var.availability_zone
  description = var.description
  engine = var.engine
  engine_version = var.engine_version
  gpdb_engine_version = var.gpdb_engine_version
  instance_charge_type = var.instance_charge_type
  instance_spec = var.instance_spec
  instance_storage = var.instance_storage
  monitoring_period = var.monitoring_period
  name = var.name
  payment_type = var.payment_type
  rds_instance_type = var.rds_instance_type
  seg_node_num = var.seg_node_num
  seg_storage_type = var.seg_storage_type
  storage_size = var.storage_size
  vpc_cidr_block = var.vpc_cidr_block
  vs_cidr_block = var.vs_cidr_block
}


variable "availability_zone" {
  type        = string
  description = "The available zone to launch modules."
  default     = ""
}

variable "description" {
  type        = string
  description = "The specification of module description."
  default     = "tf-rds-to-gpdb-description"
}

variable "engine" {
  type        = string
  description = "The specification of the engine."
  default     = "MySQL"
}

variable "engine_version" {
  type        = string
  description = "The specification of the engine version."
  default     = "5.6"
}

variable "gpdb_engine_version" {
  type        = string
  description = "The engine version type of GPDB."
  default     = "6.0"
}

variable "instance_charge_type" {
  type        = string
  description = "The specification of the instance charge type."
  default     = "Postpaid"
}

variable "instance_spec" {
  type        = string
  description = "The instance spec type of GPDB."
  default     = "2C16G"
}

variable "instance_storage" {
  type        = string
  description = "The specification of the instance storage."
  default     = "30"
}

variable "monitoring_period" {
  type        = string
  description = "The specification of the monitoring period."
  default     = "60"
}

variable "name" {
  type        = string
  description = "The specification of module name."
  default     = "tf-rds-to-gpdb"
}

variable "payment_type" {
  type        = string
  description = "The payment type type of GPDB."
  default     = "PayAsYouGo"
}

variable "rds_instance_type" {
  type        = string
  description = "The specification of the rds instance type."
  default     = "rds.mysql.s3.large"
}

variable "seg_node_num" {
  type        = string
  description = "The seg node num type of GPDB."
  default     = 4
}

variable "seg_storage_type" {
  type        = string
  description = "The seg storage type of GPDB."
  default     = "cloud_essd"
}

variable "storage_size" {
  type        = string
  description = "The storage size type of GPDB."
  default     = 50
}

variable "vpc_cidr_block" {
  type        = string
  description = "The cidr block of VPC information."
  default     = "192.168.1.0/24"
}

variable "vs_cidr_block" {
  type        = string
  description = "The cidr block of VSwitch information."
  default     = "192.168.1.0/24"
}


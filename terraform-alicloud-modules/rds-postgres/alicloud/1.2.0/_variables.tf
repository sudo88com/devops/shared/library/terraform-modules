
variable "account_name" {
  type        = string
  description = "Name of a new database account. It should be set when create_account = true."
  default     = ""
}

variable "account_password" {
  type        = string
  description = "Operation database account password. It may consist of letters, digits, or underlines, with a length of 6 to 32 characters."
  default     = ""
}

variable "account_privilege" {
  type        = string
  description = "The privilege of one account access database."
  default     = "DBOwner"
}

variable "account_type" {
  type        = string
  description = "Privilege type of account. Normal: Common privilege. Super: High privilege.Default to Normal."
  default     = "Normal"
}

variable "alarm_rule_contact_groups" {
  type        = list(string)
  description = "List contact groups of the alarm rule, which must have been created on the console."
  default     = []
}

variable "alarm_rule_effective_interval" {
  type        = string
  description = "The interval of effecting alarm rule. It foramt as 'hh:mm-hh:mm', like '0:00-4:00'."
  default     = "0:00-2:00"
}

variable "alarm_rule_name" {
  type        = string
  description = "The alarm rule name."
  default     = ""
}

variable "alarm_rule_operator" {
  type        = string
  description = "Alarm comparison operator. Valid values: ['<=', '<', '>', '>=', '==', '!=']. Default to '=='."
  default     = "=="
}

variable "alarm_rule_period" {
  type        = number
  description = "Index query cycle, which must be consistent with that defined for metrics. Default to 300, in seconds."
  default     = 300
}

variable "alarm_rule_silence_time" {
  type        = number
  description = "Notification silence period in the alarm state, in seconds. Valid value range: [300, 86400]. Default to 86400."
  default     = 86400
}

variable "alarm_rule_statistics" {
  type        = string
  description = "Statistical method. It must be consistent with that defined for metrics. Valid values: ['Average', 'Minimum', 'Maximum']. Default to 'Average'."
  default     = "Average"
}

variable "alarm_rule_threshold" {
  type        = string
  description = "Alarm threshold value, which must be a numeric value currently."
  default     = ""
}

variable "alarm_rule_triggered_count" {
  type        = number
  description = "Number of consecutive times it has been detected that the values exceed the threshold. Default to 3."
  default     = 3
}

variable "allocate_public_connection" {
  type        = bool
  description = "Whether to allocate public connection for a Postgre SQL instance. If true, the connection_prefix can not be empty."
  default     = false
}

variable "backup_retention_period" {
  type        = number
  description = "Instance backup retention days. Valid values: [7-730]. Default to 7."
  default     = 7
}

variable "connection_prefix" {
  type        = string
  description = "Prefix of an Internet connection string. It must be checked for uniqueness. It may consist of lowercase letters, numbers, and underlines, and must start with a letter and have no more than 30 characters."
  default     = ""
}

variable "create_account" {
  type        = bool
  description = "Whether to create a new account. If true, the 'account_name' should not be empty."
  default     = true
}

variable "create_backup_policy" {
  type        = bool
  description = "Whether to create Postgre SQL instance's backup policy."
  default     = true
}

variable "create_database" {
  type        = bool
  description = "Whether to create multiple databases. If true, the 'databases' should not be empty."
  default     = true
}

variable "create_instance" {
  type        = bool
  description = "Whether to create postgre sql instance. If false, you can use a existing Postgre SQL instance by setting 'existing_instance_id'."
  default     = true
}

variable "databases" {
  type        = list(map(string))
  description = "A list mapping used to add multiple databases. Each item supports keys: name, character_set and description. It should be set when create_database = true."
  default     = []
}

variable "enable_alarm_rule" {
  type        = bool
  description = "Whether to enable alarm rule. Default to true."
  default     = true
}

variable "enable_backup_log" {
  type        = bool
  description = "Whether to backup instance log. Default to true."
  default     = false
}

variable "engine_version" {
  type        = string
  description = "PostgresSQL database version. Valid values: '10', '11', '12'."
  default     = ""
}

variable "existing_instance_id" {
  type        = string
  description = "The Id of an existing postgre sql instance. If set, the 'create_instance' will be ignored."
  default     = ""
}

variable "instance_charge_type" {
  type        = string
  description = "The instance charge type. Valid values: Prepaid and Postpaid. Default to Postpaid."
  default     = "Postpaid"
}

variable "instance_name" {
  type        = string
  description = "The name of Postgre SQL Instance. A name with 'tf-module-postgres' will be set if it is empty."
  default     = "tf-module-postgres"
}

variable "instance_storage" {
  type        = number
  description = "The storage capacity of the instance. Unit: GB. The storage capacity increases at increments of 5 GB. For more information, see [Instance Types](https://www.alibabacloud.com/help/doc-detail/26312.htm)."
  default     = 20
}

variable "instance_type" {
  type        = string
  description = "Postgres Instance type, for example: rds.pg.s1.small. full list is : https://www.alibabacloud.com/help/zh/doc-detail/26312.htm"
  default     = ""
}

variable "log_backup_retention_period" {
  type        = number
  description = "Instance log backup retention days. Valid values: [7-730]. Default to 7. It can be larger than 'retention_period'."
  default     = 7
}

variable "period" {
  type        = number
  description = "The duration that you will buy Postgres Instance (in month). It is valid when instance_charge_type is PrePaid. Valid values: [1~9], 12, 24, 36. Default to 1"
  default     = 1
}

variable "port" {
  type        = number
  description = " Internet connection port. Valid value: [3001-3999]. Default to 3306."
  default     = 3306
}

variable "preferred_backup_period" {
  type        = list(string)
  description = "Postgre SQL Instance backup period."
  default     = []
}

variable "preferred_backup_time" {
  type        = string
  description = " Postgre SQL Instance backup time, in the format of HH:mmZ- HH:mmZ."
  default     = "02:00Z-03:00Z"
}

variable "profile" {
  type        = string
  description = "(Deprecated from version 1.1.0)The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0)The region used to launch this module resources."
  default     = ""
}

variable "security_group_ids" {
  type        = list(string)
  description = "List of VPC security group ids to associate with postgre sql instance."
  default     = []
}

variable "security_ips" {
  type        = list(string)
  description = " List of IP addresses allowed to access all databases of an instance. The list contains up to 1,000 IP addresses, separated by commas. Supported formats include 0.0.0.0/0, 10.23.12.24 (IP), and 10.23.12.24/24 (Classless Inter-Domain Routing (CIDR) mode. /24 represents the length of the prefix in an IP address. The range of the prefix length is [1,32])."
  default     = []
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from version 1.1.0)This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = bool
  description = "(Deprecated from version 1.1.0)Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "ssl_action" {
  type        = string
  description = "Actions performed on SSL functions, Valid values: Open: turn on SSL encryption; Close: turn off SSL encryption; Update: update SSL certificate. "
  default     = "Close"
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the postgre sql."
  default     = {}
}

variable "vswitch_id" {
  type        = string
  description = "The virtual switch ID to launch Postgre SQL Instances in one VPC."
  default     = ""
}


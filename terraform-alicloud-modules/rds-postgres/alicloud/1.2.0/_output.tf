
output "this_alarm_rule_connection_usage_status" {
  description = "The current alarm memory usage rule status."
  value       = module.rds-postgres.this_alarm_rule_connection_usage_status
}

output "this_alarm_rule_contact_groups" {
  description = "List contact groups of the alarm rule."
  value       = module.rds-postgres.this_alarm_rule_contact_groups
}

output "this_alarm_rule_cpu_usage_status" {
  description = "The current alarm cpu usage rule status."
  value       = module.rds-postgres.this_alarm_rule_cpu_usage_status
}

output "this_alarm_rule_dimensions" {
  description = "Map of the resources associated with the alarm rule."
  value       = module.rds-postgres.this_alarm_rule_dimensions
}

output "this_alarm_rule_disk_usage_status" {
  description = "The current alarm disk usage rule status."
  value       = module.rds-postgres.this_alarm_rule_disk_usage_status
}

output "this_alarm_rule_effective_interval" {
  description = "The interval of effecting alarm rule."
  value       = module.rds-postgres.this_alarm_rule_effective_interval
}

output "this_alarm_rule_enabled" {
  description = "Whether to enable alarm rule."
  value       = module.rds-postgres.this_alarm_rule_enabled
}

output "this_alarm_rule_id" {
  description = "The ID of the alarm rule."
  value       = module.rds-postgres.this_alarm_rule_id
}

output "this_alarm_rule_metric" {
  description = "Name of the monitoring metrics."
  value       = module.rds-postgres.this_alarm_rule_metric
}

output "this_alarm_rule_name" {
  description = "The alarm name."
  value       = module.rds-postgres.this_alarm_rule_name
}

output "this_alarm_rule_notify_type" {
  description = "Notification type."
  value       = module.rds-postgres.this_alarm_rule_notify_type
}

output "this_alarm_rule_operator" {
  description = "Alarm comparison operator."
  value       = module.rds-postgres.this_alarm_rule_operator
}

output "this_alarm_rule_period" {
  description = "Index query cycle."
  value       = module.rds-postgres.this_alarm_rule_period
}

output "this_alarm_rule_project" {
  description = "Monitor project name."
  value       = module.rds-postgres.this_alarm_rule_project
}

output "this_alarm_rule_silence_time" {
  description = " Notification silence period in the alarm state."
  value       = module.rds-postgres.this_alarm_rule_silence_time
}

output "this_alarm_rule_statistics" {
  description = "Statistical method."
  value       = module.rds-postgres.this_alarm_rule_statistics
}

output "this_alarm_rule_threshold" {
  description = "Alarm threshold value."
  value       = module.rds-postgres.this_alarm_rule_threshold
}

output "this_alarm_rule_triggered_count" {
  description = "Number of trigger alarm."
  value       = module.rds-postgres.this_alarm_rule_triggered_count
}

output "this_alarm_rule_webhook" {
  description = "The webhook that is called when the alarm is triggered."
  value       = module.rds-postgres.this_alarm_rule_webhook
}

output "this_db_database_account" {
  description = "Postgre SQL database account."
  value       = module.rds-postgres.this_db_database_account
}

output "this_db_database_account_privilege" {
  description = "Postgre SQL database account privilege."
  value       = module.rds-postgres.this_db_database_account_privilege
}

output "this_db_database_account_type" {
  description = "Postgre SQL database account type."
  value       = module.rds-postgres.this_db_database_account_type
}

output "this_db_database_description" {
  description = "Postgre SQL database description."
  value       = module.rds-postgres.this_db_database_description
}

output "this_db_database_id" {
  description = "Postgre SQL database id."
  value       = module.rds-postgres.this_db_database_id
}

output "this_db_database_name" {
  description = "Postgre SQL database name."
  value       = module.rds-postgres.this_db_database_name
}

output "this_db_instance_charge_type" {
  description = "Postgres instance charge type."
  value       = module.rds-postgres.this_db_instance_charge_type
}

output "this_db_instance_connection_ip_address" {
  description = "Postgre SQL instance public connection string's ip address."
  value       = module.rds-postgres.this_db_instance_connection_ip_address
}

output "this_db_instance_connection_string" {
  description = "Postgre SQL instance public connection string."
  value       = module.rds-postgres.this_db_instance_connection_string
}

output "this_db_instance_engine" {
  description = "Postgres instance engine."
  value       = module.rds-postgres.this_db_instance_engine
}

output "this_db_instance_engine_version" {
  description = "Postgres instance engine version."
  value       = module.rds-postgres.this_db_instance_engine_version
}

output "this_db_instance_id" {
  description = "Postgres instance id."
  value       = module.rds-postgres.this_db_instance_id
}

output "this_db_instance_name" {
  description = "Postgres instance name."
  value       = module.rds-postgres.this_db_instance_name
}

output "this_db_instance_period" {
  description = "Postgres instance charge period when Prepaid."
  value       = module.rds-postgres.this_db_instance_period
}

output "this_db_instance_port" {
  description = "Postgre SQL instance public connection string."
  value       = module.rds-postgres.this_db_instance_port
}

output "this_db_instance_security_group_ids" {
  description = "The security group ids in which the postgres instance."
  value       = module.rds-postgres.this_db_instance_security_group_ids
}

output "this_db_instance_security_ips" {
  description = "Postgres instance security ip list."
  value       = module.rds-postgres.this_db_instance_security_ips
}

output "this_db_instance_storage" {
  description = "Postgres instance storage."
  value       = module.rds-postgres.this_db_instance_storage
}

output "this_db_instance_tags" {
  description = "Postgres instance tags."
  value       = module.rds-postgres.this_db_instance_tags
}

output "this_db_instance_type" {
  description = "Postgres instance type."
  value       = module.rds-postgres.this_db_instance_type
}

output "this_db_instance_vswitch_id" {
  description = "The vswitch id in which the postgres instance."
  value       = module.rds-postgres.this_db_instance_vswitch_id
}

output "this_db_instance_zone_id" {
  description = "The zone id in which the postgres instance."
  value       = module.rds-postgres.this_db_instance_zone_id
}



module "ecs-rds" {
  source = "terraform-aws-modules/ecs-rds/aws"
  version = "1.0.0"
  availability_zone = var.availability_zone
  category = var.category
  description = var.description
  ecs_size = var.ecs_size
  engine_version = var.engine_version
  image_id = var.image_id
  instance_charge_type = var.instance_charge_type
  instance_storage = var.instance_storage
  instance_type = var.instance_type
  internet_max_bandwidth_out = var.internet_max_bandwidth_out
  monitoring_period = var.monitoring_period
  name = var.name
  rds_instance_type = var.rds_instance_type
  security_group_ids = var.security_group_ids
  system_disk_category = var.system_disk_category
  system_disk_description = var.system_disk_description
  system_disk_name = var.system_disk_name
  vswitch_id = var.vswitch_id
}

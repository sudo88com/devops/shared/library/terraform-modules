
data_node_amount = 2

data_node_disk_performance_level = null

data_node_disk_size = 20

data_node_disk_type = "cloud_efficiency"

data_node_spec = "elasticsearch.sn1ne.large"

description = ""

es_version = "5.5.3_with_X-Pack"

instance_charge_type = "PostPaid"

kibana_node_spec = null

kibana_whitelist = [
  "0.0.0.0/0"
]

master_node_spec = "elasticsearch.sn2ne.large"

number_of_instance = 1

password = ""

period = 1

private_whitelist = [
  "0.0.0.0/0"
]

region = ""

vswitch_id = ""



variable "data_node_amount" {
  type        = number
  description = "The Elasticsearch cluster's data node quantity, between 2 and 50."
  default     = 2
}

variable "data_node_disk_performance_level" {
  type        = string
  description = "Cloud disk performance level. Valid values are `PL0`, `PL1`, `PL2`, `PL3`. The `data_node_disk_type` muse be `cloud_essd`."
  default     = null
}

variable "data_node_disk_size" {
  type        = number
  description = "The single data node storage space."
  default     = 20
}

variable "data_node_disk_type" {
  type        = string
  description = "The data node disk type. Supported values: cloud_ssd, cloud_efficiency."
  default     = "cloud_efficiency"
}

variable "data_node_spec" {
  type        = string
  description = "The data node specifications of the Elasticsearch instance."
  default     = "elasticsearch.sn1ne.large"
}

variable "description" {
  type        = string
  description = "The description of the Elasticsearch instance."
  default     = ""
}

variable "es_version" {
  type        = string
  description = "Elasticsearch version. Supported values: 5.5.3_with_X-Pack and 6.3_with_X-Pack."
  default     = "5.5.3_with_X-Pack"
}

variable "instance_charge_type" {
  type        = string
  description = "Valid values are PrePaid, PostPaid. Default to PostPaid"
  default     = "PostPaid"
}

variable "kibana_node_spec" {
  type        = string
  description = "The kibana node specifications of the Elasticsearch instance."
  default     = null
}

variable "kibana_whitelist" {
  type        = list(string)
  description = "Set the Kibana's IP whitelist in internet network."
  default     = [
  "0.0.0.0/0"
]
}

variable "master_node_spec" {
  type        = string
  description = "The master node specifications of the Elasticsearch instance."
  default     = "elasticsearch.sn2ne.large"
}

variable "number_of_instance" {
  type        = number
  description = "Instance count"
  default     = 1
}

variable "password" {
  type        = string
  description = "The password of the instance."
  default     = ""
}

variable "period" {
  type        = number
  description = "The duration that you will buy Elasticsearch instance (in month). It is valid when instance_charge_type is PrePaid. Valid values: [1~9], 12, 24, 36."
  default     = 1
}

variable "private_whitelist" {
  type        = list(string)
  description = "Set the instance's IP whitelist in VPC network."
  default     = [
  "0.0.0.0/0"
]
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.2.0) The region used to launch this module resources."
  default     = ""
}

variable "vswitch_id" {
  type        = string
  description = "The ID of VSwitch."
  default     = ""
}



locals {
  data_node_amount = var.data_node_amount
  data_node_disk_performance_level = var.data_node_disk_performance_level
  data_node_disk_size = var.data_node_disk_size
  data_node_disk_type = var.data_node_disk_type
  data_node_spec = var.data_node_spec
  description = var.description
  es_version = var.es_version
  instance_charge_type = var.instance_charge_type
  kibana_node_spec = var.kibana_node_spec
  kibana_whitelist = var.kibana_whitelist
  master_node_spec = var.master_node_spec
  number_of_instance = var.number_of_instance
  password = var.password
  period = var.period
  private_whitelist = var.private_whitelist
  region = var.region
  vswitch_id = var.vswitch_id
}

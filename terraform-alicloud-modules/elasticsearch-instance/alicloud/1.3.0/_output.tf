
output "number_of_instance" {
  description = ""
  value       = module.elasticsearch-instance.number_of_instance
}

output "this_data_node_amount" {
  description = ""
  value       = module.elasticsearch-instance.this_data_node_amount
}

output "this_data_node_disk_size" {
  description = ""
  value       = module.elasticsearch-instance.this_data_node_disk_size
}

output "this_data_node_disk_type" {
  description = ""
  value       = module.elasticsearch-instance.this_data_node_disk_type
}

output "this_data_node_spec" {
  description = ""
  value       = module.elasticsearch-instance.this_data_node_spec
}

output "this_description" {
  description = ""
  value       = module.elasticsearch-instance.this_description
}

output "this_elasticsearch_ids" {
  description = ""
  value       = module.elasticsearch-instance.this_elasticsearch_ids
}

output "this_es_version" {
  description = ""
  value       = module.elasticsearch-instance.this_es_version
}

output "this_instance_charge_type" {
  description = ""
  value       = module.elasticsearch-instance.this_instance_charge_type
}

output "this_kibana_whitelist" {
  description = ""
  value       = module.elasticsearch-instance.this_kibana_whitelist
}

output "this_master_node_spec" {
  description = ""
  value       = module.elasticsearch-instance.this_master_node_spec
}

output "this_password" {
  description = ""
  value       = module.elasticsearch-instance.this_password
}

output "this_period" {
  description = ""
  value       = module.elasticsearch-instance.this_period
}

output "this_private_whitelist" {
  description = ""
  value       = module.elasticsearch-instance.this_private_whitelist
}

output "this_vswitch_id" {
  description = ""
  value       = module.elasticsearch-instance.this_vswitch_id
}


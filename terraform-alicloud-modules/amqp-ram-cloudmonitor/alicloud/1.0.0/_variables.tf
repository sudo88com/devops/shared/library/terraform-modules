
variable "alarm_exchange_tps_in_metric" {
  type        = string
  description = "Name of the monitoring metrics corresponding to a project, such as 'CPUUtilization' and so on. "
  default     = "ExchangeTPSIn"
}

variable "alarm_exchange_tps_out_metric" {
  type        = string
  description = "Name of the monitoring metrics corresponding to a project, such as 'CPUUtilization' and so on. "
  default     = "instance_disk_capacity"
}

variable "alarm_instance_message_input_metric" {
  type        = string
  description = "Name of the monitoring metrics corresponding to a project, such as 'CPUUtilization' and so on. "
  default     = "InstanceMessageInput"
}

variable "alarm_instance_message_output_metric" {
  type        = string
  description = "Name of the monitoring metrics corresponding to a project, such as 'CPUUtilization' and so on. "
  default     = "InstanceMessageOutput"
}

variable "alarm_user_id" {
  type        = string
  description = "The alarm of userId."
  default     = "test"
}

variable "amqp_exchang_internal" {
  type        = bool
  description = "The specification of amqp exchange internal."
  default     = false
}

variable "amqp_exchange_auto_delete_state" {
  type        = bool
  description = "The specification of amqp exchange is auto delete state."
  default     = false
}

variable "amqp_exchange_name" {
  type        = string
  description = "The specification of amqp exchange name."
  default     = "my-Exchange"
}

variable "amqp_exchange_type" {
  type        = string
  description = "The specification of amqp exchange type."
  default     = "DIRECT"
}

variable "amqp_instance_name" {
  type        = string
  description = "The specification of amqp instance name."
  default     = "test_amqp_instance"
}

variable "amqp_instance_type" {
  type        = string
  description = "The specification of amqp instance type."
  default     = "professional"
}

variable "amqp_max_eip_tps" {
  type        = number
  description = "The specification of amqp max eip tps."
  default     = 128
}

variable "amqp_max_tps" {
  type        = number
  description = "The specification of amqp max tps."
  default     = 1000
}

variable "amqp_payment_type" {
  type        = string
  description = "The specification of amqp payment type."
  default     = "Subscription"
}

variable "amqp_period" {
  type        = number
  description = "The specification of amqp period."
  default     = 1
}

variable "amqp_queue_capacity" {
  type        = number
  description = "The specification of amqp queue capacity."
  default     = 50
}

variable "amqp_queue_name" {
  type        = string
  description = "The specification of amqp queue name."
  default     = "my-Queue"
}

variable "amqp_support_eip" {
  type        = bool
  description = "The specification of amqp is  support eip."
  default     = true
}

variable "amqp_virtual_host_name" {
  type        = string
  description = "The specification of amqp virtual host name."
  default     = "my-VirtualHost"
}

variable "create_ram_access_key" {
  type        = bool
  description = "The specification of the create ram access key."
  default     = true
}

variable "create_ram_user_login_profile" {
  type        = bool
  description = "The specification of the ram user login profile."
  default     = true
}

variable "exchange_tps_in_alarm_rule_contact_groups" {
  type        = list(string)
  description = "List contact groups of the alarm rule, which must have been created on the console. "
  default     = [
  "test-web-server"
]
}

variable "exchange_tps_in_alarm_rule_effective_interval" {
  type        = string
  description = "The interval of effecting alarm rule. It foramt as 'hh:mm-hh:mm', like '0:00-4:00'."
  default     = "0:00-2:00"
}

variable "exchange_tps_in_alarm_rule_name" {
  type        = string
  description = "The alarm rule name. "
  default     = "exchange_tps_in_test_rule_name"
}

variable "exchange_tps_in_alarm_rule_operator" {
  type        = string
  description = "Alarm comparison operator. Valid values: ['<=', '<', '>', '>=', '==', '!=']. Default to '=='. "
  default     = "\u003e="
}

variable "exchange_tps_in_alarm_rule_period" {
  type        = number
  description = "Index query cycle, which must be consistent with that defined for metrics. Default to 300, in seconds. "
  default     = 300
}

variable "exchange_tps_in_alarm_rule_silence_time" {
  type        = number
  description = "Notification silence period in the alarm state, in seconds. Valid value range: [300, 86400]. Default to 86400. "
  default     = 86400
}

variable "exchange_tps_in_alarm_rule_statistics" {
  type        = string
  description = "Statistical method. It must be consistent with that defined for metrics. Valid values: ['Average', 'Minimum', 'Maximum']. Default to 'Average'. "
  default     = "Value"
}

variable "exchange_tps_in_alarm_rule_threshold" {
  type        = string
  description = "Alarm threshold value, which must be a numeric value currently. "
  default     = "90"
}

variable "exchange_tps_in_enable_alarm_rule" {
  type        = bool
  description = "Whether to enable alarm rule. Default to true. "
  default     = true
}

variable "exchange_tps_in_times" {
  type        = string
  description = "Alarm times value, which must be a numeric value currently. "
  default     = "3"
}

variable "exchange_tps_out_alarm_rule_contact_groups" {
  type        = list(string)
  description = "List contact groups of the alarm rule, which must have been created on the console. "
  default     = [
  "test-web-server"
]
}

variable "exchange_tps_out_alarm_rule_effective_interval" {
  type        = string
  description = "The interval of effecting alarm rule. It foramt as 'hh:mm-hh:mm', like '0:00-4:00'."
  default     = "0:00-2:00"
}

variable "exchange_tps_out_alarm_rule_name" {
  type        = string
  description = "The alarm rule name. "
  default     = "exchange_tps_out_test_rule_name"
}

variable "exchange_tps_out_alarm_rule_operator" {
  type        = string
  description = "Alarm comparison operator. Valid values: ['<=', '<', '>', '>=', '==', '!=']. Default to '=='. "
  default     = "\u003e="
}

variable "exchange_tps_out_alarm_rule_period" {
  type        = number
  description = "Index query cycle, which must be consistent with that defined for metrics. Default to 300, in seconds. "
  default     = 300
}

variable "exchange_tps_out_alarm_rule_silence_time" {
  type        = number
  description = "Notification silence period in the alarm state, in seconds. Valid value range: [300, 86400]. Default to 86400. "
  default     = 86400
}

variable "exchange_tps_out_alarm_rule_statistics" {
  type        = string
  description = "Statistical method. It must be consistent with that defined for metrics. Valid values: ['Average', 'Minimum', 'Maximum']. Default to 'Average'. "
  default     = "Maximum"
}

variable "exchange_tps_out_alarm_rule_threshold" {
  type        = string
  description = "Alarm threshold value, which must be a numeric value currently. "
  default     = "90"
}

variable "exchange_tps_out_enable_alarm_rule" {
  type        = bool
  description = "Whether to enable alarm rule. Default to true. "
  default     = true
}

variable "exchange_tps_out_times" {
  type        = string
  description = "Alarm times value, which must be a numeric value currently. "
  default     = "3"
}

variable "instance_message_input_alarm_rule_contact_groups" {
  type        = list(string)
  description = "List contact groups of the alarm rule, which must have been created on the console. "
  default     = [
  "test-web-server"
]
}

variable "instance_message_input_alarm_rule_effective_interval" {
  type        = string
  description = "The interval of effecting alarm rule. It foramt as 'hh:mm-hh:mm', like '0:00-4:00'."
  default     = "0:00-2:00"
}

variable "instance_message_input_alarm_rule_name" {
  type        = string
  description = "The alarm rule name. "
  default     = "instance_message_input_test_rule_name"
}

variable "instance_message_input_alarm_rule_operator" {
  type        = string
  description = "Alarm comparison operator. Valid values: ['<=', '<', '>', '>=', '==', '!=']. Default to '=='. "
  default     = "\u003e="
}

variable "instance_message_input_alarm_rule_period" {
  type        = number
  description = "Index query cycle, which must be consistent with that defined for metrics. Default to 300, in seconds. "
  default     = 300
}

variable "instance_message_input_alarm_rule_silence_time" {
  type        = number
  description = "Notification silence period in the alarm state, in seconds. Valid value range: [300, 86400]. Default to 86400. "
  default     = 86400
}

variable "instance_message_input_alarm_rule_statistics" {
  type        = string
  description = "Statistical method. It must be consistent with that defined for metrics. Valid values: ['Average', 'Minimum', 'Maximum']. Default to 'Average'. "
  default     = "Sum"
}

variable "instance_message_input_alarm_rule_threshold" {
  type        = string
  description = "Alarm threshold value, which must be a numeric value currently. "
  default     = "90"
}

variable "instance_message_input_enable_alarm_rule" {
  type        = bool
  description = "Whether to enable alarm rule. Default to true. "
  default     = true
}

variable "instance_message_input_times" {
  type        = string
  description = "Alarm times value, which must be a numeric value currently. "
  default     = "3"
}

variable "instance_message_output_alarm_rule_contact_groups" {
  type        = list(string)
  description = "List contact groups of the alarm rule, which must have been created on the console. "
  default     = [
  "test-web-server"
]
}

variable "instance_message_output_alarm_rule_effective_interval" {
  type        = string
  description = "The interval of effecting alarm rule. It foramt as 'hh:mm-hh:mm', like '0:00-4:00'."
  default     = "0:00-2:00"
}

variable "instance_message_output_alarm_rule_name" {
  type        = string
  description = "The alarm rule name. "
  default     = "disk_capacity_test_rule_name"
}

variable "instance_message_output_alarm_rule_operator" {
  type        = string
  description = "Alarm comparison operator. Valid values: ['<=', '<', '>', '>=', '==', '!=']. Default to '=='. "
  default     = "\u003e="
}

variable "instance_message_output_alarm_rule_period" {
  type        = number
  description = "Index query cycle, which must be consistent with that defined for metrics. Default to 300, in seconds. "
  default     = 300
}

variable "instance_message_output_alarm_rule_silence_time" {
  type        = number
  description = "Notification silence period in the alarm state, in seconds. Valid value range: [300, 86400]. Default to 86400. "
  default     = 86400
}

variable "instance_message_output_alarm_rule_statistics" {
  type        = string
  description = "Statistical method. It must be consistent with that defined for metrics. Valid values: ['Average', 'Minimum', 'Maximum']. Default to 'Average'. "
  default     = "Sum"
}

variable "instance_message_output_alarm_rule_threshold" {
  type        = string
  description = "Alarm threshold value, which must be a numeric value currently. "
  default     = "90"
}

variable "instance_message_output_enable_alarm_rule" {
  type        = bool
  description = "Whether to enable alarm rule. Default to true. "
  default     = true
}

variable "instance_message_output_times" {
  type        = string
  description = "Alarm times value, which must be a numeric value currently. "
  default     = "3"
}

variable "is_admin" {
  type        = bool
  description = "The specification of the ram user is admin."
  default     = true
}

variable "queue_message_accumulation_alarm_rule_contact_groups" {
  type        = list(string)
  description = "List contact groups of the alarm rule, which must have been created on the console. "
  default     = [
  "test-web-server"
]
}

variable "queue_message_accumulation_alarm_rule_effective_interval" {
  type        = string
  description = "The interval of effecting alarm rule. It foramt as 'hh:mm-hh:mm', like '0:00-4:00'."
  default     = "0:00-2:00"
}

variable "queue_message_accumulation_alarm_rule_name" {
  type        = string
  description = "The alarm rule name. "
  default     = "queue_message_accumulation_test_rule_name"
}

variable "queue_message_accumulation_alarm_rule_operator" {
  type        = string
  description = "Alarm comparison operator. Valid values: ['<=', '<', '>', '>=', '==', '!=']. Default to '=='. "
  default     = "\u003e="
}

variable "queue_message_accumulation_alarm_rule_period" {
  type        = number
  description = "Index query cycle, which must be consistent with that defined for metrics. Default to 300, in seconds. "
  default     = 300
}

variable "queue_message_accumulation_alarm_rule_silence_time" {
  type        = number
  description = "Notification silence period in the alarm state, in seconds. Valid value range: [300, 86400]. Default to 86400. "
  default     = 86400
}

variable "queue_message_accumulation_alarm_rule_statistics" {
  type        = string
  description = "Statistical method. It must be consistent with that defined for metrics. Valid values: ['Average', 'Minimum', 'Maximum']. Default to 'Average'. "
  default     = "Maximum"
}

variable "queue_message_accumulation_alarm_rule_threshold" {
  type        = string
  description = "Alarm threshold value, which must be a numeric value currently. "
  default     = "90"
}

variable "queue_message_accumulation_enable_alarm_rule" {
  type        = bool
  description = "Whether to enable alarm rule. Default to true. "
  default     = true
}

variable "queue_message_accumulation_metric" {
  type        = string
  description = "Name of the monitoring metrics corresponding to a project, such as 'CPUUtilization' and so on. "
  default     = "QueueMessageAccumulation"
}

variable "queue_message_accumulation_times" {
  type        = string
  description = "Alarm times value, which must be a numeric value currently. "
  default     = "3"
}

variable "ram_user_name" {
  type        = string
  description = "The ram user of name."
  default     = "terraform_test1"
}

variable "ram_user_password" {
  type        = string
  description = "The ram user of password."
  default     = "password123!Ab"
}

variable "region_id" {
  type        = string
  description = "The specification of regionId."
  default     = "cn-hangzhou"
}


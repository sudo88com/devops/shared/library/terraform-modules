
alarm_exchange_tps_in_metric = "ExchangeTPSIn"

alarm_exchange_tps_out_metric = "instance_disk_capacity"

alarm_instance_message_input_metric = "InstanceMessageInput"

alarm_instance_message_output_metric = "InstanceMessageOutput"

alarm_user_id = "test"

amqp_exchang_internal = false

amqp_exchange_auto_delete_state = false

amqp_exchange_name = "my-Exchange"

amqp_exchange_type = "DIRECT"

amqp_instance_name = "test_amqp_instance"

amqp_instance_type = "professional"

amqp_max_eip_tps = 128

amqp_max_tps = 1000

amqp_payment_type = "Subscription"

amqp_period = 1

amqp_queue_capacity = 50

amqp_queue_name = "my-Queue"

amqp_support_eip = true

amqp_virtual_host_name = "my-VirtualHost"

create_ram_access_key = true

create_ram_user_login_profile = true

exchange_tps_in_alarm_rule_contact_groups = [
  "test-web-server"
]

exchange_tps_in_alarm_rule_effective_interval = "0:00-2:00"

exchange_tps_in_alarm_rule_name = "exchange_tps_in_test_rule_name"

exchange_tps_in_alarm_rule_operator = "\u003e="

exchange_tps_in_alarm_rule_period = 300

exchange_tps_in_alarm_rule_silence_time = 86400

exchange_tps_in_alarm_rule_statistics = "Value"

exchange_tps_in_alarm_rule_threshold = "90"

exchange_tps_in_enable_alarm_rule = true

exchange_tps_in_times = "3"

exchange_tps_out_alarm_rule_contact_groups = [
  "test-web-server"
]

exchange_tps_out_alarm_rule_effective_interval = "0:00-2:00"

exchange_tps_out_alarm_rule_name = "exchange_tps_out_test_rule_name"

exchange_tps_out_alarm_rule_operator = "\u003e="

exchange_tps_out_alarm_rule_period = 300

exchange_tps_out_alarm_rule_silence_time = 86400

exchange_tps_out_alarm_rule_statistics = "Maximum"

exchange_tps_out_alarm_rule_threshold = "90"

exchange_tps_out_enable_alarm_rule = true

exchange_tps_out_times = "3"

instance_message_input_alarm_rule_contact_groups = [
  "test-web-server"
]

instance_message_input_alarm_rule_effective_interval = "0:00-2:00"

instance_message_input_alarm_rule_name = "instance_message_input_test_rule_name"

instance_message_input_alarm_rule_operator = "\u003e="

instance_message_input_alarm_rule_period = 300

instance_message_input_alarm_rule_silence_time = 86400

instance_message_input_alarm_rule_statistics = "Sum"

instance_message_input_alarm_rule_threshold = "90"

instance_message_input_enable_alarm_rule = true

instance_message_input_times = "3"

instance_message_output_alarm_rule_contact_groups = [
  "test-web-server"
]

instance_message_output_alarm_rule_effective_interval = "0:00-2:00"

instance_message_output_alarm_rule_name = "disk_capacity_test_rule_name"

instance_message_output_alarm_rule_operator = "\u003e="

instance_message_output_alarm_rule_period = 300

instance_message_output_alarm_rule_silence_time = 86400

instance_message_output_alarm_rule_statistics = "Sum"

instance_message_output_alarm_rule_threshold = "90"

instance_message_output_enable_alarm_rule = true

instance_message_output_times = "3"

is_admin = true

queue_message_accumulation_alarm_rule_contact_groups = [
  "test-web-server"
]

queue_message_accumulation_alarm_rule_effective_interval = "0:00-2:00"

queue_message_accumulation_alarm_rule_name = "queue_message_accumulation_test_rule_name"

queue_message_accumulation_alarm_rule_operator = "\u003e="

queue_message_accumulation_alarm_rule_period = 300

queue_message_accumulation_alarm_rule_silence_time = 86400

queue_message_accumulation_alarm_rule_statistics = "Maximum"

queue_message_accumulation_alarm_rule_threshold = "90"

queue_message_accumulation_enable_alarm_rule = true

queue_message_accumulation_metric = "QueueMessageAccumulation"

queue_message_accumulation_times = "3"

ram_user_name = "terraform_test1"

ram_user_password = "password123!Ab"

region_id = "cn-hangzhou"



output "alicloud_amqp_exchange_id" {
  description = ""
  value       = module.amqp-ram-cloudmonitor.alicloud_amqp_exchange_id
}

output "alicloud_amqp_instance_id" {
  description = ""
  value       = module.amqp-ram-cloudmonitor.alicloud_amqp_instance_id
}

output "alicloud_amqp_queue_id" {
  description = ""
  value       = module.amqp-ram-cloudmonitor.alicloud_amqp_queue_id
}

output "alicloud_amqp_virtual_host_id" {
  description = ""
  value       = module.amqp-ram-cloudmonitor.alicloud_amqp_virtual_host_id
}

output "queue_message_accumulation_alarm_rule_contact_groups" {
  description = "List contact groups of the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.queue_message_accumulation_alarm_rule_contact_groups
}

output "queue_message_accumulation_alarm_rule_dimensions" {
  description = "Map of the resources associated with the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.queue_message_accumulation_alarm_rule_dimensions
}

output "queue_message_accumulation_alarm_rule_effective_interval" {
  description = "The interval of effecting alarm rule. "
  value       = module.amqp-ram-cloudmonitor.queue_message_accumulation_alarm_rule_effective_interval
}

output "queue_message_accumulation_alarm_rule_enabled" {
  description = "Whether to enable alarm rule. "
  value       = module.amqp-ram-cloudmonitor.queue_message_accumulation_alarm_rule_enabled
}

output "queue_message_accumulation_alarm_rule_id" {
  description = "The ID of the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.queue_message_accumulation_alarm_rule_id
}

output "queue_message_accumulation_alarm_rule_metric" {
  description = "Name of the monitoring metrics. "
  value       = module.amqp-ram-cloudmonitor.queue_message_accumulation_alarm_rule_metric
}

output "queue_message_accumulation_alarm_rule_name" {
  description = "The alarm name. "
  value       = module.amqp-ram-cloudmonitor.queue_message_accumulation_alarm_rule_name
}

output "queue_message_accumulation_alarm_rule_operator" {
  description = "Alarm comparison operator. "
  value       = module.amqp-ram-cloudmonitor.queue_message_accumulation_alarm_rule_operator
}

output "queue_message_accumulation_alarm_rule_period" {
  description = "Index query cycle. "
  value       = module.amqp-ram-cloudmonitor.queue_message_accumulation_alarm_rule_period
}

output "queue_message_accumulation_alarm_rule_project" {
  description = "Monitor project name. "
  value       = module.amqp-ram-cloudmonitor.queue_message_accumulation_alarm_rule_project
}

output "queue_message_accumulation_alarm_rule_silence_time" {
  description = " Notification silence period in the alarm state. "
  value       = module.amqp-ram-cloudmonitor.queue_message_accumulation_alarm_rule_silence_time
}

output "queue_message_accumulation_alarm_rule_statistics" {
  description = "Statistical method. "
  value       = module.amqp-ram-cloudmonitor.queue_message_accumulation_alarm_rule_statistics
}

output "queue_message_accumulation_alarm_rule_threshold" {
  description = "Alarm threshold value."
  value       = module.amqp-ram-cloudmonitor.queue_message_accumulation_alarm_rule_threshold
}

output "queue_message_accumulation_alarm_rule_triggered_count" {
  description = "Number of trigger alarm. "
  value       = module.amqp-ram-cloudmonitor.queue_message_accumulation_alarm_rule_triggered_count
}

output "this_exchange_tps_in_alarm_rule_contact_groups" {
  description = "List contact groups of the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_in_alarm_rule_contact_groups
}

output "this_exchange_tps_in_alarm_rule_dimensions" {
  description = "Map of the resources associated with the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_in_alarm_rule_dimensions
}

output "this_exchange_tps_in_alarm_rule_effective_interval" {
  description = "The interval of effecting alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_in_alarm_rule_effective_interval
}

output "this_exchange_tps_in_alarm_rule_enabled" {
  description = "Whether to enable alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_in_alarm_rule_enabled
}

output "this_exchange_tps_in_alarm_rule_id" {
  description = "The ID of the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_in_alarm_rule_id
}

output "this_exchange_tps_in_alarm_rule_metric" {
  description = "Name of the monitoring metrics. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_in_alarm_rule_metric
}

output "this_exchange_tps_in_alarm_rule_name" {
  description = "The alarm name. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_in_alarm_rule_name
}

output "this_exchange_tps_in_alarm_rule_operator" {
  description = "Alarm comparison operator. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_in_alarm_rule_operator
}

output "this_exchange_tps_in_alarm_rule_period" {
  description = "Index query cycle. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_in_alarm_rule_period
}

output "this_exchange_tps_in_alarm_rule_project" {
  description = "Monitor project name. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_in_alarm_rule_project
}

output "this_exchange_tps_in_alarm_rule_silence_time" {
  description = " Notification silence period in the alarm state. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_in_alarm_rule_silence_time
}

output "this_exchange_tps_in_alarm_rule_statistics" {
  description = "Statistical method. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_in_alarm_rule_statistics
}

output "this_exchange_tps_in_alarm_rule_threshold" {
  description = "Alarm threshold value."
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_in_alarm_rule_threshold
}

output "this_exchange_tps_in_alarm_rule_triggered_count" {
  description = "Number of trigger alarm. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_in_alarm_rule_triggered_count
}

output "this_exchange_tps_out_alarm_rule_contact_groups" {
  description = "List contact groups of the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_out_alarm_rule_contact_groups
}

output "this_exchange_tps_out_alarm_rule_dimensions" {
  description = "Map of the resources associated with the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_out_alarm_rule_dimensions
}

output "this_exchange_tps_out_alarm_rule_effective_interval" {
  description = "The interval of effecting alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_out_alarm_rule_effective_interval
}

output "this_exchange_tps_out_alarm_rule_enabled" {
  description = "Whether to enable alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_out_alarm_rule_enabled
}

output "this_exchange_tps_out_alarm_rule_id" {
  description = "The ID of the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_out_alarm_rule_id
}

output "this_exchange_tps_out_alarm_rule_metric" {
  description = "Name of the monitoring metrics. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_out_alarm_rule_metric
}

output "this_exchange_tps_out_alarm_rule_name" {
  description = "The alarm name. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_out_alarm_rule_name
}

output "this_exchange_tps_out_alarm_rule_operator" {
  description = "Alarm comparison operator. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_out_alarm_rule_operator
}

output "this_exchange_tps_out_alarm_rule_period" {
  description = "Index query cycle. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_out_alarm_rule_period
}

output "this_exchange_tps_out_alarm_rule_project" {
  description = "Monitor project name. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_out_alarm_rule_project
}

output "this_exchange_tps_out_alarm_rule_silence_time" {
  description = " Notification silence period in the alarm state. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_out_alarm_rule_silence_time
}

output "this_exchange_tps_out_alarm_rule_statistics" {
  description = "Statistical method. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_out_alarm_rule_statistics
}

output "this_exchange_tps_out_alarm_rule_threshold" {
  description = "Alarm threshold value."
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_out_alarm_rule_threshold
}

output "this_exchange_tps_out_alarm_rule_triggered_count" {
  description = "Number of trigger alarm. "
  value       = module.amqp-ram-cloudmonitor.this_exchange_tps_out_alarm_rule_triggered_count
}

output "this_instance_message_input_alarm_rule_contact_groups" {
  description = "List contact groups of the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_input_alarm_rule_contact_groups
}

output "this_instance_message_input_alarm_rule_dimensions" {
  description = "Map of the resources associated with the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_input_alarm_rule_dimensions
}

output "this_instance_message_input_alarm_rule_effective_interval" {
  description = "The interval of effecting alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_input_alarm_rule_effective_interval
}

output "this_instance_message_input_alarm_rule_enabled" {
  description = "Whether to enable alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_input_alarm_rule_enabled
}

output "this_instance_message_input_alarm_rule_id" {
  description = "The ID of the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_input_alarm_rule_id
}

output "this_instance_message_input_alarm_rule_metric" {
  description = "Name of the monitoring metrics. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_input_alarm_rule_metric
}

output "this_instance_message_input_alarm_rule_name" {
  description = "The alarm name. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_input_alarm_rule_name
}

output "this_instance_message_input_alarm_rule_operator" {
  description = "Alarm comparison operator. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_input_alarm_rule_operator
}

output "this_instance_message_input_alarm_rule_period" {
  description = "Index query cycle. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_input_alarm_rule_period
}

output "this_instance_message_input_alarm_rule_project" {
  description = "Monitor project name. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_input_alarm_rule_project
}

output "this_instance_message_input_alarm_rule_silence_time" {
  description = " Notification silence period in the alarm state. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_input_alarm_rule_silence_time
}

output "this_instance_message_input_alarm_rule_statistics" {
  description = "Statistical method. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_input_alarm_rule_statistics
}

output "this_instance_message_input_alarm_rule_threshold" {
  description = "Alarm threshold value."
  value       = module.amqp-ram-cloudmonitor.this_instance_message_input_alarm_rule_threshold
}

output "this_instance_message_input_alarm_rule_triggered_count" {
  description = "Number of trigger alarm. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_input_alarm_rule_triggered_count
}

output "this_instance_message_output_alarm_rule_contact_groups" {
  description = "List contact groups of the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_output_alarm_rule_contact_groups
}

output "this_instance_message_output_alarm_rule_dimensions" {
  description = "Map of the resources associated with the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_output_alarm_rule_dimensions
}

output "this_instance_message_output_alarm_rule_effective_interval" {
  description = "The interval of effecting alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_output_alarm_rule_effective_interval
}

output "this_instance_message_output_alarm_rule_enabled" {
  description = "Whether to enable alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_output_alarm_rule_enabled
}

output "this_instance_message_output_alarm_rule_id" {
  description = "The ID of the alarm rule. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_output_alarm_rule_id
}

output "this_instance_message_output_alarm_rule_metric" {
  description = "Name of the monitoring metrics. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_output_alarm_rule_metric
}

output "this_instance_message_output_alarm_rule_name" {
  description = "The alarm name. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_output_alarm_rule_name
}

output "this_instance_message_output_alarm_rule_operator" {
  description = "Alarm comparison operator. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_output_alarm_rule_operator
}

output "this_instance_message_output_alarm_rule_period" {
  description = "Index query cycle. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_output_alarm_rule_period
}

output "this_instance_message_output_alarm_rule_project" {
  description = "Monitor project name. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_output_alarm_rule_project
}

output "this_instance_message_output_alarm_rule_silence_time" {
  description = " Notification silence period in the alarm state. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_output_alarm_rule_silence_time
}

output "this_instance_message_output_alarm_rule_statistics" {
  description = "Statistical method. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_output_alarm_rule_statistics
}

output "this_instance_message_output_alarm_rule_threshold" {
  description = "Alarm threshold value."
  value       = module.amqp-ram-cloudmonitor.this_instance_message_output_alarm_rule_threshold
}

output "this_instance_message_output_alarm_rule_triggered_count" {
  description = "Number of trigger alarm. "
  value       = module.amqp-ram-cloudmonitor.this_instance_message_output_alarm_rule_triggered_count
}


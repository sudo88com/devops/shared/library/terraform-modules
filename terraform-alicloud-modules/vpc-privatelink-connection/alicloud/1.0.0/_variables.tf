
variable "vpc_cidr_block" {
  type        = string
  description = "The secondary CIDR blocks for the VPC."
  default     = "172.16.0.0/12"
}

variable "vpc_name" {
  type        = string
  description = "The name of the VPC."
  default     = "tf-testAccVpcName"
}

variable "vpc_privatelink_bandwidth" {
  type        = string
  description = "The bandwidth of VPC privatelink."
  default     = "1024"
}

variable "vpc_privatelink_endpoint_name" {
  type        = string
  description = "The name of the VPC privatelink."
  default     = "tf-testAccVpcPrivatelinkName"
}

variable "vpc_privatelink_endpoint_service_description" {
  type        = string
  description = "The description of the VPC privatelink service."
  default     = "tf-testAccVpcPrivatelinkServiceDescription"
}

variable "vpc_security_group_description" {
  type        = string
  description = "The security group description of the VPC."
  default     = "tf-testAccVpcSecurityGroupDescription"
}

variable "vpc_security_group_name" {
  type        = string
  description = "The security group name of the VPC."
  default     = "tf-testAccVpcSecurityGroupName"
}


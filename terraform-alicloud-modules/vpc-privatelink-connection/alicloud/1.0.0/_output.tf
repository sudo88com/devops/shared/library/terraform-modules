
output "this_vpc_id" {
  description = ""
  value       = module.vpc-privatelink-connection.this_vpc_id
}

output "this_vpc_privatelink_endpoint_connection_id" {
  description = ""
  value       = module.vpc-privatelink-connection.this_vpc_privatelink_endpoint_connection_id
}

output "this_vpc_privatelink_service_id" {
  description = ""
  value       = module.vpc-privatelink-connection.this_vpc_privatelink_service_id
}


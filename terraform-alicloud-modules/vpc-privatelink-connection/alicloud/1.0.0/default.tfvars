
vpc_cidr_block = "172.16.0.0/12"

vpc_name = "tf-testAccVpcName"

vpc_privatelink_bandwidth = "1024"

vpc_privatelink_endpoint_name = "tf-testAccVpcPrivatelinkName"

vpc_privatelink_endpoint_service_description = "tf-testAccVpcPrivatelinkServiceDescription"

vpc_security_group_description = "tf-testAccVpcSecurityGroupDescription"

vpc_security_group_name = "tf-testAccVpcSecurityGroupName"


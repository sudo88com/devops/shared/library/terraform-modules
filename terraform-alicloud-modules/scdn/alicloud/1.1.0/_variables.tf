
variable "biz_name" {
  type        = string
  description = "from the Business Type Drop-down List. Valid values: download, image, scdn, video."
  default     = ""
}

variable "cert_infos" {
  type        = map(string)
  description = "Certificate Information."
  default     = {}
}

variable "check_url" {
  type        = string
  description = "The URL that is used to test the accessibility of the origin."
  default     = ""
}

variable "create_domain" {
  type        = string
  description = "Whether to create a new SCDN domain. If true, the domain_name is required."
  default     = ""
}

variable "domain_configs" {
  type        = list(object({
    function_name = string
    function_args = list(object({
      arg_name  = string
      arg_value = string
    }))
  }))
  description = "The domain configs."
  default     = []
}

variable "domain_name" {
  type        = string
  description = "The name SCDN domain. If it not existed, please set create_domain to true to create a new one."
  default     = ""
}

variable "profile" {
  type        = string
  description = "(Deprecated from v1.1.0) The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  default     = ""
}

variable "region" {
  type        = string
  description = "(Deprecated from v1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "resource_group_id" {
  type        = string
  description = "The ID of the resource group."
  default     = ""
}

variable "shared_credentials_file" {
  type        = string
  description = "(Deprecated from v1.1.0) This is the path to the shared credentials file. If this is not set and a profile is specified, $HOME/.aliyun/config.json will be used."
  default     = ""
}

variable "skip_region_validation" {
  type        = string
  description = "(Deprecated from v1.1.0) Skip static validation of region ID. Used by users of alternative AlibabaCloud-like APIs or users w/ access to regions that are not public (yet)."
  default     = false
}

variable "sources" {
  type        = map(string)
  description = "The origin information."
  default     = ""
}

variable "status" {
  type        = string
  description = "The domain status. Valid values: online, offline."
  default     = "online"
}


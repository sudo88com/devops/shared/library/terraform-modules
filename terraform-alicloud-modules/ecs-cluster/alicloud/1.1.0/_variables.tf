
variable "availability_zone" {
  type        = string
  description = "The available zone to launch ecs instance and other resources."
  default     = ""
}

variable "cluster_size" {
  type        = string
  description = "The number of ECS instances."
  default     = 6
}

variable "cpu_core_count" {
  type        = string
  description = "CPU core count used to fetch instance types."
  default     = 2
}

variable "image_id" {
  type        = string
  description = "The image id used to launch ecs instances. If not set, a system image with `image_name_regex` will be returned."
  default     = ""
}

variable "image_name_regex" {
  type        = string
  description = "The ECS image's name regex used to fetch specified image."
  default     = "^ubuntu_18.*_64"
}

variable "instance_charge_type" {
  type        = string
  description = "The charge type of instance. Choices are 'PostPaid' and 'PrePaid'."
  default     = "PostPaid"
}

variable "instance_name" {
  type        = string
  description = "Name used on all instances as prefix. Default to `this_module_name`."
  default     = ""
}

variable "instance_tags" {
  type        = map(string)
  description = "Used to mark specified ecs instance."
  default     = {
  "created_by": "Terraform",
  "created_from": "module-tf-alicloud-ecs-cluster"
}
}

variable "instance_type" {
  type        = string
  description = "The instance type used to launch ecs instances. If not set, a random type with `cpu_core_count` and `memory_size` will be returned."
  default     = ""
}

variable "internet_charge_type" {
  type        = string
  description = "The internet charge type of instance. Choices are 'PayByTraffic' and 'PayByBandwidth'."
  default     = "PayByTraffic"
}

variable "key_name" {
  type        = string
  description = "The key pair name used to config instances."
  default     = ""
}

variable "memory_size" {
  type        = string
  description = "Memory size used to fetch instance types."
  default     = 4
}

variable "password" {
  type        = string
  description = "The password of instance."
  default     = ""
}

variable "period" {
  type        = string
  description = "The period of instance when instance charge type is 'PrePaid'."
  default     = 1
}

variable "region" {
  type        = string
  description = "(Deprecated from version 1.1.0) The region used to launch this module resources."
  default     = ""
}

variable "security_group_ids" {
  type        = list(string)
  description = "List of security group ids used to create ECS instances. If not set, a new one will be created."
  default     = []
}

variable "system_category" {
  type        = string
  description = "The system disk category used to launch one or more ecs instances."
  default     = "cloud_efficiency"
}

variable "system_size" {
  type        = string
  description = "The system disk size used to launch one or more ecs instances."
  default     = "40"
}

variable "this_module_name" {
  type        = string
  description = ""
  default     = "terraform-alicloud-ecs-cluster"
}

variable "user_data" {
  type        = string
  description = "User data to pass to instance on boot"
  default     = ""
}

variable "vpc_cidr" {
  type        = string
  description = "The CIDR block used to launch a new VPC."
  default     = "172.16.0.0/16"
}

variable "vpc_id" {
  type        = string
  description = "The existing VPC ID. If not set, a new VPC will be created."
  default     = ""
}

variable "vswitch_cidr" {
  type        = string
  description = "The CIDR block used to launch a new VSwitch. If not set, `vpc_cidr` will be used."
  default     = ""
}

variable "vswitch_id" {
  type        = string
  description = "The existing VSwitch ID. If not set, a new vswitch will be created."
  default     = ""
}



locals {
  availability_zone = var.availability_zone
  cluster_size = var.cluster_size
  cpu_core_count = var.cpu_core_count
  image_id = var.image_id
  image_name_regex = var.image_name_regex
  instance_charge_type = var.instance_charge_type
  instance_name = var.instance_name
  instance_tags = var.instance_tags
  instance_type = var.instance_type
  internet_charge_type = var.internet_charge_type
  key_name = var.key_name
  memory_size = var.memory_size
  password = var.password
  period = var.period
  region = var.region
  security_group_ids = var.security_group_ids
  system_category = var.system_category
  system_size = var.system_size
  this_module_name = var.this_module_name
  user_data = var.user_data
  vpc_cidr = var.vpc_cidr
  vpc_id = var.vpc_id
  vswitch_cidr = var.vswitch_cidr
  vswitch_id = var.vswitch_id
}


availability_zone = ""

cluster_size = 6

cpu_core_count = 2

image_id = ""

image_name_regex = "^ubuntu_18.*_64"

instance_charge_type = "PostPaid"

instance_name = ""

instance_tags = {
  "created_by": "Terraform",
  "created_from": "module-tf-alicloud-ecs-cluster"
}

instance_type = ""

internet_charge_type = "PayByTraffic"

key_name = ""

memory_size = 4

password = ""

period = 1

region = ""

security_group_ids = []

system_category = "cloud_efficiency"

system_size = "40"

this_module_name = "terraform-alicloud-ecs-cluster"

user_data = ""

vpc_cidr = "172.16.0.0/16"

vpc_id = ""

vswitch_cidr = ""

vswitch_id = ""


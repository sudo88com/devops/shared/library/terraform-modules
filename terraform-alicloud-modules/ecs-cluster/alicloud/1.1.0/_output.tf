
output "this_access_instance_ip" {
  description = ""
  value       = module.ecs-cluster.this_access_instance_ip
}

output "this_access_internet_ip" {
  description = ""
  value       = module.ecs-cluster.this_access_internet_ip
}

output "this_availability_zone" {
  description = ""
  value       = module.ecs-cluster.this_availability_zone
}

output "this_ids" {
  description = ""
  value       = module.ecs-cluster.this_ids
}

output "this_image_id" {
  description = ""
  value       = module.ecs-cluster.this_image_id
}

output "this_instance_type" {
  description = ""
  value       = module.ecs-cluster.this_instance_type
}

output "this_key_name" {
  description = ""
  value       = module.ecs-cluster.this_key_name
}

output "this_private_ips" {
  description = ""
  value       = module.ecs-cluster.this_private_ips
}

output "this_security_group_ids" {
  description = ""
  value       = module.ecs-cluster.this_security_group_ids
}

output "this_vswitch_id" {
  description = ""
  value       = module.ecs-cluster.this_vswitch_id
}

